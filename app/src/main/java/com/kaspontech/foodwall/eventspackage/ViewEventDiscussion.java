package com.kaspontech.foodwall.eventspackage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.events.eventdiscussion.GetEventsDiscussionAdapter;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetEventDiscussionData;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetEventsDiscussionOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewEventDiscussion extends AppCompatActivity implements View.OnClickListener {


    // Actionbar
    Toolbar actionBar;
    TextView event_name;
    ImageButton back;

    // Recylerview
    RecyclerView eventDiscussionRecylerview;
    GetEventDiscussionData getEventDiscussionData;
    List<GetEventDiscussionData> getEventDiscussionDataList = new ArrayList<>();
    GetEventsDiscussionAdapter getEventsDiscussionAdapter;

    // Vars
    int eventId;
    String eventName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event_discussion);


        actionBar = (Toolbar) findViewById(R.id.actionbar_view_all_events_discussion);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        event_name = (TextView) actionBar.findViewById(R.id.toolbar_title);
        eventDiscussionRecylerview = (RecyclerView) findViewById(R.id.event_discussion_recylerview);

        back.setOnClickListener(this);


        /*Getting event id & event name*/
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            eventId = bundle.getInt("eventId");
            eventName = bundle.getString("eventName");
            event_name.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(eventName));

        }

        Log.e("Discussion_type", "onCreate: "+getEventDiscussionDataList.size() );


        if(getEventDiscussionDataList.isEmpty()){


            //All Discussion type display API

            getAllEventsDiscussion();


        }

    }




    /* All Discussion type display API */


    private void getAllEventsDiscussion() {

        if(Utility.isConnected(getApplicationContext())) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(ViewEventDiscussion.this, "userId"));

                Call<GetEventsDiscussionOutput> call = apiService.getEventsDiscussion("get_events_discussion_all", eventId, userid);

                call.enqueue(new Callback<GetEventsDiscussionOutput>() {
                    @Override
                    public void onResponse(Call<GetEventsDiscussionOutput> call, Response<GetEventsDiscussionOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            getEventDiscussionDataList.clear();

                            for (int i = 0; i < response.body().getData().size(); i++) {


                                int disEvtId = response.body().getData().get(i).getDisEvtId();
                                String disDescription = response.body().getData().get(i).getDisDescription();
                                String disImage = response.body().getData().get(i).getDisImage();
                                int totalLikes = response.body().getData().get(i).getTotalLikes();
                                int totalComments = response.body().getData().get(i).getTotalComments();
                                int createdBy = response.body().getData().get(i).getCreatedBy();
                                String createdOn = response.body().getData().get(i).getCreatedOn();
                                int userId = response.body().getData().get(i).getUserId();
                                String firstName = response.body().getData().get(i).getFirstName();
                                String lastName = response.body().getData().get(i).getLastName();
                                String picture = response.body().getData().get(i).getPicture();
                                int eventId = response.body().getData().get(i).getEventId();
                                int disLikes = response.body().getData().get(i).getDisLikes();
                                String eventName = response.body().getData().get(i).getEventName();

                                getEventDiscussionData = new GetEventDiscussionData(disEvtId, disDescription, disImage,
                                        totalLikes, totalComments, createdBy, createdOn,
                                        userId, firstName, lastName, picture, eventId, eventName, disLikes);

                                getEventDiscussionDataList.add(getEventDiscussionData);


                            }
                            /*Setting adapter*/
                            getEventsDiscussionAdapter = new GetEventsDiscussionAdapter(ViewEventDiscussion.this, getEventDiscussionDataList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ViewEventDiscussion.this);
                            eventDiscussionRecylerview.setLayoutManager(mLayoutManager);
                            eventDiscussionRecylerview.setItemAnimator(new DefaultItemAnimator());
                            eventDiscussionRecylerview.setAdapter(getEventsDiscussionAdapter);
                            eventDiscussionRecylerview.setNestedScrollingEnabled(false);
//                        eventDiscussionRecylerview.hasFixedSize();
                            getEventsDiscussionAdapter.notifyDataSetChanged();


                        }
                    }

                    @Override
                    public void onFailure(Call<GetEventsDiscussionOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "FailureError" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        }else {

            Toast.makeText(this, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

        }

    }


    /* Refresh API */

    private void getAllEventsDiscussion_refresh() {

        if(Utility.isConnected(getApplicationContext())){

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            int userid = Integer.parseInt(Pref_storage.getDetail(ViewEventDiscussion.this, "userId"));

            Call<GetEventsDiscussionOutput> call = apiService.getEventsDiscussion("get_events_discussion_all", eventId, userid);

            call.enqueue(new Callback<GetEventsDiscussionOutput>() {
                @Override
                public void onResponse(Call<GetEventsDiscussionOutput> call, Response<GetEventsDiscussionOutput> response) {

                    String responseStatus = response.body().getResponseMessage();

                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                    if (responseStatus.equals("success")) {

                        getEventDiscussionDataList.clear();

                        for (int i = 0; i < response.body().getData().size(); i++) {



                            int disEvtId = response.body().getData().get(i).getDisEvtId();
                            String disDescription = response.body().getData().get(i).getDisDescription();
                            String disImage = response.body().getData().get(i).getDisImage();
                            int totalLikes = response.body().getData().get(i).getTotalLikes();
                            int totalComments = response.body().getData().get(i).getTotalComments();
                            int createdBy = response.body().getData().get(i).getCreatedBy();
                            String createdOn = response.body().getData().get(i).getCreatedOn();
                            int userId = response.body().getData().get(i).getUserId();
                            String firstName = response.body().getData().get(i).getFirstName();
                            String lastName = response.body().getData().get(i).getLastName();
                            String picture = response.body().getData().get(i).getPicture();
                            int eventId = response.body().getData().get(i).getEventId();
                            int disLikes = response.body().getData().get(i).getDisLikes();
                            String eventName = response.body().getData().get(i).getEventName();

                            getEventDiscussionData = new GetEventDiscussionData(disEvtId, disDescription, disImage,
                                    totalLikes, totalComments, createdBy, createdOn,
                                    userId, firstName, lastName, picture, eventId, eventName, disLikes);

                            getEventDiscussionDataList.add(getEventDiscussionData);


                        }
                        /*Setting adapter*/
                        getEventsDiscussionAdapter = new GetEventsDiscussionAdapter(ViewEventDiscussion.this, getEventDiscussionDataList);
//                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ViewEventDiscussion.this);
//                        eventDiscussionRecylerview.setLayoutManager(mLayoutManager);
//                        eventDiscussionRecylerview.setItemAnimator(new DefaultItemAnimator());
                        eventDiscussionRecylerview.setAdapter(getEventsDiscussionAdapter);
//                        eventDiscussionRecylerview.setNestedScrollingEnabled(false);
//                        eventDiscussionRecylerview.hasFixedSize();
                        getEventsDiscussionAdapter.notifyDataSetChanged();



                    }
                }

                @Override
                public void onFailure(Call<GetEventsDiscussionOutput> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }}else {


            Toast.makeText(this, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

        }


    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            case R.id.back:

                finish();

                break;

        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        Log.e("Discussion_type", "onResume: "+getEventDiscussionDataList.size() );



        if(!getEventDiscussionDataList.isEmpty()){
            /* Refresh API */

            getAllEventsDiscussion_refresh();

//            getEventsDiscussionAdapter.notifyDataSetChanged();

        }else {

            getAllEventsDiscussion();
        }

    }
}
