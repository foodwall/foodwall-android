package com.kaspontech.foodwall.eventspackage;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventsCohost.GetEventCohostOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.REST_API.ProgressRequestBody;
//
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//


public class CreateEventDiscussion extends AppCompatActivity implements
        View.OnClickListener,
        ProgressRequestBody.UploadCallbacks {
    /**
     * Action Bar
     **/

    Toolbar actionBar;

    /**
     * Close button
     **/

    ImageButton close;

    /**
     * Post discussion
     **/

    TextView postDiscussion;

    /**
     * Event type Title
     **/
    TextView eventTypeTitle;

    /**
     * Image URI
     **/
    Uri imageUri;

    /**
     * Circle image view
     **/

    CircleImageView userphotoComment;


    /**
     * Edit text comments
     **/
    EditText edittxtComment;
    /**
     * Image button adding event discussion
     **/
    ImageButton addImageEventDiscussion;
    /**
     * Image view event discussion
     **/
    ImageView eventDiscussionImage;


    /**
     * Variables
     **/
    String userChoosenTask;
    String imagepath;
    String userImage;
    String eventName;
    String disImage = "";
    String disCaption;
    String imageExists = "";

    int discussType;
    int eventId;
    int imageStatus;
    int launchType;
    int discussId;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private static final int PICK_FROM_GALLERY = 1;

    private static final int REQUEST_PERMISSION_SETTING = 5;
    int PICK_IMAGE_MULTIPLE = 2;
    private static final int REQUEST_GALLERY_PERMISSION = 2;

    private static final String TAG = "CreateEventDiscussion";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event_discussion);

        /*Widgets initialization*/

        actionBar = findViewById(R.id.action_bar_create_event_discussion);
        close = actionBar.findViewById(R.id.close);
        postDiscussion = actionBar.findViewById(R.id.tv_create_event_discussion);
        eventTypeTitle = actionBar.findViewById(R.id.event_type_title);

        userphotoComment = findViewById(R.id.userphoto_comment);
        edittxtComment = findViewById(R.id.edittxt_comment);
        addImageEventDiscussion = findViewById(R.id.add_image_event_discussion);
        eventDiscussionImage = findViewById(R.id.event_discussion_image);

        /*On Click listener*/

        close.setOnClickListener(this);

        postDiscussion.setOnClickListener(this);

        addImageEventDiscussion.setOnClickListener(this);


        /*Getting input values from various pages and adapters*/

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            eventId = bundle.getInt("eventId");
            launchType = bundle.getInt("launchType");
            discussId = bundle.getInt("discuss_id");
            eventName = bundle.getString("eventName");
            disCaption = bundle.getString("disCaption");
            disImage = bundle.getString("disImage");

        }

        if (launchType == 2) {

            eventTypeTitle.setText("Edit post");

        }

        /*Discussion description setting*/

        if (disCaption != null) {

            edittxtComment.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(disCaption));
        }


        /*Discussion image setting*/
        if (disImage != null) {

            Utility.picassoImageLoader(disImage, 2, eventDiscussionImage, getApplicationContext());


        }


        /*Self image displaying*/
        userImage = Pref_storage.getDetail(CreateEventDiscussion.this, "picture_user_login");

        Utility.picassoImageLoader(userImage, 0, userphotoComment, getApplicationContext());

       /*Calling event co host api calling*/
        getEventCoHost(eventId);


    }

    @Override
    public void onClick(View v) {

        int id = v.getId();


        switch (id) {


            case R.id.close:

                if (launchType == 2) {

                    Intent intent = new Intent(CreateEventDiscussion.this, ViewEventDiscussion.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("eventId", eventId);
                    bundle.putString("eventName", eventName);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();

                } else {

                    finish();

                }


                break;

            case R.id.tv_create_event_discussion:

                if (edittxtComment.getText().toString().trim().length() != 0) {

                    final AlertDialog dialog = new SpotsDialog.Builder().setContext(CreateEventDiscussion.this).setMessage("").build();

                    dialog.show();

                    if (launchType == 1) {

                        validate();

                        if (imageStatus == 0) {

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                            try {

                                // Set up progress before call

                                int userid = Integer.parseInt(Pref_storage.getDetail(CreateEventDiscussion.this, "userId"));

                                RequestBody requestBodyEventDescription = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtComment.getText().toString().trim()));
                                RequestBody requestBodyImageExists = RequestBody.create(MediaType.parse("text/plain"), "");

                                Call<CommonOutputPojo> call = apiService.createEventDiscussion("create_edit_delete_events_discussion", 0, eventId, discussType,
                                        requestBodyEventDescription, imageStatus, 1, requestBodyImageExists, userid, 0, requestBodyImageExists);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {
                                            //Success
                                            dialog.dismiss();
                                            Intent intent = new Intent(CreateEventDiscussion.this, ViewEventDiscussion.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putInt("eventId", eventId);
                                            bundle.putString("eventName", eventName);
                                            intent.putExtras(bundle);
                                            startActivity(intent);
                                            finish();

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        dialog.dismiss();
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                        Toast.makeText(CreateEventDiscussion.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

                                    }
                                });
                            } catch (Exception e) {
                                dialog.dismiss();
                                Log.e("FailureError", "FailureError" + e.getMessage());
                                e.printStackTrace();
                            }


                        } else {

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                            try {

                                // Set up progress before call

                                int userid = Integer.parseInt(Pref_storage.getDetail(CreateEventDiscussion.this, "userId"));

                                RequestBody requestBodyEventDescription = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtComment.getText().toString().trim()));
//                                RequestBody requestBodyImageExists = RequestBody.create(MediaType.parse("text/plain"), disImage);

                                File file = new File(imagepath);
                                ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
                                MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), fileBody);

                                Call<CommonOutputPojo> call = apiService.createEventDiscussionWithImage("create_edit_delete_events_discussion", discussId, eventId, discussType,
                                        requestBodyEventDescription, imageStatus, 0, disImage, userid, 0, filePart);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {
                                            //Success
                                            Log.e("CreateEventDiscussion", "responseStatus->" + responseStatus);
                                            Intent intent = new Intent(CreateEventDiscussion.this, ViewEventDiscussion.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putInt("eventId", eventId);
                                            bundle.putString("eventName", eventName);
                                            intent.putExtras(bundle);
                                            startActivity(intent);
                                            finish();
                                            dialog.dismiss();

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                        Toast.makeText(CreateEventDiscussion.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();

                                    }
                                });
                            } catch (Exception e) {

                                dialog.dismiss();
                                Log.e("FailureError", "FailureError" + e.getMessage());
                                e.printStackTrace();
                            }

                        }

                    } else if (launchType == 2) {

                        editValidate();

                        if (imageStatus == 0) {

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                            try {

                                // Set up progress before call

                                int userid = Integer.parseInt(Pref_storage.getDetail(CreateEventDiscussion.this, "userId"));

                                RequestBody requestBodyEventDescription = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtComment.getText().toString().trim()));
                                RequestBody requestBodyImageExists = RequestBody.create(MediaType.parse("text/plain"), "");

                                Call<CommonOutputPojo> call = apiService.createEventDiscussion("create_edit_delete_events_discussion", discussId, eventId, discussType,
                                        requestBodyEventDescription, imageStatus, 1, requestBodyImageExists, userid, 0, requestBodyImageExists);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {
                                            //Success
                                            dialog.dismiss();
                                            Intent intent = new Intent(CreateEventDiscussion.this, ViewEventDiscussion.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putInt("eventId", eventId);
                                            bundle.putString("eventName", eventName);
                                            intent.putExtras(bundle);
                                            startActivity(intent);
                                            finish();

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        dialog.dismiss();
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                        Toast.makeText(CreateEventDiscussion.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

                                    }
                                });
                            } catch (Exception e) {
                                dialog.dismiss();
                                Log.e("FailureError", "FailureError" + e.getMessage());
                                e.printStackTrace();
                            }


                        } else {


                            if (imageExists.equals("")) {

                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    // Set up progress before call

                                    int userid = Integer.parseInt(Pref_storage.getDetail(CreateEventDiscussion.this, "userId"));

                                    RequestBody requestBodyEventDescription = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtComment.getText().toString().trim()));
//                                    RequestBody requestBodyImageExists = RequestBody.create(MediaType.parse("text/plain"), disImage);

                                    File file = new File(imagepath);
                                    ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
                                    MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), fileBody);

                                    Call<CommonOutputPojo> call = apiService.createEventDiscussionWithImage("create_edit_delete_events_discussion", discussId, eventId, discussType,
                                            requestBodyEventDescription, imageStatus, 0, disImage, userid, 0, filePart);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("success")) {
                                                //Success
                                                Log.e("CreateEventDiscussion", "responseStatus->" + responseStatus);
                                                Intent intent = new Intent(CreateEventDiscussion.this, ViewEventDiscussion.class);
                                                Bundle bundle = new Bundle();
                                                bundle.putInt("eventId", eventId);
                                                bundle.putString("eventName", eventName);
                                                intent.putExtras(bundle);
                                                startActivity(intent);
                                                finish();
                                                dialog.dismiss();

                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                            Toast.makeText(CreateEventDiscussion.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();

                                        }
                                    });
                                } catch (Exception e) {

                                    dialog.dismiss();
                                    Log.e("FailureError", "FailureError" + e.getMessage());
                                    e.printStackTrace();
                                }

                            } else {


                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    // Set up progress before call
                                    Log.e(TAG, "onClick: discussId:" + discussId);
                                    Log.e(TAG, "onClick: eventId:" + eventId);
                                    Log.e(TAG, "onClick: discussType:" + discussType);
                                    Log.e(TAG, "onClick: edittxtComment:" + edittxtComment.getText().toString());
                                    Log.e(TAG, "onClick: imageStatus:" + imageStatus);
                                    Log.e(TAG, "onClick: disImage:" + disImage);

                                    int userid = Integer.parseInt(Pref_storage.getDetail(CreateEventDiscussion.this, "userId"));

                                    RequestBody requestBodyEventDescription = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtComment.getText().toString()));
//                                    RequestBody requestBodyImageExists = RequestBody.create(MediaType.parse("text/plain"), disImage);
                                    RequestBody requestBodyImage = RequestBody.create(MediaType.parse("text/plain"), "");
                                    MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", "", requestBodyImage);

                                    Call<CommonOutputPojo> call = apiService.createEventDiscussionWithImage("create_edit_delete_events_discussion", discussId, eventId, discussType,
                                            requestBodyEventDescription, imageStatus, 0, disImage, userid, 0, filePart);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("success")) {
                                                //Success
                                                Log.e("CreateEventDiscussion", "responseStatus->" + responseStatus);
                                                Intent intent = new Intent(CreateEventDiscussion.this, ViewEventDiscussion.class);
                                                Bundle bundle = new Bundle();
                                                bundle.putInt("eventId", eventId);
                                                bundle.putString("eventName", eventName);
                                                intent.putExtras(bundle);
                                                startActivity(intent);
                                                finish();
                                                dialog.dismiss();

                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                            Toast.makeText(CreateEventDiscussion.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();

                                        }
                                    });
                                } catch (Exception e) {

                                    dialog.dismiss();
                                    Log.e("FailureError", "FailureError" + e.getMessage());
                                    e.printStackTrace();
                                }
                            }


                        }
                    }


                } else {

                    edittxtComment.setError("Write something...");

                }
                // Post Discussion


                break;

            case R.id.add_image_event_discussion:

                try {

                    if (ActivityCompat.checkSelfPermission(CreateEventDiscussion.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(CreateEventDiscussion.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECT_FILE);

                    } else {

                        selectImage();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

        }

    }


    private void validate() {

        if (eventDiscussionImage.getDrawable() == null) {

            imageStatus = 0;

        } else {

            imageStatus = 1;

        }

    }

    private void editValidate() {

        if (eventDiscussionImage.getDrawable() == null) {

            imageStatus = 0;

        } else {

            imageStatus = 1;

        }

        if (imagepath == null && launchType == 2) {

            imageExists = disImage;

        } else {

            imageExists = "";

        }

    }

    /*Calling event co host api calling*/

    public void getEventCoHost(int eventId) {
        if(Utility.isConnected(getApplicationContext())){
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                final int userid = Integer.parseInt(Pref_storage.getDetail(CreateEventDiscussion.this, "userId"));

                Call<GetEventCohostOutput> call = apiService.getEventCohost("get_events_cohosts", eventId);

                call.enqueue(new Callback<GetEventCohostOutput>() {
                    @Override
                    public void onResponse(Call<GetEventCohostOutput> call, Response<GetEventCohostOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                int cohostId = response.body().getData().get(i).getUserId();

                                if (cohostId == userid) {


                                    discussType = 1;


                                }

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetEventCohostOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }
        }else {
            Toast.makeText(this, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

        }




    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    /*List of image select option*/

                    selectImage();

                }
                break;
        }
    }

    /*List of image select option*/

    private void selectImage() {
        /*Validation*/
        validate();

        List<String> listItems = new ArrayList<String>();

        listItems.add("Take Photo");
        listItems.add("Choose from library");
        listItems.add("Cancel");

        if (imageStatus == 1) {

            listItems.remove(2);
            listItems.add("Remove photo");
            listItems.add("Cancel");

        }


        /*Alert dialog*/

        final CharSequence[] items = listItems.toArray(new CharSequence[listItems.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(CreateEventDiscussion.this);
        builder.setTitle("Choose item");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(CreateEventDiscussion.this);

                if (items[item].equals("Take Photo")) {

                    try {

                        if (ActivityCompat.checkSelfPermission(CreateEventDiscussion.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(CreateEventDiscussion.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(CreateEventDiscussion.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);

                        } else {

                            Log.e(TAG, "onClick: else part");

                            if (ActivityCompat.checkSelfPermission(CreateEventDiscussion.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(CreateEventDiscussion.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                                Log.e(TAG, "onClick: camera");

                                /*Intent to camera*/
                                cameraIntent();

                            } else {

                                Log.e(TAG, "onClick: cameraStorageGrantRequest");

                                /*Method for camera storage request*/

                                cameraStorageGrantRequest();

                            }


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (items[item].equals("Choose from library")) {

                    try {
                        if (ActivityCompat.checkSelfPermission(CreateEventDiscussion.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(CreateEventDiscussion.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);

                        } else {
                            /*Intent to camera*/

                            galleryIntent();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else if (items[item].equals("Cancel")) {

                    dialog.dismiss();

                } else if (items[item].equals("Remove photo")) {

                    eventDiscussionImage.setImageDrawable(null);

                }
            }
        });
        builder.show();

    }

    /*Method for camera storage request*/

    private void cameraStorageGrantRequest() {
        /*Alert dialog*/
        AlertDialog.Builder builder = new AlertDialog.Builder(
                CreateEventDiscussion.this);

        builder.setTitle("Food Wall would like to access your camera and storage")
                .setMessage("You previously chose not to use Android's camera or storage with Food Wall. To set your profile pic, allow camera and storage permission in App Settings. Click Ok to allow.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // permission dialog
                        dialog.dismiss();
                        try {

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();


    }
    /*Intent to camera*/

    private void galleryIntent() {

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_GALLERY_PERMISSION);
        } else {

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);

        }

    }
    /*Intent to camera*/

    private void cameraIntent() {

        if (ActivityCompat.checkSelfPermission(CreateEventDiscussion.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(CreateEventDiscussion.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(CreateEventDiscussion.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, REQUEST_CAMERA);

        } else {


            ActivityCompat.requestPermissions(CreateEventDiscussion.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {


            if (requestCode == PICK_IMAGE_MULTIPLE) {

                try {

                    if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && null != data && !(data.toString().equals("Intent { (has extras) }"))) {

                        if (data.getData() != null) {


                            Uri mImageUri = data.getData();
                            Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                            cursor.moveToFirst();
                            String document_id = cursor.getString(0);
                            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                            cursor.close();

                            cursor = getContentResolver().query(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                            cursor.moveToFirst();
                            imagepath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                            /*Displayinng event discussion image*/

                            Utility.picassoImageLoader(imagepath, 2, eventDiscussionImage, getApplicationContext());

                            cursor.close();

                            Log.v("LOG_TAG", "Selected Images" + imagepath);

                        } else {

                            if (data.getClipData() != null) {
                                ClipData mClipData = data.getClipData();
                                ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                                for (int i = 0; i < mClipData.getItemCount(); i++) {

                                    ClipData.Item item = mClipData.getItemAt(i);
                                    Uri uri = item.getUri();
                                    mArrayUri.add(uri);

                                    // Get the cursor

                                    Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                                    cursor.moveToFirst();
                                    String document_id = cursor.getString(0);
                                    document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                                    cursor.close();

                                    cursor = getContentResolver().query(
                                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                            null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                                    cursor.moveToFirst();
                                    imagepath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                                    /*Displayinng event discussion image*/

                                    Utility.picassoImageLoader(imagepath, 2, eventDiscussionImage, getApplicationContext());



                                    cursor.close();


                                    Log.v("LOG_TAG", "Selected Images" + imagepath);
                                }
                                Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                            }
                        }
                    } else {
                        /* Toast.makeText(this, "You haven't picked Image",Toast.LENGTH_LONG).show();*/
                    }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_CAMERA) {

                /*Capture result*/
                onCaptureImageResult(data);


            }
        }
    }

    /*Capture result*/

    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = null;
        try {
            thumbnail = getThumbnail(imageUri);

            /*Saving bitmap*/

            saveBitmap(thumbnail);

        } catch (IOException e) {
            e.printStackTrace();
            Log.e("CameraCaptureError", "CameraCaptureError->" + e.getMessage());
        }
    }

    /*Saving bitmap*/

    private void saveBitmap(Bitmap bm) {

        String folder_main = "FoodWall";

        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }

        File newFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        Log.e("ImageFile", "ImageFile->" + newFile);

        try {

            FileOutputStream fileOutputStream = new FileOutputStream(newFile);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            imagepath = String.valueOf(newFile);

            /*Displaying event discussion image*/

            Utility.picassoImageLoader(imagepath, 2, eventDiscussionImage, getApplicationContext());


            Log.e("MyPath", "MyImagePath->" + imagepath);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /*Getting thumbnail*/

    public Bitmap getThumbnail(Uri uri) throws IOException {
        InputStream input = CreateEventDiscussion.this.getContentResolver().openInputStream(uri);
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 800) ? (originalSize / 800) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }


    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor c = getApplicationContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        c.moveToFirst();

        int columnIndex = c.getColumnIndex(filePathColumn[0]);
        String picturePath = c.getString(columnIndex);
        c.close();

        Bitmap photo1 = (BitmapFactory.decodeFile(picturePath));

        /*Displaying event discussion image*/

        eventDiscussionImage.setImageBitmap(photo1);


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (launchType == 2) {
            /*Landing page continuation*/
            Intent intent = new Intent(CreateEventDiscussion.this, ViewEventDiscussion.class);
            Bundle bundle = new Bundle();
            bundle.putInt("eventId", eventId);
            bundle.putString("eventName", eventName);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();

        } else {

            finish();

        }
    }

    @Override
    public void onProgressUpdate(int percentage) {
        Log.e(TAG, "onProgressUpdate: " + percentage);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
