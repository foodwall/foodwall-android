package com.kaspontech.foodwall.eventspackage;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.adapters.events.EventsType.GetInvitedEventsAdapter;
import com.kaspontech.foodwall.adapters.events.EventsType.GetPublicEventsAdapter;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.eventspackage.eventtypes.InvitedEvent;
import com.kaspontech.foodwall.eventspackage.eventtypes.MyEvents;
import com.kaspontech.foodwall.eventspackage.eventtypes.PublicEvents;
import com.kaspontech.foodwall.menuPackage.UserMenu;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetAllEventData;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetAllEventsOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Events extends AppCompatActivity implements View.OnClickListener/*, RadioGroup.OnCheckedChangeListener*/ {


    /**
     * Application TAG
     **/

    private static final String TAG = "Events";

    /**
     * Bottom sheet dialog
     **/
    BottomSheetDialog dialog;

    /**
     * Action Bar
     **/

    Toolbar actionBar;
    /**
     * Image button for back
     **/
    ImageButton back;
    /**
     * Image button for search
     **/
    ImageButton eventSearch;


    /*FloatingActionMenu fab_menu;
    FloatingActionButton privateEvent, publicEvent;*/


    LinearLayout events_radio;

    /**
     * Event tab layout
     **/
    TabLayout tabEvents;


    /**
     * Invited event recyclerview
     **/
    RecyclerView invitedEventsRecylerView;
    /**
     * Event recyclerview
     **/
    RecyclerView publicEventsRecylerview;

    /**
     * Invited event adapter
     **/

    GetInvitedEventsAdapter getInvitedEventsAdapter;

    /**
     * Public event adapter
     **/

    GetPublicEventsAdapter getPublicEventsAdapter;

    /**
     * Invited event list
     **/

    List<GetAllEventData> getInvitedEventList = new ArrayList<>();

    /**
     * Public event list
     **/

    List<GetAllEventData> getPublicEventList = new ArrayList<>();

    /**
     * Event all pojo
     **/
    GetAllEventData getAllEventData;


//    SegmentedGroup segmentedGroup;
//    RadioButton invitedType, publicType;

    /**
     * Fragments
     **/
    Fragment invitedEvents, publicEvents, myEvents;

    /**
     * Integer variables
     **/
    int selectedType;

    int eventCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        //This is event class

        //((Home)getActivity()).updateStatusBarColor("#8dd76e");


        /*Widget initialization*/

        actionBar = findViewById(R.id.action_bar_events);

        back = actionBar.findViewById(R.id.back);

        eventSearch = actionBar.findViewById(R.id.event_search);

        invitedEventsRecylerView = findViewById(R.id.invited_events_recyclerview);

        publicEventsRecylerview = findViewById(R.id.public_events_recyclerview);

        tabEvents = findViewById(R.id.tab_events);

       /* fab_menu = (FloatingActionMenu) findViewById(R.id.fab_menu);
        privateEvent = (FloatingActionButton) findViewById(R.id.fab_private_event);
        publicEvent = (FloatingActionButton) findViewById(R.id.fab_public_event);*/

//        segmentedGroup = (SegmentedGroup) findViewById(R.id.segmented_events);
//        invitedType = (RadioButton) findViewById(R.id.type_invited_events);
//        publicType = (RadioButton) findViewById(R.id.type_public_events);
//        events_radio = (LinearLayout) findViewById(R.id.events_radio);

        back.setOnClickListener(this);

        eventSearch.setOnClickListener(this);

       /* privateEvent.setOnClickListener(this);
        publicEvent.setOnClickListener(this);*/
//        segmentedGroup.setOnCheckedChangeListener(this);


        /*Fragment initialization*/

        /*Invited event*/

        invitedEvents = new InvitedEvent();

        /*Public Event*/

        publicEvents = new PublicEvents();

        /*My Event*/

        myEvents = new MyEvents();

        /*Tab layout setting*/
        tabEvents.addTab(tabEvents.newTab().setText("Invited"));
        tabEvents.addTab(tabEvents.newTab().setText("Public"));
        tabEvents.addTab(tabEvents.newTab().setText("My Events"));

        /*Input variables from various activities and adapters*/
        Intent intent = getIntent();
        String eventType = intent.getStringExtra("eventType");

        if(eventType != null){

            eventCode = Integer.parseInt(eventType);

        }

        Log.e("CheckEventCode", "onCreate: "+eventCode );

        switch (eventCode) {
            default:
                /*Invited event*/
                replaceFragment(invitedEvents);

                tabEvents.getTabAt(0).select();
                break;


            case 1:

                /*Invited event*/

                replaceFragment(invitedEvents);
                tabEvents.getTabAt(0).select();

                break;

            case 2:
                /*Public Event*/
                replaceFragment(publicEvents);
                tabEvents.getTabAt(1).select();

                break;



        }


        tabEvents.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {

                    case 0:

                        /*Invited event*/

                        replaceFragment(invitedEvents);

                        break;

                    case 1:
                        /*Public Event*/

                        replaceFragment(publicEvents);


                        break;

                    case 2:
                        /*My Event*/

                        replaceFragment(myEvents);


                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        /*getInvitedEventList.clear();

        publicEventsRecylerview.setVisibility(View.GONE);

        getAllEventsData(1);

        invitedEventsRecylerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab_menu.getVisibility() == View.VISIBLE) {
                    fab_menu.setVisibility(View.GONE);
//                    events_radio.setVisibility(View.GONE);
                } else if (dy < 0 && fab_menu.getVisibility() != View.VISIBLE) {
                    fab_menu.setVisibility(View.VISIBLE);
//                    events_radio.setVisibility(View.VISIBLE);
                }
            }
        });

        publicEventsRecylerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab_menu.getVisibility() == View.VISIBLE) {
                    fab_menu.setVisibility(View.GONE);
//                    events_radio.setVisibility(View.GONE);
                } else if (dy < 0 && fab_menu.getVisibility() != View.VISIBLE) {
                    fab_menu.setVisibility(View.VISIBLE);
//                    events_radio.setVisibility(View.VISIBLE);
                }
            }
        });
*/
    }


    private void getAllEventsData(int eventType) {


        if (Utility.isConnected(Events.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(Events.this, "userId"));

                Call<GetAllEventsOutput> call = apiService.getAllEvents("get_events_type_all", userid, eventType);

                call.enqueue(new Callback<GetAllEventsOutput>() {
                    @Override
                    public void onResponse(Call<GetAllEventsOutput> call, Response<GetAllEventsOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                int eventId = response.body().getData().get(i).getEventId();
                                String eventName = response.body().getData().get(i).getEventName();
                                String eventDescription = response.body().getData().get(i).getEventDescription();
                                int totalLikes = response.body().getData().get(i).getTotalLikes();
                                int totalComments = response.body().getData().get(i).getTotalComments();
                                int eventType = response.body().getData().get(i).getEventType();
                                int userLike = response.body().getData().get(i).getEvtLikes();
                                int userGoing = response.body().getData().get(i).getGngLikes();
                                int share_type = response.body().getData().get(i).getShareType();
                                int total_discussion = response.body().getData().get(i).getTotal_discussion();
                                String ticketUrl = response.body().getData().get(i).getTicketUrl();
                                String eventImage = response.body().getData().get(i).getEventImage();
                                String eventLocation = response.body().getData().get(i).getLocation();
                                String eventStartDate = response.body().getData().get(i).getStartDate();
                                String eventEndDate = response.body().getData().get(i).getEndDate();
                                String eventCreatedBy = response.body().getData().get(i).getCreatedBy();
                                String eventCreatedOn = response.body().getData().get(i).getCreatedOn();
                                String firstName = response.body().getData().get(i).getFirstName();
                                String lastName = response.body().getData().get(i).getLastName();
                                String userPicture = response.body().getData().get(i).getPicture();

                                if (eventType == 1) {

                                    getAllEventData = new GetAllEventData(eventId, eventName, eventDescription, totalLikes, totalComments, eventImage, eventType,
                                            ticketUrl, eventLocation, eventStartDate, eventEndDate, eventCreatedBy,
                                            eventCreatedOn, firstName, lastName, userPicture, userLike, userGoing, share_type, total_discussion);
                                    getInvitedEventList.add(getAllEventData);

                                    getInvitedEventsAdapter = new GetInvitedEventsAdapter(Events.this, getInvitedEventList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Events.this);
                                    invitedEventsRecylerView.setLayoutManager(mLayoutManager);
                                    invitedEventsRecylerView.setItemAnimator(new DefaultItemAnimator());
                                    invitedEventsRecylerView.setAdapter(getInvitedEventsAdapter);
                                    invitedEventsRecylerView.hasFixedSize();
                                    getInvitedEventsAdapter.notifyDataSetChanged();

                                } else if (eventType == 2) {

                                    getAllEventData = new GetAllEventData(eventId, eventName, eventDescription, totalLikes, totalComments, eventImage, eventType,
                                            ticketUrl, eventLocation, eventStartDate, eventEndDate, eventCreatedBy, eventCreatedOn, firstName,
                                            lastName, userPicture, userLike, userGoing, share_type, total_discussion);
                                    getPublicEventList.add(getAllEventData);


                                    /*Setting adapter*/
                                    getPublicEventsAdapter = new GetPublicEventsAdapter(Events.this, getPublicEventList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Events.this);
                                    publicEventsRecylerview.setLayoutManager(mLayoutManager);
                                    publicEventsRecylerview.setItemAnimator(new DefaultItemAnimator());
                                    publicEventsRecylerview.setAdapter(getPublicEventsAdapter);
                                    publicEventsRecylerview.hasFixedSize();
                                    getPublicEventsAdapter.notifyDataSetChanged();

                                }


                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<GetAllEventsOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {


        }


    }


    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {

            case R.id.back:


                Intent intent = new Intent(Events.this, UserMenu.class);
                //intent.putExtra("redirect", "profile");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;


            case R.id.event_search:
                /*Showing bottom dialog alert*/

                showBottomSheetDialog();

                break;


            case R.id.ll_private_event:

               /* Toast.makeText(this, "Private", Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(Events.this, CreateEvent.class);
                intent1.putExtra("eventName", "Create private event");
                intent1.putExtra("eventCode", 1);
                intent1.putExtra("LaunchMode", 1);
                startActivity(intent1);
                dialog.dismiss();*/

//                fab_menu.close(true);

                break;

            case R.id.ll_public_event:

                /*Toast.makeText(this, "Public", Toast.LENGTH_SHORT).show();
                Intent intent2 = new Intent(Events.this, CreateEvent.class);
                intent2.putExtra("eventName", "Create public event");
                intent2.putExtra("eventCode", 2);
                intent2.putExtra("LaunchMode", 1);
                startActivity(intent2);
                dialog.dismiss();*/

//                fab_menu.close(true);

                break;

        }

    }

    /*Fragment replacing*/
    public void replaceFragment(Fragment fragment) {

        try {

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.events_container, fragment);
            fragmentTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public void replaceFragment(Fragment fragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }


    @Override
    public void onBackPressed()
    {
        if(getFragmentManager().getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }*/


    /*Showing bottom dialog alert*/

    public void showBottomSheetDialog() {

        View view = getLayoutInflater().inflate(R.layout.alert_bottom_create_events, null);
        dialog = new BottomSheetDialog(Events.this);
        dialog.setContentView(view);

        LinearLayout private_event = dialog.findViewById(R.id.ll_private_event);
        LinearLayout public_event = dialog.findViewById(R.id.ll_public_event);

        /*Intent to create event click listener*/
        private_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent1 = new Intent(Events.this, CreateEvent.class);
                intent1.putExtra("eventName", "Create private event");
                intent1.putExtra("eventCode", 1);
                intent1.putExtra("LaunchMode", 1);
                startActivity(intent1);
                finish();
                dialog.dismiss();
            }
        });

        /*Intent to create private event click listener*/

        public_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(Events.this, "Public", Toast.LENGTH_SHORT).show();
                Intent intent2 = new Intent(Events.this, CreateEvent.class);
                intent2.putExtra("eventName", "Create public event");
                intent2.putExtra("eventCode", 2);
                intent2.putExtra("LaunchMode", 1);
                startActivity(intent2);
                finish();
                dialog.dismiss();

            }
        });

        dialog.show();

    }


    /*@Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {


        switch (group.getCheckedRadioButtonId()) {

            case R.id.type_invited_events:

               *//* getInvitedEventList.clear();
                publicEventsRecylerview.setVisibility(View.GONE);
                invitedEventsRecylerView.setVisibility(View.VISIBLE);
                getAllEventsData(1);*//*

                replaceFragment(invitedEvents);


                break;


            case R.id.type_public_events:


               *//* getInvitedEventList.clear();
                publicEventsRecylerview.setVisibility(View.VISIBLE);
                invitedEventsRecylerView.setVisibility(View.GONE);
                getAllEventsData(2);*//*

                replaceFragment(publicEvents);


                break;

            case R.id.type_my_events:


               *//* getInvitedEventList.clear();
                publicEventsRecylerview.setVisibility(View.VISIBLE);
                invitedEventsRecylerView.setVisibility(View.GONE);
                getAllEventsData(2);*//*

                replaceFragment(myEvents);


                break;

        }

    }*/
}
