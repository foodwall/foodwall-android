package com.kaspontech.foodwall.eventspackage.eventtypes;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.events.EventsType.GetInvitedEventsAdapter;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetAllEventData;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetAllEventsOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class InvitedEvent extends Fragment {
    /**
     * Context
     **/
    Context context;
    /**
     * View
     **/
    View view;

    /**
     * No events available
     **/
    LinearLayout llNoEvents;
    /**
     *Invited events recyclerview
     **/
    RecyclerView invitedEventsRecylerView;

    /**
     *Invited events adapter
     **/
    GetInvitedEventsAdapter getInvitedEventsAdapter;
    /**
     *Invited events arraylist
     **/
    List<GetAllEventData> getInvitedEventList = new ArrayList<>();

    /**
     *Invited events pojo
     **/
    GetAllEventData getAllEventData;


    public InvitedEvent() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onSaveInstanceState(savedInstanceState);

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_invited_event, container, false);
        context = view.getContext();

        /*No events available*/

        llNoEvents = view.findViewById(R.id.ll_no_events);


        /*Invited events recyclerview*/

        invitedEventsRecylerView = view.findViewById(R.id.invited_events_recyclerview);


        /*Calling invited events api call*/
//        getallEventsData();

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        /*Calling invited events api call*/

        getallEventsData();

    }
    /*Calling invited events api call*/

    private void getallEventsData() {


        if (Utility.isConnected(context)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Call<GetAllEventsOutput> call = apiService.getAllEvents("get_events_type_all", userid, 1);

                call.enqueue(new Callback<GetAllEventsOutput>() {
                    @Override
                    public void onResponse(@NonNull Call<GetAllEventsOutput> call, @NonNull Response<GetAllEventsOutput> response) {


                        if (response.body() != null) {
                            if (response.body().getResponseMessage().equals("success")) {

                                getInvitedEventList.clear();

                                //Success


                                Log.e("responseStatus", "responseSize->" + response.body().getData().size());
                                llNoEvents.setVisibility(View.GONE);


                                for (int i = 0; i < response.body().getData().size(); i++) {

                                    int eventId = response.body().getData().get(i).getEventId();
                                    String eventName = response.body().getData().get(i).getEventName();
                                    String eventDescription = response.body().getData().get(i).getEventDescription();
                                    int totalLikes = response.body().getData().get(i).getTotalLikes();
                                    int totalComments = response.body().getData().get(i).getTotalComments();
                                    int eventType = response.body().getData().get(i).getEventType();
                                    int userLike = response.body().getData().get(i).getEvtLikes();
                                    int userGoing = response.body().getData().get(i).getGngLikes();
                                    int shareType = response.body().getData().get(i).getShareType();
                                    int totalDiscussion = response.body().getData().get(i).getTotal_discussion();
                                    String ticketUrl = response.body().getData().get(i).getTicketUrl();
                                    String eventImage = response.body().getData().get(i).getEventImage();
                                    String eventLocation = response.body().getData().get(i).getLocation();
                                    String eventStartDate = response.body().getData().get(i).getStartDate();
                                    String eventEndDate = response.body().getData().get(i).getEndDate();
                                    String eventCreatedBy = response.body().getData().get(i).getCreatedBy();
                                    String eventCreatedOn = response.body().getData().get(i).getCreatedOn();
                                    String firstName = response.body().getData().get(i).getFirstName();
                                    String lastName = response.body().getData().get(i).getLastName();
                                    String userPicture = response.body().getData().get(i).getPicture();

                                    getAllEventData = new GetAllEventData(eventId, eventName, eventDescription, totalLikes, totalComments, eventImage, eventType,
                                            ticketUrl, eventLocation, eventStartDate, eventEndDate, eventCreatedBy, eventCreatedOn,
                                            firstName, lastName, userPicture, userLike, userGoing, shareType, totalDiscussion);
                                    getInvitedEventList.add(getAllEventData);


                                }

                                /*Settings adapter*/

                                getInvitedEventsAdapter = new GetInvitedEventsAdapter(context, getInvitedEventList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                invitedEventsRecylerView.setLayoutManager(mLayoutManager);
                                invitedEventsRecylerView.setItemAnimator(new DefaultItemAnimator());
                                invitedEventsRecylerView.setAdapter(getInvitedEventsAdapter);
                                invitedEventsRecylerView.hasFixedSize();
                                invitedEventsRecylerView.setNestedScrollingEnabled(false);
                                getInvitedEventsAdapter.notifyDataSetChanged();


                            }else if(response.body().getResponseMessage().equals("nodata")){

                                llNoEvents.setVisibility(View.VISIBLE);

                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetAllEventsOutput> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());


                        llNoEvents.setVisibility(View.VISIBLE);


                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(context, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

        }


    }




}
