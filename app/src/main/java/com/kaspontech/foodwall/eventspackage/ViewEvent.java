package com.kaspontech.foodwall.eventspackage;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.adapters.events.PostCommentEventAdapter;
import com.kaspontech.foodwall.adapters.events.SelectShareInviteAdapter;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventLikes.GetEventLikesUserPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventsComments.EventsCommentsUserData;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventsComments.GetEventsCommentsPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetAllEventsOutput;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersOutput;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;
import com.kaspontech.foodwall.utills.ViewImage;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ViewEvent extends _SwipeActivityClass implements View.OnClickListener {


    /**
     * Bottoms sheet dialog
     **/
    BottomSheetDialog dialog;

    /**
     * Cordinator layout
     **/
    CoordinatorLayout coLayout;

    /**
     * Imageview background
     **/
    ImageView eventBackGround;

    /**
     * Imageview user image
     **/
    CircleImageView userImage;
    CircleImageView userphotoComment;

    TabLayout eventTablayout;

    LinearLayout ticketSection;
    EditText edittxt_comment;
    FloatingActionButton interested_fab;
    ExpandableTextView eventDescription_tv;

    Button tv_view_all_discussion;
    TextView eventSchedule_tv, eventlocation_tv, eventDate_tv, eventMonth_tv, eventName_tv,
            people_interested, people_going, ticketsUrl, user_name, follow_count, txt_adapter_post, tv_no_post_discussion;
    ProgressBar view_event_progress;
    ImageButton interested, going, share, add_post;
    LinearLayout ll_interested, ll_going, ll_share, ll_write_discussion;
    TextView interestedText, goingText, shareText;
    NestedScrollView event_details;

    boolean interest_flag = false;
    boolean going_flag = false;
    private boolean isReached = false;
    String eventName, eventImage, totalComments, eventDescription,
            eventStartDate, ticketUrl, eventEndDate, eventLocation, eventCreatedBy, eventCreatedOn, followerId,
            firstName, userPhoto, lastName, userPicture, userPhotoComment,createdBy;
    int totalLikes, eventId, eventType, userInterest, userGoing, position, totalGoing, share_type, eventTypeDisplay, total_discussion;
    int eventID, userid;

    Handler handler = new Handler();

    RecyclerView follwersRecylerview;
    SelectShareInviteAdapter selectShareInviteAdapter;
    List<GetFollowersData> getFollowersDataList = new ArrayList<>();
    List<Integer> friendsList = new ArrayList<>();
    ImageButton selectDone, selectClose;
    Toolbar selectActionBar;
    HashMap<String, Integer> cohostNameIdListd = new HashMap<>();


    // Recyclerview
    RecyclerView eventsCommentsRecylerView;
    PostCommentEventAdapter postCommentEventAdapter;
    List<EventsCommentsUserData> getAllEventDataList = new ArrayList<>();
    EventsCommentsUserData eventsCommentsUserData;

    private static final String TAG = "ViewEvent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event);

        userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));



        eventBackGround = findViewById(R.id.event_background);
        eventSchedule_tv = findViewById(R.id.event_schedule);
        eventlocation_tv = findViewById(R.id.event_location);
        eventDescription_tv = findViewById(R.id.event_description);

        eventDate_tv = findViewById(R.id.view_event_date);
        eventMonth_tv = findViewById(R.id.view_event_month);
        eventName_tv = findViewById(R.id.view_event_name);
        people_interested = findViewById(R.id.people_interested);
        people_going = findViewById(R.id.people_going);
        tv_no_post_discussion = findViewById(R.id.tv_no_post_discussion);
        tv_view_all_discussion = findViewById(R.id.tv_view_all_discussion);
        ticketsUrl = findViewById(R.id.ticketsUrl);
        user_name = findViewById(R.id.user_name);
        follow_count = findViewById(R.id.follow_count);
        edittxt_comment = findViewById(R.id.edittxt_comment);
        txt_adapter_post = findViewById(R.id.txt_adapter_post);

        eventsCommentsRecylerView = findViewById(R.id.event_comments_recyclerview);
        event_details = findViewById(R.id.event_details);
        view_event_progress = findViewById(R.id.view_event_progress);
        ticketSection = findViewById(R.id.ticketSection);

        userImage = findViewById(R.id.user_image);
        userphotoComment = findViewById(R.id.userphoto_comment);

        interested = findViewById(R.id.interest_button);
        going = findViewById(R.id.going_button);
        share = findViewById(R.id.share_button);
        add_post = findViewById(R.id.add_post);

        interestedText = findViewById(R.id.interest_text);
        goingText = findViewById(R.id.going_text);
        shareText = findViewById(R.id.share_text);

        ll_interested = findViewById(R.id.ll_interested);
        ll_going = findViewById(R.id.ll_going);
        ll_share = findViewById(R.id.ll_share);
        ll_write_discussion = findViewById(R.id.ll_write_discussion);
        coLayout = findViewById(R.id.co_layout);

        interested.setOnClickListener(this);
        going.setOnClickListener(this);
        ticketSection.setOnClickListener(this);
        txt_adapter_post.setOnClickListener(this);
        edittxt_comment.setOnClickListener(this);
        share.setOnClickListener(this);
        add_post.setOnClickListener(this);
        userImage.setOnClickListener(this);
        eventBackGround.setOnClickListener(this);
        tv_view_all_discussion.setOnClickListener(this);

        /*Event id from various adapters*/

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            eventID = bundle.getInt("eventId");
            eventTypeDisplay = bundle.getInt("eventType");

        }

        Log.e("eventTypeDisplay", "eventTypeDisplay->" + eventId);

        userPhotoComment = Pref_storage.getDetail(ViewEvent.this, "picture_user_login");


        /*Getting followers api calling*/

        getFollowers(userid);

        /*Getting single event api calling*/

        get_single_EventsData();


        /*Edit text change listener*/

        edittxt_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {

                /*Post text visibilty*/

                if (edittxt_comment.getText().toString().length() != 0) {
                    txt_adapter_post.setVisibility(View.VISIBLE);
                } else {
                    txt_adapter_post.setVisibility(View.GONE);
                }


            }
        });


        if (eventTypeDisplay == 1) {

            ticketSection.setVisibility(View.GONE);

        }

        getAllEventDataList.clear();


//        getUserInterest(eventId);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        /*Collapsing layout*/

        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.event_collapsingtoolbar);
        AppBarLayout appBarLayout = findViewById(R.id.app_bar_layout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(org.apache.commons.text.StringEscapeUtils.unescapeJava(eventName));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });


    }

    @Override
    public void onSwipeRight() {
        finish();
    }

    @Override
    protected void onSwipeLeft() {

    }

    /*Getting followers api calling*/

    private void getFollowers(final int userId) {

        if (Utility.isConnected(ViewEvent.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<GetFollowersOutput> call = apiService.getFollower("get_follower", userId);

                call.enqueue(new Callback<GetFollowersOutput>() {
                    @Override
                    public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                userid = response.body().getData().get(i).getUserid();
                                followerId = response.body().getData().get(i).getFollowerId();
                                firstName = response.body().getData().get(i).getFirstName();
                                lastName = response.body().getData().get(i).getLastName();
                                userPhoto = response.body().getData().get(i).getPicture();

                                if (!followerId.contains(String.valueOf(userid))) {

                                    GetFollowersData getFollowersData = new GetFollowersData(userid, followerId, firstName, lastName, userPhoto);
                                    getFollowersDataList.add(getFollowersData);

                                }


                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        }else {

        Toast.makeText(this, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

    }

    }

    /*Setting event details*/

    private void setEventDetails() {

        int startDate = 0 ;
        int startMonth= 0;
        int startYear = 0;

        int endDate= 0 ;
        int endMonth= 0;
        int endYear = 0;


        try{

             startDate = Integer.parseInt(eventStartDate.substring(8, 10));
             startMonth = Integer.parseInt(eventStartDate.substring(5, 7));
             startYear = Integer.parseInt(eventStartDate.substring(0, 4));

             endDate = Integer.parseInt(eventEndDate.substring(8, 10));
             endMonth = Integer.parseInt(eventEndDate.substring(5, 7));
             endYear = Integer.parseInt(eventEndDate.substring(0, 4));


        }catch (NumberFormatException e){

            Log.e(TAG, "setEventDetails: "+e );

        }


        String startdayOfTheWeek = (String) DateFormat.format("EEEE", getDate(startYear, startMonth - 1, startDate));
        String startday = (String) DateFormat.format("dd", getDate(startYear, startMonth - 1, startDate));
        String startmonthString = (String) DateFormat.format("MMM", getDate(startYear, startMonth - 1, startDate));
        String startmonthNumber = (String) DateFormat.format("MM", getDate(startYear, startMonth - 1, startDate));
        String startyear = (String) DateFormat.format("yyyy", getDate(startYear, startMonth - 1, startDate));


        String enddayOfTheWeek = (String) DateFormat.format("EEEE", getDate(endYear, endMonth - 1, endDate));
        String endday = (String) DateFormat.format("dd", getDate(endYear, endMonth - 1, endDate));
        String endmonthString = (String) DateFormat.format("MMM", getDate(endYear, endMonth - 1, endDate));
        String endmonthNumber = (String) DateFormat.format("MM", getDate(endYear, endMonth - 1, endDate));
        String endyear = (String) DateFormat.format("yyyy", getDate(endYear, endMonth - 1, endDate));

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);
        String inputString1 = formattedDate;
        String inputString2 = eventStartDate.substring(0, 10);
        String daysleft = "";

        try {


            Date date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            long diff = date2.getTime() - date1.getTime();
//            System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            daysleft = String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

            if (Integer.parseInt(daysleft) == 0) {

                daysleft = "Today";

            } else if (Integer.parseInt(daysleft) < 0) {

                daysleft = "Event was over";

            } else {

                int daysCount = Integer.parseInt(daysleft);

                if (daysCount > 1) {

                    daysleft = daysCount + " days left";

                } else {

                    daysleft = daysCount + " day left";

                }

            }


        } catch (ParseException e) {
            e.printStackTrace();
        }


        /*Image loading*/
        GlideApp.with(getApplicationContext())
                .load(eventImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.01f)
                .centerCrop()
                .into(eventBackGround);




//        GlideApp.with(ViewEvent.this)
//                .load(eventImage)
//                .into(eventBackGround);

        eventDate_tv.setText(String.valueOf(startDate));
        eventMonth_tv.setText(startmonthString);
        eventName_tv.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(eventName));
        eventSchedule_tv.setText(startdayOfTheWeek + "," + " " + startmonthString + " " + startday + " " + "at" + " " + get12HrFormat(eventStartDate.substring(11, 16)) +
                " to " + enddayOfTheWeek + "," + " " + endmonthString + " " + endday + " " + "at" + " " + get12HrFormat(eventEndDate.substring(11, 16)) + " - " + daysleft);
        eventlocation_tv.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(eventLocation));
        eventDescription_tv.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(eventDescription));
        people_interested.setText(String.valueOf(totalLikes));
        people_going.setText(String.valueOf(totalGoing));
        ticketsUrl.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(ticketUrl));


        // user part
        user_name.setText(firstName + " " + lastName);
//        follow_count.setText("");


        Utility.picassoImageLoader(userPicture,0, userImage, getApplicationContext());

//        GlideApp.with(ViewEvent.this)
//                .load(userPicture)
//                .placeholder(R.drawable.ic_add_photo)
//                .into(userImage);

        Utility.picassoImageLoader(userPhotoComment,0, userphotoComment,getApplicationContext() );


//        GlideApp.with(ViewEvent.this)
//                .load(userPhotoComment)
//                .placeholder(R.drawable.ic_add_photo)
//                .into(userphotoComment);


        if (ticketUrl.equals("0")) {

            ticketSection.setVisibility(View.GONE);

        } else if (ticketUrl == null) {

            ticketSection.setVisibility(View.GONE);

        } else {

            ticketSection.setVisibility(View.VISIBLE);

        }


        if (userInterest == 1) {

            //Success
            interested.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_star_pressed));
            interest_flag = true;


        } else {


        }


        if (userGoing == 1) {

            //Success
            going.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_going));
            going_flag = true;
            ll_interested.setVisibility(View.GONE);


        } else {


        }

        getEventCommentAll_timeline();

        /*if (frm_timeline == 1) {


        } else if (from_timeline == 2) {

            getEventCommentAll_timeline();
        } else if (from_timeline == 3) {
            getEventCommentAll_timeline();
        } else {

            getEventCommentAll(eventId);

        }*/


        if (share_type == 0) {

            ll_share.setVisibility(View.GONE);

        } else {

            ll_share.setVisibility(View.VISIBLE);

        }

        if (total_discussion == 0) {

            tv_view_all_discussion.setVisibility(View.GONE);
            tv_no_post_discussion.setVisibility(View.VISIBLE);

        } else {

            tv_view_all_discussion.setVisibility(View.VISIBLE);
            tv_no_post_discussion.setVisibility(View.GONE);
        }

        coLayout.setVisibility(View.VISIBLE);
        view_event_progress.setVisibility(View.GONE);
        event_details.setVisibility(View.VISIBLE);

    }


    public String get12HrFormat(String time24Hr) {

        String time = "";

        try {


            String _24HourTime = time24Hr;
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);

            System.out.println(_24HourDt);
            System.out.println(_12HourSDF.format(_24HourDt));

            time = _12HourSDF.format(_24HourDt);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return time;



    }

    public static Date getDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

               /* Intent intent = new Intent(ViewEvent.this, Events.class);
                startActivity(intent);
                finish();*/
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getAllEventsData(int event_type) {


        if (Utility.isConnected(ViewEvent.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

                Call<GetAllEventsOutput> call = apiService.getAllEvents("get_events_type_all", userid, event_type);

                call.enqueue(new Callback<GetAllEventsOutput>() {
                    @Override
                    public void onResponse(Call<GetAllEventsOutput> call, Response<GetAllEventsOutput> response) {

                        if (response.body() != null) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                //Success

                                Log.e("responseStatus", "responseSize->" + response.body().getData().size());
/*

                                eventId = response.body().getData().get(position).getEventId();
                                eventName = response.body().getData().get(position).getEventName();
                                eventDescription = response.body().getData().get(position).getEventDescription();
                                totalLikes = response.body().getData().get(position).getTotalLikes();
                                totalGoing = Integer.parseInt(response.body().getData().get(position).getTotalGoing());
                                totalComments = String.valueOf(response.body().getData().get(position).getTotalComments());
                                eventImage = response.body().getData().get(position).getEventImage();
                                eventType = response.body().getData().get(position).getEventType();
                                share_type = response.body().getData().get(position).getShareType();
                                ticketUrl = response.body().getData().get(position).getTicketUrl();
                                eventLocation = response.body().getData().get(position).getLocation();
                                eventStartDate = response.body().getData().get(position).getStartDate();
                                eventEndDate = response.body().getData().get(position).getEndDate();
                                eventCreatedBy = response.body().getData().get(position).getCreatedBy();
                                eventCreatedOn = response.body().getData().get(position).getCreatedOn();
                                total_discussion = response.body().getData().get(position).getTotal_discussion();
                                firstName = response.body().getData().get(position).getFirstName();
                                lastName = response.body().getData().get(position).getLastName();
                                userPicture = response.body().getData().get(position).getPicture();
                                userInterest = response.body().getData().get(position).getEvtLikes();
                                userGoing = response.body().getData().get(position).getGngLikes();
*/



                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<GetAllEventsOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {
            Snackbar snackbar = Snackbar.make(coLayout, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();

        }


    }

    /*Getting single event api calling*/

    private void get_single_EventsData() {


        if (Utility.isConnected(ViewEvent.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

                Call<GetAllEventsOutput> call = apiService.get_events_single("get_events_single", userid, eventID);

                call.enqueue(new Callback<GetAllEventsOutput>() {
                    @Override
                    public void onResponse(Call<GetAllEventsOutput> call, Response<GetAllEventsOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());

                            eventTypeDisplay = response.body().getData().get(0).getEventType();
                            eventId = response.body().getData().get(0).getEventId();
                            eventName = response.body().getData().get(0).getEventName();
                            eventDescription = response.body().getData().get(0).getEventDescription();
                            totalLikes = response.body().getData().get(0).getTotalLikes();
                            totalGoing = Integer.parseInt(response.body().getData().get(0).getTotalGoing());
                            totalComments = String.valueOf(response.body().getData().get(0).getTotalComments());
                            totalComments = String.valueOf(response.body().getData().get(0).getTotalComments());
                            eventImage = response.body().getData().get(0).getEventImage();
                            eventType = response.body().getData().get(0).getEventType();
                            share_type = response.body().getData().get(0).getShareType();
                            ticketUrl = response.body().getData().get(0).getTicketUrl();
                            eventLocation = response.body().getData().get(0).getLocation();
                            eventStartDate = response.body().getData().get(0).getStartDate();
                            eventEndDate = response.body().getData().get(0).getEndDate();
                            eventCreatedBy = response.body().getData().get(0).getCreatedBy();
                            eventCreatedOn = response.body().getData().get(0).getCreatedOn();
                            firstName = response.body().getData().get(0).getFirstName();
                            lastName = response.body().getData().get(0).getLastName();
                            userPicture = response.body().getData().get(0).getPicture();
                            createdBy = response.body().getData().get(0).getCreatedBy();
                            userInterest = response.body().getData().get(0).getEvtLikes();
                            userGoing = response.body().getData().get(0).getGngLikes();
                            total_discussion = response.body().getData().get(0).getTotal_discussion();


                            event_details.setVisibility(View.VISIBLE);

                            /*Setting event details*/

                            setEventDetails();

                        }
                    }

                    @Override
                    public void onFailure(Call<GetAllEventsOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {
            Snackbar snackbar = Snackbar.make(coLayout, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();

        }


    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            case R.id.edittxt_comment:

                Intent intent1 = new Intent(ViewEvent.this, CreateEventDiscussion.class);
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", eventId);
                bundle.putInt("launchType", 1);
                bundle.putInt("discussId", 0);
                bundle.putString("eventName", eventName);
                intent1.putExtras(bundle);
                startActivity(intent1);

                break;

            case R.id.add_post:

                Intent intent3 = new Intent(ViewEvent.this, CreateEventDiscussion.class);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("eventId", eventId);
                bundle1.putInt("launchType", 1);
                bundle1.putInt("discussId", 0);
                bundle1.putString("eventName", eventName);
                intent3.putExtras(bundle1);
                startActivity(intent3);

                break;

            case R.id.tv_view_all_discussion:

                Intent intent2 = new Intent(ViewEvent.this, ViewEventDiscussion.class);
                Bundle bundle2 = new Bundle();
                bundle2.putInt("eventId", eventId);
                bundle2.putString("eventName", eventName);
                intent2.putExtras(bundle2);
                startActivity(intent2);

                break;

            case R.id.interest_button:


                if (interest_flag) {

                    if (Utility.isConnected(getApplicationContext())) {


                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

                        Call<CommonOutputPojo> call = apiService.createEventsLikes("create_events_likes", eventId, 0, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("nodata")) {

                                    //Success
                                    interested.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_star));
                                    interest_flag = false;
                                    people_interested.setText(String.valueOf(--totalLikes));


                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }} else {

                        Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

                    }



                } else if (!interest_flag) {

                    if (Utility.isConnected(getApplicationContext())) {

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

                        Call<CommonOutputPojo> call = apiService.createEventsLikes("create_events_likes", eventId, 1, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                String responseStatus = response.body().getResponseMessage();


                                if (responseStatus.equals("success")) {

                                    //Success

                                    interested.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_star_pressed));
                                    interest_flag = true;

                                    people_interested.setText(String.valueOf(++totalLikes));
                                    Toast.makeText(ViewEvent.this, "Your interest has been saved", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                } else {

                    Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

                }


                }

                break;

            case R.id.going_button:


                if (going_flag) {


                    View view = getLayoutInflater().inflate(R.layout.alert_create_events, null);
                    dialog = new BottomSheetDialog(ViewEvent.this);
                    dialog.setContentView(view);

                    LinearLayout interested = dialog.findViewById(R.id.ll_private_event);
                    LinearLayout not_going = dialog.findViewById(R.id.ll_public_event);


                    interested.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utility.isConnected(getApplicationContext())) {

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

                                Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", eventId, 0, userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("nodata")) {

                                            //Success
                                            going.setImageDrawable(ContextCompat.getDrawable(ViewEvent.this, R.drawable.ic_not_going));
                                            going_flag = false;
                                            people_going.setText(String.valueOf(--totalGoing));
                                            ll_interested.setVisibility(View.VISIBLE);
                                            dialog.dismiss();


                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                    }
                                });

                            } catch (Exception e) {

                                e.printStackTrace();

                            }

                            } else {

                                Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

                            }

                            if (interest_flag) {

                                ll_interested.setVisibility(View.VISIBLE);
                                dialog.dismiss();

                            } else {
                                /*Create interested api call*/
                                createInterested();
                                ll_interested.setVisibility(View.VISIBLE);
                                dialog.dismiss();
                            }


                        }
                    });

                    not_going.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                                if(Utility.isConnected(getApplicationContext())){
                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

                                Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", eventId, 0, userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("nodata")) {

                                            //Success
                                            going.setImageDrawable(ContextCompat.getDrawable(ViewEvent.this, R.drawable.ic_not_going));
                                            going_flag = false;
                                            people_going.setText(String.valueOf(--totalGoing));
                                            ll_interested.setVisibility(View.VISIBLE);
                                            dialog.dismiss();


                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                    }
                                });

                            } catch (Exception e) {

                                e.printStackTrace();

                            }  } else {

                                    Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

                                }


                        }
                    });


                    dialog.show();


                } else if (!going_flag) {

                    if(Utility.isConnected(getApplicationContext())){
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

                        Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", eventId, 1, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                String responseStatus = response.body().getResponseMessage();


                                if (responseStatus.equals("success")) {

                                    //Success

                                    going.setImageDrawable(ContextCompat.getDrawable(ViewEvent.this, R.drawable.ic_going));
                                    going_flag = true;
                                    people_going.setText(String.valueOf(++totalGoing));
                                    ll_interested.setVisibility(View.GONE);
                                    Toast.makeText(ViewEvent.this, "You are going to this event", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }  } else {

                        Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

                    }


                }

                break;

            case R.id.ticketSection:

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(ticketUrl));
                startActivity(i);

                break;


            case R.id.txt_adapter_post:

                createEventComment(eventId, 0);

                break;


            case R.id.share_button:


                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ViewEvent.this);
                LayoutInflater inflater1 = LayoutInflater.from(ViewEvent.this);
                @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.activity_select_followers, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setTitle("");

                follwersRecylerview = dialogView.findViewById(R.id.user_followers_recylerview);
                selectActionBar = dialogView.findViewById(R.id.action_bar_select_follower);
                selectClose = selectActionBar.findViewById(R.id.close);
                selectDone = selectActionBar.findViewById(R.id.tv_done);

                SearchView search_followers = dialogView.findViewById(R.id.search_followers);

                search_followers.setQueryHint("Search");

                search_followers.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted
                        selectShareInviteAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        // filter recycler view when text is changed
                        selectShareInviteAdapter.getFilter().filter(query);
                        return false;
                    }
                });

                /*Setting adapter*/
                selectShareInviteAdapter = new SelectShareInviteAdapter(ViewEvent.this, getFollowersDataList, friendsList);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ViewEvent.this);
                follwersRecylerview.setLayoutManager(mLayoutManager);
                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                follwersRecylerview.setAdapter(selectShareInviteAdapter);
                follwersRecylerview.hasFixedSize();
                selectShareInviteAdapter.notifyDataSetChanged();

                final AlertDialog dialog = dialogBuilder.create();
                dialog.show();

                selectClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();

                    }
                });

                selectDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (friendsList.size() == 0) {


                        } else {


                            createEventShare();

                        }


                        dialog.dismiss();
                    }
                });


                break;


            case R.id.user_image:


                if (createdBy.equalsIgnoreCase(Pref_storage.getDetail(ViewEvent.this, "userId"))) {

                    startActivity(new Intent(ViewEvent.this, Profile.class));

                } else {

                    Intent intent = new Intent(ViewEvent.this, User_profile_Activity.class);
                    intent.putExtra("created_by", createdBy);
                    startActivity(intent);

                }

                break;


            case R.id.event_background:

                Intent intent4 = new Intent(ViewEvent.this, ViewImage.class);
                intent4.putExtra("imageurl", eventImage);
                startActivity(intent4);

                break;


        }

    }

    private void createEventShare() {
        if(Utility.isConnected(getApplicationContext())){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

            Call<CommonOutputPojo> call = apiService.createEventShare("create_events_share", eventId, userid, friendsList);

            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    String responseStatus = response.body().getResponseMessage();


                    if (responseStatus.equals("success")) {

                        //Success
                        Toast.makeText(ViewEvent.this, "Event shared successfully", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }  } else {

            Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

        }


    }

    /*Interface to invite people*/
    public void getSharedInvitedPeople(View view, GetFollowersData getFollowersData,int adapterPosition) {

        if (((CheckBox) view).isChecked()) {

            if (friendsList.contains(Integer.valueOf(getFollowersDataList.get(adapterPosition).getFollowerId()))) {

                return;

            } else {

                friendsList.add(Integer.valueOf(getFollowersDataList.get(adapterPosition).getFollowerId()));

            }


        } else {

            friendsList.remove(Integer.valueOf(getFollowersDataList.get(adapterPosition).getFollowerId()));

        }

    }


    private void getUserInterest(int event_id) {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

            Call<GetEventLikesUserPojo> call = apiService.getEventsLikesUser("get_events_likes_user", userid, event_id);

            call.enqueue(new Callback<GetEventLikesUserPojo>() {
                @Override
                public void onResponse(Call<GetEventLikesUserPojo> call, Response<GetEventLikesUserPojo> response) {

                    String responseStatus = response.body().getResponseMessage();


                    if (responseStatus.equals("success")) {

                        //Success
                        interested_fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_star_pressed));
                        interest_flag = true;


                    } else if (responseStatus.equals("nodata")) {


                    }
                }

                @Override
                public void onFailure(Call<GetEventLikesUserPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }


    }

    /*Create interested api call*/

    private void createInterested() {
        if(Utility.isConnected(getApplicationContext())){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

            Call<CommonOutputPojo> call = apiService.createEventsLikes("create_events_likes", eventId, 1, userid);

            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    String responseStatus = response.body().getResponseMessage();


                    if (responseStatus.equals("success")) {

                        //Success

                        interested.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_star_pressed));
                        interest_flag = true;

                        people_interested.setText(String.valueOf(++totalLikes));
                        Toast.makeText(ViewEvent.this, "Your interest has been saved", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }} else {

            Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

        }

    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            return (mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting());
        } else
            return false;
    }

    private void createEventComment(int event_id, int createType) {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

//            RequestBody eventComment = RequestBody.create(MediaType.parse("text/plain"),edittxtComment.getText().toString());

            Call<CommonOutputPojo> call = apiService.createEventComment("create_edit_events_comments", createType, event_id, edittxt_comment.getText().toString(), userid);

            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    String responseStatus = response.body().getResponseMessage();

                    Log.e("createEventComment", "responseStatus->" + responseStatus);


                    if (responseStatus.equals("success")) {

                        //Success
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);

                    } else if (responseStatus.equals("nodata")) {


                    }
                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }


    }

    private void getEventCommentAll(int eventId) {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

            RequestBody eventComment = RequestBody.create(MediaType.parse("text/plain"), edittxt_comment.getText().toString());

            Call<GetEventsCommentsPojo> call = apiService.getAllEventComments("get_events_comments_all", eventId, userid);

            call.enqueue(new Callback<GetEventsCommentsPojo>() {
                @Override
                public void onResponse(Call<GetEventsCommentsPojo> call, Response<GetEventsCommentsPojo> response) {

                    String responseStatus = response.body().getResponseMessage();

                    Log.e("getAllEventComment", "responseStatus->" + responseStatus);


                    if (responseStatus.equals("success")) {

                        //Success

                        for (int i = 0; i < response.body().getData().size(); i++) {


                            String eventId = response.body().getData().get(i).getEventId();
                            String totalComments = response.body().getData().get(i).getTotalComments();
                            String cmtEventId = response.body().getData().get(i).getCmmtEvtId();
                            String evtComment = response.body().getData().get(i).getEvtComments();
                            String createdBy = response.body().getData().get(i).getCreatedBy();
                            String createdOn = response.body().getData().get(i).getCreatedOn();
                            String firstName = response.body().getData().get(i).getFirstName();
                            String LastName = response.body().getData().get(i).getLastName();
                            String picture = response.body().getData().get(i).getPicture();

                            EventsCommentsUserData eventsCommentsUserData = new EventsCommentsUserData(eventId, totalComments, cmtEventId, evtComment, createdBy, createdOn
                                    , firstName, LastName, picture);

                            getAllEventDataList.add(eventsCommentsUserData);

                           /* postCommentEventAdapter = new PostCommentEventAdapter(ViewEvent.this, getAllEventDataList, position);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ViewEvent.this);
                            eventsCommentsRecylerView.setLayoutManager(mLayoutManager);
                            eventsCommentsRecylerView.setLayoutManager(mLayoutManager);
                            eventsCommentsRecylerView.setItemAnimator(new DefaultItemAnimator());
                            eventsCommentsRecylerView.setAdapter(postCommentEventAdapter);
                            eventsCommentsRecylerView.hasFixedSize();
                            postCommentEventAdapter.notifyDataSetChanged();*/


                        }


                    } else if (responseStatus.equals("nodata")) {


                    }
                }

                @Override
                public void onFailure(Call<GetEventsCommentsPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }


    }


    private void getEventCommentAll_timeline() {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {


            int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this, "userId"));

            RequestBody eventComment = RequestBody.create(MediaType.parse("text/plain"), edittxt_comment.getText().toString());

            Call<GetEventsCommentsPojo> call = apiService.getAllEventComments("get_events_comments_all", eventID, userid);

            call.enqueue(new Callback<GetEventsCommentsPojo>() {
                @Override
                public void onResponse(Call<GetEventsCommentsPojo> call, Response<GetEventsCommentsPojo> response) {

                    String responseStatus = response.body().getResponseMessage();

                    Log.e("getAllEventComment", "responseStatus->" + responseStatus);


                    if (responseStatus.equals("success")) {

                        //Success

                        for (int i = 0; i < response.body().getData().size(); i++) {


                            String eventId = response.body().getData().get(i).getEventId();
                            String totalComments = response.body().getData().get(i).getTotalComments();
                            String cmtEventId = response.body().getData().get(i).getCmmtEvtId();
                            String evtComment = response.body().getData().get(i).getEvtComments();
                            String createdBy = response.body().getData().get(i).getCreatedBy();
                            String createdOn = response.body().getData().get(i).getCreatedOn();
                            String firstName = response.body().getData().get(i).getFirstName();
                            String LastName = response.body().getData().get(i).getLastName();
                            String picture = response.body().getData().get(i).getPicture();

                            EventsCommentsUserData eventsCommentsUserData = new EventsCommentsUserData(eventId, totalComments, cmtEventId, evtComment, createdBy, createdOn
                                    , firstName, LastName, picture);

                            getAllEventDataList.add(eventsCommentsUserData);

                            /*postCommentEventAdapter = new PostCommentEventAdapter(ViewEvent.this, getAllEventDataList, position);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ViewEvent.this);
                            eventsCommentsRecylerView.setLayoutManager(mLayoutManager);
                            eventsCommentsRecylerView.setItemAnimator(new DefaultItemAnimator());
                            eventsCommentsRecylerView.setAdapter(postCommentEventAdapter);
                            eventsCommentsRecylerView.hasFixedSize();
                            postCommentEventAdapter.notifyDataSetChanged();*/


                        }


                    } else if (responseStatus.equals("nodata")) {


                    }
                }

                @Override
                public void onFailure(Call<GetEventsCommentsPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }


    }


    @Override
    protected void onResume() {
        super.onResume();


        /*Getting single event api calling*/

        get_single_EventsData();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

      /*  Intent intent = new Intent(ViewEvent.this, Events.class);
        startActivity(intent);
        finish();*/
    }


}
