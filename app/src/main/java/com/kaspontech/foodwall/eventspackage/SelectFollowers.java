package com.kaspontech.foodwall.eventspackage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.events.SelectCohostAdapter;
import com.kaspontech.foodwall.modelclasses.EventShare.EventSharePojo;
import com.kaspontech.foodwall.modelclasses.EventShare.FollowersId;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectFollowers extends AppCompatActivity implements View.OnClickListener {


    // Widgets
    TextView done, close;
    Toolbar actionBar;


    // Vars
    int eventId,userid;
    String followerId,firstName,lastName,userPhoto;

    // Recyclerview
    RecyclerView follwersRecylerview;
    SelectCohostAdapter selectCohostAdapter;
    List<GetFollowersData> getFollowersDataList = new ArrayList<>();
    GetFollowersData getFollowersData;
    List<EventSharePojo> eventSharePojoList = new ArrayList<>();
    List<FollowersId> followersIdList = new ArrayList<>();
    ArrayList<Integer> friendsList = new ArrayList<>();
    EventSharePojo eventSharePojo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_followers);

        actionBar = (Toolbar) findViewById(R.id.action_bar_select_follower);
        close = (TextView) actionBar.findViewById(R.id.close);
        done = (TextView) actionBar.findViewById(R.id.tv_done);

        close.setOnClickListener(this);
        done.setOnClickListener(this);

        userid = Integer.parseInt(Pref_storage.getDetail(SelectFollowers.this,"userId"));
        follwersRecylerview = (RecyclerView)findViewById(R.id.user_followers_recylerview);

        /*Getting Event id from multiple actvities / adapter*/
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            eventId = bundle.getInt("eventId");

        }


        /*Get followers API calling*/

        getFollowers(userid);

    }



    /*Get followers API calling*/

    private void getFollowers(int userId) {

        if (Utility.isConnected(SelectFollowers.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<GetFollowersOutput> call = apiService.getFollower("get_follower",userId );

                call.enqueue(new Callback<GetFollowersOutput>() {
                    @Override
                    public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for(int i = 0 ; i < response.body().getData().size() ; i++){

                                userid = response.body().getData().get(i).getUserid();
                                followerId = response.body().getData().get(i).getFollowerId();
                                firstName = response.body().getData().get(i).getFirstName();
                                lastName = response.body().getData().get(i).getLastName();
                                userPhoto = response.body().getData().get(i).getPicture();

                                GetFollowersData getFollowersData = new GetFollowersData(userid,followerId,firstName,lastName,userPhoto);
                                getFollowersDataList.add(getFollowersData);

                               /* selectCohostAdapter = new SelectCohostAdapter(SelectFollowers.this, getFollowersDataList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SelectFollowers.this);
                                follwersRecylerview.setLayoutManager(mLayoutManager);
                                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                                follwersRecylerview.setAdapter(selectCohostAdapter);
                                follwersRecylerview.hasFixedSize();
                                selectCohostAdapter.notifyDataSetChanged();*/

                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

    }


    public void getSelectedFollowers(View view, int position){

        if(((CheckBox)view).isChecked()){

//            Toast.makeText(this, "Selected\t"+getFollowersDataList.get(position).getFirstName(), Toast.LENGTH_SHORT).show();

            friendsList.add(Integer.valueOf(followerId));
            FollowersId followersId = new FollowersId(getFollowersDataList.get(position).getFollowerId());
            followersIdList.add(followersId);
            eventSharePojo = new EventSharePojo(eventId,userid,followersIdList);
            eventSharePojoList.add(eventSharePojo);

        }

    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id){

            case R.id.close:

                /*Intent intent = new Intent(SelectFollowers.this,CreateEvent.class);
                startActivity(intent);*/
                finish();

                break;

            case R.id.tv_done:


                if (Utility.isConnected(SelectFollowers.this)) {

                    /*Intent intent1 = new Intent(SelectFollowers.this,CreateEvent.class);
                    intent1.putExtra("friends", cohostList);
                    startActivity(intent1);*/
                    finish();

                    /*ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        Call<GetFollowersOutput> call = apiService.getFollower("get_follower",userId );

                        call.enqueue(new Callback<GetFollowersOutput>() {
                            @Override
                            public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {

                                    //Success

                                    Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                                    for(int i = 0 ; i < response.body().getData().size() ; i++){

                                        userid = response.body().getData().get(i).getUserid();
                                        followerId = response.body().getData().get(i).getFollowerId();
                                        firstName = response.body().getData().get(i).getFirstName();
                                        lastName = response.body().getData().get(i).getLastName();
                                        userPhoto = response.body().getData().get(i).getPicture();

                                        GetFollowersData getFollowersData = new GetFollowersData(userid,followerId,firstName,lastName,userPhoto);
                                        getFollowersDataList.add(getFollowersData);

                                        selectCohostAdapter = new SelectCohostAdapter(SelectFollowers.this, getFollowersDataList);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SelectFollowers.this);
                                        follwersRecylerview.setLayoutManager(mLayoutManager);
                                        follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                                        follwersRecylerview.setAdapter(selectCohostAdapter);
                                        follwersRecylerview.hasFixedSize();
                                        selectCohostAdapter.notifyDataSetChanged();

                                    }




                                }
                            }

                            @Override
                            public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }
*/
                } else {

                    Toast.makeText(this, "No internet Connection", Toast.LENGTH_SHORT).show();

                }





                break;
        }
    }




}
