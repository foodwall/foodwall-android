package com.kaspontech.foodwall.eventspackage;

import android.gesture.Prediction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlaceSearchDetails {

    @SerializedName("predictions")
    @Expose
    private List<PrediciationDetails> predictions = null;
    @SerializedName("status")
    @Expose
    private String status;

    /**
     * No args constructor for use in serialization
     *
     */
    public PlaceSearchDetails() {
    }

    /**
     *
     * @param predictions
     * @param status
     */
    public PlaceSearchDetails(List<PrediciationDetails> predictions, String status) {
        super();
        this.predictions = predictions;
        this.status = status;
    }

    public List<PrediciationDetails> getPredictions() {
        return predictions;
    }

    public void setPredictions(List<PrediciationDetails> predictions) {
        this.predictions = predictions;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
