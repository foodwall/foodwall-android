package com.kaspontech.foodwall.eventspackage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.REST_API.API_interface_restaurant;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.RecyclerViewAdapter;
import com.kaspontech.foodwall.adapters.events.SelectCohostAdapter;
import com.kaspontech.foodwall.adapters.events.SelectInvitePeopleAdapter;
import com.kaspontech.foodwall.foodFeedsPackage.CreateHostoricalMapRestname;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.modelclasses.EventShare.GetEventShareOutput;
import com.kaspontech.foodwall.modelclasses.EventsPojo.CreateEventOutput;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventsCohost.GetEventCohostOutput;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetAllEventData;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetAllEventsOutput;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.REST_API.ProgressRequestBody;
//
import com.kaspontech.foodwall.modelclasses.HistoricalMap.Get_HotelStaticLocation_Response;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Get_hotel_photo_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Getall_restaurant_details_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.OpeningHours_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Restaurant_output_pojo;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.RangeTimePickerDialog;
import com.kaspontech.foodwall.utills.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import mabbas007.tagsedittext.TagsEditText;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreateEvent extends AppCompatActivity implements
        View.OnClickListener,
        TagsEditText.TagsEditListener,
        ProgressRequestBody.UploadCallbacks, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    /**
     * Application TAG
     **/
    private static final String TAG = "CreateEvent";

    /**
     * ActionBar
     **/

    Toolbar actionBar;

    /**
     * Done button
     **/
    ImageButton selectDone;

    /**
     * Close button
     **/
    ImageButton selectClose;

    /**
     * Select action bar
     **/
    Toolbar selectActionBar;

    /**
     * Event background imageview
     **/

    ImageView eventBackground;

    /**
     * Switch guest invite
     **/

    Switch switchGuestInvite;
    /**
     * Close button
     **/
    ImageButton close;
    /**
     * Add photo button
     **/
    ImageButton addPhoto;

    /**
     * Change photo button
     **/
    ImageButton changePhoto;
    /**
     * Event types text view
     **/
    TextView eventsType;
    /**
     * Event create text view
     **/
    TextView createEvent;

    /**
     * Add co-host text view
     **/
    TextView addCoHost;
    /**
     * Invite friends text view
     **/
    TextView inviteFriends;
    /**
     * Add grouo text view
     **/
    TextView addGroup;
    /**
     * Invite friends list display text view
     **/
    TextView inviteFriendsListDisplay;

    /**
     * Guest invite relative layout
     **/
    RelativeLayout guestInvite;
    /**
     * Admin approve relative layout
     **/
    RelativeLayout adminApprove;
    /**
     * Create event layout
     **/
    LinearLayout llCreateEvent;

    /**
     * Event title input
     **/
    EmojiconEditText eventTitleInput;
    /**
     * Event description input
     **/
    EmojiconEditText eventDescriptionInput;
    /**
     * Edit text's
     **/
    EditText startDateInput;
    EditText endDateInput;
    EditText startTimeInput;
    EditText friendsListDisplay;
    EditText endTimeInput;
    //    EditText eventLocationInput;
    EditText eventTicketUrlInput;

    AutoCompleteTextView eventLocationInput;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final int GOOGLE_API_CLIENT_ID = 0;


    /**
     * Event host's lists
     **/
    List<Integer> cohostList = new ArrayList<>();

    List<Integer> friendsList = new ArrayList<>();

    HashMap<Integer, String> cohostNameIdListd = new HashMap<>();

    List<GetFollowersData> getFollowersDataList = new ArrayList<>();


    List<GetAllEventData> getMyEventList = new ArrayList<>();

    /**
     * Event data pojo
     **/

    GetAllEventData getAllEventData;


    /**
     * Followers recyclerview
     **/

    RecyclerView follwersRecylerview;

    /**
     * Search Followers
     **/

    SearchView searchFollowers;

    /**
     * Empty list layout
     **/

    RelativeLayout emptyList;


    /**
     * Select co host adapter
     **/

    SelectCohostAdapter selectCohostAdapter;

    /**
     * Select invite people adapter
     **/

    SelectInvitePeopleAdapter selectInvitePeopleAdapter;
    /**
     * int Variables
     **/

    int eventId;
    int userid;
    int startTime;
    int endTime;

    private int eventCode;
    private int cohostStatus;

    private int launchMode;
    private int eventEditPosition;

    private int requestCamera = 0;
    private int selectFile = 1;

    private static String eventTitle;
    private static String startDate;
    private static String endDate;
    private static String eventLocation;
    private static String eventDescription;
    private static String eventTicketUrl;
    private static String eventImage;
    private  double latitude;
    private  double longitude;

    private static final int PICK_FROM_GALLERY = 1;

    private static final int REQUEST_PERMISSION_SETTING = 5;

    /**
     * Place search API interface
     */

    API_interface_restaurant apiService;

    /**
     * String Variables
     **/
    String followerId;
    String firstName;
    String lastName;
    String userPhoto;
    String picturePath;
    String imageExists = "";
    String eventStartTime;
    String eventEndTime;


    /**
     * GPS Tracker
     **/

    GPSTracker gpsTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        /*GPS initialization*/
        gpsTracker = new GPSTracker(this);

        /*Getting user id fraom shared preference*/

        userid = Integer.parseInt(Pref_storage.getDetail(CreateEvent.this, "userId"));

        /*Textview initialization*/

        addCoHost = findViewById(R.id.add_co_host);
        addGroup = findViewById(R.id.add_event_group);
        inviteFriends = findViewById(R.id.invite_friends);

        inviteFriendsListDisplay = findViewById(R.id.invite_friendsListDisplay);

        /*Relative layout initialization*/
        guestInvite = findViewById(R.id.rl_guest_invite);
        llCreateEvent = findViewById(R.id.ll_create_event);
        adminApprove = findViewById(R.id.rl_approve_by_admin);

        /*Action bar initialization*/
        actionBar = findViewById(R.id.action_bar_create_events);

        eventsType = actionBar.findViewById(R.id.event_type_title);
        createEvent = actionBar.findViewById(R.id.tv_create_event);
        /*ImageButton initialization*/
        addPhoto = findViewById(R.id.add_photo);
        close = actionBar.findViewById(R.id.close);
        changePhoto = findViewById(R.id.change_photo);

        /*Image view initialization*/

        eventBackground = findViewById(R.id.event_background);

        /*Edit text view initialization*/

        eventTitleInput = findViewById(R.id.event_title);
        eventDescriptionInput = findViewById(R.id.event_details);

        friendsListDisplay = findViewById(R.id.friendsListDisplay);
        endDateInput = findViewById(R.id.event_end_date);
        endTimeInput = findViewById(R.id.event_end_time);

        startDateInput = findViewById(R.id.event_from_date);
        startTimeInput = findViewById(R.id.event_from_time);

        mGoogleApiClient = new GoogleApiClient.Builder(CreateEvent.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();


        eventLocationInput = findViewById(R.id.event_location);
        eventLocationInput.setThreshold(3);


        if(gpsTracker.isGPSEnabled) {
            // nearByRestaurants(restaurantName);
            latitude=gpsTracker.getLatitude();
            longitude=gpsTracker.getLongitude();

        }else
        {
            get_hotel_static_location();

        }

        LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(new LatLng(latitude, longitude), new LatLng(latitude, longitude));
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1, BOUNDS_MOUNTAIN_VIEW, null);
        eventLocationInput.setAdapter(mPlaceArrayAdapter);


        eventTicketUrlInput = findViewById(R.id.event_ticket_url);
        /*Switch guest initialization*/
        switchGuestInvite = findViewById(R.id.switch_guest_invite);

        /*Getting input variable from various adpaters*/
        eventsType.setText(getIntent().getStringExtra("eventName"));
        eventCode = getIntent().getIntExtra("eventCode", -1);
        launchMode = getIntent().getIntExtra("LaunchMode", -1);
        eventEditPosition = getIntent().getIntExtra("eventEditPosition", -1);


        mandatoryFieldSetting(getString(R.string.event_title), eventTitleInput);
        mandatoryFieldSetting(getString(R.string.start_date), startDateInput);
        mandatoryFieldSetting(getString(R.string.start_time), startTimeInput);
        mandatoryFieldSetting(getString(R.string.end_date), endDateInput);
        mandatoryFieldSetting(getString(R.string.end_time), endTimeInput);
        mandatoryFieldSetting(getString(R.string.write_somethings_about_this_event), eventDescriptionInput);
        mandatoryFieldSetting(getString(R.string.location), eventLocationInput);



        /*Launch mode*/
        if (launchMode == 2) {

            switch (eventCode) {

                case 1:

                    eventId = getIntent().getIntExtra("eventId", -1);
                    eventTitle = getIntent().getStringExtra("eventTitle");
                    startDate = getIntent().getStringExtra("eventStartDate");
                    endDate = getIntent().getStringExtra("eventEndDate");
                    eventLocation = getIntent().getStringExtra("eventLocation");
                    eventDescription = getIntent().getStringExtra("eventDescription");
                    eventImage = getIntent().getStringExtra("eventImage");

                    /* Event image */

                    /*Image loading*/

                    GlideApp.with(getApplicationContext())
                            .load(eventImage)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .thumbnail(0.01f)
                            .centerCrop()
                            .into(eventBackground);

                    changePhoto.setVisibility(View.VISIBLE);

                    /*Event details setting*/


                    eventTitleInput.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(eventTitle));
                    startDateInput.setText(startDate.substring(0, 10));
                    endDateInput.setText(endDate.substring(0, 10));

                    startTimeInput.setText(get12HrFormat(startDate.substring(11, 16)));
                    endTimeInput.setText(get12HrFormat(endDate.substring(11, 16)));

                    eventLocationInput.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(eventLocation));
                    eventDescriptionInput.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(eventDescription));
                    /*Calling api for event co host */
                    getEventCoHost(eventId);
//                    getEventGuest(eventId);

                    Log.e("eventStartDate", "eventStartDate" + startDate);


                    break;

                case 2:

                    eventId = getIntent().getIntExtra("eventId", -1);
                    eventTitle = getIntent().getStringExtra("eventTitle");
                    startDate = getIntent().getStringExtra("eventStartDate");
                    endDate = getIntent().getStringExtra("eventEndDate");
                    startTimeInput.setText(get12HrFormat(startDate.substring(11, 16)));
                    endTimeInput.setText(get12HrFormat(endDate.substring(11, 16)));
                    eventLocation = getIntent().getStringExtra("eventLocation");
                    eventDescription = getIntent().getStringExtra("eventDescription");
                    eventTicketUrl = getIntent().getStringExtra("eventTicketUrl");
                    eventImage = getIntent().getStringExtra("eventImage");

                    Log.e("eventStartDate", "eventStartDate" + startDate);

                    /* Event image */

                    GlideApp.with(getApplicationContext())
                            .load(eventImage)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .thumbnail(0.01f)
                            .centerCrop()
                            .into(eventBackground);


                    changePhoto.setVisibility(View.VISIBLE);

                    /*Event details setting*/

                    eventTitleInput.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(eventTitle));
                    startDateInput.setText(startDate.substring(0, 10));
                    endDateInput.setText(endDate.substring(0, 10));
                    eventLocationInput.setText(eventLocation);
                    eventDescriptionInput.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(eventDescription));

                    if (eventTicketUrl.equals("0")) {

                        eventTicketUrlInput.setText("");

                    } else {

                        eventTicketUrlInput.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(eventTicketUrl));
                    }

                    /*Calling api for event co host */

                    getEventCoHost(eventId);


                    break;
            }

        }

        /*On Click listener*/

        close.setOnClickListener(this);
        startDateInput.setOnClickListener(this);
        startTimeInput.setOnClickListener(this);
        endDateInput.setOnClickListener(this);
        endTimeInput.setOnClickListener(this);
        addPhoto.setOnClickListener(this);
        changePhoto.setOnClickListener(this);
        createEvent.setOnClickListener(this);
        addCoHost.setOnClickListener(this);
        inviteFriends.setOnClickListener(this);

        Log.e("EditingEvent", "launchMode->" + launchMode);
        Log.e("EditingEvent", "eventEditPosition->" + eventEditPosition);


        /*Getting latitude and longitude*/

        // latitude = gpsTracker.getLatitude();

        //  longitude = gpsTracker.getLongitude();





        /*Getting followers api calling*/

        getFollowers(userid);


    }// On Create ends here

    private void get_hotel_static_location() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<Get_HotelStaticLocation_Response> call = apiInterface.get_hotel_static_location("get_hotel_static_location", userid);
        call.enqueue(new Callback<Get_HotelStaticLocation_Response>() {
            @Override
            public void onResponse(Call<Get_HotelStaticLocation_Response> call, Response<Get_HotelStaticLocation_Response> response) {


                if (response.isSuccessful()) {


                    latitude = response.body().getData().get(0).getLatitude();
                    longitude = response.body().getData().get(0).getLongitude();



                } else {
                    Toast.makeText(CreateEvent.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Get_HotelStaticLocation_Response> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });

    }




    /*Mandatory field setting*/

    private void mandatoryFieldSetting(String eventName, EditText editText) {

        String colored = "*";

        SpannableStringBuilder builder = new SpannableStringBuilder();

        builder.append(eventName);

        int start = builder.length();

        builder.append(colored);

        int middle = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.RED), start, middle
                ,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        editText.setHint(builder);
    }



    /*Getting followers api calling*/

    private void getFollowers(final int userId) {

        if (Utility.isConnected(CreateEvent.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<GetFollowersOutput> call = apiService.getFollower("get_follower", userId);

                call.enqueue(new Callback<GetFollowersOutput>() {
                    @Override
                    public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                userid = response.body().getData().get(i).getUserid();
                                followerId = response.body().getData().get(i).getFollowerId();
                                firstName = response.body().getData().get(i).getFirstName();
                                lastName = response.body().getData().get(i).getLastName();
                                userPhoto = response.body().getData().get(i).getPicture();

                                if (!followerId.contains(String.valueOf(userid))) {

                                    GetFollowersData getFollowersData = new GetFollowersData(userid, followerId, firstName, lastName, userPhoto);
                                    getFollowersDataList.add(getFollowersData);

                                }


                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }

    }


    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {

            case R.id.close:

                /*Back to event page*/

                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Confirm")
                        .setContentText("Are you sure you want to skip")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                float deg = close.getRotation() + 180F;
                                close.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
                                Intent intent = new Intent(CreateEvent.this, Events.class);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


                break;

            /*From date click listener*/

            case R.id.event_from_date:

                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateEvent.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String frmdate;

                                if ((monthOfYear + 1) > 9) {

                                    if (dayOfMonth > 9) {

                                        frmdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                    } else {

                                        frmdate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;

                                    }


                                } else {

                                    if (dayOfMonth > 9) {

                                        frmdate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;

                                    } else {

                                        frmdate = year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth;

                                    }
                                }


                                startDateInput.setText(frmdate);
                                endDateInput.setText("");
                                startTimeInput.setText("");
                                endTimeInput.setText("");
                                startDateInput.setError(null);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();

                break;

            /*From time click listener*/

            case R.id.event_from_time:


                Calendar mcurrentTime = Calendar.getInstance();


                //mcurrentTime.set(2018,9,1);
                final int selectedHour = mcurrentTime.get(Calendar.HOUR_OF_DAY);

                final int selectedMinute = mcurrentTime.get(Calendar.MINUTE);

                RangeTimePickerDialog mTimePicker;

                mTimePicker = new RangeTimePickerDialog(CreateEvent.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        //get current date
                        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy ");
                        Date todayDate = new Date();
                        String thisDate = currentDate.format(todayDate);

                        // Toast.makeText(CreateEvent.this, "date" + thisDate, Toast.LENGTH_SHORT).show();
                        //  Toast.makeText(CreateEvent.this, "dddfg" + hourOfDay, Toast.LENGTH_SHORT).show();
                        //  Toast.makeText(CreateEvent.this, "dddfg" + view, Toast.LENGTH_SHORT).show();


                        // if (hourOfDay >= selectedHour && selectedMinute >= minute) {

                        if (hourOfDay > 9) {

                            if (minute > 9) {

                                eventStartTime = hourOfDay + ":" + minute;

//                                startTimeInput.setText(get12HrFormat(startTime));

                            } else {

                                eventStartTime = hourOfDay + ":0" + minute;

//                                startTimeInput.setText(get12HrFormat(startTime));

                            }

                        } else {

                            if (minute > 9) {

                                eventStartTime = "0" + hourOfDay + ":" + minute;

//                                startTimeInput.setText(get12HrFormat(startTime));

                            } else {

                                eventStartTime = "0" + hourOfDay + ":0" + minute;

//                                startTimeInput.setText(startTime);

                            }
                        }

                        Log.e(TAG, "onTimeSet: " + eventStartTime);


                        int hour = hourOfDay;
                        int minutes = minute;
                        String timeSet = "";
                        if (hour > 12) {
                            hour -= 12;
                            timeSet = "PM";
                        } else if (hour == 0) {
                            hour += 12;
                            timeSet = "AM";
                        } else if (hour == 12) {
                            timeSet = "PM";
                        } else {
                            timeSet = "AM";
                        }

                        String min = "";
                        if (minutes < 10)
                            min = "0" + minutes;
                        else
                            min = String.valueOf(minutes);

                        // Append in a StringBuilder
                        String aTime = new StringBuilder().append(hour).append(':')
                                .append(min).append(" ").append(timeSet).toString();


                        startTimeInput.setText(aTime);

                        startTime = hourOfDay;

                        startTimeInput.setError(null);

                       /* } else {
                            Toast.makeText(CreateEvent.this, "Invalid time! Please Select greater than current time", Toast.LENGTH_SHORT).show();
                        }*/

                    }
                }, selectedHour, selectedMinute, false);
                //Yes 24 hour time
                mTimePicker.show();


//                Calendar mcurrentTime = Calendar.getInstance();
////                mcurrentTime.set(2018,9,1);
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//
//                TimePickerDialog mTimePicker;
//
//                mTimePicker = new TimePickerDialog(CreateEvent.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//
//
//                        if (hourOfDay > 9) {
//
//                            if (minute > 9) {
//
//                                eventStartTime = hourOfDay + ":" + minute;
//
////                                startTimeInput.setText(get12HrFormat(startTime));
//
//                            } else {
//
//                                eventStartTime = hourOfDay + ":0" + minute;
//
////                                startTimeInput.setText(get12HrFormat(startTime));
//
//                            }
//
//                        } else {
//
//                            if (minute > 9) {
//
//                                eventStartTime = "0" + hourOfDay + ":" + minute;
//
////                                startTimeInput.setText(get12HrFormat(startTime));
//
//                            } else {
//
//                                eventStartTime = "0" + hourOfDay + ":0" + minute;
//
////                                startTimeInput.setText(startTime);
//
//                            }
//                        }
//
//
//                        Log.e(TAG, "onTimeSet: " + eventStartTime);
//
//
//                        int hour = hourOfDay;
//                        int minutes = minute;
//                        String timeSet = "";
//                        if (hour > 12) {
//                            hour -= 12;
//                            timeSet = "PM";
//                        } else if (hour == 0) {
//                            hour += 12;
//                            timeSet = "AM";
//                        } else if (hour == 12) {
//                            timeSet = "PM";
//                        } else {
//                            timeSet = "AM";
//                        }
//
//                        String min = "";
//                        if (minutes < 10)
//                            min = "0" + minutes;
//                        else
//                            min = String.valueOf(minutes);
//
//                        // Append in a StringBuilder
//                        String aTime = new StringBuilder().append(hour).append(':')
//                                .append(min).append(" ").append(timeSet).toString();
//
//
//                        startTimeInput.setText(aTime);
//
//                        startTime = hourOfDay;
//
//                        startTimeInput.setError(null);
//
//                    }
//                }, hour, minute, false);//Yes 24 hour time
//                mTimePicker.show();

                break;

            /*END date click listener*/

            case R.id.event_end_date:

                String startDate = startDateInput.getText().toString();

                long milliseconds = 0;


                if (startDate.length() != 0) {

                    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
                    try {

                        Date d = f.parse(startDate);
                        milliseconds = d.getTime();

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    final Calendar c1 = Calendar.getInstance();
                    int mYear1 = c1.get(Calendar.YEAR);
                    int mMonth1 = c1.get(Calendar.MONTH);
                    int mDay1 = c1.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog1 = new DatePickerDialog(CreateEvent.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    String frmdate;


                                    if ((monthOfYear + 1) > 9) {

                                        if (dayOfMonth > 9) {

                                            frmdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                        } else {

                                            frmdate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;

                                        }


                                    } else {

                                        if (dayOfMonth > 9) {

                                            frmdate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;

                                        } else {

                                            frmdate = year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth;

                                        }
                                    }

                                    endDateInput.setText(frmdate);
                                    endTimeInput.setText("");
                                    endDateInput.setError(null);


                                }
                            }, mYear1, mMonth1, mDay1);

                    datePickerDialog1.getDatePicker().setMinDate(milliseconds);
                    datePickerDialog1.show();


                } else {

                    Toast.makeText(this, "Select start date", Toast.LENGTH_LONG).show();
                }


                break;
            /*End time click listener*/

            case R.id.event_end_time:

                String eventStartTime = startTimeInput.getText().toString();

                if (eventStartTime.length() != 0) {

                    Calendar mcurrentTime1 = Calendar.getInstance();

                    final int selectedHour1 = mcurrentTime1.get(Calendar.HOUR_OF_DAY);

                    final int selectedminute1 = mcurrentTime1.get(Calendar.MINUTE);

                    RangeTimePickerDialog mTimePicker1;

                    mTimePicker1 = new RangeTimePickerDialog(CreateEvent.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {


                            if (selectedHour != startTime) {

                                endTimeInput.setError(null);

                                if (startDateInput.getText().toString().equalsIgnoreCase(endDateInput.getText().toString())) {
                                    if (selectedHour >= selectedHour1 && selectedMinute >= selectedminute1) {

                                        if (selectedHour > 9) {

                                            if (selectedMinute > 9) {

//                                        endTimeInput.setText(selectedHour + ":" + selectedMinute);

                                                eventEndTime = selectedHour + ":" + selectedMinute;

                                            } else {

//                                        endTimeInput.setText(selectedHour + ":0" + selectedMinute);

                                                eventEndTime = selectedHour + ":0" + selectedMinute;

                                            }


                                        } else {

                                            if (selectedMinute > 9) {

//                                        endTimeInput.setText("0"+selectedHour + ":" + selectedMinute);

                                                eventEndTime = "0" + selectedHour + ":" + selectedMinute;

                                            } else {

//                                        endTimeInput.setText("0"+selectedHour + ":0" + selectedMinute);

                                                eventEndTime = "0" + selectedHour + ":0" + selectedMinute;

                                            }
                                        }
                                        int hour = selectedHour;
                                        int minutes = selectedMinute;
                                        String timeSet = "";
                                        if (hour > 12) {
                                            hour -= 12;
                                            timeSet = "PM";
                                        } else if (hour == 0) {
                                            hour += 12;
                                            timeSet = "AM";
                                        } else if (hour == 12) {
                                            timeSet = "PM";
                                        } else {
                                            timeSet = "AM";
                                        }

                                        String min = "";
                                        if (minutes < 10)
                                            min = "0" + minutes;
                                        else
                                            min = String.valueOf(minutes);

                                        // Append in a StringBuilder
                                        String aTime = new StringBuilder().append(hour).append(':')
                                                .append(min).append(" ").append(timeSet).toString();


                                        endTimeInput.setText(aTime);


                                    } else {
                                        Toast.makeText(CreateEvent.this, "Invalid time! Please Select greater than current time", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    if (selectedHour > 9) {

                                        if (selectedMinute > 9) {

//                                        endTimeInput.setText(selectedHour + ":" + selectedMinute);

                                            eventEndTime = selectedHour + ":" + selectedMinute;

                                        } else {

//                                        endTimeInput.setText(selectedHour + ":0" + selectedMinute);

                                            eventEndTime = selectedHour + ":0" + selectedMinute;

                                        }


                                    } else {

                                        if (selectedMinute > 9) {

//                                        endTimeInput.setText("0"+selectedHour + ":" + selectedMinute);

                                            eventEndTime = "0" + selectedHour + ":" + selectedMinute;

                                        } else {

//                                        endTimeInput.setText("0"+selectedHour + ":0" + selectedMinute);

                                            eventEndTime = "0" + selectedHour + ":0" + selectedMinute;

                                        }
                                    }
                                    int hour = selectedHour;
                                    int minutes = selectedMinute;
                                    String timeSet = "";
                                    if (hour > 12) {
                                        hour -= 12;
                                        timeSet = "PM";
                                    } else if (hour == 0) {
                                        hour += 12;
                                        timeSet = "AM";
                                    } else if (hour == 12) {
                                        timeSet = "PM";
                                    } else {
                                        timeSet = "AM";
                                    }

                                    String min = "";
                                    if (minutes < 10)
                                        min = "0" + minutes;
                                    else
                                        min = String.valueOf(minutes);

                                    // Append in a StringBuilder
                                    String aTime = new StringBuilder().append(hour).append(':')
                                            .append(min).append(" ").append(timeSet).toString();


                                    endTimeInput.setText(aTime);
                                }


                            } else {

                                if (startDateInput.getText().toString().equalsIgnoreCase(endDateInput.getText().toString())) {

                                    endTimeInput.setText("");

                                    Toast.makeText(CreateEvent.this, "Event duration should be atleast one hour", Toast.LENGTH_LONG).show();

                                } else {

                                    endTimeInput.setError(null);


                                   /* if (startDateInput.getText().toString().equalsIgnoreCase(endDateInput.getText().toString())) {

                                        if (selectedHour >= selectedHour1 && selectedMinute >= selectedminute1){


                                            if (selectedHour > 9) {

                                                if (selectedMinute > 9) {

//                                            endTimeInput.setText(selectedHour + ":" + selectedMinute);

                                                    eventEndTime = selectedHour + ":" + selectedMinute;

                                                } else {

//                                            endTimeInput.setText(selectedHour + ":0" + selectedMinute);

                                                    eventEndTime = selectedHour + ":0" + selectedMinute;
                                                }


                                            } else {

                                                if (selectedMinute > 9) {

//                                            endTimeInput.setText("0"+selectedHour + ":" + selectedMinute);

                                                    eventEndTime = "0" + selectedHour + ":" + selectedMinute;

                                                } else {

//                                            endTimeInput.setText("0"+selectedHour + ":0" + selectedMinute);

                                                    eventEndTime = "0" + selectedHour + ":0" + selectedMinute;
                                                }
                                            }

                                            int hour = selectedHour;
                                            int minutes = selectedMinute;
                                            String timeSet = "";
                                            if (hour > 12) {
                                                hour -= 12;
                                                timeSet = "PM";
                                            } else if (hour == 0) {
                                                hour += 12;
                                                timeSet = "AM";
                                            } else if (hour == 12) {
                                                timeSet = "PM";
                                            } else {
                                                timeSet = "AM";
                                            }

                                            String min = "";
                                            if (minutes < 10)
                                                min = "0" + minutes;
                                            else
                                                min = String.valueOf(minutes);

                                            // Append in a StringBuilder
                                            String aTime = new StringBuilder().append(hour).append(':')
                                                    .append(min).append(" ").append(timeSet).toString();


                                            endTimeInput.setText(aTime);
                                        }
                                        else{
                                            Toast.makeText(CreateEvent.this, "Invalid time! Please Select greater than current time", Toast.LENGTH_SHORT).show();
                                        }
                                    }else {
*/
                                    if (selectedHour > 9) {

                                        if (selectedMinute > 9) {

//                                            endTimeInput.setText(selectedHour + ":" + selectedMinute);

                                            eventEndTime = selectedHour + ":" + selectedMinute;

                                        } else {

//                                            endTimeInput.setText(selectedHour + ":0" + selectedMinute);

                                            eventEndTime = selectedHour + ":0" + selectedMinute;
                                        }


                                    } else {

                                        if (selectedMinute > 9) {

//                                            endTimeInput.setText("0"+selectedHour + ":" + selectedMinute);

                                            eventEndTime = "0" + selectedHour + ":" + selectedMinute;

                                        } else {

//                                            endTimeInput.setText("0"+selectedHour + ":0" + selectedMinute);

                                            eventEndTime = "0" + selectedHour + ":0" + selectedMinute;
                                        }
                                    }


                                    int hour = selectedHour;
                                    int minutes = selectedMinute;
                                    String timeSet = "";
                                    if (hour > 12) {
                                        hour -= 12;
                                        timeSet = "PM";
                                    } else if (hour == 0) {
                                        hour += 12;
                                        timeSet = "AM";
                                    } else if (hour == 12) {
                                        timeSet = "PM";
                                    } else {
                                        timeSet = "AM";
                                    }

                                    String min = "";
                                    if (minutes < 10)
                                        min = "0" + minutes;
                                    else
                                        min = String.valueOf(minutes);

                                    // Append in a StringBuilder
                                    String aTime = new StringBuilder().append(hour).append(':')
                                            .append(min).append(" ").append(timeSet).toString();


                                    endTimeInput.setText(aTime);

//                                    }


                                }


                            }


                        }


                    }, selectedHour1, selectedminute1, false);
                    //Yes 24 hour time
                    mTimePicker1.show();


                } else {

                    Toast.makeText(this, "Select the start time", Toast.LENGTH_LONG).show();
                }


                break;

            /*Add photo click listener*/

            case R.id.add_photo:

                try {

                    if (ActivityCompat.checkSelfPermission(CreateEvent.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(CreateEvent.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);

                    } else {

                        galleryIntent();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            /*Change photo click listener*/
            case R.id.change_photo:

                final CharSequence[] items = {"Change Photo", "Remove Photo"};

                AlertDialog.Builder frontBuilder = new AlertDialog.Builder(CreateEvent.this);
                frontBuilder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (items[which].equals("Change Photo")) {
                            /*Gallery intent to select picture*/
                            galleryIntent();


                        } else if (items[which].equals("Remove Photo")) {

                            eventBackground.setImageDrawable(null);
                            changePhoto.setVisibility(View.GONE);
                            picturePath = null;
//                            pstore.setDetail(getApplicationContext(), "eventBackground", null);

                        }

                    }
                });
                frontBuilder.show();

                break;

            /*Create event click listener*/

            case R.id.tv_create_event:

                if (validate()) {

                    /*Calling create event api*/

                    sendEventDetails();
                }


                /*  if (eventTitleInput.getText().toString().trim().length() != 0) {

                 *//*Calling create event api*//*
                    sendEventDetails();


                } else {

                    eventTitleInput.setError("Enter the event title");

                }
*/

                break;

            case R.id.add_co_host:

                Log.e(TAG, "onClick: follow size " + getFollowersDataList.size());
                Log.e(TAG, "onClick: cohost size " + cohostNameIdListd.size());


                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CreateEvent.this);
                LayoutInflater inflater1 = LayoutInflater.from(CreateEvent.this);
                @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.activity_select_followers, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setTitle("");

                follwersRecylerview = dialogView.findViewById(R.id.user_followers_recylerview);
                selectActionBar = dialogView.findViewById(R.id.action_bar_select_follower);
                selectClose = selectActionBar.findViewById(R.id.close);
                selectDone = selectActionBar.findViewById(R.id.tv_done);
                emptyList = dialogView.findViewById(R.id.empty_list);

                searchFollowers = dialogView.findViewById(R.id.search_followers);

                searchFollowers.setQueryHint("Search");

                searchFollowers.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted
                        selectCohostAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        // filter recycler view when text is changed
                        selectCohostAdapter.getFilter().filter(query);

                        return false;
                    }
                });

                if (getFollowersDataList.isEmpty()) {

                    emptyList.setVisibility(View.VISIBLE);
                    follwersRecylerview.setVisibility(View.GONE);

                } else {

                    emptyList.setVisibility(View.GONE);
                    follwersRecylerview.setVisibility(View.VISIBLE);
                }
                /*Setting adapter*/

                selectCohostAdapter = new SelectCohostAdapter(CreateEvent.this, getFollowersDataList, cohostNameIdListd);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CreateEvent.this);
                follwersRecylerview.setLayoutManager(mLayoutManager);
                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                follwersRecylerview.setAdapter(selectCohostAdapter);
                follwersRecylerview.hasFixedSize();
                selectCohostAdapter.notifyDataSetChanged();



               /* if(getFollowersDataList.isEmpty()&& cohostNameIdListd.isEmpty()){

                    if (Utility.isConnected(CreateEvent.this)) {

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            Call<GetFollowersOutput> call = apiService.getFollower("get_follower", userid);

                            call.enqueue(new Callback<GetFollowersOutput>() {
                                @Override
                                public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {
                                        getFollowersDataList.clear();

                                        //Success

                                        Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                                        for (int i = 0; i < response.body().getData().size(); i++) {

                                            userid = response.body().getData().get(i).getUserid();
                                            followerId = response.body().getData().get(i).getFollowerId();
                                            firstName = response.body().getData().get(i).getFirstName();
                                            lastName = response.body().getData().get(i).getLastName();
                                            userPhoto = response.body().getData().get(i).getPicture();

                                            if (!followerId.contains(String.valueOf(userid))) {

                                                GetFollowersData getFollowersData = new GetFollowersData(userid, followerId, firstName, lastName, userPhoto);
                                                getFollowersDataList.add(getFollowersData);



                                            }

                                            selectCohostAdapter = new SelectCohostAdapter(CreateEvent.this, getFollowersDataList, cohostNameIdListd);
                                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CreateEvent.this);
                                            follwersRecylerview.setLayoutManager(mLayoutManager);
                                            follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                                            follwersRecylerview.setAdapter(selectCohostAdapter);
                                            follwersRecylerview.hasFixedSize();
                                            selectCohostAdapter.notifyDataSetChanged();



                                        }


                                    }
                                }

                                @Override
                                public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                    }else {
                        Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }

                }else {
                    selectCohostAdapter = new SelectCohostAdapter(CreateEvent.this, getFollowersDataList, cohostNameIdListd);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CreateEvent.this);
                    follwersRecylerview.setLayoutManager(mLayoutManager);
                    follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                    follwersRecylerview.setAdapter(selectCohostAdapter);
                    follwersRecylerview.hasFixedSize();
                    selectCohostAdapter.notifyDataSetChanged();
                }*/


                final AlertDialog dialog = dialogBuilder.create();
                dialog.show();
                /*Close click listener*/
                selectClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        getFollowersDataList.clear();
//                        getFollowers(userid);
                        dialog.dismiss();

                    }
                });
                /*Done click listener*/
                selectDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (cohostNameIdListd.keySet().size() == 0) {

                            friendsListDisplay.setVisibility(View.GONE);

                            addCoHost.setText(R.string.add_co_hosts);


                        } else {

                            String[] friends = cohostNameIdListd.values().toArray(new String[cohostNameIdListd.values().size()]);

                            String cohost = "";

                            if (friends.length == 1) {

                                cohost = friends[0];

                            } else if (friends.length == 2) {

                                cohost = friends[0] + " and " + friends[1];

                            } else if (friends.length > 2) {

                                cohost = friends[0] + " and " + (friends.length - 1) + " Others";

                            }


                            friendsListDisplay.setText(cohost);
                            addCoHost.setText("Edit Co-hosts");
                            List<Integer> getCoHost = new ArrayList<>(cohostNameIdListd.keySet());
                            cohostList = getCoHost;

                            friendsListDisplay.setVisibility(View.VISIBLE);


                        }

                        dialog.dismiss();


                    }
                });


                break;


            case R.id.invite_friends:


                final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(CreateEvent.this);
                LayoutInflater inflater11 = LayoutInflater.from(CreateEvent.this);
                @SuppressLint("InflateParams") final View dialogView1 = inflater11.inflate(R.layout.activity_select_followers, null);
                dialogBuilder1.setView(dialogView1);
                dialogBuilder1.setTitle("");

                follwersRecylerview = dialogView1.findViewById(R.id.user_followers_recylerview);
                selectActionBar = dialogView1.findViewById(R.id.action_bar_select_follower);
                selectClose = selectActionBar.findViewById(R.id.close);
                selectDone = selectActionBar.findViewById(R.id.tv_done);
                emptyList = dialogView1.findViewById(R.id.empty_list);

                SearchView search_followers = dialogView1.findViewById(R.id.search_followers);

                search_followers.setQueryHint("Search");

                search_followers.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted
                        selectInvitePeopleAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        // filter recycler view when text is changed
                        selectInvitePeopleAdapter.getFilter().filter(query);
                        return false;
                    }
                });


                if (getFollowersDataList.isEmpty()) {

                    emptyList.setVisibility(View.VISIBLE);
                    follwersRecylerview.setVisibility(View.GONE);

                } else {

                    emptyList.setVisibility(View.GONE);
                    follwersRecylerview.setVisibility(View.VISIBLE);
                }


                /*if(cohostList.size() == 0 ){

                    Log.e(TAG, "onClick: Empty");
                    selectInvitePeopleAdapter = new SelectInvitePeopleAdapter(CreateEvent.this, getFollowersDataList, friendsList,cohostList);
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(CreateEvent.this);
                    follwersRecylerview.setLayoutManager(mLayoutManager1);
                    follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                    follwersRecylerview.setAdapter(selectInvitePeopleAdapter);
                    follwersRecylerview.hasFixedSize();
                    selectInvitePeopleAdapter.notifyDataSetChanged();


                }else{

                    Log.e(TAG, "onClick: Not Empty");


                    invitedFollowerDataList.clear();

                    for(GetFollowersData getFollowersData: getFollowersDataList){


                        if (cohostList.contains(Integer.parseInt(getFollowersData.getFollowerId()))) {

                            // do nothing
                            Log.e(TAG, "onClick: True "+getFollowersData.getFollowerId() );

                        }else {

                            invitedFollowerDataList.add(getFollowersData);
                            Log.e(TAG, "onClick: False "+getFollowersData.getFollowerId() );

                        }

                    }

                    selectInvitePeopleAdapter = new SelectInvitePeopleAdapter(CreateEvent.this, invitedFollowerDataList, friendsList,cohostList);
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(CreateEvent.this);
                    follwersRecylerview.setLayoutManager(mLayoutManager1);
                    follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                    follwersRecylerview.setAdapter(selectInvitePeopleAdapter);
                    follwersRecylerview.hasFixedSize();
                    selectInvitePeopleAdapter.notifyDataSetChanged();


                }*/

                selectInvitePeopleAdapter = new SelectInvitePeopleAdapter(CreateEvent.this, getFollowersDataList, friendsList, cohostList);
                RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(CreateEvent.this);
                follwersRecylerview.setLayoutManager(mLayoutManager1);
                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                follwersRecylerview.setAdapter(selectInvitePeopleAdapter);
                follwersRecylerview.hasFixedSize();
                selectInvitePeopleAdapter.notifyDataSetChanged();


                final AlertDialog dialog1 = dialogBuilder1.create();
                dialog1.show();

                selectClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog1.dismiss();

                    }
                });

                selectDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (friendsList.size() == 0) {

                            inviteFriendsListDisplay.setVisibility(View.GONE);
                            inviteFriends.setText(R.string.invite_guest);


                        } else {

                            inviteFriendsListDisplay.setText(friendsList.size() + " Guests are invited");
                            inviteFriends.setText("Edit Guest");
                            inviteFriendsListDisplay.setVisibility(View.VISIBLE);
                        }


                        dialog1.dismiss();
                    }
                });


                break;

            default:

                break;

        }


    }

    public String get12HrFormat(String time24Hr) {

        String time = "";

        try {


            String _24HourTime = time24Hr;
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);

            System.out.println(_24HourDt);
            System.out.println(_12HourSDF.format(_24HourDt));

            time = _12HourSDF.format(_24HourDt);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return time;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        int permissionSize = permissions.length;

        Log.e(TAG, "onRequestPermissionsResult:" + "requestCode->" + requestCode);

        switch (requestCode) {

            case PICK_FROM_GALLERY:

                for (int i = 0; i < permissionSize; i++) {


                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");
                        galleryIntent();

                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");
                        cameraStorageGrantRequest();

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

        }
    }

    private void cameraStorageGrantRequest() {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                CreateEvent.this);

        builder.setTitle("Food Wall would like to access your camera and storage")
                .setMessage("You previously chose not to use Android's camera or storage with Food Wall. To upload your photo, allow camera and storage permission in App Settings. Click Ok to allow.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // permission dialog
                        dialog.dismiss();
                        try {

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();


    }

    /*Calling api for event co host */
    public void getEventCoHost(int eventId) {


        if (Utility.isConnected(getApplicationContext())) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                final int userid = Integer.parseInt(Pref_storage.getDetail(CreateEvent.this, "userId"));

                Call<GetEventCohostOutput> call = apiService.getEventCohost("get_events_cohosts", eventId);

                call.enqueue(new Callback<GetEventCohostOutput>() {
                    @Override
                    public void onResponse(Call<GetEventCohostOutput> call, Response<GetEventCohostOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {


                                String name = response.body().getData().get(i).getFirstName() + " " + response.body().getData().get(i).getLastName();
                                int cohostId = response.body().getData().get(i).getCohostId();
                                int cohostUserId = response.body().getData().get(i).getUserId();

                                if (cohostUserId != userid) {

                                    cohostNameIdListd.put(cohostId, name);

                                    try {

                                        Integer[] friends = cohostNameIdListd.keySet().toArray(new Integer[cohostNameIdListd.keySet().size()]);

                                        String cohost = "";

                                        if (friends.length == 1) {

                                            Log.e(TAG, "onResponse: " + cohostNameIdListd.get(friends[0]));

//                                    cohost = String.valueOf(friends[0]);
//                                        cohost = friends[0].toString();
                                            cohost = cohostNameIdListd.get(friends[0]);

                                        } else if (friends.length == 2) {

                                            Log.e(TAG, "onResponse: " + cohostNameIdListd.get(16) + " and " + cohostNameIdListd.get(17));

//                                    cohost = String.valueOf(friends[0]) + " and " + String.valueOf(friends[1]);
//                                        cohost = friends[0].toString() + " and " + friends[1].toString();
                                            cohost = cohostNameIdListd.get(friends[0]) + " and " + cohostNameIdListd.get(friends[1]);

                                        } else if (friends.length > 2) {

//                                    cohost = String.valueOf(friends[0]) + " and " + String.valueOf(friends.length - 1) + " Others";
//                                        cohost = friends[0].toString() + " and " + String.valueOf(friends.length - 1) + " Others";
                                            cohost = cohostNameIdListd.get(friends[0]) + " and " + (friends.length - 1) + " Others";

                                        }

                                        friendsListDisplay.setText(cohost);
                                        addCoHost.setText("Edit Co-hosts");

//                                friendsListDisplay.setTags(friends);
                                        friendsListDisplay.setVisibility(View.VISIBLE);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                }


                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetEventCohostOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }
        } else {
            Toast.makeText(CreateEvent.this, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

        }


    }


    public void getEventGuest(int eventId) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            final int userid = Integer.parseInt(Pref_storage.getDetail(CreateEvent.this, "userId"));

            Call<GetEventShareOutput> call = apiService.getEventGuests("get_events_share", eventId, userid);

            call.enqueue(new Callback<GetEventShareOutput>() {
                @Override
                public void onResponse(Call<GetEventShareOutput> call, Response<GetEventShareOutput> response) {

                    String responseStatus = response.body().getResponseMessage();

                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                    if (responseStatus.equals("success")) {

                        //Success

                        Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                        for (int i = 0; i < response.body().getData().size(); i++) {


                            String name = response.body().getData().get(i).getFirstName() + " " + response.body().getData().get(i).getLastName();
                            int guestId = Integer.valueOf(response.body().getData().get(i).getUserId());

                            if (guestId != userid) {

                                friendsList.add(guestId);
                            }

                        }

                        if (friendsList.isEmpty()) {

                            inviteFriendsListDisplay.setVisibility(View.GONE);
                            inviteFriends.setText(R.string.invite_guest);


                        } else {

                            inviteFriendsListDisplay.setText(friendsList.size() + " Guests are invited");
                            inviteFriends.setText("Edit Guest");
                            inviteFriendsListDisplay.setVisibility(View.VISIBLE);
                        }

                    }
                }

                @Override
                public void onFailure(Call<GetEventShareOutput> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }


    }

    /*Select co host listener*/
    public void getSelectedCoHosts(View view, GetFollowersData getFollowersData, int position) {

        String followerName;
        followerName = getFollowersData.getFirstName() + " " + getFollowersData.getLastName();


        if (position == 1) {

            if (cohostNameIdListd.containsValue(Integer.valueOf(getFollowersData.getFollowerId()))) {

                return;

            } else {

                cohostNameIdListd.put(Integer.valueOf(getFollowersData.getFollowerId()), followerName);

            }


        } else if (position == 0) {

            cohostNameIdListd.remove(Integer.valueOf(getFollowersData.getFollowerId()));

        }


        Log.e(TAG, "getSelectedCoHosts: " + cohostNameIdListd.toString());

    }


    private void getUserEventsData() {


        if (Utility.isConnected(CreateEvent.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(CreateEvent.this, "userId"));

                Call<GetAllEventsOutput> call = apiService.getUserEvents("get_events_user", 0, userid);

                call.enqueue(new Callback<GetAllEventsOutput>() {
                    @Override
                    public void onResponse(Call<GetAllEventsOutput> call, Response<GetAllEventsOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                int eventId = response.body().getData().get(i).getEventId();
                                String eventName = response.body().getData().get(i).getEventName();
                                String eventDescription = response.body().getData().get(i).getEventDescription();
                                int totalLikes = response.body().getData().get(i).getTotalLikes();
                                int totalComments = response.body().getData().get(i).getTotalComments();
                                int eventType = response.body().getData().get(i).getEventType();
                                int userLike = response.body().getData().get(i).getEvtLikes();
                                int userGoing = response.body().getData().get(i).getGngLikes();
                                int share_type = response.body().getData().get(i).getShareType();
                                int total_discussion = response.body().getData().get(i).getTotal_discussion();
                                String ticketUrl = response.body().getData().get(i).getTicketUrl();
                                String eventImage = response.body().getData().get(i).getEventImage();
                                String eventLocation = response.body().getData().get(i).getLocation();
                                String eventStartDate = response.body().getData().get(i).getStartDate();
                                String eventEndDate = response.body().getData().get(i).getEndDate();
                                String eventCreatedBy = response.body().getData().get(i).getCreatedBy();
                                String eventCreatedOn = response.body().getData().get(i).getCreatedOn();
                                String firstName = response.body().getData().get(i).getFirstName();
                                String lastName = response.body().getData().get(i).getLastName();
                                String userPicture = response.body().getData().get(i).getPicture();

                                getAllEventData = new GetAllEventData(eventId, eventName, eventDescription, totalLikes, totalComments, eventImage, eventType,
                                        ticketUrl, eventLocation, eventStartDate, eventEndDate, eventCreatedBy, eventCreatedOn, firstName,
                                        lastName, userPicture, userLike, userGoing, share_type, total_discussion);
                                getMyEventList.add(getAllEventData);


                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetAllEventsOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(CreateEvent.this, "No internet connection", Toast.LENGTH_SHORT).show();

        }


    }

    /*Invited people interface listener*/

    public void getInvitedPeople(View view, GetFollowersData getFollowersData, int position) {

        if (position == 1) {

            if (friendsList.contains(Integer.valueOf(getFollowersData.getFollowerId()))) {

                return;

            } else {

                friendsList.add(Integer.valueOf(getFollowersData.getFollowerId()));

            }


        } else if (position == 0) {

            friendsList.remove(Integer.valueOf(getFollowersData.getFollowerId()));

        }

        Log.e(TAG, "getInvitedPeople:viji " + friendsList.toString());

    }


    public void onStart() {
        super.onStart();

        eventsType.setText(getIntent().getStringExtra("eventName"));
        eventCode = getIntent().getIntExtra("eventCode", -1);
        launchMode = getIntent().getIntExtra("LaunchMode", -1);
        eventEditPosition = getIntent().getIntExtra("eventEditPosition", -1);

        /*Event type condtion*/

        eventTypeHandler(eventCode);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == selectFile) {

                try {
                    onSelectFromGalleryResult(data);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }
        }
    }
    /*Calling create event api*/

    private void sendEventDetails() {

        /*Check internet connectivity*/

        if (Utility.isConnected(CreateEvent.this)) {


            if (launchMode == 1) {

                if (validate()) {

                    /*Loader*/

                    final AlertDialog dialog = new SpotsDialog.Builder().setContext(CreateEvent.this).setMessage("").build();

                    dialog.show();

                    final int guestInvite;

                    if (switchGuestInvite.isChecked()) {

                        guestInvite = 1;

                    } else {

                        guestInvite = 0;
                    }

                    if (cohostList.isEmpty()) {

                        cohostStatus = 0;

                    } else {

                        cohostStatus = 1;

                    }

                    Log.e("cohostStatus", "cohostStatus: " + cohostStatus);
                    Log.e("cohostStatus", "cohostList.size(): " + cohostList.size());
                    Log.e("cohostStatus", "cohostList.isEmpty(): " + cohostList.isEmpty());
                    Log.e("cohostStatus", "cohostList: " + cohostList.toString());


                    String eventStart = startDateInput.getText().toString() + " " + eventStartTime + ":00";
                    String eventEnd = endDateInput.getText().toString() + " " + eventEndTime + ":00";

                    Log.e(TAG, "sendEventDetails:eventId " + eventId);
                    Log.e(TAG, "sendEventDetails:eventTitleInput " + eventTitleInput.getText().toString());
                    Log.e(TAG, "sendEventDetails:eventDescriptionInput " + eventDescriptionInput.getText().toString());
                    Log.e(TAG, "sendEventDetails:eventCode " + eventCode);
                    Log.e(TAG, "sendEventDetails:guestInvite " + guestInvite);
                    Log.e(TAG, "sendEventDetails:imageExists " + imageExists);
                    Log.e(TAG, "sendEventDetails:eventTicketUrlInput " + eventTicketUrlInput.getText().toString());
                    Log.e(TAG, "sendEventDetails:eventLocationInput " + eventLocationInput.getText().toString());
                    Log.e(TAG, "sendEventDetails:eventStart " + eventStart);
                    Log.e(TAG, "sendEventDetails:eventEnd " + eventEnd);
                    Log.e(TAG, "sendEventDetails:latitude " + latitude);
                    Log.e(TAG, "sendEventDetails:longitude " + longitude);
                    Log.e(TAG, "sendEventDetails:userid " + userid);
                    Log.e(TAG, "sendEventDetails:cohostStatus " + cohostStatus);
                    Log.e(TAG, "sendEventDetails:cohostList " + cohostList.toString());
                    Log.e(TAG, "sendEventDetails:friendsList " + friendsList.toString());
                    Log.e(TAG, "sendEventDetails:filePart " + picturePath);
                    Log.e(TAG, "onClick: " + cohostList.toString());


                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        // Set up progress before call

                        int userid = Integer.parseInt(Pref_storage.getDetail(CreateEvent.this, "userId"));


                        String cohostListJSON = new Gson().toJson(cohostList);
                        String friendsListJSON = new Gson().toJson(friendsList);
                        Log.e(TAG, "cohostListJSON:viji1 " + cohostListJSON);
                        Log.e(TAG, "cohostListJSONfriendsListJSON:viji1 " + friendsListJSON);

                        File sourceFile = new File(picturePath);
                        ProgressRequestBody fileBody = new ProgressRequestBody(sourceFile, this);
                        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", sourceFile.getName(), fileBody);

                        RequestBody requestBodyEventTitle = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(eventTitleInput.getText().toString().trim()));
                        RequestBody requestBodyEventDescription = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(eventDescriptionInput.getText().toString().trim()));
                        RequestBody requestBodyTicketUrl = RequestBody.create(MediaType.parse("text/plain"), eventTicketUrlInput.getText().toString());
                        RequestBody requestBodyLocation = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(eventLocationInput.getText().toString().trim()));
                        RequestBody requestBodyStartDate = RequestBody.create(MediaType.parse("text/plain"), eventStart);
                        RequestBody requestBodyEndDate = RequestBody.create(MediaType.parse("text/plain"), eventEnd);
                        RequestBody requestBodycohostListJSON = RequestBody.create(MediaType.parse("text/plain"), cohostListJSON);
                        RequestBody requestBodyfriendsListJSON = RequestBody.create(MediaType.parse("text/plain"), friendsListJSON);
                        RequestBody requestBodyImageExists = RequestBody.create(MediaType.parse("text/plain"), "");

                        Call<CreateEventOutput> call = apiService.createEvent("create_events",
                                0,
                                requestBodyEventTitle,
                                requestBodyEventDescription,
                                eventCode,
                                guestInvite,
                                requestBodyImageExists,
                                requestBodyTicketUrl,
                                requestBodyLocation,
                                requestBodyStartDate,
                                requestBodyEndDate,
                                latitude,
                                longitude,
                                userid,
                                cohostStatus,
                                requestBodycohostListJSON,
                                requestBodyfriendsListJSON,
                                1,
                                filePart);

                        call.enqueue(new Callback<CreateEventOutput>() {
                            @Override
                            public void onResponse(Call<CreateEventOutput> call, Response<CreateEventOutput> response) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {

                                    if (launchMode == 2) {

                                        Toast.makeText(CreateEvent.this, "Event Edited sucessfully", Toast.LENGTH_SHORT).show();

                                    } else {


                                        Toast.makeText(CreateEvent.this, "Event created sucessfully", Toast.LENGTH_SHORT).show();


                                    }
                                    dialog.dismiss();
                                    //Success
                                    Intent intent = new Intent(CreateEvent.this, Events.class);
                                    intent.putExtra("eventType", String.valueOf(eventCode));
                                    startActivity(intent);
                                    finish();

                                }
                            }

                            @Override
                            public void onFailure(Call<CreateEventOutput> call, Throwable t) {
                                //Error
                                dialog.dismiss();
                                Log.e("FailureError", "FailureError" + t.getMessage());
//                                Toast.makeText(CreateEvent.this, "Something went wrong" , Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(CreateEvent.this, Events.class);
                                intent.putExtra("eventType", String.valueOf(eventCode));
                                startActivity(intent);
                                finish();

                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            } else if (launchMode == 2) {

                if (editValidate()) {


                    final AlertDialog dialog = new SpotsDialog.Builder().setContext(CreateEvent.this).setMessage("").build();

                    dialog.show();


                    final int guestInvite;

                    if (switchGuestInvite.isChecked()) {

                        guestInvite = 1;

                    } else {

                        guestInvite = 0;
                    }

                    if (cohostList.isEmpty()) {

                        cohostStatus = 0;

                    } else {

                        cohostStatus = 1;

                    }


                    String eventStart = startDateInput.getText().toString() + " " + eventStartTime + ":00";
                    String eventEnd = endDateInput.getText().toString() + " " + eventEndTime + ":00";

                    Log.e(TAG, "sendEventDetails:eventId " + eventId);
                    Log.e(TAG, "sendEventDetails:eventTitleInput " + eventTitleInput.getText().toString());
                    Log.e(TAG, "sendEventDetails:eventDescriptionInput " + eventDescriptionInput.getText().toString());
                    Log.e(TAG, "sendEventDetails:eventCode " + eventCode);
                    Log.e(TAG, "sendEventDetails:guestInvite " + guestInvite);
                    Log.e(TAG, "sendEventDetails:imageExists " + imageExists);
                    Log.e(TAG, "sendEventDetails:eventTicketUrlInput " + eventTicketUrlInput.getText().toString());
                    Log.e(TAG, "sendEventDetails:eventLocationInput " + eventLocationInput.getText().toString());
                    Log.e(TAG, "sendEventDetails:eventStart " + eventStart);
                    Log.e(TAG, "sendEventDetails:eventEnd " + eventEnd);
                    Log.e(TAG, "sendEventDetails:latitude " + latitude);
                    Log.e(TAG, "sendEventDetails:longitude " + longitude);
                    Log.e(TAG, "sendEventDetails:userid " + userid);
                    Log.e(TAG, "sendEventDetails:cohostStatus " + cohostStatus);
                    Log.e(TAG, "sendEventDetails:cohostList " + cohostList.toString());
                    Log.e(TAG, "sendEventDetails:friendsList " + friendsList.toString());
                    Log.e(TAG, "sendEventDetails:filePart " + picturePath);

                    if (imageExists.equals("")) {

//                        Toast.makeText(this, "Image changed", Toast.LENGTH_SHORT).show();

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(CreateEvent.this, "userId"));

                            String cohostListJSON = new Gson().toJson(cohostList);
                            String friendsListJSON = new Gson().toJson(friendsList);

                            Log.e(TAG, "cohostListJSON:viji2 " + cohostListJSON);
                            Log.e(TAG, "cohostListJSONfriendsListJSON:viji2 " + friendsListJSON);

                            File sourceFile = new File(picturePath);
                            ProgressRequestBody fileBody = new ProgressRequestBody(sourceFile, this);
                            MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", sourceFile.getName(), fileBody);

                            RequestBody requestBodyEventTitle = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(eventTitleInput.getText().toString().trim()));
                            RequestBody requestBodyEventDescription = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(eventDescriptionInput.getText().toString().trim()));
                            RequestBody requestBodyTicketUrl = RequestBody.create(MediaType.parse("text/plain"), eventTicketUrlInput.getText().toString());
                            RequestBody requestBodyLocation = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(eventLocationInput.getText().toString().trim()));
                            RequestBody requestBodyStartDate = RequestBody.create(MediaType.parse("text/plain"), eventStart);
                            RequestBody requestBodyEndDate = RequestBody.create(MediaType.parse("text/plain"), eventEnd);
                            RequestBody requestBodyImageExists = RequestBody.create(MediaType.parse("text/plain"), imageExists);
                            RequestBody requestBodycohostListJSON = RequestBody.create(MediaType.parse("text/plain"), cohostListJSON);
                            RequestBody requestBodyfriendsListJSON = RequestBody.create(MediaType.parse("text/plain"), friendsListJSON);


                            Call<CreateEventOutput> call = apiService.createEvent("create_events",
                                    eventId,
                                    requestBodyEventTitle,
                                    requestBodyEventDescription,
                                    eventCode,
                                    guestInvite,
                                    requestBodyImageExists,
                                    requestBodyTicketUrl,
                                    requestBodyLocation, requestBodyStartDate,
                                    requestBodyEndDate, latitude, longitude, userid,
                                    cohostStatus,
                                    requestBodycohostListJSON,
                                    requestBodyfriendsListJSON, 1, filePart);


                            call.enqueue(new Callback<CreateEventOutput>() {
                                @Override
                                public void onResponse(Call<CreateEventOutput> call, Response<CreateEventOutput> response) {


                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        if (launchMode == 2) {

                                            Toast.makeText(CreateEvent.this, "Event Edited sucessfully", Toast.LENGTH_SHORT).show();

                                        } else {

                                            Toast.makeText(CreateEvent.this, "Event created sucessfully", Toast.LENGTH_SHORT).show();

                                        }
                                        dialog.dismiss();
                                        //Success
                                        Intent intent = new Intent(CreateEvent.this, Events.class);
                                        intent.putExtra("eventType", String.valueOf(eventCode));
                                        startActivity(intent);
                                        finish();

                                    }
                                }

                                @Override
                                public void onFailure(Call<CreateEventOutput> call, Throwable t) {
                                    //Error
                                    dialog.dismiss();
                                    Log.e("FailureError", "FailureError" + t.getMessage());
//                                    Toast.makeText(CreateEvent.this, "Something went wrong" , Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(CreateEvent.this, Events.class);
                                    intent.putExtra("eventType", String.valueOf(eventCode));
                                    startActivity(intent);
                                    finish();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {


//                        Toast.makeText(this, "Image not changed", Toast.LENGTH_SHORT).show();

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(CreateEvent.this, "userId"));

                            String cohostListJSON = new Gson().toJson(cohostList);
                            String friendsListJSON = new Gson().toJson(friendsList);

                            Log.e(TAG, "cohostListJSON:viji3 " + cohostListJSON);
                            Log.e(TAG, "cohostListJSONfriendsListJSON:viji3 " + friendsListJSON);

                            RequestBody requestBodyEventTitle = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(eventTitleInput.getText().toString().trim()));
                            RequestBody requestBodyEventDescription = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(eventDescriptionInput.getText().toString()));
                            RequestBody requestBodyTicketUrl = RequestBody.create(MediaType.parse("text/plain"), eventTicketUrlInput.getText().toString());
                            RequestBody requestBodyLocation = RequestBody.create(MediaType.parse("text/plain"), org.apache.commons.text.StringEscapeUtils.escapeJava(eventLocationInput.getText().toString()));
                            RequestBody requestBodyStartDate = RequestBody.create(MediaType.parse("text/plain"), eventStart);
                            RequestBody requestBodyEndDate = RequestBody.create(MediaType.parse("text/plain"), eventEnd);
                            RequestBody requestBodyImageExists = RequestBody.create(MediaType.parse("text/plain"), imageExists);
                            RequestBody requestBodycohostListJSON = RequestBody.create(MediaType.parse("text/plain"), cohostListJSON);
                            RequestBody requestBodyfriendsListJSON = RequestBody.create(MediaType.parse("text/plain"), friendsListJSON);

                            RequestBody requestBodyImage = RequestBody.create(MediaType.parse("text/plain"), "");
                            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", "", requestBodyImage);


                            Call<CreateEventOutput> call = apiService.createEvent("create_events", eventId,
                                    requestBodyEventTitle,
                                    requestBodyEventDescription, eventCode,
                                    guestInvite, requestBodyImageExists,
                                    requestBodyTicketUrl, requestBodyLocation,
                                    requestBodyStartDate,
                                    requestBodyEndDate, latitude,
                                    longitude, userid, cohostStatus,
                                    requestBodycohostListJSON,
                                    requestBodyfriendsListJSON,
                                    0, filePart);


                            call.enqueue(new Callback<CreateEventOutput>() {
                                @Override
                                public void onResponse(Call<CreateEventOutput> call, Response<CreateEventOutput> response) {


                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            if (launchMode == 2) {

                                                Toast.makeText(CreateEvent.this, "Event Edited sucessfully", Toast.LENGTH_SHORT).show();

                                            } else {

                                                Toast.makeText(CreateEvent.this, "Event created sucessfully", Toast.LENGTH_SHORT).show();

                                            }
                                            dialog.dismiss();
                                            //Success
                                            Intent intent = new Intent(CreateEvent.this, Events.class);
                                            intent.putExtra("eventType", String.valueOf(eventCode));
                                            startActivity(intent);
                                            finish();

                                        }

                                    }


                                }

                                @Override
                                public void onFailure(Call<CreateEventOutput> call, Throwable t) {
                                    //Error
                                    dialog.dismiss();
                                    Log.e("FailureError", "FailureError" + t.getMessage());
//                                    Toast.makeText(CreateEvent.this, "Something went wrong" , Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(CreateEvent.this, Events.class);
                                    intent.putExtra("eventType", String.valueOf(eventCode));
                                    startActivity(intent);
                                    finish();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }


                }


            }


        } else {

            Snackbar snackbar = Snackbar.make(llCreateEvent, "Please check your internet.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();

        }


    }



    /*
     * Calling location search API*/

    /*Location search */

    private void locationSearch(String placeName, final View view) {

        Call<Restaurant_output_pojo> call = apiService.location_search(placeName, APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<Restaurant_output_pojo>() {
            @Override
            public void onResponse(Call<Restaurant_output_pojo> call, Response<Restaurant_output_pojo> response) {
                Restaurant_output_pojo restaurantOutputPojo = response.body();


                if (response.isSuccessful()) {

                    Utility.hideKeyboard(view);


                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {


                        for (int i = 0; i < restaurantOutputPojo.getResults().size(); i++) {


                            String placeid = restaurantOutputPojo.getResults().get(i).getPlaceId();

                            String hotel_name = restaurantOutputPojo.getResults().get(i).getName();
                            String Id = restaurantOutputPojo.getResults().get(i).getId();

                            String reference = restaurantOutputPojo.getResults().get(i).getReference();

                            String address = restaurantOutputPojo.getResults().get(i).getFormatted_address();


//                            }


                            try {
                                double lat = restaurantOutputPojo.getResults().get(0).getGeometry().getLocation().getLatitude();
                                double longit = restaurantOutputPojo.getResults().get(0).getGeometry().getLocation().getLongitude();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<Restaurant_output_pojo> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });


    }


    String toJSON(List<String> list) {
        Gson gson = new Gson();
        StringBuilder sb = new StringBuilder();
        for (String d : list) {
            sb.append(gson.toJson(d));
        }
        return sb.toString();
    }

    /*Validation*/
    private boolean validate() {

        boolean valid = true;

        String eTitle = eventTitleInput.getText().toString().trim();
        String eStartDate = startDateInput.getText().toString();
        String eStartTime = startTimeInput.getText().toString();
        String eEndDate = endDateInput.getText().toString();
        String eEndTime = endTimeInput.getText().toString();
        String eLocation = eventLocationInput.getText().toString().trim();
        String eDescription = eventDescriptionInput.getText().toString().trim();


        if (picturePath == null && launchMode == 1) {
            Toast.makeText(this, "Add event photo", Toast.LENGTH_SHORT).show();
            valid = false;
        }


        if (eTitle.length() == 0) {
            eventTitleInput.requestFocus();
            eventTitleInput.setError("Enter event title");
            valid = false;
        } else {
            eventTitleInput.setError(null);
        }


        if (eStartDate.length() == 0) {
            startDateInput.requestFocus();
            startDateInput.setError("Select start date");
            valid = false;
        } else {
            startDateInput.setError(null);
        }

        if (eStartTime.length() == 0) {
            startTimeInput.requestFocus();
            startTimeInput.setError("Select start time");
            valid = false;
        } else {
            startTimeInput.setError(null);
        }


        if (eEndDate.length() == 0) {
            endDateInput.requestFocus();
            endDateInput.setError("Select end date");
            valid = false;
        } else {
            endDateInput.setError(null);
        }

        if (eEndTime.length() == 0) {
            endTimeInput.requestFocus();
            endTimeInput.setError("Select end time");
            valid = false;
        } else {
            endTimeInput.setError(null);
        }


        if (eLocation.length() == 0) {
            eventLocationInput.requestFocus();
            eventLocationInput.setError("Enter location");
            valid = false;
        } else {
            eventLocationInput.setError(null);
        }


        if (eDescription.length() == 0) {
            eventDescriptionInput.requestFocus();
            eventDescriptionInput.setError("Enter something about the event");
            valid = false;
        } else {
            eventDescriptionInput.setError(null);
        }


        if (friendsList.isEmpty() && eventCode == 1) {

            Toast.makeText(this, "Please invite your guests", Toast.LENGTH_SHORT).show();
            valid = false;

        } else {

        }

        return valid;
    }

    /*Edit validation*/
    private boolean editValidate() {

        boolean valid = true;

        String eTitle = eventTitleInput.getText().toString();
        String eStartDate = startDateInput.getText().toString();
        String eStartTime = startTimeInput.getText().toString();
        String eEndDate = endDateInput.getText().toString();
        String eEndTime = endTimeInput.getText().toString();
        String eLocation = eventLocationInput.getText().toString();
        String eDescription = eventDescriptionInput.getText().toString();


        if (picturePath == null && launchMode == 2) {

            imageExists = eventImage;

        } else {

            imageExists = "";

        }

        if (eventBackground.getDrawable() == null) {

            Toast.makeText(this, "Select event image", Toast.LENGTH_SHORT).show();
            valid = false;
        }


        if (eTitle.length() == 0) {
            eventTitleInput.requestFocus();
            eventTitleInput.setError("Enter event title");
            valid = false;
        } else {
            eventTitleInput.setError(null);
        }


        if (eStartDate.length() == 0) {
            startDateInput.requestFocus();
            startDateInput.setError("Select start date");
            valid = false;
        } else {
            startDateInput.setError(null);
        }

        if (eStartTime.length() == 0) {
            startTimeInput.requestFocus();
            startTimeInput.setError("Select start time");
            valid = false;
        } else {
            startTimeInput.setError(null);
        }


        if (eEndDate.length() == 0) {
            endDateInput.requestFocus();
            endDateInput.setError("Select end date");
            valid = false;
        } else {
            endDateInput.setError(null);
        }

        if (eEndTime.length() == 0) {
            endTimeInput.requestFocus();
            endTimeInput.setError("Select end time");
            valid = false;
        } else {
            endTimeInput.setError(null);
        }


        if (eLocation.length() == 0) {
            eventLocationInput.requestFocus();
            eventLocationInput.setError("Enter location");
            valid = false;
        } else {
            eventLocationInput.setError(null);
        }


        if (eDescription.length() == 0) {
            eventDescriptionInput.requestFocus();
            eventDescriptionInput.setError("Enter something about the event");
            valid = false;
        } else {
            eventDescriptionInput.setError(null);
        }


        return valid;
    }
    /*Gallery intent to select images*/

    private void galleryIntent() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, selectFile);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Uri selectedImage = data.getData();


        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor c = getApplicationContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);

        c.moveToFirst();


        int columnIndex = c.getColumnIndex(filePathColumn[0]);
        picturePath = c.getString(columnIndex);
        c.close();

        Log.e("picturePath", "picturePath->" + picturePath);

        /* Event image */

        GlideApp.with(getApplicationContext())
                .load(picturePath)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.01f)
                .centerCrop()
                .into(eventBackground);


        Bitmap photo1 = (BitmapFactory.decodeFile(picturePath));
//        eventBackground.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        changePhoto.setVisibility(View.VISIBLE);
//        Pref_storage.setDetail(getApplicationContext(), "eventBackground", picturePath);

    }

    public String getImageUrlWithAuthority(Context context, Uri uri) {
        InputStream is = null;
        if (uri.getAuthority() != null) {
            try {
                is = context.getContentResolver().openInputStream(uri);
                Bitmap bmp = BitmapFactory.decodeStream(is);
                return writeToTempImageAndGetPathUri(context, bmp).toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /*Temp image with path URI*/

    public Uri writeToTempImageAndGetPathUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    /*Event type condtion*/
    private void eventTypeHandler(int eventCode) {

        switch (eventCode) {

            case 1:

                /* Private */

                guestInvite.setVisibility(View.VISIBLE);
                addCoHost.setVisibility(View.VISIBLE);
                inviteFriends.setVisibility(View.VISIBLE);
                eventTicketUrlInput.setVisibility(View.GONE);
                adminApprove.setVisibility(View.GONE);
                addGroup.setVisibility(View.GONE);

                break;

            case 2:

                /* Public */
                guestInvite.setVisibility(View.GONE);
                eventTicketUrlInput.setVisibility(View.VISIBLE);
                addCoHost.setVisibility(View.VISIBLE);
                addGroup.setVisibility(View.GONE);
                inviteFriends.setVisibility(View.GONE);

                break;

            default:
                break;


        }

    }


    @Override
    public void onTagsChanged(Collection<String> tags) {

        if (tags.isEmpty()) {

            friendsListDisplay.setVisibility(View.GONE);

        } else {

            friendsListDisplay.setVisibility(View.VISIBLE);

        }

        cohostList.clear();

        Log.d("FollowersId", "Tags changed: ");
        Log.d("FollowersId", Arrays.toString(tags.toArray()));

        Object[] ob = tags.toArray();

       /* for (Object value : ob) {

            System.out.println("Number = " + value);
            cohostList.add(cohostNameIdListd.get(value));

        }

        Iterator<String> iterator = cohostNameIdListd.keySet().iterator();
        while (iterator.hasNext()) {

            String certification = iterator.next();

            if (tags.contains(certification)) {

                Log.e("EventCheck", "EventCheck->" + certification);

            } else {

                Log.e("EventCheck", "Not Found");
                iterator.remove();

            }

        }*/

//        Toast.makeText(this, "" + cohostNameIdListd.values().toString(), Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onEditingFinished() {

        Log.d("FollowersId", "OnEditing finished");

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(friendsListDisplay.getWindowToken(), 0);
        friendsListDisplay.clearFocus();
    }

    @Override
    public void onProgressUpdate(int percentage) {

        Log.e(TAG, "onProgressUpdate: " + percentage);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Confirm")
                .setContentText("Are you sure you want to skip")
                .setConfirmText("Yes")
                .setCancelText("No")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        float deg = close.getRotation() + 180F;
                        close.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
                        Intent intent = new Intent(CreateEvent.this, Events.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();



    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();


        }
    };

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(TAG, "Google Places API connection suspended.");
    }
}