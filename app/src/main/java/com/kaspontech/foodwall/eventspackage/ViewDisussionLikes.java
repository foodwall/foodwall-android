package com.kaspontech.foodwall.eventspackage;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.adapters.events.eventdiscussion.EventsDiscussLikeAdapter;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventLikes.DiscussionLikesInputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventLikes.DiscussionLikesOututPojo;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewDisussionLikes extends AppCompatActivity implements View.OnClickListener {
    /**
     * Application TAG
     **/
    private static final String TAG = "ViewDisussionLikes";
    /**
     * Action Bar
     **/
    Toolbar actionBar;

    /**
     * Back button
     **/
    ImageButton back;

    /**
     * Likes relative layout
     **/
    RelativeLayout rlLikes;

    /**
     * Likes recyclerview
     **/
    RecyclerView rvLikes;
    /**
     * Linear layout manager
     **/
    LinearLayoutManager llm;
    /**
     * Event Likes adapter and pojo
     **/
    EventsDiscussLikeAdapter likesAdapter;
    DiscussionLikesInputPojo discussionLikesInputPojo;

    /**
     * Loader
     **/
    ProgressBar progressDialogReply;

    /**
     * Tool bar next & title
     **/
    TextView toolbarNext;
    TextView toolbarTitle;

    /**
     * Event Likes arraylist
     **/
    ArrayList<DiscussionLikesInputPojo> likelist = new ArrayList<>();

    /**
     * String variables
     **/
    String firstname;
    String userid;
    String lastname;
    String discussEvntId;
    String userimage;
    String createdBy;
    String createdOn;
    String email;
    String dob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes_layout);

        /*Widget initialization*/
        actionBar = findViewById(R.id.actionbar_like);
        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);


        toolbarNext = actionBar.findViewById(R.id.toolbar_next);
        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarNext.setVisibility(View.GONE);

        rlLikes = findViewById(R.id.rl_likes);
        rvLikes = findViewById(R.id.rv_likes);
        progressDialogReply = findViewById(R.id.progress_dialog_reply);


        likesAdapter = new EventsDiscussLikeAdapter(ViewDisussionLikes.this, likelist);
        llm = new LinearLayoutManager(ViewDisussionLikes.this);
        rvLikes.setLayoutManager(llm);
        rvLikes.setAdapter(likesAdapter);
        rvLikes.setItemAnimator(new DefaultItemAnimator());
        rvLikes.setNestedScrollingEnabled(false);


        /* Getting discussion event id from adapter */

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            discussEvntId = String.valueOf(bundle.getInt("discuss_evnt_id"));

        }


        Log.e(TAG, "onCreate:discuss " + discussEvntId);

        /*Loader*/
        progressDialogReply.setIndeterminate(true);

        /* Calling Likes All API */

        getEventsDiscussionLikesAll();


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:

                finish();

                break;
        }


    }




    //Calling discussion_likes_all API

    private void getEventsDiscussionLikesAll() {

        if (Utility.isConnected(this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                final String createdby = Pref_storage.getDetail(ViewDisussionLikes.this, "userId");

                Call<DiscussionLikesOututPojo> call = apiService.getEventsDiscussLikes("get_events_discussion_likes_all", Integer.parseInt(discussEvntId), Integer.parseInt(createdby));
                call.enqueue(new Callback<DiscussionLikesOututPojo>() {
                    @Override
                    public void onResponse(Call<DiscussionLikesOututPojo> call, Response<DiscussionLikesOututPojo> response) {

                        if (response.body().getResponseCode() == 1) {

                            progressDialogReply.setVisibility(View.GONE);
                            progressDialogReply.setIndeterminate(false);

                            for (int j = 0; j < response.body().getData().size(); j++) {

                                firstname = response.body().getData().get(j).getFirstName();
                                userid = response.body().getData().get(j).getUserId();
                                discussEvntId = response.body().getData().get(j).getLikesDisId();
                                lastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                createdBy = response.body().getData().get(j).getCreatedBy();
                                createdOn = response.body().getData().get(j).getCreatedOn();
                                email = response.body().getData().get(j).getEmail();
                                dob = response.body().getData().get(j).getDob();

                                discussionLikesInputPojo = new DiscussionLikesInputPojo(discussEvntId, createdBy, createdOn,
                                        userid, firstname, lastname, email, "", "", dob, userimage);

                                likelist.add(discussionLikesInputPojo);


                            }

                            likesAdapter.notifyDataSetChanged();


                        }

                    }

                    @Override
                    public void onFailure(Call<DiscussionLikesOututPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Toast.makeText(getApplicationContext(), "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
