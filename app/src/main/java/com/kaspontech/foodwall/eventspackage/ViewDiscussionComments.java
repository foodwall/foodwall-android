package com.kaspontech.foodwall.eventspackage;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.events.eventdiscussion.GetEventsDisCommentsAdapter;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetEventsDisCmtsAllData;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetEventsDisCmtsAllOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewDiscussionComments extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "ViewDiscussionComments";

    //  Actionbar
    Toolbar actionbar;
    ImageButton back;
    ImageButton imgBtnClose;

    // Progress Bar
    ProgressBar progressDialogComment;

    // User profile
    CircleImageView userphotoCommentnew;
    RelativeLayout rlComment;
    RelativeLayout rlPostlayoutnew;
    RelativeLayout rlReplying;
    RelativeLayout rlCommentactivity;

    // Add Comments
    EmojiconEditText edittxtCommentnew;
    TextView txtPostnew;
    TextView txtReplyingTo;

    TextView txtAdapterPostnew;
    TextView tvNoComments;


    // Adapters and arraylist
    GetEventsDisCommentsAdapter getEventsDisCommentsAdapter;
    List<GetEventsDisCmtsAllData> getEventsDisCmtsAllDataArrayList;

    //Main Comments
    RecyclerView rvCommentsnew;
    List<GetEventsDisCmtsAllData> getEventsDisCmtsAllDataList = new ArrayList<>();

    // Vars
    int disId;

    int eventDisCmtId;
    int eventUserId;
    int eventCmtReplyId;
    int clickedforreply;
    int clickedforedit;
    int clickedforeditreply;
    int clickededitreplyid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_discussion_comments);

        actionbar = findViewById(R.id.actionbar_comment);
        back = actionbar.findViewById(R.id.button_back);

        progressDialogComment = findViewById(R.id.progress_dialog_comment);
        userphotoCommentnew = findViewById(R.id.userphoto_commentnew);
        edittxtCommentnew = findViewById(R.id.edittxt_commentnew);
        txtAdapterPostnew = findViewById(R.id.txt_adapter_postnew);
        rvCommentsnew = findViewById(R.id.rv_commentsnew);
        imgBtnClose = findViewById(R.id.img_btn_close);
        imgBtnClose.setOnClickListener(this);


        rlComment = findViewById(R.id.rl_commentnew);
        rlPostlayoutnew = findViewById(R.id.rl_postlayoutnew);
        rlReplying = findViewById(R.id.rl_replying);
        rlCommentactivity = findViewById(R.id.rl_commentactivity);

        userphotoCommentnew = findViewById(R.id.userphoto_commentnew);
        edittxtCommentnew = findViewById(R.id.edittxt_commentnew);

        txtReplyingTo = findViewById(R.id.txt_replying_to);
        txtPostnew = findViewById(R.id.txt_adapter_postnew);
        tvNoComments = findViewById(R.id.tvNoComments);


        txtPostnew.setOnClickListener(this);

        /*Getting Discussion id from multiple actvities / adapter*/

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            disId = bundle.getInt("disId");

        }


        Log.e(TAG, "disId: " + disId);

        /*On click listener*/
        back.setOnClickListener(this);
        txtAdapterPostnew.setOnClickListener(this);

        /*Calling event disscussion comments api*/

        getEventDisComments(disId);

        /*Comments edit text change listener*/

        edittxtCommentnew.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                /*Post text visiblity*/

                if (edittxtCommentnew.getText().toString().trim().length() != 0) {
                    txtAdapterPostnew.setVisibility(View.VISIBLE);
                } else {
                    txtAdapterPostnew.setVisibility(View.GONE);
                }

            }
        });


    }


    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {

            case R.id.img_btn_close:
                /*On reply close press*/
                rlReplying.setVisibility(View.GONE);
                Utility.hideKeyboard(edittxtCommentnew);

                break;


            case R.id.button_back:
                finish();
                break;


            case R.id.txt_adapter_postnew:

                /*Calling event discussion post api call*/
                Utility.hideKeyboard(edittxtCommentnew);

                Log.e(TAG, "clickedforreply-->" + clickedforreply);
                Log.e(TAG, "clickedforedit-->" + clickedforedit);
                Log.e(TAG, "clickedforeditreply-->" + clickedforeditreply);
                Log.e(TAG, "eventDisCmtId-->" + eventDisCmtId);
                Log.e(TAG, "eventCmtReplyId-->" + eventCmtReplyId);

                //Posting reply

                if (clickedforreply == 11) {
                    /* Calling discussion comment reply API */
                    createEventDisCommentReply(eventDisCmtId);

                } else if (clickedforedit == 22) {
                    /* Calling event discussion comment  API */

                    editEventDiscusionComment(eventDisCmtId);

                } else if (clickedforeditreply == 44) {
                    /* Calling event discussion comment reply API */

                    editEventDisCommentReply(eventCmtReplyId, eventDisCmtId);

                } else {
                    /* Calling event discussion comment API */

                    createEventDiscusionComment();
                }


                break;

        }

    }


    // Interface

    /*
     *  GetEventsDisCommentsAdapter
     *
     * */
    public void clicked_username(View view, String username, int position) {


        eventDisCmtId = position;
        rlReplying.setVisibility(View.VISIBLE);
        txtReplyingTo.setText("Replying to ".concat(username));
        edittxtCommentnew.requestFocus();
        Log.e("clicked_reply", "Comment_id" + eventDisCmtId);

    }

    public void clickedcommentposition(View view, int position) {
        eventDisCmtId = position;
    }

    public void clickedforreply(View view, int position) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxtCommentnew, InputMethodManager.SHOW_IMPLICIT);
        clickedforreply = position;
        clickedforedit = 0;
        edittxtCommentnew.setText("");


        Log.e("Vishnu", "clickedforreply" + clickedforreply);

    }

    //edit commment
    public void clickedcommentposition_eventid(View view, String comments, int position, int eventId) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxtCommentnew, InputMethodManager.SHOW_IMPLICIT);

        eventDisCmtId = position;
        this.eventCmtReplyId = eventId;
        edittxtCommentnew.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(comments));
        int pos = edittxtCommentnew.getText().length();
        edittxtCommentnew.setSelection(pos);
        edittxtCommentnew.requestFocus();
    }

    public void clickedfor_edit(View view, int position) {


        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxtCommentnew, InputMethodManager.SHOW_IMPLICIT);
        rlReplying.setVisibility(View.GONE);
        clickedforedit = position;
        clickedforreply = 0;


        Log.e("Vishnu", "clickedforedit" + clickedforedit);


    }


    /*
     *
     * GetEventsDisCmtReplyAdapter
     *
     * */


    //Reply edit edit commment

    public void clickedcommentposition_reply_edit(View view, String comments, int replyid, int cmt_event_id) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxtCommentnew, InputMethodManager.SHOW_IMPLICIT);
        eventDisCmtId = cmt_event_id;
        eventCmtReplyId = replyid;
        edittxtCommentnew.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(comments));
        int pos = edittxtCommentnew.getText().length();
        edittxtCommentnew.setSelection(pos);
        edittxtCommentnew.requestFocus();
    }

    public void clickedfor_replyedit(View view, int position) {

        rlReplying.setVisibility(View.GONE);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxtCommentnew, InputMethodManager.SHOW_IMPLICIT);

        clickedforeditreply = position;
        clickedforedit = 0;

        Log.e("Vishnu", "clickedforeditreply" + clickedforeditreply);

    }

    public void clickedfor_replyid(View view, int position) {

        clickededitreplyid = position;
        clickedforedit = 0;
        clickedforreply = 0;

        Log.e("Vishnu", "clickededitreplyid" + clickededitreplyid);

    }

    public void clickedcomment_userid_position(View view, int position) {
        eventUserId = position;
        Log.e("Vishnu", "clickeduserId" + eventUserId);

    }


    // API for getting all the comments

    private void getEventDisComments(final int disId) {

        if (Utility.isConnected(ViewDiscussionComments.this)) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            int userid = Integer.parseInt(Pref_storage.getDetail(ViewDiscussionComments.this, "userId"));

            Call<GetEventsDisCmtsAllOutput> call = apiService.getEventsDisComments("get_events_discussion_comments_all", disId, userid);

            call.enqueue(new Callback<GetEventsDisCmtsAllOutput>() {
                @Override
                public void onResponse(Call<GetEventsDisCmtsAllOutput> call, Response<GetEventsDisCmtsAllOutput> response) {

                    String responseStatus = response.body().getResponseMessage();

                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                    if (responseStatus.equals("success")) {

                        getEventsDisCmtsAllDataList.clear();

                        //Success
                        getEventsDisCmtsAllDataArrayList = response.body().getData();

                        if (getEventsDisCmtsAllDataArrayList.size() != 0) {

                            tvNoComments.setVisibility(View.GONE);
                            getEventsDisCommentsAdapter = new GetEventsDisCommentsAdapter(ViewDiscussionComments.this, getEventsDisCmtsAllDataArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ViewDiscussionComments.this);
                            rvCommentsnew.setLayoutManager(mLayoutManager);
                            rvCommentsnew.setItemAnimator(new DefaultItemAnimator());
                            rvCommentsnew.setAdapter(getEventsDisCommentsAdapter);
                            rvCommentsnew.hasFixedSize();
                            rvCommentsnew.setNestedScrollingEnabled(false);
                            getEventsDisCommentsAdapter.notifyDataSetChanged();

                        } else {

                            tvNoComments.setVisibility(View.VISIBLE);

                        }


                    }
                }

                @Override
                public void onFailure(Call<GetEventsDisCmtsAllOutput> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                    tvNoComments.setVisibility(View.VISIBLE);
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }}else {

            Toast.makeText(this, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

        }
    }

    /* create event discusion comment */

    private void createEventDiscusionComment() {

        if (edittxtCommentnew.getText().toString().length() != 0) {

            if (Utility.isConnected(getApplicationContext())) {

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(ViewDiscussionComments.this, "userId"));

                    Call<CommonOutputPojo> call = apiService.createEditEventDiscussionComment("create_edit_delete_events_discussion_comments", 0, disId, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtCommentnew.getText().toString().trim()), userid, 0);

                    call.enqueue(new Callback<CommonOutputPojo>() {
                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                getEventDisComments(disId);

                                edittxtCommentnew.setText("");
                                //Success


                            }
                        }

                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "FailureError" + t.getMessage());
                        }
                    });

                } catch (Exception e) {

                    e.printStackTrace();

                }
            } else {

                Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

            }

        } else {

            edittxtCommentnew.setError(getString(R.string.enter_text));
        }

    }

    /* edit event discusion comment api calling*/

    private void editEventDiscusionComment(int eventDisCmmtId) {

        if (edittxtCommentnew.getText().toString().length() != 0) {


            if (Utility.isConnected(getApplicationContext())) {

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(ViewDiscussionComments.this, "userId"));

                    Call<CommonOutputPojo> call = apiService.createEditEventDiscussionComment("create_edit_delete_events_discussion_comments", eventDisCmmtId, disId, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtCommentnew.getText().toString().trim()), userid, 0);

                    call.enqueue(new Callback<CommonOutputPojo>() {
                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                /*Resuming API*/
                                getEventDisComments(disId);

                                edittxtCommentnew.setText("");
                                //Success
                                eventDisCmtId = 0;
                                eventCmtReplyId = 0;
                                clickedforedit = 0;


                            }
                        }

                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "FailureError" + t.getMessage());
                        }
                    });

                } catch (Exception e) {

                    e.printStackTrace();

                }


            } else {

                Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

            }

        } else {

            edittxtCommentnew.setError("Enter your text");
        }
    }

    /* create event discusion comment reply */

    private void createEventDisCommentReply(int eventDisCmmtId) {


        if (edittxtCommentnew.getText().toString().length() != 0) {
            if (Utility.isConnected(getApplicationContext())) {
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(ViewDiscussionComments.this, "userId"));

                    Call<CommonOutputPojo> call = apiService.createEditEventDiscussionCommentReply("create_edit_delete_events_discussion_comments_reply", 0, eventDisCmmtId, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtCommentnew.getText().toString().trim()), userid, 0);

                    call.enqueue(new Callback<CommonOutputPojo>() {
                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {
                                /*Resuming API*/
                                getEventDisComments(disId);

                                edittxtCommentnew.setText("");
                                rlReplying.setVisibility(View.GONE);

                                eventDisCmtId = 0;
                                clickedforreply = 0;
                                //Success


                            }
                        }

                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "FailureError" + t.getMessage());
                        }
                    });

                } catch (Exception e) {

                    e.printStackTrace();

                }
            } else {

                Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

            }


        } else {

            edittxtCommentnew.setError("Enter your text");
        }

    }

    /* edit event discusion comment reply */

    private void editEventDisCommentReply(int eventCmmtReplyId, int eventDisCmmtId) {

        Log.e(TAG, "editEventDisCommentReply->ReplyId: " + eventCmmtReplyId);
        Log.e(TAG, "editEventDisCommentReply->cmmt_dis_id: " + eventDisCmmtId);

        if (edittxtCommentnew.getText().toString().length() != 0) {
            if (Utility.isConnected(getApplicationContext())) {

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(ViewDiscussionComments.this, "userId"));

                    Call<CommonOutputPojo> call = apiService.createEditEventDiscussionCommentReply("create_edit_delete_events_discussion_comments_reply", eventCmmtReplyId, eventDisCmmtId, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtCommentnew.getText().toString().trim()), userid, 0);

                    call.enqueue(new Callback<CommonOutputPojo>() {
                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                /*Resuming API*/

                                getEventDisComments(disId);

                                edittxtCommentnew.setText("");
                                //Success
                                clickedforeditreply = 0;
                                eventCmtReplyId = 0;
                                eventDisCmtId = 0;

                            }
                        }

                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "FailureError" + t.getMessage());
                        }
                    });

                } catch (Exception e) {

                    e.printStackTrace();

                }

            } else {

                Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

            }


        } else {

            edittxtCommentnew.setError("Enter your text");
        }


    }


}
