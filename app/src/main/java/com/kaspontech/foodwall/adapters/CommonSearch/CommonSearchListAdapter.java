package com.kaspontech.foodwall.adapters.CommonSearch;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.commonSearchView.CommonSearch;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchList.GetCommonSearchTypeData;

import java.util.ArrayList;
import java.util.List;

public class CommonSearchListAdapter extends RecyclerView.Adapter<CommonSearchListAdapter.MyViewHolder>  implements Filterable {


    private Context context;
    private List<GetCommonSearchTypeData> getCommonSearchTypeDataList;
    private List<GetCommonSearchTypeData> contactListFiltered;

    /*private List<GetCommonSearchTypeData> getCommonSearchTypeDataList = new ArrayList<>();
    private GetCommonSearchTypeData getCommonSearchTypeData;*/

    CommonSearch commonSearch;


    public CommonSearchListAdapter(Context context, List<GetCommonSearchTypeData> getCommonSearchTypeDataList) {
        this.context = context;
        this.getCommonSearchTypeDataList = getCommonSearchTypeDataList;
        this.contactListFiltered = getCommonSearchTypeDataList;
        commonSearch = (CommonSearch) context;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = getCommonSearchTypeDataList;
                } else {
                    List<GetCommonSearchTypeData> filteredList = new ArrayList<>();
                    for (GetCommonSearchTypeData row : getCommonSearchTypeDataList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getSearchtext().toLowerCase().contains(charString.toLowerCase()) || row.getSearchtype().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<GetCommonSearchTypeData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        LinearLayout ll_search_results;
        TextView search_text, search_type;
        CommonSearch commonSearch;


        public MyViewHolder(View view,CommonSearch commonSearch) {
            super(view);

            this.commonSearch = commonSearch;
            search_text = (TextView) view.findViewById(R.id.search_text);
            search_type = (TextView) view.findViewById(R.id.search_type);
            ll_search_results = (LinearLayout)view.findViewById(R.id.ll_search_results);
            ll_search_results.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {


            if(contactListFiltered != null){

                String searchText = contactListFiltered.get(getAdapterPosition()).getSearchtext();
                String searchType = contactListFiltered.get(getAdapterPosition()).getSearchtype();

                if(searchText != null && searchType != null){

                    commonSearch.getSelectedSearchResults(view,searchText, searchType);

                }

            }

        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_common_search, parent, false);
        return new MyViewHolder(itemView,commonSearch);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {


        String results =  contactListFiltered.get(position).getSearchtext();

        String type = contactListFiltered.get(position).getSearchtype();

        String next = "<font color='#4aa3df'>"+type+"</font>";

        holder.search_text.setText(Html.fromHtml(results + " - " +next ));

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }


}