package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;

public class Following_timelineAdapter extends RecyclerView.ViewHolder {
    //TextView
    TextView txt_following_text, txt_following_user_name, txt_user_name, txt_follow_ago;
    //ImageView
    ImageView following_user_photo;

    //Relativelayout
    RelativeLayout rl_following_you;

    public Following_timelineAdapter(View view) {
        super(view);

        //TextView
        txt_following_text = (TextView) itemView.findViewById(R.id.txt_following_text);
        txt_user_name = (TextView) itemView.findViewById(R.id.txt_user_name);
        txt_follow_ago = (TextView) itemView.findViewById(R.id.txt_follow_ago);
        txt_following_user_name = (TextView) itemView.findViewById(R.id.txt_following_user_name);

        //Imageview
        following_user_photo = (ImageView) itemView.findViewById(R.id.following_user_photo);

        //RelativeLayout

        rl_following_you = (RelativeLayout) itemView.findViewById(R.id.rl_following_you);

    }
}
