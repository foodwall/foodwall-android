package com.kaspontech.foodwall.adapters.Follow_following;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.FindFriends.GetFindFriends;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FindFriendsAdapter extends RecyclerView.Adapter<FindFriendsAdapter.MyViewHolder>
        implements RecyclerView.OnItemTouchListener, Filterable {


    private static final String TAG = "FindFriendsAdapter";
    private Context context;

//    private List<GetFindFriends> gettingfollowerslist = new ArrayList<>();
//    private List<GetFindFriends> gettingfollowerslistfiltered = new ArrayList<>();


    private List<GetFindFriends> gettingfollowerslist;
    private List<GetFindFriends> gettingfollowerslistfiltered;

    private FriendsAdapterListener friendsAdapterListener;


    private Pref_storage pref_storage;

    public interface FriendsAdapterListener {
        void onFollowed();
    }

    public FindFriendsAdapter(Context c, List<GetFindFriends> contactList, FriendsAdapterListener friendsAdapterListener) {
        this.context = c;
        this.friendsAdapterListener = friendsAdapterListener;
        this.gettingfollowerslist = contactList;
        this.gettingfollowerslistfiltered = contactList;
    }

    @NonNull
    @Override
    public FindFriendsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_followerlist, parent, false);
        // set the view's size, margins, paddings and layout parameters
        FindFriendsAdapter.MyViewHolder vh = new FindFriendsAdapter.MyViewHolder(v);

        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final FindFriendsAdapter.MyViewHolder holder, final int position) {


        holder.txt_username.setText(gettingfollowerslistfiltered.get(position).getFirstName());
        holder.txt_username.setTextColor(Color.parseColor("#000000"));

        holder.txt_userlastname.setText(gettingfollowerslistfiltered.get(position).getLastName());
        holder.txt_userlastname.setTextColor(Color.parseColor("#000000"));

        String fullname = gettingfollowerslistfiltered.get(position).getFirstName().concat(" ").concat(gettingfollowerslistfiltered.get(position).getLastName());

        holder.txt_fullname_user.setText(fullname);

        String ownuser_id = Pref_storage.getDetail(context, "userId");



        // Public account & Private Account

        String followerId = gettingfollowerslistfiltered.get(position).getFollowerId();
        String profileStatus = gettingfollowerslistfiltered.get(position).getProfileStatus();
        String reqFollowerId = gettingfollowerslistfiltered.get(position).getReqFollowerId();

        if(followerId.equals("0") && profileStatus.equals("0")){

            holder.txt_follow.setText(R.string.follow);
            holder.txt_follow.setTextColor(Color.WHITE);
            holder.rl_follow.setBackgroundResource(R.drawable.button_login_background);

        }else if(!followerId.equals("0") && profileStatus.equals("0")){

            holder.rl_follow.setBackgroundResource(R.drawable.editext_login_background);
            holder.txt_follow.setTextColor(Color.BLACK);
            holder.txt_follow.setText(R.string.following);

        }else if(followerId.equals("0") && profileStatus.equals("1") && reqFollowerId.equals("0")){

            holder.txt_follow.setText(R.string.follow);
            holder.txt_follow.setTextColor(Color.WHITE);
            holder.rl_follow.setBackgroundResource(R.drawable.button_login_background);

        }else if(followerId.equals("0") && profileStatus.equals("1") && !reqFollowerId.equals("0")){

            holder.txt_follow.setText(R.string.requested);
            holder.txt_follow.setTextColor(Color.BLACK);
            holder.rl_follow.setBackgroundResource(R.drawable.editext_login_background);


        }else if(!followerId.equals("0") && profileStatus.equals("1") && reqFollowerId.equals("0")){


            holder.txt_follow.setText(R.string.following);
            holder.txt_follow.setTextColor(Color.BLACK);
            holder.rl_follow.setBackgroundResource(R.drawable.editext_login_background);


        }




        GlideApp.with(context)
                .load(gettingfollowerslistfiltered.get(position).getPicture())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_add_photo)
                .thumbnail(0.1f)
                .into(holder.img_follower);


        holder.rl_follower_ayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String user_id = gettingfollowerslistfiltered.get(position).getUserId();

                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    context.startActivity(new Intent(context, Profile.class));

                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }

            }
        });


        //Call API follower

        holder.rl_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String status = holder.txt_follow.getText().toString();

                Log.e(TAG, "onClick: "+status );

                String following = context.getResources().getString(R.string.following);
                String follow = context.getResources().getString(R.string.follow);
                String requested = context.getResources().getString(R.string.requested);

                if (status.equals(following) || status.equals(requested)) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        String createdby = Pref_storage.getDetail(context, "userId");

                        Call<Get_following_output_Pojo> call = apiService.create_follower("create_follower", Integer.parseInt(createdby), Integer.parseInt(gettingfollowerslistfiltered.get(position).getUserId()), 0);
                        call.enqueue(new Callback<Get_following_output_Pojo>() {
                            @Override
                            public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {


                                if (response.body().getResponseMessage().equals("success")) {


                                    for (int j = 0; j < response.body().getData().size(); j++) {

                                        holder.txt_follow.setText(R.string.follow);
                                        holder.txt_follow.setTextColor(Color.WHITE);
                                        holder.rl_follow.setBackgroundResource(R.drawable.button_login_background);

                                        friendsAdapterListener.onFollowed();
                                    }


                                }

                            }

                            @Override
                            public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (status.equals(follow)) {

                    if (Integer.parseInt(gettingfollowerslistfiltered.get(position).getProfileStatus()) == 0) {

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");

                            Call<Get_following_output_Pojo> call = apiService.create_follower("create_follower", Integer.parseInt(createdby), Integer.parseInt(gettingfollowerslistfiltered.get(position).getUserId()), 1);
                            call.enqueue(new Callback<Get_following_output_Pojo>() {
                                @Override
                                public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {


                                    if (response.body().getResponseMessage().equals("success")) {


                                        for (int j = 0; j < response.body().getData().size(); j++) {

                                            holder.rl_follow.setBackgroundResource(R.drawable.editext_login_background);
                                            holder.txt_follow.setTextColor(Color.BLACK);
                                            holder.txt_follow.setText(R.string.following);
                                            friendsAdapterListener.onFollowed();
                                        }


                                    }

                                }

                                @Override
                                public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (Integer.parseInt(gettingfollowerslistfiltered.get(position).getProfileStatus()) == 1) {

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");

                            Call<CommonOutputPojo> call = apiService.create_follower_request("create_follower_request", Integer.parseInt(createdby), Integer.parseInt(gettingfollowerslistfiltered.get(position).getUserId()), 1);
                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                    if (response.body().getResponseMessage().equals("success")) {


                                        holder.txt_follow.setText(R.string.requested);
                                        friendsAdapterListener.onFollowed();


                                    }

                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }


            }
        });


    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    gettingfollowerslistfiltered = gettingfollowerslist;
                } else {
                    List<GetFindFriends> filteredList = new ArrayList<>();
                    for (GetFindFriends row : gettingfollowerslist) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getFirstName().toLowerCase().contains(charString.toLowerCase()) || row.getLastName().contains(charSequence)) {
                            filteredList.add(row);
                        } else {
                            Log.e(TAG, "performFiltering: Not matched");
                        }
                    }

                    gettingfollowerslistfiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = gettingfollowerslistfiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                gettingfollowerslistfiltered = (ArrayList<GetFindFriends>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    @Override
    public int getItemCount() {

        return gettingfollowerslistfiltered.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_username, txt_userlastname, txt_fullname_user, txt_follow, share, caption, captiontag, txt_created_on, txt_adapter_post;
        RelativeLayout rl_follow, rl_main_layout, rl_follower_ayout;
        ImageView img_follower;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_userlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txt_fullname_user = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            txt_follow = (TextView) itemView.findViewById(R.id.txt_follow);
            img_follower = (ImageView) itemView.findViewById(R.id.img_follower);
            rl_follow = (RelativeLayout) itemView.findViewById(R.id.rl_follow);
            rl_main_layout = (RelativeLayout) itemView.findViewById(R.id.rl_main_layout);
            rl_follower_ayout = (RelativeLayout) itemView.findViewById(R.id.rl_follower_ayout);


        }


    }


}

