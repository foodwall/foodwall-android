package com.kaspontech.foodwall.adapters.HistoricalMap;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Image;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Utility;


import java.util.ArrayList;
import java.util.List;

//

public class HistoryImagesAdapter  extends PagerAdapter implements View.OnTouchListener {


    List<Image> imagesEncodedList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
    private PhotoView imageView;
    private ImageButton remove_image;
    private PhotoRemovedListener photoRemovedListener;

    public interface PhotoRemovedListener {

         void removedImageLisenter(List<Image> imagesList,int img_id);

    }

    public HistoryImagesAdapter(Context context, List<Image> imagesEncodedList,PhotoRemovedListener photoRemovedListener) {
        this.context = context;
        this.imagesEncodedList = imagesEncodedList;
        this.photoRemovedListener = photoRemovedListener;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(Object object){
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public int getCount() {
        return imagesEncodedList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.slide_image_layout, view, false);

        assert imageLayout != null;
        imageView = (PhotoView) imageLayout.findViewById(R.id.image);
        remove_image = (ImageButton) imageLayout.findViewById(R.id.remove_image);

        remove_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure want to remove this image?");

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        photoRemovedListener.removedImageLisenter(imagesEncodedList, position);

                       /* imagesEncodedList.remove(position);
                        notifyDataSetChanged();*/
                        dialog.dismiss();

                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                builder.show();


            }
        });

//        Utility.picassoImageLoader(imagesEncodedList.get(position).replace("[", "").replace("]", ""),
//                1,imageView);


        GlideApp.with(context)
                .load(imagesEncodedList.get(position).getImg().replace("[", "").replace("]", ""))
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(imageView);


        view.addView(imageLayout, 0);


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

}