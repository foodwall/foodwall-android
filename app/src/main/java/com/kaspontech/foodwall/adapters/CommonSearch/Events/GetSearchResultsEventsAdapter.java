package com.kaspontech.foodwall.adapters.CommonSearch.Events;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.eventspackage.SelectFollowers;
import com.kaspontech.foodwall.eventspackage.ViewEvent;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchEventPojo;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetSearchResultsEventsAdapter extends RecyclerView.Adapter<GetSearchResultsEventsAdapter.MyViewHolder> {


    Context context;
    List<SearchEventPojo> searchEventPojoList = new ArrayList<>();
    SearchEventPojo searchEventPojo;
    boolean interest_flag = false;
    private static int userInterestedFlag = 0;
    BottomSheetDialog dialog;

    public GetSearchResultsEventsAdapter(Context context, List<SearchEventPojo> searchEventPojoList) {
        this.context = context;
        this.searchEventPojoList = searchEventPojoList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView eventBackground;
        ImageButton interested, going, share;
        LinearLayout viewEvent, ll_interested, ll_going, ll_share;
        TextView date, location, eventName, interestedText, goingText, shareText;

        public MyViewHolder(View view) {
            super(view);

            eventBackground = (ImageView) view.findViewById(R.id.event_background);

            interested = (ImageButton) view.findViewById(R.id.interest_button);
            going = (ImageButton) view.findViewById(R.id.going_button);
            share = (ImageButton) view.findViewById(R.id.share_button);

            date = (TextView) view.findViewById(R.id.event_date);
            location = (TextView) view.findViewById(R.id.event_location);
            eventName = (TextView) view.findViewById(R.id.event_name);

            interestedText = (TextView) view.findViewById(R.id.interest_text);
            goingText = (TextView) view.findViewById(R.id.going_text);
            shareText = (TextView) view.findViewById(R.id.share_text);

            viewEvent = (LinearLayout) view.findViewById(R.id.ll_view_event);
            ll_interested = (LinearLayout) view.findViewById(R.id.ll_interested);
            ll_going = (LinearLayout) view.findViewById(R.id.ll_going);
            ll_share = (LinearLayout) view.findViewById(R.id.ll_share);

        }

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_get_all_events, parent, false);
        return new MyViewHolder(itemView);    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder,final int position) {

        Log.e("GetPublicEventsAdapter", "GetPublicEventsAdapter ->" + searchEventPojoList.get(position).getEventName());

        searchEventPojo = searchEventPojoList.get(position);


        String eventDate = getMonthName(searchEventPojo.getStartDate()) + " " + getDate(searchEventPojo.getStartDate());


        Utility.picassoImageLoader(searchEventPojo.getEventImage(),1,holder.eventBackground, context);
        /*GlideApp
                .with(context)
                .load(searchEventPojo.getEventImage())
                .centerCrop()
                .into(holder.eventBackground);*/

        holder.date.setText(eventDate);

        holder.eventName.setText(searchEventPojo.getEventName());

        holder.location.setText(searchEventPojo.getLocation());

        /*if(searchEventPojo.getShareType() == 0){

            holder.ll_share.setVisibility(View.GONE);

        }else{

            holder.ll_share.setVisibility(View.VISIBLE);

        }*/


        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SelectFollowers.class);
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", searchEventPojo.getEventId());
                intent.putExtras(bundle);
                context.startActivity(intent);


            }
        });

        holder.viewEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SearchEventPojo searchEventPojo = searchEventPojoList.get(position);

                Intent intent = new Intent(context, ViewEvent.class);
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", searchEventPojoList.get(position).getEventId());
                intent.putExtras(bundle);
                context.startActivity(intent);



            }
        });


        if (searchEventPojoList.get(position).getEvtLikes() == 1) {

            //Success

            holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_pressed));
//                            interest_flag = true;
            searchEventPojoList.get(position).setUserInterested(true);


        }


        if (searchEventPojoList.get(position).getGngLikes() == 1) {

            //Success

            holder.going.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_going));
//                            interest_flag = true;
            searchEventPojoList.get(position).setUserGoing(true);
            holder.ll_interested.setVisibility(View.GONE);


        }

       /* if (searchEventPojoList.get(position).getEventId() == searchEventPojoList.get(position).getEventId()) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                int event_id = searchEventPojoList.get(position).getEventId();

                Call<GetEventLikesUserPojo> call = apiService.getEventsLikesUser("get_events_likes_user", userid, event_id);

                call.enqueue(new Callback<GetEventLikesUserPojo>() {
                    @Override
                    public void onResponse(Call<GetEventLikesUserPojo> call, Response<GetEventLikesUserPojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("getUserInterest", "responseStatus->" + responseStatus);


                        if (responseStatus.equals("success")) {

                            //Success

                            holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_pressed));
//                            interest_flag = true;
                            searchEventPojoList.get(position).isUserInterested(true);


                        } else if (responseStatus.equals("nodata")) {


                        }
                    }

                    @Override
                    public void onFailure(Call<GetEventLikesUserPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "FailureError" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }


        }

*/
        holder.interested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (searchEventPojoList.get(position).isUserInterested()) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<CommonOutputPojo> call = apiService.createEventsLikes("create_events_likes", searchEventPojoList.get(position).getEventId(), 0, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("nodata")) {

                                    //Success
                                    holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star));
                                    searchEventPojoList.get(position).setUserInterested(false);
                                    interest_flag = false;


                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }


                } else if (!searchEventPojoList.get(position).isUserInterested()) {


                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<CommonOutputPojo> call = apiService.createEventsLikes("create_events_likes", searchEventPojoList.get(position).getEventId(), 1, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                String responseStatus = response.body().getResponseMessage();


                                if (responseStatus.equals("success")) {

                                    //Success

                                    holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_pressed));
                                    interest_flag = true;
                                    searchEventPojoList.get(position).setUserInterested(true);
                                    Toast.makeText(context, "Your interest has been saved", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }


                }

            }
        });


        holder.going.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (searchEventPojoList.get(position).isUserGoing()) {

                    View myView = LayoutInflater.from(context).inflate(R.layout.alert_create_events, null);
                    dialog = new BottomSheetDialog(context);
                    dialog.setContentView(myView);

                    LinearLayout interested = (LinearLayout) dialog.findViewById(R.id.ll_private_event);
                    LinearLayout not_going = (LinearLayout) dialog.findViewById(R.id.ll_public_event);


                    interested.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", searchEventPojoList.get(position).getEventId(), 0, userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("nodata")) {

                                            //Success
                                            holder.going.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_not_going));
                                            searchEventPojoList.get(position).setUserGoing(false);
                                            interest_flag = false;


                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                    }
                                });

                            } catch (Exception e) {

                                e.printStackTrace();

                            }


                            if (searchEventPojoList.get(position).isUserInterested()) {

                                holder.ll_interested.setVisibility(View.VISIBLE);
                                dialog.dismiss();

                            } else {

                                if (!searchEventPojoList.get(position).isUserInterested()) {


                                    ApiInterface apiService1 = ApiClient.getClient().create(ApiInterface.class);

                                    try {

                                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                        Call<CommonOutputPojo> call = apiService1.createEventsLikes("create_events_likes", searchEventPojoList.get(position).getEventId(), 1, userid);

                                        call.enqueue(new Callback<CommonOutputPojo>() {
                                            @Override
                                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                String responseStatus = response.body().getResponseMessage();


                                                if (responseStatus.equals("success")) {

                                                    //Success

                                                    holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_pressed));
                                                    interest_flag = true;
                                                    searchEventPojoList.get(position).setUserInterested(true);
                                                    holder.ll_interested.setVisibility(View.VISIBLE);
                                                    dialog.dismiss();
                                                    Toast.makeText(context, "Your interest has been saved", Toast.LENGTH_SHORT).show();

                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                //Error
                                                Log.e("FailureError", "FailureError" + t.getMessage());
                                                dialog.dismiss();

                                            }
                                        });

                                    } catch (Exception e) {

                                        e.printStackTrace();

                                    }


                                }


                            }

                        }
                    });

                    not_going.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", searchEventPojoList.get(position).getEventId(), 0, userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("nodata")) {

                                            //Success
                                            holder.going.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_not_going));
                                            searchEventPojoList.get(position).setUserGoing(false);
                                            interest_flag = false;
                                            holder.ll_interested.setVisibility(View.VISIBLE);
                                            dialog.dismiss();


                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                    }
                                });

                            } catch (Exception e) {

                                e.printStackTrace();

                            }


                        }
                    });

                    dialog.show();


                } else if (!searchEventPojoList.get(position).isUserGoing()) {


                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", searchEventPojoList.get(position).getEventId(), 1, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                String responseStatus = response.body().getResponseMessage();


                                if (responseStatus.equals("success")) {

                                    //Success

                                    holder.going.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_going));
                                    interest_flag = true;
                                    searchEventPojoList.get(position).setUserGoing(true);
                                    Toast.makeText(context, "You are going to this event", Toast.LENGTH_SHORT).show();
                                    holder.ll_interested.setVisibility(View.GONE);

                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }


                }

            }
        });


    }

    private String getMonthName(String date) {

        String monthName = "";

        if(date != null){

            String month = date.substring(5, 7);

            switch (month) {


                case "01":
                    monthName = "JAN";
                    break;

                case "02":
                    monthName = "FEB";
                    break;

                case "03":
                    monthName = "MAR";
                    break;

                case "04":
                    monthName = "APR";
                    break;

                case "05":
                    monthName = "MAY";
                    break;

                case "06":
                    monthName = "JUN";
                    break;

                case "07":
                    monthName = "JUL";
                    break;

                case "08":
                    monthName = "AUG";
                    break;

                case "09":
                    monthName = "SEP";
                    break;

                case "10":
                    monthName = "OCT";
                    break;

                case "11":
                    monthName = "NOV";
                    break;

                case "12":
                    monthName = "DEC";
                    break;
            }

        }



        return monthName;
    }

    private String getDate(String date) {

        String day = date.substring(8, 10);

        return day;
    }


    @Override
    public int getItemCount() {
        return searchEventPojoList.size();
    }


}
