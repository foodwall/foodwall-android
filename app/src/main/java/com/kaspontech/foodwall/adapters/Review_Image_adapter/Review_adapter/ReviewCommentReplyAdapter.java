package com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Review_reply_all_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
//
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewCommentReplyAdapter extends RecyclerView.Adapter<ReviewCommentReplyAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    Context context;
    Review_reply_all_pojo getReplyAllPojo;
    private  ArrayList<Review_reply_all_pojo> gettingreplyalllist = new ArrayList<>();

    //Relative layout
    public RelativeLayout total_reply_layout;

    private Pref_storage pref_storage;
    Reviews_comments_all_activity commentsActivity;

    Review_comments_adapter reviewCommentsAdapter;

    public ReviewCommentReplyAdapter(Context c, ArrayList<Review_reply_all_pojo> gettinglist) {
        this.context = c;
        this.gettingreplyalllist = gettinglist;
        this.commentsActivity = (Reviews_comments_all_activity)context;


    }

    @NonNull
    @Override
    public ReviewCommentReplyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_reply_comments, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ReviewCommentReplyAdapter.MyViewHolder vh = new ReviewCommentReplyAdapter.MyViewHolder(v,commentsActivity,reviewCommentsAdapter);
//        vh.setIsRecyclable(false);
        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ReviewCommentReplyAdapter.MyViewHolder holder, final int position) {

        getReplyAllPojo = gettingreplyalllist.get(position);
        String firstname = gettingreplyalllist.get(position).getFirstName().replace("\"", "");
        String lastname = gettingreplyalllist.get(position).getLastName().replace("\"", "");

        String username = firstname+" "+lastname;
        holder.txt_username_reply.setText(username);

        holder.txt_username_reply.setTextColor(Color.parseColor("#000000"));

        holder.txt_comments_reply.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettingreplyalllist.get(position).getReply().replace("\"", "")));

        holder.txt_comments_reply.setTextColor(Color.parseColor("#000000"));


        // Edit visibility
        if (gettingreplyalllist.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
            holder.txt_comment_edit.setVisibility(View.VISIBLE);
        } else {
            holder.txt_comment_edit.setVisibility(View.GONE);
            holder.del_reply_comment.setVisibility(View.GONE);
        }


        // Loading image url
//        Utility.picassoImageLoader(gettingreplyalllist.get(position).getPicture(),
//                1,holder.img_comment_reply, context);

        GlideApp.with(context)
                .load(gettingreplyalllist.get(position).getPicture())
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)
                .into(holder.img_comment_reply);


        // Posted date functionality

        String CreatedOn = gettingreplyalllist.get(position).getCreatedOn();

        Utility.setTimeStamp(CreatedOn, holder.txt_ago_reply);



          /*  try {
                //Input time
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                long time = sdf.parse(CreatedOn).getTime();

                //Cuurent system time

                Date currentTime = Calendar.getInstance().getTime();
                SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String now_new= sdformat.format(currentTime);
                long nowdate = sdformat.parse(now_new).getTime();


                CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(time, nowdate, DateUtils.FORMAT_ABBREV_RELATIVE);

                if (relavetime1.toString().equalsIgnoreCase("0 minutes ago") ||relavetime1.toString().equalsIgnoreCase("In 0 minutes") ) {
                    holder.txt_ago_reply.setText(R.string.just_now);
                } else if (relavetime1.toString().contains("hours ago")) {
                    holder.txt_ago_reply.setText(relavetime1.toString());
                } else if (relavetime1.toString().contains("days ago")) {
                    holder.txt_ago_reply.setText(relavetime1.toString());
                } else {
                    holder.txt_ago_reply.append(relavetime1 + "");
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
*/



        holder.txt_comment_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                commentsActivity.clickedcommentposition_reply_edit(v,gettingreplyalllist.get(position).getReply(),Integer.parseInt(gettingreplyalllist.get(position).getReplyId()),Integer.parseInt(gettingreplyalllist.get(position).getCmmtHtlId()));
                commentsActivity.clickedfor_replyedit( v, 44);
                commentsActivity.clickedcommentposition(v, Integer.parseInt(gettingreplyalllist.get(position).getCmmtHtlId()));
                commentsActivity.clickedcomment_userid_position(v, Integer.parseInt(gettingreplyalllist.get(position).getUserId()));
                commentsActivity.clickedfor_replyid(v, Integer.parseInt(gettingreplyalllist.get(position).getReplyId()));


            }
        });


        holder.del_reply_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                commentsActivity.clickedfordelete();

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this comment?")
                        .setContentText("Won't be able to recover this comment anymore!")
                        .setConfirmText("Delete")
                        .setCancelText("Cancel")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                try {

                                    String createuserid = gettingreplyalllist.get(position).getUserId();
                                    String timeline_commentid = gettingreplyalllist.get(position).getCmmtHtlId();
                                    String commentid = gettingreplyalllist.get(position).getReplyId();

                                    Call<CommonOutputPojo> call = apiService.create_delete_hotel_review_comments_reply("create_delete_hotel_review_comments_reply", Integer.parseInt(commentid), Integer.parseInt(timeline_commentid), Integer.parseInt(createuserid));
                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                            if (response.body().getResponseCode()==1) {
                                                removeAt(position);


                                                Toast.makeText(context, "Deleted!!", Toast.LENGTH_SHORT).show();

//                                                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//                                                        .setTitleText("Deleted!!")
////                                                        .setContentText("Your comment has been deleted.")
//                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                                            @Override
//                                                            public void onClick(SweetAlertDialog sDialog) {
//                                                                sDialog.dismissWithAnimation();
//
//                                                            }
//                                                        })
//                                                        .show();


                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error


                                            Log.e("FailureError", "" + t.getMessage());
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }
                        })
                        .show();


            }
        });




    }

    private void removeAt(int position) {
        gettingreplyalllist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, gettingreplyalllist.size());
    }

    @Override
    public int getItemCount() {

        Log.e("Replysize", "getItemCount: "+gettingreplyalllist.size() );


        if(gettingreplyalllist.size()==0){

            commentsActivity.check_review_reply_size( gettingreplyalllist.size());
            }else {

        }



        return gettingreplyalllist.size();

    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view_reply;
        ImageView img_comment_reply;
        ImageButton del_reply_comment;
        LinearLayout rl_reply_comment_layout;

        TextView txt_username_reply, txt_comment_edit,
                txt_ago_reply, txt_reply_like, txt_comment_reply,txt_comments_reply, txt_view_all_replies, txt_replies_count;


        Reviews_comments_all_activity comments_activity;

        Review_comments_adapter review_comments_adapter;

        public MyViewHolder(View itemView, Reviews_comments_all_activity commentsActivity,Review_comments_adapter reviewCommentsAdapter1) {
            super(itemView);

            this.comments_activity = commentsActivity;
            review_comments_adapter = reviewCommentsAdapter1;

            view_reply = (View) itemView.findViewById(R.id.view_reply);

            txt_ago_reply = (TextView) itemView.findViewById(R.id.txt_ago_reply);
            txt_reply_like = (TextView) itemView.findViewById(R.id.txt_reply_like);
            txt_comment_edit = (TextView) itemView.findViewById(R.id.txt_comment_edit);
            txt_replies_count = (TextView) itemView.findViewById(R.id.txt_replies_count);
            txt_comment_reply = (TextView) itemView.findViewById(R.id.txt_comment_reply);
            txt_username_reply = (TextView) itemView.findViewById(R.id.txt_username_reply);
            txt_comments_reply = (TextView) itemView.findViewById(R.id.txt_comments_reply);
            txt_view_all_replies = (TextView) itemView.findViewById(R.id.txt_view_all_replies);


            img_comment_reply = (ImageView) itemView.findViewById(R.id.img_comment_reply);
            del_reply_comment = (ImageButton) itemView.findViewById(R.id.del_reply_comment);

            total_reply_layout = (RelativeLayout) itemView.findViewById(R.id.total_reply_layout);

            rl_reply_comment_layout = (LinearLayout) itemView.findViewById(R.id.rl_reply_comment_layout);


        }


        @Override
        public void onClick(View v) {

        }

    }


}