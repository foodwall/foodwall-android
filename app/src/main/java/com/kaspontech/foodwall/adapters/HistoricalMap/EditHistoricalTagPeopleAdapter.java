package com.kaspontech.foodwall.adapters.HistoricalMap;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kaspontech.foodwall.historicalMapPackage.EditHistoryMap;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditHistoricalTagPeopleAdapter  extends RecyclerView.Adapter<EditHistoricalTagPeopleAdapter.MyViewHolder>
        implements Filterable {


    private Context context;
    private List<GetFollowersData> contactList;
    private List<GetFollowersData> contactListFiltered;
    private ContactsAdapterListener listener;
    HashMap<Integer,String> cohostNameIdList = new HashMap<>();


    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView followerPhoto;
        TextView userName;
        CheckBox selectFollowerBox;

        public MyViewHolder(View view) {
            super(view);

            followerPhoto = (CircleImageView)view.findViewById(R.id.user_image);
            userName = (TextView)view.findViewById(R.id.user_name);
            selectFollowerBox = (CheckBox)view.findViewById(R.id.followers_checked);


            /*selectFollowerBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    *//*String followerName;
                    followerName = contactListFiltered.get(getAdapterPosition()).getFirstName() + " " + contactListFiltered.get(getAdapterPosition()).getLastName();

                    if (selectFollowerBox.isChecked()) {

                        if (cohostNameIdList.containsValue(Integer.valueOf(contactListFiltered.get(getAdapterPosition()).getFollowerId()))) {

                            return;

                        } else {

                            cohostNameIdList.put(followerName, Integer.valueOf(contactListFiltered.get(getAdapterPosition()).getFollowerId()));

                        }


                    } else {

                        cohostNameIdList.remove(followerName);

                    }*//*



                    listener.onContactSelected( selectFollowerBox,contactListFiltered.get(getAdapterPosition()));

                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected( selectFollowerBox,contactListFiltered.get(getAdapterPosition()));
                }
            });*/
        }
    }


    public EditHistoricalTagPeopleAdapter(Context context, List<GetFollowersData> contactList, HashMap<Integer,String > cohostNameIdList , ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.contactList = contactList;
        this.contactListFiltered = contactList;
        this.cohostNameIdList = cohostNameIdList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_select_followers, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final GetFollowersData getFollowersData = contactListFiltered.get(position);


        if(contactListFiltered.get(position).isSelected()){
            holder.selectFollowerBox.setChecked(true);
        }else{
            holder.selectFollowerBox.setChecked(false);
        }


        Utility.picassoImageLoader(getFollowersData.getPicture(),
                1,holder.followerPhoto, context);


//        GlideApp.with(context).
//                load(getFollowersData.getPicture())
//                .placeholder(R.drawable.ic_add_photo)
//                .into(holder.followerPhoto);

        holder.userName.setText(getFollowersData.getFirstName() + " " + getFollowersData.getLastName());


        holder.selectFollowerBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(contactListFiltered.get(position).isSelected()){

                    contactListFiltered.get(position).setSelected(false);
                    listener.onContactSelected( holder.selectFollowerBox,contactListFiltered.get(position),0);

//                    holder.selectFollowerBox.setChecked(false);

                }else{

                    contactListFiltered.get(position).setSelected(true);
                    listener.onContactSelected( holder.selectFollowerBox,contactListFiltered.get(position),1);
//                    holder.selectFollowerBox.setChecked(false);

                }

            }
        });


/*
        for (Object value : cohostNameIdList.values()) {

            if (getFollowersData.getFollowerId().equals(value.toString())) {

                holder.selectFollowerBox.setChecked(true);

            }else{

                holder.selectFollowerBox.setChecked(false);

            }

        }*/




    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = contactList;
                } else {

                    List<GetFollowersData> filteredList = new ArrayList<>();

                    for (GetFollowersData row : contactList) {

                        String userName = row.getFirstName()+" "+row.getLastName();
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (userName.toLowerCase().contains(charString.toLowerCase())) {

                            filteredList.add(row);

                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<GetFollowersData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(View view, GetFollowersData contact,int position);
    }


}
