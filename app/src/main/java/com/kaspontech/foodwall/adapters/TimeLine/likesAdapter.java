package com.kaspontech.foodwall.adapters.TimeLine;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.modelclasses.TimeLinePojo.GetallLikesPojo;
import com.kaspontech.foodwall.R;


import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

//

/**
 * Created by vishnukm on 15/3/18.
 */

public class likesAdapter extends RecyclerView.Adapter<likesAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     **/

    Context context;
    /**
     * Arraylist for likes
     **/

    ArrayList<GetallLikesPojo> gettinglikeslist = new ArrayList<>();


    /**
     * Model class for like
     **/

    GetallLikesPojo likesPojo;







    public likesAdapter(Context c, ArrayList<GetallLikesPojo> gettinglist ) {
        this.context = c;
        this.gettinglikeslist = gettinglist;
    }




    @NonNull
    @Override
    public likesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_likes, parent, false);
        // set the view's size, margins, paddings and layout parameters
        likesAdapter.MyViewHolder vh = new likesAdapter.MyViewHolder(v);


        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final likesAdapter.MyViewHolder holder, final int position) {


        /*Likes pojo initialization*/

        likesPojo = gettinglikeslist.get(position);

        /* Setting Likes activity fields */

        /* Setting User first name */

        holder.txtUsernamelike.setText(gettinglikeslist.get(position).getFirstName());


        /*Setting Last name*/

        holder.txtUserlastnamelike.setText(gettinglikeslist.get(position).getLastName());



        /* Setting Full name */


        String fullname = gettinglikeslist.get(position).getFirstName().concat(" ").concat(gettinglikeslist.get(position).getLastName());

        holder.txtFullnameUserlike.setText(fullname);



        /*Setting user image*/
        Utility.picassoImageLoader(gettinglikeslist.get(position).getPicture(),
                0,holder.img_like, context);



        /*Hide follow button*/
        holder.rl_follow.setVisibility(View.GONE);


        /*User first name on click listener*/

        holder.txtUsernamelike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userId = gettinglikeslist.get(position).getUserId();

                if(userId==null){
                    deletePosition(position);
                }else{

                    if (gettinglikeslist.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                        context.startActivity(new Intent(context, Profile.class));
                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", gettinglikeslist.get(position).getUserId());
                        context.startActivity(intent);
                    }
                }

            }
        });


        /*User last name on click listener*/

        holder.txtUserlastnamelike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userId = gettinglikeslist.get(position).getUserId();

                if(userId==null){
                    deletePosition(position);
                }else{
                    if (gettinglikeslist.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                        context.startActivity(new Intent(context, Profile.class));
                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", gettinglikeslist.get(position).getUserId());
                        context.startActivity(intent);
                    }
                }

            }
        });

       /*User full name on click listener*/

        holder.txtFullnameUserlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userId = gettinglikeslist.get(position).getUserId();

                if(userId==null){
                    deletePosition(position);
                }else{
                    if (gettinglikeslist.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                        context.startActivity(new Intent(context, Profile.class));
                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", gettinglikeslist.get(position).getUserId());
                        context.startActivity(intent);
                    }
                }

            }
        });


        /*User image on click listener*/

        holder.img_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gettinglikeslist.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));


                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", gettinglikeslist.get(position).getUserId());
                    context.startActivity(intent);

                }
            }
        });


    }
    /* Remove the deleted position */

    private void deletePosition(int position) {
        gettinglikeslist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, gettinglikeslist.size());
    }

    @Override
    public int getItemCount() {

        return gettinglikeslist.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        /*TextView widget*/

        TextView txtUsernamelike;
        TextView txtUserlastnamelike;
        TextView txtFullnameUserlike;
        TextView txtFollow;


        /*Relative layout for follower*/

        RelativeLayout rl_follow;

        /*Image view user image*/
        ImageView img_like;

        String userId;


        public MyViewHolder(View itemView) {
            super(itemView);

            /*Textview Initialization*/

            txtUsernamelike = itemView.findViewById(R.id.txt_usernamelike);

            txtUserlastnamelike = itemView.findViewById(R.id.txt_userlastnamelike);

            txtFullnameUserlike = itemView.findViewById(R.id.txt_fullname_userlike);

            txtFollow = itemView.findViewById(R.id.txt_follow);

            /*Image view Initialization*/

            img_like = itemView.findViewById(R.id.img_like);

            /*Relative layout Initialization*/

            rl_follow = itemView.findViewById(R.id.rl_follow);


        }



    }


}
