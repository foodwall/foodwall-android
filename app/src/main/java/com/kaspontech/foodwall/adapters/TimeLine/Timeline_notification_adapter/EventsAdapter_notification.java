package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.onCommentsPostListener;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;


public class EventsAdapter_notification extends RecyclerView.ViewHolder implements View.OnClickListener {

    //ImageView
    ImageView eventBackground, eventview_user_photo, userphoto_comment, img_shareadapter;


    //LinearLayout
    LinearLayout viewEvent, ll_interested, ll_going, ll_share;

    //TextView
    TextView date, location, eventName, interestedText, goingText,
            shareText, txt_view_event_text, txt_adapter_post, commenttest, comment,
            View_all_comment, liketxt, like, txt_created_on, txt_view_event_username, txt_view_event_name;
    //Edittext
    EmojiconEditText edittxt_comment;

    //ShineButton like
    ImageButton img_like, img_commentadapter;

    //Relative layout
    RelativeLayout rl_share, like_rl, rl_comment_adapter, rl_post_timeline,
            rl_share_adapter, rl_likes_layout, rl_comments_layout,rl_comment_share;

    //display latest comment
    TextView username_latestviewed, created_on, recently_commented;
    ImageView userphoto, img_eventssharechat;

    RelativeLayout recently_commented_ll;

    ProgressBar comments_progressbar;

    onCommentsPostListener onCommentsPostListener;

    View itemViewShow;

    EventsAdapter_notification(View view, onCommentsPostListener onCommentsPostListener) {
        super(view);

        this.onCommentsPostListener = onCommentsPostListener;
        this.itemViewShow = view;

        //Imageview
        eventBackground = (ImageView) view.findViewById(R.id.event_background);
        eventview_user_photo = (ImageView) view.findViewById(R.id.eventview_user_photo);
        userphoto_comment = (ImageView) view.findViewById(R.id.userphoto_comment);
        img_shareadapter = (ImageView) view.findViewById(R.id.img_shareadapter);
        img_commentadapter = (ImageButton) view.findViewById(R.id.img_commentadapter);

        //Editext
        edittxt_comment = (EmojiconEditText) view.findViewById(R.id.edittxt_comment);
        //Like Button
        img_like = (ImageButton) view.findViewById(R.id.img_like);

        //Textview
        date = (TextView) view.findViewById(R.id.event_date);
        location = (TextView) view.findViewById(R.id.event_location);
        eventName = (TextView) view.findViewById(R.id.event_name);

        txt_view_event_text = (TextView) view.findViewById(R.id.txt_view_event_text);
        txt_view_event_username = (TextView) view.findViewById(R.id.txt_view_event_username);
        txt_view_event_name = (TextView) view.findViewById(R.id.txt_view_event_name);

        interestedText = (TextView) view.findViewById(R.id.interest_text);
        goingText = (TextView) view.findViewById(R.id.going_text);
        shareText = (TextView) view.findViewById(R.id.share_text);
        txt_adapter_post = (TextView) view.findViewById(R.id.txt_adapter_post);
        commenttest = (TextView) view.findViewById(R.id.commenttest);
        comment = (TextView) view.findViewById(R.id.comment);
        View_all_comment = (TextView) view.findViewById(R.id.View_all_comment);
        liketxt = (TextView) view.findViewById(R.id.liketxt);
        like = (TextView) view.findViewById(R.id.like);
        txt_created_on = (TextView) view.findViewById(R.id.txt_created_on);
        //LinearLayout
        viewEvent = (LinearLayout) view.findViewById(R.id.ll_view_event);
        ll_interested = (LinearLayout) view.findViewById(R.id.ll_interested);
        ll_going = (LinearLayout) view.findViewById(R.id.ll_going);
        ll_share = (LinearLayout) view.findViewById(R.id.ll_share);

        //Relative Layout
        rl_share = (RelativeLayout) view.findViewById(R.id.rl_share);
        like_rl = (RelativeLayout) view.findViewById(R.id.like_rl);
        rl_comment_adapter = (RelativeLayout) view.findViewById(R.id.rl_comment_adapter);
        rl_post_timeline = (RelativeLayout) view.findViewById(R.id.rl_post_timeline);
        rl_likes_layout = (RelativeLayout) view.findViewById(R.id.rl_likes_layout);
        rl_comments_layout = (RelativeLayout) view.findViewById(R.id.rl_comments_layout);
        rl_share_adapter = (RelativeLayout) view.findViewById(R.id.rl_share_adapter);
        rl_comment_share = (RelativeLayout) view.findViewById(R.id.rl_comment_share);


        recently_commented_ll = (RelativeLayout) view.findViewById(R.id.recently_commented_ll);
        username_latestviewed = (TextView) view.findViewById(R.id.username_latestviewed);
        created_on = (TextView) view.findViewById(R.id.created_on);
        recently_commented = (TextView) view.findViewById(R.id.recently_commented);
        img_eventssharechat = (ImageView) view.findViewById(R.id.img_eventssharechat);

        comments_progressbar = (ProgressBar) view.findViewById(R.id.comments_progressbar);

        txt_adapter_post.setOnClickListener(this);
        img_eventssharechat.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.txt_adapter_post:

                onCommentsPostListener.onPostCommitedView(view, getAdapterPosition(), itemViewShow);
                break;

            case R.id.img_eventssharechat:

                onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),itemViewShow);
                break;
        }
    }
}
