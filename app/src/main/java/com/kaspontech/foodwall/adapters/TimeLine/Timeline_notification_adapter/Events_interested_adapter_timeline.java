package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.onCommentsPostListener;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;


public class Events_interested_adapter_timeline extends RecyclerView.ViewHolder implements View.OnClickListener {


    ImageView eventBackground;


    ImageView event_user_photo, userphoto_comment, img_shareadapter;

    ImageButton img_like;

    EmojiconEditText edittxt_comment;
    ImageButton interested, going, share, img_commentadapter;
    LinearLayout viewEvent, ll_interested, ll_going, ll_share;
    TextView date, location, eventName, interestedText,
            goingText,
            txt_adapter_post, commenttest, comment,
            View_all_comment, liketxt, like, txt_created_on,
            shareText, txt_event_text, txt_view_event_interest_username, txt_view_event_interest_text, txt_view_event_interest_name;

    ProgressBar comments_progressbar;


    //Relative layout
    RelativeLayout rl_share, like_rl, rl_comment_adapter, rl_post_timeline,
            rl_share_adapter, rl_likes_layout, rl_comments_layout,rl_comment_share;

    //display latest comment
    TextView username_latestviewed, created_on, recently_commented;
    ImageView userphoto,img_eventsinterestedsharechat;

    RelativeLayout recently_commented_ll;

    onCommentsPostListener onCommentsPostListener;

    View viewItem;

    public Events_interested_adapter_timeline(View view, onCommentsPostListener onCommentsPostListener) {
        super(view);

        this.onCommentsPostListener = onCommentsPostListener;
        this.viewItem = view;

        eventBackground = (ImageView) view.findViewById(R.id.event_background);

        event_user_photo = (ImageView) view.findViewById(R.id.event_user_photo);

        userphoto_comment = (ImageView) view.findViewById(R.id.userphoto_comment);
        img_shareadapter = (ImageView) view.findViewById(R.id.img_shareadapter);
        img_commentadapter = (ImageButton) view.findViewById(R.id.img_commentadapter);

        //Editext
        edittxt_comment = (EmojiconEditText) view.findViewById(R.id.edittxt_comment);
        //Like Button
        img_like = (ImageButton) view.findViewById(R.id.img_like);

        date = (TextView) view.findViewById(R.id.event_date);

        txt_view_event_interest_username = (TextView) view.findViewById(R.id.txt_view_event_interest_username);

        txt_view_event_interest_text = (TextView) view.findViewById(R.id.txt_view_event_interest_text);

        txt_view_event_interest_name = (TextView) view.findViewById(R.id.txt_view_event_interest_name);


        txt_adapter_post = (TextView) view.findViewById(R.id.txt_adapter_post);
        commenttest = (TextView) view.findViewById(R.id.commenttest);
        comment = (TextView) view.findViewById(R.id.comment);
        View_all_comment = (TextView) view.findViewById(R.id.View_all_comment);
        liketxt = (TextView) view.findViewById(R.id.liketxt);
        like = (TextView) view.findViewById(R.id.like);
        txt_created_on = (TextView) view.findViewById(R.id.txt_created_on);

        location = (TextView) view.findViewById(R.id.event_location);
        eventName = (TextView) view.findViewById(R.id.event_name);


        viewEvent = (LinearLayout) view.findViewById(R.id.ll_view_event);


        //Relative Layout
        rl_share = (RelativeLayout) itemView.findViewById(R.id.rl_share);
        like_rl = (RelativeLayout) itemView.findViewById(R.id.like_rl);
        rl_comment_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_comment_adapter);
        rl_post_timeline = (RelativeLayout) itemView.findViewById(R.id.rl_post_timeline);
        rl_likes_layout = (RelativeLayout) itemView.findViewById(R.id.rl_likes_layout);
        rl_comments_layout = (RelativeLayout) itemView.findViewById(R.id.rl_comments_layout);
        rl_share_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_share_adapter);
        rl_comment_share = (RelativeLayout) itemView.findViewById(R.id.rl_comment_share);

        recently_commented_ll = (RelativeLayout) view.findViewById(R.id.recently_commented_ll);
        username_latestviewed = (TextView) view.findViewById(R.id.username_latestviewed);
        created_on = (TextView) view.findViewById(R.id.created_on);
        recently_commented = (TextView) view.findViewById(R.id.recently_commented);
        img_eventsinterestedsharechat = (ImageView) view.findViewById(R.id.img_eventsinterestedsharechat);

        comments_progressbar = (ProgressBar) view.findViewById(R.id.comments_progressbar);

        txt_adapter_post.setOnClickListener(this);
        img_eventsinterestedsharechat.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.txt_adapter_post:

                onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),viewItem);
                break;

            case R.id.img_eventsinterestedsharechat:

                onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),viewItem);
                break;
        }
    }
}
