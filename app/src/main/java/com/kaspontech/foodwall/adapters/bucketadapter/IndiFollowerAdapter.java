package com.kaspontech.foodwall.adapters.bucketadapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.bucketlistpackage.CreateBucketlistActivty;
import com.kaspontech.foodwall.bucketlistpackage.SearchIndiBucketListActivity;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.R;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class IndiFollowerAdapter extends RecyclerView.Adapter<IndiFollowerAdapter.MyViewHolder> implements Filterable {


    private Context context;

    private List<Get_followerlist_pojo> gettingfollowerslist = new ArrayList<>();
    private List<Get_followerlist_pojo> mFilteredList = new ArrayList<>();
    HashMap<String, Integer> selectedfollowerlistId = new HashMap<>();

    private Get_followerlist_pojo getFollowerlistPojo;

    private Get_following_profile_pojo getFollowingProfilePojo;

    CreateBucketlistActivty createBucketListActivty;
    SearchIndiBucketListActivity searchIndiBucketListActivity;

    Handler doubleHandler;
    private Pref_storage pref_storage;
    boolean  doubleClick = false;


    public IndiFollowerAdapter(Context c, List<Get_followerlist_pojo> gettinglist, HashMap<String, Integer> individualfollowerId) {
        this.context = c;
        this.gettingfollowerslist = gettinglist;
        this.selectedfollowerlistId = individualfollowerId;
        createBucketListActivty=(CreateBucketlistActivty)context;
//        searchIndiBucketListActivity=(SearchIndiBucketListActivity)context;
    }

    @NonNull
    @Override
    public IndiFollowerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_chat_followerlist, parent, false);
        // set the view's size, margins, paddings and layout parameters


        pref_storage = new Pref_storage();

        return new IndiFollowerAdapter.MyViewHolder(v, createBucketListActivty);
    }

    @Override
    public void onBindViewHolder(@NonNull final IndiFollowerAdapter.MyViewHolder holder, final int position) {


        getFollowerlistPojo = gettingfollowerslist.get(position);

        holder.check_follower.setVisibility(View.VISIBLE);

        holder.txt_username.setText(getFollowerlistPojo.getFirstName());
        holder.txt_username.setTextColor(Color.parseColor("#000000"));

        holder.txt_userlastname.setText(getFollowerlistPojo.getLastName());
        holder.txt_userlastname.setTextColor(Color.parseColor("#000000"));

        String fullname= getFollowerlistPojo.getFirstName().concat(" ").concat(getFollowerlistPojo.getLastName());

        holder.txt_fullname_user.setText(fullname);

        String ownuser_id= Pref_storage.getDetail(context, "userId");

        String positionuser_id= gettingfollowerslist.get(position).getFollowerId();



        if(Integer.parseInt(gettingfollowerslist.get(position).getFollowerId())==Integer.parseInt(ownuser_id)){

            removeatposition(position);

        }

        String profile_status= gettingfollowerslist.get(position).getProfileStatus();
        String friend= gettingfollowerslist.get(position).getFriend();

        // user image
        Utility.picassoImageLoader(gettingfollowerslist.get(position).getPicture(),0,holder.img_following, context);


//        if(gettingfollowerslist.get(position).getPicture().equalsIgnoreCase("0")||gettingfollowerslist.get(position).getPicture().isEmpty()){
//            holder.img_following.setImageResource(R.drawable.ic_add_photo);
//        }else {
//            GlideApp.with(context)
//                    .load(gettingfollowerslist.get(position).getPicture())
//                    .into(holder.img_following);
//        }





        for (Object value : selectedfollowerlistId.values()) {

            if (gettingfollowerslist.get(position).getFollowerId().contains(value.toString())) {

                holder.check_follower.setChecked(true);

            }

        }

    }




//Remove position

    private  void removeatposition(int position){
        gettingfollowerslist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, gettingfollowerslist.size());
    }





    @Override
    public int getItemCount() {

        return gettingfollowerslist.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = gettingfollowerslist;
                } else {

                    mFilteredList = gettingfollowerslist;

                 /*   ArrayList<Get_followerlist_pojo> filteredList = new ArrayList<>();

                    for (Get_followerlist_pojo androidVersion : gettingfollowerslist) {

                        if (androidVersion.get().toLowerCase().contains(charString) || androidVersion.getName().toLowerCase().contains(charString) || androidVersion.getVer().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;*/
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Get_followerlist_pojo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txt_username, txt_userlastname, txt_fullname_user, txt_follow, share, caption, captiontag, txt_created_on,txt_adapter_post;
        RelativeLayout rlayout_following;

        ImageView img_checked_follower,img_following;
                CreateBucketlistActivty createBucketListActivty;
//        SearchIndiBucketListActivity searchIndiBucketListActivity;

        CheckBox check_follower;


        public MyViewHolder(View itemView, CreateBucketlistActivty create_bucketList_activty) {
            super(itemView);
            this.createBucketListActivty =  create_bucketList_activty;
            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_userlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txt_fullname_user = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            img_following = (ImageView) itemView.findViewById(R.id.img_following);
            img_checked_follower = (ImageView) itemView.findViewById(R.id.img_checked_follower);

            check_follower=(CheckBox) itemView.findViewById(R.id.check_follower);
            check_follower.setOnClickListener(this);

            rlayout_following = (RelativeLayout) itemView.findViewById(R.id.rlayout_following);




        }


        @Override
        public void onClick(View v) {
            createBucketListActivty.getSelectedfollower(v, getAdapterPosition());
        }
    }




}

