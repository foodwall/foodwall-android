package com.kaspontech.foodwall.adapters.chat;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;

public class Left_group_adapter extends RecyclerView.ViewHolder {


    TextView txt_change_grp_name;
    RelativeLayout rlGrpNameChange;


    public Left_group_adapter(View itemView) {
        super(itemView);

        txt_change_grp_name = (TextView) itemView.findViewById(R.id.txt_change_grp_name);

        rlGrpNameChange = (RelativeLayout) itemView.findViewById(R.id.rl_grp_name_change);


    }
}