package com.kaspontech.foodwall.adapters.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.fcm.Constants;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_history_single_details;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class Chat_muliti_view_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        RecyclerView.OnItemTouchListener, View.OnTouchListener {

    /**
     * Context
     **/
    private Context context;

    /**
     * Chat history details arraylist
     **/
    private ArrayList<Chat_history_single_details> getChatHistoryDetailsPojoArrayList = new ArrayList<>();

    /**
     * Chat history details pojo
     **/
    Chat_history_single_details chatHistorySingleDetails;

    /**
     * Chat id
     **/
    int chatId;

    onRedirectPageListener onRedirectPageListener;

    public Chat_muliti_view_adapter(Context context, ArrayList<Chat_history_single_details> getChatHistoryDetailsPojoArrayList, Chat_muliti_view_adapter.onRedirectPageListener onRedirectPageListener) {
        this.context = context;
        this.getChatHistoryDetailsPojoArrayList = getChatHistoryDetailsPojoArrayList;
        this.onRedirectPageListener = onRedirectPageListener;
    }

    public Chat_muliti_view_adapter(Context c, ArrayList<Chat_history_single_details> gettinglist /*,ArrayList<Chat_details_pojo> chatDetailsPojoArrayList*/) {
        this.context = c;
        this.getChatHistoryDetailsPojoArrayList = gettinglist;

        /*this.chatDetailsPojoArrayList = chatDetailsPojoArrayList;*/
    }

    @Override
    public int getItemViewType(int position) {

        // Case 0 - From user message
        // Case 1 - To user message

        // Case 0 - From user message

        //Normal message

        if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 0) {
            // Case 0 - From user message
            if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getFromuserid()) == (Integer.parseInt(Pref_storage.getDetail(context, "userId")))) {

                return 0;
                // Case 1 - To user message
            } else {

                return 1;
            }


        }

        //Changed groupname
        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 1) {
            return 2;
        }
        //Changed group_icon
        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 2) {
            return 3;

        }
        //Changed both group_icon & groupname

        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 3) {
            return 4;

        }
        //Bucket individual
        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 4) {
            return 5;

        }

        //Group bucket
        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 5) {
            return 6;

        }

        //Left group
        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 6) {
            return 7;

        }

        //Left group
        else {

            return 8;
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            // Case 0 - from user
            case 0:

                View fromuserview = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_from_chat, parent, false);
                return new Chat_from_adapter(fromuserview, onRedirectPageListener);

            // Case 1 - to user
            case 1:

                View touserview = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_chat_single, parent, false);
                return new Chat_to_adapter(touserview, onRedirectPageListener);

            //group name
            case 2:

                View grpnameChange = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_name_change, parent, false);
                return new Grpname_chage_adapter(grpnameChange);

            // group image
            case 3:

                View grpiconChange = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_icon_change, parent, false);
                return new Grpicon_change_adpter(grpiconChange);

            //group both changed
            case 4:

                View grpBoth = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_icon_change, parent, false);
                return new Grp_both_change_adapter(grpBoth);

            //Individual bucket
            case 5:

                View bucketIndiView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_name_change, parent, false);
                return new Grp_bucket_individual_adapter(bucketIndiView);


            //Group bucket
            case 6:

                View bucketGrpView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_name_change, parent, false);
                return new Grp_bucket_adapter(bucketGrpView);

            //Left bucket
            case 7:

                View leftGrpView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_name_change, parent, false);
                return new Left_group_adapter(leftGrpView);

            case 8:

                View newGrpView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_name_change, parent, false);
                return new New_to_group_adapter(newGrpView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        chatHistorySingleDetails = getChatHistoryDetailsPojoArrayList.get(position);

        switch (holder.getItemViewType()) {
            // Case 0 - from user
            case 0:
                final Chat_from_adapter chatFromAdapter = (Chat_from_adapter) holder;


                //Group username
//                String grp_username = getChatHistoryDetailsPojoArrayList.get(position).get

                String decoded_message = org.apache.commons.text.StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getMessage());

                //user message
                chatFromAdapter.emojiconTextView.setText(decoded_message);
                chatFromAdapter.emojiconTextView.setTextColor(Color.parseColor("#FFFFFF"));

                //Ago
                String ago = getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn();
                try {
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                    dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Date date = dateFormatter.parse(ago);
                    // Get time from date
                    SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                    timeFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String displayValue = timeFormatter.format(date);
                    chatFromAdapter.txtMessageTime.setText(displayValue);

                } catch (ParseException e) {
                    e.printStackTrace();
                }


                try {
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Date date = dateFormatter.parse(ago);
                    // Get date
                    SimpleDateFormat timeFormatter = new SimpleDateFormat("dd-MM-yyyy");
                    timeFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String displayValue = timeFormatter.format(date);
                    chatFromAdapter.txtChatDate.setText(displayValue);

                } catch (ParseException e) {
                    e.printStackTrace();
                }


                /*Chat single on click listener*/
                chatFromAdapter.idRlChatSingle.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {


                        chatId = Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getChatid());


                        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Are you sure want to delete this message?")
                                .setContentText("Won't be able to recover this message anymore!")
                                .setConfirmText("Yes,delete it!")
                                .setCancelText("No,cancel!")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();


                                        if (Utility.isConnected(context)) {
                                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                            try {

                                                String createdby = Pref_storage.getDetail(context, "userId");
                                                Call<CreateUserPojoOutput> call = apiService.update_delete_single_chat("update_delete_single_chat", chatId, Integer.parseInt(createdby));
                                                call.enqueue(new Callback<CreateUserPojoOutput>() {
                                                    @Override
                                                    public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                                                        if (response.body() != null && response.body().getResponseCode() == 1) {

                                                            try {
                                                                getChatHistoryDetailsPojoArrayList.remove(position);
                                                                notifyItemRemoved(position);
                                                                notifyItemRangeChanged(position, getChatHistoryDetailsPojoArrayList.size());

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }

                                                            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                                    .setTitleText("Deleted!!")

                                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                        @Override
                                                                        public void onClick(SweetAlertDialog sDialog) {
                                                                            sDialog.dismissWithAnimation();

                                                                        }
                                                                    })
                                                                    .show();


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                                        //Error
                                                        Log.e("FailureError", "" + t.getMessage());
                                                    }
                                                });
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            Toast.makeText(context, "No internet connection!!", Toast.LENGTH_SHORT).show();
                                        }


                                    }
                                }).show();


                        return false;
                    }
                });
                break;

            // Case 1 - to user
            case 1:

                final Chat_to_adapter chatToAdapter = (Chat_to_adapter) holder;
                String decoded_message2 = org.apache.commons.text.StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getMessage());


                //Group name
                String groupid = getChatHistoryDetailsPojoArrayList.get(position).getGroupid();
                if (Integer.parseInt(groupid) != 0) {
                    Log.e("Groupuser", "" + getChatHistoryDetailsPojoArrayList.get(position).getToFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getToLastname()));

                    chatToAdapter.txt_group_username.setText(getChatHistoryDetailsPojoArrayList.get(position).getToFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getToLastname()));
                    chatToAdapter.txt_group_username.setVisibility(View.VISIBLE);
                } else if (Integer.parseInt(groupid) == 0) {

                    chatToAdapter.txt_group_username.setVisibility(View.GONE);
                }
                //user message
                chatToAdapter.emojicon_text_view.setText(decoded_message2);
                chatToAdapter.emojicon_text_view.setTextColor(Color.parseColor("#000000"));


                //User picture

                Utility.picassoImageLoader(getChatHistoryDetailsPojoArrayList.get(position).getFromPicture(),
                        0, chatToAdapter.img_user_single, context);

                chatToAdapter.img_user_single.setVisibility(View.GONE);

                //Ago
                String ago2 = getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn();

                // Get time from date
                try {
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Date date = dateFormatter.parse(ago2);

                    SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                    timeFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String displayValue = timeFormatter.format(date);
                    chatToAdapter.txt_message_time.setText(displayValue);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // Get time from date

                try {
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                    dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Date date = dateFormatter.parse(ago2);

                    SimpleDateFormat timeFormatter = new SimpleDateFormat("dd-MM-yyyy");
                    timeFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String displayValue = timeFormatter.format(date);
                    chatToAdapter.txt_chat_date.setText(displayValue);

                } catch (ParseException e) {
                    e.printStackTrace();
                }





                /*Chat relative layout on click listener*/
                chatToAdapter.rl_chat_single.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {


                        chatId = Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getChatid());

                        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Are you sure want to delete this message?")
                                .setContentText("Won't be able to recover this message anymore!")
                                .setConfirmText("Yes,delete it!")
                                .setCancelText("No,cancel!")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                        if (Utility.isConnected(context)) {
                                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                            try {

                                                String createdby = Pref_storage.getDetail(context, "userId");
                                                Call<CreateUserPojoOutput> call = apiService.update_delete_single_chat("update_delete_single_chat", chatId, Integer.parseInt(createdby));
                                                call.enqueue(new Callback<CreateUserPojoOutput>() {
                                                    @Override
                                                    public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                                                        if (response.body().getResponseCode() == 1) {
                                                            removeAt(position);

                                                            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                                    .setTitleText("Deleted!!")

                                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                        @Override
                                                                        public void onClick(SweetAlertDialog sDialog) {
                                                                            sDialog.dismissWithAnimation();
                                                                        }
                                                                    })
                                                                    .show();


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                                        //Error
                                                        Log.e("FailureError", "" + t.getMessage());
                                                    }
                                                });
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            Toast.makeText(context, "No internet connnection!!", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                })
                                .show();


                        return false;
                    }
                });


                break;

            case 2:

                Grpname_chage_adapter grpnameChageAdapter = (Grpname_chage_adapter) holder;
                String grpNameChange = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group name to ").concat(StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getGroupName())));
                grpnameChageAdapter.txt_change_grp_name.setText(grpNameChange);


                grpnameChageAdapter.txt_change_grp_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(context, Home.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("redirect", "profile");
                        context.startActivity(intent);

                    }
                });

                break;


            case 3:

                Grpicon_change_adpter grpiconChangeAdpter = (Grpicon_change_adpter) holder;
                String grpIconChange = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group icon."));
                grpiconChangeAdpter.txtChangeGrpIcon.setText(grpIconChange);
                String grp_image = getChatHistoryDetailsPojoArrayList.get(position).getGroupIcon();


                Picasso.get().load(grp_image).centerInside().placeholder(R.drawable.ic_group_inside).into(grpiconChangeAdpter.img_grpicon_change_frm);
                break;


            case 4:

                Grp_both_change_adapter grpBothChangeAdapter = (Grp_both_change_adapter) holder;

                String grp_both_name = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group's icon and name to ").concat(org.apache.commons.text.StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getGroupName())));
                grpBothChangeAdapter.txtChangeGrpIcon.setText(grp_both_name);
                String grp_both_image = getChatHistoryDetailsPojoArrayList.get(position).getGroupIcon();

                Utility.picassoImageLoader(grp_both_image, 0, grpBothChangeAdapter.img_grpicon_change_frm, context);
                break;

            //Individual bucket
            case 5:

                final Grp_bucket_individual_adapter grpBucketIndividualAdapter = (Grp_bucket_individual_adapter) holder;

                String userid = getChatHistoryDetailsPojoArrayList.get(position).getFromuserid();

                if (userid.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    grpBucketIndividualAdapter.txt_change_grp_name.setText("You added a new bucket");

                } else {
                    String grpIndiName = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" added a new bucket with you."));
                    grpBucketIndividualAdapter.txt_change_grp_name.setText(grpIndiName);

                }
                grpBucketIndividualAdapter.txt_change_grp_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, Home.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("redirect", "profile");
                        intent.putExtra("redirectbucket", "2");
                        context.startActivity(intent);
                    }
                });
                break;

            //Group bucket
            case 6:

                Grp_bucket_individual_adapter grpBucketIndividualAdapter1 = (Grp_bucket_individual_adapter) holder;
                String userid2 = getChatHistoryDetailsPojoArrayList.get(position).getFromuserid();

                if (userid2.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    grpBucketIndividualAdapter1.txt_change_grp_name.setText("You added a new bucket");

                } else {

                    String grpIndiName1 = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" added a new bucket with you."));
                    grpBucketIndividualAdapter1.txt_change_grp_name.setText(grpIndiName1);
                }

                grpBucketIndividualAdapter1.txt_change_grp_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(context, Home.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("redirect", "profile");
                        intent.putExtra("redirectbucket", "2");
                        context.startActivity(intent);
                    }
                });

                break;


            //Left group
            case 7:

                Left_group_adapter leftGroupAdapter = (Left_group_adapter) holder;
                String leftUserName = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" left the group."));
                leftGroupAdapter.txt_change_grp_name.setText(leftUserName);
                break;

            //New to group
            case 8:

                New_to_group_adapter newToGroupAdapter = (New_to_group_adapter) holder;
                String newUserName = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" created this group."));
                newToGroupAdapter.txt_change_grp_name.setText(newUserName);
                break;


        }
    }

    //Real time position removal
    public void removeAt(int position) {
        getChatHistoryDetailsPojoArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getChatHistoryDetailsPojoArrayList.size());
    }

    @Override
    public int getItemCount() {
        return getChatHistoryDetailsPojoArrayList.size();
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    public interface onRedirectPageListener {
        void onRedirectPage(View view, int adapterPosition, View viewItem);
    }
}
