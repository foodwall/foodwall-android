package com.kaspontech.foodwall.adapters.events;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.kaspontech.foodwall.eventspackage.CreateEvent;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Utility;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SelectCohostAdapter extends RecyclerView.Adapter<SelectCohostAdapter.MyViewHolder>
        implements Filterable {

    /**
     * Context
     **/
    Context context;
    /**
     * Create event pojo
     **/
    CreateEvent createEvent;

    /**
     * Array lists
     **/
    private List<GetFollowersData> contactList;
    private List<GetFollowersData> contactListFiltered;


//    List<GetFollowersData> getFollowersDataList = new ArrayList<>();
    HashMap<Integer, String> cohostNameIdList = new HashMap<>();
    GetFollowersData getFollowersData;
    static boolean checked = false;

    public SelectCohostAdapter(Context context, List<GetFollowersData> contactList, HashMap<Integer, String> cohostNameIdList) {
        this.context = context;
        this.contactList = contactList;
        this.contactListFiltered = contactList;
        this.cohostNameIdList = cohostNameIdList;
        createEvent = (CreateEvent) context;
    }

    @Override
    public Filter getFilter() {

        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = contactList;
                } else {

                    List<GetFollowersData> filteredList = new ArrayList<>();

                    for (GetFollowersData row : contactList) {


                        String userName = row.getFirstName()+" "+row.getLastName();

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (userName.toLowerCase().contains(charString.toLowerCase())) {

                            filteredList.add(row);

                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<GetFollowersData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    /*View Holder*/
    public static class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {


        CircleImageView followerPhoto;
        TextView userName;
        CheckBox selectFollowerBox;
        CreateEvent createEvent;

        public MyViewHolder(View view, CreateEvent createEvent) {
            super(view);

            this.createEvent = createEvent;
            followerPhoto = (CircleImageView)view.findViewById(R.id.user_image);
            userName = (TextView)view.findViewById(R.id.user_name);
            selectFollowerBox = (CheckBox)view.findViewById(R.id.followers_checked);
//            selectFollowerBox.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

//            viewEvent.getSelectedCoHosts(view, getAdapterPosition());

        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_select_followers, parent, false);
        return new MyViewHolder(itemView, createEvent);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder,final int position) {

        getFollowersData = contactListFiltered.get(position);

        if(contactListFiltered.get(position).isSelected()){
            holder.selectFollowerBox.setChecked(true);
        }else{
            holder.selectFollowerBox.setChecked(false);
        }

        /* Follower image */

        Utility.picassoImageLoader(getFollowersData.getPicture(),0,holder.followerPhoto, context);


        /* Follower user name */

        holder.userName.setText(getFollowersData.getFirstName() + " " + getFollowersData.getLastName());

        /*Followers check box on click listener*/

        holder.selectFollowerBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(contactListFiltered.get(position).isSelected()){

                    contactListFiltered.get(position).setSelected(false);
                    createEvent.getSelectedCoHosts( holder.selectFollowerBox,contactListFiltered.get(position),0);

//                    holder.selectFollowerBox.setChecked(false);

                }else{

                    contactListFiltered.get(position).setSelected(true);
                    createEvent.getSelectedCoHosts( holder.selectFollowerBox,contactListFiltered.get(position),1);
//                    holder.selectFollowerBox.setChecked(false);

                }

            }
        });


       /* for (Object value : cohostNameIdList.values()) {


            if (contactListFiltered.get(position).getFollowerId().equals(value.toString())) {

                holder.selectFollowerBox.setChecked(true);

            }


        }*/


    }




    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }





}
