package com.kaspontech.foodwall.adapters.Review_Image_adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.GlideApp;
//

import java.util.List;

import static com.kaspontech.foodwall.reviewpackage.GalleryImage_Review.imagesEncodedList;

//


public class Review_sliding_image_adapter extends PagerAdapter implements View.OnTouchListener {


    //    private ArrayList<Integer> IMAGES;
    private List<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;

    private PhotoView imageView;
    public Review_sliding_image_adapter(Context context, List<String> IMAGES) {
        this.context = context;
        this.IMAGES=IMAGES;
        inflater = LayoutInflater.from(context);




    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slide_image_layout, view, false);

        assert imageLayout != null;
        imageView = (PhotoView) imageLayout.findViewById(R.id.image);
 /*       imageView= (ZoomImageView) imageLayout
                .findViewById(R.id.image);*/

//        Utility.picassoImageLoader(imagesEncodedList.get(position).replace("[","").replace("]",""),
//                1,imageView, context);
        GlideApp.with(context)
                .load(imagesEncodedList.get(position).replace("[","").replace("]",""))
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(imageView);


        view.addView(imageLayout, 0);


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}