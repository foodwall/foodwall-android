package com.kaspontech.foodwall.adapters.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_group_history_details;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import org.apache.commons.lang3.StringEscapeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class Chat_multi_view_group_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RecyclerView.OnItemTouchListener, View.OnTouchListener {

    /**
     * Chat group history details
     **/
    Get_chat_group_history_details chatHistorySingleDetails;

    /**
     * Chat id
     **/
    int chat_id;
    /**
     * Context
     **/
    private Context context;
    /**
     * Chat group history details arraylist
     **/
    private ArrayList<Get_chat_group_history_details> getChatHistoryDetailsPojoArrayList = new ArrayList<>();

    /**
     * Chat group inside activity interface class
     **/
    com.kaspontech.foodwall.chatpackage.chatGroupInsideActivity chatGroupInsideActivity;


    public Chat_multi_view_group_adapter(Context c, ArrayList<Get_chat_group_history_details> gettinglist /*,ArrayList<Chat_details_pojo> chatDetailsPojoArrayList*/) {
        this.context = c;
        this.getChatHistoryDetailsPojoArrayList = gettinglist;
        chatGroupInsideActivity = (com.kaspontech.foodwall.chatpackage.chatGroupInsideActivity) context;
        /*this.chatDetailsPojoArrayList = chatDetailsPojoArrayList;*/
    }


    @Override
    public int getItemViewType(int position) {

        // Case 0 - From user message
        // Case 1 - To user message


        //Normal message

        if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 0) {
            // Case 0 - From user message
            if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getFromuserid()) == (Integer.parseInt(Pref_storage.getDetail(context, "userId")))) {

                return 0;
                // Case 1 - To user message
            } else {
                return 1;
            }


        }
        //Changed groupname

        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 1) {
            return 2;
        }
        //Changed group_icon
        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 2) {
            return 3;

        }
        //Changed both group_icon & groupname

        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 3) {
            return 4;

        }
        //Bucket individual
        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 4) {
            return 5;

        }

        //Group bucket
        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 5) {
            return 6;

        }

        //Left group

        else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getTypeMessage()) == 6) {
            return 7;

        }
        //Left bucket

        else {
            return 8;
        }


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            // Case 0 - from user
            case 0:

                View fromuserview = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_from_chat, parent, false);
                return new Chat_from_adapter(fromuserview);

            // Case 1 - to user
            case 1:
                View touserview = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_chat_single, parent, false);
                return new Chat_to_adapter(touserview);

            //group name
            case 2:
                View grpnameChange = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_name_change, parent, false);
                return new Grpname_chage_adapter(grpnameChange);

            // group image
            case 3:
                View grpiconChange = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_icon_change, parent, false);
                return new Grpicon_change_adpter(grpiconChange);

            //group both changed
            case 4:
                View grpBoth = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_icon_change, parent, false);
                return new Grp_both_change_adapter(grpBoth);
            //Individual bucket

            case 5:
                View bucketIndiView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_name_change, parent, false);
                return new Grp_bucket_individual_adapter(bucketIndiView);


            //Group bucket

            case 6:
                View bucketGrpView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_name_change, parent, false);
                return new Grp_bucket_adapter(bucketGrpView);

            //Left bucket

            case 7:
                View leftGrpView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_name_change, parent, false);
                return new Left_group_adapter(leftGrpView);

            case 8:
                View newGrpView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grp_name_change, parent, false);
                return new New_to_group_adapter(newGrpView);


        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        chatHistorySingleDetails = getChatHistoryDetailsPojoArrayList.get(position);


        switch (holder.getItemViewType()) {
            // Case 0 - from user
            case 0:
                final Chat_from_adapter chatFromAdapter = (Chat_from_adapter) holder;


                //Group username

                String decodedMessage = org.apache.commons.text.StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getMessage());

                //user message

                chatFromAdapter.emojiconTextView.setText(decodedMessage);

                chatFromAdapter.emojiconTextView.setTextColor(Color.parseColor("#FFFFFF"));


                //Ago

                String ago = getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn();

                try {

                    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Date date = dateFormatter.parse(ago);


                    // Get time from date
                    SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                    timeFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String displayValue = timeFormatter.format(date);
                    chatFromAdapter.txtMessageTime.setText(displayValue);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                try {
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Date date = dateFormatter.parse(ago);


                    // Get time from date
//                    SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat timeFormatter = new SimpleDateFormat("dd-MM-yyyy");
                    timeFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String displayValue = timeFormatter.format(date);
                    chatFromAdapter.txtChatDate.setText(displayValue);

                } catch (ParseException e) {
                    e.printStackTrace();
                }



                /*Chat layout on click listener*/
                chatFromAdapter.idRlChatSingle.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {


                        chat_id = Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getChatid());

                        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Are you sure want to delete this message?")
                                .setContentText("Won't be able to recover this message anymore!")
                                .setConfirmText("Yes,delete it!")
                                .setCancelText("No,cancel!")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                        if (Utility.isConnected(context)) {
                                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                            try {

                                                String createdby = Pref_storage.getDetail(context, "userId");
                                                Call<CreateUserPojoOutput> call = apiService.update_delete_single_chat("update_delete_single_chat", chat_id, Integer.parseInt(createdby));
                                                call.enqueue(new Callback<CreateUserPojoOutput>() {
                                                    @Override
                                                    public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                                                        if (response.body() != null && response.body().getResponseCode() == 1) {

                                                            /*Delete position*/
                                                            removeAt(position);


                                                            Toast.makeText(context, "Deleted!!", Toast.LENGTH_SHORT).show();

//                                                            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//                                                                    .setTitleText("Deleted!!")
//
//                                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                                                        @Override
//                                                                        public void onClick(SweetAlertDialog sDialog) {
//                                                                            sDialog.dismissWithAnimation();
//
//
//                                                                        }
//                                                                    })
//                                                                    .show();


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                                        //Error
                                                        Log.e("FailureError", "" + t.getMessage());
                                                    }
                                                });
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                        }


                                    }
                                })
                                .show();


                        return false;
                    }
                });
                break;


            // Case 1 - to user
            case 1:
                final Chat_to_adapter chatToAdapter = (Chat_to_adapter) holder;
                String decoded_message2 = org.apache.commons.text.StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getMessage());


                //Group name

                chatToAdapter.txt_group_username.setText(getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname()));
                chatToAdapter.txt_group_username.setVisibility(View.VISIBLE);


                //user message
                chatToAdapter.emojicon_text_view.setText(decoded_message2);
                chatToAdapter.emojicon_text_view.setTextColor(Color.parseColor("#000000"));
                //User picture


               /* if (getChatHistoryDetailsPojoArrayList.get(position).getToPicture() == null) {
                    chatToAdapter.imgUserSingle.setImageResource(R.drawable.ic_add_photo);
                } else {
                    GlideApp.with(context).load(getChatHistoryDetailsPojoArrayList.get(position).getFromPicture()).
                            centerInside().placeholder(R.drawable.ic_add_photo).into(chatToAdapter.imgUserSingle);

                }*/


                Utility.picassoImageLoader(getChatHistoryDetailsPojoArrayList.get(position).getFromPicture(),
                        0, chatToAdapter.img_user_single, context);

/*
                GlideApp.with(context).load(getChatHistoryDetailsPojoArrayList.get(position).getFromPicture()).
                        centerInside().placeholder(R.drawable.ic_add_photo).into(chatToAdapter.imgUserSingle);*/


//Ago
                String ago2 = getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn();


                try {
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Date date = dateFormatter.parse(ago2);


// Get time from date
                    SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                    timeFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String displayValue = timeFormatter.format(date);
                    chatToAdapter.txt_message_time.setText(displayValue);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Date date = dateFormatter.parse(ago2);


// Get time from date
//                    SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat timeFormatter = new SimpleDateFormat("dd-MM-yyyy");
                    timeFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String displayValue = timeFormatter.format(date);
                    chatToAdapter.txt_chat_date.setText(displayValue);

                } catch (ParseException e) {
                    e.printStackTrace();
                }





      /*  try {
            long now = System.currentTimeMillis();

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convertedDate = dateFormat.parse(ago);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now,

                    DateUtils.FORMAT_ABBREV_RELATIVE);

            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                holder.txtMessageTime.setText(R.string.just_now);
            } else if (relavetime1.toString().contains("hours ago")) {
                holder.txtMessageTime.setText(relavetime1.toString().replace("hours ago", "").concat("h"));
            } else if (relavetime1.toString().contains("days ago")) {
                holder.txtMessageTime.setText(relavetime1.toString().replace("days ago", "").concat("dialog"));
            } else {
                holder.txtMessageTime.append(relavetime1 + "\n\n");

            }


        } catch (ParseException e) {
            e.printStackTrace();
        }*/

                /*Chat single on click listener*/

                chatToAdapter.rl_chat_single.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {


                        chat_id = Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getChatid());


                        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Are you sure want to delete this message?")
                                .setContentText("Won't be able to recover this message anymore!")
                                .setConfirmText("Yes,delete it!")
                                .setCancelText("No,cancel!")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        if (Utility.isConnected(context)) {

                                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                            try {

                                                String createdby = Pref_storage.getDetail(context, "userId");
                                                Call<CreateUserPojoOutput> call = apiService.update_delete_single_chat("update_delete_single_chat", chat_id, Integer.parseInt(createdby));
                                                call.enqueue(new Callback<CreateUserPojoOutput>() {
                                                    @Override
                                                    public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                                                        if (response.body().getResponseCode() == 1) {

                                                            removeAt(position);

                                                            Toast.makeText(context, "Deleted!!", Toast.LENGTH_SHORT).show();


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                                        //Error
                                                        Log.e("FailureError", "" + t.getMessage());
                                                    }
                                                });
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            Toast.makeText(context, "No internet connection!!", Toast.LENGTH_SHORT).show();
                                        }


                                    }
                                })
                                .show();


                        return false;
                    }
                });


                break;

            case 2:
                Grpname_chage_adapter grpnameChageAdapter = (Grpname_chage_adapter) holder;
                String grpNameChange = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group name to ").concat(StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getGroupName())));
                grpnameChageAdapter.txt_change_grp_name.setText(grpNameChange);
                break;


            case 3:
                Grpicon_change_adpter grpiconChangeAdpter = (Grpicon_change_adpter) holder;
                String grpIconChange = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group icon."));
                grpiconChangeAdpter.txtChangeGrpIcon.setText(grpIconChange);
                String grp_image = getChatHistoryDetailsPojoArrayList.get(position).getGroupIcon();

                Utility.picassoImageLoader(grp_image,
                        0, grpiconChangeAdpter.img_grpicon_change_frm, context);


//                    GlideApp.with(context).load(grp_image).centerInside().into(grpiconChangeAdpter.img_grpicon_change_frm);
                break;


            case 4:
                Grp_both_change_adapter grpBothChangeAdapter = (Grp_both_change_adapter) holder;

                String grpBothName = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group's icon and name to ").concat(org.apache.commons.text.StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getGroupName())));
                grpBothChangeAdapter.txtChangeGrpIcon.setText(grpBothName);
                String grp_both_image = getChatHistoryDetailsPojoArrayList.get(position).getGroupIcon();
                Utility.picassoImageLoader(grp_both_image,
                        0, grpBothChangeAdapter.img_grpicon_change_frm, context);


//                    GlideApp.with(context).load(grp_both_image).centerInside().into(grpBothChangeAdapter.img_grpicon_change_frm);
                break;

            //Individual bucket
            case 5:
                Grp_bucket_individual_adapter grpBucketIndividualAdapter = (Grp_bucket_individual_adapter) holder;
                String grpIndiName = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" added a new bucket with you"));
                grpBucketIndividualAdapter.txt_change_grp_name.setText(grpIndiName);
                break;

            //Group bucket
            case 6:
                Grp_bucket_adapter grpBucketAdapter = (Grp_bucket_adapter) holder;
                String grpBucketName = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" added a new bucket with this group"));
                grpBucketAdapter.txt_change_grp_name.setText(grpBucketName);

                grpBucketAdapter.txt_change_grp_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                           /* Intent intent=new Intent(context, MyBucketlistActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);*/
                        Intent intent = new Intent(context, Home.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("redirect", "profile");
                        intent.putExtra("redirectbucket", "3");
                        context.startActivity(intent);
                    }
                });

                break;


            //Left group
            case 7:
                Left_group_adapter leftGroupAdapter = (Left_group_adapter) holder;
                String leftUserName = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" left the group."));

                String username = Pref_storage.getDetail(context, "firstName").concat(" ").concat(Pref_storage.getDetail(context, "lastName"));
                if (getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname()).equalsIgnoreCase(username)) {

                    leftGroupAdapter.txt_change_grp_name.setText(R.string.left_group);
                } else {
                    leftGroupAdapter.txt_change_grp_name.setText(leftUserName);
                }


                break;

            //New to group
            case 8:
                New_to_group_adapter newToGroupAdapter = (New_to_group_adapter) holder;
                String newUserName = getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" created this group."));

                String username2 = Pref_storage.getDetail(context, "firstName").concat(Pref_storage.getDetail(context, "lastName"));
                if (getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname()).equalsIgnoreCase(username2)) {

                    newToGroupAdapter.txt_change_grp_name.setText(R.string.created_group);
                } else {
                    newToGroupAdapter.txt_change_grp_name.setText(newUserName);
                }

                newToGroupAdapter.txt_change_grp_name.setText(newUserName);

                break;

        }
    }


    /*Remove postion - deletion*/
    public void removeAt(int position) {
        getChatHistoryDetailsPojoArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getChatHistoryDetailsPojoArrayList.size());
    }

    @Override
    public int getItemCount() {
        return getChatHistoryDetailsPojoArrayList.size();
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
