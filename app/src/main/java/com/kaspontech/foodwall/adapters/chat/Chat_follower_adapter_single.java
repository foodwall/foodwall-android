package com.kaspontech.foodwall.adapters.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.chatpackage.Create_new_chat_single;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_history_details_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.GlideApp;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//

public class Chat_follower_adapter_single extends RecyclerView.Adapter<Chat_follower_adapter_single.MyViewHolder> implements RecyclerView.OnItemTouchListener, Filterable {


    /**
     * Context
     **/
    private Context context;
    /**
     * Followers ArrayList
     **/
    private List<Get_followerlist_pojo> getFollowerlistPojoArrayList = new ArrayList<>();
    private List<Get_followerlist_pojo> getFollowerlistPojoArrayListFiltered = new ArrayList<>();

    /**
     * Followers pojo
     **/
    private Get_followerlist_pojo getFollowerlistPojo;


    /**
     * String friend lists
     **/
    public static List<Integer> single_frd_id = new ArrayList<>();

    /**
     * String friend hash lists
     **/

    HashMap<String, Integer> hash_friends_list = new HashMap<>();

    public Chat_follower_adapter_single(Context c, List<Get_followerlist_pojo> gettinglist) {
        this.context = c;
        this.getFollowerlistPojoArrayList = gettinglist;
        this.getFollowerlistPojoArrayListFiltered = gettinglist;
    }

    @NonNull
    @Override
    public Chat_follower_adapter_single.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_chat_followerlist, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Chat_follower_adapter_single.MyViewHolder vh = new Chat_follower_adapter_single.MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Chat_follower_adapter_single.MyViewHolder holder, final int position) {


        getFollowerlistPojo = getFollowerlistPojoArrayList.get(position);

        /*User name displaying */

        holder.txtUsername.setText(getFollowerlistPojoArrayList.get(position).getFirstName());

        holder.txtUsername.setTextColor(Color.parseColor("#000000"));

        holder.txtUserlastname.setText(getFollowerlistPojoArrayList.get(position).getLastName());
        holder.txtUserlastname.setTextColor(Color.parseColor("#000000"));

        String fullname= getFollowerlistPojoArrayList.get(position).getFirstName().concat(" ").
                concat(getFollowerlistPojoArrayList.get(position).getLastName());

        holder.txtFullnameUser.setText(fullname);

        holder.txtFollowing.setText(R.string.select);

        holder.checkFollower.setVisibility(View.GONE);



        /*User image loading*/
        GlideApp.with(context)
                .load(getFollowerlistPojoArrayList.get(position).getPicture()).centerInside()
                .placeholder(R.drawable.ic_add_photo)
                .into(holder.imgFollowing);


        /*Follower layout on click listener*/
        holder.rlFollowerAyout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String friendid= getFollowerlistPojoArrayList.get(position).getFollowerId();
                single_frd_id.add(Integer.parseInt(friendid));
                String username= getFollowerlistPojoArrayList.get(position).getFirstName().concat(" ").
                        concat(getFollowerlistPojoArrayList.get(position).getLastName());

                holder.imgCheckedFollower.setVisibility(View.VISIBLE);


                Intent intent = new Intent(context, Create_new_chat_single.class);
                intent.putExtra("friendid",getFollowerlistPojoArrayList.get(position).getFollowerId());
                intent.putExtra("picture",getFollowerlistPojoArrayList.get(position).getPicture());
                intent.putExtra("username",getFollowerlistPojoArrayList.get(position).getFirstName().concat(" ").
                        concat(getFollowerlistPojoArrayList.get(position).getLastName()));
                context.startActivity(intent);
                ((AppCompatActivity)context).finish();

            }
        });





    }






    @Override
    public int getItemCount() {

        return getFollowerlistPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                Log.e("ChathistoryLog", "charString-->"+charString);


                if (charString.isEmpty()) {

                    getFollowerlistPojoArrayListFiltered = getFollowerlistPojoArrayList;
                } else {

                    ArrayList<Get_followerlist_pojo> filteredList = new ArrayList<>();

                    for (Get_followerlist_pojo followerlistPojo : getFollowerlistPojoArrayList) {


                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (followerlistPojo.getFirstName().toLowerCase().contains(charString.toLowerCase()) || followerlistPojo.getLastName().toLowerCase().contains(charSequence)) {
                            filteredList.add(followerlistPojo);


                        } else {
                            Log.e("ChathistoryLog", "performFiltering: Not matched");
                        }
                    }

                    getFollowerlistPojoArrayList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = getFollowerlistPojoArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                getFollowerlistPojoArrayList = (ArrayList<Get_followerlist_pojo>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtUsername, txtUserlastname, txtFullnameUser, txtFollowing, share, caption, captiontag, txt_created_on,txt_adapter_post;
        RelativeLayout rlFollowing, rlayoutFollowing, rlFollowerLay, rlFollowerAyout;

        ImageView imgFollowing, imgCheckedFollower;

         CheckBox checkFollower;


        public MyViewHolder(View itemView) {
            super(itemView);

            txtUsername = (TextView) itemView.findViewById(R.id.txt_username);
            txtUserlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txtFullnameUser = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            txtFollowing = (TextView) itemView.findViewById(R.id.txt_following);
            imgFollowing = (ImageView) itemView.findViewById(R.id.img_following);
            imgCheckedFollower = (ImageView) itemView.findViewById(R.id.img_checked_follower);
            checkFollower = (CheckBox) itemView.findViewById(R.id.check_follower);

            rlFollowerLay = (RelativeLayout) itemView.findViewById(R.id.rl_follower_lay);
            rlayoutFollowing = (RelativeLayout) itemView.findViewById(R.id.rlayout_following);
            rlFollowerAyout = (RelativeLayout) itemView.findViewById(R.id.rl_follower_ayout);


        }








    }




}
