package com.kaspontech.foodwall.adapters.events.EventsType;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.eventspackage.SelectFollowers;
import com.kaspontech.foodwall.eventspackage.ViewEvent;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventLikes.GetEventLikesUserPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetAllEventData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

/**
 * Created by balaji on 16/3/18.
 */

public class GetPublicEventsAdapter extends RecyclerView.Adapter<GetPublicEventsAdapter.MyViewHolder> {

    /**
     * Context
     **/
    Context context;
    /**
     * Variabe
     **/
    int event_id;
    /**
     * Arraylist event
     **/
    List<GetAllEventData> getAllEventDataList = new ArrayList<>();
    /**
     * Event pojo
     **/
    GetAllEventData getAllEventData;
    boolean interest_flag = false;
    private static int userInterestedFlag = 0;
    /**
     * Bottom sheet dialog
     **/
    BottomSheetDialog dialog;
    /**
     * Application TAG
     **/

    private static final String TAG = "GetPublicEventsAdapter";


    public GetPublicEventsAdapter(Context context, List<GetAllEventData> getAllEventDataList) {
        this.context = context;
        this.getAllEventDataList = getAllEventDataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView eventBackground;
        ImageButton interested;
        ImageButton going;
        ImageButton share;
        LinearLayout viewEvent;
        LinearLayout ll_interested;
        LinearLayout ll_going;
        LinearLayout ll_share;
        LinearLayout ll_goingText;
        LinearLayout ll_interestedText;
        TextView date;
        TextView location;
        TextView eventName;
        TextView interestedText;
        TextView goingText;
        TextView shareText;

        public MyViewHolder(View view) {
            super(view);

            eventBackground = (ImageView) view.findViewById(R.id.event_background);

            interested = (ImageButton) view.findViewById(R.id.interest_button);
            going = (ImageButton) view.findViewById(R.id.going_button);
            share = (ImageButton) view.findViewById(R.id.share_button);

            date = (TextView) view.findViewById(R.id.event_date);
            location = (TextView) view.findViewById(R.id.event_location);
            eventName = (TextView) view.findViewById(R.id.event_name);

            interestedText = (TextView) view.findViewById(R.id.interest_text);
            goingText = (TextView) view.findViewById(R.id.going_text);
            shareText = (TextView) view.findViewById(R.id.share_text);

            viewEvent = (LinearLayout) view.findViewById(R.id.ll_view_event);
            ll_interested = (LinearLayout) view.findViewById(R.id.ll_interested);
            ll_goingText = (LinearLayout) view.findViewById(R.id.ll_goingText);
            ll_interestedText = (LinearLayout) view.findViewById(R.id.ll_interestedText);
            ll_going = (LinearLayout) view.findViewById(R.id.ll_going);
            ll_share = (LinearLayout) view.findViewById(R.id.ll_share);


        }

    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_get_all_events, parent, false);

        Log.e(TAG, "onCreateViewHolder: " + getAllEventDataList.size());

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        Log.e("GetPublicEventsAdapter", "GetPublicEventsAdapter ->" + getAllEventDataList.get(position).getEventName());

        getAllEventData = getAllEventDataList.get(position);


        /*Event date getting*/
        String eventDate = getMonthName(getAllEventData.getStartDate()) + " " + getDate(getAllEventData.getStartDate());

        /*Image loading*/
        GlideApp.with(context)
                .load(getAllEventData.getEventImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.01f)
                .centerCrop()
                .into(holder.eventBackground);




        /*Event date setting*/
        holder.date.setText(eventDate);
        /*Event name setting*/
        holder.eventName.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getAllEventData.getEventName()));
        /*Event location*/
        holder.location.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getAllEventData.getLocation()));

        /*if(getAllEventData.getShareType() == 0){

            holder.ll_share.setVisibility(View.GONE);

        }else{

            holder.ll_share.setVisibility(View.VISIBLE);

        }*/

        /*Holder share on click listener*/
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SelectFollowers.class);
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", getAllEventData.getEventId());
                intent.putExtras(bundle);
                context.startActivity(intent);


            }
        });
        /*Holder view in click listener*/
        holder.viewEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetAllEventData getAllEventData = getAllEventDataList.get(position);

                Intent intent = new Intent(context, ViewEvent.class);
                Bundle bundle = new Bundle();
                bundle.putInt("eventId", getAllEventDataList.get(position).getEventId());
                bundle.putInt("eventType", getAllEventData.getEventType());
                intent.putExtras(bundle);
                context.startActivity(intent);


            }
        });


        if (getAllEventDataList.get(position).getEvtLikes() == 1) {

            //Success

            holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_pressed));
//                            interest_flag = true;
            getAllEventDataList.get(position).setUserInterested(true);


        }


        if (getAllEventDataList.get(position).getGngLikes() == 1) {

            //Success

            holder.going.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_going));
//                            interest_flag = true;
            getAllEventDataList.get(position).setUserGoing(true);
            holder.ll_interested.setVisibility(View.GONE);


        }

       /* if (getAllEventDataList.get(position).getEventId() == getAllEventDataList.get(position).getEventId()) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                int event_id = getAllEventDataList.get(position).getEventId();

                Call<GetEventLikesUserPojo> call = apiService.getEventsLikesUser("get_events_likes_user", userid, event_id);

                call.enqueue(new Callback<GetEventLikesUserPojo>() {
                    @Override
                    public void onResponse(Call<GetEventLikesUserPojo> call, Response<GetEventLikesUserPojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("getUserInterest", "responseStatus->" + responseStatus);


                        if (responseStatus.equals("success")) {

                            //Success

                            holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_pressed));
//                            interest_flag = true;
                            getAllEventDataList.get(position).isUserInterested(true);


                        } else if (responseStatus.equals("nodata")) {


                        }
                    }

                    @Override
                    public void onFailure(Call<GetEventLikesUserPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "FailureError" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }


        }


*/

        /*Interest on click listener*/
        holder.interested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getAllEventDataList.get(position).isUserInterested()) {
                    if (Utility.isConnected(context)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiService.createEventsLikes("create_events_likes", getAllEventDataList.get(position).getEventId(), 0, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("nodata")) {

                                        //Success
                                        holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star));
                                        getAllEventDataList.get(position).setUserInterested(false);
                                        interest_flag = false;


                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    } else {
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }


                } else if (!getAllEventDataList.get(position).isUserInterested()) {

                    if (Utility.isConnected(context)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiService.createEventsLikes("create_events_likes", getAllEventDataList.get(position).getEventId(), 1, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();


                                    if (responseStatus.equals("success")) {

                                        //Success

                                        holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_pressed));
                                        interest_flag = true;
                                        getAllEventDataList.get(position).setUserInterested(true);
                                        Toast.makeText(context, context.getString(R.string.intersetd), Toast.LENGTH_SHORT).show();

                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    } else {
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });


        /*Interest on click listener*/
        holder.ll_interested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getAllEventDataList.get(position).isUserInterested()) {

                    if (Utility.isConnected(context)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiService.createEventsLikes("create_events_likes", getAllEventDataList.get(position).getEventId(), 0, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("nodata")) {

                                        //Success
                                        holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star));
                                        getAllEventDataList.get(position).setUserInterested(false);
                                        interest_flag = false;


                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    } else {
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }


                } else if (!getAllEventDataList.get(position).isUserInterested()) {

                    if (Utility.isConnected(context)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiService.createEventsLikes("create_events_likes", getAllEventDataList.get(position).getEventId(), 1, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();


                                    if (responseStatus.equals("success")) {

                                        //Success

                                        holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_pressed));
                                        interest_flag = true;
                                        getAllEventDataList.get(position).setUserInterested(true);
                                        Toast.makeText(context, "Your interest has been saved", Toast.LENGTH_SHORT).show();

                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    } else {
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }


                }

            }
        });

        /*Going on click listener*/
        holder.going.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getAllEventDataList.get(position).isUserGoing()) {

                    View myView = LayoutInflater.from(context).inflate(R.layout.alert_create_events, null);
                    dialog = new BottomSheetDialog(context);
                    dialog.setContentView(myView);

                    LinearLayout interested = (LinearLayout) dialog.findViewById(R.id.ll_private_event);
                    LinearLayout not_going = (LinearLayout) dialog.findViewById(R.id.ll_public_event);


                    interested.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utility.isConnected(context)) {
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                    Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", getAllEventDataList.get(position).getEventId(), 0, userid);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("nodata")) {

                                                //Success
                                                holder.going.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_not_going));
                                                getAllEventDataList.get(position).setUserGoing(false);
                                                interest_flag = false;


                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            } else {
                                Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                            }


                            if (getAllEventDataList.get(position).isUserInterested()) {

                                holder.ll_interested.setVisibility(View.VISIBLE);
                                dialog.dismiss();

                            } else {

                                if (!getAllEventDataList.get(position).isUserInterested()) {

                                    if (Utility.isConnected(context)) {
                                        ApiInterface apiService1 = ApiClient.getClient().create(ApiInterface.class);

                                        try {

                                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                            Call<CommonOutputPojo> call = apiService1.createEventsLikes("create_events_likes", getAllEventDataList.get(position).getEventId(), 1, userid);

                                            call.enqueue(new Callback<CommonOutputPojo>() {
                                                @Override
                                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                    String responseStatus = response.body().getResponseMessage();


                                                    if (responseStatus.equals("success")) {

                                                        //Success

                                                        holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_pressed));
                                                        interest_flag = true;
                                                        getAllEventDataList.get(position).setUserInterested(true);
                                                        holder.ll_interested.setVisibility(View.VISIBLE);
                                                        dialog.dismiss();
                                                        Toast.makeText(context, "Your interest has been saved", Toast.LENGTH_SHORT).show();

                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                    //Error
                                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                                    dialog.dismiss();

                                                }
                                            });

                                        } catch (Exception e) {

                                            e.printStackTrace();

                                        }
                                    } else {
                                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                    }


                                }


                            }

                        }
                    });

                    not_going.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (Utility.isConnected(context)) {
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                    Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", getAllEventDataList.get(position).getEventId(), 0, userid);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("nodata")) {

                                                //Success
                                                holder.going.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_not_going));
                                                getAllEventDataList.get(position).setUserGoing(false);
                                                interest_flag = false;
                                                holder.ll_interested.setVisibility(View.VISIBLE);
                                                dialog.dismiss();


                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            } else {
                                Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                            }


                        }
                    });

                    dialog.show();


                } else if (!getAllEventDataList.get(position).isUserGoing()) {

                    if (Utility.isConnected(context)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", getAllEventDataList.get(position).getEventId(), 1, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();


                                    if (responseStatus.equals("success")) {

                                        //Success

                                        holder.going.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_going));
                                        interest_flag = true;
                                        getAllEventDataList.get(position).setUserGoing(true);
                                        Toast.makeText(context, "You are going to this event", Toast.LENGTH_SHORT).show();
                                        holder.ll_interested.setVisibility(View.GONE);

                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    } else {
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }


                }

            }
        });

        holder.ll_going.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getAllEventDataList.get(position).isUserGoing()) {

                    View myView = LayoutInflater.from(context).inflate(R.layout.alert_create_events, null);
                    dialog = new BottomSheetDialog(context);
                    dialog.setContentView(myView);

                    LinearLayout interested = (LinearLayout) dialog.findViewById(R.id.ll_private_event);
                    LinearLayout not_going = (LinearLayout) dialog.findViewById(R.id.ll_public_event);


                    interested.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utility.isConnected(context)) {
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                    Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", getAllEventDataList.get(position).getEventId(), 0, userid);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("nodata")) {

                                                //Success
                                                holder.going.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_not_going));
                                                getAllEventDataList.get(position).setUserGoing(false);
                                                interest_flag = false;


                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            } else {
                                Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                            }


                            if (getAllEventDataList.get(position).isUserInterested()) {

                                holder.ll_interested.setVisibility(View.VISIBLE);
                                dialog.dismiss();

                            } else {

                                if (!getAllEventDataList.get(position).isUserInterested()) {

                                    if (Utility.isConnected(context)) {
                                        ApiInterface apiService1 = ApiClient.getClient().create(ApiInterface.class);

                                        try {

                                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                            Call<CommonOutputPojo> call = apiService1.createEventsLikes("create_events_likes", getAllEventDataList.get(position).getEventId(), 1, userid);

                                            call.enqueue(new Callback<CommonOutputPojo>() {
                                                @Override
                                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                    String responseStatus = response.body().getResponseMessage();


                                                    if (responseStatus.equals("success")) {

                                                        //Success

                                                        holder.interested.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_pressed));
                                                        interest_flag = true;
                                                        getAllEventDataList.get(position).setUserInterested(true);
                                                        holder.ll_interested.setVisibility(View.VISIBLE);
                                                        dialog.dismiss();
                                                        Toast.makeText(context, "Your interest has been saved", Toast.LENGTH_SHORT).show();

                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                    //Error
                                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                                    dialog.dismiss();

                                                }
                                            });

                                        } catch (Exception e) {

                                            e.printStackTrace();

                                        }
                                    } else {
                                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                    }


                                }


                            }

                        }
                    });

                    not_going.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (Utility.isConnected(context)) {

                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                    Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", getAllEventDataList.get(position).getEventId(), 0, userid);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("nodata")) {

                                                //Success
                                                holder.going.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_not_going));
                                                getAllEventDataList.get(position).setUserGoing(false);
                                                interest_flag = false;
                                                holder.ll_interested.setVisibility(View.VISIBLE);
                                                dialog.dismiss();


                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            } else {
                                Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                            }


                        }
                    });

                    dialog.show();


                } else if (!getAllEventDataList.get(position).isUserGoing()) {

                    if (Utility.isConnected(context)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiService.createEventsGoing("create_events_going", getAllEventDataList.get(position).getEventId(), 1, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();


                                    if (responseStatus.equals("success")) {

                                        //Success

                                        holder.going.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_going));
                                        interest_flag = true;
                                        getAllEventDataList.get(position).setUserGoing(true);
                                        Toast.makeText(context, "You are going to this event", Toast.LENGTH_SHORT).show();
                                        holder.ll_interested.setVisibility(View.GONE);

                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    } else {
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }


                }

            }
        });


    }

    public void showBottomSheetDialog() {


    }

    private int getUserInterest(int event_id) {

        if (Utility.isConnected(context)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Log.e("getUserInterest", "userid->" + userid);
                Log.e("getUserInterest", "event_id->" + event_id);

                Call<GetEventLikesUserPojo> call = apiService.getEventsLikesUser("get_events_likes_user", userid, event_id);

                call.enqueue(new Callback<GetEventLikesUserPojo>() {
                    @Override
                    public void onResponse(Call<GetEventLikesUserPojo> call, Response<GetEventLikesUserPojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("getUserInterest", "responseStatus->" + responseStatus);


                        if (responseStatus.equals("success")) {

                            //Success
                            userInterestedFlag = 1;

                        } else if (responseStatus.equals("nodata")) {


                        }
                    }

                    @Override
                    public void onFailure(Call<GetEventLikesUserPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "FailureError" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }
        } else {
            Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }


        return userInterestedFlag;

    }

    /*Get month name*/

    private String getMonthName(String date) {

        String monthName = "";

        String month = date.substring(5, 7);

        switch (month) {


            case "01":
                monthName = "JAN";
                break;

            case "02":
                monthName = "FEB";
                break;

            case "03":
                monthName = "MAR";
                break;

            case "04":
                monthName = "APR";
                break;

            case "05":
                monthName = "MAY";
                break;

            case "06":
                monthName = "JUN";
                break;

            case "07":
                monthName = "JUL";
                break;

            case "08":
                monthName = "AUG";
                break;

            case "09":
                monthName = "SEP";
                break;

            case "10":
                monthName = "OCT";
                break;

            case "11":
                monthName = "NOV";
                break;

            case "12":
                monthName = "DEC";
                break;
        }


        return monthName;
    }

    /*Get date*/
    private String getDate(String date) {

        String day = date.substring(8, 10);

        return day;
    }

    @Override
    public int getItemCount() {
        return getAllEventDataList.size();
    }
}
