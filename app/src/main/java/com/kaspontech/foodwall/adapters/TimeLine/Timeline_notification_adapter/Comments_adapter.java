package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Comments_activity;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Reply_Like_activity;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Comments_reply_output_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_reply_all_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.GetallCommentsPojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;


/**
 * Created by vishnukm on 15/3/18.
 */

public class Comments_adapter extends RecyclerView.Adapter<Comments_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     **/
    Context context;

    /**
     * Comments reply adapter
     **/
    Comments_reply_adapter commentsReplyAdapter;
    /**
     * Comments all pojo
     **/
    private GetallCommentsPojo getallCommentsPojo;
    /**
     * Comments reply all pojo
     **/
    private Get_reply_all_pojo getReplyAllPojo;

    /**
     * Comments and reply all arraylists
     **/
    private ArrayList<GetallCommentsPojo> gettingcommentslist = new ArrayList<>();
    private ArrayList<Get_reply_all_pojo> gettingreplyalllist = new ArrayList<>();

    /**
     * Shared Preference
     **/

    private Pref_storage pref_storage;

    /**
     * Interface for comments activity
     **/
    Comments_activity commentsActivity;


    public Comments_adapter(Context c, ArrayList<GetallCommentsPojo> gettinglist) {
        this.context = c;
        this.gettingcommentslist = gettinglist;
        commentsActivity = (Comments_activity) context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_comments, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v, commentsActivity);
        vh.setIsRecyclable(false);

        /*Shared preference initialization*/
        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        /*Pojo initialization*/
        getallCommentsPojo = gettingcommentslist.get(position);

        /*User first and last name setting*/
        String firstname = gettingcommentslist.get(position).getFirstName().replace("\"", "");
        String lastname = gettingcommentslist.get(position).getLastName().replace("\"", "");

        String username = firstname + " " + lastname;
        holder.txt_username.setText(username);

        holder.txt_username.setTextColor(Color.parseColor("#000000"));

        /*Total likes count*/
        String txtRepliesLike = gettingcommentslist.get(position).getTotalCmmtLikes();

        /*User likes or not checking*/
        String liked = gettingcommentslist.get(position).getTlCmmtLikes();


        /* Likes display user */

        if (Integer.parseInt(liked) == 1) {

            holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
            gettingcommentslist.get(position).setLiked(true);

        } else if (Integer.parseInt(liked) == 0 || liked == null) {

            gettingcommentslist.get(position).setLiked(false);
            holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));

        }

        /* Likes display count display */

        if (Integer.parseInt(txtRepliesLike) == 0 || txtRepliesLike == null) {

            holder.txt_like.setVisibility(GONE);

        } else if (Integer.parseInt(txtRepliesLike) == 1) {

            holder.txt_like.setText(txtRepliesLike.concat(" ").concat("Like"));

        } else if (Integer.parseInt(txtRepliesLike) > 1) {

            holder.txt_like.setText(txtRepliesLike.concat(" ").concat("Likes"));
        }

        /*Total reply count*/
        String txtRepliesCount = gettingcommentslist.get(position).getTotalCmmtReply();

        if (Integer.parseInt(txtRepliesCount) == 0 || txtRepliesCount == null) {

            holder.txt_replies_count.setVisibility(GONE);
            holder.txt_view_all_replies.setVisibility(GONE);
            holder.view_reply.setVisibility(GONE);

        } else {

            holder.txt_replies_count.setText("(".concat(txtRepliesCount).concat(")"));

        }

        /* Edit visibility */

        if (gettingcommentslist.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

            holder.txt_edit.setVisibility(View.VISIBLE);
            holder.delete_comment.setVisibility(View.VISIBLE);

        } else {

            holder.txt_edit.setVisibility(View.GONE);
            holder.delete_comment.setVisibility(View.GONE);

        }

        /*Displaying user comments*/
        holder.txt_comments.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettingcommentslist.get(position).getTlComments().replace("\"", "")));

        holder.txt_comments.setTextColor(Color.parseColor("#000000"));


        /* Loading image url */

        Utility.picassoImageLoader(gettingcommentslist.get(position).getPicture(),
                0,holder.img_comment, context);



        /* Posted date functionality */


        String CreatedOn = gettingcommentslist.get(position).getCreatedOn();

        Utility.setTimeStamp(CreatedOn, holder.txt_ago);






        /* Users whom liked display */
        holder.txt_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, Reply_Like_activity.class);
                intent.putExtra("Comment_id", gettingcommentslist.get(position).getCmmtTlId());
                context.startActivity(intent);

            }
        });

        /* like for a comment */
        holder.img_comment_like.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                /*Start animation*/
                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);

                /*Dislike API calling*/
                if (gettingcommentslist.get(position).isLiked()) {

                    holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                    gettingcommentslist.get(position).setLiked(false);
                    gettingcommentslist.get(position).setTlCmmtLikes(String.valueOf(0));


                    int total_likes = Integer.parseInt(gettingcommentslist.get(position).getTotalCmmtLikes());

                    if (total_likes == 1) {

                        total_likes = total_likes - 1;
                        gettingcommentslist.get(position).setTotalCmmtLikes(String.valueOf(total_likes));
                        holder.txt_like.setVisibility(View.GONE);


                    } else if (total_likes == 2) {

                        total_likes = total_likes - 1;
                        gettingcommentslist.get(position).setTotalCmmtLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Like");
                        holder.txt_like.setVisibility(View.VISIBLE);

                    } else {

                        total_likes = total_likes - 1;
                        gettingcommentslist.get(position).setTotalCmmtLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Likes");
                        holder.txt_like.setVisibility(View.VISIBLE);

                    }

                    if(Utility.isConnected(context)){
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");
                            String clickedlike_timelineid = gettingcommentslist.get(position).getCmmtTlId();


                            Call<CommonOutputPojo> call = apiService.create_timeline_comments_likes("create_timeline_comments_likes",
                                    Integer.parseInt(clickedlike_timelineid),
                                    Integer.parseInt(createdby),
                                    0);
                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    if (response.body().getResponseCode() == 1) {






                                    }

                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }

                } else if (!gettingcommentslist.get(position).isLiked()) {

                    /*Like API calling*/

                    holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                    gettingcommentslist.get(position).setLiked(true);
                    gettingcommentslist.get(position).setTlCmmtLikes(String.valueOf(1));


                    int total_likes = Integer.parseInt(gettingcommentslist.get(position).getTotalCmmtLikes());

                    if (total_likes == 0) {

                        total_likes = total_likes + 1;
                        gettingcommentslist.get(position).setTotalCmmtLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Like");
                        holder.txt_like.setVisibility(View.VISIBLE);

                    } else {

                        total_likes = total_likes + 1;
                        gettingcommentslist.get(position).setTotalCmmtLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Likes");

                    }

                    if(Utility.isConnected(context)){

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");
                            String clickedlike_timelineid = gettingcommentslist.get(position).getCmmtTlId();


                            Call<CommonOutputPojo> call = apiService.create_timeline_comments_likes("create_timeline_comments_likes", Integer.parseInt(clickedlike_timelineid), Integer.parseInt(createdby), 1);
                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    if (response.body().getResponseCode() == 1) {



                                    }

                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else {
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

                    }

                }


            }
        });


        /* View replies */
        holder.txt_view_all_replies.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                holder.prog_reply.setVisibility(View.VISIBLE);
                /* holder.prog_reply.setIndeterminate(true);*/
                gettingreplyalllist.clear();
                String replycommentid = gettingcommentslist.get(position).getCmmtTlId();
                Pref_storage.setDetail(context, "cmmt_tl_id", replycommentid);
                String reply_cmntID = Pref_storage.getDetail(context, "cmmt_tl_id");


                if(Utility.isConnected(context)){
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        Call<Comments_reply_output_pojo> call = apiService.get_timeline_comment_reply_all("get_timeline_comment_reply_all", Integer.parseInt(reply_cmntID));
                        call.enqueue(new Callback<Comments_reply_output_pojo>() {
                            @Override
                            public void onResponse(Call<Comments_reply_output_pojo> call, Response<Comments_reply_output_pojo> response) {

                                if (response.body().getResponseCode() == 1) {
                                    holder.prog_reply.setVisibility(View.GONE);

                                    for (int j = 0; j < response.body().getData().size(); j++) {


                                        String Tl_commnt_reply = response.body().getData().get(j).getTlCmmtReply();
                                        String Total_comment_reply = response.body().getData().get(j).getTotalCmmtReply();
                                        String reply_id = response.body().getData().get(j).getReplyId();
                                        String CmmtTlId = response.body().getData().get(j).getCmmtTlId();
                                        String TlComments = response.body().getData().get(j).getTlComments();
                                        String fname = response.body().getData().get(j).getFirstName();
                                        String lname = response.body().getData().get(j).getLastName();
                                        String timeline_id = response.body().getData().get(j).getTimelineId();
                                        String pict = response.body().getData().get(j).getPicture();
                                        String createdBY = response.body().getData().get(j).getCreatedBy();
                                        String created_on = response.body().getData().get(j).getCreatedOn();
                                        String user_ID = response.body().getData().get(j).getUserId();

                                        getReplyAllPojo = new Get_reply_all_pojo(CmmtTlId, timeline_id, TlComments, Total_comment_reply, reply_id, Tl_commnt_reply, createdBY,
                                                created_on, user_ID, fname, lname, "", "", "", "", pict);

                                        gettingreplyalllist.add(getReplyAllPojo);
                                        commentsReplyAdapter = new Comments_reply_adapter(context, gettingreplyalllist);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                        holder.rv_comments_reply.setVisibility(View.VISIBLE);
                                        holder.rv_comments_reply.setAdapter(commentsReplyAdapter);
                                        holder.rv_comments_reply.setLayoutManager(mLayoutManager);
                                        holder.rv_comments_reply.setHasFixedSize(true);
                                        commentsReplyAdapter.notifyDataSetChanged();
                                        holder.rv_comments_reply.setNestedScrollingEnabled(false);
                                        holder.txt_view_all_replies.setVisibility(View.GONE);
                                        holder.txt_replies_count.setVisibility(View.GONE);
                                        holder.hide_replies.setVisibility(View.VISIBLE);


                                    }


                                }

                            }

                            @Override
                            public void onFailure(Call<Comments_reply_output_pojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {

                        e.printStackTrace();

                    }
                }else{
                    Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                }



            }


        });


        //hide reply if size is 0

      /*  if(gettingreplyalllist.size()==0){
            holder.hide_replies.setVisibility(View.GONE);
        }else {
            holder.hide_replies.setVisibility(View.VISIBLE);
        }*/


        /* hide reply */

        holder.hide_replies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gettingreplyalllist.clear();

                holder.rv_comments_reply.setVisibility(View.GONE);
                holder.hide_replies.setVisibility(View.GONE);
                holder.txt_view_all_replies.setVisibility(View.VISIBLE);
                holder.txt_replies_count.setVisibility(View.VISIBLE);

            }
        });

        /* reply for a comment */
        holder.txt_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                commentsActivity.clickedcommentposition(v, Integer.parseInt(gettingcommentslist.get(position).getCmmtTlId()));
                commentsActivity.clicked_username(v, gettingcommentslist.get(position).getFirstName(), Integer.parseInt(gettingcommentslist.get(position).getCmmtTlId()));
                commentsActivity.clickedforreply(v, 11);

            }
        });

        /* Edit a comment API */
        holder.txt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                commentsActivity.clickedcommentposition_timelineid(v, gettingcommentslist.get(position).getTlComments(), Integer.parseInt(gettingcommentslist.get(position).getCmmtTlId()), Integer.parseInt(gettingcommentslist.get(position).getTimelineId()));
                commentsActivity.clickedfor_edit(v, 22);


            }
        });


        /* Delete a comment API */
        holder.delete_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                commentsActivity.clickedfordelete();

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this comment?")
                        .setConfirmText("Delete")
                        .setCancelText("Cancel")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                try {
                                    String createuserid = gettingcommentslist.get(position).getUserId();
                                    String timeline_commentid = gettingcommentslist.get(position).getTimelineId();
                                    String commentid = gettingcommentslist.get(position).getCmmtTlId();

                                    Call<CreateUserPojoOutput> call = apiService.create_delete_timeline_comments("create_delete_timeline_comments", Integer.parseInt(commentid), Integer.parseInt(timeline_commentid), Integer.parseInt(createuserid));
                                    call.enqueue(new Callback<CreateUserPojoOutput>() {
                                        @Override
                                        public void onResponse(@NonNull Call<CreateUserPojoOutput> call, @NonNull Response<CreateUserPojoOutput> response) {

                                            if (response.body() != null && response.body().getResponseCode() == 1) {


                                                /*Delete position*/

                                                removeAt(position);

                                                Toast.makeText(context, "Deleted!!", Toast.LENGTH_LONG).show();

                                                //                                                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                //                                                        .setTitleText("Deleted!!")
                                                //
                                                //                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                //                                                            @Override
                                                //                                                            public void onClick(SweetAlertDialog sDialog) {
                                                //                                                                sDialog.dismissWithAnimation();
                                                //
                                                //
                                                //                                                            }
                                                //                                                        })
                                                //                                                        .show();


                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "" + t.getMessage());
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }
                        })
                        .show();


            }
        });

    }
    /*Remove postion after deletion*/
    private void removeAt(int position) {
        gettingcommentslist.remove(position);
        notifyDataSetChanged();
        /*notifyItemRemoved(position);
        notifyItemRangeChanged(position, gettingcommentslist.size());*/
    }

    @Override
    public int getItemCount() {

        commentsActivity.checksize(gettingcommentslist.size());

        return gettingcommentslist.size();

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    /*View holder*/
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view_reply;
        ImageView img_comment;
        ProgressBar prog_reply;
        LinearLayout rl_comment_layout;
        RecyclerView rv_comments_reply;
        ImageButton img_comment_like, delete_comment;

        TextView txt_username, txt_edit,
                txt_like, txt_ago, txt_reply, txt_comments, txt_view_all_replies, hide_replies, txt_replies_count;


        /* Interface activity */
        Comments_activity comments_activity;



        public MyViewHolder(View itemView, Comments_activity comments_activity) {
            super(itemView);


            this.comments_activity = comments_activity;

            view_reply = (View) itemView.findViewById(R.id.view_reply);
            /*Textview Initialization*/
            txt_ago = (TextView) itemView.findViewById(R.id.txt_ago);
            txt_like = (TextView) itemView.findViewById(R.id.txt_like);
            txt_edit = (TextView) itemView.findViewById(R.id.txt_edit);
            txt_reply = (TextView) itemView.findViewById(R.id.txt_reply);
            hide_replies = (TextView) itemView.findViewById(R.id.hide_replies);
            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_comments = (TextView) itemView.findViewById(R.id.txt_comments);
            txt_replies_count = (TextView) itemView.findViewById(R.id.txt_replies_count);
            rl_comment_layout = (LinearLayout) itemView.findViewById(R.id.rl_comment_layout);
            txt_view_all_replies = (TextView) itemView.findViewById(R.id.txt_view_all_replies);

            /*ImageButton for deleted comment Initialization*/

            delete_comment = (ImageButton) itemView.findViewById(R.id.del_comment);

            /*ImageButton for comment like  Initialization*/
            img_comment_like = (ImageButton) itemView.findViewById(R.id.img_comment_like);

            /*Imageview for comment initialization*/
            img_comment = (ImageView) itemView.findViewById(R.id.img_comment);
            /*Loader*/
            prog_reply = (ProgressBar) itemView.findViewById(R.id.prog_reply);
            /*Commentes reply recyclerview*/
            rv_comments_reply = (RecyclerView) itemView.findViewById(R.id.rv_comments_reply);


            txt_reply.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

//            comments_activity.clickedcommentposition(v, getAdapterPosition());

        }


    }


}


