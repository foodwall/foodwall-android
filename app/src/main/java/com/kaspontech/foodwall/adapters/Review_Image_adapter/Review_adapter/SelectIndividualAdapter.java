package com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.reviewpackage.Reviews;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SelectIndividualAdapter extends RecyclerView.Adapter<SelectIndividualAdapter.MyViewHolder> {


    Context context;
    Reviews reviews;
    List<GetFollowersData> getFollowersDataList = new ArrayList<>();
    List<Integer> invitedPeopleList = new ArrayList<>();
    GetFollowersData getFollowersData;

   public static List<Integer> friendsList_individual = new ArrayList<>();

    public SelectIndividualAdapter(Context context, List<GetFollowersData> getFollowersDataList, List<Integer> invitedPeopleList) {
        this.context = context;
        this.getFollowersDataList = getFollowersDataList;
        this.invitedPeopleList = invitedPeopleList;

    }



    @NonNull
    @Override
    public SelectIndividualAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_select_followers, parent, false);
        return new SelectIndividualAdapter.MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final SelectIndividualAdapter.MyViewHolder holder, int position) {

        getFollowersData = getFollowersDataList.get(position);

        Glide.with(context).load(getFollowersData.getPicture()).into(holder.followerPhoto);
        holder.userName.setText(getFollowersData.getFirstName() + " " + getFollowersData.getLastName());


        for (Integer value : invitedPeopleList) {

            if (getFollowersData.getFollowerId().contains(value.toString())) {

                holder.selectFollowerBox.setChecked(true);

                friendsList_individual.add(Integer.valueOf(getFollowersDataList.get(position).getFollowerId()));

                Log.e("friends","list" + friendsList_individual.toString());

            }








        }

    }

    @Override
    public int getItemCount() {
        return getFollowersDataList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        CircleImageView followerPhoto;
        TextView userName;
        CheckBox selectFollowerBox;


        public MyViewHolder(View view) {
            super(view);

            followerPhoto = (CircleImageView) view.findViewById(R.id.user_image);
            userName = (TextView) view.findViewById(R.id.user_name);
            selectFollowerBox = (CheckBox) view.findViewById(R.id.followers_checked);
            selectFollowerBox.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {






//            createEvent.getInvitedPeople(view, getAdapterPosition());

        }
    }
}

