package com.kaspontech.foodwall.adapters.chat;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;

public class Grpicon_change_adpter extends RecyclerView.ViewHolder {


    TextView txtChangeGrpIcon;
    ImageView img_grpicon_change_frm, imgToArrow,img_grpicon_change_to;
    RelativeLayout rlGrpIconChange;


    public Grpicon_change_adpter(View itemView) {
        super(itemView);

        txtChangeGrpIcon = (TextView) itemView.findViewById(R.id.txt_change_grp_icon);
        img_grpicon_change_frm = (ImageView) itemView.findViewById(R.id.img_grpicon_change_frm);
        imgToArrow = (ImageView) itemView.findViewById(R.id.img_to_arrow);
//        img_grpicon_change_to = (ImageView) itemView.findViewById(R.id.img_grpicon_change_to);

        rlGrpIconChange = (RelativeLayout) itemView.findViewById(R.id.rl_grp_icon_change);


    }
}
