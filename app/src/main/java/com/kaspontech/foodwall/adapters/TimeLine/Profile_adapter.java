package com.kaspontech.foodwall.adapters.TimeLine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_profile_details_Pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;

/**
 * Created by vishnukm on 26/3/18.
 */

public class Profile_adapter extends RecyclerView.Adapter<Profile_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {



    Context context;

    ArrayList<Get_profile_details_Pojo> gettingprofilelist = new ArrayList<>();
    Get_profile_details_Pojo getProfileDetailsPojo;
    Bitmap bitmapimage;
    String imagebitmap;
    Handler doubleHandler;
    Pref_storage pref_storage;
    boolean  doubleClick = false;
    public Profile_adapter(Context c, ArrayList<Get_profile_details_Pojo> gettinglist) {
        this.context = c;
        this.gettingprofilelist = gettinglist;
    }

    @NonNull
    @Override
    public Profile_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_comments, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Profile_adapter.MyViewHolder vh = new Profile_adapter.MyViewHolder(v);

        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Profile_adapter.MyViewHolder holder, final int position) {


        getProfileDetailsPojo = gettingprofilelist.get(position);
        String firstname= getProfileDetailsPojo.getFirstName().replace("\"","");
        String lastname= getProfileDetailsPojo.getLastName().replace("\"","");

        holder.txt_username.setText(firstname);

        holder.txt_username.setTextColor(Color.parseColor("#000000"));

        holder.txt_userlastname.setText(lastname);

        holder.txt_userlastname.setTextColor(Color.parseColor("#000000"));

//        holder.txt_comments.setText(getProfileDetailsPojo.getTl_comments().replace("\"",""));

        holder.txt_comments.setTextColor(Color.parseColor("#000000"));


//        String ago =getProfileDetailsPojo.getCreated_on();
//        Log.e("vishnucreatedon", "creat\t" + ago);

  /*      try {
            long now = System.currentTimeMillis();

            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convertedDate = dateFormat.parse(ago);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now,

                    DateUtils.FORMAT_ABBREV_RELATIVE);

            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")){
                holder.txt_ago.setText(R.string.just_now);
            }else {
                holder.txt_ago.append(relavetime1 + "\n\n");
                System.out.println(relavetime1);
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

*/


        holder.img_comment_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                holder.img_comment_like.showAnim();


            }});


/*
        holder.txt_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittxt_commentnew.requestFocus();

            }});*/




    }




    @Override
    public int getItemCount() {

        return gettingprofilelist.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_username, txt_userlastname, txt_comments, txt_ago, txt_reply;
        ImageView img_comment,userphoto_commentnew;
        RelativeLayout rl_postlayout,rl_comment_layout;
        EditText edittxt_commentnew;
        ImageButton img_comment_like;


        public MyViewHolder(View itemView) {
            super(itemView);
            rl_comment_layout= (RelativeLayout)itemView.findViewById(R.id.rl_comment_layout);
            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_userlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txt_comments = (TextView) itemView.findViewById(R.id.txt_comments);
            txt_ago = (TextView) itemView.findViewById(R.id.txt_ago);
            txt_reply = (TextView) itemView.findViewById(R.id.txt_reply);
            img_comment_like = (ImageButton) itemView.findViewById(R.id.img_comment_like);
            img_comment = (ImageView) itemView.findViewById(R.id.img_comment);






        }








    }


}


