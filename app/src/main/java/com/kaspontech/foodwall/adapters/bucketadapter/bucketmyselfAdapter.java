package com.kaspontech.foodwall.adapters.bucketadapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.bucketlistpackage.CreateBucketlistActivty;
import com.kaspontech.foodwall.bucketlistpackage.GetmyBucketInputPojo;
import com.kaspontech.foodwall.bucketlistpackage.IndividualBucketlistDetails;
import com.kaspontech.foodwall.bucketlistpackage.Individual_bucket_fragment;
import com.kaspontech.foodwall.bucketlistpackage.Mybucketfragment;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.reviewpackage.ReviewsLikesAllActivity;
import com.kaspontech.foodwall.reviewpackage.alldishtab.AllDishDisplayActivity;
import com.kaspontech.foodwall.reviewpackage.ReviewDeliveryDetails;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewAmbienceAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewAvoidDishAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewTopDishAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_in_detail_activity;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.kaspontech.foodwall.utills.Utility.hideKeyboard;
import static java.lang.Integer.parseInt;

//

public class bucketmyselfAdapter extends RecyclerView.Adapter<bucketmyselfAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     **/
    private Context context;
    /**
     * My bucket arraylist
     **/
    private ArrayList<GetmyBucketInputPojo> getallHotelReviewPojoArrayList = new ArrayList<>();
    /**
     * More options
     **/
    private final String[] itemsothers = {"Bucket list options", "Delete Review", "Share", "Share Chat"};
    /**
     * Top dish arraylist
     **/
    public ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();

    /**
     * Avoid dish arraylist
     **/
    public ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();
    /**
     * Ambience arraylist
     **/
    public ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

    Mybucketfragment listsize;

    private String loginUserId;

    private int selectedPosition = -1;


    public bucketmyselfAdapter(Context c, ArrayList<GetmyBucketInputPojo> gettinglist, String loginUseridView) {
        this.context = c;
        this.getallHotelReviewPojoArrayList = gettinglist;
        this.loginUserId = loginUseridView;

    }


    @NonNull
    @Override
    public bucketmyselfAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_bucket_list, parent, false);
        // set the view's size, margins, paddings and layout parameters
        bucketmyselfAdapter.MyViewHolder vh = new bucketmyselfAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final bucketmyselfAdapter.MyViewHolder holder, final int position) {


        try {

            /*Display hotel details*/
            displayReviewItem(getallHotelReviewPojoArrayList, holder, position);
            Log.e("responseStatus", "responseSize->" + loginUserId);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    /*Display hotel details*/


    @SuppressLint("SetTextI18n")
    public void displayReviewItem(final ArrayList<GetmyBucketInputPojo> reviewList, final bucketmyselfAdapter.MyViewHolder holder, final int position) {


        if (reviewList.size() > 0) {


            holder.rl_bucketlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, IndividualBucketlistDetails.class);
                    if (getallHotelReviewPojoArrayList.get(position).getBucketCreatedBy().equals(loginUserId)) {
                        intent.putExtra("inv_userid", getallHotelReviewPojoArrayList.get(position).getToUserId());
                        intent.putExtra("inv_username", holder.txt_usernamechat.getText().toString());
                    } else {
                        intent.putExtra("inv_userid", getallHotelReviewPojoArrayList.get(position).getRevUserId());
                        intent.putExtra("inv_username", holder.txt_usernamechat.getText().toString());
                    }
                    context.startActivity(intent);
                    notifyDataSetChanged();
                }

            });


            if (getallHotelReviewPojoArrayList.get(position).getBucketCreatedBy().equals(loginUserId)) {

                //reviewer_name
                String reviewer_lname = reviewList.get(position).getToLastName();
                String reviewer_fname = org.apache.commons.text.StringEscapeUtils.unescapeJava(getallHotelReviewPojoArrayList.get(position).getToFirstName());
                //  holder.username.setText(reviewer_fname);
                holder.txt_usernamechat.setText(reviewer_fname + " " + reviewer_lname);
                //holder.userlastname.setText(reviewer_lname);
                holder.txt_usernamechat.setTextColor(Color.parseColor("#000000"));
                holder.userlastname.setTextColor(Color.parseColor("#000000"));

                // load userphoto_profile
                if (getallHotelReviewPojoArrayList.get(position).getToPicture().equals("null") ||
                        getallHotelReviewPojoArrayList.get(position).getToPicture().equals("0")) {

                    GlideApp.with(context)
                            .load(getallHotelReviewPojoArrayList.get(position).getToPicture())
                            .error(R.drawable.ic_add_photo)
                            .into(holder.userphoto_profile);
                } else {

                    GlideApp.with(context)
                            .load(getallHotelReviewPojoArrayList.get(position).getToPicture())
                            .into(holder.userphoto_profile);
                }

            } else {

                //reviewer_name
                String reviewer_lname = reviewList.get(position).getRevLastName();
                //reviewer_name
                String reviewer_fname = org.apache.commons.text.StringEscapeUtils.unescapeJava(getallHotelReviewPojoArrayList.get(position).getRevFirstName());
                //  holder.username.setText(reviewer_fname);
                holder.txt_usernamechat.setText(reviewer_fname + reviewer_lname);
                //holder.userlastname.setText(reviewer_lname);
                holder.txt_usernamechat.setTextColor(Color.parseColor("#000000"));
                holder.userlastname.setTextColor(Color.parseColor("#000000"));

                // load userphoto_profile
                if (getallHotelReviewPojoArrayList.get(position).getToPicture().equals("null") ||
                        getallHotelReviewPojoArrayList.get(position).getToPicture().equals("0")) {

                    GlideApp.with(context)
                            .load(getallHotelReviewPojoArrayList.get(position).getRevPicture())
                            .error(R.drawable.ic_add_photo)
                            .into(holder.userphoto_profile);
                } else {

                    GlideApp.with(context)
                            .load(getallHotelReviewPojoArrayList.get(position).getRevPicture())
                            .into(holder.userphoto_profile);
                }
            }

            /* Timestamp for review */
            String createdOn = reviewList.get(position).getBucketCreatedOn();
            Utility.setTimeStamp(createdOn, holder.txt_created_on);


            holder.userphoto_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (getallHotelReviewPojoArrayList.get(position).getBucketCreatedBy().equals(loginUserId)) {
                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getToUserId());
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getInvUserId());
                        context.startActivity(intent);
                    }
                }
            });


            holder.txt_usernamechat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (getallHotelReviewPojoArrayList.get(position).getBucketCreatedBy().equals(loginUserId)) {
                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getToUserId());
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getInvUserId());
                        context.startActivity(intent);
                    }

                }
            });


            holder.userlastname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (getallHotelReviewPojoArrayList.get(position).getBucketCreatedBy().equals(loginUserId)) {
                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getToUserId());
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getInvUserId());
                        context.startActivity(intent);
                    }

                   /* if (getallHotelReviewPojoArrayList.get(position).getIndividualUserid().equalsIgnoreCase(
                            getallHotelReviewPojoArrayList.get(position).getBucketCreatedBy())) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getUserId());
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getInvUserId());
                        context.startActivity(intent);
                    }*/
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return getallHotelReviewPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        EmojiconEditText review_comment;

        ViewPager ambi_viewpager;

        EmojiconTextView txt_usernamechat;

        RelativeLayout rl_bucketlist;

        TextView userlastname, txt_created_on;

        CircleImageView userphoto_profile;

        public MyViewHolder(View itemView) {

            super(itemView);


            txt_usernamechat = (EmojiconTextView) itemView.findViewById(R.id.txt_usernamechat);

            userlastname = (TextView) itemView.findViewById(R.id.userlastname);

            txt_created_on = (TextView) itemView.findViewById(R.id.txt_created_on);

            ambi_viewpager = (ViewPager) itemView.findViewById(R.id.ambi_viewpager);

            review_comment = (EmojiconEditText) itemView.findViewById(R.id.review_comment);

            rl_bucketlist = (RelativeLayout) itemView.findViewById(R.id.rl_bucketlist);

            userphoto_profile = (CircleImageView) itemView.findViewById(R.id.userphoto_profile);

        }


    }


}

