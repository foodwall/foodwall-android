package com.kaspontech.foodwall.adapters.TimeLine;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_timeline_image_all;
import com.kaspontech.foodwall.profilePackage.Image_self_activity;
import com.kaspontech.foodwall.R;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.SquareImageView;

import java.util.ArrayList;

public class TimeLineImagesUser extends RecyclerView.Adapter<TimeLineImagesUser.MyViewHolder> {


    public Context context;
    public ArrayList<String> imageUrl = new ArrayList<>();
    public ArrayList<Get_timeline_image_all> allimagelist = new ArrayList<>();



    public TimeLineImagesUser(Context context, ArrayList<String> imageUrl,ArrayList<Get_timeline_image_all> allimagelist) {
        this.context = context;
        this.imageUrl = imageUrl;
        this.allimagelist = allimagelist;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        SquareImageView gridImageView;

        public MyViewHolder(View view) {
            super(view);

            gridImageView = (SquareImageView) view.findViewById(R.id.gridImageView);

        }


        @Override
        public void onClick(View view) {


        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_grid_imageview, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {


        String image = imageUrl.get(position);
        final String timelineid = allimagelist.get(position).getTimelineid();
        final String userID = allimagelist.get(position).getUserid();


        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(30));

//        Utility.picassoImageLoader(image,1,holder.gridImageView);


        GlideApp.with(context)
                .load(image)
                .apply(requestOptions)
                .error(R.drawable.ic_no_post)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(holder.gridImageView);

        holder.gridImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, Image_self_activity.class);

                intent.putExtra("Clickedfrom_profile", timelineid);
                intent.putExtra("Clickedfrom_profileuserid", userID);
                context.startActivity(intent);
            }
        });

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return imageUrl.size();
    }


}
