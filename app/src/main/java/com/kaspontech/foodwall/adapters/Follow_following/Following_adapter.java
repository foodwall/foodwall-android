package com.kaspontech.foodwall.adapters.Follow_following;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.followingPackage.Following_listActivity;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_unfollow_follower_output;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class Following_adapter extends RecyclerView.Adapter<Following_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener  {


    private Context context;

    private ArrayList<Get_following_profile_pojo> gettingfollowinglist = new ArrayList<>();
    private Get_followerlist_pojo getFollowerlistPojo;

    private Get_following_profile_pojo getFollowingProfilePojo;

    public Following_adapter(Context c, ArrayList<Get_following_profile_pojo> gettinglist) {
        this.context = c;
        this.gettingfollowinglist = gettinglist;
    }

    @NonNull
    @Override
    public Following_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_followinglist, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Following_adapter.MyViewHolder vh = new Following_adapter.MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Following_adapter.MyViewHolder holder, final int position) {


        getFollowingProfilePojo = gettingfollowinglist.get(position);

        holder.txt_username.setText(getFollowingProfilePojo.getFirst_name());
        holder.txt_username.setTextColor(Color.parseColor("#000000"));

        holder.txt_userlastname.setText(getFollowingProfilePojo.getLast_name());
        holder.txt_userlastname.setTextColor(Color.parseColor("#000000"));

        String fullname= getFollowingProfilePojo.getFirst_name().concat(" ").concat(getFollowingProfilePojo.getLast_name());

        holder.txt_fullname_user.setText(fullname);

        String ownuser_id= Pref_storage.getDetail(context, "userId");

        String positionuser_id= gettingfollowinglist.get(position).getUser_id();

        Pref_storage.setDetail(context,"ClickedfollowingID",positionuser_id);


        holder.rl_following.setVisibility(View.GONE);

        if(Integer.parseInt(positionuser_id)==Integer.parseInt(ownuser_id)){


//            holder.rl_following.setVisibility(View.GONE);
//            gettingfollowinglist.remove(position);

            holder.rl_follower_lay.setVisibility(View.GONE);
            holder.rl_follower_ayout.setVisibility(View.GONE);

        }
        else{

            holder.txt_following.setText(R.string.unfollow);
            holder.rl_follower_lay.setVisibility(View.VISIBLE);
            holder.rl_follower_ayout.setVisibility(View.VISIBLE);


        }

        Utility.picassoImageLoader(gettingfollowinglist.get(position).getPicture(),0,holder.img_following, context);


        /*GlideApp.with(context)
                .load(gettingfollowinglist.get(position).getPicture())
                .into(holder.img_following);*/


        /*holder.rl_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setCustomImage(R.drawable.ic_add_photo_profile)

                        .setTitleText("Are you sure to unfollow @ ".concat(gettingfollowinglist.get(position).getFirst_name().concat(" ").concat(gettingfollowinglist.get(position).getLast_name())))
                        .setConfirmText("Unfollow")
                        .setCancelText("No,cancel")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();


                String positionuser_id= gettingfollowinglist.get(position).getFollowing_id();
                                Toast.makeText(context, ""+positionuser_id, Toast.LENGTH_SHORT).show();
                Pref_storage.setDetail(context,"ClickedpositionID",positionuser_id);

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {

                    String createdby = Pref_storage.getDetail(context, "userId");

                    String going_to_unfollow_user_id= Pref_storage.getDetail(context, "ClickedpositionID");

                    Call<Get_unfollow_follower_output> call = apiService.create_unfollow("create_follower",Integer.parseInt(createdby) , Integer.parseInt(going_to_unfollow_user_id),0);
                    call.enqueue(new Callback<Get_unfollow_follower_output>() {
                        @Override
                        public void onResponse(Call<Get_unfollow_follower_output> call, Response<Get_unfollow_follower_output> response) {


                            if (response.body().getData().equalsIgnoreCase("0")) {


                                context.startActivity(new Intent(context, Following_listActivity.class));
                                ((AppCompatActivity) context).finish();

                            }

                        }

                        @Override
                        public void onFailure(Call<Get_unfollow_follower_output> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                            }
                        })
                        .show();

            }
        });
*/
        holder.img_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String user_id = gettingfollowinglist.get(position).getFollowing_id();

                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }

            }
        });


        /// Last name click listener

        holder.txt_userlastname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String user_id = gettingfollowinglist.get(position).getFollowing_id();

                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }

            }
        });
   /// First name click listener

        holder.txt_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String user_id = gettingfollowinglist.get(position).getFollowing_id();

                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }

            }
        });



    }



    @Override
    public int getItemCount() {

        return gettingfollowinglist.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_username, txt_userlastname, txt_fullname_user, txt_following, share, caption, captiontag, txt_created_on,txt_adapter_post;
        RelativeLayout rl_following,rl_likesnew_layout,rl_follower_lay,rl_follower_ayout;

        ImageView img_following;



        public MyViewHolder(View itemView) {
            super(itemView);

            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_userlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txt_fullname_user = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            txt_following = (TextView) itemView.findViewById(R.id.txt_following);
            img_following = (ImageView) itemView.findViewById(R.id.img_following);
            rl_following = (RelativeLayout) itemView.findViewById(R.id.rlayout_following);
            rl_follower_lay = (RelativeLayout) itemView.findViewById(R.id.rl_follower_lay);
            rl_follower_ayout = (RelativeLayout) itemView.findViewById(R.id.rl_follower_ayout);


        }



    }




}
