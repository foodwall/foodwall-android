package com.kaspontech.foodwall.adapters.Follow_following;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_timeline_image_all;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.New_Getting_timeline_all_pojo;
import com.kaspontech.foodwall.R;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import static com.kaspontech.foodwall.profilePackage.profileTabFragments.UserTimelineImagesFragment.totalsize_user;
import static com.kaspontech.foodwall.profilePackage.User_profile_Activity.allimagelist_user;

//

public class Timeline_userimageall_adapter extends BaseAdapter implements GridView.OnItemClickListener {


    private Context mContext;
    public static ArrayList<New_Getting_timeline_all_pojo> gettingtimelinealllist = new ArrayList<>();
    private ArrayList<Get_timeline_image_all> allimagelist_useradapter = new ArrayList<>();
    private New_Getting_timeline_all_pojo newGettingTimelineAllPojo;
    ArrayList<String> f = new ArrayList<String>();// list of file paths
    java.io.File[] listFile;

    private String[] filepath;
    private String[] filename;
    private static android.view.LayoutInflater inflater = null;


    public Timeline_userimageall_adapter(Context c, ArrayList<Get_timeline_image_all> gettinglist) {
        this.mContext = c;

        allimagelist_useradapter = gettinglist;
        inflater = (android.view.LayoutInflater) c
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    // Keep all Images in array
    public Integer[] mThumbIds = {


            R.drawable.oldman,
            R.drawable.balaji,
            R.drawable.virat,
            R.drawable.burger,


    };


    // Constructor
    public Timeline_userimageall_adapter(Context c) {
        mContext = c;
    }

    @Override
    public int getCount() {
        return totalsize_user;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {

//        Toast.makeText(mContext, ""+allimagelist_useradapter.get(position).getTimelineid().toString(), Toast.LENGTH_SHORT).show();
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);

        Utility.picassoImageLoader(allimagelist_useradapter.get(position).getTimelineimage(),1,imageView, mContext);


//        GlideApp.with(mContext).load(allimagelist_useradapter.get(position).getTimelineimage().toString()).centerCrop().into(imageView);
        imageView.setLayoutParams(new GridView.LayoutParams(250, 250));
        imageView.setPadding(5, 3, 5, 3);
        return imageView;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Pref_storage.setDetail(mContext, "user_timeline_clicked", allimagelist_user.get(position).getTimelineid().toString());
    }
}
