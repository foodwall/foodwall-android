package com.kaspontech.foodwall.adapters.chat;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.chatpackage.Add_member_group;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ChatAddmemberInputPojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Utility;


import java.util.ArrayList;
import java.util.HashMap;

//

public class ChatMemberFollowerAdapter extends RecyclerView.Adapter<ChatMemberFollowerAdapter.MyViewHolder> {

    /**
     * Context
     **/
    private Context context;
    /**
     * Chat Addmember Input Pojo
     **/
    ChatAddmemberInputPojo chatAddmemberInputPojo;
    /**
     * Chat Addmember Input Pojo arraylist
     **/
    private ArrayList<ChatAddmemberInputPojo> chatAddmemberInputPojoArrayList = new ArrayList<>();


    /**
     * Followers id list
     **/
    HashMap<String, Integer> selectedfollowerlistId = new HashMap<>();

    /**
     * Add member group pojo
     **/
    Add_member_group addMemberGroup;


    public ChatMemberFollowerAdapter(Context c, ArrayList<ChatAddmemberInputPojo> gettinglist, HashMap<String, Integer> grpfollowerId) {
        this.context = c;
        this.chatAddmemberInputPojoArrayList = gettinglist;
        this.selectedfollowerlistId = grpfollowerId;
        this.addMemberGroup=((Add_member_group)context);
    }

    @NonNull
    @Override
    public ChatMemberFollowerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_chat_followerlist, parent, false);
        // set the view's size, margins, paddings and layout parameters

        return new ChatMemberFollowerAdapter.MyViewHolder(v,addMemberGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChatMemberFollowerAdapter.MyViewHolder holder, final int position) {


        chatAddmemberInputPojo = chatAddmemberInputPojoArrayList.get(position);


        /*User name displaying*/
        holder.txtUsername.setText(chatAddmemberInputPojoArrayList.get(position).getFirstName());

        holder.txtUsername.setTextColor(Color.parseColor("#000000"));

        holder.txtUserlastname.setText(chatAddmemberInputPojoArrayList.get(position).getLastName());
        holder.txtUserlastname.setTextColor(Color.parseColor("#000000"));

        String fullname= chatAddmemberInputPojoArrayList.get(position).getFirstName().concat(" ").
                concat(chatAddmemberInputPojoArrayList.get(position).getFirstName());

        holder.txtFullnameUser.setText(fullname);


        /*User image displaying*/

        Utility.picassoImageLoader(chatAddmemberInputPojoArrayList.get(position).getPicture(),
                0,holder.imgFollowing,context );

//        GlideApp.with(context)
//                .load(chatAddmemberInputPojoArrayList.get(position).getPicture()).centerInside().placeholder(R.drawable.ic_add_photo).into(holder.imgFollowing);

/*
holder.rlFollowerAyout.setOnLongClickListener(new View.OnLongClickListener() {
    @Override
    public boolean onLongClick(View v) {
        String friendid= getFollowerlistPojoArrayList.get(position).getFollower_id();
        String username= getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                concat(getFollowerlistPojoArrayList.get(position).getLast_name());

        friendsList_chat.add(Integer.valueOf(friendid));

        Log.e("friendsList-->",""+friendsList_chat);

        holder.imgCheckedFollower.setVisibility(View.VISIBLE);

        return false;
    }
});
*/



/*holder.rlFollowerAyout.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        String friendid= getFollowerlistPojoArrayList.get(position).getFollower_id();
        String username= getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                concat(getFollowerlistPojoArrayList.get(position).getLast_name());

        friendsList_chat.add(Integer.valueOf(friendid));

        Log.e("friendsList-->",""+friendsList_chat);

        holder.imgCheckedFollower.setVisibility(View.VISIBLE);
    }
});*/

      /*  holder.rlFollowerAyout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String friendid= getFollowerlistPojoArrayList.get(position).getFollower_id();
                String username= getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                        concat(getFollowerlistPojoArrayList.get(position).getLast_name());

                friendsList_chat.add(Integer.valueOf(friendid));

                Log.e("friendsList-->",""+friendsList_chat);

                holder.imgCheckedFollower.setVisibility(View.VISIBLE);


                *//*Intent intent = new Intent(context, Create_new_chat_single.class);
                intent.putExtra("friendid",getFollowerlistPojoArrayList.get(position).getFollower_id());
                intent.putExtra("picture",getFollowerlistPojoArrayList.get(position).getPicture());
                intent.putExtra("username",getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                        concat(getFollowerlistPojoArrayList.get(position).getLast_name()));
                context.startActivity(intent);
                ((AppCompatActivity)context).finish();*//*

            }
        });*/





    }






    @Override
    public int getItemCount() {

        return chatAddmemberInputPojoArrayList.size();
    }





    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtUsername, txtUserlastname, txtFullnameUser, txtFollowing;
        RelativeLayout rlFollowing, rlayoutFollowing, rlFollowerLay, rlFollowerAyout;

        ImageView imgFollowing, imgCheckedFollower;
        CheckBox checkFollower;

        Add_member_group addMemberGroup;



        public MyViewHolder(View itemView, Add_member_group add_member_group) {
            super(itemView);
            this.addMemberGroup=add_member_group;
            txtUsername = (TextView) itemView.findViewById(R.id.txt_username);
            txtUserlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txtFullnameUser = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            txtFollowing = (TextView) itemView.findViewById(R.id.txt_following);
            imgFollowing = (ImageView) itemView.findViewById(R.id.img_following);
            checkFollower = (CheckBox) itemView.findViewById(R.id.check_follower);
            checkFollower.setOnClickListener(this);

            imgCheckedFollower = (ImageView) itemView.findViewById(R.id.img_checked_follower);

            rlFollowerLay = (RelativeLayout) itemView.findViewById(R.id.rl_follower_lay);
            rlayoutFollowing = (RelativeLayout) itemView.findViewById(R.id.rlayout_following);
            rlFollowerAyout = (RelativeLayout) itemView.findViewById(R.id.rl_follower_ayout);


        }


        @Override
        public void onClick(View v) {
            addMemberGroup.getgrpmemberList(v,getAdapterPosition());
        }
    }




}

