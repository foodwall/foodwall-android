package com.kaspontech.foodwall.adapters.chat;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_history_single_details;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class Chat_adapter_single extends RecyclerView.Adapter<Chat_adapter_single.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     **/
    private Context context;

    /**
     * Chat history details arraylist
     **/
    private ArrayList<Chat_history_single_details> getChatHistoryDetailsPojoArrayList = new ArrayList<>();

    /**
     * Chat history details pojo
     **/
    Chat_history_single_details chatHistorySingleDetails;

    /**
     * Integer variable - chat id
     **/
    int chat_id;


    public Chat_adapter_single(Context c, ArrayList<Chat_history_single_details> gettinglist /*,ArrayList<Chat_details_pojo> chatDetailsPojoArrayList*/) {
        this.context = c;
        this.getChatHistoryDetailsPojoArrayList = gettinglist;
        /*this.chatDetailsPojoArrayList = chatDetailsPojoArrayList;*/
    }


    @NonNull
    @Override
    public Chat_adapter_single.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_chat_single, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Chat_adapter_single.MyViewHolder vh = new Chat_adapter_single.MyViewHolder(v);


        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Chat_adapter_single.MyViewHolder holder, final int position) {

        chatHistorySingleDetails = getChatHistoryDetailsPojoArrayList.get(position);

        //user message

        holder.txt_chat_message.setText(getChatHistoryDetailsPojoArrayList.get(position).getMessage());
        holder.txt_chat_message.setTextColor(Color.parseColor("#000000"));

        //User picture

        Utility.picassoImageLoader(getChatHistoryDetailsPojoArrayList.get(position).getFromPicture(),
                0,holder.img_user_single, context);

//        if (getChatHistoryDetailsPojoArrayList.get(position).getToPicture() == null) {
//            holder.imgUserSingle.setImageResource(R.drawable.ic_add_photo);
//        } else {
//            GlideApp.with(context).load(getChatHistoryDetailsPojoArrayList.get(position).getFromPicture()).
//                    centerInside().into(holder.imgUserSingle);
//
//        }


      //Ago

        String ago = getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn();

        StringTokenizer token = new StringTokenizer(ago);
        String date1 = token.nextToken();
        String time1 = token.nextToken();
        try {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = dateFormatter.parse(ago);



       // Get time from date
            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
            String displayValue = timeFormatter.format(date);
            holder.txt_message_time.setText(displayValue);

        } catch (ParseException e) {
            e.printStackTrace();
        }





      /*  try {
            long now = System.currentTimeMillis();

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convertedDate = dateFormat.parse(ago);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now,

                    DateUtils.FORMAT_ABBREV_RELATIVE);

            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                holder.txtMessageTime.setText(R.string.just_now);
            } else if (relavetime1.toString().contains("hours ago")) {
                holder.txtMessageTime.setText(relavetime1.toString().replace("hours ago", "").concat("h"));
            } else if (relavetime1.toString().contains("days ago")) {
                holder.txtMessageTime.setText(relavetime1.toString().replace("days ago", "").concat("dialog"));
            } else {
                holder.txtMessageTime.append(relavetime1 + "\n\n");

            }


        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        /*Chat click on click listener*/
        holder.id_rl_chat_single.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {


                chat_id= Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getChatid());

                /*Alert dialog*/
                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this message?")
                        .setContentText("Won't be able to recover this message anymore!")
                        .setConfirmText("Yes,delete it!")
                        .setCancelText("No,cancel!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                if(Utility.isConnected(context)){
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                try {

                                    String createdby = Pref_storage.getDetail(context,"userId");
                                    Call<CreateUserPojoOutput> call = apiService.update_delete_single_chat( "update_delete_single_chat",chat_id,Integer.parseInt(createdby));
                                    call.enqueue(new Callback<CreateUserPojoOutput>() {
                                        @Override
                                        public void onResponse(Call<CreateUserPojoOutput>call, Response<CreateUserPojoOutput> response) {

                                            if (response.body() != null && response.body().getResponseCode() == 1) {

                                                /*Postion removed*/

                                                removeAt(position);

                                                Toast.makeText(context, "Deleted!!", Toast.LENGTH_LONG).show();

//                                                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//                                                        .setTitleText("Deleted!!")
//                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                                            @Override
//                                                            public void onClick(SweetAlertDialog sDialog) {
//                                                                sDialog.dismissWithAnimation();
//
//                                                            }
//                                                        })
//                                                        .show();


                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError",""+t.getMessage());
                                        }
                                    });
                                }catch (Exception e ){
                                    e.printStackTrace();
                                }
                                }else{

                                    Toast.makeText(context, "No internet connection!!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        })
                        .show();



                return false;
            }
        });



    }

    /*Postion removed*/

    public void removeAt(int position) {
        getChatHistoryDetailsPojoArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getChatHistoryDetailsPojoArrayList.size());
    }


    @Override
    public int getItemCount() {

        return getChatHistoryDetailsPojoArrayList.size();

    }

        @Override
        public boolean onInterceptTouchEvent (RecyclerView rv, MotionEvent e){
            return false;
        }

        @Override
        public void onTouchEvent (RecyclerView rv, MotionEvent e){

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent ( boolean disallowIntercept){

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView  txt_message_time, txt_chat_message;
            RelativeLayout id_rl_chat_single;

            ImageView img_user_single;


            public MyViewHolder(View itemView) {
                super(itemView);


                txt_message_time = (TextView) itemView.findViewById(R.id.txt_message_time);
                txt_chat_message = (TextView) itemView.findViewById(R.id.txt_chat_message);
                img_user_single = (ImageView) itemView.findViewById(R.id.img_user_single);
                id_rl_chat_single = (RelativeLayout) itemView.findViewById(R.id.id_rl_chat_single);


            }


        }


    }
