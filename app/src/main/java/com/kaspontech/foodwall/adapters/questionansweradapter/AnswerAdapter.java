package com.kaspontech.foodwall.adapters.questionansweradapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.questionspackage.QuestionAnswer;
import com.kaspontech.foodwall.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class AnswerAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {

    /*Widgets initialization*/

    public CircleImageView userimage;

    public LinearLayout viewQues;
    public  LinearLayout ll_answer;
    public LinearLayout ll_answer_follow;
    public  LinearLayout ll_question_headline;
    public  LinearLayout ll_answer_sharechat;

    public ImageButton answerChecked;
    public ImageButton answerUnchecked;
    public ImageButton typeAnswer;
    public ImageButton ans_options;
    public ImageButton img_btn_follow;
    public TextView question;
    public TextView answer;
    public TextView username;
    public TextView no_of_followers;
    public TextView answeredOn;
    public TextView postAnswer;
    public TextView total_answer;
    public TextView tv_follow_count;
    public TextView tv_question_headlinetext;
    public TextView tv_headline_username;
    public ImageView img_sharechat;

    QuestionAnswer questionAnswer;

    com.kaspontech.foodwall.onCommentsPostListener onCommentsPostListener;

    View itemview1;

    public AnswerAdapter(View itemView, QuestionAnswer questionAnswer1) {
        super(itemView);

        this.itemview1=itemView;
        this.questionAnswer = questionAnswer1;

        userimage = (CircleImageView) itemView.findViewById(R.id.user_image);
        question = (TextView) itemView.findViewById(R.id.question);
        answer = (TextView) itemView.findViewById(R.id.answer);
        username = (TextView) itemView.findViewById(R.id.user_name);
        no_of_followers = (TextView) itemView.findViewById(R.id.follow_count);
        tv_headline_username = (TextView) itemView.findViewById(R.id.tv_headline_username);
        tv_question_headlinetext = (TextView) itemView.findViewById(R.id.tv_question_headlinetext);
        tv_follow_count = (TextView) itemView.findViewById(R.id.tv_follow_count);

        viewQues = (LinearLayout) itemView.findViewById(R.id.ll_view_ques);
        ll_answer = (LinearLayout) itemView.findViewById(R.id.ll_answer);
        ll_answer_follow = (LinearLayout) itemView.findViewById(R.id.ll_answer_follow);
        ll_question_headline = (LinearLayout) itemView.findViewById(R.id.ll_question_headline);
        ll_answer_sharechat = (LinearLayout) itemView.findViewById(R.id.ll_answer_sharechat);


        postAnswer = (TextView) itemView.findViewById(R.id.txt_adapter_post);
        answeredOn = (TextView) itemView.findViewById(R.id.answered_on);
        answerChecked = (ImageButton) itemView.findViewById(R.id.answer_checked);
        answerUnchecked = (ImageButton) itemView.findViewById(R.id.answer_unchecked);
        ans_options = (ImageButton) itemView.findViewById(R.id.ans_options);
        img_btn_follow = (ImageButton) itemView.findViewById(R.id.img_btn_follow);
        img_sharechat = (ImageView) itemView.findViewById(R.id.img_sharechat);
    }


    public AnswerAdapter(View itemView, QuestionAnswer questionAnswer1,onCommentsPostListener onCommentsPostListener1) {
        super(itemView);


        this.onCommentsPostListener= onCommentsPostListener1;
        this.itemview1=itemView;
        this.questionAnswer = questionAnswer1;

        userimage = (CircleImageView) itemView.findViewById(R.id.user_image);
        question = (TextView) itemView.findViewById(R.id.question);
        answer = (TextView) itemView.findViewById(R.id.answer);
        username = (TextView) itemView.findViewById(R.id.user_name);
        no_of_followers = (TextView) itemView.findViewById(R.id.follow_count);
        tv_headline_username = (TextView) itemView.findViewById(R.id.tv_headline_username);
        tv_question_headlinetext = (TextView) itemView.findViewById(R.id.tv_question_headlinetext);
        tv_follow_count = (TextView) itemView.findViewById(R.id.tv_follow_count);

        viewQues = (LinearLayout) itemView.findViewById(R.id.ll_view_ques);
        ll_answer = (LinearLayout) itemView.findViewById(R.id.ll_answer);
        ll_answer_follow = (LinearLayout) itemView.findViewById(R.id.ll_answer_follow);
        ll_question_headline = (LinearLayout) itemView.findViewById(R.id.ll_question_headline);
        ll_answer_sharechat = (LinearLayout) itemView.findViewById(R.id.ll_answer_sharechat);


        postAnswer = (TextView) itemView.findViewById(R.id.txt_adapter_post);
        answeredOn = (TextView) itemView.findViewById(R.id.answered_on);
        answerChecked = (ImageButton) itemView.findViewById(R.id.answer_checked);
        answerUnchecked = (ImageButton) itemView.findViewById(R.id.answer_unchecked);
        ans_options = (ImageButton) itemView.findViewById(R.id.ans_options);
        img_btn_follow = (ImageButton) itemView.findViewById(R.id.img_btn_follow);
        img_sharechat = (ImageView) itemView.findViewById(R.id.img_sharechat);

        ll_answer.setOnClickListener(this);
        img_sharechat.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.ll_answer:

                onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),itemview1);
                break;

            case R.id.img_sharechat:

                onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),itemview1);
                break;
        }


    }
}
