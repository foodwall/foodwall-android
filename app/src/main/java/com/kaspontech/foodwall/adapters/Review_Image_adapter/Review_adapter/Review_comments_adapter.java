package com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Review_comments_all_output;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Review_comments_all_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_reply_all_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
import com.kaspontech.foodwall.reviewpackage.Reviewscommentslikeall;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

//

/**
 * Created by VISHNU on 17-06-2018.
 */

public class Review_comments_adapter extends
        RecyclerView.Adapter<Review_comments_adapter.MyViewHolder> implements
        RecyclerView.OnItemTouchListener {


    Context context;
    ReviewCommentReplyAdapter commentsReplyAdapter;

    //API call status
    private ArrayList<Review_comments_all_pojo> gettingcommentslist = new ArrayList<>();
    private ArrayList<Get_reply_all_pojo> gettingreplyalllist = new ArrayList<>();
    private Review_comments_all_pojo getallCommentsPojo;
    private Get_reply_all_pojo getReplyAllPojo;


    //like flag
    private Pref_storage pref_storage;
    Reviews_comments_all_activity reviews_comments_all_activity;

    public Review_comments_adapter(Context c, ArrayList<Review_comments_all_pojo> gettinglist) {
        this.context = c;
        this.gettingcommentslist = gettinglist;
        reviews_comments_all_activity = (Reviews_comments_all_activity) context;


    }

    @NonNull
    @Override
    public Review_comments_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_comments, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v, reviews_comments_all_activity);

        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Review_comments_adapter.MyViewHolder holder, final int position) {

        getallCommentsPojo = gettingcommentslist.get(position);

        String firstname = gettingcommentslist.get(position).getFirstName();
        String lastname = gettingcommentslist.get(position).getLastName();

        String username = firstname + " " + lastname;
        holder.txt_username.setText(username);

        holder.txt_username.setTextColor(Color.parseColor("#000000"));

        String txt_replies_like = gettingcommentslist.get(position).getCmmtTotLikes();


        String liked = gettingcommentslist.get(position).getRevCmmtLikes();


        //Likes display user

        if (Integer.parseInt(liked) == 1) {

            holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
            gettingcommentslist.get(position).setLiked(true);

        } else if (Integer.parseInt(liked) == 0 || liked == null) {

            gettingcommentslist.get(position).setLiked(false);
            holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));

        }

        //Likes display count display

        if (Integer.parseInt(txt_replies_like) == 0 || txt_replies_like == null) {
            holder.txt_like.setVisibility(GONE);
        } else if (Integer.parseInt(txt_replies_like) == 1) {

            holder.txt_like.setText(txt_replies_like.concat(" ").concat("Like"));
        } else if (Integer.parseInt(txt_replies_like) > 1) {
            holder.txt_like.setText(txt_replies_like.concat(" ").concat("likes"));
        }







        String txt_replies_count = gettingcommentslist.get(position).getCmmtTotReply();
//        Log.e("txt_replies_count", "onBindViewHolder: "+ txt_replies_count);

     /*   Pref_storage.setDetail(context,"Review_reply_size",txt_replies_count);*/

        if (Integer.parseInt(txt_replies_count) == 0 || txt_replies_count == null) {

            holder.txt_replies_count.setVisibility(GONE);
            holder.txt_view_all_replies.setVisibility(GONE);
            holder.view_reply.setVisibility(GONE);

        } else {
            holder.txt_replies_count.setText("(".concat(txt_replies_count).concat(")"));
        }

       /* String review_reply_size= Pref_storage.getDetail(context,"Review_reply_size");


        Log.e("txt_replies_count", "review_reply_size: "+ review_reply_size);

        if(review_reply_size!=null){
            if(Integer.parseInt(review_reply_size)==0){
                holder.txt_replies_count.setVisibility(GONE);
                holder.txt_view_all_replies.setVisibility(GONE);
                holder.view_reply.setVisibility(GONE);

            }else {
                holder.txt_replies_count.setText("(".concat(txt_replies_count).concat(")"));

            }
        }*/


        // Edit visibility
        if (gettingcommentslist.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
            holder.txt_edit.setVisibility(View.VISIBLE);
        } else {
            holder.txt_edit.setVisibility(View.GONE);
            holder.delete_comment.setVisibility(View.GONE);
        }

        // Display comments
        holder.txt_comments.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettingcommentslist.get(position).getHtlRevComments()));
//        Log.e("comments", "-->" + gettingcommentslist.get(position).getHtlRevComments());
        holder.txt_comments.setTextColor(Color.parseColor("#000000"));


        // Loading image url


//        Utility.picassoImageLoader(gettingcommentslist.get(position).getPicture(),
//                0,holder.img_comment, context);

        GlideApp.with(context)
                .load(gettingcommentslist.get(position).getPicture())
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)
                .into(holder.img_comment);


        // Posted date functionality
        String CreatedOn = gettingcommentslist.get(position).getCreatedOn();

        Utility.setTimeStamp(CreatedOn, holder.txt_ago);


       /* try {
            //Input time
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long time = sdf.parse(CreatedOn).getTime();

            //Cuurent system time

            Date currentTime = Calendar.getInstance().getTime();
            SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String now_new= sdformat.format(currentTime);
            long nowdate = sdformat.parse(now_new).getTime();

//            String timeAgo = com.kaspontech.foodwall.Utills.TimeAgo.getTimeAgo_new(time,nowdate);

            CharSequence ago = DateUtils.getRelativeTimeSpanString(time, nowdate, DateUtils.FORMAT_ABBREV_RELATIVE);

            if (ago.toString().equalsIgnoreCase("0 minutes ago")||ago.toString().equalsIgnoreCase("In 0 minutes")) {
                holder.txt_ago.setText(R.string.just_now);
            } else {
                holder.txt_ago.append(ago + "");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }*/



        // Users whom liked display
        holder.txt_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* Pref_storage.setDetail(context, "Comment_id", gettingcommentslist.get(position).getCmmtTlId());
                context.startActivity(new Intent(context, Reply_Like_activity.class));*/

            }
        });









//Like redirect
        holder.txt_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Reviewscommentslikeall.class);
                intent.putExtra("Comment_id", gettingcommentslist.get(position).getCmmtHtlId());
                context.startActivity(intent);
            }
        });


        //l ike for a comment
        holder.img_comment_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);


                if (gettingcommentslist.get(position).isLiked()) {

                    holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                    gettingcommentslist.get(position).setLiked(false);
                    gettingcommentslist.get(position).setRevCmmtLikes(String.valueOf(0));


                    int total_likes = Integer.parseInt(gettingcommentslist.get(position).getCmmtTotLikes());

                    if (total_likes == 1) {

                        total_likes = total_likes - 1;
                        gettingcommentslist.get(position).setCmmtTotLikes(String.valueOf(total_likes));
                        holder.txt_like.setVisibility(View.GONE);


                    } else if (total_likes == 2) {

                        total_likes = total_likes - 1;
                        gettingcommentslist.get(position).setCmmtTotLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Like");
                        holder.txt_like.setVisibility(View.VISIBLE);

                    } else {

                        total_likes = total_likes - 1;
                        gettingcommentslist.get(position).setCmmtTotLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Likes");
                        holder.txt_like.setVisibility(View.VISIBLE);

                    }


                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        String createdby = Pref_storage.getDetail(context, "userId");
                        String clickedlike_reviewid = gettingcommentslist.get(position).getCmmtHtlId();
                        Call<CommonOutputPojo> call = apiService.create_hotel_review_comments_likes("create_hotel_review_comments_likes", Integer.parseInt(clickedlike_reviewid), 0, Integer.parseInt(createdby));
                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                if (response.body().getResponseCode()==1) {




                                }

                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else if(!gettingcommentslist.get(position).isLiked()) {


                    holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                    gettingcommentslist.get(position).setLiked(true);
                    gettingcommentslist.get(position).setRevCmmtLikes(String.valueOf(1));


                    int total_likes = Integer.parseInt(gettingcommentslist.get(position).getCmmtTotLikes());

                    if (total_likes == 0) {

                        total_likes = total_likes + 1;
                        gettingcommentslist.get(position).setCmmtTotLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Like");
                        holder.txt_like.setVisibility(View.VISIBLE);

                    } else {

                        total_likes = total_likes + 1;
                        gettingcommentslist.get(position).setCmmtTotLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Likes");

                    }

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        String createdby = Pref_storage.getDetail(context, "userId");
                        String clickedlike_reviewid = gettingcommentslist.get(position).getCmmtHtlId();
                        Call<CommonOutputPojo> call = apiService.create_hotel_review_comments_likes("create_hotel_review_comments_likes", Integer.parseInt(clickedlike_reviewid), 1, Integer.parseInt(createdby));
                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                if (response.body().getData() != null) {




                                }

                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        });


//View replies
        holder.txt_view_all_replies.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                holder.prog_reply.setVisibility(View.VISIBLE);
                gettingreplyalllist.clear();


                String replycommentid = gettingcommentslist.get(position).getCmmtHtlId();
                String reviewid = gettingcommentslist.get(position).getRevratId();
                String userid = gettingcommentslist.get(position).getUserId();

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {

                    Call<Review_comments_all_output> call = apiService.get_hotel_review_comments_all("get_hotel_review_comments_all", Integer.parseInt(replycommentid), Integer.parseInt(reviewid), Integer.parseInt(userid));
                    call.enqueue(new Callback<Review_comments_all_output>() {
                        @Override
                        public void onResponse(Call<Review_comments_all_output> call, Response<Review_comments_all_output> response) {


                            if(response.body() != null){

                                if (response.body().getResponseCode() == 1) {

                                    holder.prog_reply.setVisibility(View.GONE);

                                    if(response.body().getData().get(position).getReply().size() != 0 || response.body().getData().get(position).getReply() != null){

                                        commentsReplyAdapter = new ReviewCommentReplyAdapter(context, response.body().getData().get(position).getReply());
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                        holder.rv_comments_reply.setVisibility(View.VISIBLE);
                                        holder.rv_comments_reply.setAdapter(commentsReplyAdapter);
                                        holder.rv_comments_reply.setLayoutManager(mLayoutManager);
                                        commentsReplyAdapter.notifyDataSetChanged();
                                        holder.rv_comments_reply.setNestedScrollingEnabled(false);
                                        holder.txt_view_all_replies.setVisibility(View.GONE);
                                        holder.txt_replies_count.setVisibility(View.GONE);
                                        holder.hide_replies.setVisibility(View.VISIBLE);
                                    }




                                }

                            }




                        }


                        @Override
                        public void onFailure(Call<Review_comments_all_output> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }




            }


        });


        //hide reply

        holder.hide_replies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.rv_comments_reply.setVisibility(View.GONE);
                holder.hide_replies.setVisibility(View.GONE);
                holder.txt_view_all_replies.setVisibility(View.VISIBLE);
                holder.txt_replies_count.setVisibility(View.VISIBLE);

            }
        });

        //reply for a comment
        holder.txt_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reviews_comments_all_activity.clickedcommentposition(v, Integer.parseInt(gettingcommentslist.get(position).getCmmtHtlId()));
                reviews_comments_all_activity.clicked_username(v, gettingcommentslist.get(position).getFirstName(), Integer.parseInt(gettingcommentslist.get(position).getCmmtHtlId()));
                reviews_comments_all_activity.clickedforreply(v, 11);


            }
        });


        holder.txt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                reviews_comments_all_activity.clickedcommentposition_timelineid(v, gettingcommentslist.get(position).getHtlRevComments(), Integer.parseInt(gettingcommentslist.get(position).getCmmtHtlId()), Integer.parseInt(gettingcommentslist.get(position).getHotelId()),Integer.parseInt(gettingcommentslist.get(position).getRevratId()));
                reviews_comments_all_activity.clickedfor_edit(v, 22);
            }
        });


        holder.delete_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this comment?")
                        .setContentText("Won't be able to recover this comment anymore!")
                        .setConfirmText("Delete")
                        .setCancelText("Cancel")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                try {
                                    String createuserid = gettingcommentslist.get(position).getUserId();
                                    String review_commentid = gettingcommentslist.get(position).getRevratId();
                                    String review_hotelid = gettingcommentslist.get(position).getHotelId();
                                    String commentid = gettingcommentslist.get(position).getCmmtHtlId();

                                    Call<CommonOutputPojo> call = apiService.create_delete_hotel_review_comments("create_delete_hotel_review_comments", Integer.parseInt(commentid), Integer.parseInt(review_hotelid), Integer.parseInt(review_commentid), Integer.parseInt(createuserid));
                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                            if (response.body().getResponseCode() == 1) {
                                                removeAt(position);


                                                Toast.makeText(context, "Deleted!!", Toast.LENGTH_SHORT).show();

                                       /*         new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Deleted!!")
//                                                        .setContentText("Your comment has been deleted.")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();


                                                            }
                                                        })
                                                        .show();*/


                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "" + t.getMessage());
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }
                        })
                        .show();


            }
        });





    }

    /*public  void checksize(int size){

        Log.e("sizwww", "checksize: "+size );
        if(size==0){

        }else {

        }
    }*/


    private void removeAt(int position) {
        try {
            gettingcommentslist.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, gettingcommentslist.size());
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        reviews_comments_all_activity.check_review_size(gettingcommentslist.size());
        return gettingcommentslist.size();

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view_reply;
        ImageView img_comment;
        ProgressBar prog_reply;
        LinearLayout rl_comment_layout;
        RecyclerView rv_comments_reply;
        ImageButton img_comment_like, delete_comment;

        TextView txt_username, txt_edit,
                txt_like, txt_ago, txt_reply, txt_comments, txt_view_all_replies, hide_replies, txt_replies_count;


        Reviews_comments_all_activity reviews_comments_all_activity;


        public MyViewHolder(View itemView,Reviews_comments_all_activity reviews_comments_all_activity) {
            super(itemView);

            this.reviews_comments_all_activity = reviews_comments_all_activity;


            view_reply = (View) itemView.findViewById(R.id.view_reply);

            txt_ago = (TextView) itemView.findViewById(R.id.txt_ago);
            txt_like = (TextView) itemView.findViewById(R.id.txt_like);
            txt_edit = (TextView) itemView.findViewById(R.id.txt_edit);
            txt_reply = (TextView) itemView.findViewById(R.id.txt_reply);
            hide_replies = (TextView) itemView.findViewById(R.id.hide_replies);
            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_comments = (TextView) itemView.findViewById(R.id.txt_comments);
            txt_replies_count = (TextView) itemView.findViewById(R.id.txt_replies_count);
            rl_comment_layout = (LinearLayout) itemView.findViewById(R.id.rl_comment_layout);
            txt_view_all_replies = (TextView) itemView.findViewById(R.id.txt_view_all_replies);


            delete_comment = (ImageButton) itemView.findViewById(R.id.del_comment);
            img_comment_like = (ImageButton) itemView.findViewById(R.id.img_comment_like);


            img_comment = (ImageView) itemView.findViewById(R.id.img_comment);

            prog_reply = (ProgressBar) itemView.findViewById(R.id.prog_reply);

            rv_comments_reply = (RecyclerView) itemView.findViewById(R.id.rv_comments_reply);


            txt_reply.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

//            comments_activity.clickedcommentposition(v, getAdapterPosition());

        }


    }




}


