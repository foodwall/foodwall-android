package com.kaspontech.foodwall.adapters.CommonSearch.Questions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.adapters.questionansweradapter.AnswerAdapter;
import com.kaspontech.foodwall.adapters.questionansweradapter.GetQuestionAllAdapter;
import com.kaspontech.foodwall.adapters.questionansweradapter.PollAdapter;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.modelclasses.AnswerPojoClass;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchQuestionPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchQuestionPoll;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Answer.GetAnswerData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.PollData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestionAnswerPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Poll.create_delete_answer_poll_undoPOJO;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.PollComments.GetPollCommentsOutput;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.questionspackage.EditQuestion;
import com.kaspontech.foodwall.questionspackage.QuestionAnswer;
import com.kaspontech.foodwall.questionspackage.ViewQuestionAnswer;
import com.kaspontech.foodwall.questionspackage.pollcomments.ViewPollComments;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class GetSearchResultsQuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;


    private static final String TAG = "GetSearchResultsQuestio";

    List<SearchQuestionPojo> getQuestionAllDataList = new ArrayList<>();
    SearchQuestionPojo getQuestionAllData;

    private boolean undoPoll = false;

    QuestionAnswer questionAnswer;

    GetQuestionAllAdapter getQuestionAllAdapter;

    List<QuestionAnswerPojo> pollDisplayQuestionList = new ArrayList<>();


    List<SearchQuestionPoll> getPollDataList = new ArrayList<>();
    PollData pollData;

    List<GetAnswerData> getAnswerDataList = new ArrayList<>();
    GetAnswerData getAnswerData;

    int pollListSize;
    private GPSTracker gpsTracker;
    private double latitude, longitude;


    public GetSearchResultsQuestionAdapter(Context context, List<SearchQuestionPojo> getQuestionAllDataList) {
        this.context = context;
        this.getQuestionAllDataList = getQuestionAllDataList;
    }


    @Override
    public int getItemViewType(int position) {

        // Case 0 - Normal Question
        // Case 1 - Poll Question

        if (getQuestionAllDataList.get(position).getQuesType() == 1) {

            return 2;

        } else {

            return 1;

        }

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        switch (viewType) {

            case 1:

                View answerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_que_ans_all, parent, false);
                AnswerAdapter answerAdapter = new AnswerAdapter(answerView,questionAnswer);
                return answerAdapter;

            case 2:

                View pollView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_question_as_poll, parent, false);
                PollAdapter pollAdapter = new PollAdapter(pollView,questionAnswer);
                return pollAdapter;


        }

        return null;


    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        getQuestionAllData = getQuestionAllDataList.get(position);

        switch (holder.getItemViewType()) {


            case 1:

                // Question Parts
                final AnswerAdapter answerAdapter = (AnswerAdapter) holder;

                answerAdapter.question.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getQuestionAllData.getAskQuestion()));
                String username = getQuestionAllData.getFirstName() + " " + getQuestionAllData.getLastName();
                answerAdapter.username.setText(username);

//                Utility.picassoImageLoader(getQuestionAllData.getPicture(),0,answerAdapter.userimage);


                GlideApp.with(context)
                        .load(getQuestionAllData.getPicture())
                        .placeholder(R.drawable.ic_add_photo)
                        .into(answerAdapter.userimage);

                answerAdapter.username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = getQuestionAllDataList.get(position).getUserId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }
                });

                int followCount = getQuestionAllDataList.get(position).getTotalQuestFollow();


                if (getQuestionAllDataList.get(position).getQuesFollow() != 0) {

                    answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_followed_answer));
                    String followText = "Follow - " + String.valueOf(followCount);
                    answerAdapter.tv_follow_count.setText(followText);
                    answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.blue));
                    getQuestionAllDataList.get(position).setFollowing(true);


                } else {


                    if (followCount != 0) {

                        String followText = "Follow - " + String.valueOf(followCount);
                        answerAdapter.tv_follow_count.setText(followText);

                    } else {

                        answerAdapter.tv_follow_count.setText("Follow");
                    }

                    answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_follow_answer));
                    answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.black));
                    getQuestionAllDataList.get(position).setFollowing(false);

                }


                answerAdapter.ll_answer_follow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (!getQuestionAllDataList.get(position).isFollowing()) {


                            answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_followed_answer));
                            answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.blue));
                            getQuestionAllDataList.get(position).setFollowing(true);
                            int count = getQuestionAllDataList.get(position).getTotalQuestFollow();
                            String followText = "Follow - " + String.valueOf(count + 1);
                            answerAdapter.tv_follow_count.setText(followText);
                            getQuestionAllDataList.get(position).setTotalQuestFollow(count + 1);

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                Call<CommonOutputPojo> call = apiService.createQuesFollow("create_delete_question_follower", getQuestionAllDataList.get(position).getQuestId(), getQuestionAllDataList.get(position).getCreatedBy(), 1, userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @SuppressLint("ResourceAsColor")
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                        if (response.body() != null) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("success")) {

                                                //Success


                                            } else {


                                            }

                                        } else {

                                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();
                                        }


                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                    }
                                });

                            } catch (Exception e) {

                                e.printStackTrace();

                            }


                        } else if (getQuestionAllDataList.get(position).isFollowing()) {

                            answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_follow_answer));
                            answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.black));
                            getQuestionAllDataList.get(position).setFollowing(false);
                            int count = getQuestionAllDataList.get(position).getTotalQuestFollow();
                            getQuestionAllDataList.get(position).setTotalQuestFollow(count - 1);


                            if (getQuestionAllDataList.get(position).getTotalQuestFollow() != 0) {

                                String followText = "Follow - " + String.valueOf(count - 1);
                                answerAdapter.tv_follow_count.setText(followText);


                            } else {

                                String followText = "Follow";
                                answerAdapter.tv_follow_count.setText(followText);
                            }

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                Call<CommonOutputPojo> call = apiService.createQuesFollow("create_delete_question_follower", getQuestionAllDataList.get(position).getQuestId(), getQuestionAllDataList.get(position).getCreatedBy(), 0, userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @SuppressLint("ResourceAsColor")
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                        if (response.body() != null) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("success")) {

                                                //Success


                                            } else {


                                            }

                                        } else {

                                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                        }


                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                    }
                                });

                            } catch (Exception e) {

                                e.printStackTrace();

                            }


                        }
                    }
                });

                answerAdapter.userimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        String user_id = getQuestionAllDataList.get(position).getUserId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }
                });

                int total_followers = getQuestionAllData.getTotalFollowers();

                String followers = "";

                switch (total_followers) {

                    case 0:

                        answerAdapter.no_of_followers.setVisibility(View.GONE);

                        break;

                    case 1:

                        followers = String.valueOf(total_followers) + " Follower";

                        break;

                    default:

                        followers = String.valueOf(total_followers) + " Followers";

                        break;

                }

                answerAdapter.no_of_followers.setText(followers);

                String createdon = getQuestionAllData.getCreatedOn();

                Utility.setTimeStamp(createdon,  answerAdapter.answeredOn);

               /* try {
                    //Input time
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    long time = sdf.parse(createdon).getTime();

                    //Cuurent system time

                    Date currentTime = Calendar.getInstance().getTime();
                    SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String now_new= sdformat.format(currentTime);
                    long nowdate = sdformat.parse(now_new).getTime();

//            String timeAgo = com.kaspontech.foodwall.Utills.TimeAgo.getTimeAgo_new(time,nowdate);

                    CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(time, nowdate, DateUtils.FORMAT_ABBREV_RELATIVE);

                    if (relavetime1.toString().equalsIgnoreCase("0 minutes ago") || relavetime1.toString().equalsIgnoreCase("In 0 minutes")) {

                        answerAdapter.answeredOn.setText("Asked ");
                        answerAdapter.answeredOn.append(String.valueOf(R.string.just_now));

                    } else {

                        answerAdapter.answeredOn.append("Asked " + relavetime1 + "\n");

                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }*/


                answerAdapter.ll_answer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(context);
                        LayoutInflater inflater11 = LayoutInflater.from(context);
                        @SuppressLint("InflateParams") final View dialogView1 = inflater11.inflate(R.layout.alert_answer_layout, null);
                        dialogBuilder1.setView(dialogView1);
                        dialogBuilder1.setTitle("");

                        final AlertDialog dialog1 = dialogBuilder1.create();
                        dialog1.show();

                        final EditText edittxt_comment;
                        CircleImageView userphoto_comment;
                        final TextView user_answer, cmt_user_name, txt_adapter_post;

                        edittxt_comment = (EditText) dialogView1.findViewById(R.id.edittxt_comment);
                        userphoto_comment = (CircleImageView) dialogView1.findViewById(R.id.userphoto_comment);
                        user_answer = (TextView) dialogView1.findViewById(R.id.user_answer);
                        txt_adapter_post = (TextView) dialogView1.findViewById(R.id.txt_adapter_post);

                        Utility.picassoImageLoader(Pref_storage.getDetail(context, "picture_user_login"),0,userphoto_comment,context );




                        /*GlideApp.with(context)
                                .load(Pref_storage.getDetail(context, "picture_user_login"))
                                .placeholder(R.drawable.ic_add_photo)
                                .into(userphoto_comment);*/

                        user_answer.setText(getQuestionAllDataList.get(position).getAskQuestion());

                        edittxt_comment.setHint("Add your answer");
                        edittxt_comment.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {


                            }

                            @Override
                            public void afterTextChanged(Editable s) {


                                if (edittxt_comment.getText().toString().length() != 0) {
                                    txt_adapter_post.setVisibility(View.VISIBLE);
                                } else {
                                    txt_adapter_post.setVisibility(View.GONE);
                                }


                            }
                        });

                        txt_adapter_post.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                if (edittxt_comment.getText().toString().length() != 0) {

                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                    try {

                                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                        Call<AnswerPojoClass> call = apiService.createAnswer("create_answer", 0, getQuestionAllDataList.get(position).getQuestId(),
                                                0, 0, edittxt_comment.getText().toString(), latitude, longitude, userid);

                                        call.enqueue(new Callback<AnswerPojoClass>() {
                                            @Override
                                            public void onResponse(Call<AnswerPojoClass> call, Response<AnswerPojoClass> response) {


                                                if (response.body() != null) {


                                                    String responseStatus = response.body().getResponseMessage();

                                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                    if (responseStatus.equals("success")) {

                                                        Toast.makeText(context, "Answer created successfully", Toast.LENGTH_SHORT).show();
                                                        //Success

                                                        dialog1.dismiss();

                                                        Intent intent = new Intent(context, ViewQuestionAnswer.class);
                                                        Bundle bundle = new Bundle();
                                                        bundle.putInt("position", position);
                                                        bundle.putInt("quesId", getQuestionAllDataList.get(position).getQuestId());
                                                        intent.putExtras(bundle);
                                                        context.startActivity(intent);


                                                    }

                                                    dialog1.dismiss();


                                                } else {

                                                    dialog1.dismiss();


                                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                                }


                                            }

                                            @Override
                                            public void onFailure(Call<AnswerPojoClass> call, Throwable t) {
                                                //Error
                                                Log.e("FailureError", "FailureError" + t.getMessage());
                                                dialog1.dismiss();

                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                } else {

                                    edittxt_comment.setError("Enter your answer");

                                }


                            }
                        });


                    }
                });


                answerAdapter.viewQues.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ViewQuestionAnswer.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("position", position);
                        bundle.putInt("quesId", getQuestionAllDataList.get(position).getQuestId());
                        intent.putExtras(bundle);
                        context.startActivity(intent);

                    }
                });


                answerAdapter.ans_options.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int questId = getQuestionAllDataList.get(position).getQuestId();
                        int userid = getQuestionAllDataList.get(position).getCreatedBy();
                        int quesType = getQuestionAllDataList.get(position).getQuesType();
                        String question = getQuestionAllDataList.get(position).getAskQuestion();

                        CustomDialogClass ccd = new CustomDialogClass(context, questId, userid,position);
                        ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        ccd.setCancelable(true);
                        ccd.show();

                    }
                });


                // Answer Parts


                if (getQuestionAllDataList.get(position).getTotalAnswers() == 0) {


                    answerAdapter.answer.setText("No answers yet");
                    answerAdapter.answer.setTypeface(Typeface.DEFAULT_BOLD);


                } else {


                    if (getQuestionAllDataList.get(position).getAnswer().get(0) != null) {

                        answerAdapter.answer.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getQuestionAllDataList.get(position).getAnswer().get(0).getAskAnswer()));
//                        answerAdapter.answer.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));

                    }


                }



                break;

            case 2:


                final PollAdapter pollAdapter = (PollAdapter) holder;

//                getPollDataList = getQuestionAllDataList.get(position).getPoll();



//                Utility.picassoImageLoader(Pref_storage.getDetail(context, "picture_user_login"),0,pollAdapter.userphoto_comment);


                GlideApp.with(context)
                        .load(Pref_storage.getDetail(context, "picture_user_login"))
                        .centerCrop()
                        .placeholder(R.drawable.ic_add_photo)
                        .into(pollAdapter.userphoto_comment);


                pollAdapter.tv_view_all_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ViewPollComments.class);
                        intent.putExtra("comment_quesId", String.valueOf(getQuestionAllDataList.get(position).getQuestId()));
                        context.startActivity(intent);
                    }
                });


                pollAdapter.edittxt_comment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        if (pollAdapter.edittxt_comment.getText().toString().length() != 0) {
                            pollAdapter.txt_adapter_post.setVisibility(VISIBLE);
                        } else {
                            pollAdapter.txt_adapter_post.setVisibility(GONE);
                        }


                    }

                });

                int totalComments = getQuestionAllDataList.get(position).getTotalComments();

                if (totalComments == 0) {

                    pollAdapter.tv_view_all_comments.setVisibility(GONE);

                } else if (totalComments == 1) {

                    pollAdapter.tv_view_all_comments.setVisibility(VISIBLE);
                    String viewTxt = "View " + totalComments + " Comment";
                    pollAdapter.tv_view_all_comments.setText(viewTxt);

                } else if (totalComments > 1) {

                    pollAdapter.tv_view_all_comments.setVisibility(VISIBLE);
                    String viewTxt = "View all " + totalComments + " Comments";
                    pollAdapter.tv_view_all_comments.setText(viewTxt);


                }


                pollAdapter.txt_adapter_post.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (pollAdapter.edittxt_comment.getText().toString().length() != 0) {

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                String createdby = Pref_storage.getDetail(context, "userId");

                                Call<GetPollCommentsOutput> call = apiService.createEditPollComments("create_edit_question_comments", 0, getQuestionAllDataList.get(position).getQuestId(), pollAdapter.edittxt_comment.getText().toString(), Integer.parseInt(createdby));
                                call.enqueue(new Callback<GetPollCommentsOutput>() {
                                    @Override
                                    public void onResponse(Call<GetPollCommentsOutput> call, Response<GetPollCommentsOutput> response) {

                                        String responseStatus = response.body().getResponseMessage();

                                        if (responseStatus.equals("success")) {

                                            pollAdapter.edittxt_comment.setText("");
                                            pollAdapter.txt_adapter_post.setVisibility(GONE);
                                            Intent intent = new Intent(context, ViewPollComments.class);
                                            intent.putExtra("comment_quesId", String.valueOf(getQuestionAllDataList.get(position).getQuestId()));
                                            context.startActivity(intent);

                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<GetPollCommentsOutput> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "" + t.getMessage());
                                    }
                                });
                            } catch (
                                    Exception e)

                            {
                                e.printStackTrace();
                            }


                        } else {

                            pollAdapter.edittxt_comment.setError("Enter your comment");
                        }


                    }
                });


                final int pollSize = getQuestionAllDataList.get(position).getPoll().size();


                if (getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser() == 0) {

                    pollAdapter.total_poll_votes.setText("No votes");

                } else if (getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser() == 1) {

                    pollAdapter.total_poll_votes.setText(String.valueOf(getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser()) + " Vote");

                } else {

                    pollAdapter.total_poll_votes.setText(String.valueOf(getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser()) + " Votes");

                }


                if (getQuestionAllDataList.get(position).getPollId() != 0) {

                    pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                    pollAdapter.ll_answer_section.setVisibility(View.GONE);
                    pollAdapter.tv_poll_undo.setVisibility(View.VISIBLE);


                    if (pollSize != 0) {

                        // Poll option one

                        pollAdapter.poll_answer_one.setText(getQuestionAllDataList.get(position).getPoll().get(0).getPollList());

                        double userVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                        double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll();


                        Log.e("VotesOne", "userVotesOne->" + (double) userVotesOne);
                        Log.e("VotesOne", "totalVotesOne->" + (double) totalVotesOne);
                        Log.e("VotesOne", "totalVotesOne->" + (double) totalVotesOne / userVotesOne);


                        double percentOne;

                        if (userVotesOne == 0 || totalVotesOne == 0) {

                            percentOne = 0;

                        } else {

                            percentOne = (totalVotesOne / userVotesOne) * 100;


                        }


                        if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(0).getPollId()) {

                            pollAdapter.poll_one_checked.setVisibility(View.VISIBLE);

                        } else {

                            pollAdapter.poll_one_checked.setVisibility(View.GONE);

                        }


                        pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                        pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");


                        // Poll option two
                        pollAdapter.poll_answer_two.setText(getQuestionAllDataList.get(position).getPoll().get(1).getPollList());

                        double userVotesTwo = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                        double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll();


                        Log.e("VotesOne", "userVotesTwo->" + (double) userVotesTwo);
                        Log.e("VotesOne", "totalVotesTwo->" + (double) totalVotesTwo);
                        Log.e("VotesOne", "totalVotesTwo->" + (double) totalVotesTwo / userVotesTwo);



                        double percentTwo;


                        if (userVotesTwo == 0 || totalVotesTwo == 0) {

                            percentTwo = 0;

                        } else {

                            percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                        }

                        if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(1).getPollId()) {

                            pollAdapter.poll_two_checked.setVisibility(View.VISIBLE);

                        } else {

                            pollAdapter.poll_two_checked.setVisibility(View.GONE);


                        }

                        pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                        pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                        // Poll option three

                        if (pollSize == 3) {

                            pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                            pollAdapter.poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());

                            double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                            Log.e("VotesOne", "userVotesThree->" + (double) userVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree / userVotesThree);


                            double percentThree;

                            if (userVotesThree == 0 || totalVotesThree == 0) {

                                percentThree = 0;

                            } else {

                                percentThree = (totalVotesThree / userVotesThree) * 100;

                            }


                            if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(2).getPollId()) {

                                pollAdapter.poll_three_checked.setVisibility(View.VISIBLE);

                            } else {

                                pollAdapter.poll_three_checked.setVisibility(View.GONE);


                            }

                            pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                            pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            // Poll option four

                        } else if (pollSize == 4) {


                            // poll option 3

                            pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                            pollAdapter.poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());


                            pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                            pollAdapter.poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());

                            double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();


                            Log.e("VotesOne", "userVotesThree->" + (double) userVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree / userVotesThree);


                            double percentThree;

                            if (userVotesThree == 0 || totalVotesThree == 0) {

                                percentThree = 0;

                            } else {

                                percentThree = (totalVotesThree / userVotesThree) * 100;

                            }

                            if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(2).getPollId()) {

                                pollAdapter.poll_three_checked.setVisibility(View.VISIBLE);

                            } else {

                                pollAdapter.poll_three_checked.setVisibility(View.GONE);


                            }

                            pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                            pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            // poll option 4

                            pollAdapter.ll_poll_progress_four.setVisibility(View.VISIBLE);
                            pollAdapter.poll_answer_four.setText(getQuestionAllDataList.get(position).getPoll().get(3).getPollList());


                            double userVotesFour = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesFour = getQuestionAllDataList.get(position).getPoll().get(3).getTotalPoll();

                            Log.e("VotesOne", "userVotesFour->" + (double) userVotesFour);
                            Log.e("VotesOne", "totalVotesFour->" + (double) totalVotesFour);
                            Log.e("VotesOne", "totalVotesFour->" + (double) totalVotesFour / userVotesFour);



                            double percentFour;

                            if (userVotesFour == 0 || totalVotesFour == 0) {

                                percentFour = 0;

                            } else {

                                percentFour = (totalVotesFour / userVotesFour) * 100;

                            }

                            if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(3).getPollId()) {

                                pollAdapter.poll_four_checked.setVisibility(View.VISIBLE);

                            } else {

                                pollAdapter.poll_four_checked.setVisibility(View.GONE);


                            }

                            pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                            pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");

                        }

                    }


                }


                Log.e("pollSize", "pollSize->" + pollSize);

                if (pollSize != 0) {

                    pollAdapter.tv_poll_answer_one.setText(getQuestionAllDataList.get(position).getPoll().get(0).getPollList());
                    pollAdapter.tv_poll_answer_two.setText(getQuestionAllDataList.get(position).getPoll().get(1).getPollList());

                    pollAdapter.poll_answer_one.setText(getQuestionAllDataList.get(position).getPoll().get(0).getPollList());
                    pollAdapter.poll_answer_two.setText(getQuestionAllDataList.get(position).getPoll().get(1).getPollList());


                    if (pollSize == 3) {


                        pollAdapter.ll_poll_answer_third.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());

                        pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                        pollAdapter.poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());


                    } else if (pollSize == 4) {

                        pollAdapter.ll_poll_answer_third.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());

                        pollAdapter.ll_poll_answer_four.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_answer_four.setText(getQuestionAllDataList.get(position).getPoll().get(3).getPollList());

                        pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                        pollAdapter.poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());

                        pollAdapter.ll_poll_progress_four.setVisibility(View.VISIBLE);
                        pollAdapter.poll_answer_four.setText(getQuestionAllDataList.get(position).getPoll().get(3).getPollList());

                    }

                }


                pollAdapter.ll_poll_answer_one.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = getQuestionAllDataList.get(position).getQuestId();
                        int pollId = getQuestionAllDataList.get(position).getPoll().get(0).getPollId();

                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_one_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo.setVisibility(View.VISIBLE);


                        int totalVotes = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }


                        if (pollSize != 0) {

                            // Option One

                            double userVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll() + 1;

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }

                            Log.e("VotesOne", "userVotesOne->" + (double) userVotesOne);
                            Log.e("VotesOne", "totalVotesOne->" + (double) totalVotesOne);
                            Log.e("VotesOne", "percentOne->" + percentOne);


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two

                            double userVotesTwo = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll();

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                            if (pollSize == 3) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser() ;
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();
                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            } else if (pollSize == 4) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                                // Option Four
                                double userVotesFour = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesFour = getQuestionAllDataList.get(position).getPoll().get(3).getTotalPoll();

                                double percentFour;

                                if (userVotesFour == 0 || totalVotesFour == 0) {

                                    percentFour = 0;

                                } else {

                                    percentFour = (totalVotesFour / userVotesFour) * 100;

                                }

                                pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                                pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                            }

                        }


                        createPoll(quesId, pollId);
//                        getAllQuesData();


                    }
                });

                pollAdapter.ll_poll_answer_two.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = getQuestionAllDataList.get(position).getQuestId();
                        int pollId = getQuestionAllDataList.get(position).getPoll().get(1).getPollId();
                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_two_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo.setVisibility(View.VISIBLE);


                        int totalVotes = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }

                        if (pollSize != 0) {

                            // Option One

                            double userVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll();

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two

                            double userVotesTwo = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll() + 1;

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            Log.e("VotesOne", "userVotesTwo->" + (double) userVotesTwo);
                            Log.e("VotesOne", "totalVotesTwo->" + (double) totalVotesTwo);
                            Log.e("VotesOne", "percentTwo->" + percentTwo);


                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                            if (pollSize == 3) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            } else if (pollSize == 4) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                                // Option Four
                                double userVotesFour = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesFour = getQuestionAllDataList.get(position).getPoll().get(3).getTotalPoll();

                                double percentFour;

                                if (userVotesFour == 0 || totalVotesFour == 0) {

                                    percentFour = 0;

                                } else {

                                    percentFour = (totalVotesFour / userVotesFour) * 100;

                                }

                                pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                                pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                            }

                        }


                        createPoll(quesId, pollId);
//                        getAllQuesData();


                    }
                });

                pollAdapter.ll_poll_answer_third.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = getQuestionAllDataList.get(position).getQuestId();
                        int pollId = getQuestionAllDataList.get(position).getPoll().get(2).getPollId();
                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_three_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo.setVisibility(View.VISIBLE);


                        int totalVotes = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }


                        if (pollSize != 0) {

                            // Option One
                            double userVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll();

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two
                            double userVotesTwo = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll();

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                            if (pollSize == 3) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll() + 1;

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                Log.e("VotesOne", "userVotesThree->" + (double) userVotesThree);
                                Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree);
                                Log.e("VotesOne", "percentThree->" + percentThree);



                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            } else if (pollSize == 4) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll() + 1;

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                                // Option Four
                                double userVotesFour = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesFour = getQuestionAllDataList.get(position).getPoll().get(3).getTotalPoll();
                                double percentFour;

                                if (userVotesFour == 0 || totalVotesFour == 0) {

                                    percentFour = 0;

                                } else {

                                    percentFour = (totalVotesFour / userVotesFour) * 100;

                                }

                                pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                                pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                            }

                        }


                        createPoll(quesId, pollId);
//                        getAllQuesData();


                    }
                });


                pollAdapter.ll_poll_answer_four.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = getQuestionAllDataList.get(position).getQuestId();
                        int pollId = getQuestionAllDataList.get(position).getPoll().get(3).getPollId();
                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_four_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo.setVisibility(View.VISIBLE);


                        Log.e(TAG, "onClick: " + getQuestionAllDataList.get(position).getPoll().size());


                        int totalVotes = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }


                        if (pollSize != 0) {

                            // Option One
                            double userVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll();

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two
                            double userVotesTwo = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll();

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                            if (pollSize == 3) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            } else if (pollSize == 4) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                                // Option Four
                                double userVotesFour = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesFour = getQuestionAllDataList.get(position).getPoll().get(3).getTotalPoll() + 1;

                                double percentFour;

                                if (userVotesFour == 0 || totalVotesFour == 0) {

                                    percentFour = 0;

                                } else {

                                    percentFour = (totalVotesFour / userVotesFour) * 100;

                                }

                                Log.e("VotesOne", "userVotesFour->" + (double) userVotesFour);
                                Log.e("VotesOne", "totalVotesFour->" + (double) totalVotesFour);
                                Log.e("VotesOne", "percentFour->" + percentFour);


                                pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                                pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                            }

                        }


                        createPoll(quesId, pollId);
//                        getAllQuesData();

                    }
                });


                pollAdapter.userName.setText(getQuestionAllData.getFirstName() + " " + getQuestionAllData.getLastName());

                Utility.picassoImageLoader(getQuestionAllData.getPicture(),0,pollAdapter.userimage,context );

               /* GlideApp.with(context)
                        .load(getQuestionAllData.getPicture())
                        .placeholder(R.drawable.ic_add_photo)
                        .into(pollAdapter.userimage);*/

                pollAdapter.userName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = getQuestionAllDataList.get(position).getUserId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }
                });


                pollAdapter.userimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = getQuestionAllDataList.get(position).getUserId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }
                });


                String pollCreatedon = getQuestionAllData.getCreatedOn();


                int total_follower = getQuestionAllData.getTotalFollowers();

                String follower = "";

                switch (total_follower) {

                    case 0:

                        pollAdapter.follow_count.setVisibility(View.GONE);

                        break;

                    case 1:

                        follower = String.valueOf(total_follower) + " Follower";

                        break;

                    default:

                        follower = String.valueOf(total_follower) + " Followers";

                        break;

                }

                pollAdapter.follow_count.setText(follower);

                Utility.setTimeStamp(pollCreatedon,pollAdapter.answeredOn);


           /*     try {
                    //Input time
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    long time = sdf.parse(pollCreatedon).getTime();

                    //Cuurent system time

                    Date currentTime = Calendar.getInstance().getTime();
                    SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String now_new= sdformat.format(currentTime);
                    long nowdate = sdformat.parse(now_new).getTime();

//            String timeAgo = com.kaspontech.foodwall.Utills.TimeAgo.getTimeAgo_new(time,nowdate);

                    CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(time, nowdate, DateUtils.FORMAT_ABBREV_RELATIVE);

                    if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                        pollAdapter.answeredOn.setText("Asked ");
                        pollAdapter.answeredOn.append(String.valueOf(R.string.just_now));

                    } else {
                        pollAdapter.answeredOn.append("Asked " + relavetime1 + "\n");
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }*/




                pollAdapter.question.setText(getQuestionAllData.getAskQuestion());


                pollAdapter.ans_options.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int questId = getQuestionAllDataList.get(position).getQuestId();
                        int createdBy = getQuestionAllDataList.get(position).getCreatedBy();

                        Pref_storage.setDetail(context, "clicked_quesID", String.valueOf(questId));
                        Pref_storage.setDetail(context, "clicked_quesCreatedBy", String.valueOf(createdBy));


                        CustomDialogClass ccd = new CustomDialogClass(context, questId, createdBy,position);
                        ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        ccd.setCancelable(true);
                        ccd.show();


                    }
                });


                pollAdapter.tv_poll_undo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        int totalVotes = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                        totalVotes = totalVotes - 1;

                        if (totalVotes <= 0) {

                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText("No Votes");

                        } else if(totalVotes == 1){

                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes)+" Vote");

                        } else {

                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes)+" Votes");

                        }


                        for(int i=0 ; i < getQuestionAllDataList.get(position).getPoll().size() ; i++ ){

                            if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(i).getPollId()) {

                                if(!undoPoll){

                                    int totalPoll = getQuestionAllDataList.get(position).getPoll().get(i).getTotalPoll();
                                    totalPoll = totalPoll - 1;
                                    getQuestionAllDataList.get(position).getPoll().get(i).setTotalPoll(totalPoll);
                                    undoPoll = true;

                                }

                            }

                        }


                        pollAdapter.ll_progress_section.setVisibility(View.GONE);
                        pollAdapter.ll_answer_section.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo.setVisibility(View.GONE);
                        pollAdapter.poll_one_checked.setVisibility(View.GONE);
                        pollAdapter.poll_two_checked.setVisibility(View.GONE);
                        pollAdapter.poll_three_checked.setVisibility(View.GONE);
                        pollAdapter.poll_four_checked.setVisibility(View.GONE);


                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<create_delete_answer_poll_undoPOJO> call = apiService.undoPollVote("create_delete_answer_poll_undo", getQuestionAllDataList.get(position).getQuestId(), userid);

                            call.enqueue(new Callback<create_delete_answer_poll_undoPOJO>() {

                                @Override
                                public void onResponse(Call<create_delete_answer_poll_undoPOJO> call, Response<create_delete_answer_poll_undoPOJO> response) {

                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("Balaji", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            //Success
                                            /*pollAdapter.ll_progress_section.setVisibility(View.GONE);
                                            pollAdapter.ll_answer_section.setVisibility(View.VISIBLE);
                                            pollAdapter.tv_poll_undo.setVisibility(View.GONE);
                                            pollAdapter.poll_one_checked.setVisibility(View.GONE);
                                            pollAdapter.poll_two_checked.setVisibility(View.GONE);
                                            pollAdapter.poll_three_checked.setVisibility(View.GONE);
                                            pollAdapter.poll_four_checked.setVisibility(View.GONE);

                                            Intent intent = new Intent(context, Home.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putInt("launchType", 3);
                                            intent.putExtras(bundle);
                                            context.startActivity(intent);
                                            ((AppCompatActivity) context).finish();*/
//                                            getAllQuesData();


                                        }

                                    } else {

                                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                    }


                                }

                                @Override
                                public void onFailure(Call<create_delete_answer_poll_undoPOJO> call, Throwable t) {
                                    //Error
                                    Log.e("Balaji", "" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                    }
                });

                break;


        }


    }




    private void createPoll(int quesId, int pollId) {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

            Call<AnswerPojoClass> call = apiService.createAnswer("create_answer", 0, quesId,
                    pollId, 1, "poll", latitude, longitude, userid);


            call.enqueue(new Callback<AnswerPojoClass>() {
                @Override
                public void onResponse(Call<AnswerPojoClass> call, Response<AnswerPojoClass> response) {

                    if (response.body() != null) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

//                            Toast.makeText(context, "Poll created successfully", Toast.LENGTH_SHORT).show();
                            //Success
//                            getAllQuesData();

                        }

                    } else {

                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                    }


                }

                @Override
                public void onFailure(Call<AnswerPojoClass> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return getQuestionAllDataList.size();
    }


    public class CustomDialogClass extends Dialog implements
            View.OnClickListener {

        public Activity activity;
        public Dialog dialog;
        public AppCompatButton btn_Ok;

        int quesId, userId, position;

        TextView txt_deletepost, txt_report, txt_copylink, txt_turnoffpost, txt_sharevia, txt_editpost;

        CustomDialogClass(Context a, int quesId, int userid, int position) {
            super(a);
            // TODO Auto-generated constructor stub
            context = a;
            this.quesId = quesId;
            this.userId = userid;
            this.position = position;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
//            setContentView(R.layout.dialog_version_info);

            setContentView(R.layout.custom_dialog_timeline);

            txt_deletepost = (TextView) findViewById(R.id.txt_deletepost);
            txt_editpost = (TextView) findViewById(R.id.txt_editpost);
            txt_report = (TextView) findViewById(R.id.txt_report);
            txt_copylink = (TextView) findViewById(R.id.txt_copylink);
            txt_turnoffpost = (TextView) findViewById(R.id.txt_turnoffpost);
            txt_sharevia = (TextView) findViewById(R.id.txt_sharevia);

            txt_deletepost.setOnClickListener(this);
            txt_editpost.setOnClickListener(this);
            txt_report.setOnClickListener(this);
            txt_copylink.setOnClickListener(this);
            txt_turnoffpost.setOnClickListener(this);
            txt_sharevia.setOnClickListener(this);


            if (String.valueOf(userId).equals(Pref_storage.getDetail(context, "userId"))) {
                txt_deletepost.setVisibility(VISIBLE);
                txt_editpost.setVisibility(VISIBLE);
            } else {
                txt_deletepost.setVisibility(GONE);
                txt_editpost.setVisibility(GONE);
            }


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_editpost:

                    Intent intent = new Intent(context, EditQuestion.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("quesId", quesId);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                    break;


                case R.id.txt_deletepost:

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to delete this post?")
                            .setContentText("Won't be able to recover this post anymore!")
                            .setConfirmText("Yes,delete it!")
                            .setCancelText("No,cancel!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {

                                    sDialog.dismissWithAnimation();

                                    deleteQuestion(quesId, userId,position);


                                }
                            })
                            .show();
                    dismiss();
                    break;
                case R.id.txt_report:
                    dismiss();
                    break;
                case R.id.txt_copylink:
                    Toast.makeText(context, "Link has been copied to clipboard", Toast.LENGTH_SHORT).show();
                    dismiss();
                    break;
                case R.id.txt_turnoffpost:
                    dismiss();
                    break;
                case R.id.txt_sharevia:
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                    i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                    context.startActivity(Intent.createChooser(i, "Share via"));
                    dismiss();
                    break;

                default:
                    break;
            }
        }

    }


    private void deleteQuestion(int quesid, int createdBy, final int position) {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {


            Call<CommonOutputPojo> call = apiService.deleteQuestion("create_delete_question", quesid, createdBy);

            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                    if (response.body() != null) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("deleteEventComment", "responseStatus->" + responseStatus);


                        if (responseStatus.equals("success")) {

                            //Success
                            removeAt(position);

                        } else {



                        }


                    } else {

                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                    }


                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }


    }

    public void removeAt(int position) {
        getQuestionAllDataList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getQuestionAllDataList.size());
    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


}
