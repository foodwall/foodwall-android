package com.kaspontech.foodwall.adapters.events.eventdiscussion;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.eventspackage.ViewDiscussionComments;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Reply_Like_activity;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetEventsDisCmtsAllData;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetEventsDisCmtsAllOutput;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetEventsDisCmtsAllReplyData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

//

/**
 * Created by vishnukm on 15/3/18.
 */

public class GetEventsDisCommentsAdapter extends RecyclerView.Adapter<GetEventsDisCommentsAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     **/
    Context context;
    boolean like_flag;
    /**
     * Event adapter
     **/
    GetEventsDisCmtReplyAdapter getEventsDisCmtReplyAdapter;
    /**
     * Comments all data pojo's & array lists
     **/

    private GetEventsDisCmtsAllData getEventsDisCmtsAllData;
    ViewDiscussionComments viewDiscussionComments;
    private GetEventsDisCmtsAllReplyData getEventsDisCmtsAllReplyData;

    private List<GetEventsDisCmtsAllData> getEventsDisCmtsAllDataArrayList = new ArrayList<>();
    private List<GetEventsDisCmtsAllReplyData> getEventsDisCmtsAllReplyDataArrayList = new ArrayList<>();





    public GetEventsDisCommentsAdapter(Context c, List<GetEventsDisCmtsAllData> getEventsDisCmtsAllDataArrayList) {
        this.context = c;
        this.getEventsDisCmtsAllDataArrayList = getEventsDisCmtsAllDataArrayList;
        viewDiscussionComments = (ViewDiscussionComments) context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_comments, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v, viewDiscussionComments);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        getEventsDisCmtsAllData = getEventsDisCmtsAllDataArrayList.get(position);
        String firstname = getEventsDisCmtsAllDataArrayList.get(position).getFirstName().replace("\"", "");
        String lastname = getEventsDisCmtsAllDataArrayList.get(position).getLastName().replace("\"", "");


        /*User name setting*/
        String username = firstname + " " + lastname;
        holder.txt_username.setText(username);

        holder.txt_username.setTextColor(Color.parseColor("#000000"));

        String txtRepliesLike = getEventsDisCmtsAllDataArrayList.get(position).getTotalCmmtLikes();

        String liked = getEventsDisCmtsAllDataArrayList.get(position).getDisCmmtLikes();

        //Likes display user

        if (Integer.parseInt(liked) == 1) {

            holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
            getEventsDisCmtsAllDataArrayList.get(position).setLiked(true);

        } else if (Integer.parseInt(liked) == 0 || liked == null) {

            getEventsDisCmtsAllDataArrayList.get(position).setLiked(false);
            holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));

        }

        //Likes display count display

        if (Integer.parseInt(txtRepliesLike) == 0 || txtRepliesLike == null) {

            holder.txt_like.setVisibility(GONE);

        } else if (Integer.parseInt(txtRepliesLike) == 1) {

            holder.txt_like.setText(txtRepliesLike.concat(" ").concat("Like"));

        } else if (Integer.parseInt(txtRepliesLike) > 1) {

            holder.txt_like.setText(txtRepliesLike.concat(" ").concat("Likes"));
        }


        String txtRepliesCount = getEventsDisCmtsAllDataArrayList.get(position).getTotalCmmtReply();

        if (Integer.parseInt(txtRepliesCount) == 0 || txtRepliesCount == null) {
            holder.txt_replies_count.setVisibility(GONE);
            holder.txt_view_all_replies.setVisibility(GONE);
            holder.view_reply.setVisibility(GONE);

        } else {
            holder.txt_replies_count.setText("(".concat(txtRepliesCount).concat(")"));
        }

        //Edit visibility
        if (getEventsDisCmtsAllDataArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
            holder.txt_edit.setVisibility(View.VISIBLE);
        } else {
            holder.txt_edit.setVisibility(View.GONE);
        }


        holder.txt_comments.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getEventsDisCmtsAllDataArrayList.get(position).getDisComments().replace("\"", "")));

        holder.txt_comments.setTextColor(Color.parseColor("#000000"));


//Loading image url

        Utility.picassoImageLoader(getEventsDisCmtsAllDataArrayList.get(position).getPicture(),1,holder.img_comment, context);



//Posted date functionality

        String createdon = getEventsDisCmtsAllDataArrayList.get(position).getCreatedOn();

        Utility.setTimeStamp(createdon,   holder.txt_ago);




        //Users whom liked display
        holder.txt_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(context,Reply_Like_activity.class);
                intent.putExtra("Comment_id",getEventsDisCmtsAllDataArrayList.get(position).getCmmtDisId());
                context.startActivity(intent);
//                context.startActivity(new Intent(context, Reply_Like_activity.class));

            }
        });

        //like for a comment
        holder.img_comment_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);


                if (getEventsDisCmtsAllDataArrayList.get(position).isLiked()) {

                    holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                    getEventsDisCmtsAllDataArrayList.get(position).setLiked(false);
                    getEventsDisCmtsAllDataArrayList.get(position).setDisCmmtLikes(String.valueOf(0));


                    int total_likes = Integer.parseInt(getEventsDisCmtsAllDataArrayList.get(position).getTotalCmmtLikes());

                    if (total_likes == 1) {

                        total_likes = total_likes - 1;
                        getEventsDisCmtsAllDataArrayList.get(position).setTotalCmmtLikes(String.valueOf(total_likes));
                        holder.txt_like.setVisibility(View.GONE);


                    } else if (total_likes == 2) {

                        total_likes = total_likes - 1;
                        getEventsDisCmtsAllDataArrayList.get(position).setTotalCmmtLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Like");
                        holder.txt_like.setVisibility(View.VISIBLE);

                    } else {

                        total_likes = total_likes - 1;
                        getEventsDisCmtsAllDataArrayList.get(position).setTotalCmmtLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Likes");
                        holder.txt_like.setVisibility(View.VISIBLE);

                    }
                    if(Utility.isConnected(context)){
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        String createdby = Pref_storage.getDetail(context, "userId");
                        String clickedlike_timelineid = getEventsDisCmtsAllDataArrayList.get(position).getCmmtDisId();
                        Call<CommonOutputPojo> call = apiService.createEventsDiscussionCommentsLikes("create_events_discussion_comments_likes", Integer.parseInt(clickedlike_timelineid), 0, Integer.parseInt(createdby));
                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {
                                if(response.body() != null){

                                    if (response.body().getResponseCode() == 1) {



                                    }


                                }

                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }}else{
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }


                } else if (!getEventsDisCmtsAllDataArrayList.get(position).isLiked()) {


                    holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                    getEventsDisCmtsAllDataArrayList.get(position).setLiked(true);
                    getEventsDisCmtsAllDataArrayList.get(position).setDisCmmtLikes(String.valueOf(1));


                    int total_likes = Integer.parseInt(getEventsDisCmtsAllDataArrayList.get(position).getTotalCmmtLikes());

                    if (total_likes == 0) {

                        total_likes = total_likes + 1;
                        getEventsDisCmtsAllDataArrayList.get(position).setTotalCmmtLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Like");
                        holder.txt_like.setVisibility(View.VISIBLE);

                    } else {

                        total_likes = total_likes + 1;
                        getEventsDisCmtsAllDataArrayList.get(position).setTotalCmmtLikes(String.valueOf(total_likes));
                        holder.txt_like.setText(String.valueOf(total_likes) + " Likes");

                    }

                    if(Utility.isConnected(context)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");

                            String clickedlike_timelineid = getEventsDisCmtsAllDataArrayList.get(position).getCmmtDisId();

                            Call<CommonOutputPojo> call = apiService.createEventsDiscussionCommentsLikes("create_events_discussion_comments_likes", Integer.parseInt(clickedlike_timelineid), 1, Integer.parseInt(createdby));
                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    if (response.body() != null) {

                                        if (response.body().getResponseCode() == 1) {


                                        }


                                    }

                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });


        /* View replies */
        holder.txt_view_all_replies.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                holder.prog_reply.setVisibility(View.VISIBLE);

                if(getEventsDisCmtsAllReplyDataArrayList != null){

                    getEventsDisCmtsAllReplyDataArrayList.clear();

                }

                String disId = getEventsDisCmtsAllDataArrayList.get(position).getDisEvtId();

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                if(Utility.isConnected(context)) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        Call<GetEventsDisCmtsAllOutput> call = apiService.getEventsDisComments("get_events_discussion_comments_all", Integer.parseInt(disId), userid);

                        call.enqueue(new Callback<GetEventsDisCmtsAllOutput>() {
                            @Override
                            public void onResponse(Call<GetEventsDisCmtsAllOutput> call, Response<GetEventsDisCmtsAllOutput> response) {

                                if (response.body().getResponseCode() == 1) {

                                    holder.prog_reply.setVisibility(View.GONE);


                                    getEventsDisCmtsAllReplyDataArrayList = response.body().getData().get(position).getReply();


                                    if (getEventsDisCmtsAllReplyDataArrayList != null) {

                                        getEventsDisCmtReplyAdapter = new GetEventsDisCmtReplyAdapter(context, getEventsDisCmtsAllReplyDataArrayList);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                        holder.rv_comments_reply.setVisibility(View.VISIBLE);
                                        holder.rv_comments_reply.setAdapter(getEventsDisCmtReplyAdapter);
                                        holder.rv_comments_reply.setLayoutManager(mLayoutManager);
                                        holder.rv_comments_reply.setHasFixedSize(true);
                                        getEventsDisCmtReplyAdapter.notifyDataSetChanged();
                                        holder.rv_comments_reply.setNestedScrollingEnabled(false);
                                        holder.txt_view_all_replies.setVisibility(View.GONE);
                                        holder.txt_replies_count.setVisibility(View.GONE);
                                        holder.hide_replies.setVisibility(View.VISIBLE);

                                    }


                                }

                            }

                            @Override
                            public void onFailure(Call<GetEventsDisCmtsAllOutput> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else {
                    Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                }


            }


        });


        //hide reply

        holder.hide_replies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.rv_comments_reply.setVisibility(View.GONE);
                holder.hide_replies.setVisibility(View.GONE);
                holder.txt_view_all_replies.setVisibility(View.VISIBLE);
                holder.txt_replies_count.setVisibility(View.VISIBLE);

                if(getEventsDisCmtsAllReplyDataArrayList != null){

                    getEventsDisCmtsAllReplyDataArrayList.clear();

                }

            }
        });

        //reply for a comment
        holder.txt_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                viewDiscussionComments.clickedcommentposition(v, Integer.parseInt(getEventsDisCmtsAllDataArrayList.get(position).getCmmtDisId()));
                viewDiscussionComments.clicked_username(v, getEventsDisCmtsAllDataArrayList.get(position).getFirstName(), Integer.parseInt(getEventsDisCmtsAllDataArrayList.get(position).getCmmtDisId()));
                viewDiscussionComments.clickedforreply(v, 11);

            }
        });

        //Edit a comment API
        holder.txt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewDiscussionComments.clickedcommentposition_eventid(v, getEventsDisCmtsAllDataArrayList.get(position).getDisComments(), Integer.parseInt(getEventsDisCmtsAllDataArrayList.get(position).getCmmtDisId()), Integer.parseInt(getEventsDisCmtsAllDataArrayList.get(position).getDisEvtId()));
                viewDiscussionComments.clickedfor_edit(v, 22);


            }
        });


        //Delete a comment API
        holder.delete_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this comment?")
                        .setContentText("Won't be able to recover this comment anymore!")
                        .setConfirmText("Delete")
                        .setCancelText("Cancel")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                if(Utility.isConnected(context)) {
                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                    try {

                                        String createuserid = getEventsDisCmtsAllDataArrayList.get(position).getUserId();
                                        String dis_id = getEventsDisCmtsAllDataArrayList.get(position).getDisEvtId();
                                        String commentid = getEventsDisCmtsAllDataArrayList.get(position).getCmmtDisId();

                                        Call<CommonOutputPojo> call = apiService.deleteEventDiscussionComment("create_edit_delete_events_discussion_comments", Integer.parseInt(commentid), Integer.parseInt(dis_id), Integer.parseInt(createuserid), 1);
                                        call.enqueue(new Callback<CommonOutputPojo>() {
                                            @Override
                                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                if (response.body().getResponseCode() == 1) {

                                                    removeAt(position);

                                                    Toast.makeText(context, "Deleted!!", Toast.LENGTH_LONG).show();

//                                                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//                                                            .setTitleText("Deleted!!")
//                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                                                @Override
//                                                                public void onClick(SweetAlertDialog sDialog) {
//                                                                    sDialog.dismissWithAnimation();
//
//
//                                                                }
//                                                            })
//                                                            .show();


                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                //Error
                                                Log.e("FailureError", "" + t.getMessage());
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }else {
                                    Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                }

                            }
                        })
                        .show();


            }
        });

    }

    /*Remove postion*/
    private void removeAt(int position) {

        getEventsDisCmtsAllDataArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getEventsDisCmtsAllDataArrayList.size());

    }

    @Override
    public int getItemCount() {

        return getEventsDisCmtsAllDataArrayList.size();

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view_reply;
        ImageView img_comment;
        ProgressBar prog_reply;
        LinearLayout rl_comment_layout;
        RecyclerView rv_comments_reply;
        ImageButton img_comment_like, delete_comment;

        TextView txt_username, txt_edit,
                txt_like, txt_ago, txt_reply, txt_comments, txt_view_all_replies, hide_replies, txt_replies_count;


        //Interface activity
        ViewDiscussionComments viewDiscussionComments;


        public MyViewHolder(View itemView, ViewDiscussionComments viewDiscussionComments) {
            super(itemView);


            this.viewDiscussionComments = viewDiscussionComments;

            view_reply = (View) itemView.findViewById(R.id.view_reply);

            txt_ago = (TextView) itemView.findViewById(R.id.txt_ago);
            txt_like = (TextView) itemView.findViewById(R.id.txt_like);
            txt_edit = (TextView) itemView.findViewById(R.id.txt_edit);
            txt_reply = (TextView) itemView.findViewById(R.id.txt_reply);
            hide_replies = (TextView) itemView.findViewById(R.id.hide_replies);
            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_comments = (TextView) itemView.findViewById(R.id.txt_comments);
            txt_replies_count = (TextView) itemView.findViewById(R.id.txt_replies_count);
            rl_comment_layout = (LinearLayout) itemView.findViewById(R.id.rl_comment_layout);
            txt_view_all_replies = (TextView) itemView.findViewById(R.id.txt_view_all_replies);


            delete_comment = (ImageButton) itemView.findViewById(R.id.del_comment);
            img_comment_like = (ImageButton) itemView.findViewById(R.id.img_comment_like);


            img_comment = (ImageView) itemView.findViewById(R.id.img_comment);

            prog_reply = (ProgressBar) itemView.findViewById(R.id.prog_reply);

            rv_comments_reply = (RecyclerView) itemView.findViewById(R.id.rv_comments_reply);


            txt_reply.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

//            comments_activity.clickedcommentposition(v, getAdapterPosition());

        }


    }


}


