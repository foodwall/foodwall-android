package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.kaspontech.foodwall.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class TimelineHeaderHolder extends RecyclerView.ViewHolder {
    TextView txtStoryAdd;
    ImageView img_status_timeline;
    ImageView imgStsUpdate;
    RecyclerView rv_stories;
    CircleImageView userphotoComment;
    EditText edittxtComment;
    TextView txt_adapter_post;
    ImageButton add_post;
    View ll_no_post;

    private TimelineHeaderItemListener timelineHeaderItemListener;

    public TimelineHeaderHolder(@NonNull View itemView, final TimelineHeaderItemListener timelineHeaderItemListener) {
        super(itemView);
        this.timelineHeaderItemListener = timelineHeaderItemListener;
        txtStoryAdd = itemView.findViewById(R.id.txt_story_add);
        img_status_timeline = itemView.findViewById(R.id.img_status_timeline);
        imgStsUpdate = itemView.findViewById(R.id.img_sts_update);
        rv_stories = itemView.findViewById(R.id.rv_stories);
        userphotoComment = itemView.findViewById(R.id.userphoto_comment);
        edittxtComment = itemView.findViewById(R.id.edittxt_comment);
        txt_adapter_post = itemView.findViewById(R.id.txt_adapter_post);
        add_post = itemView.findViewById(R.id.add_post);
        ll_no_post = itemView.findViewById(R.id.ll_no_post);

        ll_no_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timelineHeaderItemListener.onNoItemYetClicked();
            }
        });

        add_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timelineHeaderItemListener.onAddPostClicked();
            }
        });

        txtStoryAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timelineHeaderItemListener.txtStoryAddClicked();
            }
        });

        imgStsUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timelineHeaderItemListener.imgStsUpdateClicked();
            }
        });

        edittxtComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timelineHeaderItemListener.edittxtCommentClicked();
            }
        });

        img_status_timeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timelineHeaderItemListener.imgStatusTimelineClicked();
            }
        });
    }

    public interface TimelineHeaderItemListener{
        void onAddPostClicked();
        void txtStoryAddClicked();
        void imgStsUpdateClicked();
        void edittxtCommentClicked();
        void imgStatusTimelineClicked();
        void onNoItemYetClicked();
    }
}
