package com.kaspontech.foodwall.adapters.events;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.kaspontech.foodwall.eventspackage.ViewEvent;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Utility;


import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SelectShareInviteAdapter extends RecyclerView.Adapter<SelectShareInviteAdapter.MyViewHolder>
        implements Filterable {
    /**
     * Context
     **/

    Context context;
    /**
     * View event
     **/
    ViewEvent viewEvent;

    /**
     * Array lists
     **/
    private List<GetFollowersData> contactList;
    private List<GetFollowersData> contactListFiltered;

    List<Integer> invitedPeopleList = new ArrayList<>();
    /**
     * Followers pojo
     **/
    GetFollowersData getFollowersData;

    /**
     * Application TAG
     **/

    private static final String TAG = "SelectInvitePeopleAdapt";

    public SelectShareInviteAdapter(Context context, List<GetFollowersData> contactList, List<Integer> invitedPeopleList) {
        this.context = context;
        this.contactList = contactList;
        this.contactListFiltered = contactList;
        this.invitedPeopleList = invitedPeopleList;
        viewEvent = (ViewEvent) context;
    }

    @Override
    public Filter getFilter() {

        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = contactList;
                } else {

                    List<GetFollowersData> filteredList = new ArrayList<>();

                    for (GetFollowersData row : contactList) {


                        String userName = row.getFirstName()+" "+row.getLastName();

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (userName.toLowerCase().contains(charString.toLowerCase())) {

                            filteredList.add(row);

                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<GetFollowersData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    /*My View Holder*/

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        CircleImageView followerPhoto;
        TextView userName;
        TextView tv_cohost_text;
        CheckBox selectFollowerBox;
        ViewEvent viewEvent;

        public MyViewHolder(View view, ViewEvent viewEvent) {
            super(view);

            this.viewEvent = viewEvent;
            followerPhoto = (CircleImageView) view.findViewById(R.id.user_image);
            userName = (TextView) view.findViewById(R.id.user_name);
            tv_cohost_text = (TextView) view.findViewById(R.id.tv_cohost_text);
            selectFollowerBox = (CheckBox) view.findViewById(R.id.followers_checked);
//            selectFollowerBox.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

//            viewEvent.getInvitedPeople(view, ,getAdapterPosition());

        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_select_followers, parent, false);
        return new MyViewHolder(itemView, viewEvent);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        getFollowersData = contactListFiltered.get(position);


        /*Image loading*/

        Utility.picassoImageLoader(getFollowersData.getPicture(),0,holder.followerPhoto, context);


        /*Setting user name */
        holder.userName.setText(getFollowersData.getFirstName() + " " + getFollowersData.getLastName());

        /*Followers check box*/
        if(contactListFiltered.get(position).isSelected()){
            holder.selectFollowerBox.setChecked(true);
        }else{
            holder.selectFollowerBox.setChecked(false);
        }


        /*for (Integer value : invitedPeopleList) {

            String one = getFollowersData.getFollowerId();
            String two = String.valueOf(value);

            if (contactListFiltered.get(position).getFollowerId().equals(String.valueOf(value))) {

                Log.e(TAG, "onBindViewHolder: "+value );
                holder.selectFollowerBox.setChecked(true);

            }else {

                Log.e(TAG, "onBindViewHolder: "+value );

            }

        }*/

        /*Followers check box*/

        holder.selectFollowerBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (contactListFiltered.get(position).isSelected()) {

                    contactListFiltered.get(position).setSelected(false);
                    viewEvent.getSharedInvitedPeople(holder.selectFollowerBox, contactListFiltered.get(position), 0);

                } else {

                    contactListFiltered.get(position).setSelected(true);
                    viewEvent.getSharedInvitedPeople(holder.selectFollowerBox, contactListFiltered.get(position), 1);

                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }


}

