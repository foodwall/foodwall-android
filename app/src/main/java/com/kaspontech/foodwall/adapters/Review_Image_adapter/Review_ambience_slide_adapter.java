package com.kaspontech.foodwall.adapters.Review_Image_adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.GlideApp;
import com.squareup.picasso.Picasso;
//

import java.util.List;

//

public class Review_ambience_slide_adapter extends PagerAdapter implements View.OnTouchListener {


//    private ArrayList<Integer> IMAGES;

//    ArrayList<Get_ambience_image_all> IMAGES = new ArrayList<>();
    private List<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;

    private PhotoView imageView;
    private ImageButton remove_image;

    private PhotoRemovedListener photoRemovedListener;

    public interface PhotoRemovedListener {

        void removedImageLisenter(List<String> imagesList);

    }

    public Review_ambience_slide_adapter(Context context, List<String> imagesEncodedList,PhotoRemovedListener photoRemovedListener) {
        this.context = context;
        this.IMAGES = imagesEncodedList;
        this.photoRemovedListener = photoRemovedListener;
        inflater = LayoutInflater.from(context);

    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {

        View imageLayout = inflater.inflate(R.layout.slide_image_layout, view, false);

        assert imageLayout != null;

        imageView = (PhotoView) imageLayout.findViewById(R.id.image);
        remove_image = (ImageButton) imageLayout.findViewById(R.id.remove_image);

        remove_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                IMAGES.remove(position);
                notifyDataSetChanged();
                photoRemovedListener.removedImageLisenter(IMAGES);

            }
        });

        try {
        Log.e("IMAGES",""+IMAGES.toString());


//          Picasso.get().load(IMAGES.toString().replace("[", "").replace("]", "")).fit().centerInside().into(imageView);
//            Utility.picassoImageLoader(IMAGES.get(position).replace("[", "").replace("]", ""),
//                    0,imageView);
            GlideApp.with(context)
                    .load(IMAGES.get(position).replace("[", "").replace("]", "")).centerInside()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.1f)
                    .into(imageView);

            view.addView(imageLayout, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }





        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}


