package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.onCommentsPostListener;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;


public class Events_going_adapter_timeline extends RecyclerView.ViewHolder implements View.OnClickListener {


    ImageView eventBackground, eventgoing_user_photo, userphoto_comment, img_shareadapter;


    ImageView event_user_photo;
    ImageButton interested, going, share;
    LinearLayout viewEvent, ll_interested, ll_going, ll_share;
    TextView date, location, eventName, interestedText, goingText, shareText, txt_going_event_text, txt_view_event_going_text,

    txt_adapter_post, commenttest, comment,
            View_all_comment, liketxt, like, txt_created_on, txt_view_event_going_username, txt_view_event_going_name;

    ProgressBar comments_progressbar;

    //Edittext
    EmojiconEditText edittxt_comment;

    //ShineButton like
    ImageButton img_like,img_commentadapter;

    //Relative layout
    RelativeLayout rl_share, like_rl, rl_comment_adapter, rl_post_timeline,
            rl_share_adapter, rl_likes_layout, rl_comments_layout,rl_comment_share;

    //display latest comment
    TextView username_latestviewed, created_on, recently_commented;
    ImageView userphoto,img_eventsgoingsharechat;

    RelativeLayout recently_commented_ll;

    onCommentsPostListener onCommentsPostListener;

    View itemView;


    public Events_going_adapter_timeline(View view, onCommentsPostListener onCommentsPostListener) {
        super(view);

        this.onCommentsPostListener = onCommentsPostListener;
        this.itemView = view;

        eventBackground = (ImageView) view.findViewById(R.id.event_background);
        eventgoing_user_photo = (ImageView) view.findViewById(R.id.eventgoing_user_photo);

        userphoto_comment = (ImageView) view.findViewById(R.id.userphoto_comment);
        img_shareadapter = (ImageView) view.findViewById(R.id.img_shareadapter);
        img_commentadapter = (ImageButton) view.findViewById(R.id.img_commentadapter);

        //Editext
        edittxt_comment = (EmojiconEditText) view.findViewById(R.id.edittxt_comment);
        //Like Button
        img_like = (ImageButton) view.findViewById(R.id.img_like);

        txt_view_event_going_text = (TextView) view.findViewById(R.id.txt_view_event_going_text);
        txt_view_event_going_username = (TextView) view.findViewById(R.id.txt_view_event_going_username);
        txt_view_event_going_name = (TextView) view.findViewById(R.id.txt_view_event_going_name);

        txt_adapter_post = (TextView) view.findViewById(R.id.txt_adapter_post);
        commenttest = (TextView) view.findViewById(R.id.commenttest);
        comment = (TextView) view.findViewById(R.id.comment);
        View_all_comment = (TextView) view.findViewById(R.id.View_all_comment);
        liketxt = (TextView) view.findViewById(R.id.liketxt);
        like = (TextView) view.findViewById(R.id.like);
        txt_created_on = (TextView) view.findViewById(R.id.txt_created_on);

        date = (TextView) view.findViewById(R.id.event_date);
        img_eventsgoingsharechat = (ImageView) view.findViewById(R.id.img_eventsgoingsharechat);



        location = (TextView) view.findViewById(R.id.event_location);
        eventName = (TextView) view.findViewById(R.id.event_name);

     /*   interestedText = (TextView) view.findViewById(R.id.interest_text);
        goingText = (TextView) view.findViewById(R.id.going_text);
        shareText = (TextView) view.findViewById(R.id.share_text);*/

        viewEvent = (LinearLayout) view.findViewById(R.id.ll_view_event);

        //Relative Layout
        rl_share = (RelativeLayout) view.findViewById(R.id.rl_share);
        like_rl = (RelativeLayout) view.findViewById(R.id.like_rl);
        rl_comment_adapter = (RelativeLayout) view.findViewById(R.id.rl_comment_adapter);
        rl_post_timeline = (RelativeLayout) view.findViewById(R.id.rl_post_timeline);
        rl_likes_layout = (RelativeLayout) view.findViewById(R.id.rl_likes_layout);
        rl_comments_layout = (RelativeLayout) view.findViewById(R.id.rl_comments_layout);
        rl_share_adapter = (RelativeLayout) view.findViewById(R.id.rl_share_adapter);
        rl_comment_share = (RelativeLayout) view.findViewById(R.id.rl_comment_share);


        recently_commented_ll = (RelativeLayout) view.findViewById(R.id.recently_commented_ll);
        username_latestviewed = (TextView) view.findViewById(R.id.username_latestviewed);
        created_on = (TextView) view.findViewById(R.id.created_on);
        recently_commented = (TextView) view.findViewById(R.id.recently_commented);

        comments_progressbar = (ProgressBar) view.findViewById(R.id.comments_progressbar);

        txt_adapter_post.setOnClickListener(this);
        img_eventsgoingsharechat.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.txt_adapter_post:

                onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),itemView);
                break;

            case R.id.img_eventsgoingsharechat:

                onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),itemView);
                break;
        }
    }
}