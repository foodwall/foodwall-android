package com.kaspontech.foodwall.adapters.TimeLine;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Utility;


import java.util.ArrayList;
import java.util.List;

//

public class timeline_only_slide_adapter extends PagerAdapter implements View.OnTouchListener {


    //    private ArrayList<Integer> IMAGES;
    private List<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;

    private ArrayList<String> ImagesArray = new ArrayList<String>();
    private ArrayList<Timeline_image_only_pojo> Imagestotallist = new ArrayList<>();
    String imagee;

    Timeline_image_only_pojo timelineImageOnlyPojo;
    private PhotoView imageView;

    /*public timeline_only_slide_adapter(Context context,ArrayList<Timeline_image_only_pojo> gettinglist){
            this.context=context;
            this.Imagestotallist=gettinglist;
            inflater=LayoutInflater.from(context);


            }*/
    public timeline_only_slide_adapter(Context context, String image) {
        this.context = context;
        this.imagee = image;
        inflater = LayoutInflater.from(context);


    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return Integer.parseInt(imagee);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slide_image_layout, view, false);

        assert imageLayout != null;
        imageView = (PhotoView) imageLayout.findViewById(R.id.image);


//        timelineImageOnlyPojo = Imagestotallist.get(position);




 /*       imageView= (ZoomImageView) imageLayout
                .findViewById(R.id.image);*/
/*

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            String userid = Pref_storage.getDetail(context, "userId");

            Call<Timeline_image_only_output_pojo> call = apiService.Get_timeline_image_all("get_timeline_image_over", Integer.parseInt(userid));
            call.enqueue(new Callback<Timeline_image_only_output_pojo>() {
                @Override
                public void onResponse(Call<Timeline_image_only_output_pojo> call, Response<Timeline_image_only_output_pojo> response) {


//                    Toast.makeText(context, "vishnulikes" + response.body().getResponseCode(), Toast.LENGTH_SHORT).show();
                    Log.e("vishnulikes", "" + response.body().getResponseCode());

                    if (response.body().getResponseCode() == 1) {


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            timelineImageOnlyPojo= new Timeline_image_only_pojo("","","");


                            for (int ab=0; ab<response.body().getData().get(ab).getTimelineimage().length();ab++){
                                String picc= response.body().getData().get(ab).getTimelineimage();
                                ImagesArray.add(picc);


                                Log.e("time_size","time_size"+response.body().getData().get(ab).getTimelineimage().length());
                            }

                        }
                    }

                }

                @Override
                public void onFailure(Call<Timeline_image_only_output_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
*/


        Log.e("imagee..", "" + imagee);


        Utility.picassoImageLoader(imagee.replace("[", "").replace("]", ""),
                1,imageView, context);



//        GlideApp.with(context)
////        .load(ImagesArray.get(position).replace("[","").replace("]",""))
//                .load(imagee.replace("[", "").replace("]", ""))
//                .centerCrop()
//                .into(imageView);


        view.addView(imageLayout, 0);


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }


}