package com.kaspontech.foodwall.adapters.events.eventdiscussion;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.eventspackage.ViewDiscussionComments;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetEventsDisCmtsAllReplyData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetEventsDisCmtReplyAdapter extends RecyclerView.Adapter<GetEventsDisCmtReplyAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     **/
    private Context context;
    /**
     * All reply data
     **/
    GetEventsDisCmtsAllReplyData getEventsDisCmtsAllReplyData;
    /**
     * Array lists
     **/
    private List<GetEventsDisCmtsAllReplyData> getEventsDisCmtsAllReplyDataArrayList = new ArrayList<>();

    /**
     * Relative layout
     **/

    public RelativeLayout totalReplyLayout;

    /**
     * View discussion comments pojo
     **/
    ViewDiscussionComments viewDiscussionComments;


    public GetEventsDisCmtReplyAdapter(Context c, List<GetEventsDisCmtsAllReplyData> getEventsDisCmtsAllReplyDataArrayList) {
        this.context = c;
        this.getEventsDisCmtsAllReplyDataArrayList = getEventsDisCmtsAllReplyDataArrayList;
        this.viewDiscussionComments = (ViewDiscussionComments) context;

    }

    @NonNull
    @Override
    public GetEventsDisCmtReplyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_reply_comments, parent, false);
        // set the view's size, margins, paddings and layout parameters
        GetEventsDisCmtReplyAdapter.MyViewHolder vh = new GetEventsDisCmtReplyAdapter.MyViewHolder(v, viewDiscussionComments);


        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final GetEventsDisCmtReplyAdapter.MyViewHolder holder, final int position) {

        getEventsDisCmtsAllReplyData = getEventsDisCmtsAllReplyDataArrayList.get(position);
        String firstname = getEventsDisCmtsAllReplyDataArrayList.get(position).getFirstName().replace("\"", "");
        String lastname = getEventsDisCmtsAllReplyDataArrayList.get(position).getLastName().replace("\"", "");

        String username = firstname + " " + lastname;

        /*User name setting*/
        holder.txt_username_reply.setText(username);

        holder.txt_username_reply.setTextColor(Color.parseColor("#000000"));

        holder.txt_comments_reply.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getEventsDisCmtsAllReplyDataArrayList.get(position).getDisCmmtReply().replace("\"", "")));

        holder.txt_comments_reply.setTextColor(Color.parseColor("#000000"));


        /* Loading image url */


        Utility.picassoImageLoader(getEventsDisCmtsAllReplyDataArrayList.get(position).getPicture(),
                1,holder.img_comment_reply,context );




        /* Posted date functionality */
        String ago = getEventsDisCmtsAllReplyDataArrayList.get(position).getCreatedOn();
        Utility.setTimeStamp(ago,holder.txt_ago_reply);

        /*Edit comment on click listener*/
        holder.txt_comment_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewDiscussionComments.clickedcommentposition_reply_edit(v, getEventsDisCmtsAllReplyDataArrayList.get(position).getDisCmmtReply(), Integer.parseInt(getEventsDisCmtsAllReplyDataArrayList.get(position).getReplyId()), Integer.parseInt(getEventsDisCmtsAllReplyDataArrayList.get(position).getCmmtDisId()));
                viewDiscussionComments.clickedfor_replyedit(v, 44);
                viewDiscussionComments.clickedcommentposition(v, Integer.parseInt(getEventsDisCmtsAllReplyDataArrayList.get(position).getCmmtDisId()));
                viewDiscussionComments.clickedcomment_userid_position(v, Integer.parseInt(getEventsDisCmtsAllReplyDataArrayList.get(position).getUserId()));
                viewDiscussionComments.clickedfor_replyid(v, Integer.parseInt(getEventsDisCmtsAllReplyDataArrayList.get(position).getReplyId()));


            }
        });

        /*Delete reply comment on click listener*/

        holder.del_reply_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this reply?")
                        .setContentText("Won't be able to recover this reply anymore!")
                        .setConfirmText("Delete")
                        .setCancelText("Cancel")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                if(Utility.isConnected(context)){
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                try {

                                    String createuserid = getEventsDisCmtsAllReplyDataArrayList.get(position).getUserId();
                                    String cmmtDisId = getEventsDisCmtsAllReplyDataArrayList.get(position).getCmmtDisId();
                                    String commentid = getEventsDisCmtsAllReplyDataArrayList.get(position).getReplyId();

                                    Call<CommonOutputPojo> call = apiService.deleteEventDiscussionCommentReply("create_edit_delete_events_discussion_comments_reply", Integer.parseInt(commentid), Integer.parseInt(cmmtDisId), Integer.parseInt(createuserid),1);
                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                            if (response.body().getResponseCode() == 1) {
                                               /*Remove postioo*/
                                                removeAt(position);

                                                Toast.makeText(context, "Deleted!!", Toast.LENGTH_LONG).show();

//                                                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//                                                        .setTitleText("Deleted!!")
//                                                        .setContentText("Your reply has been deleted.")
//                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                                            @Override
//                                                            public void onClick(SweetAlertDialog sDialog) {
//                                                                sDialog.dismissWithAnimation();
//
//                                                            }
//                                                        })
//                                                        .show();


                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "" + t.getMessage());
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }}else {
                                    Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                }

                            }
                        })
                        .show();


            }
        });


    }
    /*Remove postion*/
    private void removeAt(int position) {
        getEventsDisCmtsAllReplyDataArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getEventsDisCmtsAllReplyDataArrayList.size());
    }

    @Override
    public int getItemCount() {

        return getEventsDisCmtsAllReplyDataArrayList.size();

    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view_reply;
        ImageView img_comment_reply;
        ImageButton del_reply_comment;
        LinearLayout rl_reply_comment_layout;

        TextView txt_username_reply, txt_comment_edit,
                txt_ago_reply, txt_reply_like, txt_comment_reply, txt_comments_reply, txt_view_all_replies, txt_replies_count;


        ViewDiscussionComments viewDiscussionComments;

        public MyViewHolder(View itemView, ViewDiscussionComments viewDiscussionComments) {
            super(itemView);

            this.viewDiscussionComments = viewDiscussionComments;

            view_reply = (View) itemView.findViewById(R.id.view_reply);

            txt_ago_reply = (TextView) itemView.findViewById(R.id.txt_ago_reply);
            txt_reply_like = (TextView) itemView.findViewById(R.id.txt_reply_like);
            txt_comment_edit = (TextView) itemView.findViewById(R.id.txt_comment_edit);
            txt_replies_count = (TextView) itemView.findViewById(R.id.txt_replies_count);
            txt_comment_reply = (TextView) itemView.findViewById(R.id.txt_comment_reply);
            txt_username_reply = (TextView) itemView.findViewById(R.id.txt_username_reply);
            txt_comments_reply = (TextView) itemView.findViewById(R.id.txt_comments_reply);
            txt_view_all_replies = (TextView) itemView.findViewById(R.id.txt_view_all_replies);


            img_comment_reply = (ImageView) itemView.findViewById(R.id.img_comment_reply);
            del_reply_comment = (ImageButton) itemView.findViewById(R.id.del_reply_comment);

            totalReplyLayout = (RelativeLayout) itemView.findViewById(R.id.total_reply_layout);

            rl_reply_comment_layout = (LinearLayout) itemView.findViewById(R.id.rl_reply_comment_layout);


        }


        @Override
        public void onClick(View v) {

        }

    }


}

