package com.kaspontech.foodwall.adapters.questionansweradapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.modelclasses.AnswerPojoClass;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Answer.create_answer_votesResponse;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerComments.AnswerCmtData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerComments.GetAnswerCmtsOutput;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerData;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.questionspackage.ViewQuestionAnswer;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetAllAnswersAdapter extends RecyclerView.Adapter<GetAllAnswersAdapter.MyViewHolder> {


    /**
     * Context
     **/
    private Context context;

    /**
     * Answer arraylist
     */
    List<AnswerData> getAnswerDataList = new ArrayList<>();

    /**
     * Answer details pojo
     */
    AnswerData answerData;
    /**
     * Answer flags
     */
    boolean upVotedFlag = false;
    boolean downVotedFlag = false;
    /**
     * Lat & Long
     */
    private double latitude;
    private double longitude;
    /**
     * Gps tracker
     */
    GPSTracker gpsTracker;
    /**
     * Answer comment adapter
     */
    GetAnsCmtAdapter getAnsCmtAdapter;
    /**
     * Answer comments arraylist
     */
    List<AnswerCmtData> answerCmtDataList = new ArrayList<>();

    /**
     * View question answer activty interface
     */
    ViewQuestionAnswer viewQuestionAnswer;


      onUpvotedownvoteUpdate onUpvotedownvoteUpdate;

    public GetAllAnswersAdapter(Context context, List<AnswerData> getAnswerDataList,onUpvotedownvoteUpdate onUpvotedownvoteUpdate1) {
        this.context = context;
        this.getAnswerDataList = getAnswerDataList;
        viewQuestionAnswer = (ViewQuestionAnswer) context;
        this.onUpvotedownvoteUpdate=onUpvotedownvoteUpdate1;


    }

    /*Widget initialization*/
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ProgressBar progress_ans_comments,progress_answer;
        CircleImageView userImage, ans_cmt_user_image;
        EditText edittxt_answer, edit_answer_comments;
        RecyclerView view_ans_cmts_recylerview;
        TextView tv_upVote,tv_downVote,userName, userAnswer, commentedTime, txt_adapter_post, txt_adapter_cancel, ans_cmt_adapter_post, ans_cmt_user_name, ans_cmt, tv_view_all_comments;
        ImageButton ansOptions, upVoteBtn, downVoteBtn;
        LinearLayout upVote, downVote, ll_answer_option, ll_edit_answer, ll_answer_comments, ll_commented_profile, ll_view_all_comments,ll_hide_all_comments;
        ViewQuestionAnswer viewQuestionAnswer;

        View viewitem;
        onUpvotedownvoteUpdate onUpvotedownvoteUpdate;

        public MyViewHolder(View view, ViewQuestionAnswer viewQuestionAnswer, final GetAllAnswersAdapter.onUpvotedownvoteUpdate onUpvotedownvoteUpdate) {

            super(view);

            this.viewitem=view;
            this.onUpvotedownvoteUpdate=onUpvotedownvoteUpdate;

            this.viewQuestionAnswer = viewQuestionAnswer;

            userImage = (CircleImageView) view.findViewById(R.id.user_image);
            progress_ans_comments = (ProgressBar) view.findViewById(R.id.progress_ans_comments);
            progress_answer = (ProgressBar) view.findViewById(R.id.progress_answer);
            userName = (TextView) view.findViewById(R.id.user_name);
            userAnswer = (TextView) view.findViewById(R.id.user_answer);
            txt_adapter_post = (TextView) view.findViewById(R.id.txt_adapter_post);
            ans_cmt_adapter_post = (TextView) view.findViewById(R.id.ans_cmt_adapter_post);
            txt_adapter_cancel = (TextView) view.findViewById(R.id.txt_adapter_cancel);
            commentedTime = (TextView) view.findViewById(R.id.commented_time);
            ans_cmt_user_name = (TextView) view.findViewById(R.id.ans_cmt_user_name);
            ans_cmt = (TextView) view.findViewById(R.id.ans_cmt);
            tv_view_all_comments = (TextView) view.findViewById(R.id.tv_view_all_comments);


            tv_upVote = (TextView)view.findViewById(R.id.tv_upVote);
            tv_downVote = (TextView)view.findViewById(R.id.tv_downVote);



            edittxt_answer = (EditText) view.findViewById(R.id.edittxt_answer);
            edit_answer_comments = (EditText) view.findViewById(R.id.edit_answer_comments);
            ansOptions = (ImageButton) view.findViewById(R.id.ans_options);
            upVoteBtn = (ImageButton) view.findViewById(R.id.upVoteBtn);
            downVoteBtn = (ImageButton) view.findViewById(R.id.downVoteBtn);
            upVote = (LinearLayout) view.findViewById(R.id.ll_upvote);
            downVote = (LinearLayout) view.findViewById(R.id.ll_downvote);
            ll_answer_option = (LinearLayout) view.findViewById(R.id.ll_answer_option);
            ll_edit_answer = (LinearLayout) view.findViewById(R.id.ll_edit_answer);
            ll_answer_comments = (LinearLayout) view.findViewById(R.id.ll_answer_comments);
            ll_commented_profile = (LinearLayout) view.findViewById(R.id.ll_commented_profile);
            ll_view_all_comments = (LinearLayout) view.findViewById(R.id.ll_view_all_comments);
            ll_hide_all_comments = (LinearLayout) view.findViewById(R.id.ll_hide_all_comments);
            view_ans_cmts_recylerview = (RecyclerView) view.findViewById(R.id.view_ans_cmts_recylerview);
            edit_answer_comments.setOnClickListener(this);

            upVote.setOnClickListener(this);
            downVote.setOnClickListener(this);
            tv_upVote.setOnClickListener(this);
            tv_downVote.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.tv_upVote:
                    onUpvotedownvoteUpdate.onUpvoteDownvote(v, getAdapterPosition(), viewitem);
                    break;

                case R.id.tv_downVote :
                    onUpvotedownvoteUpdate.onUpvoteDownvote(v, getAdapterPosition(), viewitem);
                    break;



            }

        }

    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_get_all_answer, parent, false);

        gpsTracker = new GPSTracker(context);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        return new MyViewHolder(itemView,viewQuestionAnswer,onUpvotedownvoteUpdate);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        answerData = getAnswerDataList.get(position);

        // Question Details Section

        /*User image loading*/
        Utility.picassoImageLoader(answerData.getPicture(),
                0,holder.userImage,context );


        /*User name displaying*/

        holder.userName.setText(answerData.getFirstName() + " " + answerData.getLastName());
        holder.userAnswer.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(answerData.getAskAnswer()));

        String userid = Pref_storage.getDetail(context, "userId");

        String createdByid = getAnswerDataList.get(position).getCreatedBy();


        /*User name click listener*/
        holder.userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userId = getAnswerDataList.get(position).getUserId();

                if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", userId);
                    context.startActivity(intent);

                }

            }
        });



        /*User image click listener*/

        holder.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String userId = getAnswerDataList.get(position).getUserId();

                if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", userId);
                    context.startActivity(intent);

                }

            }
        });


        /*Answer option visibiltiy*/
        if (userid.equals(createdByid)) {

            holder.ll_answer_option.setVisibility(View.VISIBLE);

        } else {

            holder.ll_answer_option.setVisibility(View.GONE);

        }


        /* Answer Comments Section */


        if (Integer.parseInt(getAnswerDataList.get(position).getTotalComments()) != 0) {

            holder.ll_view_all_comments.setVisibility(View.VISIBLE);

        } else {

            holder.ll_view_all_comments.setVisibility(View.GONE);

        }


        /*Edit answer comments click listener*/
        holder.edit_answer_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(context);
                LayoutInflater inflater11 = LayoutInflater.from(context);
                @SuppressLint("InflateParams")
                final View dialogView1 = inflater11.inflate(R.layout.alert_answer_comment, null);
                dialogBuilder1.setView(dialogView1);
                dialogBuilder1.setTitle("");

                final AlertDialog dialog1 = dialogBuilder1.create();
                dialog1.show();

                final EditText edittxtComment;
                CircleImageView cmt_user_image,userphoto_comment;
                final TextView user_answer,cmt_user_name,txt_adapter_post;

                edittxtComment = (EditText) dialogView1.findViewById(R.id.edittxt_comment);
                cmt_user_image = (CircleImageView) dialogView1.findViewById(R.id.cmt_user_image);
                userphoto_comment = (CircleImageView) dialogView1.findViewById(R.id.userphoto_comment);
                user_answer = (TextView) dialogView1.findViewById(R.id.user_answer);
                cmt_user_name = (TextView) dialogView1.findViewById(R.id.cmt_user_name);
                txt_adapter_post = (TextView) dialogView1.findViewById(R.id.txt_adapter_post);

                /*User image loading*/
                Utility.picassoImageLoader(getAnswerDataList.get(position).getPicture(),
                        0,cmt_user_image, context);



                /*Self User image loading*/

                Utility.picassoImageLoader(Pref_storage.getDetail(context, "picture_user_login"),
                        0,userphoto_comment, context);


                cmt_user_name.setText(getAnswerDataList.get(position).getFirstName() + " " + getAnswerDataList.get(position).getLastName());
                user_answer.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getAnswerDataList.get(position).getAskAnswer()));

                /*Edit text listener*/
                edittxtComment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {


                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        /*Post test visibility listener*/
                        if (edittxtComment.getText().toString().trim().length() != 0) {
                            txt_adapter_post.setVisibility(View.VISIBLE);
                        } else {
                            txt_adapter_post.setVisibility(View.GONE);
                        }


                    }
                });

                /*Post click listener*/
                txt_adapter_post.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (edittxtComment.getText().toString().length() != 0) {
                            if(Utility.isConnected(context)){
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                    Call<CommonOutputPojo> call = apiService.createAnswerComment("create_edit_answer_comments", 0,
                                            Integer.parseInt(getAnswerDataList.get(position).getAnsId()),
                                            org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtComment.getText().toString()), userid);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                if (responseStatus.equals("success")) {

                                                    //Success
                                                    Utility.hideKeyboard(edittxtComment);
                                                    dialog1.dismiss();
                                                    holder.edit_answer_comments.setText("");
                                                    txt_adapter_post.setVisibility(View.GONE);
                                                    Toast.makeText(context, "Comment created successfully", Toast.LENGTH_SHORT).show();

                                                    getAnswerComments(Integer.parseInt(getAnswerDataList.get(position).getAnsId()),holder);

                                                    holder.ll_hide_all_comments.setVisibility(View.VISIBLE);
                                                    holder.ll_view_all_comments.setVisibility(View.GONE);
                                                    holder.progress_ans_comments.setVisibility(View.GONE);
                                                    holder.view_ans_cmts_recylerview.setVisibility(View.VISIBLE);




                                                } else {


                                                }

                                            } else {

                                                Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                            }


                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }

                            }else{

                                Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                            }


                        } else {

                            edittxtComment.setError("Enter your comment");

                        }


                    }
                });


            }
        });

        /*Answer comments text change listener*/

        holder.edit_answer_comments.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {

                /*Post visbiity condition*/
                if (holder.edit_answer_comments.getText().toString().length() != 0) {
                    holder.ans_cmt_adapter_post.setVisibility(View.VISIBLE);
                } else {
                    holder.ans_cmt_adapter_post.setVisibility(View.GONE);
                }


            }
        });

        /*Answer post click listener*/
        holder.ans_cmt_adapter_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (holder.edit_answer_comments.getText().toString().length() != 0) {
                    if(Utility.isConnected(context)){
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiService.createAnswerComment("create_edit_answer_comments", 0,
                                    Integer.parseInt(getAnswerDataList.get(position).getAnsId()),
                                    holder.edit_answer_comments.getText().toString(), userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            //Success

                                            Utility.hideKeyboard(holder.edit_answer_comments);

                                            holder.edit_answer_comments.setText("");
                                            holder.ans_cmt_adapter_post.setVisibility(View.GONE);

                                            Toast.makeText(context, "Comment created successfully", Toast.LENGTH_SHORT).show();

                                            getAnswerComments(Integer.parseInt(getAnswerDataList.get(position).getAnsId()),holder);

                                            holder.ll_hide_all_comments.setVisibility(View.VISIBLE);
                                            holder.ll_view_all_comments.setVisibility(View.GONE);

                                       /* getAnsCmtAdapter = new GetAnsCmtAdapter(context, answerCmtDataList);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                        holder.view_ans_cmts_recylerview.setLayoutManager(mLayoutManager);
                                        holder.view_ans_cmts_recylerview.setItemAnimator(new DefaultItemAnimator());
                                        holder.view_ans_cmts_recylerview.setAdapter(getAnsCmtAdapter);
                                        holder.view_ans_cmts_recylerview.hasFixedSize();
                                        getAnsCmtAdapter.notifyDataSetChanged();*/

                                            holder.view_ans_cmts_recylerview.setVisibility(View.VISIBLE);


                                        /*Intent intent = new Intent(context, ViewQuestionAnswer.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putInt("position", position);
                                        bundle.putInt("quesId", getAnswerDataList.get(position).getQuestId());
                                        intent.putExtras(bundle);
                                        context.startActivity(intent);
                                        ((Activity) context).finish();*/


                                        } else {


                                        }

                                    } else {

                                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                    }


                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                    }else{

                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }



                } else {

                    holder.edit_answer_comments.setError("Enter your comment");

                }


            }
        });




        /* upVote count */

        if (Integer.parseInt(getAnswerDataList.get(position).getTotalUpvote()) != 0) {

            String upVoteCount = "Upvote ("+getAnswerDataList.get(position).getTotalUpvote()+")";
            holder.tv_upVote.setText(upVoteCount);

        }


        // downVote count

        if (Integer.parseInt(getAnswerDataList.get(position).getTotalDownvote() )!= 0) {

            String downVoteCount = "Downvote ("+getAnswerDataList.get(position).getTotalDownvote()+")";
            holder.tv_downVote.setText(downVoteCount);

        }



        /* Answers Detail  Section */

        if (getAnswerDataList.get(position).getUpDownVote() == 1) {

            holder.upVoteBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_upvoted));
            getAnswerDataList.get(position).setUpVoted(true);


        } else if (getAnswerDataList.get(position).getUpDownVote() == 2) {

            holder.downVoteBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_upvoted));
            getAnswerDataList.get(position).setDownVoted(true);

        } else {

            holder.upVoteBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_upvote));
            getAnswerDataList.get(position).setUpVoted(false);

        }



       // Upvote click listener
     /*   holder.upVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getAnswerDataList.get(position).isDownVoted()) {

                    if(Utility.isConnected(context)){

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                                    Integer.parseInt(getAnswerDataList.get(position).getAnsId()), 0, userid);

                            call.enqueue(new Callback<create_answer_votesResponse>() {
                                @Override
                                public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {

                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            //Success

                                            holder.downVoteBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_upvote));
                                            getAnswerDataList.get(position).setDownVoted(false);
                                            upVotedFlag = false;


                                            // downVote count

                                            if (Integer.parseInt(response.body().getData().get(0).getTotalDownvote())!= 0) {

                                                String downVoteCount = "Downvote ("+response.body().getData().get(0).getTotalDownvote()+")";
                                                holder.tv_downVote.setText(downVoteCount);

                                            }


                                        } else if (responseStatus.equals("alreadyvoted")) {

                                            Toast.makeText(context, "Already voted", Toast.LENGTH_SHORT).show();

                                        }

                                    } else {

                                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                    }


                                }

                                @Override
                                public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    }else{

                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }

                }

                if (getAnswerDataList.get(position).isUpVoted()) {

                    if(Utility.isConnected(context)){

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                                    Integer.parseInt(getAnswerDataList.get(position).getAnsId()), 0, userid);

                            call.enqueue(new Callback<create_answer_votesResponse>() {
                                @Override
                                public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {


                                    if (response.body() != null) {


                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            //Success
                                            holder.upVoteBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_upvote));
                                            getAnswerDataList.get(position).setUpVoted(false);
                                            upVotedFlag = false;

                                            int upVote = Integer.parseInt(response.body().getData().get(0).getTotalUpvote());

                                            Log.e("upVote1", "onResponse: "+upVote);

                                            if(upVote == 0){

                                                String upVoteCount = "Upvote";
                                                holder.tv_upVote.setText(upVoteCount);

                                            }else{

                                                String upVoteCount = "Upvote ("+upVote+")";
                                                holder.tv_upVote.setText(upVoteCount);
                                            }



                                        } else if (responseStatus.equals("alreadyvoted")) {

                                            Toast.makeText(context, "Already voted", Toast.LENGTH_SHORT).show();

                                        }

                                    } else {

                                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                    }


                                }

                                @Override
                                public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        } }else{

                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }


                }

                else if (!getAnswerDataList.get(position).isUpVoted()) {

                    if(Utility.isConnected(context)){
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                                    Integer.parseInt(getAnswerDataList.get(position).getAnsId()), 1, userid);

                            call.enqueue(new Callback<create_answer_votesResponse>() {
                                @Override
                                public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {


                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            //Success

                                            holder.upVoteBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_upvoted));
                                            getAnswerDataList.get(position).setUpVoted(true);
                                            upVotedFlag = true;


//                                        int upVote = getAnswerDataList.get(position).getTotalUpvote() - 1;
                                            int upVote = Integer.parseInt(response.body().getData().get(0).getTotalUpvote());

                                            Log.e("upVote1", "onResponse: "+upVote);

                                            if(upVote == 0){

                                                String upVoteCount = "Upvote";
                                                holder.tv_upVote.setText(upVoteCount);

                                            }else{

                                                String upVoteCount = "Upvote ("+upVote+")";
                                                holder.tv_upVote.setText(upVoteCount);
                                            }


                                        } else if (responseStatus.equals("alreadyvoted")) {

                                            Toast.makeText(context, "Already voted", Toast.LENGTH_SHORT).show();

                                        }
                                    } else {

                                        Toast.makeText(context, "Something went wrong please try again", Toast.LENGTH_SHORT).show();

                                    }


                                }

                                @Override
                                public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    }else{

                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }


                }

            }
        });
*/

       /* *//*Down vote click listener*//*

        holder.downVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getAnswerDataList.get(position).isUpVoted()) {
                    if(Utility.isConnected(context)){
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                                    Integer.parseInt(getAnswerDataList.get(position).getAnsId()), 0, userid);

                            call.enqueue(new Callback<create_answer_votesResponse>() {
                                @Override
                                public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {


                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {


                                            //Success
                                            holder.upVoteBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_upvote));
                                            getAnswerDataList.get(position).setUpVoted(false);
                                            upVotedFlag = false;
                                            // downVote count

                                            if (Integer.parseInt(response.body().getData().get(0).getTotalDownvote())!= 0) {

                                                String downVoteCount = "Downvote ("+response.body().getData().get(0).getTotalDownvote()+")";
                                                holder.tv_downVote.setText(downVoteCount);

                                            }

                                        } else if (responseStatus.equals("alreadyvoted")) {

                                            Toast.makeText(context, "Already voted", Toast.LENGTH_SHORT).show();

                                        }

                                    } else {

                                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                    }


                                }

                                @Override
                                public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        } }else{

                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }

                }


                if (getAnswerDataList.get(position).isDownVoted()) {
                    if(Utility.isConnected(context)){
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                                    Integer.parseInt(getAnswerDataList.get(position).getAnsId()), 0, userid);

                            call.enqueue(new Callback<create_answer_votesResponse>() {
                                @Override
                                public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {

                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            //Success
                                            holder.downVoteBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_upvote));
                                            getAnswerDataList.get(position).setDownVoted(false);
                                            downVotedFlag = false;



                                            // downVote count

                                            if (Integer.parseInt(response.body().getData().get(0).getTotalDownvote())!= 0) {

                                                String downVoteCount = "Downvote ("+response.body().getData().get(0).getTotalDownvote()+")";
                                                holder.tv_downVote.setText(downVoteCount);

                                            }

                                        } else if (responseStatus.equals("alreadyvoted")) {

                                            Toast.makeText(context, "Already voted", Toast.LENGTH_SHORT).show();

                                        }

                                    } else {

                                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                    }


                                }

                                @Override
                                public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        } }else{

                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }


                } else if (!getAnswerDataList.get(position).isDownVoted()) {

                    if(Utility.isConnected(context)){
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                                    Integer.parseInt(getAnswerDataList.get(position).getAnsId()), 2, userid);

                            call.enqueue(new Callback<create_answer_votesResponse>() {
                                @Override
                                public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {


                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            //Success

                                            holder.downVoteBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_upvoted));
                                            getAnswerDataList.get(position).setDownVoted(true);
                                            downVotedFlag = true;

                                            // downVote count

                                            if (Integer.parseInt(response.body().getData().get(0).getTotalDownvote() )!= 0) {

                                                String downVoteCount = "Downvote ("+response.body().getData().get(0).getTotalDownvote()+")";
                                                holder.tv_downVote.setText(downVoteCount);

                                            }

                                        } else if (responseStatus.equals("alreadyvoted")) {

                                            Toast.makeText(context, "Already voted", Toast.LENGTH_SHORT).show();

                                        }

                                    } else {

                                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                    }


                                }

                                @Override
                                public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        } }else{

                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }


                }

            }


        });
*/
        /*Post click listener*/
        holder.txt_adapter_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (holder.edittxt_answer.getText().toString().length() != 0) {
                    if(Utility.isConnected(context)){
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<AnswerPojoClass> call = apiService.createAnswer("create_answer",
                                    Integer.parseInt(getAnswerDataList.get(position).getAnsId()),
                                    Integer.parseInt(getAnswerDataList.get(position).getQuestId()),
                                    1, 0,
                                    holder.edittxt_answer.getText().toString(), latitude, longitude, userid);


                            call.enqueue(new Callback<AnswerPojoClass>() {
                                @Override
                                public void onResponse(Call<AnswerPojoClass> call, Response<AnswerPojoClass> response) {


                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            Utility.hideKeyboard(holder.edittxt_answer);
                                            Toast.makeText(context, "Answer edited successfully", Toast.LENGTH_SHORT).show();
                                            //Success
                                            String editedAnswer = holder.edittxt_answer.getText().toString();
                                            holder.userAnswer.setText(editedAnswer);
                                            holder.ll_edit_answer.setVisibility(View.GONE);
                                            holder.userAnswer.setVisibility(View.VISIBLE);
                                        }

                                    } else {

                                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                    }


                                }

                                @Override
                                public void onFailure(Call<AnswerPojoClass> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        } }else{

                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }

                }


            }
        });

        /*Cancel click listener*/
        holder.txt_adapter_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                holder.ll_edit_answer.setVisibility(View.GONE);
                holder.userAnswer.setVisibility(View.VISIBLE);
            }
        });


        /*Answer options click listener*/
        holder.ansOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //creating a popup menu
                PopupMenu popup = new PopupMenu(context, holder.ansOptions);

                //inflating menu from xml resource
                popup.inflate(R.menu.events_comments_options);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.menu0:

                                holder.ll_edit_answer.setVisibility(View.VISIBLE);
                                holder.userAnswer.setVisibility(View.GONE);
                                String userAnswer = holder.userAnswer.getText().toString();
                                holder.edittxt_answer.setText(userAnswer);
                                holder.edittxt_answer.requestFocus();

                                break;

                            case R.id.menu1:


                                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Are you sure to delete this answer?")
                                        .setConfirmText("Yes,delete it!")
                                        .setCancelText("No,cancel!")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                if(Utility.isConnected(context)){
                                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                                    try {

                                                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                                                        int ansId = Integer.parseInt(getAnswerDataList.get(position).getAnsId());
                                                        int quesId = Integer.parseInt(getAnswerDataList.get(position).getQuestId());

                                                        Call<CommonOutputPojo> call = apiService.deleteAnswer("create_delete_answer", ansId, quesId, userid);

                                                        call.enqueue(new Callback<CommonOutputPojo>() {
                                                            @Override
                                                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                                                if (response.body() != null) {

                                                                    String responseStatus = response.body().getResponseMessage();

                                                                    Log.e("deleteEventComment", "responseStatus->" + responseStatus);


                                                                    if (responseStatus.equals("success")) {

                                                                        //Success
                                                                        Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                                                                        getAnswerDataList.remove(position);
                                                                        notifyItemRemoved(position);
                                                                        notifyItemRangeChanged(position, getAnswerDataList.size());
                                                                        viewQuestionAnswer.loadNewData();


                                                                    } else if (responseStatus.equals("nodata")) {


                                                                    }


                                                                } else {

                                                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                                                }


                                                            }

                                                            @Override
                                                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                                //Error
                                                                Log.e("FailureError", "FailureError" + t.getMessage());
                                                            }
                                                        });

                                                    } catch (Exception e) {

                                                        e.printStackTrace();

                                                    } }else{

                                                    Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                                                }


                                            }
                                        })
                                        .show();

                                break;

                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();


            }
        });

        /*View all comments click listener*/
        holder.tv_view_all_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                holder.ll_hide_all_comments.setVisibility(View.VISIBLE);
                holder.progress_ans_comments.setVisibility(View.VISIBLE);
                holder.ll_view_all_comments.setVisibility(View.GONE);
                holder.view_ans_cmts_recylerview.setVisibility(View.VISIBLE);


                if(Utility.isConnected(context)){
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<GetAnswerCmtsOutput> call = apiService.getAnswerCommentsAll("get_answer_comment_all",
                                Integer.parseInt(getAnswerDataList.get(position).getAnsId()),userid);

                        call.enqueue(new Callback<GetAnswerCmtsOutput>() {
                            @Override
                            public void onResponse(Call<GetAnswerCmtsOutput> call, Response<GetAnswerCmtsOutput> response) {

                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {
                                        answerCmtDataList.clear();

                                        //Success

                                        answerCmtDataList = response.body().getData();

                                    /*for (int i = 0; i < response.body().getData().size(); i++) {

                                        int ansId = response.body().getData().get(i).getAnsId();
                                        int questId = response.body().getData().get(i).getQuestId();
                                        String askAnswer = response.body().getData().get(i).getAskAnswer();
                                        int totalComments = response.body().getData().get(i).getTotalComments();
                                        int cmmtAnsId = response.body().getData().get(i).getCmmtAnsId();
                                        String ansComments = response.body().getData().get(i).getAnsComments();
                                        int totalCmmtLikes = response.body().getData().get(i).getTotalCmmtLikes();
                                        int totalCmmtReply = response.body().getData().get(i).getTotalCmmtReply();
                                        int createdBy = response.body().getData().get(i).getCreatedBy();
                                        String createdOn = response.body().getData().get(i).getCreatedOn();
                                        int userId = response.body().getData().get(i).getUserId();
                                        String firstName = response.body().getData().get(i).getFirstName();
                                        String lastName = response.body().getData().get(i).getLastName();
                                        String picture = response.body().getData().get(i).getPicture();
                                        int cmmtLikesId = response.body().getData().get(i).getCmmtLikesId();
                                        int ansCmmtLikes = response.body().getData().get(i).getAnsCmmtLikes();
                                        List<CommentReplyData> reply = response.body().getData().get(i).getReply();

                                        answerCmtData = new AnswerCmtData(ansId, questId, askAnswer, totalComments, cmmtAnsId, ansComments, totalCmmtLikes, totalCmmtReply,
                                                createdBy, createdOn, userId, firstName, lastName, picture, cmmtLikesId, ansCmmtLikes, reply);

                                        answerCmtDataList.add(answerCmtData);

                                    }*/


                                        /*Setting adapter*/
                                        getAnsCmtAdapter = new GetAnsCmtAdapter(context, answerCmtDataList);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                        holder.view_ans_cmts_recylerview.setLayoutManager(mLayoutManager);
                                        holder.view_ans_cmts_recylerview.setItemAnimator(new DefaultItemAnimator());
                                        holder.view_ans_cmts_recylerview.setAdapter(getAnsCmtAdapter);
//                                    holder.view_ans_cmts_recylerview.addItemDecoration(new DividerItemDecoration(holder.view_ans_cmts_recylerview.getContext(), DividerItemDecoration.VERTICAL));
                                        holder.view_ans_cmts_recylerview.hasFixedSize();
                                        getAnsCmtAdapter.notifyDataSetChanged();
                                        holder.progress_ans_comments.setVisibility(View.GONE);

                                    } else {


                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                }


                            }

                            @Override
                            public void onFailure(Call<GetAnswerCmtsOutput> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    } }else{

                    Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                }


            }
        });

        /*Hide all comments click listener*/
        holder.ll_hide_all_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.ll_hide_all_comments.setVisibility(View.GONE);
                holder.ll_view_all_comments.setVisibility(View.VISIBLE);
                holder.view_ans_cmts_recylerview.setVisibility(View.GONE);

            }
        });


        if (answerData != null) {

            String createdon = answerData.getCreatedOn();
            /*Time stamp setting*/
            Utility.setTimeStamp(createdon,holder.commentedTime);

        }


    }

    /*Get answer comments api call */
    private void getAnswerComments(int ansId, final MyViewHolder holder) {


        if(Utility.isConnected(context)){
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Call<GetAnswerCmtsOutput> call = apiService.getAnswerCommentsAll("get_answer_comment_all", ansId, userid);

                call.enqueue(new Callback<GetAnswerCmtsOutput>() {
                    @Override
                    public void onResponse(Call<GetAnswerCmtsOutput> call, Response<GetAnswerCmtsOutput> response) {

                        if (response.body() != null) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                answerCmtDataList.clear();

                                //Success
                                answerCmtDataList = response.body().getData();

                            /*for (int i = 0; i < response.body().getData().size(); i++) {

                                int ansId = response.body().getData().get(i).getAnsId();
                                int questId = response.body().getData().get(i).getQuestId();
                                String askAnswer = response.body().getData().get(i).getAskAnswer();
                                int totalComments = response.body().getData().get(i).getTotalComments();
                                int cmmtAnsId = response.body().getData().get(i).getCmmtAnsId();
                                String ansComments = response.body().getData().get(i).getAnsComments();
                                int totalCmmtLikes = response.body().getData().get(i).getTotalCmmtLikes();
                                int totalCmmtReply = response.body().getData().get(i).getTotalCmmtReply();
                                int createdBy = response.body().getData().get(i).getCreatedBy();
                                String createdOn = response.body().getData().get(i).getCreatedOn();
                                int userId = response.body().getData().get(i).getUserId();
                                String firstName = response.body().getData().get(i).getFirstName();
                                String lastName = response.body().getData().get(i).getLastName();
                                String picture = response.body().getData().get(i).getPicture();
                                int cmmtLikesId = response.body().getData().get(i).getCmmtLikesId();
                                int ansCmmtLikes = response.body().getData().get(i).getAnsCmmtLikes();
                                List<CommentReplyData> reply = response.body().getData().get(i).getReply();

                                answerCmtData = new AnswerCmtData(ansId, questId, askAnswer, totalComments, cmmtAnsId, ansComments, totalCmmtLikes, totalCmmtReply,
                                        createdBy, createdOn, userId, firstName, lastName, picture, cmmtLikesId, ansCmmtLikes, reply);
                                answerCmtDataList.add(answerCmtData);


                            }*/

                                /*Setting Adapter*/
                                getAnsCmtAdapter = new GetAnsCmtAdapter(context, answerCmtDataList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                holder.view_ans_cmts_recylerview.setLayoutManager(mLayoutManager);
                                holder.view_ans_cmts_recylerview.setItemAnimator(new DefaultItemAnimator());
                                holder.view_ans_cmts_recylerview.setAdapter(getAnsCmtAdapter);
                                holder.view_ans_cmts_recylerview.hasFixedSize();
                                getAnsCmtAdapter.notifyDataSetChanged();

                            }

                        } else {

                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }

                    }

                    @Override
                    public void onFailure(Call<GetAnswerCmtsOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "FailureError" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }}else{

            Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }


    }

    @Override
    public int getItemCount() {
        return getAnswerDataList.size();
    }


    public interface onUpvotedownvoteUpdate{

        void onUpvoteDownvote(View view,int adapterposition,View viewitem);
    }
}
