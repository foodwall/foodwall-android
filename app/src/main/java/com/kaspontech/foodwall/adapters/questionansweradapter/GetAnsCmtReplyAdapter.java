package com.kaspontech.foodwall.adapters.questionansweradapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerComments.CommentReplyData;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by BalajiPrabhu on 6/3/2018.
 */

public class GetAnsCmtReplyAdapter extends RecyclerView.Adapter<GetAnsCmtReplyAdapter.MyViewHolder> {

    /**
     * Context
     */
    private Context context;

    /**
     * Comment reply data arraylist
     */
    private List<CommentReplyData> commentReplyDataList = new ArrayList<>();

    /**
     * Comment reply data pojo
     */

    private CommentReplyData commentReplyData;


    public GetAnsCmtReplyAdapter(Context context, List<CommentReplyData> commentReplyDataList) {
        this.context = context;
        this.commentReplyDataList = commentReplyDataList;
    }

    @NonNull
    @Override
    public GetAnsCmtReplyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_ans_cmt_reply, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        commentReplyData = commentReplyDataList.get(position);


        /*Setting user image loading*/
        Utility.picassoImageLoader(commentReplyData.getPicture(),
                0, holder.userImage, context);

        /*User name displaying*/
        holder.userName.setText(commentReplyData.getFirstName() + " " + commentReplyData.getLastName());

        holder.userAnswer.setText(commentReplyData.getAnsCmmtReply());

        /*Self user id*/
        String userid = Pref_storage.getDetail(context, "userId");

        int createdByid = commentReplyDataList.get(position).getCreatedBy();

        /*User name click listener*/
        holder.userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userId = String.valueOf(commentReplyDataList.get(position).getUserId());

                if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", userId);
                    context.startActivity(intent);

                }

            }
        });

        /*User image click listener*/

        holder.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userId = String.valueOf(commentReplyDataList.get(position).getUserId());

                if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", userId);
                    context.startActivity(intent);

                }

            }
        });

        /*Answer option visibilty*/
        if (userid.equals(String.valueOf(createdByid))) {

            holder.llAnswerOption.setVisibility(View.VISIBLE);

        } else {

            holder.llAnswerOption.setVisibility(View.GONE);

        }


        if (commentReplyData != null) {

            String createdon = commentReplyData.getCreatedOn();
            /*Time stamp funcitonality*/
            Utility.setTimeStamp(createdon, holder.commentedTime);


        }
        /*Answer option pop up click listner*/
        holder.ansOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /* creating a popup menu */
                PopupMenu popup = new PopupMenu(context, holder.ansOptions);

                /* inflating menu from xml resource */
                popup.inflate(R.menu.events_comments_options);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.menu0:

                                holder.llEditAnswer.setVisibility(View.VISIBLE);
                                holder.userAnswer.setVisibility(View.GONE);
                                String userAnswer = holder.userAnswer.getText().toString();
                                holder.edittxtAnswer.setText(userAnswer);
                                holder.edittxtAnswer.requestFocus();


                                break;

                            case R.id.menu1:


                                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Are you sure to delete this reply?")
                                        .setConfirmText("Yes,delete it!")
                                        .setCancelText("No,cancel!")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                if (Utility.isConnected(context)) {
                                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                                    try {

                                                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                                                        int ansId = commentReplyDataList.get(position).getAnsId();
                                                        int commentId = commentReplyDataList.get(position).getCmmtAnsId();

                                                        Call<CommonOutputPojo> call = apiService.deleteAnswerCommentReply("create_delete_answer_comments", commentId, ansId, userid);

                                                        call.enqueue(new Callback<CommonOutputPojo>() {
                                                            @Override
                                                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                                                if (response.body() != null) {

                                                                    String responseStatus = response.body().getResponseMessage();

                                                                    Log.e("deleteEventComment", "responseStatus->" + responseStatus);


                                                                    if (responseStatus.equals("success")) {

                                                                        //Success
                                                                        Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                                                                        commentReplyDataList.remove(position);
                                                                        notifyItemRemoved(position);
                                                                        notifyItemRangeChanged(position, commentReplyDataList.size());

                                                                        if (commentReplyDataList.size() == 0) {


                                                                        }


                                                                    } else if (responseStatus.equals("nodata")) {


                                                                    }


                                                                } else {

                                                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                                                }


                                                            }

                                                            @Override
                                                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                                //Error
                                                                Log.e("FailureError", "FailureError" + t.getMessage());
                                                            }
                                                        });

                                                    } catch (Exception e) {

                                                        e.printStackTrace();

                                                    }
                                                } else {
                                                    Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                                                }


                                            }
                                        })
                                        .show();

                                break;

                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();


            }
        });

        /*Post cancel click listener*/
        holder.txtAdapterCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.llEditAnswer.setVisibility(View.GONE);
                holder.userAnswer.setVisibility(View.VISIBLE);

            }
        });

        /*Post click listener*/
        holder.txtAdapterPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (holder.edittxtAnswer.getText().toString().length() != 0) {

                    if (Utility.isConnected(context)) {

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                            int ansId = commentReplyDataList.get(position).getAnsId();
                            int commentId = commentReplyDataList.get(position).getCmmtAnsId();

                            Call<CommonOutputPojo> call = apiService.createAnswerCommentReply("create_edit_answer_comments_reply", commentId, ansId,
                                    holder.edittxtAnswer.getText().toString(), userid);


                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            Utility.hideKeyboard(holder.edittxtAnswer);

                                            Toast.makeText(context, "Reply edited successfully", Toast.LENGTH_SHORT).show();

                                            //Success
                                            String editedAnswer = holder.edittxtAnswer.getText().toString();
                                            holder.userAnswer.setText(editedAnswer);
                                            holder.llEditAnswer.setVisibility(View.GONE);
                                            holder.userAnswer.setVisibility(View.VISIBLE);
                                        }

                                    } else {

                                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                    }


                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }

                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return commentReplyDataList.size();
    }


    /*Widets initialization*/
    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView userImage;
        EditText edittxtAnswer;
        EditText editAnswerComments;
        TextView userName;
        TextView userAnswer;
        TextView commentedTime;
        TextView txtAdapterPost;
        TextView txtAdapterCancel;
        ImageButton ansOptions;
        LinearLayout llAnswerOption;
        LinearLayout llEditAnswer;

        public MyViewHolder(View view) {
            super(view);


            userImage = (CircleImageView) view.findViewById(R.id.user_image);
            userName = (TextView) view.findViewById(R.id.user_name);
            userAnswer = (TextView) view.findViewById(R.id.user_answer);
            txtAdapterPost = (TextView) view.findViewById(R.id.txt_adapter_post);
            txtAdapterCancel = (TextView) view.findViewById(R.id.txt_adapter_cancel);
            commentedTime = (TextView) view.findViewById(R.id.commented_time);
            edittxtAnswer = (EditText) view.findViewById(R.id.edittxt_answer);
            editAnswerComments = (EditText) view.findViewById(R.id.edit_answer_comments);
            ansOptions = (ImageButton) view.findViewById(R.id.ans_options);
            llAnswerOption = (LinearLayout) view.findViewById(R.id.ll_answer_option);
            llEditAnswer = (LinearLayout) view.findViewById(R.id.ll_edit_answer);


        }
    }

}
