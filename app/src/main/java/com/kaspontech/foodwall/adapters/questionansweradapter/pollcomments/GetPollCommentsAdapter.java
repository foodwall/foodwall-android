package com.kaspontech.foodwall.adapters.questionansweradapter.pollcomments;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.PollComments.GetPollCmtReply;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.PollComments.GetPollCommentsData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.PollComments.GetPollCommentsOutput;
import com.kaspontech.foodwall.questionspackage.pollcomments.ViewPollComments;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class GetPollCommentsAdapter extends RecyclerView.Adapter<GetPollCommentsAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     */
    private Context context;

    /**
     * Like flag
     */
    private boolean likeFlag;


    /**
     * Poll comment reply adater
     */
    GetPollCmtReplyAdapter getPollCmtReplyAdapter;

    /**
     * Poll comment arraylist
     */

    private List<GetPollCommentsData> getPollCommentsDataList = new ArrayList<>();
    private List<GetPollCmtReply> getPollCmtReplyList = new ArrayList<>();

    /**
     * View poll comments activity interface
     */
    ViewPollComments viewPollComments;


    public GetPollCommentsAdapter(Context c, List<GetPollCommentsData> getPollCommentsDataList) {
        this.context = c;
        this.getPollCommentsDataList = getPollCommentsDataList;
        viewPollComments = (ViewPollComments) context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_comments, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v, viewPollComments);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        /*Username displaying*/
        String firstname = getPollCommentsDataList.get(position).getFirstName().replace("\"", "");
        String lastname = getPollCommentsDataList.get(position).getLastName().replace("\"", "");

        String username = firstname + " " + lastname;
        holder.txt_username.setText(username);

        holder.txt_username.setTextColor(Color.parseColor("#000000"));

        /*Replies like count*/
        String txtRepliesLike = getPollCommentsDataList.get(position).getTotalCmmtLikes();

        /*Check whether comment is liked or not*/
        String liked = getPollCommentsDataList.get(position).getQuesCmmtLikes();

        /* Likes display user */

        if (Integer.parseInt(liked) == 1) {
            holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
            likeFlag = true;
        } else if (Integer.parseInt(liked) == 0 || liked == null) {
            likeFlag = false;
            holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));

        }

        /* Likes display count display */

        if (Integer.parseInt(txtRepliesLike) == 0 || txtRepliesLike == null) {
            holder.txt_like.setVisibility(GONE);
        } else if (Integer.parseInt(txtRepliesLike) == 1) {

            holder.txt_like.setText(txtRepliesLike.concat(" ").concat("Like"));
        } else if (Integer.parseInt(txtRepliesLike) > 1) {
            holder.txt_like.setText(txtRepliesLike.concat(" ").concat("Likes"));
        }


        String txtRepliesCount = getPollCommentsDataList.get(position).getTotalCmmtReply();

        if (Integer.parseInt(txtRepliesCount) == 0 || txtRepliesCount == null) {
            holder.txt_replies_count.setVisibility(GONE);
            holder.txt_view_all_replies.setVisibility(GONE);
            holder.view_reply.setVisibility(GONE);

        } else {
            holder.txt_replies_count.setText("(".concat(txtRepliesCount).concat(")"));
        }

        /* Edit visibility */
        if (getPollCommentsDataList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
            holder.txt_edit.setVisibility(View.VISIBLE);
        } else {
            holder.txt_edit.setVisibility(View.GONE);
        }


        holder.txt_comments.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getPollCommentsDataList.get(position).getQuesComments().trim()));

        holder.txt_comments.setTextColor(Color.parseColor("#000000"));


        /* Loading image url */


        Utility.picassoImageLoader(getPollCommentsDataList.get(position).getPicture(),
                1,holder.img_comment,context );



        /* Posted date functionality */
        String createdon = getPollCommentsDataList.get(position).getCreatedOn();

        Utility.setTimeStamp(createdon, holder.txt_ago);


        /* Users whom liked display */
        holder.txt_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pref_storage.setDetail(context, "Comment_id", getPollCommentsDataList.get(position).getCmmtQuesId());
            }
        });

        /* like for a comment */
        holder.img_comment_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Start animation*/

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);

                Pref_storage.setDetail(context, "cmnt_like_pressed", getPollCommentsDataList.get(position).getCmmtQuesId());

                Log.e("likeFlag-->", "" + likeFlag);


                if (likeFlag) {

                    if(Utility.isConnected(context)){
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");

                            Call<CommonOutputPojo> call = apiService.createPollCommentsLikes("create_question_comments_likes", Integer.parseInt(getPollCommentsDataList.get(position).getCmmtQuesId()), 0, Integer.parseInt(createdby));
                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    if (response.body().getResponseCode() == 1) {

                                        likeFlag = false;
                                        holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));

                                        int likedecrement = Integer.parseInt(getPollCommentsDataList.get(position).getTotalCmmtLikes());

                                        if (likedecrement == 0) {
                                            holder.txt_like.setVisibility(View.GONE);
                                        } else if (likedecrement == 1) {
                                            holder.txt_like.setVisibility(View.GONE);
                                        } else if (likedecrement < 1) {
                                            holder.txt_like.setText(String.valueOf(--likedecrement).concat(" ").concat("Like"));
                                            holder.txt_like.setVisibility(View.VISIBLE);
                                        }


                                    }

                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }



                } else if (!likeFlag) {

                    if(Utility.isConnected(context)){
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");
                            Call<CommonOutputPojo> call = apiService.createPollCommentsLikes("create_question_comments_likes", Integer.parseInt(getPollCommentsDataList.get(position).getCmmtQuesId()), 1, Integer.parseInt(createdby));
                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    if (response.body().getResponseCode() == 1) {

                                        likeFlag = true;
                                        holder.img_comment_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));

                                        int likedecrement = Integer.parseInt(getPollCommentsDataList.get(position).getTotalCmmtLikes());

                                        if (likedecrement == 1) {
                                            holder.txt_like.setText(String.valueOf(++likedecrement).concat(" ").concat("Like"));
                                            holder.txt_like.setVisibility(View.VISIBLE);
                                        } else if (likedecrement < 1) {
                                            holder.txt_like.setText(String.valueOf(++likedecrement).concat(" ").concat("Like"));
                                            holder.txt_like.setVisibility(View.VISIBLE);

                                        }


                                    }

                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }


                }


            }
        });


        /* View replies */
        holder.txt_view_all_replies.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                holder.prog_reply.setVisibility(View.VISIBLE);
                /* holder.prog_reply.setIndeterminate(true);*/
                getPollCmtReplyList.clear();

                if(Utility.isConnected(context)){
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {
                    String createuserid = Pref_storage.getDetail(context, "userId");

                    Call<GetPollCommentsOutput> call = apiService.getPollCommentsAll("get_question_comments_all", Integer.parseInt(getPollCommentsDataList.get(position).getQuestId()), Integer.parseInt(createuserid));
                    call.enqueue(new Callback<GetPollCommentsOutput>() {
                        @Override
                        public void onResponse(Call<GetPollCommentsOutput> call, Response<GetPollCommentsOutput> response) {


                            if (response.body() != null) {

                                String responseStatus = response.body().getResponseMessage();

                                if (responseStatus.equals("success")) {

                                    holder.prog_reply.setVisibility(View.GONE);

                                    getPollCommentsDataList = response.body().getData();



                                    /*Setting adapter*/

                                    getPollCmtReplyAdapter = new GetPollCmtReplyAdapter(context, getPollCommentsDataList.get(position).getReply());
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                    holder.rv_comments_reply.setVisibility(View.VISIBLE);
                                    holder.rv_comments_reply.setAdapter(getPollCmtReplyAdapter);
                                    holder.rv_comments_reply.setLayoutManager(mLayoutManager);
                                    holder.rv_comments_reply.setHasFixedSize(true);
                                    getPollCmtReplyAdapter.notifyDataSetChanged();
                                    holder.rv_comments_reply.setNestedScrollingEnabled(false);
                                    holder.txt_view_all_replies.setVisibility(View.GONE);
                                    holder.txt_replies_count.setVisibility(View.GONE);
                                    holder.hide_replies.setVisibility(View.VISIBLE);


                                }

                            }


                        }

                        @Override
                        public void onFailure(Call<GetPollCommentsOutput> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

            }

            }


        });


        //hide reply

        holder.hide_replies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getPollCmtReplyList.clear();

                holder.rv_comments_reply.setVisibility(View.GONE);
                holder.hide_replies.setVisibility(View.GONE);
                holder.txt_view_all_replies.setVisibility(View.VISIBLE);
                holder.txt_replies_count.setVisibility(View.VISIBLE);

            }
        });

        //reply for a comment
        holder.txt_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewPollComments.clickedcommentposition(v, Integer.parseInt(getPollCommentsDataList.get(position).getCmmtQuesId()));
                viewPollComments.clicked_username(v, getPollCommentsDataList.get(position).getFirstName(), Integer.parseInt(getPollCommentsDataList.get(position).getCmmtQuesId()));
                viewPollComments.clickedforreply(v, 11);

            }
        });

        //Edit a comment API
        holder.txt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewPollComments.clickedcommentposition_timelineid(v, getPollCommentsDataList.get(position).getQuesComments(), Integer.parseInt(getPollCommentsDataList.get(position).getCmmtQuesId()), Integer.parseInt(getPollCommentsDataList.get(position).getQuestId()));
                viewPollComments.clickedfor_edit(v, 22);


            }
        });


        //Delete a comment API
        holder.delete_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this comment?")
                        .setContentText("Won't be able to recover this comment anymore!")
                        .setConfirmText("Delete")
                        .setCancelText("Cancel")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                if(Utility.isConnected(context)){
                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                    try {

                                        String createuserid = getPollCommentsDataList.get(position).getUserId();
                                        String timeline_commentid = getPollCommentsDataList.get(position).getQuestId();
                                        String commentid = getPollCommentsDataList.get(position).getCmmtQuesId();

                                        Call<CommonOutputPojo> call = apiService.deletePollComments("create_delete_question_comments", Integer.parseInt(commentid), Integer.parseInt(timeline_commentid), Integer.parseInt(createuserid));
                                        call.enqueue(new Callback<CommonOutputPojo>() {
                                            @Override
                                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                if (response.body() != null && response.body().getResponseCode() == 1) {
                                                    /*Postion deletion*/

                                                    removeAt(position);
                                                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                            .setTitleText("Deleted!!")

                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                @Override
                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                    sDialog.dismissWithAnimation();


                                                                }
                                                            })
                                                            .show();


                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                //Error
                                                Log.e("FailureError", "" + t.getMessage());
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                }else {

                                    Toast.makeText(context,context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                                }




                            }
                        })
                        .show();


            }
        });

    }

    /*Postion deletion*/
    private void removeAt(int position) {
        getPollCommentsDataList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getPollCommentsDataList.size());
    }

    @Override
    public int getItemCount() {

        viewPollComments.adaptersize(getPollCommentsDataList.size());

        return getPollCommentsDataList.size();

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
    /*MyViewHolder widgets*/
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view_reply;
        ImageView img_comment;
        ProgressBar prog_reply;
        LinearLayout rl_comment_layout;
        RecyclerView rv_comments_reply;
        ImageButton img_comment_like, delete_comment;

        TextView txt_username, txt_edit,
                txt_like, txt_ago, txt_reply, txt_comments, txt_view_all_replies, hide_replies, txt_replies_count;


        //Interface activity
        ViewPollComments viewPollComments;


        public MyViewHolder(View itemView, ViewPollComments viewPollComments) {
            super(itemView);


            this.viewPollComments = viewPollComments;

            view_reply = itemView.findViewById(R.id.view_reply);

            txt_ago = itemView.findViewById(R.id.txt_ago);
            txt_like = itemView.findViewById(R.id.txt_like);
            txt_edit = itemView.findViewById(R.id.txt_edit);
            txt_reply = itemView.findViewById(R.id.txt_reply);
            hide_replies = itemView.findViewById(R.id.hide_replies);
            txt_username = itemView.findViewById(R.id.txt_username);
            txt_comments = itemView.findViewById(R.id.txt_comments);
            txt_replies_count = itemView.findViewById(R.id.txt_replies_count);
            rl_comment_layout = itemView.findViewById(R.id.rl_comment_layout);
            txt_view_all_replies = itemView.findViewById(R.id.txt_view_all_replies);


            delete_comment = itemView.findViewById(R.id.del_comment);
            img_comment_like = itemView.findViewById(R.id.img_comment_like);


            img_comment = itemView.findViewById(R.id.img_comment);

            prog_reply = itemView.findViewById(R.id.prog_reply);

            rv_comments_reply = itemView.findViewById(R.id.rv_comments_reply);


            txt_reply.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

//            viewPollComments.clickedcommentposition(v, getAdapterPosition());

        }


    }


}

