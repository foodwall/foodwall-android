package com.kaspontech.foodwall.adapters.TimeLine;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Utility;


import java.util.List;

import static com.kaspontech.foodwall.profilePackage.Image_self_activity.timeline_self_list;

//

public class Sliding_image_for_single_self extends PagerAdapter implements View.OnTouchListener {


    //    private ArrayList<Integer> IMAGES;
    private List<String> imageList;
    private LayoutInflater inflater;
    private Context context;

    private PhotoView imageView;
    public Sliding_image_for_single_self(Context context, List<String> IMAGES) {
        this.context = context;
        this.imageList=IMAGES;
        inflater = LayoutInflater.from(context);




    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slide_image_layout, view, false);

        assert imageLayout != null;
        imageView = (PhotoView) imageLayout.findViewById(R.id.image);
 /*       imageView= (ZoomImageView) imageLayout
                .findViewById(R.id.image);*/

        Utility.picassoImageLoader(timeline_self_list.get(position).replace("[","").replace("]",""),
                1,imageView, context);

//        GlideApp.with(context)
////                .load(getimagelist.get(position).replace("[","").replace("]",""))
//                .load(timeline_self_list.get(position).replace("[","").replace("]",""))
//
//                .centerCrop()
//                .into(imageView);


        view.addView(imageLayout, 0);


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
