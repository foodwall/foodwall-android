package com.kaspontech.foodwall.adapters.chat;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

public class Chat_to_adapter extends RecyclerView.ViewHolder implements View.OnClickListener {


    TextView txt_message_time, txt_chat_message,txt_chat_date,txt_group_username;
    RelativeLayout rl_chat_single;
   EmojiconTextView emojicon_text_view;
    ImageView img_user_single;

    private Chat_muliti_view_adapter.onRedirectPageListener onRedirectPageListener;

    private View viewItem;

    public Chat_to_adapter(View itemView) {
        super(itemView);

        txt_message_time = (TextView) itemView.findViewById(R.id.txt_message_time);
        txt_chat_date = (TextView) itemView.findViewById(R.id.txt_chat_date);
        emojicon_text_view = (EmojiconTextView) itemView.findViewById(R.id.emojicon_text_view);
        txt_chat_message = (TextView) itemView.findViewById(R.id.txt_chat_message);
        txt_group_username = (TextView) itemView.findViewById(R.id.txt_group_username);
        img_user_single = (ImageView) itemView.findViewById(R.id.img_user_single);
        rl_chat_single = (RelativeLayout) itemView.findViewById(R.id.id_rl_chat_single);

        emojicon_text_view.setOnClickListener(this);
    }

    public Chat_to_adapter(View itemView, Chat_muliti_view_adapter.onRedirectPageListener onRedirectPageListener) {
        super(itemView);

        this.onRedirectPageListener = onRedirectPageListener;
        this.viewItem = itemView;

        txt_message_time = (TextView) itemView.findViewById(R.id.txt_message_time);
        txt_chat_date = (TextView) itemView.findViewById(R.id.txt_chat_date);
        emojicon_text_view = (EmojiconTextView) itemView.findViewById(R.id.emojicon_text_view);
        txt_chat_message = (TextView) itemView.findViewById(R.id.txt_chat_message);
        txt_group_username = (TextView) itemView.findViewById(R.id.txt_group_username);
        img_user_single = (ImageView) itemView.findViewById(R.id.img_user_single);
        rl_chat_single = (RelativeLayout) itemView.findViewById(R.id.id_rl_chat_single);

        emojicon_text_view.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.emojicon_text_view:

                onRedirectPageListener.onRedirectPage(view,getAdapterPosition(),viewItem);
                break;
        }
    }
}
