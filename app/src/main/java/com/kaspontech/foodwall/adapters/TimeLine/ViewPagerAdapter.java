package com.kaspontech.foodwall.adapters.TimeLine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.github.chrisbanes.photoview.OnViewDragListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.GlideApp;


import java.util.List;


import me.relex.photodraweeview.PhotoDraweeView;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;


public class ViewPagerAdapter extends PagerAdapter {

    List<String> dataModels;

    Context context;
   /* public ViewPagerAdapter(List<String> dataModels) {
        this.dataModels = dataModels;
    }*/

    public ViewPagerAdapter(List<String> dataModel) {
        this.dataModels = dataModel;
    }

    @Override
    public int getCount() {
        return dataModels.size();
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {

        View itemView = LayoutInflater.from(container.getContext()).inflate(R.layout.view_pager_row, container, false);
//        final PhotoDraweeView imageViewCampaign = (PhotoDraweeView) itemView.findViewById(R.id.imageview_campaign);
        final PhotoView imageViewCampaign = (PhotoView) itemView.findViewById(R.id.imageview_campaign);
        TextView textViewCampaign = (TextView) itemView.findViewById(R.id.textview_campaign);

        final String imageUrl = dataModels.get(position);
        imageViewCampaign.setScaleType(ImageView.ScaleType.FIT_CENTER);



        if (imageUrl != null && !imageUrl.isEmpty()) {


//            Utility.picassoImageLoader(imageUrl,
//                    0,imageViewCampaign);

//
            GlideApp.with(container.getContext())
                    .load(imageUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.1f)
                    .into(imageViewCampaign);




            //Fresco
//            Uri uri = Uri.parse(imageUrl);
//            imageViewCampaign.setImageURI(uri);



//
//            PipelineDraweeControllerBuilder controller = Fresco.newDraweeControllerBuilder();
//            controller.setUri(imageUrl);
//            controller.setOldController(imageViewCampaign.getController());
//            controller.setControllerListener(new BaseControllerListener<ImageInfo>() {
//                @Override
//                public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
//                    super.onFinalImageSet(id, imageInfo, animatable);
//                    if (imageInfo == null || imageViewCampaign == null) {
//                        return;
//                    }
//                    imageViewCampaign.update(imageInfo.getWidth(), imageInfo.getHeight());
//                }
//            });
//            imageViewCampaign.setController(controller.build());
//
        }
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


}
