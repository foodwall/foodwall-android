package com.kaspontech.foodwall.adapters.Follow_following;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
//
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

/**
 * Created by vishnukm on 30/3/18.
 */

public class Follower_adapter extends RecyclerView.Adapter<Follower_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;

    private List<Get_followerlist_pojo> gettingfollowerslist = new ArrayList<>();
    private Get_followerlist_pojo getFollowerlistPojo;


    private Pref_storage pref_storage;
    int userStatus = 0;


    public Follower_adapter(Context c, List<Get_followerlist_pojo> gettinglist,int userStatus ) {
        this.context = c;
        this.gettingfollowerslist = gettinglist;
        this.userStatus = userStatus;
    }

    @NonNull
    @Override
    public Follower_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_followerlist, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Follower_adapter.MyViewHolder vh = new Follower_adapter.MyViewHolder(v);


        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Follower_adapter.MyViewHolder holder, final int position) {


        getFollowerlistPojo = gettingfollowerslist.get(position);


        Log.e("followerUserstatus", "onBindViewHolder: "+ userStatus);

        if(userStatus == 1){

            holder.rl_follow.setVisibility(View.GONE);
        }else{

            holder.rl_follow.setBackgroundResource(R.drawable.editext_login_background);
            holder.txt_follow.setTextColor(Color.BLACK);
        }


        holder.txt_username.setText(getFollowerlistPojo.getFirstName());
        holder.txt_username.setTextColor(Color.parseColor("#000000"));

        holder.txt_userlastname.setText(getFollowerlistPojo.getLastName());
        holder.txt_userlastname.setTextColor(Color.parseColor("#000000"));

        String fullname = getFollowerlistPojo.getFirstName().concat(" ").concat(getFollowerlistPojo.getLastName());

        holder.txt_fullname_user.setText(fullname);

        String ownuser_id = Pref_storage.getDetail(context, "userId");

        String positionuser_id = gettingfollowerslist.get(position).getFollowerId();

        if (Integer.parseInt(gettingfollowerslist.get(position).getFollowerId()) == Integer.parseInt(ownuser_id)) {

//            removeatposition(position);

        }

        String profile_status = gettingfollowerslist.get(position).getProfileStatus();
        String friend = gettingfollowerslist.get(position).getFriend();


        //Follower who you following & Private or public account
        if (Integer.parseInt(gettingfollowerslist.get(position).getFriend()) == 0 && Integer.parseInt(gettingfollowerslist.get(position).getProfileStatus()) == 0) {

            holder.txt_follow.setText(R.string.follow);

        } else if (Integer.parseInt(gettingfollowerslist.get(position).getFriend()) == 0 && Integer.parseInt(gettingfollowerslist.get(position).getProfileStatus()) == 1) {

            holder.txt_follow.setText(R.string.requested);

        } else if (Integer.parseInt(gettingfollowerslist.get(position).getFriend()) == 1) {

            holder.txt_follow.setText(R.string.following);
        }

        // user image

        Utility.picassoImageLoader(gettingfollowerslist.get(position).getPicture(),0,holder.img_follower, context);





        holder.img_follower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String user_id = gettingfollowerslist.get(position).getFollowerId();

                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }

            }
        });


        /// Last name click listener

        holder.txt_userlastname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String user_id = gettingfollowerslist.get(position).getFollowerId();

                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }

            }
        });
        /// First name click listener

        holder.txt_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String user_id = gettingfollowerslist.get(position).getFollowerId();

                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }

            }
        });



        //Call API follower

       /* holder.rl_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String positionuser_id = gettingfollowerslist.get(position).getFollowerId();
                Pref_storage.setDetail(context, "ClickeduserID", positionuser_id);

                if (Integer.parseInt(gettingfollowerslist.get(position).getProfileStatus()) == 0) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        String createdby = Pref_storage.getDetail(context, "userId");

                        String going_to_follow_user_id = Pref_storage.getDetail(context, "ClickeduserID");

                        Call<Get_following_output_Pojo> call = apiService.create_follower("create_follower", Integer.parseInt(createdby), Integer.parseInt(going_to_follow_user_id), 1);
                        call.enqueue(new Callback<Get_following_output_Pojo>() {
                            @Override
                            public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {


                                if (response.body().getResponseCode() == 1) {


                                    for (int j = 0; j < response.body().getData().size(); j++) {

                                        String timelineId = response.body().getData().get(j).getFollowing_id();

                                        String createdOn = response.body().getData().get(j).getCreated_on();
                                        String Total_followers = response.body().getData().get(j).getTotal_followers();
                                        String total_followings = response.body().getData().get(j).getTotal_followings();
                                        String userId = response.body().getData().get(j).getUser_id();
                                        String oauthProvider = response.body().getData().get(j).getOauth_provider();
                                        String oauthUid = response.body().getData().get(j).getOauth_uid();
                                        String firstName1 = response.body().getData().get(j).getFirst_name();
                                        String lastName1 = response.body().getData().get(j).getLast_name();
                                        String location = response.body().getData().get(j).getLatitude();
                                        String locationstate = response.body().getData().get(j).getLongitude();
                                        String email1 = response.body().getData().get(j).getEmail();
                                        String contNo = response.body().getData().get(j).getCont_no();
                                        String gender = response.body().getData().get(j).getGender();
                                        String dob = response.body().getData().get(j).getDob();
                                        String following_picture = response.body().getData().get(j).getPicture();
//                                                    String friend = response.body().getData().get(j).getfriend();


                                        holder.txt_follow.setText(R.string.following);
                                                    if(Integer.parseInt(friend)==0&&Integer.parseInt(profile_status)==0 ){
                                                        holder.txt_follow.setText(R.string.follow);
                                                    } else if(Integer.parseInt(friend)==0&&Integer.parseInt(profile_status)==1 ){
                                                        holder.txt_follow.setText(R.string.requested);
                                                    }else if(Integer.parseInt(friend)==1 ){
                                                        holder.txt_follow.setText(R.string.following);
                                                    }
                                    }


                                }

                            }

                            @Override
                            public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (Integer.parseInt(gettingfollowerslist.get(position).getProfileStatus()) == 1) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        String createdby = Pref_storage.getDetail(context, "userId");

                        String going_to_follow_user_id = Pref_storage.getDetail(context, "ClickeduserID");

                        Call<Get_following_output_Pojo> call = apiService.create_follower_request("create_follower_request", Integer.parseInt(createdby), Integer.parseInt(going_to_follow_user_id), 1);
                        call.enqueue(new Callback<Get_following_output_Pojo>() {
                            @Override
                            public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {


                                if (response.body().getResponseCode() == 1) {


                                    for (int j = 0; j < response.body().getData().size(); j++) {


                                        String timelineId = response.body().getData().get(j).getFollowing_id();

                                        String createdOn = response.body().getData().get(j).getCreated_on();
                                        String Total_followers = response.body().getData().get(j).getTotal_followers();
                                        String total_followings = response.body().getData().get(j).getTotal_followings();
                                        String userId = response.body().getData().get(j).getUser_id();
                                        String oauthProvider = response.body().getData().get(j).getOauth_provider();
                                        String oauthUid = response.body().getData().get(j).getOauth_uid();
                                        String firstName1 = response.body().getData().get(j).getFirst_name();
                                        String lastName1 = response.body().getData().get(j).getLast_name();
                                        String location = response.body().getData().get(j).getLatitude();
                                        String locationstate = response.body().getData().get(j).getLongitude();
                                        String email1 = response.body().getData().get(j).getEmail();
                                        String contNo = response.body().getData().get(j).getCont_no();
                                        String gender = response.body().getData().get(j).getGender();
                                        String dob = response.body().getData().get(j).getDob();
                                        String following_picture = response.body().getData().get(j).getPicture();
//                                                    String friend = response.body().getData().get(j).getfriend();

                                        holder.txt_follow.setText(R.string.requested);
                                                    if(Integer.parseInt(friend)==0&&Integer.parseInt(profile_status)==0 ){
                                                        holder.txt_follow.setText(R.string.follow);
                                                    } else if(Integer.parseInt(friend)==0&&Integer.parseInt(profile_status)==1 ){
                                                        holder.txt_follow.setText(R.string.requested);
                                                    }else if(Integer.parseInt(friend)==1 ){
                                                        holder.txt_follow.setText(R.string.following);
                                                    }*
                                    }


                                }


}
                            @Override
                            public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        });*/


    }


    private void removeatposition(int position) {
        gettingfollowerslist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, gettingfollowerslist.size());
    }


    @Override
    public int getItemCount() {

        return gettingfollowerslist.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_username, txt_userlastname, txt_fullname_user, txt_follow, share, caption, captiontag, txt_created_on, txt_adapter_post;
        RelativeLayout rl_follow, rl_likesnew_layout;

        ImageView img_follower;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_userlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txt_fullname_user = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            txt_follow = (TextView) itemView.findViewById(R.id.txt_follow);
            img_follower = (ImageView) itemView.findViewById(R.id.img_follower);
            rl_follow = (RelativeLayout) itemView.findViewById(R.id.rl_follow);


        }


    }


}
