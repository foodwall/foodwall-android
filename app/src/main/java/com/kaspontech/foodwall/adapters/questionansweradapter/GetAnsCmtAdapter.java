package com.kaspontech.foodwall.adapters.questionansweradapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerComments.AnswerCmtData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerComments.CommentReplyData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerComments.GetAnswerCmtsOutput;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by BalajiPrabhu on 6/3/2018.
 */

public class GetAnsCmtAdapter extends RecyclerView.Adapter<GetAnsCmtAdapter.MyViewHolder> {


    Context context;
    List<AnswerCmtData> answerCmtDataList = new ArrayList<>();
    AnswerCmtData answerCmtData;

    GetAnsCmtReplyAdapter getAnsCmtReplyAdapter;


    public GetAnsCmtAdapter(Context context, List<AnswerCmtData> answerCmtDataList) {
        this.context = context;
        this.answerCmtDataList = answerCmtDataList;
    }

    @NonNull
    @Override
    public GetAnsCmtAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_ans_cmt, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final GetAnsCmtAdapter.MyViewHolder holder, final int position) {

        answerCmtData = answerCmtDataList.get(position);


        Utility.picassoImageLoader(answerCmtData.getPicture(),
                0,holder.userImage,context );

//        GlideApp.with(context)
//                .load(answerCmtData.getPicture())
//                .placeholder(R.drawable.ic_add_photo)
//                .into(holder.userImage);


        holder.userName.setText(answerCmtData.getFirstName() + " " + answerCmtData.getLastName());
        holder.userAnswer.setText(answerCmtData.getAnsComments());
        String userid = Pref_storage.getDetail(context, "userId");
        int createdByid = answerCmtDataList.get(position).getCreatedBy();

        holder.userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String user_id = String.valueOf(answerCmtDataList.get(position).getCreatedBy());

                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }

            }
        });


        holder.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_id = String.valueOf(answerCmtDataList.get(position).getCreatedBy());


                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }

            }
        });


        if (userid.equals(String.valueOf(createdByid))) {

            holder.ll_answer_option.setVisibility(View.VISIBLE);

        } else {

            holder.ll_answer_option.setVisibility(View.GONE);

        }

        if (answerCmtDataList.get(position).getAnsCmmtLikes() == 1) {

            answerCmtDataList.get(position).setUpVoted(true);
            holder.tv_upvote.setText("Liked");
            holder.tv_upvote.setTextColor(ContextCompat.getColor(context, R.color.black));


        } else if (answerCmtDataList.get(position).getAnsCmmtLikes() == 0) {

            answerCmtDataList.get(position).setUpVoted(false);
            holder.tv_upvote.setText("Like");
            holder.tv_upvote.setTextColor(ContextCompat.getColor(context, R.color.grey));

        }

        if (answerCmtData != null) {

            String createdon = answerCmtData.getCreatedOn();
            holder.commentedTime.setTextSize(12);

            Utility.setTimeStamp(createdon,holder.commentedTime);
            Log.e("createdon", "creat\t" + createdon);

           /* try {

                long now = System.currentTimeMillis();

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                Date convertedDate = dateFormat.parse(createdon);

                CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                        convertedDate.getTime(),
                        now,

                        DateUtils.FORMAT_ABBREV_RELATIVE);
                if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                    holder.commentedTime.setText(R.string.just_now);
                } else {
                    holder.commentedTime.append(relavetime1 + "\n\n");
                }


            } catch (ParseException e) {
                e.printStackTrace();
            }*/
        }

        if (answerCmtData.getTotalCmmtReply() != 0) {

            holder.ll_view_all_comments.setVisibility(View.VISIBLE);

        } else {


            holder.ll_view_all_comments.setVisibility(View.GONE);


        }


        holder.ll_view_all_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.ll_hide_all_comments.setVisibility(View.VISIBLE);
                holder.progress_ans_comments.setVisibility(View.VISIBLE);
                holder.view_ans_cmts_rpl_recylerview.setVisibility(View.VISIBLE);
                holder.ll_view_all_comments.setVisibility(View.GONE);


                getAnsCmtReply(position, answerCmtDataList.get(position).getAnsId(), holder);


                /*if (answerCmtDataList.get(position).getReply() != null) {


                    getAnsCmtReplyAdapter = new GetAnsCmtReplyAdapter(context, answerCmtDataList.get(position).getReply());
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                    holder.view_ans_cmts_rpl_recylerview.setLayoutManager(mLayoutManager);
                    holder.view_ans_cmts_rpl_recylerview.setItemAnimator(new DefaultItemAnimator());
                    holder.view_ans_cmts_rpl_recylerview.setAdapter(getAnsCmtReplyAdapter);
                    holder.view_ans_cmts_rpl_recylerview.hasFixedSize();
                    getAnsCmtReplyAdapter.notifyDataSetChanged();


                }*/

            }
        });


        holder.ll_hide_all_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.ll_hide_all_comments.setVisibility(View.GONE);
                holder.view_ans_cmts_rpl_recylerview.setVisibility(View.GONE);
                holder.ll_view_all_comments.setVisibility(View.VISIBLE);
            }
        });

        holder.tv_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*holder.ll_ans_reply.setVisibility(View.VISIBLE);
                holder.edit_comments_reply.requestFocus();
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);*/

                final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(context);
                LayoutInflater inflater11 = LayoutInflater.from(context);
                @SuppressLint("InflateParams") final View dialogView1 = inflater11.inflate(R.layout.alert_answer_comment, null);
                dialogBuilder1.setView(dialogView1);
                dialogBuilder1.setTitle("");

                final AlertDialog dialog1 = dialogBuilder1.create();
                dialog1.show();

                final EditText edittxt_comment;
                CircleImageView cmt_user_image, userphoto_comment;
                final TextView user_answer, cmt_user_name, txt_adapter_post;

                edittxt_comment = (EditText) dialogView1.findViewById(R.id.edittxt_comment);
                cmt_user_image = (CircleImageView) dialogView1.findViewById(R.id.cmt_user_image);
                userphoto_comment = (CircleImageView) dialogView1.findViewById(R.id.userphoto_comment);
                user_answer = (TextView) dialogView1.findViewById(R.id.user_answer);
                cmt_user_name = (TextView) dialogView1.findViewById(R.id.cmt_user_name);
                txt_adapter_post = (TextView) dialogView1.findViewById(R.id.txt_adapter_post);


                Utility.picassoImageLoader(answerCmtDataList.get(position).getPicture(),
                        0,cmt_user_image,context );
//
//                GlideApp.with(context)
//                        .load(answerCmtDataList.get(position).getPicture())
//                        .placeholder(R.drawable.ic_add_photo)
//                        .into(cmt_user_image);

                Utility.picassoImageLoader(Pref_storage.getDetail(context, "picture_user_login"),
                        0,userphoto_comment, context);

//                GlideApp.with(context)
//                        .load(Pref_storage.getDetail(context, "picture_user_login"))
//                        .placeholder(R.drawable.ic_add_photo)
//                        .into(userphoto_comment);

                cmt_user_name.setText(answerCmtDataList.get(position).getFirstName() + " " + answerCmtDataList.get(position).getLastName());
                user_answer.setText(answerCmtDataList.get(position).getAnsComments());

                edittxt_comment.setHint(R.string.add_your_reply);
                edittxt_comment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {


                    }

                    @Override
                    public void afterTextChanged(Editable s) {


                        if (edittxt_comment.getText().toString().length() != 0) {
                            txt_adapter_post.setVisibility(View.VISIBLE);
                        } else {
                            txt_adapter_post.setVisibility(View.GONE);
                        }


                    }
                });

                txt_adapter_post.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (edittxt_comment.getText().toString().length() != 0) {

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                Call<CommonOutputPojo> call = apiService.createAnswerCommentReply("create_edit_answer_comments_reply", 0, answerCmtDataList.get(position).getCmmtAnsId(),
                                        edittxt_comment.getText().toString(), userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                        if (response.body() != null) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);
                                            Log.e("answerCmtDataList", "answerCmtDataList->" + answerCmtDataList.get(position).getAnsId());

                                            if (responseStatus.equals("success")) {

                                                //Success

                                                Utility.hideKeyboard(edittxt_comment);
                                                dialog1.dismiss();
                                                holder.view_ans_cmts_rpl_recylerview.setVisibility(View.VISIBLE);
                                                holder.ll_hide_all_comments.setVisibility(View.VISIBLE);
                                                holder.ll_view_all_comments.setVisibility(View.GONE);

                                                edittxt_comment.setText("");
                                                txt_adapter_post.setVisibility(View.GONE);
                                                getAnsCmtReply(position, answerCmtDataList.get(position).getAnsId(), holder);


                                            } else {


                                            }

                                        } else {

                                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                        }


                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                    }
                                });

                            } catch (Exception e) {

                                e.printStackTrace();

                            }


                        } else {

                            edittxt_comment.setError("Enter your reply");

                        }


                    }
                });


            }
        });

        holder.edit_comments_reply.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


                if (holder.edit_comments_reply.getText().toString().length() != 0) {
                    holder.ans_cmt_adapter_post.setVisibility(View.VISIBLE);
                } else {
                    holder.ans_cmt_adapter_post.setVisibility(View.GONE);
                }


            }
        });


        holder.ans_cmt_adapter_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.edit_comments_reply.getText().toString().length() != 0) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<CommonOutputPojo> call = apiService.createAnswerCommentReply("create_edit_answer_comments_reply", 0, answerCmtDataList.get(position).getCmmtAnsId(),
                                holder.edit_comments_reply.getText().toString(), userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        //Success

//                                        Toast.makeText(context, "Reply created successfully", Toast.LENGTH_SHORT).show();

                                        /*Intent intent = new Intent(context, ViewQuestionAnswer.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putInt("position", position);
                                        bundle.putInt("quesId", answerCmtDataList.get(position).getQuestId());
                                        intent.putExtras(bundle);
                                        context.startActivity(intent);
                                        ((Activity) context).finish();*/

//                                        Utility.hideKeyboard(holder.edit_comments_reply);
                                        holder.edit_comments_reply.setText("");
                                        holder.ans_cmt_adapter_post.setVisibility(View.GONE);

                                        getAnsCmtReply(position, answerCmtDataList.get(position).getAnsId(), holder);

                                        holder.ll_hide_all_comments.setVisibility(View.VISIBLE);
                                        holder.ll_view_all_comments.setVisibility(View.GONE);
                                        holder.view_ans_cmts_rpl_recylerview.setVisibility(View.VISIBLE);


                                    } else {


                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                }


                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }


                } else {

                    holder.edit_comments_reply.setError("Enter your reply");

                }


            }
        });


        holder.tv_upvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (answerCmtDataList.get(position).isUpVoted()) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<CommonOutputPojo> call = apiService.createCommentVotes("create_answer_comments_likes", answerCmtDataList.get(position).getCmmtAnsId(), 0, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("nodata")) {

                                        //Success
                                        answerCmtDataList.get(position).setUpVoted(false);
                                        holder.tv_upvote.setText("Like");
                                        holder.tv_upvote.setTextColor(ContextCompat.getColor(context, R.color.grey));


                                    } else {


                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                }


                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }


                } else {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<CommonOutputPojo> call = apiService.createCommentVotes("create_answer_comments_likes", answerCmtDataList.get(position).getCmmtAnsId(), 1, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        //Success
                                        answerCmtDataList.get(position).setUpVoted(true);
                                        holder.tv_upvote.setText("Liked");
                                        holder.tv_upvote.setTextColor(ContextCompat.getColor(context, R.color.black));


                                    } else {


                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                }


                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }


                }


            }
        });


        holder.ansOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //creating a popup menu
                PopupMenu popup = new PopupMenu(context, holder.ansOptions);

                //inflating menu from xml resource
                popup.inflate(R.menu.events_comments_options);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.menu0:

                                holder.ll_edit_answer.setVisibility(View.VISIBLE);
                                holder.userAnswer.setVisibility(View.GONE);
                                String userAnswer = holder.userAnswer.getText().toString();
                                holder.edittxt_answer.setText(userAnswer);
                                holder.edittxt_answer.requestFocus();


                                break;

                            case R.id.menu1:


                                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Are you sure to delete this comment?")
                                        .setConfirmText("Yes,delete it!")
                                        .setCancelText("No,cancel!")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                                try {

                                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                                                    int ansId = answerCmtDataList.get(position).getAnsId();
                                                    int commentId = answerCmtDataList.get(position).getCmmtAnsId();

                                                    Call<CommonOutputPojo> call = apiService.deleteAnswerComment("create_delete_answer_comments", commentId, ansId, userid);

                                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                                        @Override
                                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                                            if (response.body() != null) {

                                                                String responseStatus = response.body().getResponseMessage();

                                                                Log.e("deleteEventComment", "responseStatus->" + responseStatus);


                                                                if (responseStatus.equals("success")) {

                                                                    //Success
                                                                    Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                                                                    answerCmtDataList.remove(position);
                                                                    notifyItemRemoved(position);
                                                                    notifyItemRangeChanged(position, answerCmtDataList.size());


                                                                } else if (responseStatus.equals("nodata")) {


                                                                }


                                                            } else {

                                                                Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                                            }


                                                        }

                                                        @Override
                                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                            //Error
                                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                                        }
                                                    });

                                                } catch (Exception e) {

                                                    e.printStackTrace();

                                                }


                                            }
                                        })
                                        .show();

                                break;

                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();


            }
        });


        holder.txt_adapter_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (holder.edittxt_answer.getText().toString().length() != 0) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                        int ansId = answerCmtDataList.get(position).getAnsId();
                        int commentId = answerCmtDataList.get(position).getCmmtAnsId();

                        Call<CommonOutputPojo> call = apiService.createAnswerComment("create_edit_answer_comments", commentId, ansId,
                                holder.edittxt_answer.getText().toString(), userid);


                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        Utility.hideKeyboard(holder.edittxt_answer);
                                        Toast.makeText(context, "Comment edited successfully", Toast.LENGTH_SHORT).show();
                                        //Success
                                        String editedAnswer = holder.edittxt_answer.getText().toString();
                                        holder.userAnswer.setText(editedAnswer);
                                        holder.ll_edit_answer.setVisibility(View.GONE);
                                        holder.userAnswer.setVisibility(View.VISIBLE);
                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                }


                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }
        });

        holder.txt_adapter_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.ll_edit_answer.setVisibility(View.GONE);
                holder.userAnswer.setVisibility(View.VISIBLE);

            }
        });


    }


    private void getAnsCmtReply(final int position, int ansId, final MyViewHolder holder) {

//        answerCmtDataList.clear();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

            Call<GetAnswerCmtsOutput> call = apiService.getAnswerCommentsAll("get_answer_comment_all", ansId, userid);

            call.enqueue(new Callback<GetAnswerCmtsOutput>() {
                @Override
                public void onResponse(Call<GetAnswerCmtsOutput> call, Response<GetAnswerCmtsOutput> response) {

                    if (response.body() != null) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            List<AnswerCmtData> answerCmtDataLists = new ArrayList<>();


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                int ansId = response.body().getData().get(i).getAnsId();
                                int questId = response.body().getData().get(i).getQuestId();
                                String askAnswer = response.body().getData().get(i).getAskAnswer();
                                int totalComments = response.body().getData().get(i).getTotalComments();
                                int cmmtAnsId = response.body().getData().get(i).getCmmtAnsId();
                                String ansComments = response.body().getData().get(i).getAnsComments();
                                int totalCmmtLikes = response.body().getData().get(i).getTotalCmmtLikes();
                                int totalCmmtReply = response.body().getData().get(i).getTotalCmmtReply();
                                int createdBy = response.body().getData().get(i).getCreatedBy();
                                String createdOn = response.body().getData().get(i).getCreatedOn();
                                int userId = response.body().getData().get(i).getUserId();
                                String firstName = response.body().getData().get(i).getFirstName();
                                String lastName = response.body().getData().get(i).getLastName();
                                String picture = response.body().getData().get(i).getPicture();
                                int cmmtLikesId = response.body().getData().get(i).getCmmtLikesId();
                                int ansCmmtLikes = response.body().getData().get(i).getAnsCmmtLikes();
                                List<CommentReplyData> reply = response.body().getData().get(i).getReply();


                                AnswerCmtData answerCmtData = new AnswerCmtData(ansId, questId, askAnswer, totalComments, cmmtAnsId, ansComments, totalCmmtLikes, totalCmmtReply,
                                        createdBy, createdOn, userId, firstName, lastName, picture, cmmtLikesId, ansCmmtLikes, reply);
                                answerCmtDataLists.add(answerCmtData);


                            }


                            if (answerCmtDataLists.get(position).getReply() != null) {

                                getAnsCmtReplyAdapter = new GetAnsCmtReplyAdapter(context, answerCmtDataLists.get(position).getReply());
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                holder.view_ans_cmts_rpl_recylerview.setLayoutManager(mLayoutManager);
                                holder.view_ans_cmts_rpl_recylerview.setItemAnimator(new DefaultItemAnimator());
                                holder.view_ans_cmts_rpl_recylerview.setAdapter(getAnsCmtReplyAdapter);
                                holder.view_ans_cmts_rpl_recylerview.hasFixedSize();
                                getAnsCmtReplyAdapter.notifyDataSetChanged();


                            }

                            holder.progress_ans_comments.setVisibility(View.GONE);


                        }

                    } else {

                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                    }

                }

                @Override
                public void onFailure(Call<GetAnswerCmtsOutput> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    @Override
    public int getItemCount() {
        return answerCmtDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progress_ans_comments;
        View bottom_line;
        CircleImageView userImage, ans_cmt_user_image;
        EditText edittxt_answer, edit_comments_reply;
        RecyclerView view_ans_cmts_rpl_recylerview;
        TextView userName, userAnswer, commentedTime, txt_adapter_post, txt_adapter_cancel, ans_cmt_adapter_post,
                ans_cmt_user_name, ans_cmt, tv_view_all_comments, tv_reply, tv_downvote, tv_upvote;
        ImageButton ansOptions;
        LinearLayout upVote, downVote, ll_answer_option, ll_edit_answer, ll_answer_comments, ll_hide_all_comments,
                ll_commented_profile, ll_view_all_comments, ll_ans_reply;

        public MyViewHolder(View view) {
            super(view);


            bottom_line = (View) view.findViewById(R.id.bottom_line);
            progress_ans_comments = (ProgressBar) view.findViewById(R.id.progress_ans_comments);
            userImage = (CircleImageView) view.findViewById(R.id.user_image);
            userName = (TextView) view.findViewById(R.id.user_name);
            userAnswer = (TextView) view.findViewById(R.id.user_answer);
            txt_adapter_post = (TextView) view.findViewById(R.id.txt_adapter_post);
            ans_cmt_adapter_post = (TextView) view.findViewById(R.id.ans_cmt_adapter_post);
            txt_adapter_cancel = (TextView) view.findViewById(R.id.txt_adapter_cancel);
            commentedTime = (TextView) view.findViewById(R.id.commented_time);
            ans_cmt_user_name = (TextView) view.findViewById(R.id.ans_cmt_user_name);
            ans_cmt = (TextView) view.findViewById(R.id.ans_cmt);
            tv_view_all_comments = (TextView) view.findViewById(R.id.tv_view_all_comments);
            tv_reply = (TextView) view.findViewById(R.id.tv_reply);
            tv_upvote = (TextView) view.findViewById(R.id.tv_upvote);
            tv_downvote = (TextView) view.findViewById(R.id.tv_downvote);
            edittxt_answer = (EditText) view.findViewById(R.id.edittxt_answer);
            edit_comments_reply = (EditText) view.findViewById(R.id.edit_comments_reply);
            ansOptions = (ImageButton) view.findViewById(R.id.ans_options);
            upVote = (LinearLayout) view.findViewById(R.id.ll_upvote);
            downVote = (LinearLayout) view.findViewById(R.id.ll_downvote);
            ll_answer_option = (LinearLayout) view.findViewById(R.id.ll_answer_option);
            ll_edit_answer = (LinearLayout) view.findViewById(R.id.ll_edit_answer);
            ll_answer_comments = (LinearLayout) view.findViewById(R.id.ll_answer_comments);
            ll_commented_profile = (LinearLayout) view.findViewById(R.id.ll_commented_profile);
            ll_view_all_comments = (LinearLayout) view.findViewById(R.id.ll_view_all_comments);
            ll_ans_reply = (LinearLayout) view.findViewById(R.id.ll_ans_reply);
            ll_hide_all_comments = (LinearLayout) view.findViewById(R.id.ll_hide_all_comments);
            view_ans_cmts_rpl_recylerview = (RecyclerView) view.findViewById(R.id.view_ans_cmts_rpl_recylerview);


        }
    }
}
