package com.kaspontech.foodwall.adapters.chat;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.chatpackage.Chat_followers_list_group;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.GlideApp;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//

public class Chat_follower_adapter extends RecyclerView.Adapter<Chat_follower_adapter.MyViewHolder> implements Filterable {

    /**
     * Context
     **/
    private Context context;
    /**
     * Followers array list
     **/
    private List<Get_followerlist_pojo> getFollowerlistPojoArrayList = new ArrayList<>();
    private List<Get_followerlist_pojo> getFollowerlistPojoArrayListFiltered = new ArrayList<>();
    /**
     * Followers pojo
     **/
    private Get_followerlist_pojo getFollowerlistPojo;

    /**
     * Friend list
     **/
    public static List<Integer> friendsList_chat = new ArrayList<>();

    /**
     * Selected follower list
     **/
    HashMap<String, Integer> selectedfollowerlistId = new HashMap<>();

    /**
     * Chat followers list group pojo
     **/
    Chat_followers_list_group chatFollowersListGroup;


    public Chat_follower_adapter(Context c, List<Get_followerlist_pojo> gettinglist, HashMap<String, Integer> grpfollowerId) {
        this.context = c;
        this.getFollowerlistPojoArrayList = gettinglist;
        this.selectedfollowerlistId = grpfollowerId;
        this.chatFollowersListGroup=((Chat_followers_list_group)context);
    }

    @NonNull
    @Override
    public Chat_follower_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_chat_followerlist, parent, false);
        // set the view's size, margins, paddings and layout parameters

        return new MyViewHolder(v,chatFollowersListGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull final Chat_follower_adapter.MyViewHolder holder, final int position) {


        getFollowerlistPojo = getFollowerlistPojoArrayList.get(position);

        /*User name displaying*/
        holder.txtUsername.setText(getFollowerlistPojoArrayList.get(position).getFirstName());

        holder.txtUsername.setTextColor(Color.parseColor("#000000"));

        holder.txtUserlastname.setText(getFollowerlistPojoArrayList.get(position).getLastName());
        holder.txtUserlastname.setTextColor(Color.parseColor("#000000"));

        String fullname= getFollowerlistPojoArrayList.get(position).getFirstName().concat(" ").
                concat(getFollowerlistPojoArrayList.get(position).getLastName());

        holder.txtFullnameUser.setText(fullname);
        holder.txtFollowing.setText(R.string.select);

        /*User image displaying*/

        GlideApp.with(context)
                .load(getFollowerlistPojoArrayList.get(position).getPicture()).centerInside()
                .placeholder(R.drawable.ic_add_photo)
                .into(holder.imgFollowing);

/*
holder.rlFollowerAyout.setOnLongClickListener(new View.OnLongClickListener() {
    @Override
    public boolean onLongClick(View v) {
        String friendid= getFollowerlistPojoArrayList.get(position).getFollower_id();
        String username= getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                concat(getFollowerlistPojoArrayList.get(position).getLast_name());

        friendsList_chat.add(Integer.valueOf(friendid));

        Log.e("friendsList-->",""+friendsList_chat);

        holder.imgCheckedFollower.setVisibility(View.VISIBLE);

        return false;
    }
});
*/



/*holder.rlFollowerAyout.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        String friendid= getFollowerlistPojoArrayList.get(position).getFollower_id();
        String username= getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                concat(getFollowerlistPojoArrayList.get(position).getLast_name());

        friendsList_chat.add(Integer.valueOf(friendid));

        Log.e("friendsList-->",""+friendsList_chat);

        holder.imgCheckedFollower.setVisibility(View.VISIBLE);
    }
});*/

      /*  holder.rlFollowerAyout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String friendid= getFollowerlistPojoArrayList.get(position).getFollower_id();
                String username= getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                        concat(getFollowerlistPojoArrayList.get(position).getLast_name());

                friendsList_chat.add(Integer.valueOf(friendid));

                Log.e("friendsList-->",""+friendsList_chat);

                holder.imgCheckedFollower.setVisibility(View.VISIBLE);


                *//*Intent intent = new Intent(context, Create_new_chat_single.class);
                intent.putExtra("friendid",getFollowerlistPojoArrayList.get(position).getFollower_id());
                intent.putExtra("picture",getFollowerlistPojoArrayList.get(position).getPicture());
                intent.putExtra("username",getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                        concat(getFollowerlistPojoArrayList.get(position).getLast_name()));
                context.startActivity(intent);
                ((AppCompatActivity)context).finish();*//*

            }
        });*/





    }






    @Override
    public int getItemCount() {
        if(getFollowerlistPojoArrayList.isEmpty()){

//            chatFollowersListGroup.sizecheck(getFollowerlistPojoArrayList.size());

        }else{

//            chatFollowersListGroup.sizecheck(getFollowerlistPojoArrayList.size());

        }
        return getFollowerlistPojoArrayList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                Log.e("ChathistoryLog", "charString-->"+charString);


                if (charString.isEmpty()) {

                    getFollowerlistPojoArrayListFiltered = getFollowerlistPojoArrayList;
                } else {

                    ArrayList<Get_followerlist_pojo> filteredList = new ArrayList<>();

                    for (Get_followerlist_pojo followerlistPojo : getFollowerlistPojoArrayList) {


                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (followerlistPojo.getFirstName().toLowerCase().contains(charString.toLowerCase()) || followerlistPojo.getLastName().toLowerCase().contains(charSequence)) {
                            filteredList.add(followerlistPojo);


                        } else {
                            Log.e("ChathistoryLog", "performFiltering: Not matched");
                        }
                    }

                    getFollowerlistPojoArrayList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = getFollowerlistPojoArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                getFollowerlistPojoArrayList = (ArrayList<Get_followerlist_pojo>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtUsername, txtUserlastname, txtFullnameUser, txtFollowing;

        RelativeLayout rl_following, rlayoutFollowing, rlFollowerLay, rlFollowerAyout;

        ImageView imgFollowing, imgCheckedFollower;

        CheckBox checkFollower;

        Chat_followers_list_group chatFollowersListGroup;



        public MyViewHolder(View itemView, Chat_followers_list_group chat_followers_list_group) {
            super(itemView);
            this.chatFollowersListGroup = chat_followers_list_group;
            txtUsername = (TextView) itemView.findViewById(R.id.txt_username);
            txtUserlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txtFullnameUser = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            txtFollowing = (TextView) itemView.findViewById(R.id.txt_following);
            imgFollowing = (ImageView) itemView.findViewById(R.id.img_following);
            checkFollower = (CheckBox) itemView.findViewById(R.id.check_follower);
            checkFollower.setOnClickListener(this);

            imgCheckedFollower = (ImageView) itemView.findViewById(R.id.img_checked_follower);

            rlFollowerLay = (RelativeLayout) itemView.findViewById(R.id.rl_follower_lay);
            rlayoutFollowing = (RelativeLayout) itemView.findViewById(R.id.rlayout_following);
            rlFollowerAyout = (RelativeLayout) itemView.findViewById(R.id.rl_follower_ayout);


        }


        @Override
        public void onClick(View v) {
            chatFollowersListGroup.getgrpmemberList(v,getAdapterPosition());
        }
    }




}
