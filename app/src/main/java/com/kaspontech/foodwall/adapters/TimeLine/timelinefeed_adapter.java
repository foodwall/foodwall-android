package com.kaspontech.foodwall.adapters.TimeLine;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Comments_activity;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Edit_newsfeed_post;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.likesActivity;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.CreateEditTimeline_Comments;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_profile_details_Pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Image;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.New_Getting_timeline_all_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.New_timeline_image_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.comments_2;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;
import com.kyleduo.blurpopupwindow.library.BlurPopupWindow;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


/**
 * Created by vishnukm on 8/3/18.
 */

public class timelinefeed_adapter extends RecyclerView.Adapter<timelinefeed_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener, View.OnTouchListener {


    private Context context;


    public static ArrayList<New_Getting_timeline_all_pojo> gettingtimelinealllist = new ArrayList<>();
    public static ArrayList<New_timeline_image_pojo> gettingtimelineimagelist = new ArrayList<>();

    public static List<String> getimagelist = new ArrayList<>();


    private ArrayList<String> ImagesArray = new ArrayList<String>();
    private ArrayList<Timeline_image_only_pojo> ImagesArray_pojo = new ArrayList<>();

    private New_Getting_timeline_all_pojo gettimelineAllModel;
    private New_timeline_image_pojo newTimelineImagePojo;

    private Get_following_profile_pojo getFollowingProfilePojo;

    private Image imagePojo;
    SweetAlertDialog sweetAlertDialog;
    private static String timelineid, commenttimelineid, liketimelineid;
    private final String[] itemsself = {"Delete Post", "Report", "Copy Link", "Turn Off Post Notification", "Share Via WhatsApp"};
    private final String[] itemsothers = {"Report", "Copy Link", "Turn Off Post Notification", "Share Via WhatsApp"};
    public ScaleGestureDetector scaleGestureDetector;
    Get_profile_details_Pojo getProfileDetailsPojo;
    BlurPopupWindow blurPopupWindow;
//    public static ImageView img_view_more;

    int status = 0;
    private String userfirstname, userlastname;
    public static double latituderesult;
    public static double longituderesult;

    public static String latituderesultuser;
    public static String longituderesultuser;

    public static String alllikes;
    public static String createcomment, user_id;

    GPSTracker gpsTracker;
//     private TextView txt_timeline_follow;
//     private ImageView img_follow_timeline;

    private static ViewPager mviewpager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;


    private CircleIndicator circleIndicator;

    private boolean like_flag = false;

    private String firstName1, lastName1, password, email1, gender, dob, picture, total_post, following, followers;

    public timelinefeed_adapter(Context c, ArrayList<New_Getting_timeline_all_pojo> gettinglist, ArrayList<String> gettingimage) {
        this.context = c;
        gettingtimelinealllist = gettinglist;
        getimagelist = gettingimage;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_newsfeed_story, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v);
        gpsTracker = new GPSTracker(context);

        latituderesult = gpsTracker.getLatitude();
        longituderesult = gpsTracker.getLongitude();

        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        gettimelineAllModel = gettingtimelinealllist.get(position);

        String urlProtocal = "http://bravo898.startdedicated.com/food_wall/assets/timeline/";


        try {


//Initiate multi image display

//            init();
//            init_2();


            Utility.picassoImageLoader(gettingtimelinealllist.get(position).getPicture(),
                    1,holder.userphoto_profile,context );

//            GlideApp.with(context)
//                    .load(gettingtimelinealllist.get(position).getPicture())
//                    .centerCrop()
//                    .into(holder.userphoto_profile);



            Utility.picassoImageLoader(gettingtimelinealllist.get(position).getPicture(),
                    1,holder.userphoto_comment, context);

//            GlideApp.with(context)
//                    .load(gettingtimelinealllist.get(position).getPicture())
//                    .centerCrop()
//                    .into(holder.userphoto_comment);
        } catch (Exception e) {
            e.printStackTrace();
        }


//Follow, following visibility


        if (Integer.parseInt(gettingtimelinealllist.get(position).getFollowingId()) == 0) {
            holder.txt_timeline_follow.setVisibility(VISIBLE);
            holder.img_follow_timeline.setVisibility(VISIBLE);
        } else if (Integer.parseInt(gettingtimelinealllist.get(position).getFollowingId()) != 0) {
            holder.txt_timeline_follow.setVisibility(GONE);
            holder.img_follow_timeline.setVisibility(GONE);
        }


        if (Pref_storage.getDetail(context, "userId").equals(gettingtimelinealllist.get(position).getUserId())) {
            holder.txt_timeline_follow.setVisibility(GONE);
            holder.img_follow_timeline.setVisibility(GONE);

        }


//Following


       /* if (gettingtimelinealllist.get(position).getPostType()==4){
            String following_id= gettingtimelinealllist.get(position).getFollowingId();

        }*/








//User caption visiblity
        try {
            String selfuserfirstnamecomment = gettingtimelinealllist.get(position).getFirstName().replace("\"", "");
            String selfuserlastnamecomment = gettingtimelinealllist.get(position).getLastName().replace("\"", "");

            if (gettingtimelinealllist.get(position).getFirstName().isEmpty()) {
                holder.selfusername_caption.setVisibility(GONE);
                holder.selfuserlastname_caption.setVisibility(GONE);

            } else {
                holder.selfusername_caption.setText(selfuserfirstnamecomment);
                holder.selfuserlastname_caption.setText(selfuserlastnamecomment);
                holder.selfusername_caption.setVisibility(VISIBLE);
                holder.selfuserlastname_caption.setVisibility(VISIBLE);

                holder.caption.setText(gettingtimelinealllist.get(position).getTimelineDescription());
                holder.caption.setTextColor(Color.parseColor("#000000"));
                holder.caption.setVisibility(VISIBLE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

//Total followers visibility
        String post_count = gettingtimelinealllist.get(position).getTotalPosts();

        if (Integer.parseInt(post_count) == 0) {
            holder.count_post.setVisibility(GONE);
            holder.count.setVisibility(GONE);
            holder.comma.setVisibility(GONE);
        } else if (Integer.parseInt(post_count) == 1) {
            holder.count.setText(post_count);
            holder.count.setVisibility(VISIBLE);
            holder.count_post.setText(R.string.post);
            holder.count_post.setVisibility(VISIBLE);

        } else {
            holder.count.setText(post_count);
            holder.count.setVisibility(VISIBLE);
            holder.comma.setVisibility(VISIBLE);
            holder.count_post.setVisibility(VISIBLE);
        }

//Total followers visibility

        String followers_post = gettingtimelinealllist.get(position).getTotalFollowers();
        if (Integer.parseInt(followers_post) == 0) {
            holder.txt_followers.setVisibility(GONE);
            holder.comma.setVisibility(GONE);
            holder.txt_followers_count.setVisibility(GONE);
        } else if (Integer.parseInt(followers_post) == 1) {
            holder.txt_followers_count.setText(followers_post);
            holder.txt_followers.setText(R.string.follwer);
            holder.txt_followers.setVisibility(VISIBLE);
            holder.comma.setVisibility(VISIBLE);
            holder.txt_followers_count.setVisibility(VISIBLE);
        } else if (Integer.parseInt(followers_post) > 1) {
            holder.txt_followers_count.setText(followers_post);
            holder.txt_followers.setVisibility(VISIBLE);
            holder.comma.setVisibility(VISIBLE);
            holder.txt_followers_count.setVisibility(VISIBLE);
        }

//Comments visibility
        try {
            String comment = gettingtimelinealllist.get(position).getTotalComments();
            if (Integer.parseInt(comment) == 0 || comment == null) {

                holder.viewcomment.setVisibility(GONE);
                holder.commenttest.setVisibility(GONE);
            } else {
                holder.comment.setText(comment);
                if (Integer.parseInt(comment) == 1) {
                    holder.viewcomment.setText(R.string.view);
                    holder.commenttest.setText(R.string.commenttt);
                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setVisibility(VISIBLE);
                } else if (Integer.parseInt(comment) > 1) {
                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setVisibility(VISIBLE);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//Likes Visibility
        String likes = String.valueOf(gettingtimelinealllist.get(position).getTotalLikes());
        if (Integer.parseInt(likes) == 0 || likes == null) {
            holder.like.setVisibility(GONE);
            holder.liketxt.setVisibility(GONE);

        } else if (Integer.parseInt(likes) == 1) {
            holder.like.setText(likes);
            holder.liketxt.setText(R.string.new_like);
            holder.like.setVisibility(VISIBLE);
            holder.liketxt.setVisibility(VISIBLE);

        } else {
            holder.like.setText(likes);
            holder.like.setVisibility(VISIBLE);
            holder.liketxt.setVisibility(VISIBLE);


        }

//Displaying user name
        try {
            userfirstname = gettingtimelinealllist.get(position).getFirstName().replace("\"", "");
            userlastname = gettingtimelinealllist.get(position).getLastName().replace("\"", "");

            holder.username.setText(userfirstname);
            holder.username.setTextColor(Color.parseColor("#000000"));
            holder.userlastname.setText(userlastname);
            holder.userlastname.setTextColor(Color.parseColor("#000000"));
        } catch (Exception e) {
            e.printStackTrace();
        }


//Likes display by user API

        String self_liked = gettingtimelinealllist.get(position).getTlLikes();

        if (self_liked.equals("1")) {
//            holder.img_like.setChecked(true);
        } else if (self_liked == null) {
//            holder.img_like.setChecked(false);
        }



 /*       ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            String createdby = Pref_storage.getDetail(context, "userId");
            Call<Likes_2_pojo> call = apiService.get_timeline_likes_all("get_timeline_likes_all",  Integer.parseInt(gettingtimelinealllist.get(position).getTimelineId()));
            call.enqueue(new Callback<Likes_2_pojo>() {
                @Override
                public void onResponse(Call<Likes_2_pojo> call, Response<Likes_2_pojo> response) {


                    if (response.body().getResponseCode() == 1) {

                        for (int i=0;i< response.body().getData().size();i++){
                             String userid= response.body().getData().get(i).getUser_id();

                           if(Pref_storage.getDetail(context, "userId").equals(userid)){
                               holder.img_like.setChecked(true);

                           }

                        }



                    }

                }

                @Override
                public void onFailure(Call<Likes_2_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }*/




/*

        try {
            String latitude = gettimelineAllModel.getLatitude();



            double newlati = Double.parseDouble(latitude);

            String longitude = gettimelineAllModel.getLongitude();
            double newlongi = Double.parseDouble(longitude);

//            AddressFinder(newlati,newlongi);

//            holder.location.setText(latituderesultuser);
            holder.location.setVisibility(GONE);
//            holder.location_state.setText(longituderesultuser);
            holder.location_state.setVisibility(GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
*/


// Timestamp for timeline
        String createdon = gettingtimelinealllist.get(position).getCreatedOn();

        try {
            long now = System.currentTimeMillis();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date convertedDate = dateFormat.parse(createdon);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now,

                    DateUtils.FORMAT_ABBREV_RELATIVE);
            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                holder.txt_created_on.setText(R.string.just_now);
            } else {
                holder.txt_created_on.append(relavetime1 + "\n\n");
                System.out.println(relavetime1);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

//Share Intent
        holder.img_shareadapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                context.startActivity(Intent.createChooser(i, "Share via"));

            }
        });

//Comments activity intent
        holder.rl_comment_adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pref_storage.setDetail(context, "comment_timelineid", gettingtimelinealllist.get(position).getTimelineId());

                context.startActivity(new Intent(context, Comments_activity.class));

            }
        });

     /*   //test like

        holder.img_like_new.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!holder.img_like_new.isChecked()) {
                    holder.img_like_new.setButtonDrawable(R.drawable.ic_favorite_red_24dp);
                    Toast.makeText(context, "Liked", Toast.LENGTH_SHORT).show();
                } else if (holder.img_like_new.isChecked()) {
                    holder.img_like_new.setButtonDrawable(R.drawable.ic_favorite_border_redlike_24dp);
                    Toast.makeText(context, "Disliked", Toast.LENGTH_SHORT).show();

                }
            }
        });*/

  /*      holder.rl_newlike_adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!holder.img_like_new.isChecked()){

                    holder.like_imagefill.setVisibility(VISIBLE);
                    holder.like_imagefill.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                    Toast.makeText(context, "Liked", Toast.LENGTH_SHORT).show();
                }else if (holder.img_like_new.isChecked()){
                    holder.like_image_empty.setVisibility(VISIBLE);
                    holder.like_image_empty.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                    Toast.makeText(context, "Disliked", Toast.LENGTH_SHORT).show();

                }

            }
        });
*/


//User Like API


        holder.like_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String liketimelineid = gettingtimelinealllist.get(position).getTimelineId();
                Pref_storage.setDetail(context, "clickedlike_timelineid", liketimelineid);


                /*if (holder.img_like.isChecked()) {

                    Integer totallike = gettingtimelinealllist.get(position).getTotalLikes();

                   *//* if (Integer.parseInt(totallike) == 0 || totallike == null) {
                        holder.like.setText("1");
                        holder.like.setVisibility(VISIBLE);
                        holder.liketxt.setVisibility(VISIBLE);

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");
                            String clickedlike_timelineid = Pref_storage.getDetail(context, "clickedlike_timelineid");
                            Call<CreateUserPojoOutput> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(clickedlike_timelineid), Integer.parseInt(createdby), 0);
                            call.enqueue(new Callback<CreateUserPojoOutput>() {
                                @Override
                                public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {


                                    if (response.body().getResponseMessage().contains("nodata")) {

                                        holder.img_like.setChecked(false);
                                        like_flag = false;
                                    }

                                }

                                @Override
                                public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else*//*


                    if (totallike >= 1) {
                        int likeincre = (totallike - 1);

                        if (likeincre == 0) {
                            holder.like.setVisibility(GONE);
                            holder.liketxt.setVisibility(GONE);
                        } else if (likeincre == 1) {
                            holder.like.setText(String.valueOf(likeincre));
//                            String liketxt = holder.like.getText().toString();
//                            holder.like.setText(liketxt);
                            holder.liketxt.setText(R.string.new_like);
                            holder.like.setVisibility(VISIBLE);
                            holder.liketxt.setVisibility(VISIBLE);
                        } else {
                            holder.like.setText(String.valueOf(likeincre));
//
//                            String liketxt = holder.like.getText().toString();
//                            holder.like.setText(liketxt);
                            holder.like.setVisibility(VISIBLE);
                            holder.liketxt.setVisibility(VISIBLE);
                        }


                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");
                            String clickedlike_timelineid = Pref_storage.getDetail(context, "clickedlike_timelineid");
                            Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(clickedlike_timelineid), Integer.parseInt(createdby), 0);
                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    if (response.body().getResponseCode() == 1) {
                                        holder.img_like.setChecked(false);
                                        like_flag = false;

                                    }

                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } else if (!holder.img_like.isChecked()) {

                    Integer totallike = gettingtimelinealllist.get(position).getTotalLikes();
                    if (totallike == 0 || totallike == null) {

                        *//*   holder.like.setText(String.valueOf(totallike));*//*
                        holder.like.setText("1");
                        holder.liketxt.setText(R.string.new_like);

                        holder.like.setVisibility(VISIBLE);
                        holder.liketxt.setVisibility(VISIBLE);

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");
                            String clickedlike_timelineid = Pref_storage.getDetail(context, "clickedlike_timelineid");
                            Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(clickedlike_timelineid), Integer.parseInt(createdby), 1);
                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                    if (response.body().getResponseCode() == 1) {

                                        holder.img_like.setChecked(true);
                                        like_flag = true;
                                    }

                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else if (totallike >= 1) {
                        int likeincre = (totallike + 1);
                        holder.like.setText(String.valueOf(likeincre));
//                        String liketxt = holder.like.getText().toString();
//                        holder.like.setText(liketxt);
                        holder.like.setVisibility(VISIBLE);
                        holder.liketxt.setText(R.string.likes);
                        holder.liketxt.setVisibility(VISIBLE);

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");
                            String clickedlike_timelineid = Pref_storage.getDetail(context, "clickedlike_timelineid");
                            Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(clickedlike_timelineid), Integer.parseInt(createdby), 1);
                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                    if (response.body().getResponseCode() == 1) {

                                        holder.img_like.setChecked(true);
                                        like_flag = true;
                                    }

                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }


                }
*/



             /*    if(gettingtimelinealllist.get(position).isGetisuserliked()){




                    String totallike=gettingtimelinealllist.get(position).getTotalLikes();

                   *//* if (Integer.parseInt(totallike) == 0 || totallike == null) {
                        holder.like.setText("1");
                        holder.like.setVisibility(VISIBLE);
                        holder.liketxt.setVisibility(VISIBLE);

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");
                            String clickedlike_timelineid = Pref_storage.getDetail(context, "clickedlike_timelineid");
                            Call<CreateUserPojoOutput> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(clickedlike_timelineid), Integer.parseInt(createdby), 0);
                            call.enqueue(new Callback<CreateUserPojoOutput>() {
                                @Override
                                public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {


                                    if (response.body().getResponseMessage().contains("nodata")) {

                                        holder.img_like.setChecked(false);
                                        like_flag = false;
                                    }

                                }

                                @Override
                                public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else*//*



                    if (Integer.parseInt(totallike) >=1) {
                        int likeincre = (Integer.parseInt(totallike)-1);
                        holder.like.setText(String.valueOf(likeincre));
                        holder.like.setVisibility(VISIBLE);
                        holder.liketxt.setVisibility(VISIBLE);

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");
                            String clickedlike_timelineid = Pref_storage.getDetail(context, "clickedlike_timelineid");
                            Call<CreateUserPojoOutput> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(clickedlike_timelineid), Integer.parseInt(createdby), 0);
                            call.enqueue(new Callback<CreateUserPojoOutput>() {
                                @Override
                                public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                                    if (response.body().getResponseCode()==1) {
                                        holder.img_like.setChecked(false);
                                        like_flag = false;

                                    }

                                }

                                @Override
                                public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }
*/




/*


                if(!gettingtimelinealllist.get(position).isGetisuserliked()){

                    String totallike=gettingtimelinealllist.get(position).getTotalLikes();
                    if (Integer.parseInt(totallike) == 0 || totallike == null) {

                        holder.like.setText(String.valueOf(totallike));
                        holder.like.setText("1");
                        holder.like.setVisibility(VISIBLE);
                        holder.liketxt.setVisibility(VISIBLE);

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");
                            String clickedlike_timelineid = Pref_storage.getDetail(context, "clickedlike_timelineid");
                            Call<CreateUserPojoOutput> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(clickedlike_timelineid), Integer.parseInt(createdby), 1);
                            call.enqueue(new Callback<CreateUserPojoOutput>() {
                                @Override
                                public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {


                                    if (response.body().getResponseCode() == 1) {

                                        holder.img_like.setChecked(true);
                                        like_flag = true;
                                    }

                                }

                                @Override
                                public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else if (Integer.parseInt(totallike) >=1) {
                        int likeincre = (Integer.parseInt(totallike) + 1);
                        holder.like.setText(String.valueOf(likeincre));
                        holder.like.setVisibility(VISIBLE);
                        holder.liketxt.setVisibility(VISIBLE);

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(context, "userId");
                            String clickedlike_timelineid = Pref_storage.getDetail(context, "clickedlike_timelineid");
                            Call<CreateUserPojoOutput> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(clickedlike_timelineid), Integer.parseInt(createdby), 1);
                            call.enqueue(new Callback<CreateUserPojoOutput>() {
                                @Override
                                public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {


                                    if (response.body().getResponseCode() == 1) {

                                        holder.img_like.setChecked(true);
                                        like_flag = true;
                                    }

                                }

                                @Override
                                public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }




                }
*/

            }
        });


 /*       holder.img_like.setOnCheckStateChangeListener(new ShineButton.OnCheckedChangeListener() {


            @Override
            public void onCheckedChanged(View view, boolean checked) {
                liketimelineid = gettingtimelinealllist.get(position).getTimelineId();
                if (holder.img_like.isChecked()) {
                    String totlikes = (gettingtimelinealllist.get(position).getTotalLikes());
                    if (Integer.parseInt(totlikes) == 0 || totlikes == null) {
//                    if (totlikes== 0 ) {
                        int likeincre = (Integer.parseInt(totlikes) + 1);
//                        int likeincre =((totlikes) + 1);

                        holder.like.setText("1");
                        holder.like.setVisibility(VISIBLE);
                        holder.liketxt.setVisibility(VISIBLE);
                        holder.img_like.setChecked(true);
                        send_user_like();
                    } else if (Integer.parseInt(totlikes) > 1) {
                        int likeincre = (Integer.parseInt(totlikes) + 1);
//                        String likeincre =  (totlikes + 1);
                        holder.like.setText(String.valueOf(likeincre));
                        holder.like.setVisibility(VISIBLE);
                        holder.liketxt.setVisibility(VISIBLE);
                        holder.img_like.setChecked(true);
                        send_user_like();
                    }
                } else {
                    String totlikes = gettimelineAllModel.getTotalLikes();
//                    int totlikes = gettimelineAllModel.getTotal_likes();
                    if (Integer.parseInt(totlikes) == 0 || totlikes == null) {
                        holder.like.setVisibility(GONE);
                        holder.liketxt.setVisibility(GONE);
                        holder.img_like.setChecked(false);
                        send_user_unlike();

                    } else {

                        int likedecre = (Integer.parseInt(totlikes) - 1);

                        holder.like.setText(String.valueOf(likedecre));
                        holder.like.setVisibility(VISIBLE);
                        holder.liketxt.setVisibility(VISIBLE);
                        holder.img_like.setChecked(false);
                        send_user_unlike();
                    }

                }
            }
        });*/


//        holder.img_like.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                holder.img_like.showAnim();
//
//
//            }
//        });
//

        holder.edittxt_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (holder.edittxt_comment.getText().toString().length() != 0) {
                    holder.txt_adapter_post.setVisibility(VISIBLE);
                } else {
                    holder.txt_adapter_post.setVisibility(GONE);
                }


            }
        });

        holder.rl_post_timeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commenttimelineid = gettingtimelinealllist.get(position).getTimelineId();
                createcomment = holder.edittxt_comment.getText().toString();
                hideKeyboard(v);
                holder.edittxt_comment.setText("");
               /* try {
                    String username = Pref_storage.getDetail(context, "firstName");
                    String userlaname = Pref_storage.getDetail(context, "lastName");
                    holder.user_name_comment.setText(username.concat(" ").concat(userlaname));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                holder.user_name_comment.setVisibility(VISIBLE);*/


                int Cmnt = Integer.parseInt(gettingtimelinealllist.get(position).getTotalComments()) + 1;
                holder.comment.setText(String.valueOf(Cmnt));
                holder.comment.setVisibility(VISIBLE);
                if (Cmnt == 1) {
                    holder.viewcomment.setText(R.string.view);
                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setText(R.string.commenttt);
                    holder.commenttest.setVisibility(VISIBLE);
                } else if (Cmnt > 1) {
                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setVisibility(VISIBLE);
                }

       /*         holder.user_txt_comment.setText(createcomment);
                holder.user_txt_comment.setVisibility(VISIBLE);*/
                create_edit_timeline_comments();
            }
        });


        holder.rl_likes_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pref_storage.setDetail(context, "Likes_timelineid", gettingtimelinealllist.get(position).getTimelineId());
                context.startActivity(new Intent(context, likesActivity.class));
//                ((AppCompatActivity) context).finish();
            }
        });


        holder.rl_comments_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pref_storage.setDetail(context, "comment_timelineid", gettingtimelinealllist.get(position).getTimelineId());
                context.startActivity(new Intent(context, Comments_activity.class));

//                ((AppCompatActivity) context).finish();
            }
        });

/*        holder.rl_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                send_user_like();

            }


        });*/


        holder.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_id = gettingtimelinealllist.get(position).getUserId();
                String timelineid = gettingtimelinealllist.get(position).getTimelineId();

//                Pref_storage.setDetail(context, "clickeduser_id_timeline", user_id);
//                Pref_storage.setDetail(context, "clickedtimelineid_for_user", timelineid);
                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);
                }


            }


        });
        holder.userlastname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_id = gettingtimelinealllist.get(position).getUserId();
                String timelineid = gettingtimelinealllist.get(position).getTimelineId();
//                Pref_storage.setDetail(context, "clickeduser_id_timeline", user_id);
//
//                Pref_storage.setDetail(context, "clickedtimelineid_for_user", timelineid);
                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }


            }


        });


        holder.rl_follow_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_id = gettingtimelinealllist.get(position).getUserId();
                Pref_storage.setDetail(context, "clicked_timeline_user_id", user_id);

                if (Pref_storage.getDetail(context, "userId").equals(Pref_storage.getDetail(context, "clicked_timeline_user_id"))) {
                    holder.txt_timeline_follow.setVisibility(GONE);
                    holder.img_follow_timeline.setVisibility(GONE);
                } else {
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        String createdby = Pref_storage.getDetail(context, "userId");
                        String going_to_follow_user_id = Pref_storage.getDetail(context, "clicked_timeline_user_id");

                        Call<Get_following_output_Pojo> call = apiService.create_follower("create_follower", Integer.parseInt(createdby), Integer.parseInt(going_to_follow_user_id), 1);
                        call.enqueue(new Callback<Get_following_output_Pojo>() {
                            @Override
                            public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {

                                if (response.body().getResponseCode() == 1) {


                                    for (int j = 0; j < response.body().getData().size(); j++) {

                                        getFollowingProfilePojo = new Get_following_profile_pojo("", "", "", "",

                                                "", "", "", "", "", "", "", "", "", "", "", "",
                                                "", "", "", "");

                                        String followeruserID = response.body().getData().get(j).getUser_id();
                                        Pref_storage.setDetail(context, "followeruserID", followeruserID);

                                        String createdOn = response.body().getData().get(j).getCreated_on();
                                        String Total_followers = response.body().getData().get(j).getTotal_followers();
                                        String total_followings = response.body().getData().get(j).getTotal_followings();
                                        String userId = response.body().getData().get(j).getUser_id();
                                        String oauthProvider = response.body().getData().get(j).getOauth_provider();
                                        String oauthUid = response.body().getData().get(j).getOauth_uid();
                                        String firstName1 = response.body().getData().get(j).getFirst_name();
                                        String lastName1 = response.body().getData().get(j).getLast_name();
                                        String location = response.body().getData().get(j).getLatitude();
                                        String locationstate = response.body().getData().get(j).getLongitude();
                                        String email1 = response.body().getData().get(j).getEmail();
                                        String contNo = response.body().getData().get(j).getCont_no();
                                        String gender = response.body().getData().get(j).getGender();
                                        String dob = response.body().getData().get(j).getDob();
                                        String following_picture = response.body().getData().get(j).getPicture();

                                        holder.txt_timeline_follow.setText(R.string.txt_following);
                                        holder.img_follow_timeline.setImageResource(R.drawable.ic_followers);


                                    }


                                }

                            }

                            @Override
                            public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }
        });

        holder.selfusername_caption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_id = gettingtimelinealllist.get(position).getUserId();
                String timelineid = gettingtimelinealllist.get(position).getTimelineId();

//                Pref_storage.setDetail(context, "clickedtimelineid_for_user", timelineid);
//                Pref_storage.setDetail(context, "clickeduser_id_timeline", user_id);
                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);
                }


            }


        });


        holder.selfuserlastname_caption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_id = gettingtimelinealllist.get(position).getUserId();
//                Pref_storage.setDetail(context, "clickeduser_id_timeline", user_id);

                String timelineid = gettingtimelinealllist.get(position).getTimelineId();

//                Pref_storage.setDetail(context, "clickedtimelineid_for_user", timelineid);
                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);
                }


            }


        });

        holder.txt_followers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  CustomDialogClass2 ccd = new CustomDialogClass2(context);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccd.setCancelable(true);
                ccd.show();*/
            }
        });


        holder.rl_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                String timelineid = gettingtimelinealllist.get(position).getTimelineId();
                String userid = gettingtimelinealllist.get(position).getUserId();
                Pref_storage.setDetail(context, "clicked_timelineID", timelineid);
                Pref_storage.setDetail(context, "clicked_timelineuserID", userid);


                CustomDialogClass ccd = new CustomDialogClass(context);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                ccd.setCancelable(true);
                ccd.show();

            }

        });


    }

    @Override
    public int getItemCount() {

        return gettingtimelinealllist.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        scaleGestureDetector.onTouchEvent(e);


    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }


    private void moreself() {

        AlertDialog.Builder front_builder = new AlertDialog.Builder(context);
        front_builder.setItems(itemsothers, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (itemsothers[which].equals("Report")) {

                    dialog.dismiss();
                } else if (itemsothers[which].equals("Copy Link")) {

                    dialog.dismiss();
                } else if (itemsself[which].equals("Turn Off Post Notification")) {

                    dialog.dismiss();
                } else if (itemsothers[which].equals("Share Via WhatsApp")) {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                    i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                    context.startActivity(Intent.createChooser(i, "Share via"));

                    dialog.dismiss();
                }
            }
        });
        front_builder.show();

    }

    private void moreothers() {
        AlertDialog.Builder front_builder = new AlertDialog.Builder(context);

        front_builder.setItems(itemsself, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (itemsself[which].equals("Delete Post")) {
                    Pref_storage.getDetail(context, "clicked_timelineID");

//                    Toast.makeText(context, "" + timelineid, Toast.LENGTH_SHORT).show();

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure to delete this post?")
                            .setContentText("Won't be able to recover this post anymore!")
                            .setConfirmText("Yes,delete it!")
                            .setCancelText("No,cancel!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    create_delete_timeline();

                                }
                            })
                            .show();
                    dialog.dismiss();
                } else if (itemsself[which].equals("Report")) {

                    dialog.dismiss();
                } else if (itemsself[which].equals("Copy Link")) {

                    dialog.dismiss();
                } else if (itemsself[which].equals("Turn Off Post Notification")) {

                    dialog.dismiss();
                } else if (itemsself[which].equals("Share Via WhatsApp")) {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                    i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                    context.startActivity(Intent.createChooser(i, "Share via"));

                    dialog.dismiss();
                }
            }
        });
        front_builder.show();
    }


    //View Pager

    private void init() {
//        for(int i=0;i<IMAGES.length;i++)
//            ImagesArray.add(IMAGES[i]);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            String userid = Pref_storage.getDetail(context, "userId");

            Call<Timeline_image_only_output_pojo> call = apiService.Get_timeline_image_all("get_timeline_image_over", Integer.parseInt(userid));
            call.enqueue(new Callback<Timeline_image_only_output_pojo>() {
                @Override
                public void onResponse(Call<Timeline_image_only_output_pojo> call, Response<Timeline_image_only_output_pojo> response) {


                    Log.e("vishnuresponse", "" + response.body().getResponseCode());

                    if (response.body().getResponseCode() == 1) {


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Timeline_image_only_pojo timelineImageOnlyPojo = new Timeline_image_only_pojo("", "", "");

                            String userid = response.body().getData().get(j).getUserid();
                            String timelineid = response.body().getData().get(j).getTimelineid();
                            String picc = response.body().getData().get(j).getTimelineimage();

//                            Log.e("picc", "" + response.body().getData().get(j).getTimelineimage());
//                            for (int ab=0; ab<response.body().getData().get(ab).getTimelineimage().length();ab++){

                            Timeline_image_only_pojo timelineImageOnlyPojo2 = new Timeline_image_only_pojo(timelineid, userid, picc);

//                                ImagesArray.add(picc);
                            ImagesArray_pojo.add(timelineImageOnlyPojo2);

                             /*   mviewpager.setAdapter(new timeline_only_slide_adapter(context,ImagesArray_pojo));
                                circleIndicator.setViewPager(mviewpager);
*/
                            final float density = context.getResources().getDisplayMetrics().density;

//        indicator.setRadius(5 * density);
                            NUM_PAGES = ImagesArray.size();


                            // Auto start of viewpager
                            final Handler handler = new Handler();
                            final Runnable Update = new Runnable() {
                                public void run() {
                                    if (currentPage == NUM_PAGES) {
                                        currentPage = 0;
                                    }
                                    mviewpager.setCurrentItem(currentPage++, true);
                                }
                            };
 /*       Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
*/
                            // Pager listener over indicator
                            circleIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                                @Override
                                public void onPageSelected(int position) {
                                    currentPage = position;

                                }

                                @Override
                                public void onPageScrolled(int pos, float arg1, int arg2) {

                                }

                                @Override
                                public void onPageScrollStateChanged(int pos) {

                                }
                            });


//                            }

                        }
                    }

                }

                @Override
                public void onFailure(Call<Timeline_image_only_output_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

/*
    private void init_2() {
//        for(int i=0;i<IMAGES.length;i++)
//            ImagesArray.add(IMAGES[i]);
        mviewpager.setAdapter(new SlidingImage_Adapter(context, getimagelist));
        circleIndicator.setViewPager(mviewpager);

        final float density = context.getResources().getDisplayMetrics().density;

//        indicator.setRadius(5 * density);
        NUM_PAGES = getimagelist.size();


        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mviewpager.setCurrentItem(currentPage++, true);
            }
        };
 *//*       Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
*//*
        // Pager listener over indicator
        circleIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }*/

    @Override
    public boolean onTouch(View v, MotionEvent event) {


        return false;
    }

    //Custom alert

    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity c;
        public Dialog d;
        public AppCompatButton btn_Ok;
        TextView txt_deletepost, txt_report, txt_copylink, txt_turnoffpost, txt_sharevia, txt_editpost;

        CustomDialogClass(Context a) {
            super(a);
            // TODO Auto-generated constructor stub
            context = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
//            setContentView(R.layout.dialog_version_info);

            setContentView(R.layout.custom_dialog_timeline);

            txt_deletepost = (TextView) findViewById(R.id.txt_deletepost);
            txt_editpost = (TextView) findViewById(R.id.txt_editpost);
            txt_report = (TextView) findViewById(R.id.txt_report);
            txt_copylink = (TextView) findViewById(R.id.txt_copylink);
            txt_turnoffpost = (TextView) findViewById(R.id.txt_turnoffpost);
            txt_sharevia = (TextView) findViewById(R.id.txt_sharevia);

            txt_deletepost.setOnClickListener(this);
            txt_editpost.setOnClickListener(this);
            txt_report.setOnClickListener(this);
            txt_copylink.setOnClickListener(this);
            txt_turnoffpost.setOnClickListener(this);
            txt_sharevia.setOnClickListener(this);
            if (Pref_storage.getDetail(context, "clicked_timelineuserID").equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                txt_deletepost.setVisibility(VISIBLE);
                txt_editpost.setVisibility(VISIBLE);
            } else {
                txt_deletepost.setVisibility(GONE);
                txt_editpost.setVisibility(GONE);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_editpost:
                    Pref_storage.getDetail(context, "clicked_timelineID");
                    context.startActivity(new Intent(context, Edit_newsfeed_post.class));
                    dismiss();
                    break;

                    case R.id.txt_deletepost:

                    Pref_storage.getDetail(context, "clicked_timelineID");

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to delete this post?")
                            .setContentText("Won't be able to recover this post anymore!")
                            .setConfirmText("Yes,delete it!")
                            .setCancelText("No,cancel!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    create_delete_timeline();

                                }
                            })
                            .show();
                    dismiss();
                    break;
                case R.id.txt_report:
                    dismiss();
                    break;
                case R.id.txt_copylink:
                    Toast.makeText(context, "Link has been copied to clipboard", Toast.LENGTH_SHORT).show();
                    dismiss();
                    break;
                case R.id.txt_turnoffpost:
                    dismiss();
                    break;
                case R.id.txt_sharevia:
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                    i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                    context.startActivity(Intent.createChooser(i, "Share via"));
                    dismiss();
                    break;

                default:
                    break;
            }
        }
    }

/*
//custom dialog for alert


    public class CustomDialogClass2 extends Dialog implements View.OnClickListener {

        public Activity activity;
        public Dialog dialog;
        AppCompatButton btn_cancel, btn_ok, txt_logout, txt_alert;
//        TextView txt_deletepost, txt_report, txt_copylink, txt_turnoffpost, txt_sharevia, txt_editpost;

        CustomDialogClass2(Context a) {
            super(a);
            // TODO Auto-generated constructor stub
            context = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_alert_dialog_timeline);

            btn_cancel = (AppCompatButton) findViewById(R.id.btn_cancel);
            btn_ok = (AppCompatButton) findViewById(R.id.btn_ok);
            btn_cancel.setOnClickListener(this);
            btn_ok.setOnClickListener(this);



*/
/*

            txt_logout = (TextView) findViewById(R.id.txt_logout);
            txt_logout.setOnClickListener(this);

            txt_cancel = (TextView) findViewById(R.id.txt_cancel);
            txt_cancel.setOnClickListener(this);
*//*


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.btn_cancel:
                    Toast.makeText(activity, "You logged out successfully", Toast.LENGTH_SHORT).show();
                    break;

                case R.id.btn_ok:
                    dismiss();
                    break;
                default:
                    break;
            }
        }
    }
*/


    //First timeline post API

    private void create_delete_timeline() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            String createdby = Pref_storage.getDetail(context, "userId");
            Call<CommonOutputPojo> call = apiService.create_delete_timeline("create_delete_timeline", Integer.parseInt(Pref_storage.getDetail(context, "clicked_timelineID")), Integer.parseInt(createdby));
            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    if (response.body().getResponseCode() == 1) {

                        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Deleted!!")
//                                .setContentText("Your post has been deleted.")

                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        context.startActivity(new Intent(context, Home.class));
                                        ((AppCompatActivity) context).finish();


                                    }
                                })
                                .show();


                    }

                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    //User like API
    private void send_user_like() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            String createdby = Pref_storage.getDetail(context, "userId");
            String clickedlike_timelineid = Pref_storage.getDetail(context, "clickedlike_timelineid");
            Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(clickedlike_timelineid), Integer.parseInt(createdby), 1);
            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


//                    Toast.makeText(context, "vishnulikes" + response.body().getResponseCode(), Toast.LENGTH_SHORT).show();
                    Log.e("vishnulikes", "" + response.body().getResponseCode());

                    if (response.body().getResponseCode() == 1) {
                    }

                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    //User API Unlike
    private void send_user_unlike() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            String timelineid = "1";
            String createdby = Pref_storage.getDetail(context, "userId");
            Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(liketimelineid), Integer.parseInt(createdby), 0);
            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


//                 Toast.makeText(context, "vishnuunlike" + response.body().getResponseCode(), Toast.LENGTH_SHORT).show();
//                    Log.e("vishnuunlike", "" + response.body().getResponseCode());

                    if (response.body().getResponseCode() == 1) {

                    /*    context.startActivity(new Intent(context, Home.class));
                        ((AppCompatActivity) context).finish();
*/

                    }

                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    //Create timeline comment API
    private void create_edit_timeline_comments() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {
            String createuserid = Pref_storage.getDetail(context, "userId");

            Call<CreateEditTimeline_Comments> call = apiService.create_edit_timeline_comments1("create_edit_timeline_comments", 0, Integer.parseInt(commenttimelineid), createcomment, Integer.parseInt(createuserid));
            call.enqueue(new Callback<CreateEditTimeline_Comments>() {
                @Override
                public void onResponse(Call<CreateEditTimeline_Comments> call, Response<CreateEditTimeline_Comments> response) {

                    if (response.body().getResponseCode() == 1) {


                        get_timeline_comments_all();


                    }

                }

                @Override
                public void onFailure(Call<CreateEditTimeline_Comments> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    //Get all comments of timeline API
    private void get_timeline_comments_all() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {
            String createuserid = Pref_storage.getDetail(context, "userId");
            String comment_timelineid = Pref_storage.getDetail(context, "comment_timelineid");
//            Call<comments_2> call = apiService.get_timeline_comments_all("get_timeline_comments_user", Integer.parseInt(createuserid), 22);
            Call<comments_2> call = apiService.get_timeline_comments_all("get_timeline_comments_user", Integer.parseInt(comment_timelineid), Integer.parseInt(createuserid));
            call.enqueue(new Callback<comments_2>() {
                @Override
                public void onResponse(Call<comments_2> call, Response<comments_2> response) {

                    if (response.body().getResponseCode() == 1) {


                    }

                }

                @Override
                public void onFailure(Call<comments_2> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    //Geo location address finding
    private void AddressFinder(double latitude, double longitude) {


        Geocoder geocoder;
        List<Address> addresses = new ArrayList<>();
        geocoder = new Geocoder(context, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            latituderesultuser = String.valueOf(addresses.get(0).getLatitude());
            longituderesultuser = String.valueOf(addresses.get(0).getLongitude());


        } catch (IOException e) {

            e.printStackTrace();

        }
    }

//View holder

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView username, userlastname, location, location_state, like, liketxt, user_txt_comment, count, comma, count_post, txt_followers_count, txt_followers, selfusername_caption, selfuserlastname_caption, user_name_comment, viewcomment, txt_timeline_follow, comment, commenttest, share, caption, captiontag, txt_created_on, txt_adapter_post;
        RelativeLayout  rl_likes_layout, rl_comments_lay;
        LinearLayout rl_row_layout;
        RelativeLayout rl_share_adapter, rl_comment_adapter, rl_comments_layout, like_rl, rl_username, rl_more, rl_post_timeline, rl_follow_layout, rl_newlike_adapter;

        ImageView /*Image,*/ img_view_more, userphoto_profile, img_follow_timeline, img_commentadapter, img_shareadapter, userphoto_comment;
        //                SubsamplingScaleImageView Image;
        EditText edittxt_comment;
        ImageButton img_like;


        //        CheckBox img_like;
//        ZoomageView Image;
        PhotoView image;

        //        ViewPager image;
        PhotoViewAttacher photoViewAttacher;
        //ImageButton

        ImageButton like_image_empty, like_imagefill;

        CheckBox img_like_new;

        @SuppressLint("ClickableViewAccessibility")
        public MyViewHolder(View itemView) {
            super(itemView);

            //Shine Button
            img_like = (ImageButton) itemView.findViewById(R.id.img_like);

            //Edittext
            edittxt_comment = (EditText) itemView.findViewById(R.id.edittxt_comment);


            //ViewPager
            mviewpager = (ViewPager) itemView.findViewById(R.id.mviewpager);


            //Circle Indicator
            circleIndicator = (CircleIndicator) itemView.findViewById(R.id.indicator);

//            Image.setImage(ImageSource.resource(R.drawable.burger));


            //ImageView
            image = (PhotoView) itemView.findViewById(R.id.image);
            userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);
            userphoto_comment = (ImageView) itemView.findViewById(R.id.userphoto_comment);
            img_commentadapter = (ImageView) itemView.findViewById(R.id.img_commentadapter);
            img_shareadapter = (ImageView) itemView.findViewById(R.id.img_shareadapter);
            img_view_more = (ImageView) itemView.findViewById(R.id.img_view_more);
            img_follow_timeline = (ImageView) itemView.findViewById(R.id.img_follow_timeline);

         /*   //ImageButton
            like_imagefill = (ImageButton) itemView.findViewById(R.id.like_imagefill);
            like_image_empty = (ImageButton) itemView.findViewById(R.id.like_image_empty);


            img_like_new = (CheckBox) itemView.findViewById(R.id.img_like_new);*/
            //TextView
            username = (TextView) itemView.findViewById(R.id.username);
            txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);
            userlastname = (TextView) itemView.findViewById(R.id.userlastname);
            location = (TextView) itemView.findViewById(R.id.location);
//            location_state = (TextView) itemView.findViewById(R.id.location_state);
            caption = (TextView) itemView.findViewById(R.id.caption);
            captiontag = (TextView) itemView.findViewById(R.id.captiontag);
            txt_created_on = (TextView) itemView.findViewById(R.id.txt_created_on);
            like = (TextView) itemView.findViewById(R.id.like);
            liketxt = (TextView) itemView.findViewById(R.id.liketxt);
            comment = (TextView) itemView.findViewById(R.id.comment);
            txt_timeline_follow = (TextView) itemView.findViewById(R.id.txt_timeline_follow);
            comma = (TextView) itemView.findViewById(R.id.comma);
            user_txt_comment = (TextView) itemView.findViewById(R.id.user_txt_comment);
            user_name_comment = (TextView) itemView.findViewById(R.id.user_name_comment);
            selfusername_caption = (TextView) itemView.findViewById(R.id.selfusername_caption);
            selfuserlastname_caption = (TextView) itemView.findViewById(R.id.selfuserlastname_caption);
            viewcomment = (TextView) itemView.findViewById(R.id.View_all_comment);
            commenttest = (TextView) itemView.findViewById(R.id.commenttest);
            share = (TextView) itemView.findViewById(R.id.shares);
            count = (TextView) itemView.findViewById(R.id.count);
            count_post = (TextView) itemView.findViewById(R.id.count_post);
            txt_followers = (TextView) itemView.findViewById(R.id.txt_followers);
            txt_followers_count = (TextView) itemView.findViewById(R.id.txt_followers_count);

            //RelativeLayout
            rl_row_layout = (LinearLayout) itemView.findViewById(R.id.layout_one_foradapter);
            rl_likes_layout = (RelativeLayout) itemView.findViewById(R.id.rl_likes_layout);
            rl_comments_lay = (RelativeLayout) itemView.findViewById(R.id.rl_comments_layout);
            rl_share_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_share_adapter);
            rl_comment_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_comment_adapter);
            like_rl = (RelativeLayout) itemView.findViewById(R.id.like_rl);
            rl_username = (RelativeLayout) itemView.findViewById(R.id.rl_username);
            rl_more = (RelativeLayout) itemView.findViewById(R.id.rl_more);
            rl_follow_layout = (RelativeLayout) itemView.findViewById(R.id.rl_follow_layout);
            rl_post_timeline = (RelativeLayout) itemView.findViewById(R.id.rl_post_timeline);
//            rl_newlike_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_newlike_adapter);


        }


    }


}
