package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Lalith on 27-06-2018.
 */

public class Review_created_adapter extends RecyclerView.ViewHolder implements View.OnClickListener {


    EmojiconEditText review_comment;

    ViewPager ambi_viewpager;

    ExpandableTextView reviewcmmnt;

    CircleIndicator ambi_indicator;

    RecyclerView review_photos_recylerview, rv_review_top_dish, rv_review_avoid_dish,package_recylerview;

    ImageView userphoto_profile, review_user_photo, img_overall_rating_star,
            img_ambience_star, img_taste_star, img_vfm_star, img_service_star;

    RelativeLayout rl_overall_review, rl_ambiance_dot, rl_hotel_details, rl_avoid_dish, rl_top_dish, rl_view_more_top, rl_view_more_avoid,rl_comment_share;

    LinearLayout ll_top_dish, ll_ambi_more, ll_view_more, ll_dish_to_avoid, ll_likecomment, ll_res_overall_rating, ll_ambience_rating, ll_taste_rating,
            ll_value_of_money_rating, ll_service_rating, ll_delivery_mode, ll_review_headline, ll_review_main;

    ImageButton img_btn_review_like, layout_more, img_btn_review_comment, img_bucket_list, img_view_more, avoid_more, top_more;

    // TextView

    TextView username, txt_mode_delivery, userlastname, count, count_post, comma, txt_followers_count, txt_followers, serv_or_package,
            txt_created_on, tv_restaurant_name, txt_hotel_address, tv_res_overall_rating, tv_ambience_rating, ambi_or_timedelivery,
            txt_taste_count, tv_vfm_rating, tv_service_rating, title_ambience, tv_view_more, tv_top_dishes,
            tv_dishes_to_avoid, title_dish_avoid, title_top_dish, tv_show_less, txt_review_caption,
            tv_like_count, tv_view_all_comments, txt_adapter_post, tv_headline_username, tv_review_headlinetext;

    ImageView img_reviewsharechat;

    onCommentsPostListener onCommentsPostListener;

    ProgressBar comments_progressBar;

    View itemView;

    public Review_created_adapter(View itemView, onCommentsPostListener onCommentsPostListener) {

        super(itemView);

        this.onCommentsPostListener = onCommentsPostListener;
        this.itemView = itemView;

        img_reviewsharechat = (ImageView) itemView.findViewById(R.id.img_reviewsharechat);
        username = (TextView) itemView.findViewById(R.id.username);
        txt_mode_delivery = (TextView) itemView.findViewById(R.id.txt_mode_delivery);
        userlastname = (TextView) itemView.findViewById(R.id.userlastname);
        count = (TextView) itemView.findViewById(R.id.count);
        count_post = (TextView) itemView.findViewById(R.id.count_post);
        comma = (TextView) itemView.findViewById(R.id.comma);
        txt_followers_count = (TextView) itemView.findViewById(R.id.txt_followers_count);
        txt_followers = (TextView) itemView.findViewById(R.id.txt_followers);
        txt_created_on = (TextView) itemView.findViewById(R.id.txt_created_on);
        tv_restaurant_name = (TextView) itemView.findViewById(R.id.tv_restaurant_name);
        txt_hotel_address = (TextView) itemView.findViewById(R.id.txt_hotel_address);
        tv_res_overall_rating = (TextView) itemView.findViewById(R.id.tv_res_overall_rating);
        tv_ambience_rating = (TextView) itemView.findViewById(R.id.tv_ambience_rating);
        txt_taste_count = (TextView) itemView.findViewById(R.id.txt_taste_count);
        tv_ambience_rating = (TextView) itemView.findViewById(R.id.tv_ambience_rating);
        ambi_or_timedelivery = (TextView) itemView.findViewById(R.id.ambi_or_timedelivery);
        serv_or_package = (TextView) itemView.findViewById(R.id.serv_or_package);
        tv_vfm_rating = (TextView) itemView.findViewById(R.id.tv_vfm_rating);
        tv_service_rating = (TextView) itemView.findViewById(R.id.tv_service_rating);
        tv_view_more = (TextView) itemView.findViewById(R.id.tv_view_more);
        title_ambience = (TextView) itemView.findViewById(R.id.title_ambience);
        tv_top_dishes = (TextView) itemView.findViewById(R.id.tv_top_dishes);
        tv_dishes_to_avoid = (TextView) itemView.findViewById(R.id.tv_dishes_to_avoid);
        title_dish_avoid = (TextView) itemView.findViewById(R.id.title_dish_avoid);
        title_top_dish = (TextView) itemView.findViewById(R.id.title_top_dish);
        tv_show_less = (TextView) itemView.findViewById(R.id.tv_show_less);
        tv_like_count = (TextView) itemView.findViewById(R.id.tv_like_count);
        tv_view_all_comments = (TextView) itemView.findViewById(R.id.tv_view_all_comments);
        txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);
        tv_headline_username = (TextView) itemView.findViewById(R.id.tv_headline_username);
        tv_review_headlinetext = (TextView) itemView.findViewById(R.id.tv_review_headlinetext);


        txt_hotel_address = (TextView) itemView.findViewById(R.id.txt_hotel_address);


        ambi_viewpager = (ViewPager) itemView.findViewById(R.id.ambi_viewpager);

        ambi_indicator = (CircleIndicator) itemView.findViewById(R.id.ambi_indicator);
        review_comment = (EmojiconEditText) itemView.findViewById(R.id.review_comment);
        reviewcmmnt = (ExpandableTextView) itemView.findViewById(R.id.hotel_revieww_description);


        txt_review_caption = (TextView) itemView.findViewById(R.id.tv_review_caption);

        //Imageview

        userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);

        img_overall_rating_star = (ImageView) itemView.findViewById(R.id.img_overall_rating_star);
        img_ambience_star = (ImageView) itemView.findViewById(R.id.img_ambience_star);
        img_taste_star = (ImageView) itemView.findViewById(R.id.img_taste_star);
        img_vfm_star = (ImageView) itemView.findViewById(R.id.img_vfm_star);
        img_service_star = (ImageView) itemView.findViewById(R.id.img_service_star);
        userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);
        review_user_photo = (ImageView) itemView.findViewById(R.id.review_user_photo);

        //ImageButton
        img_btn_review_like = (ImageButton) itemView.findViewById(R.id.img_btn_review_like);
        img_btn_review_comment = (ImageButton) itemView.findViewById(R.id.img_btn_review_comment);
        img_view_more = (ImageButton) itemView.findViewById(R.id.img_view_more);
        img_bucket_list = (ImageButton) itemView.findViewById(R.id.img_bucket_list);
        top_more = (ImageButton) itemView.findViewById(R.id.top_more);
        avoid_more = (ImageButton) itemView.findViewById(R.id.avoid_more);
        layout_more = (ImageButton) itemView.findViewById(R.id.layout_more);


        rl_overall_review = (RelativeLayout) itemView.findViewById(R.id.rl_overall_review);
        ll_likecomment = (LinearLayout) itemView.findViewById(R.id.ll_likecomment);
        ll_res_overall_rating = (LinearLayout) itemView.findViewById(R.id.ll_res_overall_rating);
        ll_dish_to_avoid = (LinearLayout) itemView.findViewById(R.id.ll_dish_to_avoid);

        ll_ambience_rating = (LinearLayout) itemView.findViewById(R.id.ll_ambience_rating);
        ll_taste_rating = (LinearLayout) itemView.findViewById(R.id.ll_taste_rating);
        ll_value_of_money_rating = (LinearLayout) itemView.findViewById(R.id.ll_value_of_money_rating);
        ll_service_rating = (LinearLayout) itemView.findViewById(R.id.ll_service_rating);
        ll_view_more = (LinearLayout) itemView.findViewById(R.id.ll_view_more);
        ll_ambi_more = (LinearLayout) itemView.findViewById(R.id.ll_ambi_more);
        ll_review_main = (LinearLayout) itemView.findViewById(R.id.ll_review_main);
        ll_delivery_mode = (LinearLayout) itemView.findViewById(R.id.ll_delivery_mode);
        ll_review_headline = (LinearLayout) itemView.findViewById(R.id.ll_review_headline);
        ll_top_dish = (LinearLayout) itemView.findViewById(R.id.ll_top_dish);


        review_photos_recylerview = (RecyclerView) itemView.findViewById(R.id.review_photos_recylerview);
        package_recylerview = (RecyclerView) itemView.findViewById(R.id.package_recylerview);
        rv_review_top_dish = (RecyclerView) itemView.findViewById(R.id.rv_review_top_dish);
        rv_review_avoid_dish = (RecyclerView) itemView.findViewById(R.id.rv_review_avoid_dish);

        rl_ambiance_dot = (RelativeLayout) itemView.findViewById(R.id.rl_ambiance_dot);
        rl_hotel_details = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_details);
        rl_avoid_dish = (RelativeLayout) itemView.findViewById(R.id.rl_avoid_dish);
        rl_top_dish = (RelativeLayout) itemView.findViewById(R.id.rl_top_dish);
        rl_view_more_top = (RelativeLayout) itemView.findViewById(R.id.rl_view_more_top);
        rl_comment_share = (RelativeLayout) itemView.findViewById(R.id.rl_comment_share);

        rl_view_more_avoid = (RelativeLayout) itemView.findViewById(R.id.rl_view_more_avoid);
        comments_progressBar = (ProgressBar) itemView.findViewById(R.id.comments_progressbar);

        txt_adapter_post.setOnClickListener(this);
        img_reviewsharechat.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.txt_adapter_post:

                onCommentsPostListener.onPostCommitedView(view, getAdapterPosition(), itemView);
                break;

            case R.id.img_reviewsharechat:

                onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),itemView);
                break;
        }
    }
}