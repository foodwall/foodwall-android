/*
package com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.bucketListPackage.CreateBucketlistActivty;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersOutput;
import com.kaspontech.foodwall.modelclasses.Review_pojo.GetAllInsideReviewPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.GetAllReviewPojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.reviewPackage.ReviewDeliveryDetails;
import com.kaspontech.foodwall.reviewPackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewPackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewPackage.Review_Updated_Pojo.ReviewInputAllPojo;
import com.kaspontech.foodwall.reviewPackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.reviewPackage.Review_Updater_adapter.GetHotelDetailsAdapter;
import com.kaspontech.foodwall.reviewPackage.Review_Updater_adapter.ReviewAmbienceAdapter;
import com.kaspontech.foodwall.reviewPackage.Review_Updater_adapter.ReviewAvoidDishAdapter;
import com.kaspontech.foodwall.reviewPackage.Review_Updater_adapter.ReviewTopDishAdapter;
import com.kaspontech.foodwall.reviewPackage.Review_in_detail_activity;
import com.kaspontech.foodwall.reviewPackage.Reviews_comments_all_activity;
import com.kaspontech.foodwall.reviewPackage.ReviewsLikesAllActivity;
import com.kaspontech.foodwall.reviewPackage.allDishTab.AllDishDisplayActivity;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static java.lang.Integer.parseInt;

@RequiresApi(api = Build.VERSION_CODES.M)
public class ReviewHotelAllAdapter extends RecyclerView.Adapter<ReviewHotelAllAdapter.MyViewHolder>
        implements RecyclerView.OnItemTouchListener, RecyclerView.OnScrollChangeListener {


    private static final String TAG = "GetHotelDetailsAdapter";


    // Context

    public Context context;


    // Data


    public Pref_storage pref_storage;


    // Api

    public Call<CommonOutputPojo> call;
    public ApiInterface apiInterface;


    // Adapters

    public ReviewTopDishAdapter reviewTopDishAdapter;
    public ReviewAmbienceAdapter ambianceImagesAdapter;
    public ReviewAvoidDishAdapter reviewWorstDishAdapter;

    // Variables

    String fromwhich;

    boolean liked = false;

    public final String[] itemsothers = {"Bucket list options", "Delete review"};


    //Top dish list
    public ReviewTopDishPojo reviewTopDishPojo;
    //Avoid dish list
    public ReviewAvoidDishPojo reviewAvoidDishPojo;
    //Ambience list
    public ReviewAmbiImagePojo reviewAmbiImagePojo;

    public GetAllReviewPojo getHotelAllInnerReviewPojo;


    //Top dish Adapter
    public List<Integer> friendsList = new ArrayList<>();
    public List<GetFollowersData> getFollowersDataList = new ArrayList<>();
    public ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();
    public ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();
    public ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();
    public ArrayList<GetAllReviewPojo> getallHotelReviewPojoArrayList = new ArrayList<>();


    public ReviewHotelAllAdapter(Context c, ArrayList<GetAllReviewPojo> gettinglist,String fromwhich) {
        this.context = c;
        this.getallHotelReviewPojoArrayList = gettinglist;
        this.fromwhich=fromwhich;
//        setHasStableIds(true);

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ReviewHotelAllAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_hotel_review_all, parent, false);
        ReviewHotelAllAdapter.MyViewHolder vh = new ReviewHotelAllAdapter.MyViewHolder(v);
//        vh.setIsRecyclable(false);
        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ReviewHotelAllAdapter.MyViewHolder holder, final int position) {


//        holder.setIsRecyclable(false);

        getHotelAllInnerReviewPojo = getallHotelReviewPojoArrayList.get(position);

        //reviewer_name

//        String reviewer_fname = getHotelAllInnerReviewPojo.getFirstName();
//        String reviewer_lname = getHotelAllInnerReviewPojo.getLastName();

       */
/* if(getHotelAllInnerReviewPojo.getFirstName()==null){

            holder.username.setText("Unknown");
        }else {

            holder.username.setText(getHotelAllInnerReviewPojo.getFirstName());

        }


        if(getHotelAllInnerReviewPojo.getLastName()==null){

            holder.userlastname.setText("Unknown");

        }else {

            holder.userlastname.setText(getHotelAllInnerReviewPojo.getLastName());

        }
*//*


        holder.username.setTextColor(Color.parseColor("#000000"));
        holder.userlastname.setTextColor(Color.parseColor("#000000"));

        if (getallHotelReviewPojoArrayList.get(position).getCategoryType().equalsIgnoreCase("2")) {

            holder.title_ambience.setText("Packaging");

        } else {

            holder.title_ambience.setText("Hotel Ambience");

        }


//        if(getallHotelReviewPojoArrayList.get(position).getBucket_list() != 0){
//            holder.img_bucket_list.setImageResource(R.drawable.ic_bucket_neww);
//        }else{
//            holder.img_bucket_list.setImageResource(R.drawable.ic_bucket);
//        }

        // txt_hotelname

        holder.tv_restaurant_name.setText(getallHotelReviewPojoArrayList.get(position).getHotelName());

        // Mode of delivery text

        if (getallHotelReviewPojoArrayList.get(position).getMode_id().equalsIgnoreCase("0")) {

            holder.txt_mode_delivery.setVisibility(GONE);
//            holder.ll_delivery_mode.setVisibility(GONE);

        } else {

            Log.e("fromwhich", "onBindViewHolder: "+fromwhich );

            if(fromwhich==null||fromwhich.equalsIgnoreCase("")){

                holder.ll_delivery_mode.setVisibility(VISIBLE);

                if (getallHotelReviewPojoArrayList.get(position).getMode_id().equalsIgnoreCase("1")) {
                    holder.txt_mode_delivery.setText(getallHotelReviewPojoArrayList.get(position).getComp_name());
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#f25353"));

                } else if (getallHotelReviewPojoArrayList.get(position).getMode_id().equalsIgnoreCase("2")) {
                    holder.txt_mode_delivery.setText(getallHotelReviewPojoArrayList.get(position).getComp_name().replace("swiggy", "Swiggy"));
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#ff8008"));

                } else if (getallHotelReviewPojoArrayList.get(position).getMode_id().equalsIgnoreCase("3")) {
                    holder.txt_mode_delivery.setText(getallHotelReviewPojoArrayList.get(position).getComp_name().replace("Food panda", "Foodpanda"));
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#ffff8800"));

                } else if (getallHotelReviewPojoArrayList.get(position).getMode_id().equalsIgnoreCase("4")) {
                    holder.txt_mode_delivery.setText(getallHotelReviewPojoArrayList.get(position).getComp_name());
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#2ecc71"));

                }
            }else {
                holder.ll_delivery_mode.setVisibility(GONE);
            }




        }


        if (getallHotelReviewPojoArrayList.get(position).getCategoryType().equalsIgnoreCase("2")) {

            if (fromwhich==null||fromwhich.equalsIgnoreCase("")){
                holder.ll_delivery_mode.setVisibility(VISIBLE);
            }else {
                holder.ll_delivery_mode.setVisibility(GONE);
            }



        } else {

            holder.ll_delivery_mode.setVisibility(GONE);

        }

        // Reviewer Image

//        Utility.picassoImageLoader(getallHotelReviewPojoArrayList.get(position).getPicture(),0,holder.userphoto_profile,context );


        GlideApp.with(context)
                .load(getallHotelReviewPojoArrayList.get(position).getPicture())
                .placeholder(R.drawable.ic_add_photo)
                .into(holder.userphoto_profile);

        // User Image

//        Utility.picassoImageLoader(Pref_storage.getDetail(context, "picture_user_login"),0,holder.review_user_photo,context );



        GlideApp.with(context)
                .load(Pref_storage.getDetail(context, "picture_user_login"))
                .placeholder(R.drawable.ic_add_photo)
                .into(holder.review_user_photo);


        // Timestamp for review

        String createdon = getHotelAllInnerReviewPojo.getCreatedOn();

        Utility.setTimeStamp(createdon,holder.txt_created_on);


     */
/*   try {
            //Input time
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long time = sdf.parse(createdon).getTime();

            //Cuurent system time

            Date currentTime = Calendar.getInstance().getTime();
            SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String now_new= sdformat.format(currentTime);
            long nowdate = sdformat.parse(now_new).getTime();

//            String timeAgo = com.kaspontech.foodwall.Utills.TimeAgo.getTimeAgo_new(time,nowdate);

            CharSequence ago = DateUtils.getRelativeTimeSpanString(time, nowdate, DateUtils.FORMAT_ABBREV_RELATIVE);

            if (ago.toString().equalsIgnoreCase("0 minutes ago")) {
                holder.txt_created_on.setText(R.string.just_now);
            } else {
                holder.txt_created_on.append(ago + "");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }*//*



        // Display top dishes

        Log.e("Topdish", "-->"+getallHotelReviewPojoArrayList.get(position).getTopdish());
        if ( getallHotelReviewPojoArrayList.get(position).getTopdish().trim().equalsIgnoreCase("")||getallHotelReviewPojoArrayList.get(position).getTopdish().trim().isEmpty()|| getallHotelReviewPojoArrayList.get(position).getTopdish() == null ) {

            holder.tv_top_dishes.setVisibility(GONE);
            holder.rl_top_dish.setVisibility(GONE);
            holder.ll_top_dish.setVisibility(GONE);

        } else {

            holder.tv_top_dishes.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getallHotelReviewPojoArrayList.get(position).getTopdish().replace(",", ", ")));
            holder.rl_top_dish.setVisibility(VISIBLE);
            holder.ll_top_dish.setVisibility(VISIBLE);


        }


        try{

//            Log.e("Topdishimage", "-->"+getallHotelReviewPojoArrayList.get(position).getTopdishimage().get(0).getImg() );
            if(getallHotelReviewPojoArrayList.get(position).getTopdishimage().get(0).getImg().trim().equalsIgnoreCase("")||getallHotelReviewPojoArrayList.get(position).getTopdishimage().get(0).getImg().isEmpty()){

                holder.rl_top_dish.setVisibility(GONE);
                holder.tv_top_dishes.setVisibility(GONE);

            }else{
                holder.tv_top_dishes.setVisibility(VISIBLE);
                holder.rl_top_dish.setVisibility(VISIBLE);

            }
        }catch (Exception e){
            e.printStackTrace();
        }


        // Display avoid dishes


        Log.e("Avoiddish", "-->"+getallHotelReviewPojoArrayList.get(position).getAvoiddish());
        if (getallHotelReviewPojoArrayList.get(position).getAvoiddish().trim().equalsIgnoreCase("")||getallHotelReviewPojoArrayList.get(position).getAvoiddish().trim().isEmpty()||getallHotelReviewPojoArrayList.get(position).getAvoiddish() == null ) {

            holder.tv_dishes_to_avoid.setVisibility(GONE);
            holder.rl_avoid_dish.setVisibility(GONE);
            holder.ll_dish_to_avoid.setVisibility(GONE);



        } else {

            holder.tv_dishes_to_avoid.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getallHotelReviewPojoArrayList.get(position).getAvoiddish().replace(",", ", ")));
            holder.rl_avoid_dish.setVisibility(VISIBLE);
            holder.ll_dish_to_avoid.setVisibility(VISIBLE);


        }

        try{

            Log.e("Avoiddishimage", "-->"+getallHotelReviewPojoArrayList.get(position).getAvoiddishimage().get(0).getImg() );
            if(getallHotelReviewPojoArrayList.get(position).getAvoiddishimage().get(0).getImg().trim().equals("")){

                holder.rl_avoid_dish.setVisibility(GONE);
                holder.tv_dishes_to_avoid.setVisibility(GONE);

            }else{
                holder.tv_dishes_to_avoid.setVisibility(VISIBLE);
                holder.rl_avoid_dish.setVisibility(VISIBLE);

            }
        }catch (Exception e){
            e.printStackTrace();
        }


        // Edit text comment

        holder.review_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (holder.review_comment.getText().toString().trim().length() != 0) {
                    holder.txt_adapter_post.setVisibility(VISIBLE);
                } else {
                    holder.txt_adapter_post.setVisibility(GONE);
                }


            }
        });


        // Comment posting API
        holder.txt_adapter_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideKeyboard(v);

                String comment = org.apache.commons.text.StringEscapeUtils.escapeJava(holder.review_comment.getText().toString().trim());

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {
                    String createuserid = Pref_storage.getDetail(context, "userId");

                    Call<CommonOutputPojo> call = apiService.create_edit_hotel_review_comments("create_edit_hotel_review_comments", 0, Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getHotelId()), Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getRevratId()),comment , Integer.parseInt(createuserid));
                    call.enqueue(new Callback<CommonOutputPojo>() {
                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                            if (response.body().getResponseCode() == 1) {


//                                Toast.makeText(context, "Comment created", Toast.LENGTH_SHORT).show();
                                int tot_comments = Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getTotalComments());
                                if (tot_comments == 0 || getallHotelReviewPojoArrayList.get(position).getTotalComments() == null) {

                                    holder.tv_view_all_comments.setText(R.string.view_one_comment);

                                } else if (tot_comments >= 1) {

                                    holder.tv_view_all_comments.setText("View all " + tot_comments + " comments");

                                }
                                holder.review_comment.setText("");
                                holder.txt_adapter_post.setVisibility(GONE);

                                Intent intent = new Intent(context, Reviews_comments_all_activity.class);
                                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                                intent.putExtra("userid", getallHotelReviewPojoArrayList.get(position).getUserId());
                                context.startActivity(intent);
                            }

                        }

                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        // Dishes to avoid text count


        int dish_avoid = getallHotelReviewPojoArrayList.get(position).getAvoidCount();

        if (dish_avoid == 0) {
            holder.ll_dish_to_avoid.setVisibility(View.GONE);
        }


        // Overallrating

        String rating = getallHotelReviewPojoArrayList.get(position).getFoodExprience();

        holder.tv_res_overall_rating.setText(rating);

        if (Integer.parseInt(rating) == 1) {
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_red);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

        } else if (Integer.parseInt(rating) == 2) {
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.ll_res_overall_rating.setBackgroundResource(R.drawable.review_rating_orange_bg);
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_orange_star);
        } else if (Integer.parseInt(rating) == 3) {
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
        } else if (Integer.parseInt(rating) == 4) {
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
        } else if (Integer.parseInt(rating) == 5) {
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
        }


        try {

            // Time delivery

            if (Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getCategoryType()) == 2) {

                String time_deleivery = getallHotelReviewPojoArrayList.get(position).getTotalTimedelivery();

                */
/*if(time_deleivery == null){

                    time_deleivery = "4";
                }*//*


                holder.ambi_or_timedelivery.setText(R.string.Delivery_new);
                holder.tv_ambience_rating.setText(time_deleivery);

                if (Integer.parseInt(time_deleivery) == 1) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_red);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

                } else if (Integer.parseInt(time_deleivery) == 2) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                } else if (Integer.parseInt(time_deleivery) == 3) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                } else if (Integer.parseInt(time_deleivery) == 4) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_star);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                } else if (Integer.parseInt(time_deleivery) == 5) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                }

            } else {

                // Ambiance

                String ambiance = getallHotelReviewPojoArrayList.get(position).getAmbiance();

                holder.tv_ambience_rating.setText(ambiance);

                if (Integer.parseInt(ambiance) == 1) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_red);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

                } else if (Integer.parseInt(ambiance) == 2) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                } else if (Integer.parseInt(ambiance) == 3) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                } else if (Integer.parseInt(ambiance) == 4) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_star);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                } else if (Integer.parseInt(ambiance) == 5) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        // Taste

        String taste = getallHotelReviewPojoArrayList.get(position).getTaste();

        holder.txt_taste_count.setText(taste);

        if (Integer.parseInt(taste) == 1) {
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_red);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

        } else if (Integer.parseInt(taste) == 2) {
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_orange_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
        } else if (Integer.parseInt(taste) == 3) {
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
        } else if (Integer.parseInt(taste) == 4) {
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
        } else if (Integer.parseInt(taste) == 5) {
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
        }


        // Value for money

        String vfm = getallHotelReviewPojoArrayList.get(position).getValueMoney();

        holder.tv_vfm_rating.setText(vfm);

        if (Integer.parseInt(vfm) == 1) {
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_red);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
        } else if (Integer.parseInt(vfm) == 2) {
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_orange_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
        } else if (Integer.parseInt(vfm) == 3) {
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
        } else if (Integer.parseInt(vfm) == 4) {
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
        } else if (Integer.parseInt(vfm) == 5) {
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
        }


        try {


            // Packaging

            if (Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getCategoryType()) == 2) {

                String package_rating = getallHotelReviewPojoArrayList.get(position).get_package();

                */
/*if(package_rating == null){

                    package_rating = "4";
                }*//*


                holder.tv_service_rating.setText(package_rating);
                holder.serv_or_package.setText(R.string.Package);

                if (Integer.parseInt(package_rating) == 1) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_red);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
                } else if (Integer.parseInt(package_rating) == 2) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                } else if (Integer.parseInt(package_rating) == 3) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                } else if (Integer.parseInt(package_rating) == 4) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_star);

                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                } else if (Integer.parseInt(package_rating) == 5) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                }


            } else {

                // Service

                String service = getallHotelReviewPojoArrayList.get(position).getService();

                holder.tv_service_rating.setText(service);
                if (Integer.parseInt(service) == 1) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_red);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
                } else if (Integer.parseInt(service) == 2) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                } else if (Integer.parseInt(service) == 3) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                } else if (Integer.parseInt(service) == 4) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_star);

                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                } else if (Integer.parseInt(service) == 5) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        String total_reviews = getallHotelReviewPojoArrayList.get(position).getTotalReview();
        String total_followers = getallHotelReviewPojoArrayList.get(position).getTotalFollowers();
*/
/*

        // Followers display

        if (Integer.parseInt(total_followers) != 0 && Integer.parseInt(total_followers) == 1) {
            holder.comma.setVisibility(VISIBLE);
            holder.txt_followers_count.setText(total_followers);
            holder.txt_followers_count.setVisibility(VISIBLE);
            holder.txt_followers.setVisibility(VISIBLE);
        } else if (Integer.parseInt(total_followers) != 0 && Integer.parseInt(total_followers) > 1) {

            holder.comma.setVisibility(VISIBLE);
            holder.txt_followers_count.setText(total_followers);
            holder.txt_followers.setText(R.string.followers);
            holder.txt_followers_count.setVisibility(VISIBLE);
            holder.txt_followers.setVisibility(VISIBLE);
        }

        if (parseInt(total_reviews) == 1) {
            holder.count.setText(total_reviews.concat(" Review"));
        } else if (parseInt(total_reviews) > 1) {
            holder.count.setText(total_reviews.concat(" Reviews"));
        }

*//*


        // Review comment

        if (getallHotelReviewPojoArrayList.get(position).getHotelReview().equalsIgnoreCase("0")) {
            holder.reviewcmmnt.setVisibility(GONE);
        } else {
            holder.reviewcmmnt.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getallHotelReviewPojoArrayList.get(position).getHotelReview()));

        }


        // Total likes

        final String total_likes = getallHotelReviewPojoArrayList.get(position).getTotalLikes();

        if (Integer.parseInt(total_likes) == 0) {

            holder.ll_likecomment.setVisibility(GONE);

        } else {

            holder.ll_likecomment.setVisibility(VISIBLE);

        }

        int userliked = Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getHtlRevLikes());

        if (userliked == 0) {

            getallHotelReviewPojoArrayList.get(position).setUserliked(false);
            holder.img_btn_review_like.setVisibility(View.VISIBLE);
            holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));

        } else if (userliked == 1) {

            getallHotelReviewPojoArrayList.get(position).setUserliked(true);
            holder.img_btn_review_like.setVisibility(View.VISIBLE);
            holder.ll_likecomment.setVisibility(VISIBLE);
            holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));

        }


        if (parseInt(total_likes) == 0 || total_likes.equalsIgnoreCase(null) || total_likes.equalsIgnoreCase("")) {

            holder.tv_like_count.setVisibility(View.GONE);
        }
        if (parseInt(total_likes) == 1) {
            holder.tv_like_count.setText(getallHotelReviewPojoArrayList.get(position).getTotalLikes().concat(" ").concat("like"));
            holder.tv_like_count.setVisibility(View.VISIBLE);


        } else if (parseInt(total_likes) > 1) {
            holder.tv_like_count.setText(getallHotelReviewPojoArrayList.get(position).getTotalLikes().concat(" ").concat("likes"));
            holder.tv_like_count.setVisibility(View.VISIBLE);
        }

        // Total comments

        String total_comments = getallHotelReviewPojoArrayList.get(position).getTotalComments();

        if (parseInt(total_comments) == 0 || total_likes.equalsIgnoreCase(null) || total_likes.equalsIgnoreCase("")) {

            holder.tv_view_all_comments.setVisibility(View.GONE);

        }

        if (parseInt(total_comments) == 1) {

            holder.tv_view_all_comments.setText(R.string.view_one_comment);
            holder.tv_view_all_comments.setVisibility(View.VISIBLE);

        } else if (parseInt(total_comments) > 1) {

            holder.tv_view_all_comments.setText("View all " + total_comments + " comments");
            holder.tv_view_all_comments.setVisibility(View.VISIBLE);
        }


        holder.tv_view_all_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, Reviews_comments_all_activity.class);
                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("userid", getallHotelReviewPojoArrayList.get(position).getUserId());
                context.startActivity(intent);
            }
        });


        holder.tv_like_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ReviewsLikesAllActivity.class);

                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("userid", getallHotelReviewPojoArrayList.get(position).getUserId());
                context.startActivity(intent);
            }
        });


        holder.img_btn_review_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(context, Reviews_comments_all_activity.class);

                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("userid", getallHotelReviewPojoArrayList.get(position).getUserId());
                context.startActivity(intent);

            }
        });


        holder.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getallHotelReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    Intent intent = new Intent(context, Profile.class);
                    intent.putExtra("created_by",getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);

                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by",getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);
                }

            }
        });


        holder.userlastname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getallHotelReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    Intent intent = new Intent(context, Profile.class);
                    intent.putExtra("created_by",getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);

                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by",getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);
                }

            }
        });

        holder.userphoto_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getallHotelReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    Intent intent = new Intent(context, Profile.class);
                    intent.putExtra("created_by",getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);

                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by",getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);
                }

            }
        });

        holder.review_user_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getallHotelReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    Intent intent = new Intent(context, Profile.class);
                    intent.putExtra("created_by",getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);

                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by",getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);
                }

            }
        });


        holder.rl_hotel_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, Review_in_detail_activity.class);
                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("f_name", getallHotelReviewPojoArrayList.get(position).getFirstName());
                intent.putExtra("l_name", getallHotelReviewPojoArrayList.get(position).getLastName());
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("timelineId", "0");
                context.startActivity(intent);

            }
        });

        holder.ll_delivery_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ReviewDeliveryDetails.class);
                intent.putExtra("catType", getallHotelReviewPojoArrayList.get(position).getCategoryType());
                intent.putExtra("delMode", getallHotelReviewPojoArrayList.get(position).getDelivery_mode());
                context.startActivity(intent);

            }
        });


        // Like functionality

        holder.img_btn_review_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);
                holder.ll_likecomment.setVisibility(VISIBLE);


                if (getallHotelReviewPojoArrayList.get(position).isUserliked()) {


                    try {


                        holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                        getallHotelReviewPojoArrayList.get(position).setUserliked(false);
                        getallHotelReviewPojoArrayList.get(position).setHtlRevLikes(String.valueOf(0));


                        int total_likes = Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getTotalLikes());

                        if (total_likes == 1) {

                            total_likes = total_likes - 1;
                            getallHotelReviewPojoArrayList.get(position).setTotalLikes(String.valueOf(total_likes));
                            holder.tv_like_count.setVisibility(View.GONE);


                        } else if (total_likes == 2) {

                            total_likes = total_likes - 1;
                            getallHotelReviewPojoArrayList.get(position).setTotalLikes(String.valueOf(total_likes));
                            holder.tv_like_count.setText(String.valueOf(total_likes) + " Like");
                            holder.tv_like_count.setVisibility(View.VISIBLE);

                        } else {

                            total_likes = total_likes - 1;
                            getallHotelReviewPojoArrayList.get(position).setTotalLikes(String.valueOf(total_likes));
                            holder.tv_like_count.setText(String.valueOf(total_likes) + " Likes");
                            holder.tv_like_count.setVisibility(View.VISIBLE);

                        }


                        apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        int userid = parseInt(Pref_storage.getDetail(context, "userId"));

                        call = apiInterface.create_hotel_review_likes("create_hotel_review_likes", parseInt(getallHotelReviewPojoArrayList.get(position).getHotelId()), parseInt(getallHotelReviewPojoArrayList.get(position).getRevratId()), 0, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                String responseStatus = response.body().getResponseMessage();
                                int resposecode = response.body().getResponseCode();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);
                                Log.e("resposecode", "resposecode->" + resposecode);

                                if (resposecode == 1) {

                                    //Success

                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    try {


                        holder.ll_likecomment.setVisibility(VISIBLE);
                        holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                        getallHotelReviewPojoArrayList.get(position).setUserliked(true);
                        getallHotelReviewPojoArrayList.get(position).setHtlRevLikes(String.valueOf(1));


                        int total_likes = Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getTotalLikes());

                        if (total_likes == 0) {

                            total_likes = total_likes + 1;
                            getallHotelReviewPojoArrayList.get(position).setTotalLikes(String.valueOf(total_likes));
                            holder.tv_like_count.setText(String.valueOf(total_likes) + " Like");
                            holder.tv_like_count.setVisibility(View.VISIBLE);

                        } else {

                            total_likes = total_likes + 1;
                            getallHotelReviewPojoArrayList.get(position).setTotalLikes(String.valueOf(total_likes));
                            holder.tv_like_count.setText(String.valueOf(total_likes) + " Likes");

                        }

                        apiInterface = ApiClient.getClient().create(ApiInterface.class);

                        int userid = parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<CommonOutputPojo> call = apiInterface.create_hotel_review_likes("create_hotel_review_likes", parseInt(getallHotelReviewPojoArrayList.get(position).getHotelId()), parseInt(getallHotelReviewPojoArrayList.get(position).getRevratId()), 1, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {

                                    //Success


                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }


            }
        });


        // More settings visibility


        if (getallHotelReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
            holder.img_view_more.setVisibility(VISIBLE);
        } else {
            holder.img_view_more.setVisibility(GONE);
        }

        // More settings

        holder.img_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder front_builder = new AlertDialog.Builder(context);
                front_builder.setItems(itemsothers, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (itemsothers[which].equals("Bucket list options")) {

                            dialog.dismiss();

                            Intent intent = new Intent(context, CreateBucketlistActivty.class);
                            intent.putExtra("f_name", getallHotelReviewPojoArrayList.get(position).getFirstName());
                            intent.putExtra("l_name", getallHotelReviewPojoArrayList.get(position).getLastName());
                            intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                            intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                            intent.putExtra("timelineId", "0");
                            context.startActivity(intent);

                        } else if (itemsothers[which].equals("Delete review")) {


                            new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Are you sure want to delete this reviewed hotel?")
                                    .setContentText("Won't be able to recover this hotel anymore!")
                                    .setConfirmText("Delete")
                                    .setCancelText("Cancel")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                            try {
                                                String createuserid = Pref_storage.getDetail(context, "userId");

                                                String review_id = getallHotelReviewPojoArrayList.get(position).getRevratId();

                                                Call<CommonOutputPojo> call = apiService.update_delete_hotel_review("update_delete_hotel_review", Integer.parseInt(review_id), Integer.parseInt(createuserid));
                                                call.enqueue(new Callback<CommonOutputPojo>() {
                                                    @Override
                                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                        if (response.body().getResponseCode() == 1) {


                                                            getallHotelReviewPojoArrayList.remove(position);
                                                            notifyItemRemoved(position);
                                                            notifyItemRangeChanged(position, getallHotelReviewPojoArrayList.size());



                                                            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                                    .setTitleText("Deleted!!")
                                                                    .setContentText("Your hotel review has been deleted.")
                                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                        @Override
                                                                        public void onClick(SweetAlertDialog sDialog) {
                                                                            sDialog.dismissWithAnimation();

                                                                           */
/* getallHotelReviewPojoArrayList.remove(position);
                                                                            notifyItemRemoved(position);
                                                                            notifyItemRangeChanged(position, getallHotelReviewPojoArrayList.size());*//*

                                                                        }
                                                                    })

                                                                    .show();



                                                        }

                                                    }

                                                    @Override
                                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                        //Error
                                                        Log.e("FailureError", "" + t.getMessage());
                                                    }
                                                });
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    })
                                    .show();

                            dialog.dismiss();
                        }
                    }
                });
                front_builder.show();
            }
        });


        // Adding bucketlist API

        holder.img_bucket_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);

                Intent intent = new Intent(context, CreateBucketlistActivty.class);
                intent.putExtra("f_name", getallHotelReviewPojoArrayList.get(position).getFirstName());
                intent.putExtra("l_name", getallHotelReviewPojoArrayList.get(position).getLastName());
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());

                intent.putExtra("timelineId", "0");

                context.startActivity(intent);


            }
        });


        // Load all image Activity

        holder.rl_view_more_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);
                Intent intent = new Intent(context, AllDishDisplayActivity.class);
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                intent.putExtra("cat_type", getallHotelReviewPojoArrayList.get(position).getCategoryType());
                context.startActivity(intent);


            }
        });


        // Load all image Activity

        holder.top_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);
                Intent intent = new Intent(context, AllDishDisplayActivity.class);
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                intent.putExtra("cat_type", getallHotelReviewPojoArrayList.get(position).getCategoryType());

                context.startActivity(intent);


            }
        });

        // Load all image Activity

        holder.rl_view_more_avoid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);
                Intent intent = new Intent(context, AllDishDisplayActivity.class);
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                context.startActivity(intent);

            }
        });

        // Load all image Activity

        holder.avoid_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);
                Intent intent = new Intent(context, AllDishDisplayActivity.class);
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                context.startActivity(intent);


            }
        });


        // API for top dish images


        if (!getallHotelReviewPojoArrayList.get(position).getTopdish().trim().equalsIgnoreCase("")) {



            reviewTopDishPojoArrayList.clear();

            ArrayList<ReviewTopDishPojo> reviewTopDishPojoList = new ArrayList<>();

            try{



                if(getallHotelReviewPojoArrayList.get(position).getTopCount()==0){


                }else {
                    for (int k = 0; k < getallHotelReviewPojoArrayList.get(position).getTopCount(); k++) {

                        String topdishimage = getallHotelReviewPojoArrayList.get(position).getTopdishimage().get(k).getImg();
                        String topdishimagename = getallHotelReviewPojoArrayList.get(position).getTopdishimage().get(k).getDishname();
                        reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimagename);
                        reviewTopDishPojoList.add(reviewTopDishPojo);


                    }
                }

            }catch (Exception e){e.printStackTrace();}

            String reviewId = getallHotelReviewPojoArrayList.get(position).getRevratId();
            String hotelName = getallHotelReviewPojoArrayList.get(position).getHotelName();
            String catType = getallHotelReviewPojoArrayList.get(position).getCategoryType();

            ReviewTopDishAdapter reviewTopDishAdapter = new ReviewTopDishAdapter(context, reviewTopDishPojoList, reviewId, hotelName, catType);
            LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);
            holder.rv_review_top_dish.setLayoutManager(llm);
//            Collections.reverse(reviewTopDishPojoArrayList);

            holder.rv_review_top_dish.setItemAnimator(new DefaultItemAnimator());
            holder.rv_review_top_dish.setAdapter(reviewTopDishAdapter);
            holder.rv_review_top_dish.scrollToPosition(reviewTopDishPojoList.size()-1);
            holder.rv_review_top_dish.setNestedScrollingEnabled(false);
            reviewTopDishAdapter.notifyDataSetChanged();
            holder.rl_top_dish.setVisibility(VISIBLE);

        }

        // Dish to avoid names API

        if (!getallHotelReviewPojoArrayList.get(position).getAvoiddish().trim().equalsIgnoreCase("")) {

            reviewAvoidDishPojoArrayList.clear();
            try{

                if(getallHotelReviewPojoArrayList.get(position).getAvoidCount()==0){


                }else {
                    for (int i = 0; i < getallHotelReviewPojoArrayList.get(position).getAvoidCount(); i++) {

                        String avoiddishimage = getallHotelReviewPojoArrayList.get(position).getAvoiddishimage().get(i).getImg();
                        String avoiddishimagename = getallHotelReviewPojoArrayList.get(position).getAvoiddishimage().get(i).getDishname();

                        reviewAvoidDishPojo = new ReviewAvoidDishPojo(avoiddishimage, avoiddishimagename);
                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);

                    }
                }
            }catch (Exception e){e.printStackTrace();}



            String reviewId = getallHotelReviewPojoArrayList.get(position).getRevratId();
            String hotelName = getallHotelReviewPojoArrayList.get(position).getHotelName();
            String catType = getallHotelReviewPojoArrayList.get(position).getCategoryType();


            reviewWorstDishAdapter = new ReviewAvoidDishAdapter(context, reviewAvoidDishPojoArrayList, reviewId, hotelName, catType);

            LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);
            holder.rv_review_avoid_dish.setLayoutManager(llm);
//            Collections.reverse(reviewAvoidDishPojoArrayList);
            holder.rv_review_avoid_dish.setItemAnimator(new DefaultItemAnimator());
            holder.rv_review_avoid_dish.setAdapter(reviewWorstDishAdapter);
            holder.rv_review_avoid_dish.scrollToPosition(reviewAvoidDishPojoArrayList.size()-1);
            holder.rv_review_avoid_dish.setNestedScrollingEnabled(false);
            reviewWorstDishAdapter.notifyDataSetChanged();
            holder.rl_avoid_dish.setVisibility(VISIBLE);

        }


        // API for ambience images

        int ambi_image_count = getallHotelReviewPojoArrayList.get(position).getAmbiImageCount();

        if (ambi_image_count != 0) {

            reviewAmbiImagePojoArrayList.clear();

            for (int m = 0; m < getallHotelReviewPojoArrayList.get(position).getAmbiImageCount(); m++) {

                reviewAmbiImagePojo = new ReviewAmbiImagePojo(getallHotelReviewPojoArrayList.get(position).getAmbiImage().get(m).getImg());
                reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);

            }

            String reviewId = getallHotelReviewPojoArrayList.get(position).getRevratId();
            String hotelName = getallHotelReviewPojoArrayList.get(position).getHotelName();
            String catType = getallHotelReviewPojoArrayList.get(position).getCategoryType();

            ambianceImagesAdapter = new ReviewAmbienceAdapter(context, reviewAmbiImagePojoArrayList, reviewId, hotelName, catType);

            LinearLayoutManager llm3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);

            holder.review_photos_recylerview.setLayoutManager(llm3);
//            Collections.reverse(reviewAmbiImagePojoArrayList);
            holder.review_photos_recylerview.setItemAnimator(new DefaultItemAnimator());
            holder.review_photos_recylerview.setAdapter(ambianceImagesAdapter);
            holder.review_photos_recylerview.setNestedScrollingEnabled(false);
            ambianceImagesAdapter.notifyDataSetChanged();
            holder.ll_view_more.setVisibility(VISIBLE);
            holder.ll_view_more.setVisibility(VISIBLE);


            if (reviewAmbiImagePojoArrayList.size() == 0) {
                holder.ambi_viewpager.setVisibility(View.GONE);
                holder.rl_ambiance_dot.setVisibility(View.GONE);
                holder.title_ambience.setVisibility(GONE);
                holder.ll_ambi_more.setVisibility(GONE);
                holder.ll_view_more.setVisibility(GONE);
                holder.review_photos_recylerview.setVisibility(GONE);
            } else {


            }

        }


        // Hotel Images

        holder.ll_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.ll_ambi_more.setVisibility(VISIBLE);
                holder.ll_view_more.setVisibility(GONE);

            }
        });

        holder.layout_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.ll_ambi_more.setVisibility(VISIBLE);
                holder.ll_view_more.setVisibility(GONE);

            }
        });


    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    //Hide Keyboard

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }


    // Internet Check

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    private void getFollowers(final int userId) {

        if (isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<GetFollowersOutput> call = apiService.getFollower("get_follower", userId);

                call.enqueue(new Callback<GetFollowersOutput>() {
                    @Override
                    public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {
                            getFollowersDataList.clear();

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                if (response.body().getData().get(i).getFollowerId() == null) {

                                } else {
                                    int userid = response.body().getData().get(i).getUserid();
                                    String followerId = response.body().getData().get(i).getFollowerId();
                                    String firstName = response.body().getData().get(i).getFirstName();
                                    String lastName = response.body().getData().get(i).getLastName();
                                    String userPhoto = response.body().getData().get(i).getPicture();

                                    if (!followerId.contains(String.valueOf(userid))) {

                                        GetFollowersData getFollowersData = new GetFollowersData(userid, followerId, firstName, lastName, userPhoto);
                                        getFollowersDataList.add(getFollowersData);
                                    }
                                }


                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

    }


    // getFollowers

    public void getfriendslist(View view, int position) {

        if (((CheckBox) view).isChecked()) {

            if (friendsList.contains(Integer.valueOf(getFollowersDataList.get(position).getFollowerId()))) {

                return;

            } else {

                friendsList.add(Integer.valueOf(getFollowersDataList.get(position).getFollowerId()));

            }


        } else {

            friendsList.remove(Integer.valueOf(getFollowersDataList.get(position).getFollowerId()));

        }

    }


    // Interface friends list

    @Override
    public int getItemCount() {

        return getallHotelReviewPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

        Log.e(TAG, "onScrollChange: " + v.getVerticalScrollbarPosition());

    }

    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity c;
        public Dialog d;

        TextView txt_edit_review, txt_delete_review;

        CustomDialogClass(Context a) {
            super(a);
            // TODO Auto-generated constructor stub
            context = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.edit_delete_review_layout);


            txt_edit_review = (TextView) findViewById(R.id.txt_edit_review);
            txt_delete_review = (TextView) findViewById(R.id.txt_delete_review);

            txt_edit_review.setOnClickListener(this);
            txt_delete_review.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_edit_review:
                    dismiss();
                    break;
                case R.id.txt_delete_review:

         */
/*           new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to delete this reviewed hotel?")
                            .setContentText("Won't be able to recover this hotel anymore!")
                            .setConfirmText("Delete")
                            .setCancelText("Cancel")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                    try {
                                        String createuserid = Pref_storage.getDetail(context, "userId");

                                        String review_id = getallHotelReviewPojoArrayList.get(position).getRevratId();

                                        Call<CommonOutputPojo> call = apiService.update_delete_hotel_review("update_delete_hotel_review", Integer.parseInt(review_id), Integer.parseInt(createuserid));
                                        call.enqueue(new Callback<CommonOutputPojo>() {
                                            @Override
                                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                if (response.body().getResponseCode() == 1) {
                                                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                            .setTitleText("Deleted!!")
                                                            .setContentText("Your comment has been deleted.")

                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                @Override
                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                    sDialog.dismissWithAnimation();

                                                                    getallHotelReviewPojoArrayList.remove(position);
                                                                    notifyItemRemoved(position);
                                                                    notifyItemRangeChanged(position, getallHotelReviewPojoArrayList.size());
                                                                }
                                                            })
                                                            .show();


                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                //Error
                                                Log.e("FailureError", "" + t.getMessage());
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                }
                            })
                            .show();
*//*


                    dismiss();
                    break;


                default:
                    break;
            }
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        // New design view holder


        //        EditText review_comment;
        EmojiconEditText review_comment;

        ViewPager ambi_viewpager;

        ExpandableTextView reviewcmmnt;

        CircleIndicator ambi_indicator;

        RecyclerView review_photos_recylerview, rv_review_top_dish, rv_review_avoid_dish;

        ImageView userphoto_profile, review_user_photo, img_overall_rating_star,
                img_ambience_star, img_taste_star, img_vfm_star, img_service_star;

        RelativeLayout rl_overall_review, rl_ambiance_dot, rl_hotel_details, rl_avoid_dish, rl_top_dish, rl_view_more_top, rl_view_more_avoid;

        LinearLayout ll_ambi_more, ll_view_more, ll_dish_to_avoid, ll_likecomment, ll_res_overall_rating, ll_ambience_rating, ll_taste_rating,
                ll_value_of_money_rating, ll_service_rating, ll_delivery_mode,ll_top_dish;

        ImageButton img_btn_review_like, layout_more, img_btn_review_comment, img_bucket_list, img_view_more, avoid_more, top_more;

        // TextView

        TextView username, txt_mode_delivery, userlastname, count, count_post, comma, txt_followers_count, txt_followers, serv_or_package,
                txt_created_on, tv_restaurant_name, txt_hotel_address, tv_res_overall_rating, tv_ambience_rating, ambi_or_timedelivery,
                txt_taste_count, tv_vfm_rating, tv_service_rating, title_ambience, tv_view_more, tv_top_dishes,
                tv_dishes_to_avoid, title_dish_avoid, title_top_dish, tv_show_less, txt_review_caption,
                tv_like_count, tv_view_all_comments, txt_adapter_post;


        public MyViewHolder(View itemView) {
            super(itemView);


            username = (TextView) itemView.findViewById(R.id.username);
            txt_mode_delivery = (TextView) itemView.findViewById(R.id.txt_mode_delivery);
            userlastname = (TextView) itemView.findViewById(R.id.userlastname);
            count = (TextView) itemView.findViewById(R.id.count);
            count_post = (TextView) itemView.findViewById(R.id.count_post);
            comma = (TextView) itemView.findViewById(R.id.comma);
            txt_followers_count = (TextView) itemView.findViewById(R.id.txt_followers_count);
            txt_followers = (TextView) itemView.findViewById(R.id.txt_followers);
            txt_created_on = (TextView) itemView.findViewById(R.id.txt_created_on);
            tv_restaurant_name = (TextView) itemView.findViewById(R.id.tv_restaurant_name);
            txt_hotel_address = (TextView) itemView.findViewById(R.id.txt_hotel_address);
            tv_res_overall_rating = (TextView) itemView.findViewById(R.id.tv_res_overall_rating);
            tv_ambience_rating = (TextView) itemView.findViewById(R.id.tv_ambience_rating);
            txt_taste_count = (TextView) itemView.findViewById(R.id.txt_taste_count);
            tv_ambience_rating = (TextView) itemView.findViewById(R.id.tv_ambience_rating);
            ambi_or_timedelivery = (TextView) itemView.findViewById(R.id.ambi_or_timedelivery);
            serv_or_package = (TextView) itemView.findViewById(R.id.serv_or_package);
            tv_vfm_rating = (TextView) itemView.findViewById(R.id.tv_vfm_rating);
            tv_service_rating = (TextView) itemView.findViewById(R.id.tv_service_rating);
            tv_view_more = (TextView) itemView.findViewById(R.id.tv_view_more);
            title_ambience = (TextView) itemView.findViewById(R.id.title_ambience);
            tv_top_dishes = (TextView) itemView.findViewById(R.id.tv_top_dishes);
            tv_dishes_to_avoid = (TextView) itemView.findViewById(R.id.tv_dishes_to_avoid);
            title_dish_avoid = (TextView) itemView.findViewById(R.id.title_dish_avoid);
            title_top_dish = (TextView) itemView.findViewById(R.id.title_top_dish);
            tv_show_less = (TextView) itemView.findViewById(R.id.tv_show_less);
            tv_like_count = (TextView) itemView.findViewById(R.id.tv_like_count);
            tv_view_all_comments = (TextView) itemView.findViewById(R.id.tv_view_all_comments);
            txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);


            txt_hotel_address = (TextView) itemView.findViewById(R.id.txt_hotel_address);


            ambi_viewpager = (ViewPager) itemView.findViewById(R.id.ambi_viewpager);

            ambi_indicator = (CircleIndicator) itemView.findViewById(R.id.ambi_indicator);
            review_comment = (EmojiconEditText) itemView.findViewById(R.id.review_comment);
            reviewcmmnt = (ExpandableTextView) itemView.findViewById(R.id.hotel_revieww_description);


            txt_review_caption = (TextView) itemView.findViewById(R.id.tv_review_caption);

            //Imageview

            userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);

            img_overall_rating_star = (ImageView) itemView.findViewById(R.id.img_overall_rating_star);
            img_ambience_star = (ImageView) itemView.findViewById(R.id.img_ambience_star);
            img_taste_star = (ImageView) itemView.findViewById(R.id.img_taste_star);
            img_vfm_star = (ImageView) itemView.findViewById(R.id.img_vfm_star);
            img_service_star = (ImageView) itemView.findViewById(R.id.img_service_star);
            userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);
            review_user_photo = (ImageView) itemView.findViewById(R.id.review_user_photo);

            //ImageButton
            img_btn_review_like = (ImageButton) itemView.findViewById(R.id.img_btn_review_like);
            img_btn_review_comment = (ImageButton) itemView.findViewById(R.id.img_btn_review_comment);
            img_view_more = (ImageButton) itemView.findViewById(R.id.img_view_more);
            img_bucket_list = (ImageButton) itemView.findViewById(R.id.img_bucket_list);
            top_more = (ImageButton) itemView.findViewById(R.id.top_more);
            avoid_more = (ImageButton) itemView.findViewById(R.id.avoid_more);
            layout_more = (ImageButton) itemView.findViewById(R.id.layout_more);


            rl_overall_review = (RelativeLayout) itemView.findViewById(R.id.rl_overall_review);
            ll_likecomment = (LinearLayout) itemView.findViewById(R.id.ll_likecomment);
            ll_res_overall_rating = (LinearLayout) itemView.findViewById(R.id.ll_res_overall_rating);
            ll_dish_to_avoid = (LinearLayout) itemView.findViewById(R.id.ll_dish_to_avoid);
            ll_ambience_rating = (LinearLayout) itemView.findViewById(R.id.ll_ambience_rating);
            ll_taste_rating = (LinearLayout) itemView.findViewById(R.id.ll_taste_rating);
            ll_value_of_money_rating = (LinearLayout) itemView.findViewById(R.id.ll_value_of_money_rating);
            ll_service_rating = (LinearLayout) itemView.findViewById(R.id.ll_service_rating);
            ll_view_more = (LinearLayout) itemView.findViewById(R.id.ll_view_more);
            ll_ambi_more = (LinearLayout) itemView.findViewById(R.id.ll_ambi_more);
            ll_delivery_mode = (LinearLayout) itemView.findViewById(R.id.ll_delivery_mode);
            ll_top_dish = (LinearLayout) itemView.findViewById(R.id.ll_top_dish);


            review_photos_recylerview = (RecyclerView) itemView.findViewById(R.id.review_photos_recylerview);
            rv_review_top_dish = (RecyclerView) itemView.findViewById(R.id.rv_review_top_dish);
            rv_review_avoid_dish = (RecyclerView) itemView.findViewById(R.id.rv_review_avoid_dish);

            rl_ambiance_dot = (RelativeLayout) itemView.findViewById(R.id.rl_ambiance_dot);
            rl_hotel_details = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_details);
            rl_avoid_dish = (RelativeLayout) itemView.findViewById(R.id.rl_avoid_dish);
            rl_top_dish = (RelativeLayout) itemView.findViewById(R.id.rl_top_dish);
            rl_view_more_top = (RelativeLayout) itemView.findViewById(R.id.rl_view_more_top);

            rl_view_more_avoid = (RelativeLayout) itemView.findViewById(R.id.rl_view_more_avoid);
        }


    }


}
*/
