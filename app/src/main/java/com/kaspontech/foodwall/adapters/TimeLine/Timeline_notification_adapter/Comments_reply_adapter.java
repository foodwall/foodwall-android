package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Comments_activity;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_reply_all_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//

public class Comments_reply_adapter extends RecyclerView.Adapter<Comments_reply_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     **/
    Context context;
    /**
     * Reply all pojo
     **/

    Get_reply_all_pojo getReplyAllPojo;

    /**
     * Reply all array list
     **/
    private ArrayList<Get_reply_all_pojo> gettingreplyalllist = new ArrayList<>();

    /**
     * Relative layout
     **/
    public RelativeLayout total_reply_layout;

    /**
     * Shared Preference
     **/

    private Pref_storage pref_storage;

    /**
     * Interface for comments activity
     **/
    Comments_activity commentsActivity;

    /**
     * Interface for comments adapter
     **/
    Comments_adapter commentsAdapter;


    /**
     *Constructor
     **/
    public Comments_reply_adapter(Context c, ArrayList<Get_reply_all_pojo> gettinglist) {
        this.context = c;
        this.gettingreplyalllist = gettinglist;
        this.commentsActivity = (Comments_activity) context;

    }

    @NonNull
    @Override
    public Comments_reply_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_reply_comments, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Comments_reply_adapter.MyViewHolder vh = new Comments_reply_adapter.MyViewHolder(v, commentsActivity);

        /*Shared preference initialization*/

        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Comments_reply_adapter.MyViewHolder holder, final int position) {
        /*Pojo initialization*/
        getReplyAllPojo = gettingreplyalllist.get(position);

        /*User first and last name setting*/
        String firstname = gettingreplyalllist.get(position).getFirstName().replace("\"", "");
        String lastname = gettingreplyalllist.get(position).getLastName().replace("\"", "");

        String username = firstname + " " + lastname;
        holder.txt_username_reply.setText(username);
        holder.txt_username_reply.setTextColor(Color.parseColor("#000000"));

        /*User comments displaying*/

        holder.txt_comments_reply.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettingreplyalllist.get(position).getTlCmmtReply().replace("\"", "")));

        holder.txt_comments_reply.setTextColor(Color.parseColor("#000000"));


//Loading image url

        Utility.picassoImageLoader(gettingreplyalllist.get(position).getPicture(),
                1,holder.img_comment_reply, context);




//Posted date functionality
        String createdOn = gettingreplyalllist.get(position).getCreatedOn();

        Utility.setTimeStamp(createdOn, holder.txt_ago_reply);




        String userid = Pref_storage.getDetail(context, "userId");

        /*Edit and delete visibility condition*/
        if(gettingreplyalllist.get(position).getUserId().equals(userid)){

            holder.txt_comment_edit.setVisibility(View.VISIBLE);
            holder.del_reply_comment.setVisibility(View.VISIBLE);

        }else{

            holder.txt_comment_edit.setVisibility(View.GONE);
            holder.del_reply_comment.setVisibility(View.GONE);

        }

        /*Edit click listener and interface to comments activity*/
        holder.txt_comment_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                commentsActivity.clickedcommentposition_reply_edit(v, gettingreplyalllist.get(position).getTlCmmtReply(), Integer.parseInt(gettingreplyalllist.get(position).getReplyId()), Integer.parseInt(gettingreplyalllist.get(position).getCmmtTlId()));
                commentsActivity.clickedfor_replyedit(v, 44);
                commentsActivity.clickedcommentposition(v, Integer.parseInt(gettingreplyalllist.get(position).getCmmtTlId()));
                commentsActivity.clickedcomment_userid_position(v, Integer.parseInt(gettingreplyalllist.get(position).getUserId()));
                commentsActivity.clickedfor_replyid(v, Integer.parseInt(gettingreplyalllist.get(position).getReplyId()));


            }
        });

        /*Delete click listener and interface to comments activity*/

        holder.del_reply_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                commentsActivity.clickedfordelete();

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this comment?")
                        .setContentText("Won't be able to recover this comment anymore!")
                        .setConfirmText("Delete")
                        .setCancelText("Cancel")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                try {
                                    String createuserid = gettingreplyalllist.get(position).getUserId();
                                    String timeline_commentid = gettingreplyalllist.get(position).getCmmtTlId();
                                    String commentid = gettingreplyalllist.get(position).getReplyId();

                                    Call<CreateUserPojoOutput> call = apiService.create_delete_timeline_comments_reply("create_delete_timeline_comments_reply", Integer.parseInt(commentid), Integer.parseInt(timeline_commentid), Integer.parseInt(createuserid));
                                    call.enqueue(new Callback<CreateUserPojoOutput>() {
                                        @Override
                                        public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                                            if (response.body().getResponseCode() == 1) {
                                                /*Delete position*/

                                                removeAt(position);

                                                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Deleted!!")
//                                                        .setContentText("Your comment has been deleted.")

                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                            }
                                                        })
                                                        .show();


                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "" + t.getMessage());
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }
                        })
                        .show();


            }
        });


    }
    /*Delete position*/

    private void removeAt(int position) {
        gettingreplyalllist.remove(position);
        notifyDataSetChanged();
        /*notifyItemRemoved(position);
        notifyItemRangeChanged(position, gettingreplyalllist.size());*/
    }

    @Override
    public int getItemCount() {


        if (gettingreplyalllist.isEmpty()) {

            commentsActivity.check_timeline_reply_size(gettingreplyalllist.size());
        } else {

        }


        return gettingreplyalllist.size();

    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    /*View holder*/


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view_reply;
        ImageView img_comment_reply;
        ImageButton del_reply_comment;
        LinearLayout rl_reply_comment_layout;

        TextView txt_username_reply, txt_comment_edit,
                txt_ago_reply, txt_reply_like, txt_comment_reply, txt_comments_reply, txt_view_all_replies, txt_replies_count;

        /* Interface activity */

        Comments_activity comments_activity;

        public MyViewHolder(View itemView, Comments_activity commentsActivity) {
            super(itemView);

            this.comments_activity = commentsActivity;

            view_reply = (View) itemView.findViewById(R.id.view_reply);

            /*Textview Initialization*/

            txt_ago_reply = (TextView) itemView.findViewById(R.id.txt_ago_reply);
            txt_reply_like = (TextView) itemView.findViewById(R.id.txt_reply_like);
            txt_comment_edit = (TextView) itemView.findViewById(R.id.txt_comment_edit);
            txt_replies_count = (TextView) itemView.findViewById(R.id.txt_replies_count);
            txt_comment_reply = (TextView) itemView.findViewById(R.id.txt_comment_reply);
            txt_username_reply = (TextView) itemView.findViewById(R.id.txt_username_reply);
            txt_comments_reply = (TextView) itemView.findViewById(R.id.txt_comments_reply);
            txt_view_all_replies = (TextView) itemView.findViewById(R.id.txt_view_all_replies);

            /*Imageview for reply Initialization*/
            img_comment_reply = (ImageView) itemView.findViewById(R.id.img_comment_reply);

            /*ImageButton for deleted reply Initialization*/
            del_reply_comment = (ImageButton) itemView.findViewById(R.id.del_reply_comment);

            /*Total reply relative layout initialization*/
            total_reply_layout = (RelativeLayout) itemView.findViewById(R.id.total_reply_layout);

            /*Total reply linear layout initialization*/
            rl_reply_comment_layout = (LinearLayout) itemView.findViewById(R.id.rl_reply_comment_layout);


        }


        @Override
        public void onClick(View v) {

        }

    }


}


