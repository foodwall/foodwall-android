package com.kaspontech.foodwall.adapters.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_grp_member_input_pojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

//

public class Group_member_adapter extends RecyclerView.Adapter<Group_member_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener  {

    /**
     * Context
     **/
    private Context context;
    /**
     * Group member input pojo arraylist
     **/
    private ArrayList<Get_grp_member_input_pojo> getFollowerlistPojoArrayList = new ArrayList<>();

    /**
     * Group member input pojo
     **/
    private Get_grp_member_input_pojo getGrpMemberInputPojo;

    /**
     * Chat group user profile activity
     **/

    com.kaspontech.foodwall.chatpackage.chatGroupUserProfileActivity chatGroupUserProfileActivity;

    public Group_member_adapter(Context c, ArrayList<Get_grp_member_input_pojo> gettinglist) {
        this.context = c;
        this.getFollowerlistPojoArrayList = gettinglist;
        this.chatGroupUserProfileActivity=(com.kaspontech.foodwall.chatpackage.chatGroupUserProfileActivity) context;
    }

    @NonNull
    @Override
    public Group_member_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_chat_followerlist, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Group_member_adapter.MyViewHolder vh = new Group_member_adapter.MyViewHolder(v,chatGroupUserProfileActivity);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Group_member_adapter.MyViewHolder holder, final int position) {


        getGrpMemberInputPojo = getFollowerlistPojoArrayList.get(position);

        /*User name displaying*/
        holder.txtUsername.setText(getFollowerlistPojoArrayList.get(position).getFirstName());

        holder.txtUsername.setTextColor(Color.parseColor("#000000"));

        holder.txtUserlastname.setText(getFollowerlistPojoArrayList.get(position).getLastName());
        holder.txtUserlastname.setTextColor(Color.parseColor("#000000"));

        String fullname= getFollowerlistPojoArrayList.get(position).getFirstName().concat(" ").
                concat(getFollowerlistPojoArrayList.get(position).getLastName());

        holder.txtFullnameUser.setText(fullname);

        holder.txtFollowing.setText(R.string.select);

        holder.rlayoutFollowing.setVisibility(View.GONE);



      /*  //Exit group text view interface
        if(getFollowerlistPojoArrayList.get(position).getFriendid().equalsIgnoreCase(Pref_storage.getDetail(context,"userId"))){
            chatGroupUserProfileActivity.checkselfid(getFollowerlistPojoArrayList.get(position).getFriendid());
        }else {
            chatGroupUserProfileActivity.checkselfid(getFollowerlistPojoArrayList.get(position).getFriendid());
        }*/




        /*User image displaying*/

        Utility.picassoImageLoader(getFollowerlistPojoArrayList.get(position).getPicture(),0,holder.imgFollowing, context);



        /*Follwing layout on click listener*/
        holder.rlFollowerLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Pref_storage.setDetail(context,"clickedgrpuser_id",getFollowerlistPojoArrayList.get(position).getFriendid());

                if(getFollowerlistPojoArrayList.get(position).getFriendid().equalsIgnoreCase(Pref_storage.getDetail(context,"userId"))){
                    Intent intent= new Intent(context, Profile.class);
                    context.startActivity(intent);
                }else {
                    Intent intent= new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by",getFollowerlistPojoArrayList.get(position).getFriendid());
                    context.startActivity(intent);
                }

            }
        });


      /*  holder.rlayoutFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String friendid= getFollowerlistPojoArrayList.get(position).getFollower_id();
                String username= getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                        concat(getFollowerlistPojoArrayList.get(position).getLast_name());

                holder.imgCheckedFollower.setVisibility(View.VISIBLE);


                Intent intent = new Intent(context, Create_new_chat_single.class);
                intent.putExtra("friendid",getFollowerlistPojoArrayList.get(position).getFollower_id());
                intent.putExtra("picture",getFollowerlistPojoArrayList.get(position).getPicture());
                intent.putExtra("username",getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                        concat(getFollowerlistPojoArrayList.get(position).getLast_name()));
                context.startActivity(intent);
                ((AppCompatActivity)context).finish();

            }
        });
*/




    }






    @Override
    public int getItemCount() {

        return getFollowerlistPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtUsername, txtUserlastname, txtFullnameUser, txtFollowing;
        RelativeLayout rlFollowing, rlayoutFollowing, rlFollowerLay, rlFollowerAyout;

        ImageView imgFollowing, imgCheckedFollower;

        /*Interface listener*/

       com.kaspontech.foodwall.chatpackage.chatGroupUserProfileActivity chatGroupUserProfileActivity;


        public MyViewHolder(View itemView, com.kaspontech.foodwall.chatpackage.chatGroupUserProfileActivity chat_group_user_profile_activity1) {
            super(itemView);

            this.chatGroupUserProfileActivity = chat_group_user_profile_activity1;

            txtUsername = (TextView) itemView.findViewById(R.id.txt_username);
            txtUserlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txtFullnameUser = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            txtFollowing = (TextView) itemView.findViewById(R.id.txt_following);
            imgFollowing = (ImageView) itemView.findViewById(R.id.img_following);
            imgCheckedFollower = (ImageView) itemView.findViewById(R.id.img_checked_follower);

            rlFollowerLay = (RelativeLayout) itemView.findViewById(R.id.rl_follower_lay);
            rlayoutFollowing = (RelativeLayout) itemView.findViewById(R.id.rlayout_following);
            rlFollowerAyout = (RelativeLayout) itemView.findViewById(R.id.rl_follower_ayout);


        }








    }




}
