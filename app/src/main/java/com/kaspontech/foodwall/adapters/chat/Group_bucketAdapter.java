package com.kaspontech.foodwall.adapters.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.bucketlistpackage.GetmyBucketInputPojo;
 import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.reviewpackage.Image_load_activity;

import java.util.ArrayList;

public class Group_bucketAdapter extends RecyclerView.Adapter<Group_bucketAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener  {

    /**
     * Context
     **/
    private Context context;
    /**
     * Bucket input arraylist
     **/
    private ArrayList<GetmyBucketInputPojo> getmyBucketInputPojoArrayList = new ArrayList<>();
    /**
     * Bucket input pojo
     **/
    private GetmyBucketInputPojo getmyBucketInputPojo;



    public Group_bucketAdapter(Context c, ArrayList<GetmyBucketInputPojo> gettinglist) {
        this.context = c;
        this.getmyBucketInputPojoArrayList = gettinglist;
    }

    @NonNull
    @Override
    public Group_bucketAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_image_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Group_bucketAdapter.MyViewHolder vh = new Group_bucketAdapter.MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Group_bucketAdapter.MyViewHolder holder, final int position) {


        getmyBucketInputPojo = getmyBucketInputPojoArrayList.get(position);


        /*Displaying hotel name */
        if(getmyBucketInputPojoArrayList.get(position).getHotelName()==null){
            removeAt(position);
        }else {
            holder.txtHotelname.setText(getmyBucketInputPojoArrayList.get(position).getHotelName());
            holder.txtHotelname.setTextColor(Color.parseColor("#000000"));
        }


          //Hotel Image displayij=ng

        if(getmyBucketInputPojoArrayList.get(position).getPhotoReference()==null){
            removeAt(position);
        }else {
            Image_load_activity.loadGooglePhoto(context,holder.imageReview,getmyBucketInputPojoArrayList.get(position).getPhotoReference());


        }





//

      /*  if(getmyBucketInputPojo.getBucketId().contains(getmyBucketInputPojo.getBucketId())){
            deletePosition(position);
        }
*/


      /*Hotel layout on click listener*/
      holder.rlHotelDatas.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

             // context.startActivity(new Intent(context, MyBucketlistActivity.class));
              Intent intent = new Intent(context, Home.class);
              intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
              intent.putExtra("redirect", "profile");
              context.startActivity(intent);

          }
      });

      /*  holder.rlayoutFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String friendid= getFollowerlistPojoArrayList.get(position).getFollower_id();
                String username= getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                        concat(getFollowerlistPojoArrayList.get(position).getLast_name());

                holder.imgCheckedFollower.setVisibility(View.VISIBLE);


                Intent intent = new Intent(context, Create_new_chat_single.class);
                intent.putExtra("friendid",getFollowerlistPojoArrayList.get(position).getFollower_id());
                intent.putExtra("picture",getFollowerlistPojoArrayList.get(position).getPicture());
                intent.putExtra("username",getFollowerlistPojoArrayList.get(position).getFirst_name().concat(" ").
                        concat(getFollowerlistPojoArrayList.get(position).getLast_name()));
                context.startActivity(intent);
                ((AppCompatActivity)context).finish();

            }
        });
*/




    }


    /*Remove position*/
    public void removeAt(int position) {
        getmyBucketInputPojoArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getmyBucketInputPojoArrayList.size());
    }


    @Override
    public int getItemCount() {

        return getmyBucketInputPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtHotelname;
        RelativeLayout rlHotelDatas;

        ImageView imageReview;




        public MyViewHolder(View itemView) {
            super(itemView);

            txtHotelname = (TextView) itemView.findViewById(R.id.txt_image);

            imageReview = (ImageView) itemView.findViewById(R.id.image_review);


            rlHotelDatas = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_datas);



        }








    }




}
