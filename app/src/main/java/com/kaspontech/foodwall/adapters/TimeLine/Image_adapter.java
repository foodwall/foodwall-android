package com.kaspontech.foodwall.adapters.TimeLine;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.kaspontech.foodwall.modelclasses.TimeLinePojo.New_Getting_timeline_all_pojo;
import com.kaspontech.foodwall.R;

import java.util.ArrayList;

/**
 * Created by vishnukm on 9/3/18.
 */

public class Image_adapter extends BaseAdapter {

    private Context mContext;
    public static ArrayList<New_Getting_timeline_all_pojo> gettingtimelinealllist = new ArrayList<>();
    private New_Getting_timeline_all_pojo newGettingTimelineAllPojo;
    ArrayList<String> f = new ArrayList<String>();// list of file paths
      java.io.File[] listFile;

      private String[] filepath;
	private String[] filename;
	private static android.view.LayoutInflater inflater = null;


//    public Image_adapter(Context activity, ArrayList<New_Getting_timeline_all_pojo> gettinglist) {
//        this.mContext = activity;
//
//        gettingtimelinealllist = gettinglist;
//
//    }
   public Image_adapter(Context c, String[] fpath, String[] fname) {
        this.mContext = c;
          filepath = fpath;
		filename = fname;
		inflater = (android.view.LayoutInflater) c
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }





    // Keep all Images in array
    public Integer[] mThumbIds = {


            R.drawable.oldman,
            R.drawable.balaji,
            R.drawable.virat,
            R.drawable.burger,


    };


    public void getFromSdcard()
    {
        java.io.File file= new java.io.File(android.os.Environment.getExternalStorageDirectory(),"Pictures");

        if (file.isDirectory())
        {
            listFile = file.listFiles();


            for (int i = 0; i < listFile.length; i++)
            {

                f.add(listFile[i].getAbsolutePath());

            }
        }
    }
    // Constructor
    public Image_adapter(Context c) {
        mContext = c;
    }

    @Override
    public int getCount() {
        return filepath.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
//        getFromSdcard();

//        android.graphics.Bitmap myBitmap = android.graphics.BitmapFactory.decodeFile(f.get(position));

        android.graphics.Bitmap bmp = android.graphics.BitmapFactory.decodeFile(filepath[position]);
        imageView.setImageBitmap(bmp);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(250, 250));
        imageView.setPadding(5, 3, 5, 3);
        return imageView;
    }

}

