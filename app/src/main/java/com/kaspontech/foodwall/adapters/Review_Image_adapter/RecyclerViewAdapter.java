package com.kaspontech.foodwall.adapters.Review_Image_adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Getall_restaurant_details_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.OpeningHours_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Restaurant_output_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.StoreModel;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.REST_API.API_interface_restaurant;
import com.kaspontech.foodwall.reviewpackage.Image_load_activity;
import com.kaspontech.foodwall.reviewpackage.View_restaurant_details;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {


    private List<Getall_restaurant_details_pojo> stLstStores;
    private     List<OpeningHours_pojo> openingHoursPojoList;
    private List<StoreModel> models;
    Context context;
    Restaurant_output_pojo restaurantOutputPojo;
    API_interface_restaurant apiService;
    OpeningHours_pojo openingHoursPojo;

    Bitmap bitmap;

    public RecyclerViewAdapter(Context context, List<Getall_restaurant_details_pojo> stores, List<StoreModel> storeModels, List<OpeningHours_pojo> openingHoursPojo) {
        this.context = context;
        stLstStores = stores;
        models = storeModels;
        openingHoursPojoList= openingHoursPojo;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.places_storelist, parent, false);

        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.txtStoreName.setText(stLstStores.get(position).getName());
        holder.txtStoreAddr.setText(stLstStores.get(position).getFormatted_address());

       /* if(stLstStores.get(position).getRating()==null){
            holder.rtng_restaurant.setVisibility(View.GONE);
        }else {

            try {
                holder.rtng_restaurant.setRating(Float.parseFloat(stLstStores.get(position).getRating()));
                LayerDrawable stars = (LayerDrawable) holder.rtng_restaurant.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.parseColor("#008080"), PorterDuff.Mode.SRC_ATOP);

            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }*/



// GlideApp.with(context).load(stLstStores.get(position).getPhotos().get(position).getHtmlAttributions().get(position).toString()).into(holder.imgrestaurant);

/*

        int width = stLstStores.get(position).getPhotos().get(position).getWidth();
*/
        try {
            String html_reference = stLstStores.get(position).getPhotos().get(position).getPhotoReference();

            Log.e("photo_ref", "" + html_reference);
            Image_load_activity.loadGooglePhoto(context, holder.imgrestaurant, html_reference);
        }catch (Exception e){
            e.printStackTrace();
        }


//        places_photo(width,html_reference);


//        Log.e("open", "" + openingHoursPojoList.get(position).getOpenNow());


        holder.ll_hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, View_restaurant_details.class);
                intent.putExtra("Hotel_name", stLstStores.get(position).getName());
                intent.putExtra("Hotel_rating", stLstStores.get(position).getRating());
                intent.putExtra("placeid", stLstStores.get(position).getPlaceId());
                intent.putExtra("googleid", stLstStores.get(position).getId());
                if(stLstStores.get(position).getFormatted_address()==null){
                    intent.putExtra("Hotel_location", "Address is not available.");
                }else {
                    intent.putExtra("Hotel_location", stLstStores.get(position).getFormatted_address());
                }

                if (stLstStores.get(position).getPhotos().get(position).getPhotoReference()==null){
                    intent.putExtra("Hotel_icon", stLstStores.get(position).getIcon());
                }else {
                    intent.putExtra("Hotel_icon", stLstStores.get(position).getPhotos().get(position).getPhotoReference());
                }

                context.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return stLstStores.size();
    }


    //Photo_places

    private void places_photo(int width, String html_reference) {

        Call<Restaurant_output_pojo> call = apiService.places_photo(width, html_reference, APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<Restaurant_output_pojo>() {
            @Override
            public void onResponse(Call<Restaurant_output_pojo> call, Response<Restaurant_output_pojo> response) {
                Restaurant_output_pojo restaurantOutputPojo = response.body();


                if (response.isSuccessful()) {

                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {
                        Toast.makeText(context, "Success " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                    }


                } else if (!response.isSuccessful()) {
                    Toast.makeText(context, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<Restaurant_output_pojo> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });


    }








    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView txtStoreName;
        TextView txtStoreAddr, txtStoreimage;

        ImageView imgrestaurant;
        RatingBar rtng_restaurant;
        StoreModel model;
        LinearLayout ll_hotel;


        public MyViewHolder(View itemView) {
            super(itemView);

//            this.txtStoreopenhours = (TextView) itemView.findViewById(R.id.txtStoreDist);
            this.txtStoreName = (TextView) itemView.findViewById(R.id.txtStoreName);
            this.txtStoreAddr = (TextView) itemView.findViewById(R.id.txtStoreAddr);
            this.txtStoreimage = (TextView) itemView.findViewById(R.id.txtStoreimage);

            this.imgrestaurant = (ImageView) itemView.findViewById(R.id.img_location_icon);
            this.rtng_restaurant = (RatingBar) itemView.findViewById(R.id.rtng_restaurant);
            this.ll_hotel = (LinearLayout) itemView.findViewById(R.id.ll_hotel);


        }

    }
}
