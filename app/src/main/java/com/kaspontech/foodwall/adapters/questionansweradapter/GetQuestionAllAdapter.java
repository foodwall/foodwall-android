package com.kaspontech.foodwall.adapters.questionansweradapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.modelclasses.AnswerPojoClass;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestionAnswerPojo;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.questionspackage.EditQuestion;
import com.kaspontech.foodwall.questionspackage.pollcomments.ViewPollComments;
import com.kaspontech.foodwall.questionspackage.QuestionAnswer;
import com.kaspontech.foodwall.questionspackage.ViewQuestionAnswer;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class GetQuestionAllAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * Context
     */
    private Context context;

    /**
     * QuestionAnswerPojo
     */

    QuestionAnswerPojo getQuestionAllData;

    /**
     * QuestionAnswer array list
     */

    List<QuestionAnswerPojo> getQuestionAllDataList = new ArrayList<>();

    /**
     * Application TAG
     */

    private static final String TAG = "GetQuestionAllAdapter";

    /**
     * Undo poll flag
     */

    private boolean undoPoll = false;

    /**
     * GPSTracker
     */

    private GPSTracker gpsTracker;
    /**
     * Lat &  Long
     */
    private double latitude;
    private double longitude;

    /* QandA class for interface */
    QuestionAnswer questionAnswer;

    private onRefershListener onRefershListener;

    private onCommentsPostListener onCommentsPostListener;


    public GetQuestionAllAdapter(Context c, List<QuestionAnswerPojo> getQuestionAllDataList, onRefershListener onRefershListener, onCommentsPostListener onCommentsPostListener) {
        this.context = c;
        this.getQuestionAllDataList = getQuestionAllDataList;
        this.onRefershListener = onRefershListener;
        this.onCommentsPostListener = onCommentsPostListener;
    }


    @Override
    public int getItemViewType(int position) {


        // Case 0 - Normal Question
        // Case 1 - Poll Question

        if (Integer.parseInt(getQuestionAllDataList.get(position).getQuesType()) == 1) {

            return 2;

        } else {

            return 1;

        }

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        /*Initialization of GPS tracker and getting latitude and longitude*/
        gpsTracker = new GPSTracker(context);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        switch (viewType) {

            case 1:

                View answerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_que_ans_all, parent, false);
                return new AnswerAdapter(answerView, questionAnswer, onCommentsPostListener);

            case 2:

                View pollView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_question_as_poll, parent, false);
                return new PollAdapter(pollView, questionAnswer, onRefershListener);


        }
        return null;


    }


    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        holder.setIsRecyclable(false);

        getQuestionAllData = getQuestionAllDataList.get(position);

        switch (holder.getItemViewType()) {


            case 1:

                /* Question Parts */
                final AnswerAdapter answerAdapter = (AnswerAdapter) holder;

                /*Question text displaying*/
                answerAdapter.question.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getQuestionAllData.getAskQuestion()));
                String username = getQuestionAllData.getFirstName() + " " + getQuestionAllData.getLastName();
                answerAdapter.username.setText(username);

                /*User image displaying*/
                Utility.picassoImageLoader(getQuestionAllData.getPicture(), 0, answerAdapter.userimage, context);


                /*User name click listener*/
                answerAdapter.username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = getQuestionAllDataList.get(position).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);
                        }

                    }
                });


                /*Follower count display*/
                int followCount = getQuestionAllDataList.get(position).getTotalQuestFollow();
                if (getQuestionAllDataList.get(position).getQuesFollow() != 0) {

                    answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_followed_answer));
                    String followText = "Follow - " + String.valueOf(followCount);
                    answerAdapter.tv_follow_count.setText(followText);
                    answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.blue));
                    getQuestionAllDataList.get(position).setFollowing(true);


                } else {

                    if (followCount != 0) {

                        String followText = "Follow - " + String.valueOf(followCount);
                        answerAdapter.tv_follow_count.setText(followText);

                    } else {

                        answerAdapter.tv_follow_count.setText("Follow");
                    }

                    answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_follow_answer));
                    answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.black));
                    getQuestionAllDataList.get(position).setFollowing(false);

                }

                /*Follow layout click listener*/
                answerAdapter.ll_answer_follow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (!getQuestionAllDataList.get(position).isFollowing()) {


                            answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_followed_answer));
                            answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.blue));
                            getQuestionAllDataList.get(position).setFollowing(true);


                            if (Utility.isConnected(context)) {

                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                    Call<CommonOutputPojo> call = apiService.createQuesFollow("create_delete_question_follower",
                                            Integer.parseInt(getQuestionAllDataList.get(position).getQuestId()),
                                            Integer.parseInt(getQuestionAllDataList.get(position).getCreatedBy()),
                                            1, userid);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @SuppressLint("ResourceAsColor")
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                if (responseStatus.equals("success")) {

                                                    //Success

                                                    int count = response.body().getData();
                                                    String followText = "Follow - " + count;
                                                    answerAdapter.tv_follow_count.setText(followText);
                                                    //  getQuestionAllDataList.get(position).setTotalQuestFollow(count);


                                                } else {


                                                }

                                            } else {

                                                Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();
                                            }


                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            } else {

                                Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                            }


                        } else if (getQuestionAllDataList.get(position).isFollowing()) {

                            answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_follow_answer));
                            answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.black));
                            getQuestionAllDataList.get(position).setFollowing(false);


                            if (Utility.isConnected(context)) {
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                    Call<CommonOutputPojo> call = apiService.createQuesFollow("create_delete_question_follower",
                                            Integer.parseInt(getQuestionAllDataList.get(position).getQuestId()),
                                            Integer.parseInt(getQuestionAllDataList.get(position).getCreatedBy()),
                                            0, userid);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @SuppressLint("ResourceAsColor")
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                if (responseStatus.equals("success")) {

                                                    //Success
                                                    int count = response.body().getData();
                                                    // getQuestionAllDataList.get(position).setTotalQuestRequest(String.valueOf(count-1));


                                                    if (count != 0) {

                                                        String followText = "Follow - " + count;
                                                        answerAdapter.tv_follow_count.setText(followText);


                                                    } else {

                                                        String followText = "Follow";
                                                        answerAdapter.tv_follow_count.setText(followText);
                                                    }

                                                } else {


                                                }

                                            } else {

                                                Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                            }


                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            } else {

                                Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                            }


                        }
                    }
                });

                /*User image click listener*/
                answerAdapter.userimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        String userId = getQuestionAllDataList.get(position).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }

                    }
                });


                /*Total followers condition*/
                int total_followers = getQuestionAllData.getTotalFollowers();

                String followers = "";

                switch (total_followers) {

                    case 0:

                        answerAdapter.no_of_followers.setVisibility(View.GONE);

                        break;

                    case 1:

                        followers = String.valueOf(total_followers) + " Follower";

                        break;

                    default:

                        followers = String.valueOf(total_followers) + " Followers";

                        break;

                }

                answerAdapter.no_of_followers.setText(followers);

                String createdon = getQuestionAllData.getCreatedOn();
                /*Time stamp displaying*/
                Utility.setTimeStamp(createdon, answerAdapter.answeredOn);


                /*View Question on click listener*/
                answerAdapter.viewQues.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {



                        Intent intent = new Intent(context, ViewQuestionAnswer.class);
                        intent.putExtra("position", String.valueOf(position));
                        intent.putExtra("quesId", getQuestionAllDataList.get(position).getQuestId());
                        intent.putExtra("firstname", getQuestionAllDataList.get(position).getFirstName() + " " + getQuestionAllDataList.get(position).getLastName());
                        intent.putExtra("createdon", getQuestionAllDataList.get(position).getCreatedOn());
                        intent.putExtra("profile", getQuestionAllDataList.get(position).getPicture());
                        context.startActivity(intent);

                    }
                });

                /*View answer option on click listener*/

                answerAdapter.ans_options.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int questId = Integer.parseInt(getQuestionAllDataList.get(position).getQuestId());
                        int userid = Integer.parseInt(getQuestionAllDataList.get(position).getCreatedBy());
                        int quesType = Integer.parseInt(getQuestionAllDataList.get(position).getQuesType());
                        String question = getQuestionAllDataList.get(position).getAskQuestion();

                        /*Custom dialog click listener*/

                        CustomDialogClass ccd = new CustomDialogClass(context, questId, userid, position);
                        Objects.requireNonNull(ccd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        ccd.setCancelable(true);
                        ccd.show();

                    }
                });


                /* Answer Parts */


                if (Integer.parseInt(getQuestionAllDataList.get(position).getTotalAnswers()) == 0) {


                    answerAdapter.answer.setText("No answers yet");
                    answerAdapter.answer.setTypeface(Typeface.DEFAULT_BOLD);


                } else {


                    if (getQuestionAllDataList.get(position).getAnswer().get(0) != null) {

                        answerAdapter.answer.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getQuestionAllDataList.get(position).getAnswer().get(0).getAskAnswer()));
//                        answerAdapter.answer.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));

                    }


                }


                break;

            case 2:


                final PollAdapter pollAdapter = (PollAdapter) holder;

//                getPollDataList = getQuestionAllDataList.get(position).getPoll();


                /*User image loading*/
                Utility.picassoImageLoader(Pref_storage.getDetail(context, "picture_user_login"), 0, pollAdapter.userphoto_comment, context);



                /*View all comments click listener*/
                pollAdapter.tv_view_all_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent intent = new Intent(context, ViewPollComments.class);

                        intent.putExtra("comment_quesId", String.valueOf(getQuestionAllDataList.get(position).getQuestId()));
                        intent.putExtra("comment_question", getQuestionAllDataList.get(position).getAskQuestion());
                        intent.putExtra("comment_picture", getQuestionAllDataList.get(position).getPicture());
                        intent.putExtra("comment_firstname", getQuestionAllDataList.get(position).getFirstName() + " " + getQuestionAllDataList.get(position).getLastName());
                        intent.putExtra("comment_createdon", getQuestionAllDataList.get(position).getCreatedOn());
                        intent.putExtra("comment_pollid", getQuestionAllDataList.get(position).getPollId());

                        intent.putExtra("pollsize", String.valueOf(getQuestionAllDataList.get(position).getPoll().size()));

                        try {

                            double userVotesTwo = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            intent.putExtra("userVotes", String.valueOf(userVotesTwo));

                            if (getQuestionAllDataList.get(position).getPoll().size() == 2) {
                                double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll();
                                double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll();

                                intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                intent.putExtra("comment_poll1", getQuestionAllDataList.get(position).getPoll().get(0).getPollList());
                                intent.putExtra("comment_poll2", getQuestionAllDataList.get(position).getPoll().get(1).getPollList());
                            } else if (getQuestionAllDataList.get(position).getPoll().size() == 3) {
                                double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll();
                                double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                                intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                intent.putExtra("totalVotesThree", String.valueOf(totalVotesThree));

                                intent.putExtra("comment_poll1", getQuestionAllDataList.get(position).getPoll().get(0).getPollList());
                                intent.putExtra("comment_poll2", getQuestionAllDataList.get(position).getPoll().get(1).getPollList());
                                intent.putExtra("comment_poll3", getQuestionAllDataList.get(position).getPoll().get(2).getPollList());
                            } else if (getQuestionAllDataList.get(position).getPoll().size() == 4) {


                                double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll();
                                double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();
                                double totalVotesFour = getQuestionAllDataList.get(position).getPoll().get(3).getTotalPoll();

                                intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                intent.putExtra("totalVotesThree", String.valueOf(totalVotesThree));
                                intent.putExtra("totalVotesFour", String.valueOf(totalVotesFour));

                                intent.putExtra("comment_poll1", getQuestionAllDataList.get(position).getPoll().get(0).getPollList());
                                intent.putExtra("comment_poll2", getQuestionAllDataList.get(position).getPoll().get(1).getPollList());
                                intent.putExtra("comment_poll3", getQuestionAllDataList.get(position).getPoll().get(2).getPollList());
                                intent.putExtra("comment_poll4", getQuestionAllDataList.get(position).getPoll().get(3).getPollList());
                            }


                            intent.putExtra("comment_votecount", getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser() + " votes ");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        context.startActivity(intent);

                    }
                });



                /*Poll edit text change listener*/
                pollAdapter.edittxt_comment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        /*Post visibility condition*/

                        if (pollAdapter.edittxt_comment.getText().toString().trim().length() != 0) {

                            pollAdapter.txt_adapter_post.setVisibility(VISIBLE);

                        } else {

                            pollAdapter.txt_adapter_post.setVisibility(GONE);
                        }

                    }

                });



                /*Total comments count displaying*/
                int totalComments = Integer.parseInt(getQuestionAllDataList.get(position).getTotalComments());

                if (totalComments == 0) {

                    pollAdapter.tv_view_all_comments.setVisibility(GONE);

                } else if (totalComments == 1) {

                    pollAdapter.tv_view_all_comments.setVisibility(VISIBLE);
                    String viewTxt = "View " + totalComments + " Comment";
                    pollAdapter.tv_view_all_comments.setText(viewTxt);

                } else if (totalComments > 1) {

                    pollAdapter.tv_view_all_comments.setVisibility(VISIBLE);
                    String viewTxt = "View all " + totalComments + " Comments";
                    pollAdapter.tv_view_all_comments.setText(viewTxt);


                }




                final int pollSize = getQuestionAllDataList.get(position).getPoll().size();


                if (getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser() == 0) {

                    pollAdapter.total_poll_votes.setText("No votes");

                } else if (getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser() == 1) {

                    pollAdapter.total_poll_votes.setText(String.valueOf(getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser()) + " Vote");

                } else {

                    pollAdapter.total_poll_votes.setText(String.valueOf(getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser()) + " Votes");

                }


                if (Integer.parseInt(getQuestionAllDataList.get(position).getPollId()) != 0) {

                    pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                    pollAdapter.ll_answer_section.setVisibility(View.GONE);
                    pollAdapter.tv_poll_undo.setVisibility(View.VISIBLE);


                    if (pollSize != 0) {

                        /* Poll option one */

                        pollAdapter.poll_answer_one.setText(getQuestionAllDataList.get(position).getPoll().get(0).getPollList());

                        double userVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                        double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll();


                        Log.e("VotesOne", "userVotesOne->" + (double) userVotesOne);
                        Log.e("VotesOne", "totalVotesOne->" + (double) totalVotesOne);
                        Log.e("VotesOne", "totalVotesOne->" + (double) totalVotesOne / userVotesOne);


                        double percentOne;

                        if (userVotesOne == 0 || totalVotesOne == 0) {

                            percentOne = 0;

                        } else {

                            percentOne = (totalVotesOne / userVotesOne) * 100;

                        }


                        if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(0).getPollId()) {

                            pollAdapter.poll_one_checked.setVisibility(View.VISIBLE);

                        } else {

                            pollAdapter.poll_one_checked.setVisibility(View.GONE);

                        }


                        pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                        pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");


                        /* Poll option two */
                        pollAdapter.poll_answer_two.setText(getQuestionAllDataList.get(position).getPoll().get(1).getPollList());

                        double userVotesTwo = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                        double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll();


                        Log.e("VotesOne", "userVotesTwo->" + (double) userVotesTwo);
                        Log.e("VotesOne", "totalVotesTwo->" + (double) totalVotesTwo);
                        Log.e("VotesOne", "totalVotesTwo->" + (double) totalVotesTwo / userVotesTwo);


                        double percentTwo;


                        if (userVotesTwo == 0 || totalVotesTwo == 0) {

                            percentTwo = 0;

                        } else {

                            percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                        }

                        if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(1).getPollId()) {

                            pollAdapter.poll_two_checked.setVisibility(View.VISIBLE);

                        } else {

                            pollAdapter.poll_two_checked.setVisibility(View.GONE);


                        }

                        pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                        pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                        /* Poll option three */

                        if (pollSize == 3) {

                            pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                            pollAdapter.poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());

                            double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                            Log.e("VotesOne", "userVotesThree->" + (double) userVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree / userVotesThree);


                            double percentThree;

                            if (userVotesThree == 0 || totalVotesThree == 0) {

                                percentThree = 0;

                            } else {

                                percentThree = (totalVotesThree / userVotesThree) * 100;

                            }


                            if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(2).getPollId()) {

                                pollAdapter.poll_three_checked.setVisibility(View.VISIBLE);

                            } else {

                                pollAdapter.poll_three_checked.setVisibility(View.GONE);


                            }

                            pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                            pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            /* Poll option four */

                        } else if (pollSize == 4) {


                            // poll option 3

                            pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                            pollAdapter.poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());


                            pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                            pollAdapter.poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());

                            double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();


                            Log.e("VotesOne", "userVotesThree->" + (double) userVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree / userVotesThree);


                            double percentThree;

                            if (userVotesThree == 0 || totalVotesThree == 0) {

                                percentThree = 0;

                            } else {

                                percentThree = (totalVotesThree / userVotesThree) * 100;

                            }

                            if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(2).getPollId()) {

                                pollAdapter.poll_three_checked.setVisibility(View.VISIBLE);

                            } else {

                                pollAdapter.poll_three_checked.setVisibility(View.GONE);


                            }

                            pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                            pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            /* poll option 4 */

                            pollAdapter.ll_poll_progress_four.setVisibility(View.VISIBLE);
                            pollAdapter.poll_answer_four.setText(getQuestionAllDataList.get(position).getPoll().get(3).getPollList());


                            double userVotesFour = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesFour = getQuestionAllDataList.get(position).getPoll().get(3).getTotalPoll();

                            Log.e("VotesOne", "userVotesFour->" + (double) userVotesFour);
                            Log.e("VotesOne", "totalVotesFour->" + (double) totalVotesFour);
                            Log.e("VotesOne", "totalVotesFour->" + (double) totalVotesFour / userVotesFour);


                            double percentFour;

                            if (userVotesFour == 0 || totalVotesFour == 0) {

                                percentFour = 0;

                            } else {

                                percentFour = (totalVotesFour / userVotesFour) * 100;

                            }

                            if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(3).getPollId()) {

                                pollAdapter.poll_four_checked.setVisibility(View.VISIBLE);

                            } else {

                                pollAdapter.poll_four_checked.setVisibility(View.GONE);


                            }

                            pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                            pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");

                        }

                    }


                }


                Log.e("pollSize", "pollSize->" + pollSize);

                if (pollSize != 0) {

                    pollAdapter.tv_poll_answer_one.setText(getQuestionAllDataList.get(position).getPoll().get(0).getPollList());
                    pollAdapter.tv_poll_answer_two.setText(getQuestionAllDataList.get(position).getPoll().get(1).getPollList());

                    pollAdapter.poll_answer_one.setText(getQuestionAllDataList.get(position).getPoll().get(0).getPollList());
                    pollAdapter.poll_answer_two.setText(getQuestionAllDataList.get(position).getPoll().get(1).getPollList());


                    if (pollSize == 3) {


                        pollAdapter.ll_poll_answer_third.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());

                        pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                        pollAdapter.poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());


                    } else if (pollSize == 4) {

                        pollAdapter.ll_poll_answer_third.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());

                        pollAdapter.ll_poll_answer_four.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_answer_four.setText(getQuestionAllDataList.get(position).getPoll().get(3).getPollList());

                        pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                        pollAdapter.poll_answer_third.setText(getQuestionAllDataList.get(position).getPoll().get(2).getPollList());

                        pollAdapter.ll_poll_progress_four.setVisibility(View.VISIBLE);
                        pollAdapter.poll_answer_four.setText(getQuestionAllDataList.get(position).getPoll().get(3).getPollList());

                    }

                }

                /*Answer one click listener*/
                pollAdapter.ll_poll_answer_one.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = Integer.parseInt(getQuestionAllDataList.get(position).getQuestId());
                        int pollId = Integer.parseInt(getQuestionAllDataList.get(position).getPoll().get(0).getPollId());

                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_one_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo.setVisibility(View.VISIBLE);


                        int totalVotes = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }


                        if (pollSize != 0) {

                            // Option One

                            double userVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll() + 1;

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }

                            Log.e("VotesOne", "userVotesOne->" + (double) userVotesOne);
                            Log.e("VotesOne", "totalVotesOne->" + (double) totalVotesOne);
                            Log.e("VotesOne", "percentOne->" + percentOne);


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two

                            double userVotesTwo = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll();

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                            if (pollSize == 3) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();
                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            } else if (pollSize == 4) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                                // Option Four
                                double userVotesFour = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesFour = getQuestionAllDataList.get(position).getPoll().get(3).getTotalPoll();

                                double percentFour;

                                if (userVotesFour == 0 || totalVotesFour == 0) {

                                    percentFour = 0;

                                } else {

                                    percentFour = (totalVotesFour / userVotesFour) * 100;

                                }

                                pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                                pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                            }

                        }


                        createPoll(quesId, pollId);
//                        getAllQuesData();


                    }
                });
                /*Answer two click listener*/

                pollAdapter.ll_poll_answer_two.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = Integer.parseInt(getQuestionAllDataList.get(position).getQuestId());
                        int pollId = Integer.parseInt(getQuestionAllDataList.get(position).getPoll().get(1).getPollId());
                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_two_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo.setVisibility(View.VISIBLE);


                        int totalVotes = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }

                        if (pollSize != 0) {

                            // Option One

                            double userVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll();

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two

                            double userVotesTwo = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll() + 1;

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            Log.e("VotesOne", "userVotesTwo->" + (double) userVotesTwo);
                            Log.e("VotesOne", "totalVotesTwo->" + (double) totalVotesTwo);
                            Log.e("VotesOne", "percentTwo->" + percentTwo);


                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                            if (pollSize == 3) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            } else if (pollSize == 4) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                                // Option Four
                                double userVotesFour = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesFour = getQuestionAllDataList.get(position).getPoll().get(3).getTotalPoll();

                                double percentFour;

                                if (userVotesFour == 0 || totalVotesFour == 0) {

                                    percentFour = 0;

                                } else {

                                    percentFour = (totalVotesFour / userVotesFour) * 100;

                                }

                                pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                                pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                            }

                        }


                        createPoll(quesId, pollId);
//                        getAllQuesData();


                    }
                });

                pollAdapter.ll_poll_answer_third.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = Integer.parseInt(getQuestionAllDataList.get(position).getQuestId());
                        int pollId = Integer.parseInt(getQuestionAllDataList.get(position).getPoll().get(2).getPollId());
                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_three_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo.setVisibility(View.VISIBLE);


                        int totalVotes = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }


                        if (pollSize != 0) {

                            // Option One
                            double userVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll();

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two
                            double userVotesTwo = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll();

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                            if (pollSize == 3) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll() + 1;

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                Log.e("VotesOne", "userVotesThree->" + (double) userVotesThree);
                                Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree);
                                Log.e("VotesOne", "percentThree->" + percentThree);


                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            } else if (pollSize == 4) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll() + 1;

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                                // Option Four
                                double userVotesFour = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesFour = getQuestionAllDataList.get(position).getPoll().get(3).getTotalPoll();
                                double percentFour;

                                if (userVotesFour == 0 || totalVotesFour == 0) {

                                    percentFour = 0;

                                } else {

                                    percentFour = (totalVotesFour / userVotesFour) * 100;

                                }

                                pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                                pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                            }

                        }


                        createPoll(quesId, pollId);
//                        getAllQuesData();


                    }
                });


                pollAdapter.ll_poll_answer_four.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = Integer.parseInt(getQuestionAllDataList.get(position).getQuestId());
                        int pollId = Integer.parseInt(getQuestionAllDataList.get(position).getPoll().get(3).getPollId());
                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_four_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo.setVisibility(View.VISIBLE);


                        Log.e(TAG, "onClick: " + getQuestionAllDataList.get(position).getPoll().size());


                        int totalVotes = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            getQuestionAllDataList.get(position).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }


                        if (pollSize != 0) {

                            // Option One
                            double userVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPoll();

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two
                            double userVotesTwo = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = getQuestionAllDataList.get(position).getPoll().get(1).getTotalPoll();

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                            if (pollSize == 3) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            } else if (pollSize == 4) {


                                // Option Three
                                double userVotesThree = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = getQuestionAllDataList.get(position).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                                // Option Four
                                double userVotesFour = getQuestionAllDataList.get(position).getPoll().get(0).getTotalPollUser();
                                double totalVotesFour = getQuestionAllDataList.get(position).getPoll().get(3).getTotalPoll() + 1;

                                double percentFour;

                                if (userVotesFour == 0 || totalVotesFour == 0) {

                                    percentFour = 0;

                                } else {

                                    percentFour = (totalVotesFour / userVotesFour) * 100;

                                }

                                Log.e("VotesOne", "userVotesFour->" + (double) userVotesFour);
                                Log.e("VotesOne", "totalVotesFour->" + (double) totalVotesFour);
                                Log.e("VotesOne", "percentFour->" + percentFour);


                                pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                                pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                            }

                        }


                        createPoll(quesId, pollId);
//                        getAllQuesData();

                    }
                });


                pollAdapter.userName.setText(getQuestionAllData.getFirstName() + " " + getQuestionAllData.getLastName());

                Utility.picassoImageLoader(getQuestionAllData.getPicture(), 0, pollAdapter.userimage, context);



                pollAdapter.userName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = getQuestionAllDataList.get(position).getUserId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }
                });


                pollAdapter.userimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = getQuestionAllDataList.get(position).getUserId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }
                });


                String pollCreatedon = getQuestionAllData.getCreatedOn();


                int total_follower = getQuestionAllData.getTotalFollowers();

                String follower = "";

                switch (total_follower) {

                    case 0:

                        pollAdapter.follow_count.setVisibility(View.GONE);

                        break;

                    case 1:

                        follower = String.valueOf(total_follower) + " Follower";

                        break;

                    default:

                        follower = String.valueOf(total_follower) + " Followers";

                        break;

                }

                pollAdapter.follow_count.setText(follower);

                Utility.setTimeStamp(pollCreatedon, pollAdapter.answeredOn);




                pollAdapter.question.setText(getQuestionAllData.getAskQuestion());


                pollAdapter.ans_options.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int questId = Integer.parseInt(getQuestionAllDataList.get(position).getQuestId());
                        int createdBy = Integer.parseInt(getQuestionAllDataList.get(position).getCreatedBy());

                        Pref_storage.setDetail(context, "clicked_quesID", String.valueOf(questId));
                        Pref_storage.setDetail(context, "clicked_quesCreatedBy", String.valueOf(createdBy));


                        CustomDialogClass ccd = new CustomDialogClass(context, questId, createdBy, position);
                        ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        ccd.setCancelable(true);
                        ccd.show();


                    }
                });




                break;


        }


    }


    /*Create poll api call*/
    private void createPoll(int quesId, int pollId) {

        if (Utility.isConnected(context)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Call<AnswerPojoClass> call = apiService.createAnswer("create_answer", 0, quesId,
                        pollId, 1, "poll", latitude, longitude, userid);

                call.enqueue(new Callback<AnswerPojoClass>() {
                    @Override
                    public void onResponse(Call<AnswerPojoClass> call, Response<AnswerPojoClass> response) {

                        if (response.body() != null) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                            }

                        } else {

                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<AnswerPojoClass> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "FailureError" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }


    }

    @Override
    public int getItemCount() {

        if (getQuestionAllDataList.isEmpty()) {

//            ll_nodata.setVisibility(View.VISIBLE);

        } else {

//            ll_nodata.setVisibility(View.GONE);

        }
        return getQuestionAllDataList.size();
    }


    /*Custom dialog class*/
    public class CustomDialogClass extends Dialog implements
            View.OnClickListener {

        public Activity activity;
        public Dialog dialog;
        public AppCompatButton btn_Ok;

        int quesId, userId, position;

        TextView txt_deletepost, txt_report, txt_copylink, txt_turnoffpost, txt_sharevia, txt_editpost;

        CustomDialogClass(Context a, int quesId, int userid, int position) {
            super(a);
            // TODO Auto-generated constructor stub
            context = a;
            this.quesId = quesId;
            this.userId = userid;
            this.position = position;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
//            setContentView(R.layout.dialog_version_info);

            setContentView(R.layout.custom_dialog_timeline);

            txt_deletepost = (TextView) findViewById(R.id.txt_deletepost);
            txt_editpost = (TextView) findViewById(R.id.txt_editpost);
            txt_report = (TextView) findViewById(R.id.txt_report);
            txt_copylink = (TextView) findViewById(R.id.txt_copylink);
            txt_turnoffpost = (TextView) findViewById(R.id.txt_turnoffpost);
            txt_sharevia = (TextView) findViewById(R.id.txt_sharevia);

            txt_deletepost.setOnClickListener(this);
            txt_editpost.setOnClickListener(this);
            txt_report.setOnClickListener(this);
            txt_copylink.setOnClickListener(this);
            txt_turnoffpost.setOnClickListener(this);
            txt_sharevia.setOnClickListener(this);


            if (String.valueOf(userId).equals(Pref_storage.getDetail(context, "userId"))) {
                txt_deletepost.setVisibility(VISIBLE);
                txt_editpost.setVisibility(VISIBLE);
            } else {
                txt_deletepost.setVisibility(GONE);
                txt_editpost.setVisibility(GONE);
            }


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_editpost:

                    Intent intent = new Intent(context, EditQuestion.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("quesId", quesId);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                    dismiss();

                    break;


                case R.id.txt_deletepost:

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to delete this post?")
                            .setContentText("Won't be able to recover this post anymore!")
                            .setConfirmText("Yes,delete it!")
                            .setCancelText("No,cancel!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {

                                    sDialog.dismissWithAnimation();
                                    /*Calling delete question api calling*/
                                    deleteQuestion(quesId, userId, position);


                                }
                            })
                            .show();
                    dismiss();
                    break;

                case R.id.txt_report:
                    dismiss();
                    break;

                case R.id.txt_copylink:
                    Toast.makeText(context, "Link has been copied to clipboard", Toast.LENGTH_SHORT).show();
                    dismiss();
                    break;

                case R.id.txt_turnoffpost:
                    dismiss();
                    break;
                case R.id.txt_sharevia:

                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                    i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                    context.startActivity(Intent.createChooser(i, "Share via"));
                    dismiss();
                    break;

                default:
                    break;
            }
        }

    }

    /*Calling delete question api calling*/

    private void deleteQuestion(int quesid, int createdBy, final int position) {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {


                Call<CommonOutputPojo> call = apiService.deleteQuestion("create_delete_question", quesid, createdBy);

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                        if (response.body() != null) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("deleteEventComment", "responseStatus->" + responseStatus);


                            if (responseStatus.equals("success")) {

                                //Success
                                /*Remove postion*/
                                removeAt(position);

                            } else {


                            }


                        } else {

                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "FailureError" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }
        } else {

            Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }


    }

    /*Remove postion*/

    public void removeAt(int position) {
        getQuestionAllDataList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getQuestionAllDataList.size());
    }


    public interface onRefershListener {
        void onRefereshedFragment(View view, int adapterPosition, View pollAdapterView);
    }

}
