package com.kaspontech.foodwall.adapters.bucketadapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.bucketlistpackage.CreateBucketlistActivty;
import com.kaspontech.foodwall.bucketlistpackage.Mybucketfragment;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.BucketIndividualuser_Datum;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.reviewpackage.ReviewDeliveryDetails;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.PackagingDetailsAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewAmbienceAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewAvoidDishAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewTopDishAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_in_detail_activity;
import com.kaspontech.foodwall.reviewpackage.ReviewsLikesAllActivity;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
import com.kaspontech.foodwall.reviewpackage.alldishtab.AllDishDisplayActivity;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static java.lang.Integer.parseInt;

public class GroupBucketlist_userAdapter extends RecyclerView.Adapter<GroupBucketlist_userAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     **/
    private Context context;
    /**
     * My bucket arraylist
     **/
    private ArrayList<BucketIndividualuser_Datum> getallHotelReviewPojoArrayList = new ArrayList<>();
    /**
     * More options
     **/
    private final String[] itemsothers = {"Bucket list options","Share","Share Chat"/*,"Delete Review"*/};
    /**
     * Top dish arraylist
     **/
    public ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();

    /**
     * Avoid dish arraylist
     **/
    public ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();
    /**
     * Ambience arraylist
     **/
    public ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

    Mybucketfragment listsize;

     onCommentsPostListener onCommentsPostListener;

    public GroupBucketlist_userAdapter(Context c, ArrayList<BucketIndividualuser_Datum> gettinglist,onCommentsPostListener onCommentsPostListener1) {
        this.context = c;
        this.getallHotelReviewPojoArrayList = gettinglist;
        this.onCommentsPostListener = onCommentsPostListener1;


    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.newbucketlist, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v,onCommentsPostListener);

//        vh.setIsRecyclable(false);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        holder.img_bucket_list.setVisibility(GONE);

        try {

            /*Display hotel details*/
            displayReviewItem(getallHotelReviewPojoArrayList, holder, position);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    /*Display hotel details*/


    public void displayReviewItem(final ArrayList<BucketIndividualuser_Datum> reviewList, final MyViewHolder holder, final int position) {


        if (reviewList.size() > 0) {


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.ll_review_main.setVisibility(VISIBLE);
                }
            });

            // holder.ll_review_main.setVisibility(VISIBLE);
            //reviewer_name
            String reviewer_fname = reviewList.get(position).getRevFirstName();
            String reviewer_lname = reviewList.get(position).getRevLastName();
            holder.username.setText(reviewer_fname);
            holder.userlastname.setText(reviewer_lname);
            holder.username.setTextColor(Color.parseColor("#000000"));
            holder.userlastname.setTextColor(Color.parseColor("#000000"));

            if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {

                holder.title_ambience.setText("Packaging");

            } else {

                holder.title_ambience.setText("Hotel Ambience");

            }

            /* txt_hotelname */

            holder.tv_restaurant_name.setText(reviewList.get(position).getHotelName());

            /* Mode of delivery text */

            if (reviewList.get(position).getModeId().equalsIgnoreCase("0")) {

                holder.txt_mode_delivery.setVisibility(GONE);
//            holder.ll_delivery_mode.setVisibility(GONE);

            } else {

                if (reviewList.get(position).getModeId().equalsIgnoreCase("1")) {
                    holder.txt_mode_delivery.setText(reviewList.get(position).getCompName());
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#f25353"));

                } else if (reviewList.get(position).getModeId().equalsIgnoreCase("2")) {
                    holder.txt_mode_delivery.setText(reviewList.get(position).getCompName().replace("swiggy", "Swiggy"));
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#ff8008"));

                } else if (reviewList.get(position).getModeId().equalsIgnoreCase("3")) {
                    holder.txt_mode_delivery.setText(reviewList.get(position).getCompName().replace("Food panda", "Foodpanda"));
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#ffff8800"));

                } else if (reviewList.get(position).getModeId().equalsIgnoreCase("4")) {
                    holder.txt_mode_delivery.setText(reviewList.get(position).getCompName());
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#2ecc71"));

                }


            }


            if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {

                holder.ll_delivery_mode.setVisibility(VISIBLE);

            } else {

                holder.ll_delivery_mode.setVisibility(GONE);

            }

            /* Reviewer Image */

            Utility.picassoImageLoader(reviewList.get(position).getRevPicture(),0,holder.userphoto_profile, context);




            /* User Image */
            Utility.picassoImageLoader(Pref_storage.getDetail(context, "picture_user_login"),0,holder.review_user_photo, context);




            /* Timestamp for review */

            String createdOn = reviewList.get(position).getCreatedOn();

            Utility.setTimeStamp(createdOn, holder.txt_created_on);


            // Display top dishes

            if (reviewList.get(position).getTopdish() == null) {

                holder.tv_top_dishes.setVisibility(VISIBLE);
            } else {

               // holder.tv_top_dishes.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(reviewList.get(position).getTopdish().replace(",", ", ")));
                holder.tv_top_dishes.setVisibility(GONE);

            }


            /* Display avoid dishes */

            if (reviewList.get(position).getAvoiddish() == null) {
                holder.tv_dishes_to_avoid.setVisibility(VISIBLE);
            } else {

                //holder.tv_dishes_to_avoid.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(reviewList.get(position).getAvoiddish().replace(",", ", ")));
                holder.tv_dishes_to_avoid.setVisibility(GONE);

            }


            /* Edit text comment */

            holder.review_comment.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (holder.review_comment.getText().toString().length() != 0) {
                        holder.txt_adapter_post.setVisibility(VISIBLE);
                    } else {
                        holder.txt_adapter_post.setVisibility(GONE);
                    }


                }
            });


           /* *//* Comment posting API *//*
            holder.txt_adapter_post.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Utility.hideKeyboard(v);

                    String comment = org.apache.commons.text.StringEscapeUtils.escapeJava(holder.review_comment.getText().toString().trim());
                    if(Utility.isConnected(context)){
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {
                            String createuserid = Pref_storage.getDetail(context, "userId");

                            Call<CommonOutputPojo> call = apiService.create_edit_hotel_review_comments("create_edit_hotel_review_comments", 0, Integer.parseInt(reviewList.get(position).getHotelId()), Integer.parseInt(reviewList.get(position).getReviewid()), comment, Integer.parseInt(createuserid));
                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    if (response.body().getResponseCode() == 1) {


//                                Toast.makeText(context, "Comment created", Toast.LENGTH_SHORT).show();
                                        int tot_comments = Integer.parseInt(reviewList.get(position).getTotalComments());
                                        if (tot_comments == 0 || reviewList.get(position).getTotalComments() == null) {

                                            holder.tv_view_all_comments.setText(R.string.view_one_comment);

                                        } else if (tot_comments >= 1) {

                                            holder.tv_view_all_comments.setText("View all " + tot_comments + " comments");

                                        }
                                        holder.review_comment.setText("");
                                        holder.txt_adapter_post.setVisibility(GONE);

                                        Intent intent = new Intent(context, Reviews_comments_all_activity.class);

                                        intent.putExtra("posttype", reviewList.get(position).getCategoryType());
                                        intent.putExtra("hotelid", reviewList.get(position).getHotelId());
                                        intent.putExtra("reviewid", reviewList.get(position).getReviewid());
                                        intent.putExtra("userid", reviewList.get(position).getUserId());

                                        intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                                        intent.putExtra("overallrating", reviewList.get(position).getFoodExprience());

                                        if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                                            intent.putExtra("totaltimedelivery", reviewList.get(position).getTotalTimedelivery());
                                        } else {
                                            intent.putExtra("totaltimedelivery", reviewList.get(position).getAmbiance());
                                        }

                                        intent.putExtra("taste_count", reviewList.get(position).getTaste());
                                        intent.putExtra("vfm_rating", reviewList.get(position).getValueMoney());
                                        if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                                            intent.putExtra("package_rating", reviewList.get(position).getPackage());
                                        } else {
                                            intent.putExtra("package_rating", reviewList.get(position).getService());
                                        }

                                        intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                                        intent.putExtra("reviewprofile", reviewList.get(position).getRevPicture());
                                        intent.putExtra("createdon", reviewList.get(position).getCreatedOn());
                                        intent.putExtra("firstname", reviewList.get(position).getRevFirstName());
                                        intent.putExtra("lastname", reviewList.get(position).getRevLastName());

                                        if (reviewList.get(0).getCategoryType().equalsIgnoreCase("2")) {
                                            intent.putExtra("title_ambience", "Packaging");
                                        } else {
                                            intent.putExtra("title_ambience", "Hotel Ambience");
                                        }

                                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                        context.startActivity(intent);

                                    }

                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }else{
                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }

                }
            });
*/

            // Dishes to avoid text count


            int dishAvoid = reviewList.get(position).getAvoidCount();

            if (dishAvoid == 0) {
                holder.ll_dish_to_avoid.setVisibility(View.GONE);
            }


            /* Overallrating */

            String rating = reviewList.get(position).getFoodExprience();

            holder.tv_res_overall_rating.setText(rating);

            if (Integer.parseInt(rating) == 1) {

                holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_red);
                holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

            } else if (Integer.parseInt(rating) == 2) {

                holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.ll_res_overall_rating.setBackgroundResource(R.drawable.review_rating_orange_bg);
                holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_orange_star);

            } else if (Integer.parseInt(rating) == 3) {

                holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));

            } else if (Integer.parseInt(rating) == 4) {
                holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_star);
                holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
            } else if (Integer.parseInt(rating) == 5) {
                holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
            }


            try {

                /* Time delivery */

                if (Integer.parseInt(reviewList.get(position).getCategoryType()) == 2) {

                    String time_deleivery = reviewList.get(position).getTimedelivery();

                /*if(time_deleivery == null){

                    time_deleivery = "4";
                }*/

                    holder.ambi_or_timedelivery.setText(R.string.Delivery_new);
                    holder.tv_ambience_rating.setText(time_deleivery);

                    if (Integer.parseInt(time_deleivery) == 1) {
                        holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_red);

                        holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

                    } else if (Integer.parseInt(time_deleivery) == 2) {
                        holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                        holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                    } else if (Integer.parseInt(time_deleivery) == 3) {
                        holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                        holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                    } else if (Integer.parseInt(time_deleivery) == 4) {
                        holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_star);

                        holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                    } else if (Integer.parseInt(time_deleivery) == 5) {
                        holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                        holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                    }

                } else {

                    /* Ambiance */

                    String ambiance = reviewList.get(position).getAmbiance();

                    holder.tv_ambience_rating.setText(ambiance);

                    if (Integer.parseInt(ambiance) == 1) {
                        holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_red);

                        holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

                    } else if (Integer.parseInt(ambiance) == 2) {
                        holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                        holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                    } else if (Integer.parseInt(ambiance) == 3) {
                        holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                        holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                    } else if (Integer.parseInt(ambiance) == 4) {
                        holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_star);

                        holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                    } else if (Integer.parseInt(ambiance) == 5) {
                        holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                        holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            /* Taste */

            String taste = reviewList.get(position).getTaste();

            holder.txt_taste_count.setText(taste);

            if (Integer.parseInt(taste) == 1) {
                holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_red);
                holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

            } else if (Integer.parseInt(taste) == 2) {
                holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
            } else if (Integer.parseInt(taste) == 3) {
                holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
            } else if (Integer.parseInt(taste) == 4) {
                holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_star);
                holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
            } else if (Integer.parseInt(taste) == 5) {
                holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
            }


            /* Value for money */

            String vfm = reviewList.get(position).getValueMoney();

            holder.tv_vfm_rating.setText(vfm);

            if (Integer.parseInt(vfm) == 1) {
                holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_red);
                holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
            } else if (Integer.parseInt(vfm) == 2) {
                holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
            } else if (Integer.parseInt(vfm) == 3) {
                holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
            } else if (Integer.parseInt(vfm) == 4) {
                holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_star);
                holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
            } else if (Integer.parseInt(vfm) == 5) {
                holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
            }


            try {


                /* Packaging */

                if (Integer.parseInt(reviewList.get(position).getCategoryType()) == 2) {

                    String package_rating = reviewList.get(position).getPackage();

                /*if(package_rating == null){

                    package_rating = "4";
                }*/

                    holder.tv_service_rating.setText(package_rating);
                    holder.serv_or_package.setText(R.string.Package);

                    if (Integer.parseInt(package_rating) == 1) {
                        holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_service_star.setImageResource(R.drawable.ic_review_rating_red);
                        holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
                    } else if (Integer.parseInt(package_rating) == 2) {
                        holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_service_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                        holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                    } else if (Integer.parseInt(package_rating) == 3) {
                        holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_service_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                        holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                    } else if (Integer.parseInt(package_rating) == 4) {
                        holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_service_star.setImageResource(R.drawable.ic_review_rating_star);

                        holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                    } else if (Integer.parseInt(package_rating) == 5) {
                        holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_service_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                        holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                    }


                } else {

                    /* Service */

                    String service = reviewList.get(position).getService();

                    holder.tv_service_rating.setText(service);
                    if (Integer.parseInt(service) == 1) {
                        holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_service_star.setImageResource(R.drawable.ic_review_rating_red);
                        holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
                    } else if (Integer.parseInt(service) == 2) {
                        holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_service_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                        holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                    } else if (Integer.parseInt(service) == 3) {
                        holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_service_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                        holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                    } else if (Integer.parseInt(service) == 4) {
                        holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_service_star.setImageResource(R.drawable.ic_review_rating_star);

                        holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                    } else if (Integer.parseInt(service) == 5) {
                        holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.img_service_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                        holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }


          /*  String total_reviews = reviewList.get(position).getTotalReview();
            String total_followers = reviewList.get(position).getTotalFollowers();

            // Followers display

            if (Integer.parseInt(total_followers) != 0 && Integer.parseInt(total_followers) == 1) {
                holder.comma.setVisibility(VISIBLE);
                holder.txt_followers_count.setText(total_followers);
                holder.txt_followers_count.setVisibility(VISIBLE);
                holder.txt_followers.setVisibility(VISIBLE);
            } else if (Integer.parseInt(total_followers) != 0 && Integer.parseInt(total_followers) > 1) {

                holder.comma.setVisibility(VISIBLE);
                holder.txt_followers_count.setText(total_followers);
                holder.txt_followers.setText(R.string.followers);
                holder.txt_followers_count.setVisibility(VISIBLE);
                holder.txt_followers.setVisibility(VISIBLE);
            }

            if (parseInt(total_reviews) == 1) {
                holder.count.setText(total_reviews.concat(" Review"));
            } else if (parseInt(total_reviews) > 1) {
                holder.count.setText(total_reviews.concat(" Reviews"));
            }
*/

            /* Review comment */

            if (reviewList.get(position).getHotelReview().equalsIgnoreCase("0")) {
                holder.reviewcmmnt.setVisibility(GONE);
            } else {

                holder.reviewcmmnt.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(reviewList.get(position).getHotelReview()));


            }


            /* Total likes */

            final String total_likes = reviewList.get(position).getTotalLikes();

            if (Integer.parseInt(total_likes) == 0) {

                holder.ll_likecomment.setVisibility(GONE);

            } else {

                holder.ll_likecomment.setVisibility(VISIBLE);

            }

            int userliked = Integer.parseInt(reviewList.get(position).getHtlRevLikes());

            if (userliked == 0) {

                reviewList.get(position).setUserLiked(false);
                holder.img_btn_review_like.setVisibility(View.VISIBLE);
                holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));

            } else if (userliked == 1) {

                reviewList.get(position).setUserLiked(true);
                holder.img_btn_review_like.setVisibility(View.VISIBLE);
                holder.ll_likecomment.setVisibility(VISIBLE);
                holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));

            }


            if (parseInt(total_likes) == 0 || total_likes.equalsIgnoreCase(null) || total_likes.equalsIgnoreCase("")) {

                holder.tv_like_count.setVisibility(View.GONE);
            }
            if (parseInt(total_likes) == 1) {
                holder.tv_like_count.setText(reviewList.get(position).getTotalLikes().concat(" ").concat("like"));
                holder.tv_like_count.setVisibility(View.VISIBLE);


            } else if (parseInt(total_likes) > 1) {
                holder.tv_like_count.setText(reviewList.get(position).getTotalLikes().concat(" ").concat("likes"));
                holder.tv_like_count.setVisibility(View.VISIBLE);
            }

            // Total comments

            String total_comments = reviewList.get(position).getTotalComments();

            if (parseInt(total_comments) == 0 || total_likes.equalsIgnoreCase(null) || total_likes.equalsIgnoreCase("")) {

                holder.tv_view_all_comments.setVisibility(View.GONE);

            }

            if (parseInt(total_comments) == 1) {

                holder.tv_view_all_comments.setText(R.string.view_one_comment);
                holder.tv_view_all_comments.setVisibility(View.VISIBLE);

            } else if (parseInt(total_comments) > 1) {

                holder.tv_view_all_comments.setText("View all " + total_comments + " comments");
                holder.tv_view_all_comments.setVisibility(View.VISIBLE);
            }


            holder.tv_view_all_comments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, Reviews_comments_all_activity.class);

                    intent.putExtra("posttype", reviewList.get(position).getCategoryType());
                    intent.putExtra("hotelid", reviewList.get(position).getHotelId());
                    intent.putExtra("reviewid", reviewList.get(position).getReviewid());
                    intent.putExtra("userid", reviewList.get(position).getUserId());

                    intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                    intent.putExtra("overallrating", reviewList.get(position).getFoodExprience());

                    if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("totaltimedelivery", reviewList.get(position).getTotalTimedelivery());
                    } else {
                        intent.putExtra("totaltimedelivery", reviewList.get(position).getAmbiance());
                    }

                    intent.putExtra("taste_count", reviewList.get(position).getTaste());
                    intent.putExtra("vfm_rating", reviewList.get(position).getValueMoney());
                    if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("package_rating", reviewList.get(position).getPackage());
                    } else {
                        intent.putExtra("package_rating", reviewList.get(position).getService());
                    }

                    intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                    intent.putExtra("reviewprofile", reviewList.get(position).getRevPicture());
                    intent.putExtra("createdon", reviewList.get(position).getCreatedOn());
                    intent.putExtra("firstname", reviewList.get(position).getRevFirstName());
                    intent.putExtra("lastname", reviewList.get(position).getRevLastName());

                    if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("title_ambience", "Packaging");
                    } else {
                        intent.putExtra("title_ambience", "Hotel Ambience");
                    }

                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);

                    context.startActivity(intent);
                }
            });


            holder.tv_like_count.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, ReviewsLikesAllActivity.class);

                    intent.putExtra("hotelid", reviewList.get(position).getHotelId());
                    intent.putExtra("reviewid", reviewList.get(position).getReviewid());
                    intent.putExtra("userid", reviewList.get(position).getUserId());
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });


            holder.img_btn_review_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(context, Reviews_comments_all_activity.class);


                    intent.putExtra("posttype", reviewList.get(position).getCategoryType());
                    intent.putExtra("hotelid", reviewList.get(position).getHotelId());
                    intent.putExtra("reviewid", reviewList.get(position).getReviewid());
                    intent.putExtra("userid", reviewList.get(position).getUserId());

                    intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                    intent.putExtra("overallrating", reviewList.get(position).getFoodExprience());

                    if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("totaltimedelivery", reviewList.get(position).getTotalTimedelivery());
                    } else {
                        intent.putExtra("totaltimedelivery", reviewList.get(position).getAmbiance());
                    }

                    intent.putExtra("taste_count", reviewList.get(position).getTaste());
                    intent.putExtra("vfm_rating", reviewList.get(position).getValueMoney());
                    if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("package_rating", reviewList.get(position).getPackage());
                    } else {
                        intent.putExtra("package_rating", reviewList.get(position).getService());
                    }

                    intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                    intent.putExtra("reviewprofile", reviewList.get(position).getRevPicture());
                    intent.putExtra("createdon", reviewList.get(position).getCreatedOn());
                    intent.putExtra("firstname", reviewList.get(position).getRevFirstName());
                    intent.putExtra("lastname", reviewList.get(position).getRevLastName());

                    if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("title_ambience", "Packaging");
                    } else {
                        intent.putExtra("title_ambience", "Hotel Ambience");
                    }


                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                }
            });


            holder.username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (reviewList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                }
            });


            holder.userlastname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (reviewList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                }
            });

            holder.userphoto_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (reviewList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                }
            });

            holder.review_user_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (reviewList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                }
            });


            holder.rl_hotel_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, Review_in_detail_activity.class);
                    intent.putExtra("hotelid", reviewList.get(position).getHotelId());
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                }
            });

            holder.ll_delivery_mode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, ReviewDeliveryDetails.class);
                    intent.putExtra("catType", reviewList.get(position).getCategoryType());
                    intent.putExtra("delMode", reviewList.get(position).getDeliveryMode());
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                }
            });


            // Like functionality

            holder.img_btn_review_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);
                    holder.ll_likecomment.setVisibility(VISIBLE);


                    if (reviewList.get(position).isUserLiked()) {


                        try {


                            holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                            reviewList.get(position).setUserLiked(false);
                            reviewList.get(position).setHtlRevLikes(String.valueOf(position));


                            int total_likes = Integer.parseInt(reviewList.get(position).getTotalLikes());

                            if (total_likes == 1) {

                                total_likes = total_likes - 1;
                                reviewList.get(position).setTotalLikes(String.valueOf(total_likes));
                                holder.tv_like_count.setVisibility(View.GONE);


                            } else if (total_likes == 2) {

                                total_likes = total_likes - 1;
                                reviewList.get(position).setTotalLikes(String.valueOf(total_likes));
                                holder.tv_like_count.setText(String.valueOf(total_likes) + " Like");
                                holder.tv_like_count.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes - 1;
                                reviewList.get(position).setTotalLikes(String.valueOf(total_likes));
                                holder.tv_like_count.setText(String.valueOf(total_likes) + " Likes");
                                holder.tv_like_count.setVisibility(View.VISIBLE);

                            }


                            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                            int userid = parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiInterface.create_hotel_review_likes("create_hotel_review_likes", parseInt(reviewList.get(position).getHotelId()), parseInt(reviewList.get(position).getReviewid()), 0, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();
                                    int resposecode = response.body().getResponseCode();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);
                                    Log.e("resposecode", "resposecode->" + resposecode);

                                    if (resposecode == 1) {

                                        //Success

                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {

                        try {


                            holder.ll_likecomment.setVisibility(VISIBLE);
                            holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                            reviewList.get(position).setUserLiked(true);
                            reviewList.get(position).setHtlRevLikes(String.valueOf(1));


                            int total_likes = Integer.parseInt(reviewList.get(position).getTotalLikes());

                            if (total_likes == 0) {

                                total_likes = total_likes + 1;
                                reviewList.get(position).setTotalLikes(String.valueOf(total_likes));
                                holder.tv_like_count.setText(String.valueOf(total_likes) + " Like");
                                holder.tv_like_count.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes + 1;
                                reviewList.get(position).setTotalLikes(String.valueOf(total_likes));
                                holder.tv_like_count.setText(String.valueOf(total_likes) + " Likes");

                            }

                            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

                            int userid = parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiInterface.create_hotel_review_likes("create_hotel_review_likes", parseInt(reviewList.get(position).getHotelId()), parseInt(reviewList.get(position).getReviewid()), 1, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        //Success


                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                    }


                }
            });


            // More settings visibility


            if (reviewList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                holder.img_view_more.setVisibility(VISIBLE);
            } else {
                holder.img_view_more.setVisibility(GONE);
            }

            // More settings

            holder.img_view_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder front_builder = new AlertDialog.Builder(context);
                    front_builder.setItems(itemsothers, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (itemsothers[which].equals("Bucket list options")) {

                                dialog.dismiss();

                                Intent intent = new Intent(context, CreateBucketlistActivty.class);
                                intent.putExtra("f_name", reviewList.get(position).getRevFirstName());
                                intent.putExtra("l_name", reviewList.get(position).getRevLastName());
                                intent.putExtra("reviewid", reviewList.get(position).getReviewid());
                                intent.putExtra("hotelid", reviewList.get(position).getHotelId());
                                intent.putExtra("timelineId", reviewList.get(position).getReviewid());
                                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);

                                context.startActivity(intent);

                            }
                            else if (itemsothers[which].equals("Delete Bucket")) {

                                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Are you sure want to delete this reviewed hotel?")
                                        .setContentText("Won't be able to recover this hotel anymore!")
                                        .setConfirmText("Delete")
                                        .setCancelText("Cancel")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                                try {
                                                    String createuserid = Pref_storage.getDetail(context, "userId");

                                                    String review_id = reviewList.get(position).getReviewid();

                                                    Log.e("reviewid", "onClick: review_id"+review_id);

                                                    Call<CommonOutputPojo> call = apiService.update_delete_hotel_review("update_delete_hotel_review", parseInt(review_id), parseInt(createuserid));
                                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                                        @Override
                                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                            if (response.body().getResponseCode() == 1) {
                                                                reviewList.remove(position);
                                                                notifyItemRemoved(position);
                                                                notifyItemRangeChanged(position, reviewList.size());
                                                                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                                        .setTitleText("Deleted!!")
                                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                            @Override
                                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                                sDialog.dismissWithAnimation();


                                                                            }
                                                                        })
                                                                        .show();


                                                            }

                                                        }

                                                        @Override
                                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                            //Error
                                                            Log.e("FailureError", "" + t.getMessage());
                                                        }
                                                    });
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        })
                                        .show();

                                dialog.dismiss();
                            }

                            else if(itemsothers[which].equals("Share"))
                            {
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("text/plain");
                                i.putExtra(Intent.EXTRA_SUBJECT, "See this Foodwall photo by @xxxx");
                                i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/pdffd/Rgdf78hfhud876");
                                context.startActivity(Intent.createChooser(i, "Share via"));

                            }else if(itemsothers[which].equals("Share Chat"))
                            {

                                Intent intent =new Intent(context, ChatActivity.class);
                                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                            }
                        }
                    });
                    front_builder.show();
                }
            });


            // Adding bucketlist API

            holder.img_bucket_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);

                    Intent intent = new Intent(context, CreateBucketlistActivty.class);
                    intent.putExtra("f_name", reviewList.get(position).getRevFirstName());
                    intent.putExtra("l_name", reviewList.get(position).getRevLastName());
                    intent.putExtra("reviewid", reviewList.get(position).getReviewid());
                    intent.putExtra("hotelid", reviewList.get(position).getHotelId());
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);


                }
            });


            // Load all image Activity

            holder.rl_view_more_top.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);

                    Intent intent = new Intent(context, AllDishDisplayActivity.class);
                    intent.putExtra("reviewid", reviewList.get(position).getReviewid());
                    intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                    intent.putExtra("cat_type", reviewList.get(position).getCategoryType());
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);


                }
            });


            // Load all image Activity

            holder.top_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);
                    Intent intent = new Intent(context, AllDishDisplayActivity.class);
                    intent.putExtra("reviewid", reviewList.get(position).getReviewid());
                    intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                    intent.putExtra("cat_type", reviewList.get(position).getCategoryType());
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);

                    context.startActivity(intent);


                }
            });

            // Load all image Activity

            holder.rl_view_more_avoid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);
                    Intent intent = new Intent(context, AllDishDisplayActivity.class);
                    intent.putExtra("reviewid", reviewList.get(position).getReviewid());
                    intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                }
            });

            // Load all image Activity

            holder.avoid_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);
                    Intent intent = new Intent(context, AllDishDisplayActivity.class);
                    intent.putExtra("reviewid", reviewList.get(position).getReviewid());
                    intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);


                }
            });


            // API for top dish images


            if (reviewList.get(position).getTopCount() != 0) {

                reviewTopDishPojoArrayList.clear();

                ArrayList<ReviewTopDishPojo> reviewTopDishPojoList = new ArrayList<>();

                for (int k = 0; k < reviewList.get(position).getTopCount(); k++) {

                    String topdishimage = reviewList.get(position).getTopdishimage().get(k).getImg();
                    String topdishimagename = reviewList.get(position).getTopdishimage().get(k).getDishname();
                    ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimagename);

                    reviewTopDishPojoList.add(reviewTopDishPojo);


                }

                String review_id = reviewList.get(position).getReviewid();
                String hotelName = reviewList.get(position).getHotelName();
                String catType = reviewList.get(position).getCategoryType();

                ReviewTopDishAdapter reviewTopDishAdapter = new ReviewTopDishAdapter(context, reviewTopDishPojoList, review_id, hotelName, catType);
                LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);
                holder.rv_review_top_dish.setLayoutManager(llm);
//                Collections.reverse(reviewTopDishPojoArrayList);
                holder.rv_review_top_dish.setItemAnimator(new DefaultItemAnimator());
                holder.rv_review_top_dish.setAdapter(reviewTopDishAdapter);
                holder.rv_review_top_dish.setNestedScrollingEnabled(false);
                reviewTopDishAdapter.notifyDataSetChanged();
                holder.rl_top_dish.setVisibility(VISIBLE);

            }

            // Dish to avoid names API

            if (reviewList.get(position).getAvoidCount() != 0) {

                reviewAvoidDishPojoArrayList.clear();

                for (int i = 0; i < reviewList.get(position).getAvoidCount(); i++) {

                    String avoiddishimage = reviewList.get(position).getAvoiddishimage().get(i).getImg();
                    String avoiddishimagename = reviewList.get(position).getAvoiddishimage().get(i).getDishname();

                    ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(avoiddishimage, avoiddishimagename);
                    reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);

                }


                String review_id = reviewList.get(position).getReviewid();
                String hotelName = reviewList.get(position).getHotelName();
                String catType = reviewList.get(position).getCategoryType();


                ReviewAvoidDishAdapter reviewWorstDishAdapter = new ReviewAvoidDishAdapter(context, reviewAvoidDishPojoArrayList, review_id, hotelName, catType);

                LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);
                holder.rv_review_avoid_dish.setLayoutManager(llm);
//                Collections.reverse(reviewAvoidDishPojoArrayList);
                holder.rv_review_avoid_dish.setItemAnimator(new DefaultItemAnimator());
                holder.rv_review_avoid_dish.setAdapter(reviewWorstDishAdapter);
                holder.rv_review_avoid_dish.setNestedScrollingEnabled(false);
                reviewWorstDishAdapter.notifyDataSetChanged();
                holder.rl_avoid_dish.setVisibility(VISIBLE);

            }


            // API for ambience images

            int ambi_image_count = reviewList.get(position).getAmbiImageCount();

            if (ambi_image_count != 0) {

                reviewAmbiImagePojoArrayList.clear();

                for (int m = 0; m < reviewList.get(position).getAmbiImageCount(); m++) {

                    ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(reviewList.get(position).getAvoiddishimage().get(m).getImg());
                    reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);

                }

                String review_id = reviewList.get(position).getReviewid();
                String hotelName = reviewList.get(position).getHotelName();
                String catType = reviewList.get(position).getCategoryType();

                ReviewAmbienceAdapter ambianceImagesAdapter = new ReviewAmbienceAdapter(context, reviewAmbiImagePojoArrayList, review_id, hotelName, catType);

                RecyclerView.LayoutManager llm3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);

                holder.review_photos_recylerview.setLayoutManager(llm3);
//                Collections.reverse(reviewAmbiImagePojoArrayList);
                holder.review_photos_recylerview.setItemAnimator(new DefaultItemAnimator());
                holder.review_photos_recylerview.setAdapter(ambianceImagesAdapter);
                holder.review_photos_recylerview.setNestedScrollingEnabled(false);
                ambianceImagesAdapter.notifyDataSetChanged();
                holder.ll_view_more.setVisibility(VISIBLE);
                holder.ll_view_more.setVisibility(VISIBLE);


                if (reviewAmbiImagePojoArrayList.size() == 0) {
                    holder.ambi_viewpager.setVisibility(View.GONE);
                    holder.rl_ambiance_dot.setVisibility(View.GONE);
                    holder.title_ambience.setVisibility(GONE);
                    holder.ll_ambi_more.setVisibility(GONE);
                    holder.ll_view_more.setVisibility(GONE);
                    holder.review_photos_recylerview.setVisibility(GONE);
                } else {


                }

            }

            //package images
            int package_image_count = reviewList.get(position).getPackImageCount();
             ArrayList<PackImage> reviewPackImagePojoArrayList = new ArrayList<>();

            if (package_image_count != 0) {

                reviewPackImagePojoArrayList.clear();

                for (int m = 0; m < reviewList.get(position).getPackImage().size(); m++) {

                    PackImage reviewAmbiImagePojo = new PackImage(reviewList.get(position).getPackImage().get(m).getImg());
                    reviewPackImagePojoArrayList.add(reviewAmbiImagePojo);

                }

                String review_id = reviewList.get(position).getReviewid();
                String hotelName = reviewList.get(position).getHotelName();
                String catType = reviewList.get(position).getCategoryType();

                PackagingDetailsAdapter packagingDetailsAdapter = new PackagingDetailsAdapter(context, reviewPackImagePojoArrayList, review_id, hotelName, catType);

                RecyclerView.LayoutManager llm3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);

                holder.package_recylerview.setLayoutManager(llm3);
//                Collections.reverse(reviewAmbiImagePojoArrayList);
                holder.package_recylerview.setItemAnimator(new DefaultItemAnimator());
                holder.package_recylerview.setAdapter(packagingDetailsAdapter);
                holder.package_recylerview.setNestedScrollingEnabled(false);
                packagingDetailsAdapter.notifyDataSetChanged();
                holder.ll_view_more.setVisibility(VISIBLE);


                if (reviewPackImagePojoArrayList.size() == 0) {
                    holder.ambi_viewpager.setVisibility(View.GONE);
                    holder.rl_ambiance_dot.setVisibility(View.GONE);
                    holder.title_ambience.setVisibility(GONE);
                    holder.ll_ambi_more.setVisibility(GONE);
                    holder.ll_view_more.setVisibility(GONE);
                    holder.review_photos_recylerview.setVisibility(GONE);
                } else {


                }

            }



            // Hotel Images

            holder.ll_view_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    holder.ll_ambi_more.setVisibility(VISIBLE);
                    holder.ll_view_more.setVisibility(GONE);

                }
            });

            holder.layout_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.ll_ambi_more.setVisibility(VISIBLE);
                    holder.ll_view_more.setVisibility(GONE);

                }
            });


        }


    }


    @Override
    public int getItemCount() {




    /*    if(getallHotelReviewPojoArrayList.size()==0){
            listsize.checksize(getallHotelReviewPojoArrayList.size());
        }else {
            listsize.checksize(getallHotelReviewPojoArrayList.size());
        }
*/


        return getallHotelReviewPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        EmojiconEditText review_comment;

        ViewPager ambi_viewpager;

        ExpandableTextView reviewcmmnt;

        CircleIndicator ambi_indicator;

        RecyclerView package_recylerview,review_photos_recylerview, rv_review_top_dish, rv_review_avoid_dish;

        ImageView userphoto_profile, review_user_photo, img_overall_rating_star,
                img_ambience_star, img_taste_star, img_vfm_star, img_service_star;

        RelativeLayout rl_overall_review,rl_ambiance_dot, rl_hotel_details, rl_avoid_dish, rl_top_dish, rl_view_more_top, rl_view_more_avoid;

        LinearLayout ll_ambi_more, ll_view_more, ll_dish_to_avoid, ll_likecomment, ll_res_overall_rating, ll_ambience_rating, ll_taste_rating,
                ll_value_of_money_rating, ll_service_rating, ll_delivery_mode, ll_review_headline, ll_review_main,rl_username;

        ImageButton img_btn_review_like, layout_more, img_btn_review_comment, img_bucket_list, img_view_more, avoid_more, top_more;

        // TextView

        TextView username, txt_mode_delivery, userlastname, count, count_post, comma, txt_followers_count, txt_followers, serv_or_package,
                txt_created_on, tv_restaurant_name, txt_hotel_address, tv_res_overall_rating, tv_ambience_rating, ambi_or_timedelivery,
                txt_taste_count, tv_vfm_rating, tv_service_rating, title_ambience, tv_view_more, tv_top_dishes,
                tv_dishes_to_avoid, title_dish_avoid, title_top_dish, tv_show_less, txt_review_caption,
                tv_like_count, tv_view_all_comments, txt_adapter_post, tv_headline_username, tv_review_headlinetext;


        onCommentsPostListener onCommentsPostListener;
        View viewItems;
        public MyViewHolder(View itemView, com.kaspontech.foodwall.onCommentsPostListener onCommentsPostListener) {

            super(itemView);

            this.onCommentsPostListener=onCommentsPostListener;
            this.viewItems=itemView;


            username = (TextView) itemView.findViewById(R.id.username);
            txt_mode_delivery = (TextView) itemView.findViewById(R.id.txt_mode_delivery);
            userlastname = (TextView) itemView.findViewById(R.id.userlastname);
            count = (TextView) itemView.findViewById(R.id.count);
            count_post = (TextView) itemView.findViewById(R.id.count_post);
            comma = (TextView) itemView.findViewById(R.id.comma);
            txt_followers_count = (TextView) itemView.findViewById(R.id.txt_followers_count);
            txt_followers = (TextView) itemView.findViewById(R.id.txt_followers);
            txt_created_on = (TextView) itemView.findViewById(R.id.txt_created_on);
            tv_restaurant_name = (TextView) itemView.findViewById(R.id.tv_restaurant_name);
            txt_hotel_address = (TextView) itemView.findViewById(R.id.txt_hotel_address);
            tv_res_overall_rating = (TextView) itemView.findViewById(R.id.tv_res_overall_rating);
            tv_ambience_rating = (TextView) itemView.findViewById(R.id.tv_ambience_rating);
            txt_taste_count = (TextView) itemView.findViewById(R.id.txt_taste_count);
            tv_ambience_rating = (TextView) itemView.findViewById(R.id.tv_ambience_rating);
            ambi_or_timedelivery = (TextView) itemView.findViewById(R.id.ambi_or_timedelivery);
            serv_or_package = (TextView) itemView.findViewById(R.id.serv_or_package);
            tv_vfm_rating = (TextView) itemView.findViewById(R.id.tv_vfm_rating);
            tv_service_rating = (TextView) itemView.findViewById(R.id.tv_service_rating);
            tv_view_more = (TextView) itemView.findViewById(R.id.tv_view_more);
            title_ambience = (TextView) itemView.findViewById(R.id.title_ambience);
            tv_top_dishes = (TextView) itemView.findViewById(R.id.tv_top_dishes);
            tv_dishes_to_avoid = (TextView) itemView.findViewById(R.id.tv_dishes_to_avoid);
            title_dish_avoid = (TextView) itemView.findViewById(R.id.title_dish_avoid);
            title_top_dish = (TextView) itemView.findViewById(R.id.title_top_dish);
            tv_show_less = (TextView) itemView.findViewById(R.id.tv_show_less);
            tv_like_count = (TextView) itemView.findViewById(R.id.tv_like_count);
            tv_view_all_comments = (TextView) itemView.findViewById(R.id.tv_view_all_comments);
            txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);


            txt_hotel_address = (TextView) itemView.findViewById(R.id.txt_hotel_address);


            ambi_viewpager = (ViewPager) itemView.findViewById(R.id.ambi_viewpager);

            ambi_indicator = (CircleIndicator) itemView.findViewById(R.id.ambi_indicator);
            review_comment = (EmojiconEditText) itemView.findViewById(R.id.review_comment);
            reviewcmmnt = (ExpandableTextView) itemView.findViewById(R.id.hotel_revieww_description);


            txt_review_caption = (TextView) itemView.findViewById(R.id.tv_review_caption);

            //Imageview

            userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);

            img_overall_rating_star = (ImageView) itemView.findViewById(R.id.img_overall_rating_star);
            img_ambience_star = (ImageView) itemView.findViewById(R.id.img_ambience_star);
            img_taste_star = (ImageView) itemView.findViewById(R.id.img_taste_star);
            img_vfm_star = (ImageView) itemView.findViewById(R.id.img_vfm_star);
            img_service_star = (ImageView) itemView.findViewById(R.id.img_service_star);
            userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);
            review_user_photo = (ImageView) itemView.findViewById(R.id.review_user_photo);

            //ImageButton
            img_btn_review_like = (ImageButton) itemView.findViewById(R.id.img_btn_review_like);
            img_btn_review_comment = (ImageButton) itemView.findViewById(R.id.img_btn_review_comment);
            img_view_more = (ImageButton) itemView.findViewById(R.id.img_view_more);
            img_bucket_list = (ImageButton) itemView.findViewById(R.id.img_bucket_list);
            top_more = (ImageButton) itemView.findViewById(R.id.top_more);
            avoid_more = (ImageButton) itemView.findViewById(R.id.avoid_more);
            layout_more = (ImageButton) itemView.findViewById(R.id.layout_more);


            rl_overall_review = (RelativeLayout) itemView.findViewById(R.id.rl_overall_review);
            ll_likecomment = (LinearLayout) itemView.findViewById(R.id.ll_likecomment);
            ll_res_overall_rating = (LinearLayout) itemView.findViewById(R.id.ll_res_overall_rating);
            ll_dish_to_avoid = (LinearLayout) itemView.findViewById(R.id.ll_dish_to_avoid);
            ll_ambience_rating = (LinearLayout) itemView.findViewById(R.id.ll_ambience_rating);
            ll_taste_rating = (LinearLayout) itemView.findViewById(R.id.ll_taste_rating);
            ll_value_of_money_rating = (LinearLayout) itemView.findViewById(R.id.ll_value_of_money_rating);
            ll_service_rating = (LinearLayout) itemView.findViewById(R.id.ll_service_rating);
            ll_view_more = (LinearLayout) itemView.findViewById(R.id.ll_view_more);
            ll_ambi_more = (LinearLayout) itemView.findViewById(R.id.ll_ambi_more);
            ll_review_main = (LinearLayout) itemView.findViewById(R.id.ll_review_main);
            ll_delivery_mode = (LinearLayout) itemView.findViewById(R.id.ll_delivery_mode);
            rl_username = (LinearLayout) itemView.findViewById(R.id.rl_username);


            package_recylerview = (RecyclerView) itemView.findViewById(R.id.package_recylerview);
            review_photos_recylerview = (RecyclerView) itemView.findViewById(R.id.review_photos_recylerview);
            rv_review_top_dish = (RecyclerView) itemView.findViewById(R.id.rv_review_top_dish);
            rv_review_avoid_dish = (RecyclerView) itemView.findViewById(R.id.rv_review_avoid_dish);

            rl_ambiance_dot = (RelativeLayout) itemView.findViewById(R.id.rl_ambiance_dot);
            rl_hotel_details = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_details);
            rl_avoid_dish = (RelativeLayout) itemView.findViewById(R.id.rl_avoid_dish);
            rl_top_dish = (RelativeLayout) itemView.findViewById(R.id.rl_top_dish);
            rl_view_more_top = (RelativeLayout) itemView.findViewById(R.id.rl_view_more_top);

            rl_view_more_avoid = (RelativeLayout) itemView.findViewById(R.id.rl_view_more_avoid);
            txt_adapter_post.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId())
            {
                case R.id.txt_adapter_post:
                    onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),viewItems);
                    break;
            }

        }
    }


}


