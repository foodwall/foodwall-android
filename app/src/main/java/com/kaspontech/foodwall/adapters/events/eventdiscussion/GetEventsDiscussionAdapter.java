package com.kaspontech.foodwall.adapters.events.eventdiscussion;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.eventspackage.CreateEventDiscussion;
import com.kaspontech.foodwall.eventspackage.ViewDiscussionComments;
import com.kaspontech.foodwall.eventspackage.ViewDisussionLikes;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetEventDiscussionData;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class GetEventsDiscussionAdapter extends RecyclerView.Adapter<GetEventsDiscussionAdapter.MyViewHolder> {

    /**
     * Context
     **/
    Context context;

    /**
     * Event discussion arraylist and pojo
     **/
    List<GetEventDiscussionData> getEventDiscussionDataList = new ArrayList<>();
    GetEventDiscussionData getEventDiscussionData;

    public GetEventsDiscussionAdapter(Context context, List<GetEventDiscussionData> getEventDiscussionDataList) {
        this.context = context;
        this.getEventDiscussionDataList = getEventDiscussionDataList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_events_discussion, parent, false);
        MyViewHolder vh = new MyViewHolder(itemView);

//        vh.setIsRecyclable(true);
        return new MyViewHolder(itemView);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        EmojiconEditText edittxt_comment;
        RelativeLayout rl_post_timeline, rl_comments_layout, rl_more, rl_likes_layout;
        CircleImageView user_image, userphoto_comment;
        TextView user_name, commented_time, event_discussion_description, View_all_comment, commenttest,
                event_discussion_like_count, txt_adapter_post, comment;
        ImageButton img_view_more, event_discussion_like, event_discussion_comment;
        ImageView event_discussion_image;


        public MyViewHolder(View itemView) {
            super(itemView);

            user_image = (CircleImageView) itemView.findViewById(R.id.user_image);
            user_name = (TextView) itemView.findViewById(R.id.user_name);
            comment = (TextView) itemView.findViewById(R.id.comment);
            commented_time = (TextView) itemView.findViewById(R.id.commented_time);
            View_all_comment = (TextView) itemView.findViewById(R.id.View_all_comment);
            commenttest = (TextView) itemView.findViewById(R.id.commenttest);
            txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);
            event_discussion_description = (TextView) itemView.findViewById(R.id.event_discussion_description);
            event_discussion_like_count = (TextView) itemView.findViewById(R.id.event_discussion_like_count);
            img_view_more = (ImageButton) itemView.findViewById(R.id.img_view_more);
            event_discussion_like = (ImageButton) itemView.findViewById(R.id.event_discussion_like);
            event_discussion_comment = (ImageButton) itemView.findViewById(R.id.event_discussion_comment);
            event_discussion_image = (ImageView) itemView.findViewById(R.id.event_discussion_image);


            edittxt_comment = (EmojiconEditText) itemView.findViewById(R.id.edittxt_comment);
            rl_post_timeline = (RelativeLayout) itemView.findViewById(R.id.rl_post_timeline);
            rl_comments_layout = (RelativeLayout) itemView.findViewById(R.id.rl_comments_layout);
            rl_more = (RelativeLayout) itemView.findViewById(R.id.rl_more);
            rl_likes_layout = (RelativeLayout) itemView.findViewById(R.id.rl_likes_layout);
            userphoto_comment = (CircleImageView) itemView.findViewById(R.id.userphoto_comment);


        }
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        getEventDiscussionData = getEventDiscussionDataList.get(position);

        int totalLikes = getEventDiscussionDataList.get(position).getTotalLikes();
        int totalComments = getEventDiscussionDataList.get(position).getTotalComments();


            /*User image loading*/
        Utility.picassoImageLoader(getEventDiscussionData.getPicture(),0,holder.user_image, context);



        String userPhoto = Pref_storage.getDetail(context, "picture_user_login");

        /*User self imoage loading*/
        Utility.picassoImageLoader(userPhoto,0,holder.userphoto_comment,context );


        /*User name setting*/

        holder.user_name.setText(getEventDiscussionData.getFirstName() + " " + getEventDiscussionData.getLastName());

        /*Event discussion setting*/
        holder.event_discussion_description.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getEventDiscussionData.getDisDescription()));
        String userid = Pref_storage.getDetail(context, "userId");
        int createdByid = getEventDiscussionDataList.get(position).getCreatedBy();

        Log.e("TestingLike", "TestingLike->" + totalLikes);


        if (totalLikes != 0) {

            holder.rl_likes_layout.setVisibility(VISIBLE);

            if (totalLikes == 1) {


                holder.event_discussion_like_count.setText(String.valueOf(totalLikes) + " Like");

            } else {

                holder.event_discussion_like_count.setText(String.valueOf(totalLikes) + " Likes");

            }

        } else {

            holder.event_discussion_like_count.setVisibility(GONE);
        }

        if (totalComments != 0) {


            holder.rl_comments_layout.setVisibility(View.VISIBLE);


        } else {

            holder.rl_comments_layout.setVisibility(View.GONE);

        }

        try {

            int comment = getEventDiscussionDataList.get(position).getTotalComments();

            if (comment == 0) {

                holder.rl_comments_layout.setVisibility(GONE);

            } else {

                holder.rl_comments_layout.setVisibility(VISIBLE);
                holder.comment.setText(String.valueOf(comment));

                if (comment == 1) {

                    holder.View_all_comment.setText(R.string.view);
                    holder.commenttest.setText(R.string.commenttt);
                    holder.View_all_comment.setVisibility(VISIBLE);
                    holder.commenttest.setVisibility(VISIBLE);
                    holder.comment.setVisibility(VISIBLE);


                } else if (comment > 1) {
                    holder.View_all_comment.setVisibility(VISIBLE);
                    holder.commenttest.setVisibility(VISIBLE);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.edittxt_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (holder.edittxt_comment.getText().toString().trim().length() != 0) {
                    holder.txt_adapter_post.setVisibility(VISIBLE);
                } else {
                    holder.txt_adapter_post.setVisibility(GONE);
                }


            }
        });


        holder.txt_adapter_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.edittxt_comment.getText().toString().length() != 0) {

                    int Cmnt = getEventDiscussionDataList.get(position).getTotalComments() + 1;
                    holder.comment.setText(String.valueOf(Cmnt));
                    holder.comment.setVisibility(VISIBLE);
                    if (Cmnt == 1) {
                        holder.View_all_comment.setText(R.string.view);
                        holder.View_all_comment.setVisibility(VISIBLE);
                        holder.commenttest.setText(R.string.commenttt);
                        holder.commenttest.setVisibility(VISIBLE);
                    } else if (Cmnt > 1) {
                        holder.View_all_comment.setVisibility(VISIBLE);
                        holder.commenttest.setVisibility(VISIBLE);
                    }

                    if(Utility.isConnected(context)){
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    int disId = getEventDiscussionDataList.get(position).getDisEvtId();

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        String comment = org.apache.commons.text.StringEscapeUtils.escapeJava(holder.edittxt_comment.getText().toString().trim());

                        Call<CommonOutputPojo> call = apiService.createEditEventDiscussionComment("create_edit_delete_events_discussion_comments", 0, disId,comment , userid, 0);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {

                                    //Success
                                    holder.txt_adapter_post.setVisibility(GONE);
                                    holder.edittxt_comment.setText("");
                                    holder.rl_comments_layout.setVisibility(VISIBLE);
                                    Intent intent = new Intent(context, ViewDiscussionComments.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putInt("disId", getEventDiscussionDataList.get(position).getDisEvtId());
                                    intent.putExtras(bundle);
                                    context.startActivity(intent);

                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }}else{
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }


                }

            }
        });

        holder.user_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userId = String.valueOf(getEventDiscussionDataList.get(position).getUserId());

                if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    context.startActivity(new Intent(context, Profile.class));

                } else {
                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", userId);
                    context.startActivity(intent);

                }

            }
        });


        if (userid.equals(String.valueOf(createdByid))) {

            holder.rl_more.setVisibility(View.VISIBLE);

        } else {

            holder.rl_more.setVisibility(View.GONE);

        }

        /*More click listener*/
        holder.img_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int event_id = getEventDiscussionDataList.get(position).getEventId();
                int discuss_id = getEventDiscussionDataList.get(position).getDisEvtId();
                int createdby = getEventDiscussionDataList.get(position).getCreatedBy();
                String eventName = getEventDiscussionDataList.get(position).getEventName();
                String disImage = getEventDiscussionDataList.get(position).getDisImage();
                String disCaption = getEventDiscussionDataList.get(position).getDisDescription();

                CustomDialogClass ccd = new CustomDialogClass(context, discuss_id, event_id, position, createdby, eventName, disImage, disCaption);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccd.setCancelable(true);
                ccd.show();
            }
        });

        if (getEventDiscussionDataList.get(position).getDisLikes() == 0) {

            getEventDiscussionDataList.get(position).setLiked(false);
            holder.event_discussion_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));


        } else {

            getEventDiscussionDataList.get(position).setLiked(true);
            holder.event_discussion_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));


        }


        if (getEventDiscussionData != null) {

            String createdon = getEventDiscussionData.getCreatedOn();

            Utility.setTimeStamp(createdon,holder.commented_time);
            Log.e("createdon", "create" + createdon);


           /* try {
                long now = System.currentTimeMillis();

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                Date convertedDate = dateFormat.parse(createdon);

                CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                        convertedDate.getTime(),
                        now,
                        DateUtils.FORMAT_ABBREV_RELATIVE);
                if (relavetime1.toString().equalsIgnoreCase("0 minutes ago") || relavetime1.toString().equalsIgnoreCase("In 0 minutes")) {
                    holder.commented_time.setText(R.string.just_now);
                } else {
                    holder.commented_time.append(relavetime1 + "\n");
                }


            } catch (ParseException e) {
                e.printStackTrace();
            }*/
        }

        if (getEventDiscussionDataList.get(position).getDisImage().equals("0")) {

            holder.event_discussion_image.setVisibility(View.GONE);

        } else {

            holder.event_discussion_image.setVisibility(View.VISIBLE);

            /*Event image loading*/
            Utility.picassoImageLoader(getEventDiscussionData.getDisImage(),2,holder.event_discussion_image,context );


        }
        /*Disscussion like click listener*/

        holder.event_discussion_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);

                if (getEventDiscussionDataList.get(position).isLiked()) {

                    holder.event_discussion_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                    getEventDiscussionDataList.get(position).setLiked(false);
                    getEventDiscussionDataList.get(position).setDisLikes(0);


                    int total_likes = getEventDiscussionDataList.get(position).getTotalLikes();

                    if (total_likes == 1) {

                        total_likes = total_likes - 1;
                        getEventDiscussionDataList.get(position).setTotalLikes(total_likes);
                        holder.event_discussion_like_count.setVisibility(View.GONE);


                    } else if (total_likes == 2) {

                        total_likes = total_likes - 1;
                        getEventDiscussionDataList.get(position).setTotalLikes(total_likes);
                        holder.event_discussion_like_count.setText(String.valueOf(total_likes) + " Like");
                        holder.event_discussion_like_count.setVisibility(View.VISIBLE);

                    } else {

                        total_likes = total_likes - 1;
                        getEventDiscussionDataList.get(position).setTotalLikes(total_likes);
                        holder.event_discussion_like_count.setText(String.valueOf(total_likes) + " Likes");
                        holder.event_discussion_like_count.setVisibility(View.VISIBLE);

                    }

                    if(Utility.isConnected(context)){

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<CommonOutputPojo> call = apiService.createEventsDiscussionLikes("create_events_discussion_likes", getEventDiscussionDataList.get(position).getDisEvtId(), 0, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("nodata")) {

                                        //Success


                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }}else{
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    holder.event_discussion_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                    getEventDiscussionDataList.get(position).setLiked(true);
                    getEventDiscussionDataList.get(position).setDisLikes(1);

                    int total_likes = getEventDiscussionDataList.get(position).getTotalLikes();

                    if (total_likes == 0) {

                        total_likes = total_likes + 1;
                        getEventDiscussionDataList.get(position).setTotalLikes(total_likes);
                        holder.event_discussion_like_count.setText(String.valueOf(total_likes) + " Like");
                        holder.event_discussion_like_count.setVisibility(View.VISIBLE);


                    } else {

                        total_likes = total_likes + 1;
                        getEventDiscussionDataList.get(position).setTotalLikes(total_likes);
                        holder.event_discussion_like_count.setText(String.valueOf(total_likes) + " Likes");

                    }

                    if(Utility.isConnected(context)){
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<CommonOutputPojo> call = apiService.createEventsDiscussionLikes("create_events_discussion_likes", getEventDiscussionDataList.get(position).getDisEvtId(), 1, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        //Success


                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                }

                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }}else{
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

        /*Event discussion comment on click listener*/
        holder.event_discussion_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ViewDiscussionComments.class);
                Bundle bundle = new Bundle();
                bundle.putInt("disId", getEventDiscussionDataList.get(position).getDisEvtId());
                intent.putExtras(bundle);
                context.startActivity(intent);

            }
        });

    /*Event discussion on click listener*/
  holder.event_discussion_like_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ViewDisussionLikes.class);
                Bundle bundle = new Bundle();
                bundle.putInt("discuss_evnt_id", getEventDiscussionDataList.get(position).getDisEvtId());
                intent.putExtras(bundle);
                context.startActivity(intent);



            }
        });

        /*Comments on click listener*/
        holder.rl_comments_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ViewDiscussionComments.class);
                Bundle bundle = new Bundle();
                bundle.putInt("disId", getEventDiscussionDataList.get(position).getDisEvtId());
                intent.putExtras(bundle);
                context.startActivity(intent);

            }
        });

    }

    /*Custom alert click listener*/

    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity c;
        public Dialog d;
        String eventName, disImage, disCaption;
        int discss_id, event_id, position, createdby;
        public AppCompatButton btn_Ok;
        TextView txt_deletepost, txt_report, txt_copylink, txt_turnoffpost, txt_sharevia, txt_editpost;

        CustomDialogClass(Context a, int discss_id, int event_id, int position, int createdby, String eventName, String disImage, String disCaption) {
            super(a);
            context = a;
            this.event_id = event_id;
            this.discss_id = discss_id;
            this.position = position;
            this.createdby = createdby;
            this.eventName = eventName;
            this.disImage = disImage;
            this.disCaption = disCaption;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
//            setContentView(R.layout.dialog_version_info);

            setContentView(R.layout.custom_dialog_timeline);

            txt_deletepost = (TextView) findViewById(R.id.txt_deletepost);
            txt_editpost = (TextView) findViewById(R.id.txt_editpost);
            txt_report = (TextView) findViewById(R.id.txt_report);
            txt_copylink = (TextView) findViewById(R.id.txt_copylink);
            txt_turnoffpost = (TextView) findViewById(R.id.txt_turnoffpost);
            txt_sharevia = (TextView) findViewById(R.id.txt_sharevia);
            txt_deletepost.setOnClickListener(this);
            txt_editpost.setOnClickListener(this);
            txt_report.setOnClickListener(this);
            txt_copylink.setOnClickListener(this);
            txt_turnoffpost.setOnClickListener(this);
            txt_sharevia.setOnClickListener(this);

            if (String.valueOf(createdby).equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                txt_deletepost.setVisibility(VISIBLE);
                txt_editpost.setVisibility(VISIBLE);
            } else {
                txt_deletepost.setVisibility(GONE);
                txt_editpost.setVisibility(GONE);
            }

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_editpost:

                    Intent intent1 = new Intent(context, CreateEventDiscussion.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("eventId", event_id);
                    bundle.putInt("launchType", 2);
                    bundle.putInt("discuss_id", discss_id);
                    bundle.putString("eventName", eventName);
                    bundle.putString("disCaption", disCaption);
                    bundle.putString("disImage", disImage);
                    intent1.putExtras(bundle);
                    context.startActivity(intent1);
                    ((Activity) context).finish();
                    dismiss();


                    break;

                case R.id.txt_deletepost:


                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to delete this post?")
                            .setContentText("Won't be able to recover this post anymore!")
                            .setConfirmText("Yes,delete it!")
                            .setCancelText("No,cancel!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                    createDeleteDiscussion(discss_id, event_id, position);

                                }
                            })
                            .show();
                    dismiss();
                    break;

                case R.id.txt_report:
                    dismiss();
                    break;

                /*case R.id.txt_copylink:
                    Toast.makeText(context, "Link has been copied to clipboard", Toast.LENGTH_SHORT).show();
                    dismiss();
                    break;
                case R.id.txt_turnoffpost:
                    dismiss();
                    break;
                case R.id.txt_sharevia:
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                    i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                    context.startActivity(Intent.createChooser(i, "Share via"));
                    dismiss();
                    break;*/

                default:
                    break;
            }
        }
    }

    /*Delete discussions on api calling*/
    private void createDeleteDiscussion(int discss_id, int event_id, final int position) {

        if(Utility.isConnected(context)){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            String createdby = Pref_storage.getDetail(context, "userId");

            Call<CommonOutputPojo> call = apiService.deleteEventDiscussion("create_edit_delete_events_discussion", discss_id, event_id, Integer.parseInt(createdby), 1);
            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    if (response.body() != null) {

                        Log.e("ResponseStatus", "ResponseStatus->" + response.body().getResponseMessage());

                        if (response.body().getResponseMessage().equals("success")) {
                            /*Remove postion*/
                            removeAt(position);
                            Toast.makeText(context, "Deleted!!", Toast.LENGTH_LONG).show();

//                            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//                                    .setTitleText("Deleted!!")
//                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                        @Override
//                                        public void onClick(SweetAlertDialog sDialog) {
//                                            sDialog.dismissWithAnimation();
//
//
//                                        }
//                                    })
//                                    .show();


                        }

                    }


                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }}else {
            Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }


    }
    /*Remove postion*/
    public void removeAt(int position) {
        getEventDiscussionDataList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getEventDiscussionDataList.size());
    }


    @Override
    public int getItemCount() {
        return getEventDiscussionDataList.size();
    }


}
