package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.support.design.widget.BottomSheetDialog;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.utills.ClickableViewPager;
import com.kassisdion.library.ViewPagerWithIndicator;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import me.relex.circleindicator.CircleIndicator;

public class TimelineAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {

    /**
     * Bottom sheet dialog
     **/
    BottomSheetDialog dialog;

    PhotoView image;

    EmojiconEditText edittxtComment;

    ImageButton img_view_more;

    ImageButton img_like;

    ImageButton img_commentadapter;

    TextView username;
    TextView location;
    TextView location_state;
    TextView like;
    TextView liketxt;
    TextView user_txt_comment;
    TextView count;
    TextView comma;
    TextView count_post;
    TextView txt_followers_count;
    TextView txt_followers;
    TextView selfusername_caption;
    TextView selfuserlastname_caption;
    TextView user_name_comment;
    TextView viewcomment;
    TextView txt_timeline_follow;
    TextView comment,recently_commented,username_latestviewed,created_on;
    ImageView userphoto;
    RelativeLayout recently_commented_ll;

    TextView commenttest;
    TextView share;
    TextView caption;
    TextView captiontag;
    TextView txt_created_on;
    TextView txt_adapter_post;

    RelativeLayout rl_likes_layout;
    RelativeLayout rl_comments_lay;
    RelativeLayout rl_comment_share;
    RelativeLayout rl_username;
    RelativeLayout rl_share_adapter;
    RelativeLayout rl_comment_adapter;
    RelativeLayout like_rl;
    RelativeLayout rl_more, rl_post_timeline;
    RelativeLayout rl_follow_layout;
    RelativeLayout rl_newlike_adapter;
    LinearLayout rl_row_layout;
   ImageView ll_prev,ll_next;
    LinearLayout rl_fullimage;
    ImageView img_follow_timeline;
    ImageView img_shareadapter;
    ImageView userphoto_comment,img_timelinesharechat;
    CircleImageView userphoto_profile;
    // Like bottom sheet dialog
    BottomSheetDialog likeBottomdialog;
   // ViewPagerWithIndicator mViewPagerWithIndicator;

    ImageView prev_img,next_img;

    ProgressBar comments_progressbar;

    public int NUM_PAGES = 0;
    public int currentPage = 0;
     ViewPager mviewpager;


    private static final String TAG = "TimelineAdapter";


    public CircleIndicator circleIndicatortimeline;

    private onCommentsPostListener  onCommentsPostListener;
    private View itemView;

    public TimelineAdapter(View itemView, onCommentsPostListener onCommentsPostListener) {
        super(itemView);


        this.onCommentsPostListener = onCommentsPostListener;
        this.itemView = itemView;

        //ViewPager
        //mviewpager = (ViewPager) itemView.findViewById(R.id.mviewpager);
        mviewpager = (ViewPager) itemView.findViewById(R.id.mviewpager);
        //mViewPagerWithIndicator = (ViewPagerWithIndicator) itemView.findViewById(R.id.viewPagerWithIndicator);


        //Edittext
        edittxtComment = (EmojiconEditText) itemView.findViewById(R.id.edittxt_comment);


        //Circle Indicator
        circleIndicatortimeline = (CircleIndicator) itemView.findViewById(R.id.indicator);


        //ImageView
        image = (PhotoView) itemView.findViewById(R.id.image);
        img_like = (ImageButton) itemView.findViewById(R.id.img_like);
        img_view_more = (ImageButton) itemView.findViewById(R.id.img_view_more);
        img_shareadapter = (ImageView) itemView.findViewById(R.id.img_shareadapter);
        userphoto_profile = (CircleImageView) itemView.findViewById(R.id.userphoto_profile);
        userphoto_comment = (ImageView) itemView.findViewById(R.id.userphoto_comment);
        img_commentadapter = (ImageButton) itemView.findViewById(R.id.img_commentadapter);
        img_follow_timeline = (ImageView) itemView.findViewById(R.id.img_follow_timeline);
        img_timelinesharechat = (ImageView) itemView.findViewById(R.id.img_timelinesharechat);
        prev_img = (ImageView) itemView.findViewById(R.id.prev_img);
        next_img = (ImageView) itemView.findViewById(R.id.next_img);


        like = (TextView) itemView.findViewById(R.id.like);
        comma = (TextView) itemView.findViewById(R.id.comma);
        count = (TextView) itemView.findViewById(R.id.count);
        share = (TextView) itemView.findViewById(R.id.shares);
        caption = (TextView) itemView.findViewById(R.id.caption);
        liketxt = (TextView) itemView.findViewById(R.id.liketxt);
        comment = (TextView) itemView.findViewById(R.id.comment);
         //to load
        recently_commented = (TextView) itemView.findViewById(R.id.recently_commented);
        username_latestviewed = (TextView) itemView.findViewById(R.id.username_latestviewed);
        created_on = (TextView) itemView.findViewById(R.id.created_on);
        userphoto = (ImageView) itemView.findViewById(R.id.userphoto);
        recently_commented_ll = (RelativeLayout) itemView.findViewById(R.id.recently_commented_ll);


        username = (TextView) itemView.findViewById(R.id.username);
        location = (TextView) itemView.findViewById(R.id.location);
        captiontag = (TextView) itemView.findViewById(R.id.captiontag);
        commenttest = (TextView) itemView.findViewById(R.id.commenttest);
        viewcomment = (TextView) itemView.findViewById(R.id.View_all_comment);
        txt_created_on = (TextView) itemView.findViewById(R.id.txt_created_on);
        txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);
        user_txt_comment = (TextView) itemView.findViewById(R.id.user_txt_comment);
        user_name_comment = (TextView) itemView.findViewById(R.id.user_name_comment);
        txt_timeline_follow = (TextView) itemView.findViewById(R.id.txt_timeline_follow);
        selfusername_caption = (TextView) itemView.findViewById(R.id.selfusername_caption);
        selfuserlastname_caption = (TextView) itemView.findViewById(R.id.selfuserlastname_caption);


        count_post = (TextView) itemView.findViewById(R.id.count_post);
        txt_followers = (TextView) itemView.findViewById(R.id.txt_followers);
        txt_followers_count = (TextView) itemView.findViewById(R.id.txt_followers_count);

        //RelativeLayout
        rl_more = (RelativeLayout) itemView.findViewById(R.id.rl_more);
        like_rl = (RelativeLayout) itemView.findViewById(R.id.like_rl);
        rl_username = (RelativeLayout) itemView.findViewById(R.id.rl_username);
        rl_fullimage = (LinearLayout) itemView.findViewById(R.id.rl_fullimage);
        rl_likes_layout = (RelativeLayout) itemView.findViewById(R.id.rl_likes_layout);
        rl_follow_layout = (RelativeLayout) itemView.findViewById(R.id.rl_follow_layout);
        rl_post_timeline = (RelativeLayout) itemView.findViewById(R.id.rl_post_timeline);
        rl_share_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_share_adapter);
        rl_row_layout = (LinearLayout) itemView.findViewById(R.id.layout_one_foradapter);
        rl_comments_lay = (RelativeLayout) itemView.findViewById(R.id.rl_comments_layout);
        rl_comment_share = (RelativeLayout) itemView.findViewById(R.id.rl_comment_share_timeline);
        rl_comment_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_comment_adapter);
      //  ll_next = (ImageView) itemView.findViewById(R.id.ll_next);
       // ll_prev = (ImageView) itemView.findViewById(R.id.ll_prev);

        comments_progressbar = (ProgressBar) itemView.findViewById(R.id.comments_progressbar);

        txt_adapter_post.setOnClickListener(this);
        img_timelinesharechat.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.txt_adapter_post:

                onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),itemView);
                break;

            case R.id.img_timelinesharechat:
                onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),itemView);
                break;
        }
    }
}