package com.kaspontech.foodwall.adapters.Review_Image_adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_adapter.Review_ambience_image_adapter;
import com.kaspontech.foodwall.reviewpackage.Review_adapter_folder.Review_image_pojo;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;

public class Review_packaging_dish_adapter extends RecyclerView.Adapter<Review_packaging_dish_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;


    private ArrayList<Review_image_pojo> getttingimagelist = new ArrayList<>();

    private Review_image_pojo reviewImagePojo;

    private Review_ambience_image_adapter.TopDishRemovedListener topDishRemovedListener;

    public interface TopDishRemovedListener {

        void removedAmbienceImageLisenter(ArrayList<Review_image_pojo> imagesList);

    }

    public Review_packaging_dish_adapter(Context c, ArrayList<Review_image_pojo> gettinglist, Review_ambience_image_adapter.TopDishRemovedListener topDishRemovedListener) {
        this.context = c;
        this.getttingimagelist = gettinglist;
        this.topDishRemovedListener = topDishRemovedListener;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_review_image, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v);

        Pref_storage pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        reviewImagePojo = getttingimagelist.get(position);


        Log.e("Image-->", "image" + reviewImagePojo.getImage());
        Log.e("Image-->", "text-->" + reviewImagePojo.getImage_text());

//        Utility.picassoImageLoader(reviewImagePojo.getImage(),
//                 0,holder.image_review);
        GlideApp.with(context)
                .load(reviewImagePojo.getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(holder.image_review);

        holder.txt_image.setVisibility(View.GONE);

       // holder.txt_image.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(reviewImagePojo.getImage_text()));
      //  holder.txt_image.setTextColor(Color.parseColor("#000000"));


        holder.remove_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getttingimagelist.remove(position);
                notifyDataSetChanged();
                topDishRemovedListener.removedAmbienceImageLisenter(getttingimagelist);

            }
        });


    }


    @Override
    public int getItemCount() {
        return getttingimagelist.size();

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //TextView

        TextView txt_image;

        //Imageview

        ImageView image_review;
        ImageButton remove_image;

        RelativeLayout rl_hotel_datas;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_image = (TextView) itemView.findViewById(R.id.txt_image);
            image_review = (ImageView) itemView.findViewById(R.id.image_review);
            remove_image = (ImageButton) itemView.findViewById(R.id.remove_image);
            rl_hotel_datas = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_datas);

        }


    }


}

