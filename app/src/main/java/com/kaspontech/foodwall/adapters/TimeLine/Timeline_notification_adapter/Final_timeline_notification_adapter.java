package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.choota.dev.ctimeago.TimeAgo;
import com.kaspontech.foodwall.CustomViewPagerAdapter;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.SlidingImage_Adapter;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_packaging_dish_adapter;
import com.kaspontech.foodwall.adapters.questionansweradapter.AnswerAdapter;
import com.kaspontech.foodwall.adapters.questionansweradapter.PollAdapter;
import com.kaspontech.foodwall.adapters.TimeLine.ViewPagerAdapter;
import com.kaspontech.foodwall.bucketlistpackage.CreateBucketlistActivty;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.eventspackage.ViewEvent;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Comments_activity;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.likesActivity;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_Adapter.StoriesAdapter;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Get_all_stories_pojo;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.historicalMapPackage.EditHistoryMap;
import com.kaspontech.foodwall.menuPackage.UserMenu;
import com.kaspontech.foodwall.modelclasses.AnswerPojoClass;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.CreateEditEventsComments;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.CreateEditTimeline_Comments;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestionAnswerPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Poll.create_delete_answer_poll_undoPOJO;
import com.kaspontech.foodwall.modelclasses.ReportResponse;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.GetallCommentsPojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.New_Getting_timeline_all_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.comments_2;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.questionspackage.QuestionAnswer;
import com.kaspontech.foodwall.questionspackage.ViewQuestionAnswer;
import com.kaspontech.foodwall.questionspackage.pollcomments.ViewPollComments;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.PackagingDetailsAdapter;
import com.kaspontech.foodwall.reviewpackage.alldishtab.AllDishDisplayActivity;
import com.kaspontech.foodwall.reviewpackage.ReviewDeliveryDetails;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewInputAllPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewAmbienceAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewAvoidDishAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewTopDishAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_in_detail_activity;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
import com.kaspontech.foodwall.reviewpackage.ReviewsLikesAllActivity;

import com.kaspontech.foodwall.utills.ClickableViewPager;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.OnSwipeTouchListener;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.TimeAgo2;
import com.kaspontech.foodwall.utills.Utility;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static java.lang.Integer.parseInt;


public class Final_timeline_notification_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements

        RecyclerView.OnItemTouchListener, View.OnTouchListener {

    /**
     * Tag for log
     **/
    private static final String TAG = "FinalTimelineAdapter";

    /**
     * Alert option string array
     **/
    public final String[] itemsothers = {"Bucket list options", "Delete review"};

    /**
     * Context
     **/
    Context context;

    /**
     * Likes and comment integer
     **/
    int totalLikes;
    int totalComments;

    /**
     * Recycler view for tagged people
     **/
    RecyclerView recyler_tagged_people;
    /**
     * Tagged people adapter
     **/
    ShowTaggedPeopleAdapter showTaggedPeopleAdapter;

    private String imageStatusTimeline = null;

    /**
     * Top, worst, ambience arraylist
     **/
    public ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();
    public ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();
    public ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

    // Stories recyclerview
    private List<Get_all_stories_pojo> storiesPojoList = new ArrayList<>();

    /**
     * From which string
     **/
    String fromWhich;

    // to display latest comment
    String firstName;
    /**
     * latitude & longitude double values
     **/
    GPSTracker gpsTracker;

    Double latitude;

    Double longitude;

    /**
     * Timeline ArrayList and Model class
     **/
    New_Getting_timeline_all_pojo newGettingTimelineAllPojo;

    List<New_Getting_timeline_all_pojo> gettingTimelineAllPojos_list = new ArrayList<>();


    onRefershListener onRefershListener;

    private boolean undoPoll = false;

    /**
     * Q&A interface
     **/
    QuestionAnswer questionAnswer;
    TimelineAdapter timelineAdapter;
    private onCommentsPostListener onCommentsPostListener;
    private TimelineHeaderHolder.TimelineHeaderItemListener onTimeLineHeaderHolderListener;

    int value;
    boolean Swipable = true;

    private boolean firstTime;

    public Final_timeline_notification_adapter(Context context, List<New_Getting_timeline_all_pojo> gettingTimelineAllPojos_list, String fromWhich, onRefershListener onRefershListener, onCommentsPostListener onCommentsPostListener, TimelineHeaderHolder.TimelineHeaderItemListener onTimeLineHeaderHolderListener) {
        this.context = context;
        this.fromWhich = fromWhich;
        this.gettingTimelineAllPojos_list = gettingTimelineAllPojos_list;
        this.onRefershListener = onRefershListener;
        this.onCommentsPostListener = onCommentsPostListener;
        this.onTimeLineHeaderHolderListener = onTimeLineHeaderHolderListener;
        this.firstTime = true;
    }


    @Override
    public int getItemViewType(int position) {


        // Case 0   - Timeline
        // Case 1   - Events
        // Case 2   - Events_going
        // Case 3   - Events_likes
        // Case 4   - Following
        // Case 5   - Review
        // Case 6   - Review_like
        // Case 7   - Review_comments
        // Case 8   - Question
        // Case 9   - Answer
        // Case 10  - Upvote
        // Case 11  - Poll
        // Case 12  - Pagination loader
        // Case 101 - Header

        // Case 0 - Timeline

        if (gettingTimelineAllPojos_list.get(position).isHeader()) {
            return 101;
        } else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("0")) {

            return 0;

            // Case 1 - Events
        } else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("1")) {

            return 1;

            // Case 2 - Events_going
        } else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("2")) {

            return 2;

            // Case 3 - events_likes
        } else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("3")) {

            return 3;
            // Case 4 -following
        } else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("4")) {

            return 4;

        } // Case 5 -review
        else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("5")) {

            return 5;
            // Case 6 - review_like
        } else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("6")) {

            return 6;
            // Case 7 - review_comments
        } else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("7")) {

            return 7;

        }  // Case 8 - question
        else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("8") && gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getQuesType().equalsIgnoreCase("0")) {

            return 8;

        } // Case 9 -  answer
        else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("9") && gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getQuesType().equalsIgnoreCase("0")) {

            return 9;

        }
        // Case 10 - upvote
        else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("10") && gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getQuesType().equalsIgnoreCase("0")) {

            return 10;

        } //Case 11 - Poll
        else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("8") && gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getQuesType().equalsIgnoreCase("1")) {

            return 11;

        } //Pagination loader
        else {

            return 12;

        }

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        switch (viewType) {


            // Case 0 - Timeline
            // Case 1 - Events
            // Case 2 - Events_going
            // Case 3 - events_likes
            // Case 4 - following
            // Case 5 - review
            // Case 6 - review_like
            // Case 7 - review_comments
            // Case 8 -  question
            // Case 9 -  answer
            // Case 10 - upvote
            // Case 11 - poll
            // Case 12 - pagination loader
            // Case 101 - header

            case 101:
                View headerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timeline_header, parent, false);
                return new TimelineHeaderHolder(headerView, onTimeLineHeaderHolderListener);

            // Case 0 - Timeline
            case 0:

                View timelineView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_newsfeed_story, parent, false);
                return new TimelineAdapter(timelineView, onCommentsPostListener);


            // Case 1 - Events
            case 1:

                View evenView = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeline_event_displaying_layout, parent, false);
                return new EventsAdapter_notification(evenView, onCommentsPostListener);


            // Case 2 - Events_going
            case 2:

                View eventViewGoing = LayoutInflater.from(parent.getContext()).inflate(R.layout.events_going_timelineadapter_layout, parent, false);
                return new Events_going_adapter_timeline(eventViewGoing, onCommentsPostListener);

            // Case 3 - events_interested
            case 3:

                View eventViewLike = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_interested_timeline_layout, parent, false);
                return new Events_interested_adapter_timeline(eventViewLike, onCommentsPostListener);

            // Case 4 -following
            case 4:

                View followingView = LayoutInflater.from(parent.getContext()).inflate(R.layout.following_you_layout, parent, false);
                return new Following_timelineAdapter(followingView);


            // Case 5 -review
            case 5:

                View reviewCreatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_timeline_review, parent, false);
                return new Review_created_adapter(reviewCreatedView, onCommentsPostListener);

            // Case 6 - review_like
            case 6:

                View reviewLikedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_timeline_review, parent, false);
                return new Review_created_adapter(reviewLikedView, onCommentsPostListener);

            // Case 7 - review_comments
            case 7:

                View reviewCommentedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_timeline_review, parent, false);
                return new Review_created_adapter(reviewCommentedView, onCommentsPostListener);

            // Case 8 - question
            case 8:

                View questionView = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_timeline_layout, parent, false);
                return new AnswerAdapter(questionView, questionAnswer, onCommentsPostListener);

            // Case 9 -  answer
            case 9:

                View answerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_timeline_layout, parent, false);
                return new AnswerAdapter(answerView, questionAnswer, onCommentsPostListener);

            // Case 10 - upvote
            case 10:

                View upvoteView = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_timeline_layout, parent, false);
                return new AnswerAdapter(upvoteView, questionAnswer, onCommentsPostListener);


            // Case 11 - Poll
            case 11:

                View pollView = LayoutInflater.from(parent.getContext()).inflate(R.layout.poll_timeline_adapter, parent, false);
                return new PollAdapter(pollView, questionAnswer, onRefershListener, onCommentsPostListener);

            // Case 12 - Pagination loader
            case 12:

                View loaderview = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_loader, parent, false);
                return new Paginat_adapter(loaderview);

        }

        return null;


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.size() > 0 && payloads.get(0) instanceof Boolean && holder instanceof TimelineHeaderHolder) {
            boolean visibility = (boolean) payloads.get(0);
            ((TimelineHeaderHolder) holder).ll_no_post.setVisibility(visibility ? VISIBLE : GONE);
            firstTime = false;
        }
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder timelineAdapeterHolder, final int position) {

        newGettingTimelineAllPojo = gettingTimelineAllPojos_list.get(position);

        /*GPS tracker initialization for getting latitude and longitude*/
        gpsTracker = new GPSTracker(context);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        if (!(timelineAdapeterHolder instanceof TimelineAdapter)) {
            timelineAdapeterHolder.itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (context != null && context instanceof _SwipeActivityClass) {
                        ((_SwipeActivityClass) context).setOverlookSwipe(false);
                    }
                    return false;
                }
            });
        }


        switch (timelineAdapeterHolder.getItemViewType()) {


            /**
             * Header logic
             *
             */

            case 101:

                TimelineHeaderHolder timelineHeaderHolder = (TimelineHeaderHolder) timelineAdapeterHolder;
                //Images
                String userimageuri = Pref_storage.getDetail(context, "UserImage");
                String Propiclogin = Pref_storage.getDetail(context, "picture_user_login");

                if (Propiclogin != null) {

                    // Using glide app for loading image URL

                    GlideApp.with(context)
                            .load(Propiclogin)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .thumbnail(0.1f)
                            .centerInside()
                            .placeholder(R.drawable.ic_add_photo)
                            .into(timelineHeaderHolder.img_status_timeline);

                    if (imageStatusTimeline == null) {

                        GlideApp.with(context)
                                .load(Propiclogin)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .thumbnail(0.1f)
                                .centerInside()
                                .placeholder(R.drawable.ic_add_photo)
                                .into(timelineHeaderHolder.userphotoComment);
                    }


                } else {

                    GlideApp.with(context)
                            .load(userimageuri)
                            .centerInside()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .thumbnail(0.1f)
                            .placeholder(R.drawable.ic_add_photo)
                            .into(timelineHeaderHolder.img_status_timeline);

                    GlideApp.with(context)
                            .load(userimageuri)
                            .centerInside()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .thumbnail(0.1f)
                            .placeholder(R.drawable.ic_add_photo)
                            .into(timelineHeaderHolder.userphotoComment);
                }

                if (imageStatusTimeline != null) {
                    GlideApp.with(context)
                            .load(Propiclogin)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .thumbnail(0.1f)
                            .centerInside()
                            .placeholder(R.drawable.ic_add_photo)
                            .into(timelineHeaderHolder.img_status_timeline);
                }

                //Stories adapter
                StoriesAdapter storiesAdapter;
                if (timelineHeaderHolder.rv_stories.getTag() != null && timelineHeaderHolder.rv_stories.getTag() instanceof StoriesAdapter)
                    storiesAdapter = (StoriesAdapter) timelineHeaderHolder.rv_stories.getTag();
                else {
                    storiesAdapter = new StoriesAdapter(context, storiesPojoList);
                    timelineHeaderHolder.rv_stories.setTag(storiesAdapter);
                }

                LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                llm.setStackFromEnd(true);
                timelineHeaderHolder.rv_stories.setLayoutManager(llm);
                timelineHeaderHolder.rv_stories.setAdapter(storiesAdapter);
                timelineHeaderHolder.rv_stories.setItemAnimator(new DefaultItemAnimator());
                timelineHeaderHolder.rv_stories.setHorizontalScrollBarEnabled(true);
                timelineHeaderHolder.rv_stories.setHasFixedSize(true);
                // rv_stories.setItemViewCacheSize(20);

                if (firstTime) {
                    firstTime = false;
                    timelineHeaderHolder.ll_no_post.setVisibility(GONE);
                } else if (gettingTimelineAllPojos_list.size() > 1) {
                    timelineHeaderHolder.ll_no_post.setVisibility(GONE);
                } else {
                    timelineHeaderHolder.ll_no_post.setVisibility(VISIBLE);
                }

                break;

            /**
             *  Timeline logic
             *
             * */
            case 0:

                final TimelineAdapter timelineAdapter = (TimelineAdapter) timelineAdapeterHolder;

                totalComments = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getTotalComments());

                if (totalComments == 0) {

                    timelineAdapter.rl_comments_lay.setVisibility(GONE);

                } else {

                    timelineAdapter.rl_comments_lay.setVisibility(VISIBLE);

                }

                // Comments edit text change listener
                timelineAdapter.edittxtComment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (timelineAdapter.edittxtComment.getText().toString().trim().length() != 0) {
                            Log.e("view", "testing--------->" + s.toString());
                            timelineAdapter.txt_adapter_post.setVisibility(VISIBLE);
                        } else {
                            Log.e("view", "testing--------->" + s.toString());
                            timelineAdapter.txt_adapter_post.setVisibility(GONE);
                        }
                    }
                });

                // TimeLine Image Display
                List<String> imagesList = new ArrayList<>();

                if (gettingTimelineAllPojos_list.get(position).getImageCount() != 0) {

                    for (int i = 0; i < gettingTimelineAllPojos_list.get(position).getImage().size(); i++) {

                        imagesList.add(gettingTimelineAllPojos_list.get(position).getImage().get(i).getImg());

                    }

                }

                if (gettingTimelineAllPojos_list.get(position).getImageCount() == 0) {

                    timelineAdapter.rl_fullimage.setVisibility(View.GONE);

                } else if (gettingTimelineAllPojos_list.get(position).getImageCount() == 1) {

                    timelineAdapter.rl_fullimage.setVisibility(View.VISIBLE);
                    timelineAdapter.circleIndicatortimeline.setVisibility(View.GONE);

                }


                /// Display post images
                initializeViews(imagesList, timelineAdapeterHolder, position);

                //timelineAdapter.circleIndicatortimeline.setViewPager(timelineAdapter.mviewpager);
                timelineAdapter.NUM_PAGES = imagesList.size();

                /*ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        // disable swipe
                        if(!Swipable) {
                        }
                    }
                    public void onPageScrollStateChanged(int state) {}
                    public void onPageSelected(int position) {
                       /* value = gettingTimelineAllPojos_list.get(timelineAdapeterHolder.getAdapterPosition()).getImageCount();

                        int pagei = position + 1;
                        String pages = pagei + "";

                        //Toast.makeText(context, context.getString(R.string.changeinfopage) + " " + pagei, Toast.LENGTH_SHORT).show();
                        timelineAdapter.currentPage = position;

                        //timelineAdapter.mviewpager.setCurrentItem(pagei, true);
                        if (value == pagei) {
                            Intent intent1 = new Intent(context, ChatActivity.class);
                            Pref_storage.setDetail(context, "scroll", "True");
                            context.startActivity(intent1);
                        } else if (pagei == 1) {
                            Intent intent1 = new Intent(context, UserMenu.class);
                            context.startActivity(intent1);
                            //Swiple=false;
                        }
                    }
                };
                timelineAdapter.mviewpager.addOnPageChangeListener(onPageChangeListener);*/

                timelineAdapter.circleIndicatortimeline.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int i, float v, int i1) {

                    }

                    @Override
                    public void onPageSelected(int i) {
                        timelineAdapter.currentPage = position;
                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {

                    }
                });

                try {

                    // User profile photo
                    GlideApp.with(context)
                            .load(gettingTimelineAllPojos_list.get(position).getPicture())
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .thumbnail(0.1f)
                            .placeholder(R.drawable.ic_add_photo)
                            .into(timelineAdapter.userphoto_profile);

                    // Near comment box
                    GlideApp.with(context)
                            .load(Pref_storage.getDetail(context, "picture_user_login"))
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .thumbnail(0.1f)
                            .placeholder(R.drawable.ic_add_photo)
                            .into(timelineAdapter.userphoto_comment);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                // Follow, following visibility
                if (Integer.parseInt(gettingTimelineAllPojos_list.get(position).getFollowingId()) == 0) {

                    timelineAdapter.txt_timeline_follow.setVisibility(VISIBLE);
                    timelineAdapter.img_follow_timeline.setVisibility(VISIBLE);

                } else if (Integer.parseInt(gettingTimelineAllPojos_list.get(position).getFollowingId()) != 0) {

                    timelineAdapter.txt_timeline_follow.setVisibility(GONE);
                    timelineAdapter.img_follow_timeline.setVisibility(GONE);

                }


                if (Pref_storage.getDetail(context, "userId").equals(gettingTimelineAllPojos_list.get(position).getUserId())) {

                    timelineAdapter.txt_timeline_follow.setVisibility(GONE);
                    timelineAdapter.img_follow_timeline.setVisibility(GONE);

                }


                // User caption visiblity

                try {

                    String caption = gettingTimelineAllPojos_list.get(position).getTimelineDescription();

                    if (caption.equals("0")) {

                        timelineAdapter.caption.setVisibility(GONE);

                    } else {

                        timelineAdapter.caption.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettingTimelineAllPojos_list.get(position).getTimelineDescription()));

                        timelineAdapter.caption.setTextColor(Color.parseColor("#000000"));
                        timelineAdapter.caption.setVisibility(VISIBLE);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


                // Comments visibility


                try {


                    String comment = gettingTimelineAllPojos_list.get(position).getTotalComments();


                    if (Integer.parseInt(comment) == 0 || comment == null) {

                        timelineAdapter.viewcomment.setVisibility(GONE);
                        timelineAdapter.commenttest.setVisibility(GONE);

                    } else {

                        timelineAdapter.comment.setText(comment);

                        // display latest comment with username
                        if (gettingTimelineAllPojos_list.get(position).getTimelineLastCmtCount() == 1) {

                            timelineAdapter.recently_commented_ll.setVisibility(VISIBLE);

                            timelineAdapter.recently_commented.setText(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getTlComments());
                            timelineAdapter.username_latestviewed.setText(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getFirstName());


                            /* Posted date functionality */
                            String createdon = gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getCreatedOn();

                            Utility.setTimeStamp(createdon, timelineAdapter.created_on);


                            GlideApp.with(context)
                                    .load(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getPicture())
                                    .centerCrop()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .thumbnail(0.1f)
                                    .placeholder(R.drawable.ic_add_photo)
                                    .into(timelineAdapter.userphoto);


                        } else {

                            timelineAdapter.recently_commented_ll.setVisibility(GONE);
                        }


                        if (Integer.parseInt(comment) == 1) {

                            timelineAdapter.viewcomment.setText(R.string.view);
                            timelineAdapter.commenttest.setText(R.string.commenttt);
                            timelineAdapter.viewcomment.setVisibility(VISIBLE);
                            timelineAdapter.commenttest.setVisibility(VISIBLE);

                        } else if (Integer.parseInt(comment) > 1) {

                            timelineAdapter.viewcomment.setVisibility(VISIBLE);
                            timelineAdapter.commenttest.setVisibility(VISIBLE);

                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                // Displaying user name

                try {

                    String username = gettingTimelineAllPojos_list.get(position).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getLastName();
                    String hotelname = gettingTimelineAllPojos_list.get(position).getTimelineHotel();
                    int taggedCount = gettingTimelineAllPojos_list.get(position).getWhomCount();

                    String taggedPeopleText = "";
                    String finalUserNameText = "";

                    if (taggedCount != 0) {

                        String taggedFirstName = "";
                        String taggedSecondName = "";


                        switch (taggedCount) {


                            case 1:

                                taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                taggedPeopleText = taggedFirstName;

                                break;

                            case 2:

                                taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                taggedSecondName = gettingTimelineAllPojos_list.get(position).getWhom().get(1).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(1).getLastName();
                                taggedPeopleText = taggedFirstName + " and " + taggedSecondName;


                                break;


                            default:

                                taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                taggedPeopleText = taggedFirstName + " and " + String.valueOf(taggedCount - 1) + " Others";

                                break;

                        }


                    }

                    if (taggedCount == 0) {

                        finalUserNameText = username + " at " + hotelname;

                        SpannableString ss = new SpannableString(finalUserNameText);

                        ClickableSpan clickableSpan1 = new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {

                                String user_id = gettingTimelineAllPojos_list.get(position).getUserId();
                                String timelineid = gettingTimelineAllPojos_list.get(position).getTimelineId();

                                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                                    context.startActivity(new Intent(context, Profile.class));
                                } else {

                                    Intent intent = new Intent(context, User_profile_Activity.class);
                                    intent.putExtra("created_by", user_id);
                                    context.startActivity(intent);
                                }

                            }

                            @Override
                            public void updateDrawState(TextPaint ds) {
                                super.updateDrawState(ds);
                                ds.setColor(Color.BLACK);
                                ds.setUnderlineText(false);
                            }
                        };

                        ClickableSpan clickableSpan2 = new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {

                                Intent intent = new Intent(context, Review_in_detail_activity.class);
                                intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                intent.putExtra("f_name", gettingTimelineAllPojos_list.get(position).getFirstName());
                                intent.putExtra("l_name", gettingTimelineAllPojos_list.get(position).getLastName());
                                intent.putExtra("reviewid", "0");
                                intent.putExtra("timelineId", gettingTimelineAllPojos_list.get(position).getTimelineId());
                                context.startActivity(intent);

                            }

                            @Override
                            public void updateDrawState(TextPaint ds) {
                                super.updateDrawState(ds);
                                ds.setColor(Color.BLACK);
                                ds.setUnderlineText(false);
                            }
                        };

                        ss.setSpan(clickableSpan1, 0, username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        ss.setSpan(clickableSpan2, username.length() + 4, finalUserNameText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                        timelineAdapter.username.setText(ss);
                        timelineAdapter.username.setMovementMethod(LinkMovementMethod.getInstance());


                    } else {

                        String isWith = " is with ";
                        String at = " at ";

                        finalUserNameText = username + isWith + taggedPeopleText + at + hotelname;

                        SpannableString ss = new SpannableString(finalUserNameText);

                        /*User name on click listener*/
                        ClickableSpan clickableSpan1 = new ClickableSpan() {
                            @Override
                            public void onClick(View v) {

                                String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                                if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                                    context.startActivity(new Intent(context, Profile.class));
                                } else {

                                    Intent intent = new Intent(context, User_profile_Activity.class);
                                    intent.putExtra("created_by", userId);
                                    context.startActivity(intent);

                                }

                            }

                            @Override
                            public void updateDrawState(TextPaint ds) {
                                super.updateDrawState(ds);
                                ds.setColor(Color.BLACK);
                                ds.setUnderlineText(false);
                            }
                        };

                        ClickableSpan clickableSpan2 = new ClickableSpan() {
                            @Override
                            public void onClick(View v) {
                                /*Alert dialog*/

                                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                                LayoutInflater inflater1 = LayoutInflater.from(context);
                                @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.alert_show_tagged_people, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setTitle("");

                                recyler_tagged_people = (RecyclerView) dialogView.findViewById(R.id.recyler_tagged_people);

                                /*Tagged people adapter*/
                                showTaggedPeopleAdapter = new ShowTaggedPeopleAdapter(context, gettingTimelineAllPojos_list.get(position).getWhom());
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                recyler_tagged_people.setLayoutManager(mLayoutManager);
                                recyler_tagged_people.setItemAnimator(new DefaultItemAnimator());
                                recyler_tagged_people.setAdapter(showTaggedPeopleAdapter);
                                recyler_tagged_people.hasFixedSize();
                                /*Alert dialog*/
                                final AlertDialog dialog = dialogBuilder.create();
                                dialog.show();


                            }

                            @Override
                            public void updateDrawState(TextPaint ds) {
                                super.updateDrawState(ds);
                                ds.setColor(Color.BLACK);
                                ds.setUnderlineText(false);
                            }
                        };

                        /*Hotel name click listener*/

                        ClickableSpan clickableSpan3 = new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {

                                Intent intent = new Intent(context, Review_in_detail_activity.class);
                                intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                intent.putExtra("f_name", gettingTimelineAllPojos_list.get(position).getFirstName());
                                intent.putExtra("l_name", gettingTimelineAllPojos_list.get(position).getLastName());
                                intent.putExtra("reviewid", "0");
                                intent.putExtra("timelineId", gettingTimelineAllPojos_list.get(position).getTimelineId());
                                context.startActivity(intent);


                            }

                            @Override
                            public void updateDrawState(TextPaint ds) {
                                super.updateDrawState(ds);
                                ds.setColor(Color.BLACK);
                                ds.setUnderlineText(false);
                            }
                        };


                        int first = 0, second = username.length();
                        int third = username.length() + isWith.length(), fourth = username.length() + isWith.length() + taggedPeopleText.length();
                        int fifth = username.length() + isWith.length() + taggedPeopleText.length() + at.length(), sixth = finalUserNameText.length();


                        ss.setSpan(clickableSpan1, first, second, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        ss.setSpan(clickableSpan2, third, fourth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        ss.setSpan(clickableSpan3, fifth, sixth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


                        timelineAdapter.username.setText(ss);
                        timelineAdapter.username.setMovementMethod(LinkMovementMethod.getInstance());

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


                timelineAdapter.img_commentadapter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String createdusername = null;
                        // Displaying user name

                        try {

                            String username = gettingTimelineAllPojos_list.get(position).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getLastName();
                            String hotelname = gettingTimelineAllPojos_list.get(position).getTimelineHotel();
                            int taggedCount = gettingTimelineAllPojos_list.get(position).getWhomCount();

                            String taggedPeopleText = "";
                            String finalUserNameText = "";

                            if (taggedCount != 0) {

                                String taggedFirstName = "";
                                String taggedSecondName = "";


                                switch (taggedCount) {


                                    case 1:

                                        taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                        taggedPeopleText = taggedFirstName;

                                        break;

                                    case 2:

                                        taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                        taggedSecondName = gettingTimelineAllPojos_list.get(position).getWhom().get(1).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(1).getLastName();
                                        taggedPeopleText = taggedFirstName + " and " + taggedSecondName;


                                        break;


                                    default:

                                        taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                        taggedPeopleText = taggedFirstName + " and " + String.valueOf(taggedCount - 1) + " Others";

                                        break;

                                }


                            }

                            if (taggedCount == 0) {

                                finalUserNameText = username + " at " + hotelname;

                                SpannableString ss = new SpannableString(finalUserNameText);

                                ClickableSpan clickableSpan1 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View widget) {

                                        String user_id = gettingTimelineAllPojos_list.get(position).getUserId();
                                        String timelineid = gettingTimelineAllPojos_list.get(position).getTimelineId();

                                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                                            context.startActivity(new Intent(context, Profile.class));
                                        } else {

                                            Intent intent = new Intent(context, User_profile_Activity.class);
                                            intent.putExtra("created_by", user_id);
                                            context.startActivity(intent);
                                        }

                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                ClickableSpan clickableSpan2 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View widget) {

                                        Intent intent = new Intent(context, Review_in_detail_activity.class);
                                        intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                        intent.putExtra("f_name", gettingTimelineAllPojos_list.get(position).getFirstName());
                                        intent.putExtra("l_name", gettingTimelineAllPojos_list.get(position).getLastName());
                                        intent.putExtra("reviewid", "0");
                                        intent.putExtra("timelineId", gettingTimelineAllPojos_list.get(position).getTimelineId());
                                        context.startActivity(intent);

                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                ss.setSpan(clickableSpan1, 0, username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                ss.setSpan(clickableSpan2, username.length() + 4, finalUserNameText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                                createdusername = String.valueOf(ss);

                            } else {

                                String isWith = " is with ";
                                String at = " at ";

                                finalUserNameText = username + isWith + taggedPeopleText + at + hotelname;

                                SpannableString ss = new SpannableString(finalUserNameText);

                                /*User name on click listener*/
                                ClickableSpan clickableSpan1 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View v) {

                                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                                            context.startActivity(new Intent(context, Profile.class));
                                        } else {

                                            Intent intent = new Intent(context, User_profile_Activity.class);
                                            intent.putExtra("created_by", userId);
                                            context.startActivity(intent);

                                        }

                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                ClickableSpan clickableSpan2 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View v) {
                                        /*Alert dialog*/

                                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                                        LayoutInflater inflater1 = LayoutInflater.from(context);
                                        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.alert_show_tagged_people, null);
                                        dialogBuilder.setView(dialogView);
                                        dialogBuilder.setTitle("");

                                        recyler_tagged_people = (RecyclerView) dialogView.findViewById(R.id.recyler_tagged_people);

                                        /*Tagged people adapter*/
                                        showTaggedPeopleAdapter = new ShowTaggedPeopleAdapter(context, gettingTimelineAllPojos_list.get(position).getWhom());
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                        recyler_tagged_people.setLayoutManager(mLayoutManager);
                                        recyler_tagged_people.setItemAnimator(new DefaultItemAnimator());
                                        recyler_tagged_people.setAdapter(showTaggedPeopleAdapter);
                                        recyler_tagged_people.hasFixedSize();
                                        /*Alert dialog*/
                                        final AlertDialog dialog = dialogBuilder.create();
                                        dialog.show();


                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                /*Hotel name click listener*/

                                ClickableSpan clickableSpan3 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View widget) {

                                        Intent intent = new Intent(context, Review_in_detail_activity.class);
                                        intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                        intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                        intent.putExtra("f_name", gettingTimelineAllPojos_list.get(position).getFirstName());
                                        intent.putExtra("l_name", gettingTimelineAllPojos_list.get(position).getLastName());
                                        intent.putExtra("reviewid", "0");
                                        intent.putExtra("timelineId", gettingTimelineAllPojos_list.get(position).getTimelineId());
                                        context.startActivity(intent);


                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };


                                int first = 0, second = username.length();
                                int third = username.length() + isWith.length(), fourth = username.length() + isWith.length() + taggedPeopleText.length();
                                int fifth = username.length() + isWith.length() + taggedPeopleText.length() + at.length(), sixth = finalUserNameText.length();


                                ss.setSpan(clickableSpan1, first, second, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                ss.setSpan(clickableSpan2, third, fourth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                ss.setSpan(clickableSpan3, fifth, sixth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


                                createdusername = String.valueOf(ss);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Intent intent = new Intent(context, Comments_activity.class);
                        // Bundle bundle = new Bundle();
                        intent.putExtra("comment_timelinepicture", gettingTimelineAllPojos_list.get(position).getPicture());
                        intent.putExtra("comment_posttpe", gettingTimelineAllPojos_list.get(position).getPostType());
                        intent.putExtra("comment_timelineid", gettingTimelineAllPojos_list.get(position).getTimelineId());
                        intent.putExtra("comment_profile", gettingTimelineAllPojos_list.get(position).getPicture());
                        intent.putExtra("comment_created_on", gettingTimelineAllPojos_list.get(position).getCreatedOn());
                        intent.putExtra("username", createdusername);

                        try {
                            intent.putExtra("comment_image", gettingTimelineAllPojos_list.get(position).getImage().get(0).getImg());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        context.startActivity(intent);
                    }
                });
                // Timestamp for timeline


                String createdon = gettingTimelineAllPojos_list.get(position).getCreatedOn();

                Utility.setTimeStamp(createdon, timelineAdapter.txt_created_on);


                timelineAdapter.rl_comment_adapter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String createdusername = null;
                        // Displaying user name

                        try {

                            String username = gettingTimelineAllPojos_list.get(position).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getLastName();
                            String hotelname = gettingTimelineAllPojos_list.get(position).getTimelineHotel();
                            int taggedCount = gettingTimelineAllPojos_list.get(position).getWhomCount();

                            String taggedPeopleText = "";
                            String finalUserNameText = "";

                            if (taggedCount != 0) {

                                String taggedFirstName = "";
                                String taggedSecondName = "";


                                switch (taggedCount) {


                                    case 1:

                                        taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                        taggedPeopleText = taggedFirstName;

                                        break;

                                    case 2:

                                        taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                        taggedSecondName = gettingTimelineAllPojos_list.get(position).getWhom().get(1).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(1).getLastName();
                                        taggedPeopleText = taggedFirstName + " and " + taggedSecondName;


                                        break;


                                    default:

                                        taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                        taggedPeopleText = taggedFirstName + " and " + String.valueOf(taggedCount - 1) + " Others";

                                        break;

                                }


                            }

                            if (taggedCount == 0) {

                                finalUserNameText = username + " at " + hotelname;

                                SpannableString ss = new SpannableString(finalUserNameText);

                                ClickableSpan clickableSpan1 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View widget) {

                                        String user_id = gettingTimelineAllPojos_list.get(position).getUserId();
                                        String timelineid = gettingTimelineAllPojos_list.get(position).getTimelineId();

                                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                                            context.startActivity(new Intent(context, Profile.class));
                                        } else {

                                            Intent intent = new Intent(context, User_profile_Activity.class);
                                            intent.putExtra("created_by", user_id);
                                            context.startActivity(intent);
                                        }

                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                ClickableSpan clickableSpan2 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View widget) {

                                        Intent intent = new Intent(context, Review_in_detail_activity.class);
                                        intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                        intent.putExtra("f_name", gettingTimelineAllPojos_list.get(position).getFirstName());
                                        intent.putExtra("l_name", gettingTimelineAllPojos_list.get(position).getLastName());
                                        intent.putExtra("reviewid", "0");
                                        intent.putExtra("timelineId", gettingTimelineAllPojos_list.get(position).getTimelineId());
                                        context.startActivity(intent);

                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                ss.setSpan(clickableSpan1, 0, username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                ss.setSpan(clickableSpan2, username.length() + 4, finalUserNameText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                                createdusername = String.valueOf(ss);

                            } else {

                                String isWith = " is with ";
                                String at = " at ";

                                finalUserNameText = username + isWith + taggedPeopleText + at + hotelname;

                                SpannableString ss = new SpannableString(finalUserNameText);

                                /*User name on click listener*/
                                ClickableSpan clickableSpan1 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View v) {

                                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                                            context.startActivity(new Intent(context, Profile.class));
                                        } else {

                                            Intent intent = new Intent(context, User_profile_Activity.class);
                                            intent.putExtra("created_by", userId);
                                            context.startActivity(intent);

                                        }

                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                ClickableSpan clickableSpan2 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View v) {
                                        /*Alert dialog*/

                                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                                        LayoutInflater inflater1 = LayoutInflater.from(context);
                                        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.alert_show_tagged_people, null);
                                        dialogBuilder.setView(dialogView);
                                        dialogBuilder.setTitle("");

                                        recyler_tagged_people = (RecyclerView) dialogView.findViewById(R.id.recyler_tagged_people);

                                        /*Tagged people adapter*/
                                        showTaggedPeopleAdapter = new ShowTaggedPeopleAdapter(context, gettingTimelineAllPojos_list.get(position).getWhom());
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                        recyler_tagged_people.setLayoutManager(mLayoutManager);
                                        recyler_tagged_people.setItemAnimator(new DefaultItemAnimator());
                                        recyler_tagged_people.setAdapter(showTaggedPeopleAdapter);
                                        recyler_tagged_people.hasFixedSize();
                                        /*Alert dialog*/
                                        final AlertDialog dialog = dialogBuilder.create();
                                        dialog.show();


                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                /*Hotel name click listener*/

                                ClickableSpan clickableSpan3 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View widget) {

                                        Intent intent = new Intent(context, Review_in_detail_activity.class);
                                        intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                        intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                        intent.putExtra("f_name", gettingTimelineAllPojos_list.get(position).getFirstName());
                                        intent.putExtra("l_name", gettingTimelineAllPojos_list.get(position).getLastName());
                                        intent.putExtra("reviewid", "0");
                                        intent.putExtra("timelineId", gettingTimelineAllPojos_list.get(position).getTimelineId());
                                        context.startActivity(intent);


                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };


                                int first = 0, second = username.length();
                                int third = username.length() + isWith.length(), fourth = username.length() + isWith.length() + taggedPeopleText.length();
                                int fifth = username.length() + isWith.length() + taggedPeopleText.length() + at.length(), sixth = finalUserNameText.length();


                                ss.setSpan(clickableSpan1, first, second, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                ss.setSpan(clickableSpan2, third, fourth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                ss.setSpan(clickableSpan3, fifth, sixth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


                                createdusername = String.valueOf(ss);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Intent intent = new Intent(context, Comments_activity.class);

                        // Bundle bundle = new Bundle();
                        intent.putExtra("comment_timelinepicture", gettingTimelineAllPojos_list.get(position).getPicture());
                        intent.putExtra("comment_posttpe", gettingTimelineAllPojos_list.get(position).getPostType());
                        intent.putExtra("comment_timelineid", gettingTimelineAllPojos_list.get(position).getTimelineId());
                        intent.putExtra("comment_profile", gettingTimelineAllPojos_list.get(position).getPicture());
                        intent.putExtra("comment_created_on", gettingTimelineAllPojos_list.get(position).getCreatedOn());
                        intent.putExtra("username", createdusername);
                        // intent.putExtra("comment_image", gettingTimelineAllPojos_list.get(position).getImage().get(0).getImg());
                        try {
                            intent.putExtra("comment_image", gettingTimelineAllPojos_list.get(position).getImage().get(0).getImg());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        context.startActivity(intent);


                    }
                });


                // User Like API

                totalLikes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                if (totalLikes == 0) {

                    timelineAdapter.like.setVisibility(View.GONE);

                } else if (totalLikes == 1) {

                    timelineAdapter.like.setVisibility(View.VISIBLE);
                    timelineAdapter.like.setText(String.valueOf(totalLikes) + " Like");

                } else {

                    timelineAdapter.like.setVisibility(View.VISIBLE);
                    timelineAdapter.like.setText(String.valueOf(totalLikes) + " Likes");
                }

                if (gettingTimelineAllPojos_list.get(position).getTlLikes().equals("0")) {

                    gettingTimelineAllPojos_list.get(position).setLiked(false);
                    timelineAdapter.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));


                } else {

                    gettingTimelineAllPojos_list.get(position).setLiked(true);
                    timelineAdapter.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));

                }


                // Image like button on click
                timelineAdapter.img_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Animation for like click

                        final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                        v.startAnimation(anim);


                        if (gettingTimelineAllPojos_list.get(position).isLiked()) {

                            timelineAdapter.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                            gettingTimelineAllPojos_list.get(position).setLiked(false);
                            gettingTimelineAllPojos_list.get(position).setTlLikes(String.valueOf(0));


                            int total_likes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                            if (total_likes == 1) {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                timelineAdapter.like.setVisibility(View.GONE);


                            } else if (total_likes == 2) {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                timelineAdapter.like.setText(String.valueOf(total_likes) + " Like");
                                timelineAdapter.like.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                timelineAdapter.like.setText(String.valueOf(total_likes) + " Likes");
                                timelineAdapter.like.setVisibility(View.VISIBLE);

                            }

                            if (Utility.isConnected(context)) {

                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                                    int timelineId = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getTimelineId());


                                    Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", timelineId, 0, userid);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                if (responseStatus.equals("nodata")) {

                                                    //Success


                                                }

                                            } else {

                                                Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            } else {
                                Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                            }


                        } else {

                            timelineAdapter.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                            gettingTimelineAllPojos_list.get(position).setLiked(true);
                            gettingTimelineAllPojos_list.get(position).setTlLikes(String.valueOf(1));


                            int total_likes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                            if (total_likes == 0) {

                                total_likes = total_likes + 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                timelineAdapter.like.setText(String.valueOf(total_likes) + " Like");
                                timelineAdapter.like.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes + 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                timelineAdapter.like.setText(String.valueOf(total_likes) + " Likes");

                            }
                            if (Utility.isConnected(context)) {
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                                    int timelineId = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getTimelineId());


                                    Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", timelineId, 1, userid);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                if (responseStatus.equals("success")) {

                                                    //Success

                                                }

                                            } else {

                                                Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            } else {
                                Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                });


                // Likes layout click listener

                timelineAdapter.rl_likes_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent intent = new Intent(context, likesActivity.class);
                        intent.putExtra("Likes_timelineid", gettingTimelineAllPojos_list.get(position).getTimelineId());
                        context.startActivity(intent);

                    }
                });

                // Comments layout click listener

                timelineAdapter.rl_comments_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String createdusername = null;
                        // Displaying user name

                        try {

                            String username = gettingTimelineAllPojos_list.get(position).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getLastName();
                            String hotelname = gettingTimelineAllPojos_list.get(position).getTimelineHotel();
                            int taggedCount = gettingTimelineAllPojos_list.get(position).getWhomCount();

                            String taggedPeopleText = "";
                            String finalUserNameText = "";

                            if (taggedCount != 0) {

                                String taggedFirstName = "";
                                String taggedSecondName = "";


                                switch (taggedCount) {


                                    case 1:

                                        taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                        taggedPeopleText = taggedFirstName;

                                        break;

                                    case 2:

                                        taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                        taggedSecondName = gettingTimelineAllPojos_list.get(position).getWhom().get(1).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(1).getLastName();
                                        taggedPeopleText = taggedFirstName + " and " + taggedSecondName;


                                        break;


                                    default:

                                        taggedFirstName = gettingTimelineAllPojos_list.get(position).getWhom().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getWhom().get(0).getLastName();
                                        taggedPeopleText = taggedFirstName + " and " + String.valueOf(taggedCount - 1) + " Others";

                                        break;

                                }


                            }

                            if (taggedCount == 0) {

                                finalUserNameText = username + " at " + hotelname;

                                SpannableString ss = new SpannableString(finalUserNameText);

                                ClickableSpan clickableSpan1 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View widget) {

                                        String user_id = gettingTimelineAllPojos_list.get(position).getUserId();
                                        String timelineid = gettingTimelineAllPojos_list.get(position).getTimelineId();

                                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                                            context.startActivity(new Intent(context, Profile.class));
                                        } else {

                                            Intent intent = new Intent(context, User_profile_Activity.class);
                                            intent.putExtra("created_by", user_id);
                                            context.startActivity(intent);
                                        }

                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                ClickableSpan clickableSpan2 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View widget) {

                                        Intent intent = new Intent(context, Review_in_detail_activity.class);
                                        intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                        intent.putExtra("f_name", gettingTimelineAllPojos_list.get(position).getFirstName());
                                        intent.putExtra("l_name", gettingTimelineAllPojos_list.get(position).getLastName());
                                        intent.putExtra("reviewid", "0");
                                        intent.putExtra("timelineId", gettingTimelineAllPojos_list.get(position).getTimelineId());
                                        context.startActivity(intent);

                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                ss.setSpan(clickableSpan1, 0, username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                ss.setSpan(clickableSpan2, username.length() + 4, finalUserNameText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                                // timelineAdapter.username.setText(ss);
                                // timelineAdapter.username.setMovementMethod(LinkMovementMethod.getInstance());
                                createdusername = String.valueOf(ss);

                            } else {

                                String isWith = " is with ";
                                String at = " at ";

                                finalUserNameText = username + isWith + taggedPeopleText + at + hotelname;

                                SpannableString ss = new SpannableString(finalUserNameText);

                                /*User name on click listener*/
                                ClickableSpan clickableSpan1 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View v) {

                                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                                            context.startActivity(new Intent(context, Profile.class));
                                        } else {

                                            Intent intent = new Intent(context, User_profile_Activity.class);
                                            intent.putExtra("created_by", userId);
                                            context.startActivity(intent);

                                        }

                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                ClickableSpan clickableSpan2 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View v) {
                                        /*Alert dialog*/

                                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                                        LayoutInflater inflater1 = LayoutInflater.from(context);
                                        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.alert_show_tagged_people, null);
                                        dialogBuilder.setView(dialogView);
                                        dialogBuilder.setTitle("");

                                        recyler_tagged_people = (RecyclerView) dialogView.findViewById(R.id.recyler_tagged_people);

                                        /*Tagged people adapter*/
                                        showTaggedPeopleAdapter = new ShowTaggedPeopleAdapter(context, gettingTimelineAllPojos_list.get(position).getWhom());
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                        recyler_tagged_people.setLayoutManager(mLayoutManager);
                                        recyler_tagged_people.setItemAnimator(new DefaultItemAnimator());
                                        recyler_tagged_people.setAdapter(showTaggedPeopleAdapter);
                                        recyler_tagged_people.hasFixedSize();
                                        /*Alert dialog*/
                                        final AlertDialog dialog = dialogBuilder.create();
                                        dialog.show();


                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };

                                /*Hotel name click listener*/

                                ClickableSpan clickableSpan3 = new ClickableSpan() {
                                    @Override
                                    public void onClick(View widget) {

                                        Intent intent = new Intent(context, Review_in_detail_activity.class);
                                        intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                        intent.putExtra("hotelid", gettingTimelineAllPojos_list.get(position).getHotelId());
                                        intent.putExtra("f_name", gettingTimelineAllPojos_list.get(position).getFirstName());
                                        intent.putExtra("l_name", gettingTimelineAllPojos_list.get(position).getLastName());
                                        intent.putExtra("reviewid", "0");
                                        intent.putExtra("timelineId", gettingTimelineAllPojos_list.get(position).getTimelineId());
                                        context.startActivity(intent);


                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(Color.BLACK);
                                        ds.setUnderlineText(false);
                                    }
                                };


                                int first = 0, second = username.length();
                                int third = username.length() + isWith.length(), fourth = username.length() + isWith.length() + taggedPeopleText.length();
                                int fifth = username.length() + isWith.length() + taggedPeopleText.length() + at.length(), sixth = finalUserNameText.length();


                                ss.setSpan(clickableSpan1, first, second, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                ss.setSpan(clickableSpan2, third, fourth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                ss.setSpan(clickableSpan3, fifth, sixth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


                                createdusername = String.valueOf(ss);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Intent intent = new Intent(context, Comments_activity.class);

                        // Bundle bundle = new Bundle();
                        intent.putExtra("comment_posttpe", gettingTimelineAllPojos_list.get(position).getPostType());
                        intent.putExtra("comment_timelineid", gettingTimelineAllPojos_list.get(position).getTimelineId());
                        intent.putExtra("comment_profile", gettingTimelineAllPojos_list.get(position).getPicture());
                        intent.putExtra("comment_created_on", gettingTimelineAllPojos_list.get(position).getCreatedOn());
                        intent.putExtra("username", createdusername);
                        // intent.putExtra("comment_image", gettingTimelineAllPojos_list.get(position).getImage().get(0).getImg());
                        try {
                            intent.putExtra("comment_image", gettingTimelineAllPojos_list.get(position).getImage().get(0).getImg());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        context.startActivity(intent);


                    }
                });


                // User photo click

                timelineAdapter.userphoto_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                            context.startActivity(new Intent(context, Profile.class));

                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }


                    }


                });

                // Image view more click listener

                timelineAdapter.img_view_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {

                        String timelineid = gettingTimelineAllPojos_list.get(position).getTimelineId();
                        String userid = gettingTimelineAllPojos_list.get(position).getUserId();

                        /*Custom alert dialog with options*/

                        CustomDialogClass ccd = new CustomDialogClass(context, timelineid, userid, position);
                        Objects.requireNonNull(ccd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        ccd.setCancelable(true);
                        ccd.show();

                    }


                });
                break;

            case 1:

                final EventsAdapter_notification eventsAdapterNotification = (EventsAdapter_notification) timelineAdapeterHolder;

                newGettingTimelineAllPojo = gettingTimelineAllPojos_list.get(position);

                String eventDate = null;
                try {
                    eventDate = getMonthName(newGettingTimelineAllPojo.getStartDate()) + " " + getDate(newGettingTimelineAllPojo.getStartDate());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // Event Image Loading


                GlideApp.with(context)
                        .load(newGettingTimelineAllPojo.getEventImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .centerCrop()
                        .into(eventsAdapterNotification.eventBackground);

                // UserPciture Loading


                GlideApp
                        .with(context)
                        .load(Pref_storage.getDetail(context, "picture_user_login"))
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .placeholder(R.drawable.ic_add_photo)
                        .into(eventsAdapterNotification.userphoto_comment);

                // User comment Image loading


                GlideApp
                        .with(context)
                        .load(newGettingTimelineAllPojo.getPicture())
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .placeholder(R.drawable.ic_add_photo)
                        .into(eventsAdapterNotification.eventview_user_photo);

                if (newGettingTimelineAllPojo.getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    eventsAdapterNotification.txt_view_event_username.setText(R.string.you);

                } else {

                    eventsAdapterNotification.txt_view_event_username.setText(newGettingTimelineAllPojo.getFirstName().concat(" ").
                            concat(newGettingTimelineAllPojo.getLastName()));
                }

                eventsAdapterNotification.date.setText(eventDate);
                eventsAdapterNotification.txt_view_event_name.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(newGettingTimelineAllPojo.getEventName().concat(".")));

                eventsAdapterNotification.eventName.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(newGettingTimelineAllPojo.getEventName()));

                eventsAdapterNotification.location.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(newGettingTimelineAllPojo.getLocation()));


                // Comments visibility

                try {

                    String comment = gettingTimelineAllPojos_list.get(position).getTotalComments();
                    if (Integer.parseInt(comment) == 0 || comment == null) {

                        eventsAdapterNotification.View_all_comment.setVisibility(GONE);
                        eventsAdapterNotification.commenttest.setVisibility(GONE);

                    } else {

                        eventsAdapterNotification.comment.setText(comment);

                        if (Integer.parseInt(comment) == 1) {
                            eventsAdapterNotification.View_all_comment.setText(R.string.view);
                            eventsAdapterNotification.commenttest.setText(R.string.commenttt);
                            eventsAdapterNotification.View_all_comment.setVisibility(VISIBLE);
                            eventsAdapterNotification.commenttest.setVisibility(VISIBLE);
                        } else if (Integer.parseInt(comment) > 1) {
                            eventsAdapterNotification.View_all_comment.setVisibility(VISIBLE);
                            eventsAdapterNotification.commenttest.setVisibility(VISIBLE);
                        }

                        // display latest comment with username
                        if (gettingTimelineAllPojos_list.get(position).getTimelineLastCmtCount() == 1) {

                            eventsAdapterNotification.recently_commented_ll.setVisibility(VISIBLE);

                            eventsAdapterNotification.recently_commented.setText(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getTlComments());
                            eventsAdapterNotification.username_latestviewed.setText(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getFirstName());

                            /* Posted date functionality */
                            String eventcreatedon = gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getCreatedOn();

                            Utility.setTimeStamp(eventcreatedon, eventsAdapterNotification.created_on);


                            GlideApp.with(context)
                                    .load(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getPicture())
                                    .centerCrop()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .thumbnail(0.1f)
                                    .placeholder(R.drawable.ic_add_photo)
                                    .into(eventsAdapterNotification.userphoto);


                        } else {

                            eventsAdapterNotification.recently_commented_ll.setVisibility(GONE);
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                // Timestamp for timeline

                String createdonnew = gettingTimelineAllPojos_list.get(position).getCreatedOn();


                try {

                    Utility.setTimeStamp(createdonnew, eventsAdapterNotification.txt_created_on);


                } catch (Exception e) {
                    e.printStackTrace();
                }


                // Comments activity intent

                eventsAdapterNotification.rl_comment_adapter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, Comments_activity.class);
                        intent.putExtra("comment_posttpe", gettingTimelineAllPojos_list.get(position).getPostType());
                        intent.putExtra("comment_timelineid", gettingTimelineAllPojos_list.get(position).getTimelineId());
                        intent.putExtra("username", gettingTimelineAllPojos_list.get(position).getFirstName());
                        intent.putExtra("comment_eventname", gettingTimelineAllPojos_list.get(position).getEventName());
                        intent.putExtra("comment_created_on", gettingTimelineAllPojos_list.get(position).getCreatedOn());
                        intent.putExtra("comment_image", gettingTimelineAllPojos_list.get(position).getEventImage());
                        intent.putExtra("comment_profile", gettingTimelineAllPojos_list.get(position).getPicture());

                        context.startActivity(intent);

                    }
                });


                // To user profile
                eventsAdapterNotification.eventview_user_photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();


                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }

                    }
                });


                // To user profile

                eventsAdapterNotification.txt_view_event_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }

                    }
                });


                totalLikes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                if (totalLikes == 0) {

                    eventsAdapterNotification.like.setVisibility(View.GONE);

                } else if (totalLikes == 1) {

                    eventsAdapterNotification.like.setVisibility(View.VISIBLE);
                    eventsAdapterNotification.like.setText(String.valueOf(totalLikes) + " Like");

                } else {

                    eventsAdapterNotification.like.setVisibility(View.VISIBLE);
                    eventsAdapterNotification.like.setText(String.valueOf(totalLikes) + " Likes");
                }

                if (gettingTimelineAllPojos_list.get(position).getTlLikes().equals("0")) {

                    gettingTimelineAllPojos_list.get(position).setLiked(false);
                    eventsAdapterNotification.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));


                } else {

                    gettingTimelineAllPojos_list.get(position).setLiked(true);
                    eventsAdapterNotification.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));

                }

                // Event like click listener
                eventsAdapterNotification.img_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                        v.startAnimation(anim);

                        if (gettingTimelineAllPojos_list.get(position).isLiked()) {

                            eventsAdapterNotification.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                            gettingTimelineAllPojos_list.get(position).setLiked(false);
                            gettingTimelineAllPojos_list.get(position).setTlLikes(String.valueOf(0));

                            int total_likes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                            if (total_likes == 1) {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
//                                            holder.event_discussion_like_count.setText(String.valueOf(--totalLikes)+" Likes");
                                eventsAdapterNotification.like.setVisibility(View.GONE);


                            } else if (total_likes == 2) {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsAdapterNotification.like.setText(String.valueOf(total_likes) + " Like");
                                eventsAdapterNotification.like.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsAdapterNotification.like.setText(String.valueOf(total_likes) + " Likes");
                                eventsAdapterNotification.like.setVisibility(View.VISIBLE);

                            }

                            // Event Like API call

                            if (Utility.isConnected(context)) {
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                    Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(gettingTimelineAllPojos_list.get(position).getTimelineId()), 0, userid);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                if (responseStatus.equals("nodata")) {

                                                    //Success


                                                }

                                            } else {

                                                Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            } else {
                                Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                            }


                        } else {


                            eventsAdapterNotification.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                            gettingTimelineAllPojos_list.get(position).setLiked(true);
                            gettingTimelineAllPojos_list.get(position).setTlLikes(String.valueOf(1));


                            int total_likes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                            if (total_likes == 0) {

                                total_likes = total_likes + 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsAdapterNotification.like.setText(String.valueOf(total_likes) + " Like");
                                eventsAdapterNotification.like.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes + 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsAdapterNotification.like.setText(String.valueOf(total_likes) + " Likes");

                            }
                            // Like API call
                            if (Utility.isConnected(context)) {
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                    Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(gettingTimelineAllPojos_list.get(position).getTimelineId()), 1, userid);

                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                if (responseStatus.equals("success")) {

                                                    //Success


                                                }

                                            } else {

                                                Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            } else {
                                Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

                            }


                        }

                    }

                });

                // Edit text listener for commenting
                eventsAdapterNotification.edittxt_comment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        if (eventsAdapterNotification.edittxt_comment.getText().toString().trim().length() != 0) {

                            eventsAdapterNotification.txt_adapter_post.setVisibility(VISIBLE);

                        } else {

                            eventsAdapterNotification.txt_adapter_post.setVisibility(GONE);
                        }


                    }
                });


                // Likes layout intent
                eventsAdapterNotification.rl_likes_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, likesActivity.class);
                        intent.putExtra("Likes_timelineid", gettingTimelineAllPojos_list.get(position).getTimelineId());
                        context.startActivity(intent);

                    }
                });

                // Comments layout intent

                eventsAdapterNotification.rl_comments_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, Comments_activity.class);
                        intent.putExtra("comment_timelinepicture", gettingTimelineAllPojos_list.get(position).getPicture());
                        intent.putExtra("comment_posttpe", gettingTimelineAllPojos_list.get(position).getPostType());
                        intent.putExtra("comment_timelineid", gettingTimelineAllPojos_list.get(position).getEventId());
                        intent.putExtra("username", gettingTimelineAllPojos_list.get(position).getFirstName());
                        intent.putExtra("comment_eventname", gettingTimelineAllPojos_list.get(position).getEventName());
                        intent.putExtra("comment_created_on", gettingTimelineAllPojos_list.get(position).getCreatedOn());
                        intent.putExtra("comment_image", gettingTimelineAllPojos_list.get(position).getEventImage());
                        intent.putExtra("comment_profile", gettingTimelineAllPojos_list.get(position).getPicture());

                        context.startActivity(intent);
                    }
                });

                // View event intent

                eventsAdapterNotification.viewEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ViewEvent.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("eventId", Integer.parseInt(gettingTimelineAllPojos_list.get(position).getEventId()));
                        intent.putExtras(bundle);
                        context.startActivity(intent);


                    }
                });


                break;


            case 2:


                final Events_going_adapter_timeline eventsGoingAdapterTimeline = (Events_going_adapter_timeline) timelineAdapeterHolder;

                newGettingTimelineAllPojo = gettingTimelineAllPojos_list.get(position);

                String eventDate_going = null;
                try {
                    eventDate_going = getMonthName(gettingTimelineAllPojos_list.get(position).getStartDate()) + " " + getDate(gettingTimelineAllPojos_list.get(position).getStartDate());
                } catch (Exception e) {
                    e.printStackTrace();
                }


                // Event image


                GlideApp.with(context)
                        .load(gettingTimelineAllPojos_list.get(position).getEventImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.001f)
                        .centerCrop()
                        .into(eventsGoingAdapterTimeline.eventBackground);


                // User Picture

                GlideApp
                        .with(context)
                        .load(gettingTimelineAllPojos_list.get(position).getPicture())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .centerCrop()
                        .placeholder(R.drawable.ic_add_photo)
                        .into(eventsGoingAdapterTimeline.eventgoing_user_photo);


                // Self image of user

                GlideApp.with(context)
                        .load(Pref_storage.getDetail(context, "picture_user_login"))
                        .centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .placeholder(R.drawable.ic_add_photo)
                        .into(eventsGoingAdapterTimeline.userphoto_comment);

                eventsGoingAdapterTimeline.date.setText(eventDate_going);
                eventsGoingAdapterTimeline.eventName.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettingTimelineAllPojos_list.get(position).getEventName()));
                eventsGoingAdapterTimeline.txt_view_event_going_name.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettingTimelineAllPojos_list.get(position).getEventName()));
                eventsGoingAdapterTimeline.location.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettingTimelineAllPojos_list.get(position).getLocation()));


                if (gettingTimelineAllPojos_list.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    eventsGoingAdapterTimeline.txt_view_event_going_username.setText(R.string.you);

                } else {
                    eventsGoingAdapterTimeline.txt_view_event_going_username.setText(gettingTimelineAllPojos_list.get(position).getFirstName().concat(" ").
                            concat(gettingTimelineAllPojos_list.get(position).getLastName()));
                }


                // Comments visibility

                try {

                    String comment = gettingTimelineAllPojos_list.get(position).getTotalComments();
                    if (Integer.parseInt(comment) == 0 || comment == null) {

                        eventsGoingAdapterTimeline.View_all_comment.setVisibility(GONE);
                        eventsGoingAdapterTimeline.commenttest.setVisibility(GONE);

                    } else {

                        eventsGoingAdapterTimeline.comment.setText(comment);

                        if (Integer.parseInt(comment) == 1) {
                            eventsGoingAdapterTimeline.View_all_comment.setText(R.string.view);
                            eventsGoingAdapterTimeline.commenttest.setText(R.string.commenttt);
                            eventsGoingAdapterTimeline.View_all_comment.setVisibility(VISIBLE);
                            eventsGoingAdapterTimeline.commenttest.setVisibility(VISIBLE);
                        } else if (Integer.parseInt(comment) > 1) {
                            eventsGoingAdapterTimeline.View_all_comment.setVisibility(VISIBLE);
                            eventsGoingAdapterTimeline.commenttest.setVisibility(VISIBLE);
                        }


                        // display latest comment with username
                        if (gettingTimelineAllPojos_list.get(position).getTimelineLastCmtCount() == 1) {

                            eventsGoingAdapterTimeline.recently_commented_ll.setVisibility(VISIBLE);

                            eventsGoingAdapterTimeline.recently_commented.setText(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getTlComments());
                            eventsGoingAdapterTimeline.username_latestviewed.setText(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getFirstName());

                            /* Posted date functionality */
                            String eventcreatedon = gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getCreatedOn();

                            Utility.setTimeStamp(eventcreatedon, eventsGoingAdapterTimeline.created_on);


                            GlideApp.with(context)
                                    .load(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getPicture())
                                    .centerCrop()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .thumbnail(0.1f)
                                    .placeholder(R.drawable.ic_add_photo)
                                    .into(eventsGoingAdapterTimeline.userphoto);


                        } else {
                            eventsGoingAdapterTimeline.recently_commented_ll.setVisibility(GONE);
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                // Timestamp for timeline

                String createdgoing = gettingTimelineAllPojos_list.get(position).getCreatedOn();


                try {
                    Utility.setTimeStamp(createdgoing, eventsGoingAdapterTimeline.txt_created_on);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                // Comments activity intent

                eventsGoingAdapterTimeline.rl_comment_adapter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, Comments_activity.class);
                        intent.putExtra("comment_timelineid", gettingTimelineAllPojos_list.get(position).getTimelineId());

                        intent.putExtra("comment_posttpe", gettingTimelineAllPojos_list.get(position).getPostType());
                        intent.putExtra("username", gettingTimelineAllPojos_list.get(position).getFirstName());
                        intent.putExtra("comment_eventname", gettingTimelineAllPojos_list.get(position).getEventName());
                        intent.putExtra("comment_created_on", gettingTimelineAllPojos_list.get(position).getCreatedOn());
                        intent.putExtra("comment_image", gettingTimelineAllPojos_list.get(position).getEventImage());
                        intent.putExtra("comment_profile", gettingTimelineAllPojos_list.get(position).getPicture());
                        context.startActivity(intent);

                    }
                });


                //To user Profile
                eventsGoingAdapterTimeline.eventgoing_user_photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = gettingTimelineAllPojos_list.get(position).getUserId();
                        String timelineid = gettingTimelineAllPojos_list.get(position).getTimelineId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }
                });


                // To user Profile

                eventsGoingAdapterTimeline.txt_view_event_going_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = gettingTimelineAllPojos_list.get(position).getUserId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }
                });


                totalLikes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                if (totalLikes == 0) {

                    eventsGoingAdapterTimeline.like.setVisibility(View.GONE);

                } else if (totalLikes == 1) {

                    eventsGoingAdapterTimeline.like.setVisibility(View.VISIBLE);
                    eventsGoingAdapterTimeline.like.setText(String.valueOf(totalLikes) + " Like");

                } else {

                    eventsGoingAdapterTimeline.like.setVisibility(View.VISIBLE);
                    eventsGoingAdapterTimeline.like.setText(String.valueOf(totalLikes) + " Likes");
                }

                if (gettingTimelineAllPojos_list.get(position).getTlLikes().equals("0")) {

                    gettingTimelineAllPojos_list.get(position).setLiked(false);
                    eventsGoingAdapterTimeline.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));


                } else {

                    gettingTimelineAllPojos_list.get(position).setLiked(true);
                    eventsGoingAdapterTimeline.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));

                }

                // Image like button click listener

                eventsGoingAdapterTimeline.img_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                        v.startAnimation(anim);
//                        Log.e("TestingLike", "TestingLike->" + gettingTimelineAllPojos_list.get(position).isLiked());

                        if (gettingTimelineAllPojos_list.get(position).isLiked()) {

                            eventsGoingAdapterTimeline.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                            gettingTimelineAllPojos_list.get(position).setLiked(false);
                            gettingTimelineAllPojos_list.get(position).setTlLikes(String.valueOf(0));


                            int total_likes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                            if (total_likes == 1) {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsGoingAdapterTimeline.like.setVisibility(View.GONE);


                            } else if (total_likes == 2) {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsGoingAdapterTimeline.like.setText(String.valueOf(total_likes) + " Like");
                                eventsGoingAdapterTimeline.like.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsGoingAdapterTimeline.like.setText(String.valueOf(total_likes) + " Likes");
                                eventsGoingAdapterTimeline.like.setVisibility(View.VISIBLE);

                            }


                            //Calling image like api

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(gettingTimelineAllPojos_list.get(position).getTimelineId()), 0, userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                        if (response.body() != null) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("nodata")) {

                                                //Success
                                            }

                                        } else {

                                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                    }
                                });

                            } catch (Exception e) {

                                e.printStackTrace();

                            }

                        } else {

                            eventsGoingAdapterTimeline.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                            gettingTimelineAllPojos_list.get(position).setLiked(true);
                            gettingTimelineAllPojos_list.get(position).setTlLikes(String.valueOf(1));


                            int total_likes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                            if (total_likes == 0) {

                                total_likes = total_likes + 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsGoingAdapterTimeline.like.setText(String.valueOf(total_likes) + " Like");
                                eventsGoingAdapterTimeline.like.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes + 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsGoingAdapterTimeline.like.setText(String.valueOf(total_likes) + " Likes");

                            }
                            // Image button like api call

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(gettingTimelineAllPojos_list.get(position).getTimelineId()), 1, userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                        if (response.body() != null) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("success")) {

                                                //Success


                                            }

                                        } else {

                                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                    }
                                });

                            } catch (Exception e) {

                                e.printStackTrace();

                            }

                        }


                    }
                });
                // Edit text comment listener

                eventsGoingAdapterTimeline.edittxt_comment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (eventsGoingAdapterTimeline.edittxt_comment.getText().toString().trim().length() != 0) {
                            eventsGoingAdapterTimeline.txt_adapter_post.setVisibility(VISIBLE);
                        } else {
                            eventsGoingAdapterTimeline.txt_adapter_post.setVisibility(GONE);
                        }


                    }
                });

                // Likes layout intent

                eventsGoingAdapterTimeline.rl_likes_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent intent = new Intent(context, likesActivity.class);
                        intent.putExtra("Likes_timelineid", gettingTimelineAllPojos_list.get(position).getTimelineId());
                        context.startActivity(intent);
                    }
                });

                // Comments layout intent

                eventsGoingAdapterTimeline.rl_comments_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, Comments_activity.class);
                        intent.putExtra("comment_timelineid", gettingTimelineAllPojos_list.get(position).getEventId());
                        intent.putExtra("comment_posttpe", gettingTimelineAllPojos_list.get(position).getPostType());
                        intent.putExtra("username", gettingTimelineAllPojos_list.get(position).getFirstName());
                        intent.putExtra("comment_eventname", gettingTimelineAllPojos_list.get(position).getEventName());
                        intent.putExtra("comment_created_on", gettingTimelineAllPojos_list.get(position).getCreatedOn());
                        intent.putExtra("comment_image", gettingTimelineAllPojos_list.get(position).getEventImage());
                        intent.putExtra("comment_profile", gettingTimelineAllPojos_list.get(position).getPicture());
                        context.startActivity(intent);

                    }
                });

                // View event intent

                eventsGoingAdapterTimeline.viewEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ViewEvent.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("eventId", Integer.parseInt(gettingTimelineAllPojos_list.get(position).getEventId()));
                        intent.putExtras(bundle);
                        context.startActivity(intent);


                    }
                });


                break;


            case 3:

                final Events_interested_adapter_timeline eventsInterestedAdapterTimeline = (Events_interested_adapter_timeline) timelineAdapeterHolder;

                newGettingTimelineAllPojo = gettingTimelineAllPojos_list.get(position);


                String eventDate_interested = null;
                try {
                    eventDate_interested = getMonthName(gettingTimelineAllPojos_list.get(position).getStartDate()) + " " + getDate(gettingTimelineAllPojos_list.get(position).getStartDate());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Event image loading

                GlideApp
                        .with(context)
                        .load(gettingTimelineAllPojos_list.get(position).getEventImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .centerCrop()
                        .into(eventsInterestedAdapterTimeline.eventBackground);


                // User Picture

                GlideApp
                        .with(context)
                        .load(gettingTimelineAllPojos_list.get(position).getPicture())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .centerCrop()
                        .placeholder(R.drawable.ic_add_photo)
                        .into(eventsInterestedAdapterTimeline.event_user_photo);
                // User image loading

                GlideApp
                        .with(context)
                        .load(Pref_storage.getDetail(context, "picture_user_login"))
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .placeholder(R.drawable.ic_add_photo)
                        .into(eventsInterestedAdapterTimeline.userphoto_comment);


                eventsInterestedAdapterTimeline.date.setText(eventDate_interested);


                eventsInterestedAdapterTimeline.eventName.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettingTimelineAllPojos_list.get(position).getEventName()));


                eventsInterestedAdapterTimeline.txt_view_event_interest_name.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettingTimelineAllPojos_list.get(position).getEventName().concat(".")));

                eventsInterestedAdapterTimeline.location.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettingTimelineAllPojos_list.get(position).getLocation()));


                if (gettingTimelineAllPojos_list.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    eventsInterestedAdapterTimeline.txt_view_event_interest_username.setText(R.string.you);

                } else {
                    eventsInterestedAdapterTimeline.txt_view_event_interest_username.setText(newGettingTimelineAllPojo.getFirstName().concat(" ").
                            concat(gettingTimelineAllPojos_list.get(position).getLastName()));
                }


                // Comments visibility

                try {


                    String comment = gettingTimelineAllPojos_list.get(position).getTotalComments();

                    if (Integer.parseInt(comment) == 0 || comment == null) {

                        eventsInterestedAdapterTimeline.View_all_comment.setVisibility(GONE);
                        eventsInterestedAdapterTimeline.commenttest.setVisibility(GONE);
                    } else {
                        eventsInterestedAdapterTimeline.comment.setText(comment);
                        if (Integer.parseInt(comment) == 1) {
                            eventsInterestedAdapterTimeline.View_all_comment.setText(R.string.view);
                            eventsInterestedAdapterTimeline.commenttest.setText(R.string.commenttt);
                            eventsInterestedAdapterTimeline.View_all_comment.setVisibility(VISIBLE);
                            eventsInterestedAdapterTimeline.commenttest.setVisibility(VISIBLE);
                        } else if (Integer.parseInt(comment) > 1) {
                            eventsInterestedAdapterTimeline.View_all_comment.setVisibility(VISIBLE);
                            eventsInterestedAdapterTimeline.commenttest.setVisibility(VISIBLE);
                        }

                        // display latest comment with username
                        if (gettingTimelineAllPojos_list.get(position).getTimelineLastCmtCount() == 1) {

                            eventsInterestedAdapterTimeline.recently_commented_ll.setVisibility(VISIBLE);

                            eventsInterestedAdapterTimeline.recently_commented.setText(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getTlComments());
                            eventsInterestedAdapterTimeline.username_latestviewed.setText(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getFirstName());

                            /* Posted date functionality */
                            String eventcreatedon = gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getCreatedOn();

                            Utility.setTimeStamp(eventcreatedon, eventsInterestedAdapterTimeline.created_on);


                            GlideApp.with(context)
                                    .load(gettingTimelineAllPojos_list.get(position).getTimelineLastCmt().get(0).getPicture())
                                    .centerCrop()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .thumbnail(0.1f)
                                    .placeholder(R.drawable.ic_add_photo)
                                    .into(eventsInterestedAdapterTimeline.userphoto);


                        } else {
                            eventsInterestedAdapterTimeline.recently_commented_ll.setVisibility(GONE);
                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                // Timestamp for timeline

                String created_interested = gettingTimelineAllPojos_list.get(position).getCreatedOn();

                try {
                    Utility.setTimeStamp(created_interested, eventsInterestedAdapterTimeline.txt_created_on);


                } catch (Exception e) {
                    e.printStackTrace();
                }


                // Comments activity intent

                eventsInterestedAdapterTimeline.rl_comment_adapter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, Comments_activity.class);
                        intent.putExtra("comment_timelineid", gettingTimelineAllPojos_list.get(position).getTimelineId());
                        intent.putExtra("comment_posttpe", gettingTimelineAllPojos_list.get(position).getPostType());
                        intent.putExtra("username", gettingTimelineAllPojos_list.get(position).getFirstName());
                        intent.putExtra("comment_eventname", gettingTimelineAllPojos_list.get(position).getEventName());
                        intent.putExtra("comment_created_on", gettingTimelineAllPojos_list.get(position).getCreatedOn());
                        intent.putExtra("comment_image", gettingTimelineAllPojos_list.get(position).getEventImage());
                        intent.putExtra("comment_profile", gettingTimelineAllPojos_list.get(position).getPicture());
                        context.startActivity(intent);

                    }
                });

                // To user profile
                eventsInterestedAdapterTimeline.event_user_photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = gettingTimelineAllPojos_list.get(position).getUserId();
                        String timelineid = gettingTimelineAllPojos_list.get(position).getTimelineId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }
                });


                // To user Profile

                eventsInterestedAdapterTimeline.txt_view_event_interest_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = gettingTimelineAllPojos_list.get(position).getUserId();
                        String timelineid = gettingTimelineAllPojos_list.get(position).getTimelineId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }
                });


                totalLikes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                if (totalLikes == 0) {

                    eventsInterestedAdapterTimeline.like.setVisibility(View.GONE);

                } else if (totalLikes == 1) {

                    eventsInterestedAdapterTimeline.like.setVisibility(View.VISIBLE);
                    eventsInterestedAdapterTimeline.like.setText(String.valueOf(totalLikes) + " Like");

                } else {

                    eventsInterestedAdapterTimeline.like.setVisibility(View.VISIBLE);
                    eventsInterestedAdapterTimeline.like.setText(String.valueOf(totalLikes) + " Likes");
                }

                if (gettingTimelineAllPojos_list.get(position).getTlLikes().equals("0")) {

                    gettingTimelineAllPojos_list.get(position).setLiked(false);
                    eventsInterestedAdapterTimeline.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));


                } else {

                    gettingTimelineAllPojos_list.get(position).setLiked(true);
                    eventsInterestedAdapterTimeline.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));

                }


                // Image like click listener

                eventsInterestedAdapterTimeline.img_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Animation for like click listener

                        final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                        v.startAnimation(anim);


                        if (gettingTimelineAllPojos_list.get(position).isLiked()) {

                            eventsInterestedAdapterTimeline.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                            gettingTimelineAllPojos_list.get(position).setLiked(false);
                            gettingTimelineAllPojos_list.get(position).setTlLikes(String.valueOf(0));


                            int total_likes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                            if (total_likes == 1) {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsInterestedAdapterTimeline.like.setVisibility(View.GONE);


                            } else if (total_likes == 2) {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsInterestedAdapterTimeline.like.setText(String.valueOf(total_likes) + " Like");
                                eventsInterestedAdapterTimeline.like.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes - 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsInterestedAdapterTimeline.like.setText(String.valueOf(total_likes) + " Likes");
                                eventsInterestedAdapterTimeline.like.setVisibility(View.VISIBLE);

                            }

                            // Calling api for event like


                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(gettingTimelineAllPojos_list.get(position).getTimelineId()), 0, userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                        if (response.body() != null) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("nodata")) {

                                                //Success


                                            }

                                        } else {

                                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                    }
                                });

                            } catch (Exception e) {

                                e.printStackTrace();

                            }

                        } else {

                            eventsInterestedAdapterTimeline.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                            gettingTimelineAllPojos_list.get(position).setLiked(true);
                            gettingTimelineAllPojos_list.get(position).setTlLikes(String.valueOf(1));


                            int total_likes = gettingTimelineAllPojos_list.get(position).getTotalLikes();

                            if (total_likes == 0) {

                                total_likes = total_likes + 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsInterestedAdapterTimeline.like.setText(String.valueOf(total_likes) + " Like");
                                eventsInterestedAdapterTimeline.like.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes + 1;
                                gettingTimelineAllPojos_list.get(position).setTotalLikes(total_likes);
                                eventsInterestedAdapterTimeline.like.setText(String.valueOf(total_likes) + " Likes");

                            }

                            // Calling api for event like

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", Integer.parseInt(gettingTimelineAllPojos_list.get(position).getTimelineId()), 1, userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(@NonNull Call<CommonOutputPojo> call, @NonNull Response<CommonOutputPojo> response) {


                                        if (response.body() != null) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("success")) {

                                                //Success


                                            }

                                        } else {

                                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                    }
                                });

                            } catch (Exception e) {

                                e.printStackTrace();

                            }

                        }


                    }
                });

                // Event comment edit text listener

                eventsInterestedAdapterTimeline.edittxt_comment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (eventsInterestedAdapterTimeline.edittxt_comment.getText().toString().trim().length() != 0) {
                            eventsInterestedAdapterTimeline.txt_adapter_post.setVisibility(VISIBLE);
                        } else {
                            eventsInterestedAdapterTimeline.txt_adapter_post.setVisibility(GONE);
                        }


                    }
                });

                // Event likes intent

                eventsInterestedAdapterTimeline.rl_likes_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, likesActivity.class);
                        intent.putExtra("Likes_timelineid", gettingTimelineAllPojos_list.get(position).getTimelineId());
                        context.startActivity(intent);
                    }
                });

                // Event comments intent

                eventsInterestedAdapterTimeline.rl_comments_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, Comments_activity.class);
                        intent.putExtra("comment_timelineid", gettingTimelineAllPojos_list.get(position).getEventId());
                        intent.putExtra("comment_posttpe", gettingTimelineAllPojos_list.get(position).getPostType());
                        intent.putExtra("username", gettingTimelineAllPojos_list.get(position).getFirstName());
                        intent.putExtra("comment_eventname", gettingTimelineAllPojos_list.get(position).getEventName());
                        intent.putExtra("comment_created_on", gettingTimelineAllPojos_list.get(position).getCreatedOn());
                        intent.putExtra("comment_image", gettingTimelineAllPojos_list.get(position).getEventImage());
                        intent.putExtra("comment_profile", gettingTimelineAllPojos_list.get(position).getPicture());
                        context.startActivity(intent);

                    }
                });

                // Event view intent

                eventsInterestedAdapterTimeline.viewEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ViewEvent.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("eventId", Integer.parseInt(gettingTimelineAllPojos_list.get(position).getEventId()));
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                });


                break;


            case 4:

                final Following_timelineAdapter following_timelineAdapter = (Following_timelineAdapter) timelineAdapeterHolder;

                try {


                    // Image loader

                    GlideApp.with(context)
                            .load(newGettingTimelineAllPojo.getPicture())
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .thumbnail(0.1f)
                            .placeholder(R.drawable.ic_add_photo)
                            .into(following_timelineAdapter.following_user_photo);


                    if (newGettingTimelineAllPojo.getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                        following_timelineAdapter.txt_following_user_name.setText(R.string.you);
                    } else {
                        following_timelineAdapter.txt_following_user_name.setText(newGettingTimelineAllPojo.getFirstName().concat(" ").concat(newGettingTimelineAllPojo.getLastName()));
                    }

                    following_timelineAdapter.txt_user_name.setText(newGettingTimelineAllPojo.getFollowuserFirstname().concat(" ").concat(newGettingTimelineAllPojo.getFollowuserLastname().concat(".")));

                    // Following click listener

                    following_timelineAdapter.rl_following_you.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String clickeduserid = gettingTimelineAllPojos_list.get(position).getFollowuserId();

                            if (clickeduserid.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                                Intent intent = new Intent(context, Profile.class);
                                intent.putExtra("created_by", clickeduserid);
                                context.startActivity(intent);
                                context.startActivity(new Intent(context, Profile.class));

                            } else {

                                Intent intent = new Intent(context, User_profile_Activity.class);
                                intent.putExtra("created_by", clickeduserid);
                                context.startActivity(intent);

                            }


                        }
                    });


                    // Timestamp for timeline

                    String createdFollowing = gettingTimelineAllPojos_list.get(position).getCreatedOn();

                    Utility.setTimeStamp(createdFollowing, following_timelineAdapter.txt_follow_ago);


                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case 5:


                final Review_created_adapter reviewCreatedAdapter = (Review_created_adapter) timelineAdapeterHolder;

                // Headline

                String headlineUsername = gettingTimelineAllPojos_list.get(position).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getLastName();
                String headlineText = "created a review.";

                reviewCreatedAdapter.tv_headline_username.setText(headlineUsername);
                reviewCreatedAdapter.tv_review_headlinetext.setText(headlineText);

                //User name click listener

                reviewCreatedAdapter.tv_headline_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                            context.startActivity(new Intent(context, Profile.class));

                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }
                    }
                });

                // Display review images display

                if (gettingTimelineAllPojos_list.get(position).getReviewInp() != null) {

                    if (Integer.parseInt(gettingTimelineAllPojos_list.get(position).getHotelId()) != 0) {

                        displayReviewItem((ArrayList<ReviewInputAllPojo>) gettingTimelineAllPojos_list.get(position).getReviewInp(), timelineAdapeterHolder, position);

                    }
                }


                break;


            case 6:


                final Review_created_adapter reviewLikedAdapter = (Review_created_adapter) timelineAdapeterHolder;

                // Headline

                String headlinelikedUsername = gettingTimelineAllPojos_list.get(position).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getLastName();
                String headlinelikedText = "liked this review.";

                reviewLikedAdapter.tv_headline_username.setText(headlinelikedUsername);
                reviewLikedAdapter.tv_review_headlinetext.setText(headlinelikedText);
                // Headline username click listener
                reviewLikedAdapter.tv_headline_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }
                    }
                });


                // Review image display setting
                if (gettingTimelineAllPojos_list.get(position).getReviewInp() != null) {

                    displayReviewItem((ArrayList<ReviewInputAllPojo>) gettingTimelineAllPojos_list.get(position).getReviewInp(), timelineAdapeterHolder, position);

                }


                break;


            case 7:


                final Review_created_adapter reviewCommentedAdapter = (Review_created_adapter) timelineAdapeterHolder;

                String headlineCommentUsername = gettingTimelineAllPojos_list.get(position).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getLastName();
                String headlineCommentText = "liked a review.";

                reviewCommentedAdapter.tv_headline_username.setText(headlineCommentUsername);
                reviewCommentedAdapter.tv_review_headlinetext.setText(headlineCommentText);

                // Review headline adapter

                reviewCommentedAdapter.tv_headline_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }
                    }
                });


                // Review image loading and display setting

                if (gettingTimelineAllPojos_list.get(position).getReviewInp() != null) {

                    displayReviewItem((ArrayList<ReviewInputAllPojo>) gettingTimelineAllPojos_list.get(position).getReviewInp(), timelineAdapeterHolder, position);

                }


                break;

            case 8:


                // Asked a question
                final AnswerAdapter answerAdapter = (AnswerAdapter) timelineAdapeterHolder;


                String questionUserName = gettingTimelineAllPojos_list.get(position).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getLastName();
                String questionheadline = "asked a question";

                answerAdapter.tv_headline_username.setText(questionUserName);
                answerAdapter.tv_question_headlinetext.setText(questionheadline);

                // Headline username on click listener

                answerAdapter.tv_headline_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }
                    }
                });

                // Answer image loading and display setting
                if (gettingTimelineAllPojos_list.get(position).getQuestionInp() != null) {

                    displayQuestionItem(gettingTimelineAllPojos_list.get(position).getQuestionInp(), timelineAdapeterHolder, position);

                }
                break;

            case 9:

                //  Answer question
                final AnswerAdapter answerAdapter1 = (AnswerAdapter) timelineAdapeterHolder;
                String questionUserName1 = gettingTimelineAllPojos_list.get(position).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getLastName();
                String questionheadline1 = "answered a question";

                answerAdapter1.tv_headline_username.setText(questionUserName1);
                answerAdapter1.tv_question_headlinetext.setText(questionheadline1);
                // Headliene user name on click listener

                answerAdapter1.tv_headline_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }
                    }
                });

                // Answer display setting

                if (gettingTimelineAllPojos_list.get(position).getQuestionInp() != null) {

                    displayQuestionItem(gettingTimelineAllPojos_list.get(position).getQuestionInp(), timelineAdapeterHolder, position);
                }
                break;

            case 10:

                // Upvote for Q&A

                final AnswerAdapter answerAdapter2 = (AnswerAdapter) timelineAdapeterHolder;

                String questionUserName2 = gettingTimelineAllPojos_list.get(position).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getLastName();
                String questionheadline2 = "upvoted an answer";

                answerAdapter2.tv_headline_username.setText(questionUserName2);
                answerAdapter2.tv_question_headlinetext.setText(questionheadline2);

                // Headline user name on click listener

                answerAdapter2.tv_headline_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }
                    }
                });


                // Displaying question loading and displaying

                if (gettingTimelineAllPojos_list.get(position).getQuestionInp() != null) {

                    displayQuestionItem(gettingTimelineAllPojos_list.get(position).getQuestionInp(), timelineAdapeterHolder, position);

                }
                answerAdapter2.img_sharechat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent1 = new Intent(context, ChatActivity.class);
                        intent1.putExtra("redirect_url", gettingTimelineAllPojos_list.get(position).getRedirect_url());
                        context.startActivity(intent1);
                    }
                });

                break;


            case 11:


                final PollAdapter pollAdapter = (PollAdapter) timelineAdapeterHolder;

                String questionUserName3 = gettingTimelineAllPojos_list.get(position).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getLastName();
                String questionheadline3 = "asked a poll";

                pollAdapter.tv_headline_username.setText(questionUserName3);
                pollAdapter.tv_question_headlinetext.setText(questionheadline3);
                // Headline username on click listener

                pollAdapter.tv_headline_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }
                    }
                });


                // User image loader

                GlideApp.with(context)
                        .load(Pref_storage.getDetail(context, "picture_user_login"))
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .placeholder(R.drawable.ic_add_photo)
                        .into(pollAdapter.userphoto_comment);


                // Comments count displaying
                int totalComments = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getTotalComments());

                if (totalComments == 0) {

                    pollAdapter.tv_view_all_comments.setVisibility(GONE);

                } else if (totalComments == 1) {

                    pollAdapter.tv_view_all_comments.setVisibility(VISIBLE);
                    String viewTxt = "View " + totalComments + " Comment";
                    pollAdapter.tv_view_all_comments.setText(viewTxt);

                } else if (totalComments > 1) {

                    pollAdapter.tv_view_all_comments.setVisibility(VISIBLE);
                    String viewTxt = "View all " + totalComments + " Comments";
                    pollAdapter.tv_view_all_comments.setText(viewTxt);


                }

                // Text View all commemt intent
                pollAdapter.tv_view_all_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ViewPollComments.class);

                        intent.putExtra("comment_quesId", String.valueOf(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getQuestId()));
                        intent.putExtra("comment_question", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getAskQuestion());
                        intent.putExtra("comment_picture", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPicture());
                        intent.putExtra("comment_firstname", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getFirstName() + " " + gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getLastName());
                        intent.putExtra("comment_createdon", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getCreatedOn());
                        intent.putExtra("comment_pollid", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPollId());
                        intent.putExtra("pollsize", String.valueOf(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().size()));

                        try {

                            double userVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            intent.putExtra("userVotes", String.valueOf(userVotesTwo));

                            if (gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().size() == 2) {
                                double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();
                                double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();

                                intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                intent.putExtra("comment_poll1", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getPollList());
                                intent.putExtra("comment_poll2", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getPollList());

                            } else if (gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().size() == 3) {

                                double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();
                                double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();
                                double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();

                                intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                intent.putExtra("totalVotesThree", String.valueOf(totalVotesThree));
                                intent.putExtra("comment_poll1", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getPollList());
                                intent.putExtra("comment_poll2", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getPollList());
                                intent.putExtra("comment_poll3", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getPollList());
                            } else if (gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().size() == 4) {


                                double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();
                                double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();
                                double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();
                                double totalVotesFour = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getTotalPoll();

                                intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                intent.putExtra("totalVotesThree", String.valueOf(totalVotesThree));
                                intent.putExtra("totalVotesFour", String.valueOf(totalVotesFour));

                                intent.putExtra("comment_poll1", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getPollList());
                                intent.putExtra("comment_poll2", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getPollList());
                                intent.putExtra("comment_poll3", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getPollList());
                                intent.putExtra("comment_poll4", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getPollList());
                            }

                            intent.putExtra("comment_votecount", gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser() + " votes ");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        context.startActivity(intent);
                    }
                });

                // Edit text listener for comment posting
                pollAdapter.edittxt_comment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        if (pollAdapter.edittxt_comment.getText().toString().trim().length() != 0) {
                            pollAdapter.txt_adapter_post.setVisibility(VISIBLE);
                        } else {
                            pollAdapter.txt_adapter_post.setVisibility(GONE);
                        }


                    }

                });


                //find poll size
                final int pollSize = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().size();


                if (gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser() == 0) {

                    pollAdapter.total_poll_votes.setText("No votes");

                } else if (gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser() == 1) {

                    pollAdapter.total_poll_votes.setText(String.valueOf(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser()) + " Vote");

                } else {

                    pollAdapter.total_poll_votes.setText(String.valueOf(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser()) + " Votes");

                }


                if (Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPollId()) != 0) {

                    pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                    pollAdapter.ll_answer_section.setVisibility(View.GONE);
                    pollAdapter.tv_poll_undo_timeline.setVisibility(View.VISIBLE);


                    if (pollSize > 1) {

                        // Poll option one

                        pollAdapter.poll_answer_one.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getPollList());

                        double userVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                        double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();


                        Log.e("VotesOne", "userVotesOne->" + (double) userVotesOne);
                        Log.e("VotesOne", "totalVotesOne->" + (double) totalVotesOne);
                        Log.e("VotesOne", "totalVotesOne->" + (double) totalVotesOne / userVotesOne);


                        double percentOne;

                        if (userVotesOne == 0 || totalVotesOne == 0) {

                            percentOne = 0;

                        } else {

                            percentOne = (totalVotesOne / userVotesOne) * 100;

                        }


                        if (gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPollId() == gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getPollId()) {

                            pollAdapter.poll_one_checked.setVisibility(View.VISIBLE);

                        } else {

                            pollAdapter.poll_one_checked.setVisibility(View.GONE);

                        }


                        pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                        pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");


                        // Poll option two
                        pollAdapter.poll_answer_two.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getPollList());

                        double userVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                        double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();


                        Log.e("VotesOne", "userVotesTwo->" + (double) userVotesTwo);
                        Log.e("VotesOne", "totalVotesTwo->" + (double) totalVotesTwo);
                        Log.e("VotesOne", "totalVotesTwo->" + (double) totalVotesTwo / userVotesTwo);


                        double percentTwo;


                        if (userVotesTwo == 0 || totalVotesTwo == 0) {

                            percentTwo = 0;

                        } else {

                            percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                        }

                        if (gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPollId() == gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getPollId()) {

                            pollAdapter.poll_two_checked.setVisibility(View.VISIBLE);

                        } else {

                            pollAdapter.poll_two_checked.setVisibility(View.GONE);


                        }

                        pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                        pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                        // Poll option three

                        if (pollSize == 3) {

                            pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                            pollAdapter.poll_answer_third.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getPollList());

                            double userVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();

                            Log.e("VotesOne", "userVotesThree->" + (double) userVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree / userVotesThree);


                            double percentThree;

                            if (userVotesThree == 0 || totalVotesThree == 0) {

                                percentThree = 0;

                            } else {

                                percentThree = (totalVotesThree / userVotesThree) * 100;

                            }


                            if (gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPollId() == gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getPollId()) {

                                pollAdapter.poll_three_checked.setVisibility(View.VISIBLE);

                            } else {

                                pollAdapter.poll_three_checked.setVisibility(View.GONE);


                            }

                            pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                            pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            // Poll option four

                        } else if (pollSize == 4) {


                            // poll option 3

                            pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                            pollAdapter.poll_answer_third.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getPollList());


                            double userVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();


                            Log.e("VotesOne", "userVotesThree->" + (double) userVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree);
                            Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree / userVotesThree);


                            double percentThree;

                            if (userVotesThree == 0 || totalVotesThree == 0) {

                                percentThree = 0;

                            } else {

                                percentThree = (totalVotesThree / userVotesThree) * 100;

                            }

                            if (gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPollId() == gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getPollId()) {

                                pollAdapter.poll_three_checked.setVisibility(View.VISIBLE);

                            } else {

                                pollAdapter.poll_three_checked.setVisibility(View.GONE);


                            }

                            pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                            pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            // poll option 4

                            pollAdapter.ll_poll_progress_four.setVisibility(View.VISIBLE);
                            pollAdapter.poll_answer_four.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getPollList());


                            double userVotesFour = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesFour = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getTotalPoll();

                            Log.e("VotesOne", "userVotesFour->" + (double) userVotesFour);
                            Log.e("VotesOne", "totalVotesFour->" + (double) totalVotesFour);
                            Log.e("VotesOne", "totalVotesFour->" + (double) totalVotesFour / userVotesFour);


                            double percentFour;

                            if (userVotesFour == 0 || totalVotesFour == 0) {

                                percentFour = 0;

                            } else {

                                percentFour = (totalVotesFour / userVotesFour) * 100;

                            }

                            if (gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPollId() == gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getPollId()) {

                                pollAdapter.poll_four_checked.setVisibility(View.VISIBLE);

                            } else {

                                pollAdapter.poll_four_checked.setVisibility(View.GONE);


                            }

                            pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                            pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");

                        }

                    } else {

                        pollAdapter.ll_poll_progress_one.setVisibility(View.GONE);
                        pollAdapter.ll_poll_progress_two.setVisibility(View.GONE);

                    }


                }


                Log.e("pollSize", "pollSize->" + pollSize);

                if (pollSize > 1) {

                    pollAdapter.tv_poll_answer_one.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getPollList());
                    pollAdapter.tv_poll_answer_two.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getPollList());

                    pollAdapter.poll_answer_one.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getPollList());
                    pollAdapter.poll_answer_two.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getPollList());


                    if (pollSize == 3) {


                        pollAdapter.ll_poll_answer_third.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_answer_third.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getPollList());

                        pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                        pollAdapter.poll_answer_third.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getPollList());


                    } else if (pollSize == 4) {

                        pollAdapter.ll_poll_answer_third.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_answer_third.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getPollList());

                        pollAdapter.ll_poll_answer_four.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_answer_four.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getPollList());

                        pollAdapter.ll_poll_progress_third.setVisibility(View.VISIBLE);
                        pollAdapter.poll_answer_third.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getPollList());

                        pollAdapter.ll_poll_progress_four.setVisibility(View.VISIBLE);
                        pollAdapter.poll_answer_four.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getPollList());

                    }

                } else {
                    pollAdapter.ll_poll_answer_one.setVisibility(GONE);
                    pollAdapter.ll_poll_answer_two.setVisibility(GONE);


                }

                // Poll answer one click listener


                pollAdapter.ll_poll_answer_one.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getQuestId());
                        int pollId = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getPollId());

                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_one_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo_timeline.setVisibility(View.VISIBLE);


                        int totalVotes = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }


                        if (pollSize > 1) {

                            // Option One

                            double userVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll() + 1;

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }

                            Log.e("VotesOne", "userVotesOne->" + (double) userVotesOne);
                            Log.e("VotesOne", "totalVotesOne->" + (double) totalVotesOne);
                            Log.e("VotesOne", "percentOne->" + percentOne);


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two

                            double userVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                        }
                        if (pollSize == 3) {
                            // Option One

                            double userVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll() + 1;

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }

                            Log.e("VotesOne", "userVotesOne->" + (double) userVotesOne);
                            Log.e("VotesOne", "totalVotesOne->" + (double) totalVotesOne);
                            Log.e("VotesOne", "percentOne->" + percentOne);


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two

                            double userVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                            // Option Three
                            double userVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();
                            double percentThree;

                            if (userVotesThree == 0 || totalVotesThree == 0) {

                                percentThree = 0;

                            } else {

                                percentThree = (totalVotesThree / userVotesThree) * 100;

                            }

                            pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                            pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                        } else if (pollSize == 4) {

                            // Option One

                            double userVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll() + 1;

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }

                            Log.e("VotesOne", "userVotesOne->" + (double) userVotesOne);
                            Log.e("VotesOne", "totalVotesOne->" + (double) totalVotesOne);
                            Log.e("VotesOne", "percentOne->" + percentOne);


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two

                            double userVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");


                            // Option Three
                            double userVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();

                            double percentThree;

                            if (userVotesThree == 0 || totalVotesThree == 0) {

                                percentThree = 0;

                            } else {

                                percentThree = (totalVotesThree / userVotesThree) * 100;

                            }

                            pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                            pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            // Option Four
                            double userVotesFour = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesFour = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getTotalPoll();

                            double percentFour;

                            if (userVotesFour == 0 || totalVotesFour == 0) {

                                percentFour = 0;

                            } else {

                                percentFour = (totalVotesFour / userVotesFour) * 100;

                            }

                            pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                            pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                        }



                        /* Create API call for Poll */

                        createPoll(quesId, pollId);
//                        getAllQuesData();


                    }
                });
                // Poll answer two click listener

                pollAdapter.ll_poll_answer_two.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getQuestId());
                        int pollId = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getPollId());
                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_two_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo_timeline.setVisibility(View.VISIBLE);


                        int totalVotes = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }

                        if (pollSize > 1) {

                            // Option One

                            double userVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two

                            double userVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll() + 1;

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            Log.e("VotesOne", "userVotesTwo->" + (double) userVotesTwo);
                            Log.e("VotesOne", "totalVotesTwo->" + (double) totalVotesTwo);
                            Log.e("VotesOne", "percentTwo->" + percentTwo);


                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                        }

                        if (pollSize == 3) {

                            // Option One

                            double userVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two

                            double userVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll() + 1;

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            Log.e("VotesOne", "userVotesTwo->" + (double) userVotesTwo);
                            Log.e("VotesOne", "totalVotesTwo->" + (double) totalVotesTwo);
                            Log.e("VotesOne", "percentTwo->" + percentTwo);


                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");
                            // Option Three
                            double userVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();

                            double percentThree;

                            if (userVotesThree == 0 || totalVotesThree == 0) {

                                percentThree = 0;

                            } else {

                                percentThree = (totalVotesThree / userVotesThree) * 100;

                            }

                            pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                            pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                        } else if (pollSize == 4) {

                            // Option One
                            double userVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two

                            double userVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll() + 1;

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            Log.e("VotesOne", "userVotesTwo->" + (double) userVotesTwo);
                            Log.e("VotesOne", "totalVotesTwo->" + (double) totalVotesTwo);
                            Log.e("VotesOne", "percentTwo->" + percentTwo);


                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");
                            // Option Three
                            double userVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();

                            double percentThree;

                            if (userVotesThree == 0 || totalVotesThree == 0) {

                                percentThree = 0;

                            } else {

                                percentThree = (totalVotesThree / userVotesThree) * 100;

                            }

                            pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                            pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            // Option Four
                            double userVotesFour = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesFour = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getTotalPoll();

                            double percentFour;

                            if (userVotesFour == 0 || totalVotesFour == 0) {

                                percentFour = 0;

                            } else {

                                percentFour = (totalVotesFour / userVotesFour) * 100;

                            }

                            pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                            pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                        }


                        createPoll(quesId, pollId);
//                        getAllQuesData();


                    }
                });
                // Poll answer third click listener

                pollAdapter.ll_poll_answer_third.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getQuestId());
                        int pollId = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getPollId());
                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_three_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo_timeline.setVisibility(View.VISIBLE);


                        int totalVotes = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }


                        if (pollSize != 0) {

                            // Option One
                            double userVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two
                            double userVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                            if (pollSize == 3) {


                                // Option Three
                                double userVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll() + 1;

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                Log.e("VotesOne", "userVotesThree->" + (double) userVotesThree);
                                Log.e("VotesOne", "totalVotesThree->" + (double) totalVotesThree);
                                Log.e("VotesOne", "percentThree->" + percentThree);


                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            } else if (pollSize == 4) {


                                // Option Three
                                double userVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll() + 1;

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                                // Option Four
                                double userVotesFour = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                                double totalVotesFour = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getTotalPoll();
                                double percentFour;

                                if (userVotesFour == 0 || totalVotesFour == 0) {

                                    percentFour = 0;

                                } else {

                                    percentFour = (totalVotesFour / userVotesFour) * 100;

                                }

                                pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                                pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                            }

                        }

                        // Create poll API call
                        createPoll(quesId, pollId);
//                        getAllQuesData();


                    }
                });

                // Poll answer four click listener

                pollAdapter.ll_poll_answer_four.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int quesId = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getQuestId());
                        int pollId = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getPollId());
                        pollAdapter.ll_progress_section.setVisibility(View.VISIBLE);
                        pollAdapter.ll_answer_section.setVisibility(View.GONE);
                        pollAdapter.poll_four_checked.setVisibility(View.VISIBLE);
                        pollAdapter.tv_poll_undo_timeline.setVisibility(View.VISIBLE);


                        Log.e(TAG, "onClick: " + gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().size());


                        int totalVotes = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();

                        if (totalVotes == 0) {

                            totalVotes = totalVotes + 1;
                            gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Vote");

                        } else {

                            totalVotes = totalVotes + 1;
                            gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).setTotalPollUser(totalVotes);
                            pollAdapter.total_poll_votes.setText(String.valueOf(totalVotes) + " Votes");

                        }


                        if (pollSize != 0) {

                            // Option One
                            double userVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesOne = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();

                            double percentOne;

                            if (userVotesOne == 0 || totalVotesOne == 0) {

                                percentOne = 0;

                            } else {

                                percentOne = (totalVotesOne / userVotesOne) * 100;

                            }


                            pollAdapter.poll_answer_one_progress.setProgress((int) percentOne);
                            pollAdapter.poll_answer_one_percent.setText((int) percentOne + " %");

                            // Option Two
                            double userVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                            double totalVotesTwo = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();

                            double percentTwo;

                            if (userVotesTwo == 0 || totalVotesTwo == 0) {

                                percentTwo = 0;

                            } else {

                                percentTwo = (totalVotesTwo / userVotesTwo) * 100;

                            }

                            pollAdapter.poll_answer_two_progress.setProgress((int) percentTwo);
                            pollAdapter.poll_answer_two_percent.setText((int) percentTwo + " %");

                            if (pollSize == 3) {


                                // Option Three
                                double userVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                            } else if (pollSize == 4) {


                                // Option Three
                                double userVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                                double totalVotesThree = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();

                                double percentThree;

                                if (userVotesThree == 0 || totalVotesThree == 0) {

                                    percentThree = 0;

                                } else {

                                    percentThree = (totalVotesThree / userVotesThree) * 100;

                                }

                                pollAdapter.poll_answer_third_progress.setProgress((int) percentThree);
                                pollAdapter.poll_answer_third_percent.setText((int) percentThree + " %");


                                // Option Four
                                double userVotesFour = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                                double totalVotesFour = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPoll().get(3).getTotalPoll() + 1;

                                double percentFour;

                                if (userVotesFour == 0 || totalVotesFour == 0) {

                                    percentFour = 0;

                                } else {

                                    percentFour = (totalVotesFour / userVotesFour) * 100;

                                }

                                Log.e("VotesOne", "userVotesFour->" + (double) userVotesFour);
                                Log.e("VotesOne", "totalVotesFour->" + (double) totalVotesFour);
                                Log.e("VotesOne", "percentFour->" + percentFour);


                                pollAdapter.poll_answer_four_progress.setProgress((int) percentFour);
                                pollAdapter.poll_answer_four_percent.setText((int) percentFour + " %");


                            }

                        }

                        // Create poll API call
                        createPoll(quesId, pollId);
//                        getAllQuesData();

                    }
                });


                String firstName = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getFirstName();
                String lastName = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getLastName();

                pollAdapter.userName.setText(firstName + " " + lastName);

                String picture = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getPicture();


                // User image  loading and displaying

                GlideApp.with(context)
                        .load(picture)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .placeholder(R.drawable.ic_add_photo)
                        .into(pollAdapter.userimage);

                // Username on click listener

                pollAdapter.userName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }

                    }
                });

                // User image on click listener

                pollAdapter.userimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String userId = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getUserId();

                        if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", userId);
                            context.startActivity(intent);

                        }

                    }
                });


                String pollCreatedon = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getCreatedOn();


                int totalFollowers = gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getTotalFollowers();

                String follower = "";

                switch (totalFollowers) {

                    case 0:

                        pollAdapter.follow_count.setVisibility(View.GONE);

                        break;

                    case 1:

                        follower = String.valueOf(totalFollowers) + " Follower";

                        break;

                    default:

                        follower = String.valueOf(totalFollowers) + " Followers";

                        break;

                }

                pollAdapter.follow_count.setText(follower);

                Utility.setTimeStamp(pollCreatedon, pollAdapter.answeredOn);


                pollAdapter.question.setText(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getAskQuestion());

                // Answer option pop up params in click listener

                pollAdapter.ans_options.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int questId = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getQuestId());
                        int createdBy = Integer.parseInt(gettingTimelineAllPojos_list.get(position).getQuestionInp().get(0).getCreatedBy());

                        Pref_storage.setDetail(context, "clicked_quesID", String.valueOf(questId));
                        Pref_storage.setDetail(context, "clicked_quesCreatedBy", String.valueOf(createdBy));


                        CustomDialogClass ccd = new CustomDialogClass(context, String.valueOf(questId), String.valueOf(createdBy), position);
                        ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        ccd.setCancelable(true);
                        ccd.show();


                    }
                });


                break;

            case 12:


                final Paginat_adapter paginatAdapter = (Paginat_adapter) timelineAdapeterHolder;
                paginatAdapter.pagination_loader.setIndeterminate(true);
                paginatAdapter.pagination_loader.setVisibility(GONE);
                break;


        }


    }

    private int getItem(int i) {
        return timelineAdapter.mviewpager.getCurrentItem() + i;
    }


    @Override
    public int getItemCount() {

        Log.e(TAG, "getItemCount: " + gettingTimelineAllPojos_list.size());

        return gettingTimelineAllPojos_list.size();

    }


    // Displayng question items
    public void displayQuestionItem(final List<QuestionAnswerPojo> questionAnswerPojoList, RecyclerView.ViewHolder holder, final int position) {


        final AnswerAdapter answerAdapter = (AnswerAdapter) holder;


        answerAdapter.question.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(questionAnswerPojoList.get(0).getAskQuestion()));
        String username = questionAnswerPojoList.get(0).getFirstName() + " " + questionAnswerPojoList.get(0).getLastName();
        answerAdapter.username.setText(username);


        // Image loading

        GlideApp.with(context)
                .load(questionAnswerPojoList.get(0).getPicture())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)
                .into(answerAdapter.userimage);
        // User name on click listener

        answerAdapter.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userId = questionAnswerPojoList.get(0).getUserId();

                if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", userId);
                    context.startActivity(intent);

                }

            }
        });

        int followCount = questionAnswerPojoList.get(0).getTotalQuestFollow();


        if (questionAnswerPojoList.get(0).getQuesFollow() != 0) {

            answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_followed_answer));
            String followText = "Follow - " + String.valueOf(followCount);
            answerAdapter.tv_follow_count.setText(followText);
            answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.blue));
            questionAnswerPojoList.get(0).setFollowing(true);


        } else {


            if (followCount != 0) {

                String followText = "Follow - " + String.valueOf(followCount);
                answerAdapter.tv_follow_count.setText(followText);

            } else {

                answerAdapter.tv_follow_count.setText("Follow");
            }

            answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_follow_answer));
            answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.black));
            questionAnswerPojoList.get(0).setFollowing(false);

        }

        // Follow answer on click listener

        answerAdapter.ll_answer_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!questionAnswerPojoList.get(0).isFollowing()) {


                    answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_followed_answer));
                    answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.blue));
                    questionAnswerPojoList.get(0).setFollowing(true);
                    int count = questionAnswerPojoList.get(0).getTotalQuestFollow();
                    String followText = "Follow - " + String.valueOf(count + 1);
                    answerAdapter.tv_follow_count.setText(followText);
                    questionAnswerPojoList.get(0).setTotalQuestFollow(count + 1);

                    // Calling API for answer follow

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<CommonOutputPojo> call = apiService.createQuesFollow("create_delete_question_follower",
                                Integer.parseInt(questionAnswerPojoList.get(0).getQuestId()),
                                Integer.parseInt(questionAnswerPojoList.get(0).getCreatedBy()),
                                1, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @SuppressLint("ResourceAsColor")
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        //Success


                                    } else {


                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();
                                }


                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }


                } else if (questionAnswerPojoList.get(0).isFollowing()) {

                    answerAdapter.img_btn_follow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_follow_answer));
                    answerAdapter.tv_follow_count.setTextColor(ContextCompat.getColor(context, R.color.black));
                    questionAnswerPojoList.get(0).setFollowing(false);
                    int count = questionAnswerPojoList.get(0).getTotalQuestFollow();
                    questionAnswerPojoList.get(0).setTotalQuestFollow(count - 1);


                    if (questionAnswerPojoList.get(0).getTotalQuestFollow() != 0) {

                        String followText = "Follow - " + String.valueOf(count - 1);
                        answerAdapter.tv_follow_count.setText(followText);


                    } else {

                        String followText = "Follow";
                        answerAdapter.tv_follow_count.setText(followText);
                    }

                    // API call unfollow answer
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                        Call<CommonOutputPojo> call = apiService.createQuesFollow("create_delete_question_follower",
                                Integer.parseInt(questionAnswerPojoList.get(0).getQuestId()),
                                Integer.parseInt(questionAnswerPojoList.get(0).getCreatedBy()), 0,
                                userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @SuppressLint("ResourceAsColor")
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        //Success


                                    } else {


                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                }


                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }


                }
            }
        });
        // User image on click listener

        answerAdapter.userimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String userId = gettingTimelineAllPojos_list.get(position).getUserId();

                if (userId.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", userId);
                    context.startActivity(intent);

                }

            }
        });

        int total_followers = questionAnswerPojoList.get(0).getTotalFollowers();

        String followers = "";

        switch (total_followers) {

            case 0:

                answerAdapter.no_of_followers.setVisibility(View.GONE);

                break;

            case 1:

                followers = String.valueOf(total_followers) + " Follower";

                break;

            default:

                followers = String.valueOf(total_followers) + " Followers";

                break;

        }

        answerAdapter.no_of_followers.setText(followers);

        String createdOn = questionAnswerPojoList.get(0).getCreatedOn();

        Utility.setTimeStamp(createdOn, answerAdapter.answeredOn);


        // Intent to view answer on click listener

        answerAdapter.viewQues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(context, ViewQuestionAnswer.class);
                intent.putExtra("position", String.valueOf(position));
                intent.putExtra("quesId", questionAnswerPojoList.get(0).getQuestId());
                intent.putExtra("firstname", questionAnswerPojoList.get(0).getFirstName() + " " + questionAnswerPojoList.get(0).getLastName());
                intent.putExtra("createdon", questionAnswerPojoList.get(0).getCreatedOn());
                intent.putExtra("profile", questionAnswerPojoList.get(0).getPicture());
                context.startActivity(intent);

            }
        });

        // Pop for answer options
        answerAdapter.ans_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int questId = Integer.parseInt(questionAnswerPojoList.get(0).getQuestId());
                int userid = Integer.parseInt(questionAnswerPojoList.get(0).getCreatedBy());
                int quesType = Integer.parseInt(questionAnswerPojoList.get(0).getQuesType());
                String question = gettingTimelineAllPojos_list.get(position).getAskQuestion();

                CustomDialogClass ccd = new CustomDialogClass(context, String.valueOf(questId), String.valueOf(userid), position);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccd.setCancelable(true);
                ccd.show();

            }
        });


        // Answer Parts


        if (Integer.parseInt(questionAnswerPojoList.get(0).getTotalAnswers()) == 0) {


            answerAdapter.answer.setText("No answers yet");
            answerAdapter.answer.setTypeface(Typeface.DEFAULT_BOLD);


        } else {


            if (questionAnswerPojoList.get(0).getAnswer().get(0) != null) {

                answerAdapter.answer.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(questionAnswerPojoList.get(0).getAnswer().get(0).getAskAnswer()));
//                        answerAdapter.answer.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));

            }


        }
    }


    // Display review items for the timeline feeds


    public void displayReviewItem(final ArrayList<ReviewInputAllPojo> reviewList, RecyclerView.ViewHolder holder, final int position) {

        final Review_created_adapter reviewCreatedAdapter = (Review_created_adapter) holder;

        if (reviewList.size() > 0) {

            reviewCreatedAdapter.ll_review_main.setVisibility(VISIBLE);
            //reviewer_name
            String reviewerFname = reviewList.get(0).getFirstName();
            String reviewerLname = reviewList.get(0).getLastName();
            reviewCreatedAdapter.username.setText(reviewerFname);
            reviewCreatedAdapter.userlastname.setText(reviewerLname);
            reviewCreatedAdapter.username.setTextColor(Color.parseColor("#000000"));
            reviewCreatedAdapter.userlastname.setTextColor(Color.parseColor("#000000"));

            if (reviewList.get(0).getCategoryType() != null) {

                if (reviewList.get(0).getCategoryType().equalsIgnoreCase("2")) {

                    reviewCreatedAdapter.title_ambience.setText("Packaging");

                } else {

                    reviewCreatedAdapter.title_ambience.setText("Hotel Ambience");

                }
            }

            // Bucket list visibility

            if (reviewList.get(0).getBucket_list() != 0) {
                reviewCreatedAdapter.img_bucket_list.setImageResource(R.drawable.ic_bucket_neww);
            } else {
                reviewCreatedAdapter.img_bucket_list.setImageResource(R.drawable.ic_bucket);
            }

            // txt_hotelname

            reviewCreatedAdapter.tv_restaurant_name.setText(reviewList.get(0).getHotelName());

            // Mode of delivery text

            if (reviewList.get(0).getMode_id().equalsIgnoreCase("0")) {

                reviewCreatedAdapter.txt_mode_delivery.setVisibility(GONE);
//            reviewCreatedAdapter.ll_delivery_mode.setVisibility(GONE);

            } else {

                if (reviewList.get(0).getMode_id().equalsIgnoreCase("1")) {
                    reviewCreatedAdapter.txt_mode_delivery.setText(reviewList.get(0).getComp_name());
                    reviewCreatedAdapter.txt_mode_delivery.setTextColor(Color.parseColor("#f25353"));

                } else if (reviewList.get(0).getMode_id().equalsIgnoreCase("2")) {
                    reviewCreatedAdapter.txt_mode_delivery.setText(reviewList.get(0).getComp_name().replace("swiggy", "Swiggy"));
                    reviewCreatedAdapter.txt_mode_delivery.setTextColor(Color.parseColor("#ff8008"));

                } else if (reviewList.get(0).getMode_id().equalsIgnoreCase("3")) {
                    reviewCreatedAdapter.txt_mode_delivery.setText(reviewList.get(0).getComp_name().replace("Food panda", "Foodpanda"));
                    reviewCreatedAdapter.txt_mode_delivery.setTextColor(Color.parseColor("#ffff8800"));

                } else if (reviewList.get(0).getMode_id().equalsIgnoreCase("4")) {
                    reviewCreatedAdapter.txt_mode_delivery.setText(reviewList.get(0).getComp_name());
                    reviewCreatedAdapter.txt_mode_delivery.setTextColor(Color.parseColor("#2ecc71"));

                }


            }

            // Delivery mode visiblity

            if (reviewList.get(0).getCategoryType().equalsIgnoreCase("2")) {

                reviewCreatedAdapter.ll_delivery_mode.setVisibility(VISIBLE);

            } else {

                reviewCreatedAdapter.ll_delivery_mode.setVisibility(GONE);

            }

            // Reviewer Image
            Utility.picassoImageLoader(reviewList.get(0).getPicture(),
                    1,
                    reviewCreatedAdapter.userphoto_profile, context);


            // User Image
//
            Utility.picassoImageLoader(Pref_storage.getDetail(context, "picture_user_login"),
                    1,
                    reviewCreatedAdapter.review_user_photo, context);


            // Timestamp for review

            String createdOn = reviewList.get(0).getCreatedOn();

            Utility.setTimeStamp(createdOn, reviewCreatedAdapter.txt_created_on);


            // Display top dishes

            if (reviewList.get(0).getTopdish() == null || reviewList.get(0).getTopdish().trim().equalsIgnoreCase("")) {

                reviewCreatedAdapter.tv_top_dishes.setVisibility(GONE);
                reviewCreatedAdapter.rl_top_dish.setVisibility(GONE);
                reviewCreatedAdapter.ll_top_dish.setVisibility(GONE);

            } else {

                //  reviewCreatedAdapter.tv_top_dishes.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(reviewList.get(0).getTopdish().replace(",", ", ")));
                reviewCreatedAdapter.tv_top_dishes.setVisibility(GONE);

            }


            // Display avoid dishes

            if (reviewList.get(0).getAvoiddish() == null || reviewList.get(0).getAvoiddish().trim().equalsIgnoreCase("")) {
                reviewCreatedAdapter.tv_dishes_to_avoid.setVisibility(GONE);
                reviewCreatedAdapter.rl_avoid_dish.setVisibility(GONE);
                reviewCreatedAdapter.ll_dish_to_avoid.setVisibility(GONE);
            } else {

                // reviewCreatedAdapter.tv_dishes_to_avoid.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(reviewList.get(0).getAvoiddish().replace(",", ", ")));
                reviewCreatedAdapter.tv_dishes_to_avoid.setVisibility(GONE);

            }


            if (reviewList.get(0).getTopCount() == 0) {

                reviewCreatedAdapter.tv_top_dishes.setVisibility(GONE);
                reviewCreatedAdapter.rl_top_dish.setVisibility(GONE);
                reviewCreatedAdapter.ll_top_dish.setVisibility(GONE);

            }


            // Display avoid dishes

            if (reviewList.get(0).getAvoidCount() == 0) {
                reviewCreatedAdapter.tv_dishes_to_avoid.setVisibility(GONE);
                reviewCreatedAdapter.rl_avoid_dish.setVisibility(GONE);
                reviewCreatedAdapter.ll_dish_to_avoid.setVisibility(GONE);
            }


            // Edit text comment
            reviewCreatedAdapter.review_comment.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (reviewCreatedAdapter.review_comment.getText().toString().length() != 0) {
                        reviewCreatedAdapter.txt_adapter_post.setVisibility(VISIBLE);
                    } else {
                        reviewCreatedAdapter.txt_adapter_post.setVisibility(GONE);
                    }


                }
            });

            // Dishes to avoid text count
            int dish_avoid = reviewList.get(0).getAvoidCount();

            if (dish_avoid == 0) {
                reviewCreatedAdapter.ll_dish_to_avoid.setVisibility(View.GONE);
            }


            // Overallrating

            String rating = reviewList.get(0).getFoodExprience();

            reviewCreatedAdapter.tv_res_overall_rating.setText(rating);

            if (Integer.parseInt(rating) == 1) {

                reviewCreatedAdapter.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_red);
                reviewCreatedAdapter.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

            } else if (Integer.parseInt(rating) == 2) {

                reviewCreatedAdapter.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.ll_res_overall_rating.setBackgroundResource(R.drawable.review_rating_orange_bg);
                reviewCreatedAdapter.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_orange_star);

            } else if (Integer.parseInt(rating) == 3) {

                reviewCreatedAdapter.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                reviewCreatedAdapter.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));

            } else if (Integer.parseInt(rating) == 4) {
                reviewCreatedAdapter.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_star);
                reviewCreatedAdapter.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
            } else if (Integer.parseInt(rating) == 5) {
                reviewCreatedAdapter.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                reviewCreatedAdapter.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
            }


            try {

                // Time delivery

                if (Integer.parseInt(reviewList.get(0).getCategoryType()) == 2) {

                    String timedelivery = reviewList.get(0).getTotalTimedelivery();

                /*if(time_deleivery == null){

                    time_deleivery = "4";
                }*/

                    reviewCreatedAdapter.ambi_or_timedelivery.setText(R.string.Delivery_new);
                    reviewCreatedAdapter.tv_ambience_rating.setText(timedelivery);

                    if (Integer.parseInt(timedelivery) == 1) {
                        reviewCreatedAdapter.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_ambience_star.setImageResource(R.drawable.ic_review_rating_red);

                        reviewCreatedAdapter.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

                    } else if (Integer.parseInt(timedelivery) == 2) {
                        reviewCreatedAdapter.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_ambience_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                        reviewCreatedAdapter.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                    } else if (Integer.parseInt(timedelivery) == 3) {
                        reviewCreatedAdapter.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_ambience_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                        reviewCreatedAdapter.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                    } else if (Integer.parseInt(timedelivery) == 4) {
                        reviewCreatedAdapter.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_ambience_star.setImageResource(R.drawable.ic_review_rating_star);

                        reviewCreatedAdapter.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                    } else if (Integer.parseInt(timedelivery) == 5) {
                        reviewCreatedAdapter.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_ambience_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                        reviewCreatedAdapter.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                    }

                } else {

                    // Ambiance

                    String ambiance = reviewList.get(0).getAmbiance();

                    reviewCreatedAdapter.tv_ambience_rating.setText(ambiance);

                    if (Integer.parseInt(ambiance) == 1) {
                        reviewCreatedAdapter.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_ambience_star.setImageResource(R.drawable.ic_review_rating_red);

                        reviewCreatedAdapter.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

                    } else if (Integer.parseInt(ambiance) == 2) {
                        reviewCreatedAdapter.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_ambience_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                        reviewCreatedAdapter.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                    } else if (Integer.parseInt(ambiance) == 3) {
                        reviewCreatedAdapter.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_ambience_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                        reviewCreatedAdapter.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                    } else if (Integer.parseInt(ambiance) == 4) {
                        reviewCreatedAdapter.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_ambience_star.setImageResource(R.drawable.ic_review_rating_star);

                        reviewCreatedAdapter.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                    } else if (Integer.parseInt(ambiance) == 5) {
                        reviewCreatedAdapter.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_ambience_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                        reviewCreatedAdapter.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            // Taste

            String taste = reviewList.get(0).getTaste();

            reviewCreatedAdapter.txt_taste_count.setText(taste);

            if (Integer.parseInt(taste) == 1) {
                reviewCreatedAdapter.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_taste_star.setImageResource(R.drawable.ic_review_rating_red);
                reviewCreatedAdapter.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

            } else if (Integer.parseInt(taste) == 2) {
                reviewCreatedAdapter.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_taste_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                reviewCreatedAdapter.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
            } else if (Integer.parseInt(taste) == 3) {
                reviewCreatedAdapter.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_taste_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                reviewCreatedAdapter.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
            } else if (Integer.parseInt(taste) == 4) {
                reviewCreatedAdapter.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_taste_star.setImageResource(R.drawable.ic_review_rating_star);
                reviewCreatedAdapter.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
            } else if (Integer.parseInt(taste) == 5) {
                reviewCreatedAdapter.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_taste_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                reviewCreatedAdapter.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
            }


            // Value for money

            String vfm = reviewList.get(0).getValueMoney();

            reviewCreatedAdapter.tv_vfm_rating.setText(vfm);

            if (Integer.parseInt(vfm) == 1) {
                reviewCreatedAdapter.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_vfm_star.setImageResource(R.drawable.ic_review_rating_red);
                reviewCreatedAdapter.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
            } else if (Integer.parseInt(vfm) == 2) {
                reviewCreatedAdapter.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_vfm_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                reviewCreatedAdapter.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
            } else if (Integer.parseInt(vfm) == 3) {
                reviewCreatedAdapter.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_vfm_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                reviewCreatedAdapter.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
            } else if (Integer.parseInt(vfm) == 4) {
                reviewCreatedAdapter.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_vfm_star.setImageResource(R.drawable.ic_review_rating_star);
                reviewCreatedAdapter.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
            } else if (Integer.parseInt(vfm) == 5) {
                reviewCreatedAdapter.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                reviewCreatedAdapter.img_vfm_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                reviewCreatedAdapter.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
            }


            try {


                // Packaging

                if (Integer.parseInt(reviewList.get(0).getCategoryType()) == 2) {

                    String packageRating = reviewList.get(0).get_package();

                    reviewCreatedAdapter.tv_service_rating.setText(packageRating);
                    reviewCreatedAdapter.serv_or_package.setText(R.string.Package);

                    if (Integer.parseInt(packageRating) == 1) {
                        reviewCreatedAdapter.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_service_star.setImageResource(R.drawable.ic_review_rating_red);
                        reviewCreatedAdapter.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
                    } else if (Integer.parseInt(packageRating) == 2) {
                        reviewCreatedAdapter.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_service_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                        reviewCreatedAdapter.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                    } else if (Integer.parseInt(packageRating) == 3) {
                        reviewCreatedAdapter.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_service_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                        reviewCreatedAdapter.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                    } else if (Integer.parseInt(packageRating) == 4) {
                        reviewCreatedAdapter.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_service_star.setImageResource(R.drawable.ic_review_rating_star);

                        reviewCreatedAdapter.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                    } else if (Integer.parseInt(packageRating) == 5) {
                        reviewCreatedAdapter.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_service_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                        reviewCreatedAdapter.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                    }


                } else {

                    // Service

                    String service = reviewList.get(0).getService();

                    reviewCreatedAdapter.tv_service_rating.setText(service);
                    if (Integer.parseInt(service) == 1) {
                        reviewCreatedAdapter.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_service_star.setImageResource(R.drawable.ic_review_rating_red);
                        reviewCreatedAdapter.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
                    } else if (Integer.parseInt(service) == 2) {
                        reviewCreatedAdapter.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_service_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                        reviewCreatedAdapter.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                    } else if (Integer.parseInt(service) == 3) {
                        reviewCreatedAdapter.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_service_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                        reviewCreatedAdapter.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                    } else if (Integer.parseInt(service) == 4) {
                        reviewCreatedAdapter.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_service_star.setImageResource(R.drawable.ic_review_rating_star);

                        reviewCreatedAdapter.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                    } else if (Integer.parseInt(service) == 5) {
                        reviewCreatedAdapter.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                        reviewCreatedAdapter.img_service_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                        reviewCreatedAdapter.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Review comment
            if (reviewList.get(0).getHotelReview().equalsIgnoreCase("0")) {
                reviewCreatedAdapter.reviewcmmnt.setVisibility(GONE);
            } else {

                reviewCreatedAdapter.reviewcmmnt.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(reviewList.get(0).getHotelReview()));


            }


            // Total likes
            final String total_likes = reviewList.get(0).getTotalLikes();

            if (Integer.parseInt(total_likes) == 0) {

                reviewCreatedAdapter.ll_likecomment.setVisibility(GONE);

            } else {

                reviewCreatedAdapter.ll_likecomment.setVisibility(VISIBLE);

            }

            int userliked = Integer.parseInt(reviewList.get(0).getHtlRevLikes());

            if (userliked == 0) {

                reviewList.get(0).setUserliked(false);
                reviewCreatedAdapter.img_btn_review_like.setVisibility(View.VISIBLE);
                reviewCreatedAdapter.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));

            } else if (userliked == 1) {

                reviewList.get(0).setUserliked(true);
                reviewCreatedAdapter.img_btn_review_like.setVisibility(View.VISIBLE);
                reviewCreatedAdapter.ll_likecomment.setVisibility(VISIBLE);
                reviewCreatedAdapter.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));

            }


            if (parseInt(total_likes) == 0 || total_likes.equalsIgnoreCase(null) || total_likes.equalsIgnoreCase("")) {

                reviewCreatedAdapter.tv_like_count.setVisibility(View.GONE);
            }
            if (parseInt(total_likes) == 1) {
                reviewCreatedAdapter.tv_like_count.setText(reviewList.get(0).getTotalLikes().concat(" ").concat("like"));
                reviewCreatedAdapter.tv_like_count.setVisibility(View.VISIBLE);


            } else if (parseInt(total_likes) > 1) {
                reviewCreatedAdapter.tv_like_count.setText(reviewList.get(0).getTotalLikes().concat(" ").concat("likes"));
                reviewCreatedAdapter.tv_like_count.setVisibility(View.VISIBLE);
            }

            // Total comments

            String total_comments = reviewList.get(0).getTotalComments();

            if (parseInt(total_comments) == 0 || total_likes.equalsIgnoreCase(null) || total_likes.equalsIgnoreCase("")) {

                reviewCreatedAdapter.tv_view_all_comments.setVisibility(View.GONE);

            }

            if (parseInt(total_comments) == 1) {

                reviewCreatedAdapter.tv_view_all_comments.setText(R.string.view_one_comment);
                reviewCreatedAdapter.tv_view_all_comments.setVisibility(View.VISIBLE);

            } else if (parseInt(total_comments) > 1) {

                reviewCreatedAdapter.tv_view_all_comments.setText("View all " + total_comments + " comments");
                reviewCreatedAdapter.tv_view_all_comments.setVisibility(View.VISIBLE);
            }


            // View all text comments on click listener
            reviewCreatedAdapter.tv_view_all_comments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, Reviews_comments_all_activity.class);
                    intent.putExtra("posttype", reviewList.get(0).getCategoryType());
                    intent.putExtra("hotelid", reviewList.get(0).getHotelId());
                    intent.putExtra("reviewid", reviewList.get(0).getRevratId());
                    intent.putExtra("userid", reviewList.get(0).getUserId());
                    intent.putExtra("hotelname", reviewList.get(0).getHotelName());
                    intent.putExtra("overallrating", reviewList.get(0).getFoodExprience());

                    if (reviewList.get(0).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("totaltimedelivery", reviewList.get(0).getTotalTimedelivery());

                    } else {
                        intent.putExtra("totaltimedelivery", reviewList.get(0).getAmbiance());

                    }
                    intent.putExtra("taste_count", reviewList.get(0).getTaste());
                    intent.putExtra("vfm_rating", reviewList.get(0).getValueMoney());
                    if (reviewList.get(0).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("package_rating", reviewList.get(0).get_package());

                    } else {
                        intent.putExtra("package_rating", reviewList.get(0).getService());

                    }
                    intent.putExtra("hotelname", reviewList.get(0).getHotelName());
                    intent.putExtra("reviewprofile", reviewList.get(0).getPicture());
                    intent.putExtra("createdon", reviewList.get(0).getCreatedOn());
                    intent.putExtra("firstname", reviewList.get(0).getFirstName());
                    intent.putExtra("lastname", reviewList.get(0).getLastName());
                    if (reviewList.get(0).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("title_ambience", "Packaging");
                    } else {
                        intent.putExtra("title_ambience", "Hotel Ambience");
                    }
                    if (reviewList.get(0).getBucket_list() != 0) {
                        intent.putExtra("addbucketlist", "yes");
                    } else {
                        intent.putExtra("addbucketlist", "no");
                    }
                    context.startActivity(intent);
                }
            });

            // View all text likes on click listener

            reviewCreatedAdapter.tv_like_count.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, ReviewsLikesAllActivity.class);

                    intent.putExtra("hotelid", reviewList.get(position).getHotelId());
                    intent.putExtra("reviewid", reviewList.get(position).getRevratId());
                    intent.putExtra("userid", reviewList.get(position).getUserId());
                    intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                    intent.putExtra("overallrating", reviewList.get(position).getFoodExprience());

                    if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("totaltimedelivery", reviewList.get(position).getTotalTimedelivery());

                    } else {
                        intent.putExtra("totaltimedelivery", reviewList.get(position).getAmbiance());

                    }
                    intent.putExtra("taste_count", reviewList.get(position).getTaste());
                    intent.putExtra("vfm_rating", reviewList.get(position).getValueMoney());
                    if (reviewList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("package_rating", reviewList.get(position).get_package());

                    } else {
                        intent.putExtra("package_rating", reviewList.get(position).getService());

                    }
                    intent.putExtra("hotelname", reviewList.get(position).getHotelName());
                    intent.putExtra("reviewprofile", reviewList.get(position).getPicture());
                    intent.putExtra("createdon", reviewList.get(position).getCreatedOn());
                    intent.putExtra("firstname", reviewList.get(position).getFirstName());
                    intent.putExtra("lastname", reviewList.get(position).getLastName());

                    context.startActivity(intent);
                }
            });

            // Review button on click listener

            reviewCreatedAdapter.img_btn_review_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(context, Reviews_comments_all_activity.class);

                    intent.putExtra("posttype", reviewList.get(0).getCategoryType());
                    intent.putExtra("hotelid", reviewList.get(0).getHotelId());
                    intent.putExtra("reviewid", reviewList.get(0).getRevratId());
                    intent.putExtra("userid", reviewList.get(0).getUserId());
                    intent.putExtra("hotelname", reviewList.get(0).getHotelName());
                    intent.putExtra("overallrating", reviewList.get(0).getFoodExprience());

                    if (reviewList.get(0).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("totaltimedelivery", reviewList.get(0).getTotalTimedelivery());

                    } else {
                        intent.putExtra("totaltimedelivery", reviewList.get(0).getAmbiance());

                    }
                    intent.putExtra("taste_count", reviewList.get(0).getTaste());
                    intent.putExtra("vfm_rating", reviewList.get(0).getValueMoney());
                    if (reviewList.get(0).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("package_rating", reviewList.get(0).get_package());
                    } else {
                        intent.putExtra("package_rating", reviewList.get(0).getService());
                    }
                    intent.putExtra("hotelname", reviewList.get(0).getHotelName());
                    intent.putExtra("reviewprofile", reviewList.get(0).getPicture());
                    intent.putExtra("createdon", reviewList.get(0).getCreatedOn());
                    intent.putExtra("firstname", reviewList.get(0).getFirstName());
                    intent.putExtra("lastname", reviewList.get(0).getLastName());
                    if (reviewList.get(0).getCategoryType().equalsIgnoreCase("2")) {
                        intent.putExtra("title_ambience", "Packaging");
                    } else {
                        intent.putExtra("title_ambience", "Hotel Ambience");
                    }

                    if (reviewList.get(0).getBucket_list() != 0) {
                        intent.putExtra("addbucketlist", "yes");
                    } else {
                        intent.putExtra("addbucketlist", "no");
                    }
                    context.startActivity(intent);

                }
            });

            // User name on click listener

            reviewCreatedAdapter.username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (reviewList.get(0).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", reviewList.get(0).getUserId());
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", reviewList.get(0).getUserId());
                        context.startActivity(intent);
                    }

                }
            });

            //  User last name on click listener

            reviewCreatedAdapter.userlastname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (reviewList.get(0).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", reviewList.get(0).getUserId());
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", reviewList.get(0).getUserId());
                        context.startActivity(intent);
                    }

                }
            });

            //  User profile image on click listener

            reviewCreatedAdapter.userphoto_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (reviewList.get(0).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", reviewList.get(0).getUserId());
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", reviewList.get(0).getUserId());
                        context.startActivity(intent);
                    }

                }
            });

            //  Reviewer profile image on click listener


            reviewCreatedAdapter.review_user_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (reviewList.get(0).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", reviewList.get(0).getUserId());
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", reviewList.get(0).getUserId());
                        context.startActivity(intent);
                    }

                }
            });

            //  Hotel view on click listener

            reviewCreatedAdapter.rl_hotel_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, Review_in_detail_activity.class);
                    intent.putExtra("hotelid", reviewList.get(0).getHotelId());
                    intent.putExtra("f_name", reviewList.get(0).getFirstName());
                    intent.putExtra("l_name", reviewList.get(0).getLastName());
                    intent.putExtra("reviewid", reviewList.get(0).getRevratId());
                    intent.putExtra("timelineId", gettingTimelineAllPojos_list.get(position).getTimelineId());
                    context.startActivity(intent);

                }
            });
            //  Delivery mode view on click listener

            reviewCreatedAdapter.ll_delivery_mode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, ReviewDeliveryDetails.class);
                    intent.putExtra("catType", reviewList.get(0).getCategoryType());
                    intent.putExtra("delMode", reviewList.get(0).getDelivery_mode());
                    context.startActivity(intent);

                }
            });


            // Like functionality and its api calling

            reviewCreatedAdapter.img_btn_review_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);
                    reviewCreatedAdapter.ll_likecomment.setVisibility(VISIBLE);


                    if (reviewList.get(0).isUserliked()) {


                        try {


                            reviewCreatedAdapter.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                            reviewList.get(0).setUserliked(false);
                            reviewList.get(0).setHtlRevLikes(String.valueOf(0));


                            int total_likes = Integer.parseInt(reviewList.get(0).getTotalLikes());

                            if (total_likes == 1) {

                                total_likes = total_likes - 1;
                                reviewList.get(0).setTotalLikes(String.valueOf(total_likes));
                                reviewCreatedAdapter.tv_like_count.setVisibility(View.GONE);


                            } else if (total_likes == 2) {

                                total_likes = total_likes - 1;
                                reviewList.get(0).setTotalLikes(String.valueOf(total_likes));
                                reviewCreatedAdapter.tv_like_count.setText(String.valueOf(total_likes) + " Like");
                                reviewCreatedAdapter.tv_like_count.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes - 1;
                                reviewList.get(0).setTotalLikes(String.valueOf(total_likes));
                                reviewCreatedAdapter.tv_like_count.setText(String.valueOf(total_likes) + " Likes");
                                reviewCreatedAdapter.tv_like_count.setVisibility(View.VISIBLE);

                            }


                            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                            int userid = parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiInterface.create_hotel_review_likes("create_hotel_review_likes", parseInt(reviewList.get(0).getHotelId()), parseInt(reviewList.get(0).getRevratId()), 0, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();
                                    int resposecode = response.body().getResponseCode();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);
                                    Log.e("resposecode", "resposecode->" + resposecode);

                                    if (resposecode == 1) {

                                        //Success

                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {

                        try {


                            reviewCreatedAdapter.ll_likecomment.setVisibility(VISIBLE);
                            reviewCreatedAdapter.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                            reviewList.get(0).setUserliked(true);
                            reviewList.get(0).setHtlRevLikes(String.valueOf(1));


                            int total_likes = Integer.parseInt(reviewList.get(0).getTotalLikes());

                            if (total_likes == 0) {

                                total_likes = total_likes + 1;
                                reviewList.get(0).setTotalLikes(String.valueOf(total_likes));
                                reviewCreatedAdapter.tv_like_count.setText(String.valueOf(total_likes) + " Like");
                                reviewCreatedAdapter.tv_like_count.setVisibility(View.VISIBLE);

                            } else {

                                total_likes = total_likes + 1;
                                reviewList.get(0).setTotalLikes(String.valueOf(total_likes));
                                reviewCreatedAdapter.tv_like_count.setText(String.valueOf(total_likes) + " Likes");

                            }

                            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

                            int userid = parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiInterface.create_hotel_review_likes("create_hotel_review_likes", parseInt(reviewList.get(0).getHotelId()), parseInt(reviewList.get(0).getRevratId()), 1, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        //Success


                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                    }


                }
            });


            // More settings visibility


            if (reviewList.get(0).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                reviewCreatedAdapter.img_view_more.setVisibility(VISIBLE);
            } else {
                reviewCreatedAdapter.img_view_more.setVisibility(GONE);
            }

            // More settings

            reviewCreatedAdapter.img_view_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    android.support.v7.app.AlertDialog.Builder front_builder = new android.support.v7.app.AlertDialog.Builder(context);
                    front_builder.setItems(itemsothers, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (itemsothers[which].equals("Bucket list options")) {

                                dialog.dismiss();

                                Intent intent = new Intent(context, CreateBucketlistActivty.class);
                                intent.putExtra("f_name", reviewList.get(0).getFirstName());
                                intent.putExtra("l_name", reviewList.get(0).getLastName());
                                intent.putExtra("reviewid", reviewList.get(0).getRevratId());
                                intent.putExtra("hotelid", reviewList.get(0).getHotelId());
                                intent.putExtra("timelineId", gettingTimelineAllPojos_list.get(position).getTimelineId());

                                context.startActivity(intent);

                            } else if (itemsothers[which].equals("Delete review")) {


                                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Are you sure want to delete this reviewed hotel?")
                                        .setContentText("Won't be able to recover this hotel anymore!")
                                        .setConfirmText("Delete")
                                        .setCancelText("Cancel")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                                try {
                                                    String createuserid = Pref_storage.getDetail(context, "userId");

                                                    String review_id = reviewList.get(0).getRevratId();

                                                    Call<CommonOutputPojo> call = apiService.update_delete_hotel_review("update_delete_hotel_review", Integer.parseInt(review_id), Integer.parseInt(createuserid));
                                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                                        @Override
                                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                            if (response.body().getResponseCode() == 1) {
                                                                reviewList.remove(0);
                                                                notifyItemRemoved(0);
                                                                notifyItemRangeChanged(0, reviewList.size());

                                                                Toast.makeText(context, "Deleted!!", Toast.LENGTH_LONG).show();


                                                            }

                                                        }

                                                        @Override
                                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                            //Error
                                                            Log.e("FailureError", "" + t.getMessage());
                                                        }
                                                    });
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        })
                                        .show();

                                dialog.dismiss();
                            }
                        }
                    });
                    front_builder.show();
                }
            });


            // Adding bucketlist API

            reviewCreatedAdapter.img_bucket_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);

                    Intent intent = new Intent(context, CreateBucketlistActivty.class);
                    intent.putExtra("f_name", reviewList.get(0).getFirstName());
                    intent.putExtra("l_name", reviewList.get(0).getLastName());
                    intent.putExtra("reviewid", reviewList.get(0).getRevratId());
                    intent.putExtra("hotelid", reviewList.get(0).getHotelId());
                    intent.putExtra("timelineId", gettingTimelineAllPojos_list.get(position).getTimelineId());

                    context.startActivity(intent);


                }
            });


            // Load all image Activity

            reviewCreatedAdapter.rl_view_more_top.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);

                    Intent intent = new Intent(context, AllDishDisplayActivity.class);
                    intent.putExtra("reviewid", reviewList.get(0).getRevratId());
                    intent.putExtra("hotelname", reviewList.get(0).getHotelName());
                    intent.putExtra("cat_type", reviewList.get(0).getCategoryType());
                    context.startActivity(intent);


                }
            });


            // Load all image Activity

            reviewCreatedAdapter.top_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);
                    Intent intent = new Intent(context, AllDishDisplayActivity.class);
                    intent.putExtra("reviewid", reviewList.get(0).getRevratId());
                    intent.putExtra("hotelname", reviewList.get(0).getHotelName());
                    intent.putExtra("cat_type", reviewList.get(0).getCategoryType());

                    context.startActivity(intent);


                }
            });

            // Load all image Activity

            reviewCreatedAdapter.rl_view_more_avoid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);
                    Intent intent = new Intent(context, AllDishDisplayActivity.class);
                    intent.putExtra("reviewid", reviewList.get(0).getRevratId());
                    intent.putExtra("hotelname", reviewList.get(0).getHotelName());
                    context.startActivity(intent);

                }
            });

            // Load all image Activity

            reviewCreatedAdapter.avoid_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);
                    Intent intent = new Intent(context, AllDishDisplayActivity.class);
                    intent.putExtra("reviewid", reviewList.get(0).getRevratId());
                    intent.putExtra("hotelname", reviewList.get(0).getHotelName());
                    context.startActivity(intent);


                }
            });


            // API for top dish images


            if (reviewList.get(0).getTop_dish_img_count() == 0) {

            } else {
                try {

                    reviewTopDishPojoArrayList.clear();

                    ArrayList<ReviewTopDishPojo> reviewTopDishPojoList = new ArrayList<>();


                    for (int k = 0; k < reviewList.get(0).getTopdishimage().size(); k++) {

                        String topdishimage = reviewList.get(0).getTopdishimage().get(k).getImg();
                        String topdishimagename = reviewList.get(0).getTopdishimage().get(k).getDishname();

                        ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimagename);
                        reviewTopDishPojoList.add(reviewTopDishPojo);
                    }

                    String review_id = reviewList.get(0).getRevratId();
                    String hotelName = reviewList.get(0).getHotelName();
                    String catType = reviewList.get(0).getCategoryType();

                    ReviewTopDishAdapter reviewTopDishAdapter = new ReviewTopDishAdapter(context, reviewTopDishPojoList, review_id, hotelName, catType);
                    LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    reviewCreatedAdapter.rv_review_top_dish.setLayoutManager(llm);
//                Collections.reverse(reviewTopDishPojoArrayList);

                    reviewCreatedAdapter.rv_review_top_dish.setItemAnimator(new DefaultItemAnimator());
                    reviewCreatedAdapter.rv_review_top_dish.setAdapter(reviewTopDishAdapter);
//                reviewCreatedAdapter.rv_review_top_dish.smoothScrollToPosition(0);
                    reviewCreatedAdapter.rv_review_top_dish.setNestedScrollingEnabled(false);
                    reviewTopDishAdapter.notifyDataSetChanged();
                    reviewCreatedAdapter.rl_top_dish.setVisibility(VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            // Dish to avoid names API

            if (reviewList.get(0).getAvoid_dish_img_count() != 0) {
                try {
                    reviewAvoidDishPojoArrayList.clear();

                    for (int i = 0; i < reviewList.get(0).getAvoiddishimage().size(); i++) {

                        String avoiddishimage = reviewList.get(0).getAvoiddishimage().get(i).getImg();
                        String avoiddishimagename = reviewList.get(0).getAvoiddishimage().get(i).getDishname();

                        ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(avoiddishimage, avoiddishimagename);
                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);

                    }


                    String review_id = reviewList.get(0).getRevratId();
                    String hotelName = reviewList.get(0).getHotelName();
                    String catType = reviewList.get(0).getCategoryType();


                    ReviewAvoidDishAdapter reviewWorstDishAdapter = new ReviewAvoidDishAdapter(context, reviewAvoidDishPojoArrayList, review_id, hotelName, catType);

                    LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    reviewCreatedAdapter.rv_review_avoid_dish.setLayoutManager(llm);
//                Collections.reverse(reviewAvoidDishPojoArrayList);

                    reviewCreatedAdapter.rv_review_avoid_dish.setItemAnimator(new DefaultItemAnimator());
                    reviewCreatedAdapter.rv_review_avoid_dish.setAdapter(reviewWorstDishAdapter);
//                reviewCreatedAdapter.rv_review_avoid_dish.smoothScrollToPosition(0);
                    reviewCreatedAdapter.rv_review_avoid_dish.setNestedScrollingEnabled(false);
                    reviewWorstDishAdapter.notifyDataSetChanged();
                    reviewCreatedAdapter.rl_avoid_dish.setVisibility(VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            // API for ambience images

            int ambi_image_count = reviewList.get(0).getAmbiImageCount();

            if (ambi_image_count != 0) {

                reviewCreatedAdapter.review_photos_recylerview.setVisibility(VISIBLE);

                try {

                    reviewAmbiImagePojoArrayList.clear();

                    for (int m = 0; m < reviewList.get(0).getAmbiImageCount(); m++) {

                        ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(reviewList.get(0).getAmbiImage().get(m).getImg());
                        reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);

                    }

                    String review_id = reviewList.get(0).getRevratId();
                    String hotelName = reviewList.get(0).getHotelName();
                    String catType = reviewList.get(0).getCategoryType();

                    ReviewAmbienceAdapter ambianceImagesAdapter = new ReviewAmbienceAdapter(context, reviewAmbiImagePojoArrayList, review_id, hotelName, catType);

                    RecyclerView.LayoutManager llm3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

                    reviewCreatedAdapter.review_photos_recylerview.setLayoutManager(llm3);
                    reviewCreatedAdapter.review_photos_recylerview.setItemAnimator(new DefaultItemAnimator());
                    reviewCreatedAdapter.review_photos_recylerview.setAdapter(ambianceImagesAdapter);
                    reviewCreatedAdapter.review_photos_recylerview.setNestedScrollingEnabled(false);
                    ambianceImagesAdapter.notifyDataSetChanged();
                    reviewCreatedAdapter.ll_view_more.setVisibility(VISIBLE);
                    reviewCreatedAdapter.ll_view_more.setVisibility(VISIBLE);


                    if (reviewAmbiImagePojoArrayList.size() == 0) {
                        reviewCreatedAdapter.ambi_viewpager.setVisibility(View.GONE);
                        reviewCreatedAdapter.rl_ambiance_dot.setVisibility(View.GONE);
                        reviewCreatedAdapter.title_ambience.setVisibility(GONE);
                        reviewCreatedAdapter.ll_ambi_more.setVisibility(GONE);
                        reviewCreatedAdapter.ll_view_more.setVisibility(GONE);
                        reviewCreatedAdapter.review_photos_recylerview.setVisibility(GONE);
                    } else {


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            // API for packaging images images

            int pack_image_count = reviewList.get(0).getPackImageCount();
            Log.e(TAG, "pack_image_countviji: " + pack_image_count);
            ArrayList<PackImage> reviewPackageImagePojoArrayList = new ArrayList<>();

            if (pack_image_count != 0) {

                reviewCreatedAdapter.review_photos_recylerview.setVisibility(GONE);
                try {

                    reviewPackageImagePojoArrayList.clear();

                    for (int n = 0; n < reviewList.get(0).getPackImageCount(); n++) {

                        PackImage packImage = new PackImage(reviewList.get(0).getPackImage().get(n).getImg());
                        reviewPackageImagePojoArrayList.add(packImage);

                    }

                    String review_id = reviewList.get(0).getRevratId();
                    String hotelName = reviewList.get(0).getHotelName();
                    String catType = reviewList.get(0).getCategoryType();

                    PackagingDetailsAdapter packagingDetailsAdapter = new PackagingDetailsAdapter(context, reviewPackageImagePojoArrayList, review_id, hotelName, catType);

                    RecyclerView.LayoutManager llm3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

                    reviewCreatedAdapter.package_recylerview.setLayoutManager(llm3);
                    reviewCreatedAdapter.package_recylerview.setItemAnimator(new DefaultItemAnimator());
                    reviewCreatedAdapter.package_recylerview.setAdapter(packagingDetailsAdapter);
                    reviewCreatedAdapter.package_recylerview.setNestedScrollingEnabled(false);
                    reviewCreatedAdapter.ll_view_more.setVisibility(VISIBLE);


                    if (reviewPackageImagePojoArrayList.size() == 0) {
                        reviewCreatedAdapter.ambi_viewpager.setVisibility(View.GONE);
                        reviewCreatedAdapter.rl_ambiance_dot.setVisibility(View.GONE);
                        reviewCreatedAdapter.title_ambience.setVisibility(GONE);
                        reviewCreatedAdapter.ll_ambi_more.setVisibility(GONE);
                        reviewCreatedAdapter.ll_view_more.setVisibility(GONE);
                        reviewCreatedAdapter.review_photos_recylerview.setVisibility(GONE);
                    } else {


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            // Hotel Images
            reviewCreatedAdapter.ll_view_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    reviewCreatedAdapter.ll_ambi_more.setVisibility(VISIBLE);
                    reviewCreatedAdapter.ll_view_more.setVisibility(GONE);

                }
            });

            reviewCreatedAdapter.layout_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reviewCreatedAdapter.ll_ambi_more.setVisibility(VISIBLE);
                    reviewCreatedAdapter.ll_view_more.setVisibility(GONE);

                }
            });


        }


    }


    /* Create API call for Poll */

    private void createPoll(int quesId, int pollId) {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Call<AnswerPojoClass> call = apiService.createAnswer("create_answer", 0, quesId,
                        pollId, 1, "poll", latitude, longitude, userid);

                call.enqueue(new Callback<AnswerPojoClass>() {
                    @Override
                    public void onResponse(@NonNull Call<AnswerPojoClass> call, @NonNull Response<AnswerPojoClass> response) {

                        if (response.body() != null) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                            }

                        } else {

                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(@NonNull Call<AnswerPojoClass> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "FailureError" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }


    }


    // Deleting the timeline post

    private void createDeleteTimeline(String timelineId, final int position) {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                String createdBy = Pref_storage.getDetail(context, "userId");

                Call<CommonOutputPojo> call = apiService.create_delete_timeline("create_delete_timeline", Integer.parseInt(timelineId), Integer.parseInt(createdBy));

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(@NonNull Call<CommonOutputPojo> call, @NonNull Response<CommonOutputPojo> response) {

                        if (response.body() != null) {

                            Log.e("ResponseStatus", "ResponseStatus->" + response.body().getResponseMessage());

                            if (response.body().getResponseMessage().equals("nodata")) {

                                /* Delete postion */

                                deletePosition(position);

                                Toast.makeText(context, "Deleted!!", Toast.LENGTH_LONG).show();


                            }

                        }


                    }

                    @Override
                    public void onFailure(@NonNull Call<CommonOutputPojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }


    }


    /* Setting the images in the view pager */

    private void initializeViews(List<String> dataModel, final RecyclerView.ViewHolder holder, int position) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(dataModel);

        ((TimelineAdapter) holder).mviewpager.setAdapter(adapter);
        ((TimelineAdapter) holder).mviewpager.setClipToPadding(false);
        ((TimelineAdapter) holder).mviewpager.setPadding(0, 0, 0, 0);
        ((TimelineAdapter) holder).circleIndicatortimeline.setViewPager(((TimelineAdapter) holder).mviewpager);
        adapter.notifyDataSetChanged();

        ((TimelineAdapter) holder).mviewpager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (context != null && context instanceof _SwipeActivityClass) {
                    ((_SwipeActivityClass) context).setOverlookSwipe(true);
                }
                return false;
            }
        });

    }


    /* Remove the deleted position */

    private void deletePosition(int position) {

        gettingTimelineAllPojos_list.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, gettingTimelineAllPojos_list.size());

    }

    /* First timeline post API */

    private String getMonthName(String date) {

        String monthName = "";

        String month = date.substring(5, 7);

        switch (month) {


            case "01":
                monthName = "JAN";
                break;

            case "02":
                monthName = "FEB";
                break;

            case "03":
                monthName = "MAR";
                break;

            case "04":
                monthName = "APR";
                break;

            case "05":
                monthName = "MAY";
                break;

            case "06":
                monthName = "JUN";
                break;

            case "07":
                monthName = "JUL";
                break;

            case "08":
                monthName = "AUG";
                break;

            case "09":
                monthName = "SEP";
                break;

            case "10":
                monthName = "OCT";
                break;

            case "11":
                monthName = "NOV";
                break;

            case "12":
                monthName = "DEC";
                break;


        }


        return monthName;

    }


    // Get month name

    private String getDate(String date) {

        String day = date.substring(8, 10);

        return day;
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    public void updateImageStatus(String url) {
        imageStatusTimeline = url;
    }


    /*More Options dialog listener class*/
    public class CustomDialogClass extends Dialog implements View.OnClickListener /*ViewPager.OnPageChangeListener*/ {

        PopupWindow popupWindow;
        View popup;
        private AlertDialog alt;
        int position;
        String timelineid;
        String userid;
        TextView txtDeletepost;
        TextView txtReport;
        TextView txtCopylink;
        TextView txtTurnoffpost;
        TextView txtSharevia;
        TextView txtEditpost;
        TextView txt_sharechat;

        CustomDialogClass(Context ctx, String timelineid, String userid, int position) {
            super(context);
            context = ctx;
            this.timelineid = timelineid;
            this.userid = userid;
            this.position = position;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Objects.requireNonNull(getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);

            setContentView(R.layout.custom_dialog_timeline);

            txtDeletepost = findViewById(R.id.txt_deletepost);
            txtEditpost = findViewById(R.id.txt_editpost);
            txtReport = findViewById(R.id.txt_report);
            txtCopylink = findViewById(R.id.txt_copylink);
            txtCopylink.setVisibility(GONE);
            txtTurnoffpost = findViewById(R.id.txt_turnoffpost);
            txtSharevia = findViewById(R.id.txt_sharevia);
            txt_sharechat = findViewById(R.id.txt_sharechat);

            txtDeletepost.setOnClickListener(this);
            txtEditpost.setOnClickListener(this);
            txtReport.setOnClickListener(this);
            txtCopylink.setOnClickListener(this);
            txtTurnoffpost.setOnClickListener(this);
            txtSharevia.setOnClickListener(this);
            txt_sharechat.setOnClickListener(this);

            if (userid.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                txtDeletepost.setVisibility(VISIBLE);
                txtEditpost.setVisibility(VISIBLE);

            } else {

                txtDeletepost.setVisibility(GONE);
                txtEditpost.setVisibility(GONE);

            }

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                /*Edit post on click listener*/

                case R.id.txt_editpost:

                    Intent intent = new Intent(context, EditHistoryMap.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("timelineId", timelineid);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    dismiss();

                    break;


                /*Delete post on click listener*/

                case R.id.txt_deletepost:

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to delete this post?")
                            .setContentText("Won't be able to recover this post anymore!")
                            .setConfirmText("Yes,delete it!")
                            .setCancelText("No,cancel!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {

                                    sDialog.dismissWithAnimation();


                                    /* Calling delete post timeline */

                                    createDeleteTimeline(timelineid, position);

                                }
                            })
                            .show();
                    dismiss();
                    break;

                case R.id.txt_report:

                    // showPopUp(v);
                    // report issue
                    popup = LayoutInflater.from(context).inflate(R.layout.reportoption_popup, null);
                    popupWindow = new PopupWindow(popup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
                    final AlertDialog.Builder alertbox = new AlertDialog.Builder(context);

                    final EditText edt_reportcomments = (EditText) popup.findViewById(R.id.edt_reportcomments);
                    Button cancel = (Button) popup.findViewById(R.id.cancel);
                    Button submit = (Button) popup.findViewById(R.id.submit);
                    final ProgressBar progress_timeline = (ProgressBar) popup.findViewById(R.id.progress_timeline);


                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            popupWindow.dismiss();
                            alt.dismiss();
                            dismiss();

                        }
                    });


                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //call create_report_email api
                            if (Utility.isConnected(context)) {

                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                if (edt_reportcomments.getText().toString().length() != 0) {
                                    try {
                                        progress_timeline.setVisibility(VISIBLE);

                                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                                        int posttype = 0;
                                        String moduleid = null;

                                        //get timeline id
                                        if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("0")) {
                                            posttype = 1;
                                        } else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("1") ||
                                                gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("2") ||
                                                gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("3")) {
                                            posttype = 2;

                                        } else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("5") ||
                                                gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("6") ||
                                                gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("7")) {
                                            posttype = 3;

                                        } else if (gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("8") ||
                                                gettingTimelineAllPojos_list.get(position).getPostType().equalsIgnoreCase("9")) {
                                            posttype = 4;
                                        }


                                        //find module id
                                        if (Integer.valueOf(gettingTimelineAllPojos_list.get(position).getTimelineId()) != 0) {
                                            moduleid = gettingTimelineAllPojos_list.get(position).getTimelineId();
                                        } else if (Integer.valueOf(gettingTimelineAllPojos_list.get(position).getReviewId()) != 0) {
                                            moduleid = gettingTimelineAllPojos_list.get(position).getReviewId();

                                        } else if (Integer.valueOf(gettingTimelineAllPojos_list.get(position).getEventId()) != 0) {
                                            moduleid = gettingTimelineAllPojos_list.get(position).getEventId();

                                        } else if (Integer.valueOf(gettingTimelineAllPojos_list.get(position).getQuestId()) != 0) {
                                            moduleid = gettingTimelineAllPojos_list.get(position).getQuestId();
                                        }


                                        Call<ReportResponse> call = apiService.create_report_email("create_report_email",
                                                posttype,
                                                edt_reportcomments.getText().toString(),
                                                Integer.valueOf(gettingTimelineAllPojos_list.get(position).getCreatedBy()),
                                                moduleid,
                                                userid);


                                        call.enqueue(new Callback<ReportResponse>() {
                                            @Override
                                            public void onResponse(Call<ReportResponse> call, Response<ReportResponse> response) {

                                                if (response.body() != null) {

                                                    String responseStatus = response.body().getResponseMessage();

                                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                    if (responseStatus.equals("success")) {

                                                        progress_timeline.setVisibility(GONE);

                                                        //Success
                                                        popupWindow.dismiss();
                                                        alt.dismiss();

                                                        dismiss();
                                                    }

                                                } else {

                                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<ReportResponse> call, Throwable t) {
                                                //Error
                                                Log.e("FailureError", "FailureError" + t.getMessage());
                                                progress_timeline.setVisibility(GONE);
                                            }
                                        });

                                    } catch (Exception e) {

                                        e.printStackTrace();

                                    }
                                } else {
                                    edt_reportcomments.setError("Type your report");
                                }
                            } else {
                                Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                            }

                        }
                    });


                    alertbox.setView(popup);
                    alt = alertbox.show();
                    popupWindow.dismiss();


                    //dismiss();

                    break;

                case R.id.txt_copylink:

                    //Toast.makeText(context, "Link has been copied to clipboard", Toast.LENGTH_SHORT).show();

                    dismiss();

                    break;

                case R.id.txt_turnoffpost:

                    dismiss();

                    break;

                /* WhatsApp share on click listener */

                case R.id.txt_sharevia:

//                    try {
//
////                      String smsNumber = "";
//
//                        Intent sendIntent = new Intent("android.intent.action.MAIN");
//
//                        //sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
//
//                        sendIntent.setAction(Intent.ACTION_SEND);
//
//                        sendIntent.setType("text/plain");
//
//                        sendIntent.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
//
////                      sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net"); //phone number without "+" prefix
//
//                        sendIntent.putExtra("Foodwall", "@s.whatsapp.net"); // phone number without "+" prefix
//
//                        sendIntent.setPackage("com.whatsapp");
//
//                        context.startActivity(sendIntent);
//
//                    } catch (Exception e) {
//
//                        Toast.makeText(context, "No whatsapp installed!!", Toast.LENGTH_SHORT).show();
//
//                    }

                    /* All social media intent */

                    Intent i = new Intent(Intent.ACTION_SEND);

                    i.setType("text/plain");

                    i.putExtra(Intent.EXTRA_SUBJECT, "See this Foodwall photo by @xxxx");

                    i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/pdffd/Rgdf78hfhud876");

                    context.startActivity(Intent.createChooser(i, "Share via"));

                    dismiss();




                    /* Show share dialog BOTH image and text*/

                    /*Uri imageUri = Uri.parse(Pref_storage.getDetail(context,"picture_user_login"));
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    //Target whatsapp:
                    shareIntent.setPackage("com.whatsapp");
                    //Add text and then Image URI
                    shareIntent.putExtra(Intent.EXTRA_TEXT, "Foodwall Picture");
                    shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                    shareIntent.setType("image/jpeg");
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    try {

                        context.startActivity(shareIntent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(context, "No whatsapp installed!!", Toast.LENGTH_SHORT).show();
                    }*/


                    break;

                case R.id.txt_sharechat:
                    Intent intent1 = new Intent(context, ChatActivity.class);
                    intent1.putExtra("redirect_url", gettingTimelineAllPojos_list.get(position).getRedirect_url());
                    context.startActivity(intent1);

                    break;


                default:

                    break;
            }
        }


    }


    public List<Get_all_stories_pojo> getStoriesPojoList() {
        return storiesPojoList;
    }

    public void updateStoriesPojoList(List<Get_all_stories_pojo> storiesPojoList) {
        this.storiesPojoList.clear();
        this.storiesPojoList.addAll(storiesPojoList);
        notifyItemChanged(0);
    }

    public interface onRefershListener {

        void onRefereshedFragment(View view, int adapterPosition, View pollAdapterView);
    }

}
