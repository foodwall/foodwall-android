package com.kaspontech.foodwall.adapters.events.EventsType;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.eventspackage.CreateEvent;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetAllEventData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class GetMyEventsAdapter extends RecyclerView.Adapter<GetMyEventsAdapter.MyViewHolder> {

    /**
     * Context
     **/
    Context context;

    int event_id;
    /**
     * Arraylist
     **/
    List<GetAllEventData> getAllEventDataList = new ArrayList<>();

    /**
     * Getall event data page
     **/
    GetAllEventData getAllEventData;

    boolean interest_flag = false;
    private static int userInterestedFlag = 0;


    public GetMyEventsAdapter(Context context, List<GetAllEventData> getAllEventDataList) {
        this.context = context;
        this.getAllEventDataList = getAllEventDataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView eventBackground;
        ImageButton edit_button, delete_button;
        LinearLayout ll_edit, ll_delete;
        TextView date, location, eventName, editText, deleteText;

        public MyViewHolder(View view) {
            super(view);

            ll_edit = (LinearLayout) view.findViewById(R.id.ll_edit);
            ll_delete = (LinearLayout) view.findViewById(R.id.ll_delete);

            edit_button = (ImageButton) view.findViewById(R.id.edit_button);
            delete_button = (ImageButton) view.findViewById(R.id.delete_button);

            editText = (TextView) view.findViewById(R.id.edit_text);
            deleteText = (TextView) view.findViewById(R.id.delete_text);

            eventBackground = (ImageView) view.findViewById(R.id.event_background);
            date = (TextView) view.findViewById(R.id.event_date);
            location = (TextView) view.findViewById(R.id.event_location);
            eventName = (TextView) view.findViewById(R.id.event_name);

        }

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_get_user_events, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        getAllEventData = getAllEventDataList.get(position);


        /*Event date */
        String eventDate = getMonthName(getAllEventData.getStartDate()) + " " + getDate(getAllEventData.getStartDate());

        /*Image loading*/

        GlideApp.with(context)
                .load(getAllEventData.getEventImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.01f)
                .centerCrop()
                .into(holder.eventBackground);


        /*Event date setting*/
        holder.date.setText(eventDate);

        /*Event name setting*/
        holder.eventName.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getAllEventData.getEventName()));

        /*Event location setting*/
        holder.location.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getAllEventData.getLocation()));

        /*Edit button on click listener*/
        holder.edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetAllEventData getAllEventData = getAllEventDataList.get(position);

                if (getAllEventData.getEventType() == 1) {

                    Intent intent = new Intent(context, CreateEvent.class);
                    intent.putExtra("eventName", "Edit private event");
                    intent.putExtra("eventCode", 1);
                    intent.putExtra("eventEditPosition", position);
                    intent.putExtra("LaunchMode", 2);

                    intent.putExtra("eventId", getAllEventDataList.get(position).getEventId());
                    intent.putExtra("eventImage", getAllEventDataList.get(position).getEventImage());
                    intent.putExtra("eventTitle", getAllEventDataList.get(position).getEventName());
                    intent.putExtra("eventStartDate", getAllEventDataList.get(position).getStartDate());
                    intent.putExtra("eventEndDate", getAllEventDataList.get(position).getEndDate());
                    intent.putExtra("eventLocation", getAllEventDataList.get(position).getLocation());
                    intent.putExtra("eventDescription", getAllEventDataList.get(position).getEventDescription());

                    context.startActivity(intent);
                    ((Activity) context).finish();

                } else {

                    Intent intent = new Intent(context, CreateEvent.class);
                    intent.putExtra("eventName", "Edit public event");
                    intent.putExtra("eventCode", 2);
                    intent.putExtra("eventEditPosition", position);
                    intent.putExtra("LaunchMode", 2);

                    intent.putExtra("eventId", getAllEventDataList.get(position).getEventId());
                    intent.putExtra("eventImage", getAllEventDataList.get(position).getEventImage());
                    intent.putExtra("eventTitle", getAllEventDataList.get(position).getEventName());
                    intent.putExtra("eventStartDate", getAllEventDataList.get(position).getStartDate());
                    intent.putExtra("eventEndDate", getAllEventDataList.get(position).getEndDate());
                    intent.putExtra("eventLocation", getAllEventDataList.get(position).getLocation());
                    intent.putExtra("eventDescription", getAllEventDataList.get(position).getEventDescription());
                    intent.putExtra("eventTicketUrl", getAllEventDataList.get(position).getTicketUrl());

                    context.startActivity(intent);
                    ((Activity) context).finish();

                }


            }
        });

        /*Edit text on click listener*/
        holder.editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetAllEventData getAllEventData = getAllEventDataList.get(position);

                if (getAllEventData.getEventType() == 1) {

                    Intent intent = new Intent(context, CreateEvent.class);
                    intent.putExtra("eventName", "Edit private event");
                    intent.putExtra("eventCode", 1);
                    intent.putExtra("eventEditPosition", position);
                    intent.putExtra("LaunchMode", 2);

                    intent.putExtra("eventId", getAllEventDataList.get(position).getEventId());
                    intent.putExtra("eventImage", getAllEventDataList.get(position).getEventImage());
                    intent.putExtra("eventTitle", getAllEventDataList.get(position).getEventName());
                    intent.putExtra("eventStartDate", getAllEventDataList.get(position).getStartDate());
                    intent.putExtra("eventEndDate", getAllEventDataList.get(position).getEndDate());
                    intent.putExtra("eventLocation", getAllEventDataList.get(position).getLocation());
                    intent.putExtra("eventDescription", getAllEventDataList.get(position).getEventDescription());

                    context.startActivity(intent);
                    ((Activity) context).finish();

                } else {

                    Intent intent = new Intent(context, CreateEvent.class);
                    intent.putExtra("eventName", "Edit public event");
                    intent.putExtra("eventCode", 2);
                    intent.putExtra("eventEditPosition", position);
                    intent.putExtra("LaunchMode", 2);

                    intent.putExtra("eventId", getAllEventDataList.get(position).getEventId());
                    intent.putExtra("eventImage", getAllEventDataList.get(position).getEventImage());
                    intent.putExtra("eventTitle", getAllEventDataList.get(position).getEventName());
                    intent.putExtra("eventStartDate", getAllEventDataList.get(position).getStartDate());
                    intent.putExtra("eventEndDate", getAllEventDataList.get(position).getEndDate());
                    intent.putExtra("eventLocation", getAllEventDataList.get(position).getLocation());
                    intent.putExtra("eventDescription", getAllEventDataList.get(position).getEventDescription());
                    intent.putExtra("eventTicketUrl", getAllEventDataList.get(position).getTicketUrl());

                    context.startActivity(intent);
                    ((Activity) context).finish();

                }


            }
        });

        /*Delete on click listener*/
        holder.ll_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure to delete this event?")
                        .setContentText("Won't be able to recover this event anymore!")
                        .setConfirmText("Yes,delete it!")
                        .setCancelText("No,cancel!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();


                                deleteEventsData(getAllEventDataList.get(position).getEventId(), position);

                            }
                        })
                        .show();


            }
        });

        /*Delete button on click listener*/
        holder.delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure to delete this event?")
                        .setContentText("Won't be able to recover this event anymore!")
                        .setConfirmText("Yes,delete it!")
                        .setCancelText("No,cancel!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();


                                deleteEventsData(getAllEventDataList.get(position).getEventId(), position);

                            }
                        })
                        .show();

            }
        });


    }

    /*Deleting API call*/
    private void deleteEventsData(int eventId, final int position) {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Call<CommonOutputPojo> call = apiService.deleteEvents("create_delete_events", eventId, userid);

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {
                            //Success
                            removeAt(position);

                            Toast.makeText(context, "Event deleted successully", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();

        }


    }
    /*Removing the postion*/
    public void removeAt(int position) {
        getAllEventDataList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getAllEventDataList.size());
    }

    /*Getting month name*/

    private String getMonthName(String date) {

        String monthName = "";

        String month = date.substring(5, 7);

        switch (month) {


            case "01":
                monthName = "JAN";
                break;

            case "02":
                monthName = "FEB";
                break;

            case "03":
                monthName = "MAR";
                break;

            case "04":
                monthName = "APR";
                break;

            case "05":
                monthName = "MAY";
                break;

            case "06":
                monthName = "JUN";
                break;

            case "07":
                monthName = "JUL";
                break;

            case "08":
                monthName = "AUG";
                break;

            case "09":
                monthName = "SEP";
                break;

            case "10":
                monthName = "OCT";
                break;

            case "11":
                monthName = "NOV";
                break;

            case "12":
                monthName = "DEC";
                break;
        }


        return monthName;
    }

    private String getDate(String date) {

        String day = date.substring(8, 10);

        return day;
    }

    @Override
    public int getItemCount() {
        return getAllEventDataList.size();
    }
}
