package com.kaspontech.foodwall.adapters.Review_Image_adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_image_pojo;
import com.kaspontech.foodwall.R;
import com.squareup.picasso.Picasso;
//

import java.util.ArrayList;

//

public class Review_worst_dish_slider_adapter extends PagerAdapter implements View.OnTouchListener {


    ArrayList<Get_dish_type_image_pojo> IMAGES = new ArrayList<>();
    //private List<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;

    private PhotoView imageView;

    /*public Slide_ambience_adapter(Context context, List<String> IMAGES) {
            this.context = context;
            this.IMAGES=IMAGES;
            inflater = LayoutInflater.from(context);
            }*/
    public Review_worst_dish_slider_adapter(Context context, ArrayList<Get_dish_type_image_pojo> IMAGES) {
        this.context = context;
        this.IMAGES=IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {

        View imageLayout = inflater.inflate(R.layout.slide_image_layout, view, false);

        assert imageLayout != null;
        imageView = (PhotoView) imageLayout.findViewById(R.id.image);


        try {
            Log.e("worst_images","IMAGES"+IMAGES.get(position).getImg().replace("[", "").replace("]", ""));

           Picasso.get()
                    .load(IMAGES.get(position).getImg().replace("[", "").replace("]", ""))
                    .centerCrop()
                    .into(imageView);

/*

            GlideApp.with(context).load(notificationgetimagelist.get(position).replace("[", "").replace("]", ""))
                   .centerCrop().into(imageView);
*/

            view.addView(imageLayout, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }





        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
