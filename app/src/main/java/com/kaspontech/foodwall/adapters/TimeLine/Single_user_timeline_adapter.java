package com.kaspontech.foodwall.adapters.TimeLine;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter.ShowTaggedPeopleAdapter;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Comments_activity;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.likesActivity;
import com.kaspontech.foodwall.historicalMapPackage.EditHistoryMap;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.CreateEditTimeline_Comments;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Timeline_single_user_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Image;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.comments_2;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

//

public class Single_user_timeline_adapter extends RecyclerView.Adapter<Single_user_timeline_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener, View.OnTouchListener {
    private Context context;
    private Timeline_single_user_pojo timelineSingleUserPojo;
    private Image image;

    boolean like_flag;
    String commenttimelineid, createcomment;
    Get_following_profile_pojo getFollowingProfilePojo;
    public  ArrayList<Timeline_single_user_pojo> getusertimelinelist = new ArrayList<>();
    public  ArrayList<String> getimagelist = new ArrayList<>();
    private static ViewPager mviewpager_user;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    RecyclerView recyler_tagged_people;
    ShowTaggedPeopleAdapter showTaggedPeopleAdapter;
    int totalLikes, totalComments;

    private CircleIndicator circleIndicator;

    private static final String TAG = "Single_self_timeline_ad";

    public Single_user_timeline_adapter(Context c, ArrayList<Timeline_single_user_pojo> gettinglist, ArrayList<String> gettingimage) {
        this.context = c;
        getusertimelinelist = gettinglist;
        getimagelist = gettingimage;

    }


    @NonNull
    @Override
    public Single_user_timeline_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_user_timeline, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Single_user_timeline_adapter.MyViewHolder vh = new Single_user_timeline_adapter.MyViewHolder(v);


        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Single_user_timeline_adapter.MyViewHolder holder, final int position) {

        timelineSingleUserPojo = getusertimelinelist.get(position);


        totalComments = Integer.parseInt(getusertimelinelist.get(position).getTotalComments());

        if (totalComments == 0) {

            holder.rl_comments_lay.setVisibility(GONE);

        } else {

            holder.rl_comments_lay.setVisibility(VISIBLE);

        }

        // TimeLine Image Display


        List<String> imagesList = new ArrayList<>();

        if (getusertimelinelist.get(position).getImageCount() != 0) {

            for (int i = 0; i < getusertimelinelist.get(position).getImage().size(); i++) {

                imagesList.add(getusertimelinelist.get(position).getImage().get(i).getImg());

            }

        }


        Log.e("TimeLineImages", "TimeLineImages->" + imagesList.toString());

        if (getusertimelinelist.get(position).getImageCount() == 0) {

            holder.rl_fullimage.setVisibility(View.GONE);

        } else if (getusertimelinelist.get(position).getImageCount() == 1) {

            holder.rl_fullimage.setVisibility(View.VISIBLE);
            holder.circleIndicatortimeline.setVisibility(View.GONE);

        }

        initializeViews(imagesList, holder, position);

        holder.circleIndicatortimeline.setViewPager(holder.mviewpager);

        final float density = context.getResources().getDisplayMetrics().density;

        holder.NUM_PAGES = imagesList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();

        final Runnable Update = new Runnable() {

            public void run() {

                if (holder.currentPage == holder.NUM_PAGES) {

                    holder.currentPage = 0;

                }

                holder.mviewpager.setCurrentItem(holder.currentPage++, true);
            }

        };

        // Pager listener over indicator
        holder.circleIndicatortimeline.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                holder.currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

               /* holder.mviewpager.setAdapter(new SlidingImage_Adapter(context, imagesList));

                holder.circleIndicatortimeline.setViewPager(holder.mviewpager);

                final float density = context.getResources().getDisplayMetrics().density;

                holder.NUM_PAGES = imagesList.size();

                // Auto start of viewpager
                final Handler handler = new Handler();

                final Runnable Update = new Runnable() {

                    public void run() {

                        if (holder.currentPage == holder.NUM_PAGES) {

                            holder.currentPage = 0;

                        }

                        holder.mviewpager.setCurrentItem(holder.currentPage++, true);
                    }

                };

                // Pager listener over indicator
                holder.circleIndicatortimeline.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                    @Override
                    public void onPageSelected(int position) {
                        holder.currentPage = position;

                    }

                    @Override
                    public void onPageScrolled(int pos, float arg1, int arg2) {

                    }

                    @Override
                    public void onPageScrollStateChanged(int pos) {

                    }
                });
*/


        //Userphoto_display

        try {
            Utility.picassoImageLoader(getusertimelinelist.get(position).getPicture(),1,holder.userphoto_profile, context);


//            GlideApp.with(context)
//                    .load(getusertimelinelist.get(position).getPicture())
//                    .centerCrop()
//                    .into(holder.userphoto_profile);

            //usercomment_photo_display
            Utility.picassoImageLoader(Pref_storage.getDetail(context, "picture_user_login"),1,holder.userphoto_comment, context);

//            GlideApp.with(context)
//                    .load(Pref_storage.getDetail(context, "picture_user_login"))
//                    .centerCrop()
//                    .into(holder.userphoto_comment);

        } catch (Exception e) {
            e.printStackTrace();
        }


        //Follow, following visibility


        if (Integer.parseInt(getusertimelinelist.get(position).getFollowingId()) == 0) {
            holder.txt_timeline_follow.setVisibility(VISIBLE);
            holder.img_follow_timeline.setVisibility(VISIBLE);
        } else if (Integer.parseInt(getusertimelinelist.get(position).getFollowingId()) != 0) {
            holder.txt_timeline_follow.setVisibility(GONE);
            holder.img_follow_timeline.setVisibility(GONE);
        }


        if (Pref_storage.getDetail(context, "userId").equals(getusertimelinelist.get(position).getUserId())) {
            holder.txt_timeline_follow.setVisibility(GONE);
            holder.img_follow_timeline.setVisibility(GONE);

        }


//User caption visiblity
        try {
            String selfuserfirstnamecomment = getusertimelinelist.get(position).getFirstName().replace("\"", "");
            String selfuserlastnamecomment = getusertimelinelist.get(position).getLastName().replace("\"", "");

            if (getusertimelinelist.get(position).getFirstName().isEmpty()) {
                holder.selfusername_caption.setVisibility(GONE);
                holder.selfuserlastname_caption.setVisibility(GONE);

            } else {
                        /*holder.selfusername_caption.setText(selfuserfirstnamecomment);
                        holder.selfuserlastname_caption.setText(selfuserlastnamecomment);
                        holder.selfusername_caption.setVisibility(VISIBLE);
                        holder.selfuserlastname_caption.setVisibility(VISIBLE);*/

                holder.caption.setText(getusertimelinelist.get(position).getTimelineDescription());
                holder.caption.setTextColor(Color.parseColor("#000000"));
                holder.caption.setVisibility(VISIBLE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        //Total posts visibility
        String post_count = getusertimelinelist.get(position).getTotalPosts();


        if (Integer.parseInt(post_count) == 0) {
            holder.count_post.setVisibility(GONE);
            holder.count.setVisibility(GONE);
            holder.comma.setVisibility(GONE);
        } else if (Integer.parseInt(post_count) == 1) {
            holder.count.setText(post_count);
            holder.count.setVisibility(VISIBLE);
            holder.count_post.setText(R.string.post);
            holder.count_post.setVisibility(VISIBLE);

        } else {

            holder.count.setText(post_count);
            holder.count.setVisibility(VISIBLE);
            holder.comma.setVisibility(VISIBLE);
            holder.count_post.setVisibility(VISIBLE);


            //1000 posts to 1K posts convertion method


                  /*  if (Math.abs(number / 1000000) > 1) {
                        numberString = (number / 1000000).toString() + "m";
                    }
                    else if (Math.abs(number / 1000) > 1) {
                        numberString = (number / 1000).toString() + "k";
                    }
                    else {
                        numberString = number.toString();
                    }
*/

        }


//Total followers visibility

        String followers_post = getusertimelinelist.get(position).getTotalFollowers();

        if (Integer.parseInt(followers_post) == 0) {
            holder.txt_followers.setVisibility(GONE);
            holder.comma.setVisibility(GONE);
            holder.txt_followers_count.setVisibility(GONE);
        } else if (Integer.parseInt(followers_post) == 1) {
            holder.txt_followers_count.setText(followers_post);
            holder.txt_followers.setText(R.string.follwer);
            holder.txt_followers.setVisibility(VISIBLE);
            holder.comma.setVisibility(VISIBLE);
            holder.txt_followers_count.setVisibility(VISIBLE);
        } else if (Integer.parseInt(followers_post) > 1) {
            holder.txt_followers_count.setText(followers_post);
            holder.txt_followers.setVisibility(VISIBLE);
            holder.comma.setVisibility(VISIBLE);
            holder.txt_followers.setText("Followers");
            holder.txt_followers_count.setVisibility(VISIBLE);
        }

//Comments visibility
        try {
            String comment = getusertimelinelist.get(position).getTotalComments();
            if (Integer.parseInt(comment) == 0 || comment == null) {

                holder.viewcomment.setVisibility(GONE);
                holder.commenttest.setVisibility(GONE);
            } else {
                holder.comment.setText(comment);
                if (Integer.parseInt(comment) == 1) {
                    holder.viewcomment.setText(R.string.view);
                    holder.commenttest.setText(R.string.commenttt);
                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setVisibility(VISIBLE);
                } else if (Integer.parseInt(comment) > 1) {
                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setVisibility(VISIBLE);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//Likes Visibility
               /* String likes = getusertimelinelist.get(position).getTotalLikes();
                if (Integer.parseInt(likes) == 0 || likes == null) {
                    holder.like.setVisibility(GONE);
                    holder.liketxt.setVisibility(GONE);

                } else if (Integer.parseInt(likes) == 1) {
                    holder.like.setText(likes);
                    holder.liketxt.setText(R.string.new_like);
                    holder.like.setVisibility(VISIBLE);
                    holder.liketxt.setVisibility(VISIBLE);

                } else {
                    holder.like.setText(likes);
                    holder.like.setVisibility(VISIBLE);
                    holder.liketxt.setVisibility(VISIBLE);


                }
*/
        //Displaying user name


        try {

            String username = getusertimelinelist.get(position).getFirstName() + " " + getusertimelinelist.get(position).getLastName();
            String hotelname = getusertimelinelist.get(position).getTimelineHotel();
            int taggedCount = getusertimelinelist.get(position).getWhomCount();

            String taggedPeopleText = "";
            String finalUserNameText = "";

            if (taggedCount != 0) {

                String taggedFirstName = "";
                String taggedSecondName = "";


                switch (taggedCount) {


                    case 1:

                        taggedFirstName = getusertimelinelist.get(position).getWhom().get(0).getFirstName() + " " + getusertimelinelist.get(position).getWhom().get(0).getLastName();
                        taggedPeopleText = taggedFirstName;

                        break;

                    case 2:

                        taggedFirstName = getusertimelinelist.get(position).getWhom().get(0).getFirstName() + " " + getusertimelinelist.get(position).getWhom().get(0).getLastName();
                        taggedSecondName = getusertimelinelist.get(position).getWhom().get(1).getFirstName() + " " + getusertimelinelist.get(position).getWhom().get(1).getLastName();
                        taggedPeopleText = taggedFirstName + " and " + taggedSecondName;


                        break;


                    default:

                        taggedFirstName = getusertimelinelist.get(position).getWhom().get(0).getFirstName() + " " + getusertimelinelist.get(position).getWhom().get(0).getLastName();
                        taggedPeopleText = taggedFirstName + " and " + String.valueOf(taggedCount - 1) + " Others";

                        break;

                }


            }

            if (taggedCount == 0) {

                finalUserNameText = username + " at " + hotelname;

                SpannableString ss = new SpannableString(finalUserNameText);

                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {

                        String user_id = getusertimelinelist.get(position).getUserId();
                        String timelineid = getusertimelinelist.get(position).getTimelineId();

//                        Pref_storage.setDetail(context, "clickeduser_id_timeline", user_id);
//                        Pref_storage.setDetail(context, "clickedtimelineid_for_user", timelineid);
                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLACK);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {


                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLACK);
                        ds.setUnderlineText(false);
                    }
                };

                ss.setSpan(clickableSpan1, 0, username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(clickableSpan2, username.length() + 4, finalUserNameText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                holder.username.setText(ss);
                holder.username.setMovementMethod(LinkMovementMethod.getInstance());


            } else {

                String isWith = " is with ";
                String at = " at ";

                finalUserNameText = username + isWith + taggedPeopleText + at + hotelname;

                SpannableString ss = new SpannableString(finalUserNameText);

                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {

                        String user_id = getusertimelinelist.get(position).getUserId();
                        String timelineid = getusertimelinelist.get(position).getTimelineId();

//                        Pref_storage.setDetail(context, "clickeduser_id_timeline", user_id);
//                        Pref_storage.setDetail(context, "clickedtimelineid_for_user", timelineid);

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLACK);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {

                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                        LayoutInflater inflater1 = LayoutInflater.from(context);
                        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.alert_show_tagged_people, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setTitle("");

                        recyler_tagged_people = (RecyclerView) dialogView.findViewById(R.id.recyler_tagged_people);

                        showTaggedPeopleAdapter = new ShowTaggedPeopleAdapter(context, getusertimelinelist.get(position).getWhom());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                        recyler_tagged_people.setLayoutManager(mLayoutManager);
                        recyler_tagged_people.setItemAnimator(new DefaultItemAnimator());
                        recyler_tagged_people.setAdapter(showTaggedPeopleAdapter);
                        recyler_tagged_people.hasFixedSize();

                        final AlertDialog dialog = dialogBuilder.create();
                        dialog.show();
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLACK);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan3 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {


                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLACK);
                        ds.setUnderlineText(false);
                    }
                };


                int first = 0, second = username.length();
                int third = username.length() + isWith.length(), fourth = username.length() + isWith.length() + taggedPeopleText.length();
                int fifth = username.length() + isWith.length() + taggedPeopleText.length() + at.length(), sixth = finalUserNameText.length();

                Log.e("TestingLength", "First :" + first + " Second :" + second);
                Log.e("TestingLength", "third :" + third + " fourth :" + fourth);
                Log.e("TestingLength", "fifth :" + fifth + " Second :" + sixth);

                ss.setSpan(clickableSpan1, first, second, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(clickableSpan2, third, fourth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(clickableSpan3, fifth, sixth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                Log.e(TAG, "onBindViewHolder: " + ss.toString());


                holder.username.setText(ss);
                holder.username.setMovementMethod(LinkMovementMethod.getInstance());

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


//Likes display by user API


// Timestamp for timeline
        String createdon = getusertimelinelist.get(position).getCreatedOn();

        try {
            long now = System.currentTimeMillis();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date convertedDate = dateFormat.parse(createdon);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now,

                    DateUtils.FORMAT_ABBREV_RELATIVE);
            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                holder.txt_created_on.setText(R.string.just_now);
            } else {
                holder.txt_created_on.append(relavetime1 + "\n\n");
                System.out.println(relavetime1);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

//Share Intent
        holder.img_shareadapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                context.startActivity(Intent.createChooser(i, "Share via"));

            }
        });

        //Comments activity intent

        holder.rl_comment_adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pref_storage.setDetail(context, "comment_timelineid", getusertimelinelist.get(position).getTimelineId());

                Intent intent = new Intent(context, Comments_activity.class);
                intent.putExtra("comment_timelineid", getusertimelinelist.get(position).getTimelineId());
                context.startActivity(intent);


            }
        });


        //User Like API

        totalLikes = getusertimelinelist.get(position).getTotalLikes();

        if(totalLikes == 0){

            holder.like.setVisibility(View.GONE);

        }else if(totalLikes == 1){

            holder.like.setVisibility(View.VISIBLE);
            holder.like.setText(String.valueOf(totalLikes)+" Like");

        }else{

            holder.like.setVisibility(View.VISIBLE);
            holder.like.setText(String.valueOf(totalLikes)+" Likes");
        }

        if (getusertimelinelist.get(position).getTlLikes().equals("0")) {

            getusertimelinelist.get(position).setLiked(false);
            holder.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));


        } else {

            getusertimelinelist.get(position).setLiked(true);
            holder.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));

        }

        holder.img_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);


                if (getusertimelinelist.get(position).isLiked()) {

                    holder.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                    getusertimelinelist.get(position).setLiked(false);

                    int total_likes = getusertimelinelist.get(position).getTotalLikes();

                    if (total_likes == 1) {

                        total_likes = total_likes - 1;
                        getusertimelinelist.get(position).setTotalLikes(total_likes);
//                                            holder.event_discussion_like_count.setText(String.valueOf(--totalLikes)+" Likes");
                        holder.like.setVisibility(View.GONE);


                    } else if (total_likes == 2) {

                        total_likes = total_likes - 1;
                        getusertimelinelist.get(position).setTotalLikes(total_likes);
                        holder.like.setText(String.valueOf(total_likes) + " Like");
                        holder.like.setVisibility(View.VISIBLE);

                    } else {

                        total_likes = total_likes - 1;
                        getusertimelinelist.get(position).setTotalLikes(total_likes);
                        holder.like.setText(String.valueOf(total_likes) + " Likes");
                        holder.like.setVisibility(View.VISIBLE);

                    }



                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                        int timelineId = Integer.parseInt(getusertimelinelist.get(position).getTimelineId());

                        Log.d(TAG, "onClick: "+timelineId);


                        Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", timelineId, 0, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("nodata")) {

                                        //Success
                                        notifyDataSetChanged();



                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                } else {

                    holder.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                    getusertimelinelist.get(position).setLiked(true);

                    int total_likes = getusertimelinelist.get(position).getTotalLikes();

                    if (total_likes == 0) {

                        total_likes = total_likes + 1;
                        getusertimelinelist.get(position).setTotalLikes(total_likes);
                        holder.like.setText(String.valueOf(total_likes) + " Like");
                        holder.like.setVisibility(View.VISIBLE);

                    } else {

                        total_likes = total_likes + 1;
                        getusertimelinelist.get(position).setTotalLikes(total_likes);
                        holder.like.setText(String.valueOf(total_likes) + " Likes");

                    }

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                        int timelineId = Integer.parseInt(getusertimelinelist.get(position).getTimelineId());

                        Log.d(TAG, "onClick: "+timelineId);

                        Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", timelineId, 1, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        //Success

                                        notifyDataSetChanged();


                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                }

                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }


            }
        });


        holder.edittxt_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (holder.edittxt_comment.getText().toString().length() != 0) {
                    holder.txt_adapter_post.setVisibility(VISIBLE);
                } else {
                    holder.txt_adapter_post.setVisibility(GONE);
                }


            }
        });

        holder.rl_post_timeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commenttimelineid = getusertimelinelist.get(position).getTimelineId();
                createcomment = holder.edittxt_comment.getText().toString();
                hideKeyboard(v);
                holder.edittxt_comment.setText("");
               /* try {
                    String username = Pref_storage.getDetail(context, "firstName");
                    String userlaname = Pref_storage.getDetail(context, "lastName");
                    holder.user_name_comment.setText(username.concat(" ").concat(userlaname));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                holder.user_name_comment.setVisibility(VISIBLE);*/


                int Cmnt = Integer.parseInt(getusertimelinelist.get(position).getTotalComments()) + 1;
                holder.comment.setText(String.valueOf(Cmnt));
                holder.comment.setVisibility(VISIBLE);
                if (Cmnt == 1) {
                    holder.viewcomment.setText(R.string.view);
                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setText(R.string.commenttt);
                    holder.commenttest.setVisibility(VISIBLE);
                } else if (Cmnt > 1) {
                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setVisibility(VISIBLE);
                }

       /*         holder.user_txt_comment.setText(createcomment);
                holder.user_txt_comment.setVisibility(VISIBLE);*/
                create_edit_timeline_comments();
            }
        });


        holder.rl_likes_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pref_storage.setDetail(context, "Likes_timelineid", getusertimelinelist.get(position).getTimelineId());
                Intent intent = new Intent(context, likesActivity.class);
                intent.putExtra("Likes_timelineid", getusertimelinelist.get(position).getTimelineId());
                context.startActivity(intent);
//                ((AppCompatActivity) context).finish();
            }
        });


        holder.rl_comments_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pref_storage.setDetail(context, "comment_timelineid", getusertimelinelist.get(position).getTimelineId());

                Intent intent = new Intent(context, Comments_activity.class);
                intent.putExtra("comment_timelineid", getusertimelinelist.get(position).getTimelineId());
                context.startActivity(intent);


//                ((AppCompatActivity) context).finish();
            }
        });

/*        holder.rl_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                send_user_like();

            }


        });*/


        holder.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }


        });

                /*holder.userlastname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = getusertimelinelist.get(position).getUserId();
                        String timelineid = getusertimelinelist.get(position).getTimelineId();
                        Pref_storage.setDetail(context, "clickeduser_id_timeline", user_id);

                        Pref_storage.setDetail(context, "clickedtimelineid_for_user", timelineid);
                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }


                    }


                });*/


        //user photo click

        holder.userphoto_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_id = getusertimelinelist.get(position).getUserId();
                String timelineid = getusertimelinelist.get(position).getTimelineId();

//                Pref_storage.setDetail(context, "clickeduser_id_timeline", user_id);
//                Pref_storage.setDetail(context, "clickedtimelineid_for_user", timelineid);
                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }


            }


        });

        holder.rl_follow_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_id = getusertimelinelist.get(position).getUserId();
                Pref_storage.setDetail(context, "clicked_timeline_user_id", user_id);

                if (Pref_storage.getDetail(context, "userId").equals(Pref_storage.getDetail(context, "clicked_timeline_user_id"))) {
                    holder.txt_timeline_follow.setVisibility(GONE);
                    holder.img_follow_timeline.setVisibility(GONE);
                } else {
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        String createdby = Pref_storage.getDetail(context, "userId");
                        String going_to_follow_user_id = Pref_storage.getDetail(context, "clicked_timeline_user_id");

                        Call<Get_following_output_Pojo> call = apiService.create_follower("create_follower", Integer.parseInt(createdby), Integer.parseInt(going_to_follow_user_id), 1);
                        call.enqueue(new Callback<Get_following_output_Pojo>() {
                            @Override
                            public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {

                                if (response.body().getResponseCode() == 1) {


                                    for (int j = 0; j < response.body().getData().size(); j++) {

                                        getFollowingProfilePojo = new Get_following_profile_pojo("", "", "", "",

                                                "", "", "", "", "", "", "", "", "", "", "", "",
                                                "", "", "", "");

                                        String followeruserID = response.body().getData().get(j).getUser_id();
                                        Pref_storage.setDetail(context, "followeruserID", followeruserID);

                                        String createdOn = response.body().getData().get(j).getCreated_on();
                                        String Total_followers = response.body().getData().get(j).getTotal_followers();
                                        String total_followings = response.body().getData().get(j).getTotal_followings();
                                        String userId = response.body().getData().get(j).getUser_id();
                                        String oauthProvider = response.body().getData().get(j).getOauth_provider();
                                        String oauthUid = response.body().getData().get(j).getOauth_uid();
                                        String firstName1 = response.body().getData().get(j).getFirst_name();
                                        String lastName1 = response.body().getData().get(j).getLast_name();
                                        String location = response.body().getData().get(j).getLatitude();
                                        String locationstate = response.body().getData().get(j).getLongitude();
                                        String email1 = response.body().getData().get(j).getEmail();
                                        String contNo = response.body().getData().get(j).getCont_no();
                                        String gender = response.body().getData().get(j).getGender();
                                        String dob = response.body().getData().get(j).getDob();
                                        String following_picture = response.body().getData().get(j).getPicture();

                                        holder.txt_timeline_follow.setText(R.string.txt_following);
                                        holder.img_follow_timeline.setImageResource(R.drawable.ic_followers);


                                    }


                                }

                            }

                            @Override
                            public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }
        });

        holder.selfusername_caption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_id = getusertimelinelist.get(position).getUserId();
                String timelineid = getusertimelinelist.get(position).getTimelineId();
//
//                Pref_storage.setDetail(context, "clickedtimelineid_for_user", timelineid);
//                Pref_storage.setDetail(context, "clickeduser_id_timeline", user_id);
                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {
                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }


            }


        });


        holder.selfuserlastname_caption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_id = getusertimelinelist.get(position).getUserId();
//                Pref_storage.setDetail(context, "clickeduser_id_timeline", user_id);

                String timelineid = getusertimelinelist.get(position).getTimelineId();

//                Pref_storage.setDetail(context, "clickedtimelineid_for_user", timelineid);
                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }


            }


        });

        holder.txt_followers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  CustomDialogClass2 ccd = new CustomDialogClass2(context);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccd.setCancelable(true);
                ccd.show();*/
            }
        });


        holder.img_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                String timelineid = getusertimelinelist.get(position).getTimelineId();
                String userid = getusertimelinelist.get(position).getUserId();
                Pref_storage.setDetail(context, "clicked_timelineID", timelineid);
                Pref_storage.setDetail(context, "clicked_timelineuserID", userid);
                Pref_storage.setDetail(context, "clicked_timelinePosition", String.valueOf(position));

                Single_user_timeline_adapter.CustomDialogClass ccd = new Single_user_timeline_adapter.CustomDialogClass(context);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccd.setCancelable(true);
                ccd.show();


            }


        });



    }


    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }

    //Create timeline comment API
    private void create_edit_timeline_comments() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {
            String createuserid = Pref_storage.getDetail(context, "userId");

            Call<CreateEditTimeline_Comments> call = apiService.create_edit_timeline_comments1("create_edit_timeline_comments", 0, Integer.parseInt(commenttimelineid), createcomment, Integer.parseInt(createuserid));
            call.enqueue(new Callback<CreateEditTimeline_Comments>() {
                @Override
                public void onResponse(Call<CreateEditTimeline_Comments> call, Response<CreateEditTimeline_Comments> response) {

                    if (response.body().getResponseCode() == 1) {


                        get_timeline_comments_all();


                    }

                }

                @Override
                public void onFailure(Call<CreateEditTimeline_Comments> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void get_timeline_comments_all() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {
            String createuserid = Pref_storage.getDetail(context, "userId");
            String comment_timelineid = Pref_storage.getDetail(context, "comment_timelineid");
//            Call<comments_2> call = apiService.get_timeline_comments_all("get_timeline_comments_user", Integer.parseInt(createuserid), 22);
            Call<comments_2> call = apiService.get_timeline_comments_all("get_timeline_comments_user", Integer.parseInt(comment_timelineid), Integer.parseInt(createuserid));
            call.enqueue(new Callback<comments_2>() {
                @Override
                public void onResponse(Call<comments_2> call, Response<comments_2> response) {

                    if (response.body().getResponseCode() == 1) {


                    }

                }

                @Override
                public void onFailure(Call<comments_2> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void initializeViews(List<String> dataModel, final Single_user_timeline_adapter.MyViewHolder holder, int position) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(dataModel);
        holder.mviewpager.setAdapter(adapter);
        holder.mviewpager.setClipToPadding(false);
        holder.mviewpager.setPadding(0, 0, 0, 0);
        if (position == 0) {
//            ((TimelineAdapter) holder).title.setText("Just for the weekend");
        }
        else{
//            ((TimelineAdapter)holder).title.setText("AirBnb Favorites");
        }
    }

    @Override
    public int getItemCount() {
        return getusertimelinelist.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    //View_Holder

    public class MyViewHolder extends RecyclerView.ViewHolder {


        PhotoView image;
        EditText edittxt_comment;
        ImageButton img_view_more,img_like;
        TextView username, location, location_state, like, liketxt, user_txt_comment, count, comma, count_post, txt_followers_count, txt_followers, selfusername_caption, selfuserlastname_caption, user_name_comment, viewcomment, txt_timeline_follow, comment, commenttest, share, caption, captiontag, txt_created_on, txt_adapter_post;
        RelativeLayout rl_likes_layout, rl_comments_lay,rl_username;
        RelativeLayout rl_share_adapter, rl_comment_adapter, like_rl,rl_more, rl_post_timeline, rl_follow_layout, rl_newlike_adapter;
        LinearLayout rl_row_layout,rl_fullimage;
        ImageView userphoto_profile, img_follow_timeline, img_commentadapter, img_shareadapter, userphoto_comment;



        public  int NUM_PAGES = 0;
        public  int currentPage = 0;
        public ViewPager mviewpager;


        private static final String TAG = "TimelineAdapter";


        public  CircleIndicator circleIndicatortimeline;


        public MyViewHolder(View itemView) {
            super(itemView);

//ViewPager
            mviewpager = (ViewPager) itemView.findViewById(R.id.mviewpager);


            //Edittext
            edittxt_comment = (EditText) itemView.findViewById(R.id.edittxt_comment);


            //Circle Indicator
            circleIndicatortimeline = (CircleIndicator) itemView.findViewById(R.id.indicator);


            //ImageView
            image = (PhotoView) itemView.findViewById(R.id.image);
            img_like = (ImageButton) itemView.findViewById(R.id.img_like);
            img_view_more = (ImageButton) itemView.findViewById(R.id.img_view_more);
            img_shareadapter = (ImageView) itemView.findViewById(R.id.img_shareadapter);
            userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);
            userphoto_comment = (ImageView) itemView.findViewById(R.id.userphoto_comment);
            img_commentadapter = (ImageView) itemView.findViewById(R.id.img_commentadapter);
            img_follow_timeline = (ImageView) itemView.findViewById(R.id.img_follow_timeline);


            like = (TextView) itemView.findViewById(R.id.like);
            comma = (TextView) itemView.findViewById(R.id.comma);
            count = (TextView) itemView.findViewById(R.id.count);
            share = (TextView) itemView.findViewById(R.id.shares);
            caption = (TextView) itemView.findViewById(R.id.caption);
            liketxt = (TextView) itemView.findViewById(R.id.liketxt);
            comment = (TextView) itemView.findViewById(R.id.comment);
            username = (TextView) itemView.findViewById(R.id.username);
            location = (TextView) itemView.findViewById(R.id.location);
            captiontag = (TextView) itemView.findViewById(R.id.captiontag);
            commenttest = (TextView) itemView.findViewById(R.id.commenttest);
            viewcomment = (TextView) itemView.findViewById(R.id.View_all_comment);
            txt_created_on = (TextView) itemView.findViewById(R.id.txt_created_on);
            txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);
            user_txt_comment = (TextView) itemView.findViewById(R.id.user_txt_comment);
            user_name_comment = (TextView) itemView.findViewById(R.id.user_name_comment);
            txt_timeline_follow = (TextView) itemView.findViewById(R.id.txt_timeline_follow);
            selfusername_caption = (TextView) itemView.findViewById(R.id.selfusername_caption);
            selfuserlastname_caption = (TextView) itemView.findViewById(R.id.selfuserlastname_caption);


            count_post = (TextView) itemView.findViewById(R.id.count_post);
            txt_followers = (TextView) itemView.findViewById(R.id.txt_followers);
            txt_followers_count = (TextView) itemView.findViewById(R.id.txt_followers_count);

            //RelativeLayout
            rl_more = (RelativeLayout) itemView.findViewById(R.id.rl_more);
            like_rl = (RelativeLayout) itemView.findViewById(R.id.like_rl);
            rl_username = (RelativeLayout) itemView.findViewById(R.id.rl_username);
            rl_fullimage = (LinearLayout) itemView.findViewById(R.id.rl_fullimage);
            rl_likes_layout = (RelativeLayout) itemView.findViewById(R.id.rl_likes_layout);
            rl_follow_layout = (RelativeLayout) itemView.findViewById(R.id.rl_follow_layout);
            rl_post_timeline = (RelativeLayout) itemView.findViewById(R.id.rl_post_timeline);
            rl_share_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_share_adapter);
            rl_row_layout = (LinearLayout) itemView.findViewById(R.id.layout_one_foradapter);
            rl_comments_lay = (RelativeLayout) itemView.findViewById(R.id.rl_comments_layout);
            rl_comment_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_comment_adapter);


        }
    }

    private void create_delete_timeline() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            String createdby = Pref_storage.getDetail(context, "userId");
            Call<CommonOutputPojo> call = apiService.create_delete_timeline("create_delete_timeline", Integer.parseInt(Pref_storage.getDetail(context, "clicked_timelineID")), Integer.parseInt(createdby));
            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    if (response.body() != null) {

                        Log.e("ResponseStatus", "ResponseStatus->" + response.body().getResponseMessage());

                        if (response.body().getResponseMessage().equals("nodata")) {
                            removeAt(Integer.parseInt(Pref_storage.getDetail(context, "clicked_timelinePosition")));


                            Toast.makeText(context, "Deleted!!", Toast.LENGTH_LONG).show();

//                            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//                                    .setTitleText("Deleted!!")
////                                    .setContentText("Your post has been deleted.")
//                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                        @Override
//                                        public void onClick(SweetAlertDialog sDialog) {
//                                            sDialog.dismissWithAnimation();
//                                           /* context.startActivity(new Intent(context, Home.class));
//                                            ((AppCompatActivity) context).finish();*/
//
//                                        }
//                                    })
//                                    .show();


                        }

                    }


                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void removeAt(int position) {
        getusertimelinelist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getusertimelinelist.size());
    }


    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity c;
        public Dialog d;
        public AppCompatButton btn_Ok;
        TextView txt_deletepost, txt_report, txt_copylink, txt_turnoffpost, txt_sharevia, txt_editpost;

        CustomDialogClass(Context a) {
            super(a);
            // TODO Auto-generated constructor stub
            context = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
//            setContentView(R.layout.dialog_version_info);

            setContentView(R.layout.custom_dialog_timeline);

            txt_deletepost = (TextView) findViewById(R.id.txt_deletepost);
            txt_editpost = (TextView) findViewById(R.id.txt_editpost);
            txt_report = (TextView) findViewById(R.id.txt_report);
            txt_copylink = (TextView) findViewById(R.id.txt_copylink);
            txt_turnoffpost = (TextView) findViewById(R.id.txt_turnoffpost);
            txt_sharevia = (TextView) findViewById(R.id.txt_sharevia);

            txt_deletepost.setOnClickListener(this);
            txt_editpost.setOnClickListener(this);
            txt_report.setOnClickListener(this);
            txt_copylink.setOnClickListener(this);
            txt_turnoffpost.setOnClickListener(this);
            txt_sharevia.setOnClickListener(this);
            if (Pref_storage.getDetail(context, "clicked_timelineuserID").equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                txt_deletepost.setVisibility(VISIBLE);
                txt_editpost.setVisibility(VISIBLE);
            } else {
                txt_deletepost.setVisibility(GONE);
                txt_editpost.setVisibility(GONE);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_editpost:
                    Pref_storage.getDetail(context, "clicked_timelineID");
                    context.startActivity(new Intent(context, EditHistoryMap.class));
                    dismiss();
                    break;
                case R.id.txt_deletepost:

                    Pref_storage.getDetail(context, "clicked_timelineID");

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to delete this post?")
                            .setContentText("Won't be able to recover this post anymore!")
                            .setConfirmText("Yes,delete it!")
                            .setCancelText("No,cancel!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    create_delete_timeline();

                                }
                            })
                            .show();
                    dismiss();
                    break;
                case R.id.txt_report:
                    dismiss();
                    break;
                case R.id.txt_copylink:
                    Toast.makeText(context, "Link has been copied to clipboard", Toast.LENGTH_SHORT).show();
                    dismiss();
                    break;
                case R.id.txt_turnoffpost:
                    dismiss();
                    break;
                case R.id.txt_sharevia:
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                    i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                    context.startActivity(Intent.createChooser(i, "Share via"));
                    dismiss();
                    break;

                default:
                    break;
            }
        }
    }

}

