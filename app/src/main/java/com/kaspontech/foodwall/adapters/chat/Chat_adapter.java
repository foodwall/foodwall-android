package com.kaspontech.foodwall.adapters.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.chatpackage.chatGroupInsideActivity;
import com.kaspontech.foodwall.chatpackage.chatGroupUserProfileActivity;
import com.kaspontech.foodwall.chatpackage.chatInsideActivity;
import com.kaspontech.foodwall.chatpackage.chatUserProfileActivity;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_history_details_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.github.curioustechizen.ago.RelativeTimeTextView;

//

public class Chat_adapter extends RecyclerView.Adapter<Chat_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener, Filterable {

    /**
     * Context
     **/

    private Context context;

    /**
     * Chat activity class interface
     **/


    ChatActivity chatActivity;
    /**
     * Array list for chat history details
     **/

    public static ArrayList<Get_chat_history_details_pojo> getChatHistoryDetailsPojoArrayList = new ArrayList<>();

    private ArrayList<Get_chat_history_details_pojo> getChatHistoryDetailsPojoArrayListFiltered = new ArrayList<>();

    /**
     * Chat history details pojo
     **/
    Get_chat_history_details_pojo getChatHistoryDetailsPojo;

    /**
     * Int variable as history id
     **/
    int histId;

    int selectedposition=-1;


    public Chat_adapter(Context c, ArrayList<Get_chat_history_details_pojo> gettinglist) {
        this.context = c;
        this.getChatHistoryDetailsPojoArrayList = gettinglist;
        this.getChatHistoryDetailsPojoArrayListFiltered = gettinglist;
        chatActivity = (ChatActivity) context;
    }


    @NonNull
    @Override
    public Chat_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_chat, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Chat_adapter.MyViewHolder vh = new Chat_adapter.MyViewHolder(v, chatActivity);
//        setHasStableIds(true);
//        vh.setIsRecyclable(false);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Chat_adapter.MyViewHolder holder, final int position) {

        getChatHistoryDetailsPojo = getChatHistoryDetailsPojoArrayList.get(position);
        //User name
        if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getGroupid()) != 0) {

            holder.txt_usernamechat.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getGroupName()));

        } else if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getGroupid()) == 0) {

            holder.txt_usernamechat.setText(getChatHistoryDetailsPojoArrayList.get(position).getToFirstname());
            holder.txt_userlastnamechat.setText(getChatHistoryDetailsPojoArrayList.get(position).getToLastname());


        }

        //user comment
        int online = Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getToOnlineStatus());
        if (online == 0) {


            String message = org.apache.commons.text.StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getLastmessage());
            if (message.contains("changed group name")) {
                holder.txt_last_message.setText((getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group name."))));
            } else if (message.contains("icon")) {
                holder.txt_last_message.setText((getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group icon."))));
            } else if (message.contains("both")) {
                holder.txt_last_message.setText((getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group name and icon."))));
            } else if (message.contains("eft")) {
                if (message.contains(Pref_storage.getDetail(context, "userId"))) {
                    holder.txt_last_message.setText("You left the group");
                } else {
                    holder.txt_last_message.setText((getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" left from group."))));
                }

            } else if (message.equalsIgnoreCase("0")) {
                holder.txt_last_message.setText("Tap to chat");
            } else {
                holder.txt_last_message.setText(message);
            }

        } else if (online == 1) {
            String message = org.apache.commons.text.StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getLastmessage());
            if (message.contains("changed group name")) {
                holder.txt_last_message.setText((getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group name."))));
            } else if (message.contains("icon")) {
                holder.txt_last_message.setText((getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group icon."))));
            } else if (message.contains("both")) {
                holder.txt_last_message.setText((getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" changed group name and icon."))));
            } else if (message.contains("eft")) {
                if (message.contains(Pref_storage.getDetail(context, "userId"))) {
                    holder.txt_last_message.setText("You left the group");
                } else {
                    holder.txt_last_message.setText((getChatHistoryDetailsPojoArrayList.get(position).getFromFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getFromLastname().concat(" left from group."))));
                }

            } else if (message.equalsIgnoreCase("0")) {
                holder.txt_last_message.setText("Tap to chat");
            } else {
                holder.txt_last_message.setText(message);
            }


//            holder.txt_last_message.setText(R.string.active_now);
          //  holder.img_online.setVisibility(View.VISIBLE);
        }
//        holder.txt_last_message.setText(getChatHistoryDetailsPojoArrayList.get(position).getLastmessage());

        //User picture
        if (getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {
          /*  if(getChatHistoryDetailsPojoArrayList.get(position).getToPicture()==null||getChatHistoryDetailsPojoArrayList.get(position).getToPicture().equalsIgnoreCase("0")){
                holder.img_chat.setImageResource(R.drawable.ic_add_photo);
            }else {
                GlideApp.with(context)
                        .load(getChatHistoryDetailsPojoArrayList.get(position).getToPicture())
                        .placeholder(R.drawable.ic_group_inside)
                        .centerInside()
                        .into(holder.img_chat);
            }*/


            GlideApp.with(context)
                    .load(getChatHistoryDetailsPojoArrayList.get(position).getToPicture())
                    .placeholder(R.drawable.ic_group_inside)
                    .centerInside()
                    .into(holder.img_chat);

//            Picasso.get()
//                    .load(getChatHistoryDetailsPojoArrayList.get(position).getToPicture())
//                    .placeholder(R.drawable.ic_add_photo)
//                    .fit().centerInside()
//                    .into(holder.img_chat);
        } else if (!getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {

          /*  if(getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon().equalsIgnoreCase("0")||getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon()==null){
                holder.img_chat.setImageResource(R.drawable.ic_group_inside);
            }
            else {
                GlideApp.with(context)
                        .load(getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon())
                        .placeholder(R.drawable.ic_group_inside)
                        .centerInside().into(holder.img_chat);
            }*/


            GlideApp.with(context)
                    .load(getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon())
                    .placeholder(R.drawable.ic_group_inside)
                    .centerInside().into(holder.img_chat);


//            Picasso.get()
//                    .load(getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon())
//                    .placeholder(R.drawable.ic_group_inside)
//                    .fit()
//                    .centerInside().into(holder.img_chat);
        }


        //On position click listener

        holder.rl_chat_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {

                    Intent intent2 = new Intent(context, chatInsideActivity.class);
                    intent2.putExtra("created_by", getChatHistoryDetailsPojoArrayList.get(position).getFriendid());
                    intent2.putExtra("redirect_url", getChatHistoryDetailsPojoArrayList.get(position).getRedirect_url());
                    intent2.putExtra("userid", getChatHistoryDetailsPojoArrayList.get(position).getUserid());
                    intent2.putExtra("groupid", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                    intent2.putExtra("group_name", getChatHistoryDetailsPojoArrayList.get(position).getGroupName());
                    intent2.putExtra("pro_sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                    intent2.putExtra("group_icon", getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon());
                    intent2.putExtra("group_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                    intent2.putExtra("single_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                    intent2.putExtra("single_createddate", getChatHistoryDetailsPojoArrayList.get(position).getCreatedDate());
                    intent2.putExtra("username", getChatHistoryDetailsPojoArrayList.get(position).getToFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getToLastname()));
                    intent2.putExtra("friendid", getChatHistoryDetailsPojoArrayList.get(position).getFriendid());
                    intent2.putExtra("sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                    intent2.putExtra("picture", getChatHistoryDetailsPojoArrayList.get(position).getFromPicture());
                    intent2.putExtra("topicture", getChatHistoryDetailsPojoArrayList.get(position).getToPicture());
                    intent2.putExtra("active_status", getChatHistoryDetailsPojoArrayList.get(position).getToOnlineStatus());

                    context.startActivity(intent2);

//                            ((AppCompatActivity) context).overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
//                    ((AppCompatActivity) context).finish();

                } else if (!getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {

                    Intent intent3 = new Intent(context, chatGroupInsideActivity.class);

                    intent3.putExtra("redirect_url", getChatHistoryDetailsPojoArrayList.get(position).getRedirect_url());

                    intent3.putExtra("created_by", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                    intent3.putExtra("userid", getChatHistoryDetailsPojoArrayList.get(position).getUserid());
                    intent3.putExtra("groupid", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                    intent3.putExtra("group_name", getChatHistoryDetailsPojoArrayList.get(position).getGroupName());
                    intent3.putExtra("group_icon", getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon());
                    intent3.putExtra("group_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                    intent3.putExtra("username", getChatHistoryDetailsPojoArrayList.get(position).getToFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getToLastname()));

                    intent3.putExtra("friendid", getChatHistoryDetailsPojoArrayList.get(position).getFriendid());
                    intent3.putExtra("pro_friendid", getChatHistoryDetailsPojoArrayList.get(position).getFriendid());
                    intent3.putExtra("sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                    intent3.putExtra("picture", getChatHistoryDetailsPojoArrayList.get(position).getFromPicture());
                    intent3.putExtra("topicture", getChatHistoryDetailsPojoArrayList.get(position).getToPicture());
                    intent3.putExtra("active_status", getChatHistoryDetailsPojoArrayList.get(position).getToOnlineStatus());
                    intent3.putExtra("pro_username", getChatHistoryDetailsPojoArrayList.get(position).getGroupName());
                    intent3.putExtra("pro_groupid", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                    intent3.putExtra("pro_sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                    intent3.putExtra("pro_picture", getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon());
                    intent3.putExtra("pro_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                    intent3.putExtra("pro_createdby", getChatHistoryDetailsPojoArrayList.get(position).getCreatedDate());
                    intent3.putExtra("pro_createdbyname", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedLastname()));
                    context.startActivity(intent3);
//                            ((AppCompatActivity) context).overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
//                    ((AppCompatActivity) context).finish();

                }


            }
        });


        //Image_chat_user_popup


        holder.img_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View popupView2 = LayoutInflater.from(context).inflate(R.layout.chat_user_popup, null, false);
                final PopupWindow popupWindow2 = new PopupWindow(popupView2, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, true);
                popupWindow2.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                popupWindow2.setOutsideTouchable(true);
                popupWindow2.setAnimationStyle(android.R.style.Animation_Dialog);
//                popupWindow2.setAnimationStyle(R.style.PauseDialogAnimation);
                popupWindow2.showAtLocation(popupView2, Gravity.CENTER, 0, 0);
                dimBehind(popupWindow2);


                /*Widgets Initialization*/
                EmojiconTextView txtChatUsrname;
                ImageView imgChatProfile;
                ImageButton imgChatClick;
                ImageButton imgInfo;

                txtChatUsrname = popupView2.findViewById(R.id.txt_chat_usrname);
                imgChatProfile = popupView2.findViewById(R.id.img_chat_profile);
                imgChatClick = popupView2.findViewById(R.id.img_chat_click);
                imgInfo = popupView2.findViewById(R.id.img_info);

                if (getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {

                    txtChatUsrname.setText(getChatHistoryDetailsPojoArrayList.get(position).getToFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getToLastname()));

                    GlideApp.with(context).load(getChatHistoryDetailsPojoArrayList.get(position).getToPicture()).thumbnail(0.01f).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_add_photo).into(imgChatProfile);


                } else if (!getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {

                    txtChatUsrname.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getChatHistoryDetailsPojoArrayList.get(position).getGroupName()));

                    GlideApp.with(context).load(getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon()).thumbnail(0.01f).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_group_inside).into(imgChatProfile);

                }

                /*Chat click listener*/

                imgChatClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        popupWindow2.dismiss();

                        if (getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {

                            Intent intent = new Intent(context, chatInsideActivity.class);
                            intent.putExtra("created_by", getChatHistoryDetailsPojoArrayList.get(position).getFriendid());
                            intent.putExtra("redirect_url", getChatHistoryDetailsPojoArrayList.get(position).getRedirect_url());
                            intent.putExtra("userid", getChatHistoryDetailsPojoArrayList.get(position).getUserid());
                            intent.putExtra("groupid", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                            intent.putExtra("group_name", getChatHistoryDetailsPojoArrayList.get(position).getGroupName());
                            intent.putExtra("group_icon", getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon());
                            intent.putExtra("group_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                            intent.putExtra("group_createdby", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedby());
                            intent.putExtra("group_createdbyname", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedLastname()));
                            intent.putExtra("single_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                            intent.putExtra("single_createddate", getChatHistoryDetailsPojoArrayList.get(position).getCreatedDate());
                            intent.putExtra("username", getChatHistoryDetailsPojoArrayList.get(position).getToFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getToLastname()));

                            intent.putExtra("friendid", getChatHistoryDetailsPojoArrayList.get(position).getFriendid());
                            intent.putExtra("sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                            intent.putExtra("picture", getChatHistoryDetailsPojoArrayList.get(position).getFromPicture());
                            intent.putExtra("topicture", getChatHistoryDetailsPojoArrayList.get(position).getToPicture());
                            intent.putExtra("active_status", getChatHistoryDetailsPojoArrayList.get(position).getToOnlineStatus());

                            intent.putExtra("pro_username", getChatHistoryDetailsPojoArrayList.get(position).getGroupName());
                            intent.putExtra("pro_groupid", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                            intent.putExtra("pro_picture", getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon());
                            intent.putExtra("pro_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                            intent.putExtra("pro_sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                            intent.putExtra("pro_createdby", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedby());
                            intent.putExtra("pro_createdbyname", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedLastname()));

                            context.startActivity(intent);
                            /*((AppCompatActivity) context).overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);*/
                            /* ((AppCompatActivity) context).finish();*/

                        } else if (!getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {

                            Intent intent = new Intent(context, chatGroupInsideActivity.class);
                            intent.putExtra("created_by", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                            intent.putExtra("redirect_url", getChatHistoryDetailsPojoArrayList.get(position).getRedirect_url());
                            intent.putExtra("userid", getChatHistoryDetailsPojoArrayList.get(position).getUserid());
                            intent.putExtra("groupid", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                            intent.putExtra("group_name", getChatHistoryDetailsPojoArrayList.get(position).getGroupName());
                            intent.putExtra("group_icon", getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon());
                            intent.putExtra("group_createdbyname", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedLastname()));
                            intent.putExtra("group_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                            intent.putExtra("group_createdby", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedby());
                            intent.putExtra("username", getChatHistoryDetailsPojoArrayList.get(position).getToFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getToLastname()));

                            intent.putExtra("friendid", getChatHistoryDetailsPojoArrayList.get(position).getFriendid());
                            intent.putExtra("sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                            intent.putExtra("picture", getChatHistoryDetailsPojoArrayList.get(position).getFromPicture());
                            intent.putExtra("topicture", getChatHistoryDetailsPojoArrayList.get(position).getToPicture());
                            intent.putExtra("active_status", getChatHistoryDetailsPojoArrayList.get(position).getToOnlineStatus());

                            intent.putExtra("pro_username", getChatHistoryDetailsPojoArrayList.get(position).getGroupName());
                            intent.putExtra("pro_groupid", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                            intent.putExtra("pro_picture", getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon());
                            intent.putExtra("pro_sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                            intent.putExtra("pro_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                            intent.putExtra("pro_createdby", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedby());
                            intent.putExtra("pro_createdbyname", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedLastname()));

                            context.startActivity(intent);
                            /*((AppCompatActivity) context).overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);*/
                            /* ((AppCompatActivity) context).finish();*/

                        }

                    }
                });

                /*Image info click listner*/
                imgInfo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popupWindow2.dismiss();

                        if (getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {
                            Intent intent = new Intent(context, chatUserProfileActivity.class);
                            intent.putExtra("pro_username", getChatHistoryDetailsPojoArrayList.get(position).getToFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getToLastname()));
                            intent.putExtra("pro_picture", getChatHistoryDetailsPojoArrayList.get(position).getToPicture());
                            intent.putExtra("pro_sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                            intent.putExtra("pro_groupid", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                            intent.putExtra("pro_friendid", getChatHistoryDetailsPojoArrayList.get(position).getFriendid());
                            intent.putExtra("pro_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                            intent.putExtra("pro_createdby", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedby());
                            intent.putExtra("pro_createdbyname", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedLastname()));
                            context.startActivity(intent);
                        } else if (!getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {
                            Intent intent = new Intent(context, chatGroupUserProfileActivity.class);
                            intent.putExtra("pro_username", getChatHistoryDetailsPojoArrayList.get(position).getGroupName());
                            intent.putExtra("pro_groupid", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                            intent.putExtra("pro_picture", getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon());
                            intent.putExtra("pro_sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                            intent.putExtra("pro_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                            intent.putExtra("pro_createdby", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedby());
                            intent.putExtra("pro_createdbyname", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedLastname()));
                            context.startActivity(intent);
                        }

                    }
                });

                /*Image chat profile click listener*/

                imgChatProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popupWindow2.dismiss();

                        if (getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {
                            Intent intent = new Intent(context, chatUserProfileActivity.class);
                            intent.putExtra("pro_username", getChatHistoryDetailsPojoArrayList.get(position).getToFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getToLastname()));
                            intent.putExtra("pro_picture", getChatHistoryDetailsPojoArrayList.get(position).getToPicture());
                            intent.putExtra("pro_sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                            intent.putExtra("pro_groupid", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                            intent.putExtra("pro_friendid", getChatHistoryDetailsPojoArrayList.get(position).getFriendid());
                            intent.putExtra("pro_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                            intent.putExtra("pro_createdby", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedby());
                            intent.putExtra("pro_createdbyname", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedLastname()));
                            context.startActivity(intent);
                        } else if (!getChatHistoryDetailsPojoArrayList.get(position).getGroupid().equalsIgnoreCase("0")) {
                            Intent intent = new Intent(context, chatGroupUserProfileActivity.class);
                            intent.putExtra("pro_username", getChatHistoryDetailsPojoArrayList.get(position).getGroupName());
                            intent.putExtra("pro_groupid", getChatHistoryDetailsPojoArrayList.get(position).getGroupid());
                            intent.putExtra("pro_picture", getChatHistoryDetailsPojoArrayList.get(position).getGroup_icon());
                            intent.putExtra("pro_sessionid", getChatHistoryDetailsPojoArrayList.get(position).getSessionid());
                            intent.putExtra("pro_createdon", getChatHistoryDetailsPojoArrayList.get(position).getCreatedOn());
                            intent.putExtra("pro_createdby", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedby());
                            intent.putExtra("pro_createdbyname", getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedFirstname().concat(" ").concat(getChatHistoryDetailsPojoArrayList.get(position).getGroupCreatedLastname()));
                            context.startActivity(intent);
                        }

                    }
                });

            }
        });





        /* Ago time stamp */

        String createdOn = getChatHistoryDetailsPojoArrayList.get(position).getLastseen();

       // Utility.setTimeStamp(createdOn, holder.txt_ago);



        /*Chat overall layout click listener*/
/*
        holder.rl_chat_layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                 histId = Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getSessionid());

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this chat history?")
                        .setContentText("Won't be able to recover this post anymore!")
                        .setConfirmText("Yes,delete it!")
                        .setCancelText("No,cancel!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                if(Utility.isConnected(context)){
                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                    try {

                                        String createdby = Pref_storage.getDetail(context,"userId");
                                        Call<CreateUserPojoOutput> call = apiService.update_delete_chat_history( "update_delete_chat_history",Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getSessionid()),Integer.parseInt(createdby));
                                        call.enqueue(new Callback<CreateUserPojoOutput>() {
                                            @Override
                                            public void onResponse(Call<CreateUserPojoOutput>call, Response<CreateUserPojoOutput> response) {

                                                if (response.body() != null && response.body().getResponseCode() == 1) {
                                                    */
        /*Postition removed*//*

                                                    removeat(position);

                                                    Toast.makeText(context, "Deleted!!", Toast.LENGTH_LONG).show();

//                                                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//                                                            .setTitleText("Deleted!!")
//                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                                                @Override
//                                                                public void onClick(SweetAlertDialog sDialog) {
//                                                                    sDialog.dismissWithAnimation();
//
//
//                                                                }
//                                                            })
//                                                            .show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                                //Error
                                                Log.e("FailureError",""+t.getMessage());
                                            }
                                        });
                                    }catch (Exception e ){
                                        e.printStackTrace();
                                    }
                                }else{

                                    Toast.makeText(context, "No internet connection!!", Toast.LENGTH_SHORT).show();
                                }



                            }
                        })
                        .show();



                return false;
            }
        });
*/


        //swipe delete
        holder.ll_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                histId = Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getSessionid());

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this chat history?")
                        .setContentText("Won't be able to recover this post anymore!")
                        .setConfirmText("Yes,delete it!")
                        .setCancelText("No,cancel!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                if (Utility.isConnected(context)) {
                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                    try {

                                        String createdby = Pref_storage.getDetail(context, "userId");
                                        Call<CreateUserPojoOutput> call = apiService.update_delete_chat_history("update_delete_chat_history", Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(position).getSessionid()), Integer.parseInt(createdby));
                                        call.enqueue(new Callback<CreateUserPojoOutput>() {
                                            @Override
                                            public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                                                if (response.body() != null && response.body().getResponseCode() == 1) {
                                                    /*Postition removed*/
                                                    removeat(position);

                                                    Toast.makeText(context, "Deleted!!", Toast.LENGTH_LONG).show();

//                                                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//                                                            .setTitleText("Deleted!!")
//                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                                                @Override
//                                                                public void onClick(SweetAlertDialog sDialog) {
//                                                                    sDialog.dismissWithAnimation();
//
//
//                                                                }
//                                                            })
//                                                            .show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                                                //Error
                                                Log.e("FailureError", "" + t.getMessage());
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {

                                    Toast.makeText(context, "No internet connection!!", Toast.LENGTH_SHORT).show();
                                }


                            }
                        })
                        .show();
            }
        });


    }
    /*Postition removed*/

    private void removeat(int position) {

        getChatHistoryDetailsPojoArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getChatHistoryDetailsPojoArrayList.size());

    }


    /*Dimming behind the pop up window*/
    public static void dimBehind(PopupWindow popupWindow) {
        View container;
        if (popupWindow.getBackground() == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                container = (View) popupWindow.getContentView().getParent();
            } else {
                container = popupWindow.getContentView();
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                container = (View) popupWindow.getContentView().getParent().getParent();
            } else {
                container = (View) popupWindow.getContentView().getParent();
            }
        }
        Context context = popupWindow.getContentView().getContext();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
        p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        p.dimAmount = 0.3f;
        wm.updateViewLayout(container, p);
    }

    @Override
    public int getItemCount() {

        if (getChatHistoryDetailsPojoArrayList.size() == 0) {

            chatActivity.sizecheck(getChatHistoryDetailsPojoArrayList.size());
//            ll_nodata.setVisibility(View.VISIBLE);
        } else {
            chatActivity.sizecheck(getChatHistoryDetailsPojoArrayList.size());
//            ll_nodata.setVisibility(View.GONE);
        }
        return getChatHistoryDetailsPojoArrayList.size();
    }


    public static String parseDate(String timeAtMiliseconds) {


        if (timeAtMiliseconds.equalsIgnoreCase("")) {
            return "";
        }
        //API.log("Day Ago "+dayago);
        String result = "Just now";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            String todayDate = formatter.format(new Date());
            Calendar calendar = Calendar.getInstance();

            long dayagolong = Long.valueOf(timeAtMiliseconds) * 1000;
            calendar.setTimeInMillis(dayagolong);
            String agoformater = formatter.format(calendar.getTime());


            Date CurrentDate = null;
            Date CreateDate = null;

            try {
                CurrentDate = formatter.parse(todayDate);
                CreateDate = formatter.parse(agoformater);

                long different = Math.abs(CurrentDate.getTime() - CreateDate.getTime());

                long secondsInMilli = 1000;
                long minutesInMilli = secondsInMilli * 60;
                long hoursInMilli = minutesInMilli * 60;
                long daysInMilli = hoursInMilli * 24;

                long elapsedDays = different / daysInMilli;
                different = different % daysInMilli;

                long elapsedHours = different / hoursInMilli;
                different = different % hoursInMilli;

                long elapsedMinutes = different / minutesInMilli;
                different = different % minutesInMilli;

                long elapsedSeconds = different / secondsInMilli;

                different = different % secondsInMilli;
                if (elapsedDays == 0) {
                    if (elapsedHours == 0) {
                        if (elapsedMinutes == 0) {
                            if (elapsedSeconds < 0) {
                                return "0" + " s";
                            } else {
                                if (elapsedDays > 0 && elapsedSeconds < 59) {
                                    return "now";
                                }
                            }
                        } else {
                            return String.valueOf(elapsedMinutes) + "m ago";
                        }
                    } else {
                        return String.valueOf(elapsedHours) + "h ago";
                    }

                } else {
                    if (elapsedDays <= 29) {
                        return String.valueOf(elapsedDays) + "d ago";
                    }
                    if (elapsedDays > 29 && elapsedDays <= 58) {
                        return "1Mth ago";
                    }
                    if (elapsedDays > 58 && elapsedDays <= 87) {
                        return "2Mth ago";
                    }
                    if (elapsedDays > 87 && elapsedDays <= 116) {
                        return "3Mth ago";
                    }
                    if (elapsedDays > 116 && elapsedDays <= 145) {
                        return "4Mth ago";
                    }
                    if (elapsedDays > 145 && elapsedDays <= 174) {
                        return "5Mth ago";
                    }
                    if (elapsedDays > 174 && elapsedDays <= 203) {
                        return "6Mth ago";
                    }
                    if (elapsedDays > 203 && elapsedDays <= 232) {
                        return "7Mth ago";
                    }
                    if (elapsedDays > 232 && elapsedDays <= 261) {
                        return "8Mth ago";
                    }
                    if (elapsedDays > 261 && elapsedDays <= 290) {
                        return "9Mth ago";
                    }
                    if (elapsedDays > 290 && elapsedDays <= 319) {
                        return "10Mth ago";
                    }
                    if (elapsedDays > 319 && elapsedDays <= 348) {
                        return "11Mth ago";
                    }
                    if (elapsedDays > 348 && elapsedDays <= 360) {
                        return "12Mth ago";
                    }

                    if (elapsedDays > 360 && elapsedDays <= 720) {
                        return "1 year ago";
                    }

                    if (elapsedDays > 720) {
                        SimpleDateFormat formatterYear = new SimpleDateFormat("MM/dd/yyyy");
                        Calendar calendarYear = Calendar.getInstance();
                        calendarYear.setTimeInMillis(dayagolong);
                        return formatterYear.format(calendarYear.getTime()) + "";
                    }

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    public static long getDateInMillis(String srcDate) {

        SimpleDateFormat desiredFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        long dateInMillis = 0;

        try {

            Date date = desiredFormat.parse(srcDate);

            dateInMillis = date.getTime();

            return dateInMillis;

        } catch (ParseException e) {

            e.printStackTrace();

        }

        return 0;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    // Filter option
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                Log.e("ChathistoryLog", "charString-->" + charString);


                if (charString.isEmpty()) {

                    getChatHistoryDetailsPojoArrayListFiltered = getChatHistoryDetailsPojoArrayList;

                } else {

                    ArrayList<Get_chat_history_details_pojo> filteredList = new ArrayList<>();

                    for (Get_chat_history_details_pojo row : getChatHistoryDetailsPojoArrayList) {


                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getToFirstname().toLowerCase().contains(charString.toLowerCase()) || row.getToLastname().toLowerCase().contains(charSequence) || row.getGroupName().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);


                        } else {
                            Log.e("ChathistoryLog", "performFiltering: Not matched");
                        }
                    }

                    getChatHistoryDetailsPojoArrayList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = getChatHistoryDetailsPojoArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                getChatHistoryDetailsPojoArrayList = (ArrayList<Get_chat_history_details_pojo>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_ago;

        EmojiconTextView txt_last_message;
        EmojiconTextView txt_usernamechat;
        EmojiconTextView txt_userlastnamechat;
        RelativeLayout rl_chat_layout;

        ImageView img_chat;
        ImageView img_online;

        LinearLayout ll_delete;

        ChatActivity chatActivity;


        public MyViewHolder(View itemView, ChatActivity chatActivity1) {
            super(itemView);

            this.chatActivity = chatActivity1;

            txt_usernamechat = (EmojiconTextView) itemView.findViewById(R.id.txt_usernamechat);
            txt_userlastnamechat = (EmojiconTextView) itemView.findViewById(R.id.txt_userlastnamechat);
            txt_ago = (TextView) itemView.findViewById(R.id.txt_ago);
//            txt_ago = (RelativeTimeTextView) itemView.findViewById(R.id.txt_ago);
            txt_last_message = (EmojiconTextView) itemView.findViewById(R.id.txt_last_message);
            img_chat = (ImageView) itemView.findViewById(R.id.img_chat);
            img_online = (ImageView) itemView.findViewById(R.id.img_online);
            rl_chat_layout = (RelativeLayout) itemView.findViewById(R.id.rl_chat_layout);
            ll_delete = (LinearLayout) itemView.findViewById(R.id.ll_delete);


        }


    }


}
