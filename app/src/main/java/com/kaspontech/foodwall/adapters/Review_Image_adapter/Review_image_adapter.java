package com.kaspontech.foodwall.adapters.Review_Image_adapter;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.kaspontech.foodwall.R;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

import static com.kaspontech.foodwall.reviewpackage.GalleryImage_Review.imagesEncodedList;

public class Review_image_adapter extends RecyclerView.Adapter<Review_image_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener{
    private Context context;
    private static ViewPager review_viewpager;
    private static int review_currentPage = 0;
    private static int review_NUM_PAGES = 0;
    private CircleIndicator circleIndicator;

    public static List<String> get_review_imagelist = new ArrayList<>();

    public Review_image_adapter(Context c, List<String> gettingimage) {
        this.context = c;

        get_review_imagelist = gettingimage;

    }

    @NonNull
    @Override
    public Review_image_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_write_review, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Review_image_adapter.MyViewHolder vh = new Review_image_adapter.MyViewHolder(v);



        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull final Review_image_adapter.MyViewHolder holder, final int position) {

        init();

    }

    @Override
    public int getItemCount() {
        return imagesEncodedList.size();
    }


    //View Pager

    private void init() {
//        for(int i=0;i<IMAGES.length;i++)
//            ImagesArray.add(IMAGES[i]);
        review_viewpager.setAdapter(new Review_sliding_image_adapter(context, imagesEncodedList));
        circleIndicator.setViewPager(review_viewpager);

        final float density = context.getResources().getDisplayMetrics().density;

//        indicator.setRadius(5 * density);
        review_NUM_PAGES = imagesEncodedList.size();


        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (review_currentPage == review_NUM_PAGES) {
                    review_currentPage = 0;
                }
                review_viewpager.setCurrentItem(review_currentPage++, true);
            }
        };

 /*       Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
*/
        // Pager listener over indicator
        circleIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                review_currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


//View holder

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyViewHolder(View itemView) {
            super(itemView);

            //ViewPager
           review_viewpager  = (ViewPager) itemView.findViewById(R.id.reviewpager);


            //Circle Indicator
            circleIndicator = (CircleIndicator) itemView.findViewById(R.id.rl_review_dotindicator);
        }
    }

}
