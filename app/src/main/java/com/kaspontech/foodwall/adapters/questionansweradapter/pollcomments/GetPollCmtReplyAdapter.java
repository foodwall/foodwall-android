package com.kaspontech.foodwall.adapters.questionansweradapter.pollcomments;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.PollComments.GetPollCmtReply;
import com.kaspontech.foodwall.questionspackage.pollcomments.ViewPollComments;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetPollCmtReplyAdapter extends RecyclerView.Adapter<GetPollCmtReplyAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    /**
     * Context
     */
    private Context context;

    /**
     * Poll reply pojo
     */
    GetPollCmtReply getPollCmtReply;

    /**
     * Poll reply arraylist
     */
    private List<GetPollCmtReply> getPollCmtReplyList = new ArrayList<>();

    /**
     * Poll reply relative layout
     */
    private RelativeLayout totalReplyLayout;
    /**
     * View poll comments activity interface
     */
    ViewPollComments viewPollComments;


    public GetPollCmtReplyAdapter(Context c, List<GetPollCmtReply> getPollCmtReplyList) {
        this.context = c;
        this.getPollCmtReplyList = getPollCmtReplyList;
        this.viewPollComments = (ViewPollComments) context;

    }

    @NonNull
    @Override
    public GetPollCmtReplyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_reply_comments, parent, false);
        // set the view's size, margins, paddings and layout parameters
        GetPollCmtReplyAdapter.MyViewHolder vh = new GetPollCmtReplyAdapter.MyViewHolder(v, viewPollComments);


        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final GetPollCmtReplyAdapter.MyViewHolder holder, final int position) {

        getPollCmtReply = getPollCmtReplyList.get(position);
        /*Username displaying*/

        String firstname = getPollCmtReplyList.get(position).getFirstName().replace("\"", "");
        String lastname = getPollCmtReplyList.get(position).getLastName().replace("\"", "");

        String username = firstname + " " + lastname;
        holder.txt_username_reply.setText(username);

        holder.txt_username_reply.setTextColor(Color.parseColor("#000000"));

        holder.txt_comments_reply.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getPollCmtReplyList.get(position).getQuesCmmtReply().replace("\"", "")));

        holder.txt_comments_reply.setTextColor(Color.parseColor("#000000"));


        /* Loading image url */


        Utility.picassoImageLoader(getPollCmtReplyList.get(position).getPicture(),
                1,holder.img_comment_reply,context );



        /* Posted date functionality */
        String createdOn = getPollCmtReplyList.get(position).getCreatedOn();
        Utility.setTimeStamp(createdOn,holder.txt_ago_reply);



        /*Edit click listener interface*/
        holder.txt_comment_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewPollComments.clickedcommentposition_reply_edit(v, getPollCmtReplyList.get(position).getQuesCmmtReply(),Integer.parseInt(getPollCmtReplyList.get(position).getReplyId()),Integer.parseInt(getPollCmtReplyList.get(position).getCmmtQuesId()));
                viewPollComments.clickedfor_replyedit( v, 44);
                viewPollComments.clickedcommentposition(v, Integer.parseInt(getPollCmtReplyList.get(position).getCmmtQuesId()));
                viewPollComments.clickedcomment_userid_position(v, Integer.parseInt(getPollCmtReplyList.get(position).getUserId()));
                viewPollComments.clickedfor_replyid(v, Integer.parseInt(getPollCmtReplyList.get(position).getReplyId()));

            }
        });

        /*Delete click listener*/

        holder.del_reply_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this comment?")
                        .setContentText("Won't be able to recover this comment anymore!")
                        .setConfirmText("Delete")
                        .setCancelText("Cancel")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                if(Utility.isConnected(context)){
                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                    try {
                                        String createuserid = getPollCmtReplyList.get(position).getUserId();
                                        String timelineCommentid = getPollCmtReplyList.get(position).getCmmtQuesId();
                                        String commentid = getPollCmtReplyList.get(position).getReplyId();

                                        Call<CommonOutputPojo> call = apiService.deletePollCommentsReply("create_delete_question_comments_reply", Integer.parseInt(commentid), Integer.parseInt(timelineCommentid), Integer.parseInt(createuserid));
                                        call.enqueue(new Callback<CommonOutputPojo>() {
                                            @Override
                                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                if (response.body() != null && response.body().getResponseCode() == 1) {

                                                    /*Delete postion*/

                                                    removeAt(position);


                                                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                            .setTitleText("Deleted!!")

                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                @Override
                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                    sDialog.dismissWithAnimation();

                                                                }
                                                            })
                                                            .show();


                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                //Error
                                                Log.e("FailureError", "" + t.getMessage());
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }else{
                                    Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                                }



                            }
                        })
                        .show();


            }
        });


    }

    /*Delete postion*/
    private void removeAt(int position) {
        getPollCmtReplyList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getPollCmtReplyList.size());
    }

    @Override
    public int getItemCount() {

        if(getPollCmtReplyList.isEmpty()){

            viewPollComments.check_timeline_reply_size( getPollCmtReplyList.size());
        }


        return getPollCmtReplyList.size();

    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
   /*Widgets holder*/
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view_reply;
        ImageView img_comment_reply;
        ImageButton del_reply_comment;
        LinearLayout rl_reply_comment_layout;

        TextView txt_username_reply, txt_comment_edit,
                txt_ago_reply, txt_reply_like, txt_comment_reply, txt_comments_reply, txt_view_all_replies, txt_replies_count;


        ViewPollComments comments_activity;

        public MyViewHolder(View itemView, ViewPollComments viewPollComments) {
            super(itemView);

            this.comments_activity = viewPollComments;

            view_reply = itemView.findViewById(R.id.view_reply);

            txt_ago_reply = itemView.findViewById(R.id.txt_ago_reply);
            txt_reply_like = itemView.findViewById(R.id.txt_reply_like);
            txt_comment_edit = itemView.findViewById(R.id.txt_comment_edit);
            txt_replies_count = itemView.findViewById(R.id.txt_replies_count);
            txt_comment_reply = itemView.findViewById(R.id.txt_comment_reply);
            txt_username_reply = itemView.findViewById(R.id.txt_username_reply);
            txt_comments_reply = itemView.findViewById(R.id.txt_comments_reply);
            txt_view_all_replies = itemView.findViewById(R.id.txt_view_all_replies);


            img_comment_reply = itemView.findViewById(R.id.img_comment_reply);
            del_reply_comment = itemView.findViewById(R.id.del_reply_comment);

            totalReplyLayout = itemView.findViewById(R.id.total_reply_layout);

            rl_reply_comment_layout = itemView.findViewById(R.id.rl_reply_comment_layout);


        }


        @Override
        public void onClick(View v) {

        }

    }


}
