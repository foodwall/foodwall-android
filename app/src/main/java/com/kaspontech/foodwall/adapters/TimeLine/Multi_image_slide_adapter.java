package com.kaspontech.foodwall.adapters.TimeLine;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_multi_image_pojo;
import com.kaspontech.foodwall.R;
import com.squareup.picasso.Picasso;


import java.util.List;

//

public class Multi_image_slide_adapter extends PagerAdapter implements View.OnTouchListener {


    //    private ArrayList<Integer> IMAGES;
    private List<Get_multi_image_pojo> IMAGES;
    private LayoutInflater inflater;
    private Context context;

    private PhotoView imageView;

    public Multi_image_slide_adapter(Context context, List<Get_multi_image_pojo> IMAGES) {
        this.context = context;
        this.IMAGES = IMAGES;
        inflater = LayoutInflater.from(context);


    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slide_image_layout, view, false);

        assert imageLayout != null;
        imageView = (PhotoView) imageLayout.findViewById(R.id.image);


        try {


           Picasso.get()
                    .load(IMAGES.get(position).getTimelineimage())
                    .centerCrop()
                    .into(imageView);

            view.addView(imageLayout, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
