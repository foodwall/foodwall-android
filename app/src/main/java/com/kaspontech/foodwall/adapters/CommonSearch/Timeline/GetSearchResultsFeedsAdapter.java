package com.kaspontech.foodwall.adapters.CommonSearch.Timeline;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter.ShowTaggedPeopleAdapter;
import com.kaspontech.foodwall.adapters.TimeLine.ViewPagerAdapter;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Comments_activity;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.likesActivity;
import com.kaspontech.foodwall.historicalMapPackage.EditHistoryMap;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchTimelinePojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.CreateEditTimeline_Comments;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.comments_2;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.reviewpackage.Review_in_detail_activity;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class GetSearchResultsFeedsAdapter extends RecyclerView.Adapter<GetSearchResultsFeedsAdapter.MyViewHolder> {


    Context context;
    List<SearchTimelinePojo> searchTimelinePojoList = new ArrayList<>();
    boolean like_flag;
    String commenttimelineid, createcomment;
    Get_following_profile_pojo getFollowingProfilePojo;

    RecyclerView recyler_tagged_people;
    ShowTaggedPeopleAdapter showTaggedPeopleAdapter;
    int totalLikes, totalComments;

    private static final String TAG = "GetSearchResultsFeedsAd";


    public GetSearchResultsFeedsAdapter(Context context, List<SearchTimelinePojo> searchTimelinePojoList) {
        this.context = context;
        this.searchTimelinePojoList = searchTimelinePojoList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView username, userlastname, location, like, liketxt, user_txt_comment, count, comma, count_post, txt_followers_count, txt_followers, selfusername_caption, selfuserlastname_caption, user_name_comment, viewcomment, txt_timeline_follow, comment, commenttest, share, caption, captiontag, txt_created_on, txt_adapter_post;
        RelativeLayout rl_likes_layout, rl_comments_lay;
        RelativeLayout rl_share_adapter, rl_username,rl_comment_adapter, like_rl, rl_more, rl_post_timeline, rl_follow_layout;
        LinearLayout rl_row_layout,  rl_fullimage;
        ImageView userphoto_profile, img_follow_timeline, img_commentadapter, img_shareadapter, userphoto_comment;
        EditText edittxt_comment;
        ImageButton img_like;
        ImageButton img_view_more;


        // ZoomageView Image;
        PhotoView image;

        //  ViewPager image;
        PhotoViewAttacher photoViewAttacher;


        //ImageButton
        ImageButton like_image_empty, like_imagefill;

        CheckBox img_like_new;

        public ViewPager mviewpager;
        public int currentPage = 0;
        public int NUM_PAGES = 0;


        public CircleIndicator circleIndicatortimeline;

        public MyViewHolder(View itemView) {
            super(itemView);

            //Shine Button
            img_like = (ImageButton) itemView.findViewById(R.id.img_like);

            //Edittext
            edittxt_comment = (EditText) itemView.findViewById(R.id.edittxt_comment);
            
            //ViewPager
            mviewpager = (ViewPager) itemView.findViewById(R.id.mviewpager);


            //Circle Indicator
            circleIndicatortimeline = (CircleIndicator) itemView.findViewById(R.id.indicator);
            
            //ImageView
            image = (PhotoView) itemView.findViewById(R.id.image);
            userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);
            userphoto_comment = (ImageView) itemView.findViewById(R.id.userphoto_comment);
            img_commentadapter = (ImageView) itemView.findViewById(R.id.img_commentadapter);
            img_shareadapter = (ImageView) itemView.findViewById(R.id.img_shareadapter);
            img_view_more = (ImageButton) itemView.findViewById(R.id.img_view_more);
            img_follow_timeline = (ImageView) itemView.findViewById(R.id.img_follow_timeline);
            

            //TextView
            username = (TextView) itemView.findViewById(R.id.username);
            txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);
            userlastname = (TextView) itemView.findViewById(R.id.userlastname);
            location = (TextView) itemView.findViewById(R.id.location);
            caption = (TextView) itemView.findViewById(R.id.caption);
            captiontag = (TextView) itemView.findViewById(R.id.captiontag);
            txt_created_on = (TextView) itemView.findViewById(R.id.txt_created_on);
            like = (TextView) itemView.findViewById(R.id.like);
            liketxt = (TextView) itemView.findViewById(R.id.liketxt);
            comment = (TextView) itemView.findViewById(R.id.comment);
            txt_timeline_follow = (TextView) itemView.findViewById(R.id.txt_timeline_follow);
            comma = (TextView) itemView.findViewById(R.id.comma);
            user_txt_comment = (TextView) itemView.findViewById(R.id.user_txt_comment);
            user_name_comment = (TextView) itemView.findViewById(R.id.user_name_comment);
            selfusername_caption = (TextView) itemView.findViewById(R.id.selfusername_caption);
            selfuserlastname_caption = (TextView) itemView.findViewById(R.id.selfuserlastname_caption);
            viewcomment = (TextView) itemView.findViewById(R.id.View_all_comment);
            commenttest = (TextView) itemView.findViewById(R.id.commenttest);
            share = (TextView) itemView.findViewById(R.id.shares);
            count = (TextView) itemView.findViewById(R.id.count);
            count_post = (TextView) itemView.findViewById(R.id.count_post);
            txt_followers = (TextView) itemView.findViewById(R.id.txt_followers);
            txt_followers_count = (TextView) itemView.findViewById(R.id.txt_followers_count);

            //RelativeLayout
            rl_row_layout = (LinearLayout) itemView.findViewById(R.id.layout_one_foradapter);
            rl_likes_layout = (RelativeLayout) itemView.findViewById(R.id.rl_likes_layout);
            rl_comments_lay = (RelativeLayout) itemView.findViewById(R.id.rl_comments_layout);
            rl_share_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_share_adapter);
            rl_comment_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_comment_adapter);
            like_rl = (RelativeLayout) itemView.findViewById(R.id.like_rl);
            rl_username = (RelativeLayout) itemView.findViewById(R.id.rl_username);
            rl_more = (RelativeLayout) itemView.findViewById(R.id.rl_more);
            rl_follow_layout = (RelativeLayout) itemView.findViewById(R.id.rl_follow_layout);
            rl_post_timeline = (RelativeLayout) itemView.findViewById(R.id.rl_post_timeline);
            rl_fullimage = (LinearLayout) itemView.findViewById(R.id.rl_fullimage);


        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_newsfeed_story, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        holder.setIsRecyclable(false);


        // TimeLine Image Display

        totalComments = Integer.parseInt(searchTimelinePojoList.get(position).getTotalComments());

        if (totalComments == 0) {

            holder.rl_comments_lay.setVisibility(GONE);

        } else {

            holder.rl_comments_lay.setVisibility(VISIBLE);

        }

        // TimeLine Image Display

        List<String> imagesList = new ArrayList<>();

        if (searchTimelinePojoList.get(position).getImageCount() != 0) {

            for (int i = 0; i < searchTimelinePojoList.get(position).getImage().size(); i++) {

                imagesList.add(searchTimelinePojoList.get(position).getImage().get(i).getImg());

            }

        }

        if (searchTimelinePojoList.get(position).getImageCount() == 0) {

            holder.rl_fullimage.setVisibility(View.GONE);

        } else if (searchTimelinePojoList.get(position).getImageCount() == 1) {

            holder.rl_fullimage.setVisibility(View.VISIBLE);
            holder.circleIndicatortimeline.setVisibility(View.GONE);

        }

        initializeViews(imagesList, holder, position);

        holder.circleIndicatortimeline.setViewPager(holder.mviewpager);
        holder.NUM_PAGES = imagesList.size();

        // Pager listener over indicator

        holder.circleIndicatortimeline.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                holder.currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


        // User profile photo

        try {

            // Near username


//                   Utility.picassoImageLoader(searchTimelinePojoList.get(position).getPicture(),1,holder.userphoto_profile);
//

            GlideApp.with(context)
                    .load(searchTimelinePojoList.get(position).getPicture())
                    .centerCrop()
                    .placeholder(R.drawable.ic_add_photo)
                    .into(holder.userphoto_profile);


            // Near comment box


//                    Utility.picassoImageLoader(Pref_storage.getDetail(context, "picture_user_login"),1,holder.userphoto_comment);

            GlideApp.with(context)
                    .load(Pref_storage.getDetail(context, "picture_user_login"))
                    .centerCrop()
                    .placeholder(R.drawable.ic_add_photo)
                    .into(holder.userphoto_comment);

        } catch (Exception e) {
            e.printStackTrace();
        }


        // Follow, following visibility

        /*if (Integer.parseInt(searchTimelinePojoList.get(position).()) == 0) {

            holder.txt_timeline_follow.setVisibility(VISIBLE);
            holder.img_follow_timeline.setVisibility(VISIBLE);

        } else if (Integer.parseInt(searchTimelinePojoList.get(position).getFollowingId()) != 0) {

            holder.txt_timeline_follow.setVisibility(GONE);
            holder.img_follow_timeline.setVisibility(GONE);

        }*/


        if (Pref_storage.getDetail(context, "userId").equals(searchTimelinePojoList.get(position).getUserId())) {

            holder.txt_timeline_follow.setVisibility(GONE);
            holder.img_follow_timeline.setVisibility(GONE);

        }


        // User caption visiblity

        try {

            String caption = searchTimelinePojoList.get(position).getTimelineDescription();

            if(caption.equals("0")){

                holder.caption.setVisibility(GONE);

            }else {

                holder.caption.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(searchTimelinePojoList.get(position).getTimelineDescription()));

                holder.caption.setTextColor(Color.parseColor("#000000"));
                holder.caption.setVisibility(VISIBLE);
            }



        } catch (Exception e) {
            e.printStackTrace();
        }



/*
                // Total posts visibility

                String post_count = searchTimelinePojoList.get(position).getTotalPosts();


                if (Integer.parseInt(post_count) == 0) {

                    holder.count_post.setVisibility(GONE);
                    holder.count.setVisibility(GONE);
                    holder.comma.setVisibility(GONE);

                } else if (Integer.parseInt(post_count) == 1) {

                    holder.count.setText(post_count);
                    holder.count.setVisibility(VISIBLE);
                    holder.count_post.setText(R.string.post);
                    holder.count_post.setVisibility(VISIBLE);

                } else {

                    holder.count.setText(post_count);
                    holder.count.setVisibility(VISIBLE);
                    holder.comma.setVisibility(VISIBLE);
                    holder.count_post.setVisibility(VISIBLE);


                    //1000 posts to 1K posts convertion method

                   *//* if (Math.abs(number / 1000000) > 1) {
                        numberString = (number / 1000000).toString() + "m";
                    }
                    else if (Math.abs(number / 1000) > 1) {
                        numberString = (number / 1000).toString() + "k";
                    }
                    else {
                        numberString = number.toString();
                    }*//*

                }*/


            /*    // Total followers visibility

                String followers_post = searchTimelinePojoList.get(position).getTotalFollowers();

                if (Integer.parseInt(followers_post) == 0) {

                    holder.txt_followers.setVisibility(GONE);
                    holder.comma.setVisibility(GONE);
                    holder.txt_followers_count.setVisibility(GONE);

                } else if (Integer.parseInt(followers_post) == 1) {

                    holder.txt_followers_count.setText(followers_post);
                    holder.txt_followers.setText(R.string.follwer);
                    holder.txt_followers.setVisibility(VISIBLE);
                    holder.comma.setVisibility(VISIBLE);
                    holder.txt_followers_count.setVisibility(VISIBLE);

                } else if (Integer.parseInt(followers_post) > 1) {

                    holder.txt_followers_count.setText(followers_post);
                    holder.txt_followers.setVisibility(VISIBLE);
                    holder.comma.setVisibility(VISIBLE);
                    holder.txt_followers.setText("Followers");
                    holder.txt_followers_count.setVisibility(VISIBLE);

                }
*/

        // Comments visibility


        try {


            String comment = searchTimelinePojoList.get(position).getTotalComments();

            if (Integer.parseInt(comment) == 0 || comment == null) {

                holder.viewcomment.setVisibility(GONE);
                holder.commenttest.setVisibility(GONE);

            } else {

                holder.comment.setText(comment);

                if (Integer.parseInt(comment) == 1) {

                    holder.viewcomment.setText(R.string.view);
                    holder.commenttest.setText(R.string.commenttt);
                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setVisibility(VISIBLE);

                } else if (Integer.parseInt(comment) > 1) {

                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setVisibility(VISIBLE);

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        // Displaying user name

        try {

            String username = searchTimelinePojoList.get(position).getFirstName() + " " + searchTimelinePojoList.get(position).getLastName();
            String hotelname = searchTimelinePojoList.get(position).getTimelineHotel();
            int taggedCount = searchTimelinePojoList.get(position).getWhomCount();

            String taggedPeopleText = "";
            String finalUserNameText = "";

            if (taggedCount != 0) {

                String taggedFirstName = "";
                String taggedSecondName = "";


                switch (taggedCount) {


                    case 1:

                        taggedFirstName = searchTimelinePojoList.get(position).getWhom().get(0).getFirstName() + " " + searchTimelinePojoList.get(position).getWhom().get(0).getLastName();
                        taggedPeopleText = taggedFirstName;

                        break;

                    case 2:

                        taggedFirstName = searchTimelinePojoList.get(position).getWhom().get(0).getFirstName() + " " + searchTimelinePojoList.get(position).getWhom().get(0).getLastName();
                        taggedSecondName = searchTimelinePojoList.get(position).getWhom().get(1).getFirstName() + " " + searchTimelinePojoList.get(position).getWhom().get(1).getLastName();
                        taggedPeopleText = taggedFirstName + " and " + taggedSecondName;


                        break;


                    default:

                        taggedFirstName = searchTimelinePojoList.get(position).getWhom().get(0).getFirstName() + " " + searchTimelinePojoList.get(position).getWhom().get(0).getLastName();
                        taggedPeopleText = taggedFirstName + " and " + String.valueOf(taggedCount - 1) + " Others";

                        break;

                }


            }

            if (taggedCount == 0) {

                finalUserNameText = username + " at " + hotelname;

                SpannableString ss = new SpannableString(finalUserNameText);

                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {

                        String user_id = searchTimelinePojoList.get(position).getUserId();
                        String timelineid = searchTimelinePojoList.get(position).getTimelineId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLACK);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {

                        Intent intent = new Intent(context, Review_in_detail_activity.class);
                        intent.putExtra("hotelid", searchTimelinePojoList.get(position).getHotelId());
                        intent.putExtra("f_name", searchTimelinePojoList.get(position).getFirstName());
                        intent.putExtra("l_name", searchTimelinePojoList.get(position).getLastName());
                        intent.putExtra("reviewid", "0");
                        intent.putExtra("timelineId", searchTimelinePojoList.get(position).getTimelineId());

                        context.startActivity(intent);

                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLACK);
                        ds.setUnderlineText(false);
                    }
                };

                ss.setSpan(clickableSpan1, 0, username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(clickableSpan2, username.length() + 4, finalUserNameText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                holder.username.setText(ss);
                holder.username.setMovementMethod(LinkMovementMethod.getInstance());


            } else {

                String isWith = " is with ";
                String at = " at ";

                finalUserNameText = username + isWith + taggedPeopleText + at + hotelname;

                SpannableString ss = new SpannableString(finalUserNameText);

                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {

                        String user_id = searchTimelinePojoList.get(position).getUserId();
                        String timelineid = searchTimelinePojoList.get(position).getTimelineId();

                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLACK);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {

                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                        LayoutInflater inflater1 = LayoutInflater.from(context);
                        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.alert_show_tagged_people, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setTitle("");

                        recyler_tagged_people = (RecyclerView) dialogView.findViewById(R.id.recyler_tagged_people);

                        showTaggedPeopleAdapter = new ShowTaggedPeopleAdapter(context, searchTimelinePojoList.get(position).getWhom());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                        recyler_tagged_people.setLayoutManager(mLayoutManager);
                        recyler_tagged_people.setItemAnimator(new DefaultItemAnimator());
                        recyler_tagged_people.setAdapter(showTaggedPeopleAdapter);
                        recyler_tagged_people.hasFixedSize();

                        final AlertDialog dialog = dialogBuilder.create();
                        dialog.show();
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLACK);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan3 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {

                        Intent intent = new Intent(context, Review_in_detail_activity.class);
                        intent.putExtra("hotelid", searchTimelinePojoList.get(position).getHotelId());
                        intent.putExtra("hotelid", searchTimelinePojoList.get(position).getHotelId());
                        intent.putExtra("f_name", searchTimelinePojoList.get(position).getFirstName());
                        intent.putExtra("l_name", searchTimelinePojoList.get(position).getLastName());
                        intent.putExtra("reviewid", "0");
                        intent.putExtra("timelineId", searchTimelinePojoList.get(position).getTimelineId());
                        context.startActivity(intent);


                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLACK);
                        ds.setUnderlineText(false);
                    }
                };


                int first = 0, second = username.length();
                int third = username.length() + isWith.length(), fourth = username.length() + isWith.length() + taggedPeopleText.length();
                int fifth = username.length() + isWith.length() + taggedPeopleText.length() + at.length(), sixth = finalUserNameText.length();

//                        Log.e("TestingLength", "First :" + first + " Second :" + second);
//                        Log.e("TestingLength", "third :" + third + " fourth :" + fourth);
//                        Log.e("TestingLength", "fifth :" + fifth + " Second :" + sixth);

                ss.setSpan(clickableSpan1, first, second, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(clickableSpan2, third, fourth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(clickableSpan3, fifth, sixth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

//                        Log.e(TAG, "onBindViewHolder: " + ss.toString());


                holder.username.setText(ss);
                holder.username.setMovementMethod(LinkMovementMethod.getInstance());

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        // Timestamp for timeline


        String createdon = searchTimelinePojoList.get(position).getCreatedOn();

        try {
            long now = System.currentTimeMillis();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date convertedDate = dateFormat.parse(createdon);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now,

                    DateUtils.FORMAT_ABBREV_RELATIVE);
            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                holder.txt_created_on.setText(R.string.just_now);
            } else {
                holder.txt_created_on.append(relavetime1 + "");
                System.out.println(relavetime1);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Comments activity intent

        holder.rl_comment_adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* Intent intent = new Intent(context, Comments_activity.class);
                intent.putExtra("comment_timelineid", searchTimelinePojoList.get(position).getTimelineId());
                context.startActivity(intent);
*/
                Intent intent = new Intent(context, Comments_activity.class);
                // Bundle bundle = new Bundle();
                intent.putExtra("comment_timelineid", searchTimelinePojoList.get(position).getTimelineId());
                intent.putExtra("comment_timelinepicture", searchTimelinePojoList.get(position).getPicture());
                intent.putExtra("comment_posttpe", searchTimelinePojoList.get(position).getPostType());
                intent.putExtra("comment_profile", searchTimelinePojoList.get(position).getPicture());
                intent.putExtra("comment_created_on", searchTimelinePojoList.get(position).getCreatedOn());
                intent.putExtra("username", searchTimelinePojoList.get(position).getFirstName());

                try {
                    intent.putExtra("comment_image", searchTimelinePojoList.get(position).getImage().get(0).getImg());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        // User Like API

        totalLikes = searchTimelinePojoList.get(position).getTotalLikes();

        if (totalLikes == 0) {

            holder.like.setVisibility(View.GONE);

        } else if (totalLikes == 1) {

            holder.like.setVisibility(View.VISIBLE);
            holder.like.setText(String.valueOf(totalLikes) + " Like");

        } else {

            holder.like.setVisibility(View.VISIBLE);
            holder.like.setText(String.valueOf(totalLikes) + " Likes");
        }

        if (searchTimelinePojoList.get(position).getTlLikes().equals("0")) {

            searchTimelinePojoList.get(position).setLiked(false);
            holder.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));


        } else {

            searchTimelinePojoList.get(position).setLiked(true);
            holder.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));

        }

        holder.img_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);


                if (searchTimelinePojoList.get(position).isLiked()) {

                    holder.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                    searchTimelinePojoList.get(position).setLiked(false);
                    searchTimelinePojoList.get(position).setTlLikes(String.valueOf(0));


                    int total_likes = searchTimelinePojoList.get(position).getTotalLikes();

                    if (total_likes == 1) {

                        total_likes = total_likes - 1;
                        searchTimelinePojoList.get(position).setTotalLikes(total_likes);
                        holder.like.setVisibility(View.GONE);


                    } else if (total_likes == 2) {

                        total_likes = total_likes - 1;
                        searchTimelinePojoList.get(position).setTotalLikes(total_likes);
                        holder.like.setText(String.valueOf(total_likes) + " Like");
                        holder.like.setVisibility(View.VISIBLE);

                    } else {

                        total_likes = total_likes - 1;
                        searchTimelinePojoList.get(position).setTotalLikes(total_likes);
                        holder.like.setText(String.valueOf(total_likes) + " Likes");
                        holder.like.setVisibility(View.VISIBLE);

                    }


                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                        int timelineId = Integer.parseInt(searchTimelinePojoList.get(position).getTimelineId());

//                                Log.d(TAG, "onClick: " + timelineId);

                        Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", timelineId, 0, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("nodata")) {

                                        //Success


                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                } else {

                    holder.img_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                    searchTimelinePojoList.get(position).setLiked(true);
                    searchTimelinePojoList.get(position).setTlLikes(String.valueOf(1));


                    int total_likes = searchTimelinePojoList.get(position).getTotalLikes();

                    if (total_likes == 0) {

                        total_likes = total_likes + 1;
                        searchTimelinePojoList.get(position).setTotalLikes(total_likes);
                        holder.like.setText(String.valueOf(total_likes) + " Like");
                        holder.like.setVisibility(View.VISIBLE);

                    } else {

                        total_likes = total_likes + 1;
                        searchTimelinePojoList.get(position).setTotalLikes(total_likes);
                        holder.like.setText(String.valueOf(total_likes) + " Likes");

                    }

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                        int timelineId = Integer.parseInt(searchTimelinePojoList.get(position).getTimelineId());

//                                Log.d(TAG, "onClick: " + timelineId);

                        Call<CommonOutputPojo> call = apiService.create_timeline_likes("create_timeline_likes", timelineId, 1, userid);

                        call.enqueue(new Callback<CommonOutputPojo>() {
                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                if (response.body() != null) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        //Success


                                    }

                                } else {

                                    Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                }

                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "FailureError" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }


            }
        });


        holder.edittxt_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (holder.edittxt_comment.getText().toString().length() != 0) {
                    holder.txt_adapter_post.setVisibility(VISIBLE);
                } else {
                    holder.txt_adapter_post.setVisibility(GONE);
                }


            }
        });

        holder.rl_post_timeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String commenttimelineid = searchTimelinePojoList.get(position).getTimelineId();
                String createcomment = org.apache.commons.text.StringEscapeUtils.escapeJava(holder.edittxt_comment.getText().toString());

                hideKeyboard(v);

                int Cmnt = Integer.parseInt(searchTimelinePojoList.get(position).getTotalComments()) + 1;

                holder.comment.setText(String.valueOf(Cmnt));
                holder.comment.setVisibility(VISIBLE);

                if (Cmnt == 1) {

                    holder.viewcomment.setText(R.string.view);
                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setText(R.string.commenttt);
                    holder.commenttest.setVisibility(VISIBLE);

                } else if (Cmnt > 1) {

                    holder.viewcomment.setVisibility(VISIBLE);
                    holder.commenttest.setVisibility(VISIBLE);

                }

                create_edit_timeline_comments(commenttimelineid, createcomment);

                holder.edittxt_comment.setText("");

            }
        });


        holder.rl_likes_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, likesActivity.class);
                intent.putExtra("Likes_timelineid", searchTimelinePojoList.get(position).getTimelineId());
                context.startActivity(intent);

            }
        });


        holder.rl_comments_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* Intent intent = new Intent(context, Comments_activity.class);
                intent.putExtra("comment_timelineid", searchTimelinePojoList.get(position).getTimelineId());
                context.startActivity(intent);*/
                Intent intent = new Intent(context, Comments_activity.class);
                // Bundle bundle = new Bundle();
                intent.putExtra("comment_timelineid", searchTimelinePojoList.get(position).getTimelineId());
                intent.putExtra("comment_timelinepicture", searchTimelinePojoList.get(position).getPicture());
                intent.putExtra("comment_posttpe", searchTimelinePojoList.get(position).getPostType());
                intent.putExtra("comment_profile", searchTimelinePojoList.get(position).getPicture());
                intent.putExtra("comment_created_on", searchTimelinePojoList.get(position).getCreatedOn());
                intent.putExtra("username", searchTimelinePojoList.get(position).getFirstName());

                try {
                    intent.putExtra("comment_image", searchTimelinePojoList.get(position).getImage().get(0).getImg());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        // User photo click

        holder.userphoto_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_id = searchTimelinePojoList.get(position).getUserId();

                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    context.startActivity(new Intent(context, Profile.class));

                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }


            }


        });


        holder.img_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                String timelineid = searchTimelinePojoList.get(position).getTimelineId();
                String userid = searchTimelinePojoList.get(position).getUserId();

                CustomDialogClass ccd = new CustomDialogClass(context, timelineid, userid, position);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccd.setCancelable(true);
                ccd.show();


            }


        });




    }


    private void create_edit_timeline_comments(final String commenttimelineid, String createcomment) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {
            String createuserid = Pref_storage.getDetail(context, "userId");

            Call<CreateEditTimeline_Comments> call = apiService.create_edit_timeline_comments1("create_edit_timeline_comments", 0, Integer.parseInt(commenttimelineid), createcomment, Integer.parseInt(createuserid));
            call.enqueue(new Callback<CreateEditTimeline_Comments>() {
                @Override
                public void onResponse(Call<CreateEditTimeline_Comments> call, Response<CreateEditTimeline_Comments> response) {

                    if (response.body().getResponseCode() == 1) {


                        get_timeline_comments_all();


                    }

                }

                @Override
                public void onFailure(Call<CreateEditTimeline_Comments> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void get_timeline_comments_all() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {
            String createuserid = Pref_storage.getDetail(context, "userId");
            String comment_timelineid = Pref_storage.getDetail(context, "comment_timelineid");
//            Call<comments_2> call = apiService.get_timeline_comments_all("get_timeline_comments_user", Integer.parseInt(createuserid), 22);
            Call<comments_2> call = apiService.get_timeline_comments_all("get_timeline_comments_user", Integer.parseInt(comment_timelineid), Integer.parseInt(createuserid));
            call.enqueue(new Callback<comments_2>() {
                @Override
                public void onResponse(Call<comments_2> call, Response<comments_2> response) {

                    if (response.body().getResponseCode() == 1) {


                    }

                }

                @Override
                public void onFailure(Call<comments_2> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }

    private void initializeViews(List<String> dataModel, final MyViewHolder holder, int position) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(dataModel);
        holder.mviewpager.setAdapter(adapter);
        holder.mviewpager.setClipToPadding(false);
        holder.mviewpager.setPadding(0, 0, 0, 0);
        if (position == 0) {
//            ((holder) holder).title.setText("Just for the weekend");
        }
        else{
//            ((holder)holder).title.setText("AirBnb Favorites");
        }
    }

    @Override
    public int getItemCount() {
        return searchTimelinePojoList.size();
    }

    private void create_delete_timeline(String timelineid) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            String createdby = Pref_storage.getDetail(context, "userId");
            Call<CommonOutputPojo> call = apiService.create_delete_timeline("create_delete_timeline", Integer.parseInt(timelineid), Integer.parseInt(createdby));
            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    if (response.body() != null) {

                        Log.e("ResponseStatus", "ResponseStatus->" + response.body().getResponseMessage());

                        if (response.body().getResponseMessage().equals("nodata")) {
                            removeAt(Integer.parseInt(Pref_storage.getDetail(context, "clicked_timelinePosition")));


                            Toast.makeText(context, "Deleted!!", Toast.LENGTH_LONG).show();

//                            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//                                    .setTitleText("Deleted!!")
//                                    .setContentText("Your post has been deleted.")
//                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                        @Override
//                                        public void onClick(SweetAlertDialog sDialog) {
//                                            sDialog.dismissWithAnimation();
//                                           /* context.startActivity(new Intent(context, Home.class));
//                                            ((AppCompatActivity) context).finish();*/
//
//                                        }
//                                    })
//                                    .show();


                        }

                    }


                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void removeAt(int position) {
        searchTimelinePojoList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, searchTimelinePojoList.size());
    }


    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity c;
        public Dialog d;
        public AppCompatButton btn_Ok;

        int position;
        String timelineid, userid;

        TextView txt_deletepost, txt_report, txt_copylink, txt_turnoffpost, txt_sharevia, txt_editpost;

        CustomDialogClass(Context a,String timelineid,String userid,int position) {
            super(a);
            // TODO Auto-generated constructor stub
            context = a;
            this.position = position;
            this.timelineid = timelineid;
            this.userid = userid;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
//            setContentView(R.layout.dialog_version_info);

            setContentView(R.layout.custom_dialog_timeline);

            txt_deletepost = (TextView) findViewById(R.id.txt_deletepost);
            txt_editpost = (TextView) findViewById(R.id.txt_editpost);
            txt_report = (TextView) findViewById(R.id.txt_report);
            txt_copylink = (TextView) findViewById(R.id.txt_copylink);
            txt_turnoffpost = (TextView) findViewById(R.id.txt_turnoffpost);
            txt_sharevia = (TextView) findViewById(R.id.txt_sharevia);

            txt_deletepost.setOnClickListener(this);
            txt_editpost.setOnClickListener(this);
            txt_report.setOnClickListener(this);
            txt_copylink.setOnClickListener(this);
            txt_turnoffpost.setOnClickListener(this);
            txt_sharevia.setOnClickListener(this);


            if (userid.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                txt_deletepost.setVisibility(VISIBLE);
                txt_editpost.setVisibility(VISIBLE);
            } else {
                txt_deletepost.setVisibility(GONE);
                txt_editpost.setVisibility(GONE);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_editpost:

                    Intent intent = new Intent(context,EditHistoryMap.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("timelineId", timelineid);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    dismiss();

                    break;
                case R.id.txt_deletepost:


                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to delete this post?")
                            .setContentText("Won't be able to recover this post anymore!")
                            .setConfirmText("Yes,delete it!")
                            .setCancelText("No,cancel!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    create_delete_timeline(timelineid);

                                }
                            })
                            .show();

                    dismiss();
                    break;
                case R.id.txt_report:
                    dismiss();
                    break;
                case R.id.txt_copylink:
                    Toast.makeText(context, "Link has been copied to clipboard", Toast.LENGTH_SHORT).show();
                    dismiss();
                    break;
                case R.id.txt_turnoffpost:
                    dismiss();
                    break;
                case R.id.txt_sharevia:
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                    i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                    context.startActivity(Intent.createChooser(i, "Share via"));
                    dismiss();
                    break;

                default:
                    break;
            }
        }
    }


}
