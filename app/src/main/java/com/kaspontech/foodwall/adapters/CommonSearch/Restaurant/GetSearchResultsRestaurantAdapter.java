package com.kaspontech.foodwall.adapters.CommonSearch.Restaurant;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchRestaurantPojo;
import com.kaspontech.foodwall.reviewpackage.Image_load_activity;
import com.kaspontech.foodwall.reviewpackage.Review_in_detail_activity;

import java.util.ArrayList;
import java.util.List;

public class GetSearchResultsRestaurantAdapter extends RecyclerView.Adapter<GetSearchResultsRestaurantAdapter.MyViewHolder> {


    Context context;
    List<SearchRestaurantPojo> searchRestaurantPojoList = new ArrayList<>();
    private static final String TAG = "GetSearchResultsRestaur";

    public GetSearchResultsRestaurantAdapter(Context context, List<SearchRestaurantPojo> searchRestaurantPojoList) {
        this.context = context;
        this.searchRestaurantPojoList = searchRestaurantPojoList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_restaurant_image,img_overall_rating_star;
        LinearLayout ll_res_overall_rating, ll_restaurant;
        TextView tv_restaurant_name, tv_restaurant_address, tv_res_overall_rating;

        public MyViewHolder(View itemView) {
            super(itemView);

            img_restaurant_image = (ImageView) itemView.findViewById(R.id.img_restaurant_image);
            img_overall_rating_star = (ImageView) itemView.findViewById(R.id.img_overall_rating_star);
            ll_restaurant = (LinearLayout) itemView.findViewById(R.id.ll_restaurant);
            ll_res_overall_rating = (LinearLayout) itemView.findViewById(R.id.ll_res_overall_rating);
            tv_restaurant_name = (TextView) itemView.findViewById(R.id.tv_restaurant_name);
            tv_restaurant_address = (TextView) itemView.findViewById(R.id.tv_restaurant_address);
            tv_res_overall_rating = (TextView) itemView.findViewById(R.id.tv_res_overall_rating);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_restaurant, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        Log.e(TAG, "onBindViewHolder: " + getItemCount());
        Log.e(TAG, "onBindViewHolder: " + searchRestaurantPojoList.size());

        String restaurantName = searchRestaurantPojoList.get(position).getHotelName();
        String restaurantAddress = searchRestaurantPojoList.get(position).getAddress();
        String restaurantImage = searchRestaurantPojoList.get(position).getPhotoReference();

        holder.tv_restaurant_name.setText(restaurantName);
        holder.tv_restaurant_address.setText(restaurantAddress);

        Image_load_activity.loadRoundCornerIcon(context, restaurantImage, holder.img_restaurant_image);

        String rating = searchRestaurantPojoList.get(position).getTotal_food_exprience();
        String totalReview = searchRestaurantPojoList.get(position).getTotal_review();

        //get total rating
        int rate=Integer.parseInt(rating)/Integer.parseInt(totalReview);
        holder.tv_res_overall_rating.setText(String.valueOf(rate)+".0");


       // holder.tv_res_overall_rating.setText(rating +".0");

        if ( rate  == 1) {

            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_red);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

        } else if ( rate  == 2) {

            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.ll_res_overall_rating.setBackgroundResource(R.drawable.review_rating_orange_bg);
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_orange_star);

        } else if (rate == 3) {

            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));

        } else if (rate == 4) {

            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));

        } else if (rate == 5) {

            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
        }


        holder.ll_restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, Review_in_detail_activity.class);
                intent.putExtra("hotelid", searchRestaurantPojoList.get(position).getHotelId());
                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return searchRestaurantPojoList.size();
    }


}
