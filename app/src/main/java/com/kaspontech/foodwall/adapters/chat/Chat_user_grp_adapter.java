package com.kaspontech.foodwall.adapters.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.bucketlistpackage.CreateBucketlistActivty;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.Get_grp_input_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.R;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

//

public class Chat_user_grp_adapter extends RecyclerView.Adapter<Chat_user_grp_adapter.MyViewHolder> {

    /**
     * Context
     **/
    private Context context;

    /**
     * Group arraylist
     **/
    private ArrayList<Get_grp_input_pojo> gettinggrouplist = new ArrayList<>();


    /**
     * Group input pojo
     **/
    private Get_grp_input_pojo getGrpInputPojo;

    /**
     * Following profile pojo
     **/
    private Get_following_profile_pojo getFollowingProfilePojo;


    boolean  doubleClick = false;


    public Chat_user_grp_adapter(Context c, ArrayList<Get_grp_input_pojo> gettinglist) {
        this.context = c;
        this.gettinggrouplist = gettinglist;

    }

    @NonNull
    @Override
    public Chat_user_grp_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_chat_followerlist, parent, false);
        // set the view's size, margins, paddings and layout parameters



        return new Chat_user_grp_adapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Chat_user_grp_adapter.MyViewHolder holder, final int position) {


        getGrpInputPojo = gettinggrouplist.get(position);

        holder.check_follower.setVisibility(View.GONE);


        /*User name displaying*/
        holder.txt_username.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettinggrouplist.get(position).getGroupName()));
        holder.txt_username.setTextColor(Color.parseColor("#000000"));
        holder.txt_userlastname.setVisibility(View.GONE);
        holder.txt_fullname_user.setVisibility(View.GONE);


//        holder.txtUserlastname.setText(getFollowerlistPojo.getLast_name());
//        holder.txtUserlastname.setTextColor(Color.parseColor("#000000"));

      /*  String fullname= getFollowerlistPojo.getFirst_name().concat(" ").concat(getFollowerlistPojo.getLast_name());

        holder.txtFullnameUser.setText(fullname);*/

        String ownuser_id= Pref_storage.getDetail(context, "userId");




// user image


        GlideApp.with(context)
                .load(gettinggrouplist.get(position).getGroupIcon()).placeholder(R.drawable.ic_group_inside)
                .into(holder.img_following);




        /*Follower activity on click listener*/
        holder.rl_follower_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity( new Intent(context, ChatActivity.class));



              /*  Intent intent= new Intent(context, ChatActivity.class);

                *//*intent.putExtra("userid",getFollowerlistPojoArrayList.get(position).getFriendid());*//*
                context.startActivity(intent);*/
            }
        });
    }




//Remove position

    private  void removeatposition(int position){
        gettinggrouplist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, gettinggrouplist.size());
    }





    @Override
    public int getItemCount() {

        return gettinggrouplist.size();
    }





    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_username, txt_userlastname, txt_fullname_user, txt_follow, share, caption, captiontag, txt_created_on,txt_adapter_post;
        RelativeLayout rlayout_following,rl_follower_ayout,rl_follower_lay;

        ImageView img_checked_follower,img_following;


        CheckBox check_follower;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_userlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txt_fullname_user = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            img_following = (ImageView) itemView.findViewById(R.id.img_following);
            img_checked_follower = (ImageView) itemView.findViewById(R.id.img_checked_follower);

            check_follower=(CheckBox) itemView.findViewById(R.id.check_follower);


            rlayout_following = (RelativeLayout) itemView.findViewById(R.id.rlayout_following);

            rl_follower_ayout = (RelativeLayout) itemView.findViewById(R.id.rl_follower_ayout);

            rl_follower_lay = (RelativeLayout) itemView.findViewById(R.id.rl_follower_lay);




        }



    }




}

