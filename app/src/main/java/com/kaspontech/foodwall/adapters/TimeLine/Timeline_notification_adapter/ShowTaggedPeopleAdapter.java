package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.TaggedPeoplePojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ShowTaggedPeopleAdapter extends RecyclerView.Adapter<ShowTaggedPeopleAdapter.MyViewHolder> {


    Context context;
    List<TaggedPeoplePojo> taggedPeoplePojoList = new ArrayList<>();


    public ShowTaggedPeopleAdapter(Context context, List<TaggedPeoplePojo> taggedPeoplePojoList) {
        this.context = context;
        this.taggedPeoplePojoList = taggedPeoplePojoList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView user_image;
        TextView user_name;

        public MyViewHolder(View itemView) {
            super(itemView);

            user_image = (CircleImageView) itemView.findViewById(R.id.user_image);
            user_name = (TextView) itemView.findViewById(R.id.user_name);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_results_people, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {


        String userName = taggedPeoplePojoList.get(position).getFirstName() + " " + taggedPeoplePojoList.get(position).getLastName();
        String profile_photo_url = taggedPeoplePojoList.get(position).getPicture();

        holder.user_name.setText(userName);


        Utility.picassoImageLoader(profile_photo_url,
                0,holder.user_image, context);
//        GlideApp.with(context.getApplicationContext())
//                .load(profile_photo_url)
//                .placeholder(R.drawable.ic_add_photo)
//                .into(holder.user_image);


        holder.user_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userid = taggedPeoplePojoList.get(position).getUserId();
                String myUserId = Pref_storage.getDetail(context,"userId");


                if(myUserId.equals(userid)){

                    Intent intent = new Intent(context, Profile.class);
                    context.startActivity(intent);


                }else{

                    Pref_storage.setDetail(context, "SearchPeopleUserid", taggedPeoplePojoList.get(position).getUserId());

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", taggedPeoplePojoList.get(position).getUserId());
                    /*Bundle bundle = new Bundle();
                    bundle.putString("userid", taggedPeoplePojoList.get(position).getUserId());
                    intent.putExtras(bundle);*/
                    context.startActivity(intent);

                }




            }
        });

    }

    @Override
    public int getItemCount() {
        return taggedPeoplePojoList.size();
    }

}
