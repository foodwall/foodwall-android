package com.kaspontech.foodwall.adapters.bucketadapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.bucketlistpackage.CreateBucketlistActivty;
import com.kaspontech.foodwall.bucketlistpackage.GroupBucketList_Datum;
import com.kaspontech.foodwall.bucketlistpackage.GroupbucketlistDetails;
import com.kaspontech.foodwall.bucketlistpackage.IndividualBucketlistDetails;
import com.kaspontech.foodwall.bucketlistpackage.Mybucketfragment;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.reviewpackage.ReviewDeliveryDetails;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewAmbienceAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewAvoidDishAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.ReviewTopDishAdapter;
import com.kaspontech.foodwall.reviewpackage.Review_in_detail_activity;
import com.kaspontech.foodwall.reviewpackage.ReviewsLikesAllActivity;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
import com.kaspontech.foodwall.reviewpackage.alldishtab.AllDishDisplayActivity;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static java.lang.Integer.parseInt;

public class GroupBucketlistAdapter extends RecyclerView.Adapter<GroupBucketlistAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     **/
    private Context context;
    /**
     * My bucket arraylist
     **/
    private List<GroupBucketList_Datum> getallHotelReviewPojoArrayList = new ArrayList<>();
    /**
     * More options
     **/
    private final String[] itemsothers = {"Bucket list options", "Share", "Share Chat"};
    /**
     * Top dish arraylist
     **/
    public ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();

    /**
     * Avoid dish arraylist
     **/
    public ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();
    /**
     * Ambience arraylist
     **/
    public ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

    Mybucketfragment listsize;


    private int selectedPosition = -1;
    onCommentsPostListener onCommentsPostListener;

    public GroupBucketlistAdapter(Context c, List<GroupBucketList_Datum> gettinglist,onCommentsPostListener onCommentsPostListener1) {
        this.context = c;
        this.getallHotelReviewPojoArrayList = gettinglist;
        this.onCommentsPostListener = onCommentsPostListener1;


    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_bucket_list, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v,onCommentsPostListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {




        try {

            /*Display hotel details*/
            displayReviewItem(getallHotelReviewPojoArrayList, holder, position);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    /*Display hotel details*/


    public void displayReviewItem(final List<GroupBucketList_Datum> reviewList, final MyViewHolder holder, final int position) {


        if (reviewList.size() > 0) {


            holder.rl_bucketlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, GroupbucketlistDetails.class);
                    intent.putExtra("groupuserid",getallHotelReviewPojoArrayList.get(position).getGroupId());
                    context.startActivity(intent);
                    notifyDataSetChanged();
                 }

            });




            holder.txt_usernamechat.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getallHotelReviewPojoArrayList.get(position).getGroupName()));

            //  holder.userlastname.setText(reviewer_lname);
            holder.txt_usernamechat.setTextColor(Color.parseColor("#000000"));
            holder.userlastname.setTextColor(Color.parseColor("#000000"));


            // load userphoto_profile
            if (getallHotelReviewPojoArrayList.get(position).getGroupIcon().equalsIgnoreCase("null") ||
                    getallHotelReviewPojoArrayList.get(position).getGroupIcon().equalsIgnoreCase("0")) {

                GlideApp.with(context)
                        .load(getallHotelReviewPojoArrayList.get(position).getGroupIcon())
                        .error(R.drawable.ic_people_group)
                        .into(holder.userphoto_profile);
            } else {

                Glide.with(context).load(getallHotelReviewPojoArrayList.get(position).getGroupIcon()).into(holder.userphoto_profile);
            }






            /* Timestamp for review */

            String createdOn = reviewList.get(position).getBucketCreatedOn();

            Utility.setTimeStamp(createdOn, holder.txt_created_on);




            holder.txt_usernamechat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (reviewList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        context.startActivity(intent);
                    }

                }
            });


            holder.userlastname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (reviewList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        context.startActivity(intent);
                    }

                }
            });

            holder.userphoto_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (reviewList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                        Intent intent = new Intent(context, Profile.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        context.startActivity(intent);

                    } else {

                        Intent intent = new Intent(context, User_profile_Activity.class);
                        intent.putExtra("created_by", reviewList.get(position).getUserId());
                        context.startActivity(intent);
                    }

                }
            });












        }


    }


    @Override
    public int getItemCount() {




        return getallHotelReviewPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        EmojiconEditText review_comment;

        ViewPager ambi_viewpager;

        RelativeLayout rl_bucketlist;

        CircleImageView userphoto_profile;

        EmojiconTextView txt_usernamechat;

        TextView userlastname,txt_created_on;



        onCommentsPostListener onCommentsPostListener;
        View viewItem;

        public MyViewHolder(View itemView, com.kaspontech.foodwall.onCommentsPostListener onCommentsPostListener) {

            super(itemView);

           this.viewItem=itemView;
           this.onCommentsPostListener=onCommentsPostListener;

            txt_usernamechat = (EmojiconTextView) itemView.findViewById(R.id.txt_usernamechat);

            userlastname = (TextView) itemView.findViewById(R.id.userlastname);

            txt_created_on = (TextView) itemView.findViewById(R.id.txt_created_on);

            ambi_viewpager = (ViewPager) itemView.findViewById(R.id.ambi_viewpager);


            review_comment = (EmojiconEditText) itemView.findViewById(R.id.review_comment);

            userphoto_profile = (CircleImageView) itemView.findViewById(R.id.userphoto_profile);


            rl_bucketlist = (RelativeLayout) itemView.findViewById(R.id.rl_bucketlist);

        }


        @Override
        public void onClick(View view) {
            switch (view.getId())
            {
                case R.id.txt_adapter_post:
                    onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),viewItem);
                    break;
            }

        }
    }


}

