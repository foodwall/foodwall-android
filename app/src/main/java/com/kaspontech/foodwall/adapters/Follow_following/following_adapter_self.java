package com.kaspontech.foodwall.adapters.Follow_following;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.followingPackage.Following_listActivity;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_unfollow_follower_output;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class following_adapter_self extends RecyclerView.Adapter<following_adapter_self.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;

    private ArrayList<Get_following_profile_pojo> gettingfollowinglist = new ArrayList<>();
    private Get_followerlist_pojo getFollowerlistPojo;

    private Get_following_profile_pojo getFollowingProfilePojo;

    UpdateUnfollowListener updateUnfollowListener;

    public interface UpdateUnfollowListener{
        void updateFollowingList();
    }

    public following_adapter_self(Context c, ArrayList<Get_following_profile_pojo> gettinglist, UpdateUnfollowListener updateUnfollowListener) {
        this.context = c;
        this.gettingfollowinglist = gettinglist;
        this.updateUnfollowListener = updateUnfollowListener;
    }

    @NonNull
    @Override
    public following_adapter_self.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_followinglist, parent, false);
        // set the view's size, margins, paddings and layout parameters
        following_adapter_self.MyViewHolder vh = new following_adapter_self.MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final following_adapter_self.MyViewHolder holder, final int position) {


        getFollowingProfilePojo = gettingfollowinglist.get(position);

        holder.txt_username.setText(getFollowingProfilePojo.getFirst_name());
        holder.txt_username.setTextColor(Color.parseColor("#000000"));

        holder.txt_userlastname.setText(getFollowingProfilePojo.getLast_name());
        holder.txt_userlastname.setTextColor(Color.parseColor("#000000"));

        String fullname = getFollowingProfilePojo.getFirst_name().concat(" ").concat(getFollowingProfilePojo.getLast_name());

        holder.txt_fullname_user.setText(fullname);

        String ownuser_id = Pref_storage.getDetail(context, "userId");

        String positionuser_id = gettingfollowinglist.get(position).getFollowing_id();


        Log.e("positionuser_id-->", "" + positionuser_id);
        Log.e("ownuser_id-->", "" + ownuser_id);

        Pref_storage.setDetail(context, "ClickedfollowingID", positionuser_id);


        if (Integer.parseInt(positionuser_id) == Integer.parseInt(ownuser_id)) {

            holder.rlayout_following.setVisibility(View.GONE);

        } else {
            holder.txt_following.setText(R.string.unfollow);
            holder.rl_follower_lay.setVisibility(View.VISIBLE);
            holder.rl_follower_ayout.setVisibility(View.VISIBLE);
            holder.rlayout_following.setVisibility(View.VISIBLE);


        }

        // Profile image loading

//        Utility.picassoImageLoader(gettingfollowinglist.get(position).getPicture(),0,holder.img_following,context );

        GlideApp.with(context)
                .load(gettingfollowinglist.get(position).getPicture())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)

                .into(holder.img_following);


        holder.img_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_id = gettingfollowinglist.get(position).getFollowing_id();

                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }
            }
        });


        holder.rlayout_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String positionuser_id = gettingfollowinglist.get(position).getFollowing_id();
                String following_image = gettingfollowinglist.get(position).getPicture();

//                String followingname = "Unfollow @ ".concat(gettingfollowinglist.get(position).getFirst_name().concat(" ").concat(gettingfollowinglist.get(position).getLast_name().concat(" ?")));
                String followingname = "Unfollow @ ".concat(gettingfollowinglist.get(position).getFirst_name().concat(" ?"));

                CustomDialogClass2 ccd = new CustomDialogClass2(context, positionuser_id, followingname, following_image, position);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                ccd.setCancelable(true);
                ccd.show();


            }
        });


    }


    //custom dialog for alert


    public class CustomDialogClass2 extends Dialog implements View.OnClickListener {

        public Activity c;
        public Dialog d;
        TextView txt_unfollow_yes, txt_unfollow_cancel, txt_alert;

        ImageView img_following;
        RelativeLayout rl_unfollowing_yes, rl_cancel;
        int position;
        String positionuser_id, followingname, following_image;

        CustomDialogClass2(Context a, String positionuser_id, String followingname, String following_image, int position) {
            super(a);
            // TODO Auto-generated constructor stub
            context = a;
            this.positionuser_id = positionuser_id;
            this.followingname = followingname;
            this.following_image = following_image;
            this.position = position;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_alert_dialog_timeline);

            txt_unfollow_yes = (TextView) findViewById(R.id.txt_unfollow_yes);

            txt_unfollow_cancel = (TextView) findViewById(R.id.txt_unfollow_cancel);

            txt_alert = (TextView) findViewById(R.id.txt_alert);
            txt_alert.setText(followingname);

            img_following = (ImageView) findViewById(R.id.img_following);
            rl_unfollowing_yes = (RelativeLayout) findViewById(R.id.rl_unfollowing_yes);
            rl_unfollowing_yes.setOnClickListener(this);
            rl_cancel = (RelativeLayout) findViewById(R.id.rl_cancel);
            rl_cancel.setOnClickListener(this);


            Utility.picassoImageLoader(following_image,0,img_following, context);


           /* GlideApp.with(context)
                    .load(following_image)
                    .placeholder(R.drawable.ic_add_photo)
                    .into(img_following);
*/

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.rl_unfollowing_yes:

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        String createdby = Pref_storage.getDetail(context, "userId");

                        String going_to_unfollow_user_id = positionuser_id;

                        Call<Get_unfollow_follower_output> call = apiService.create_unfollow("create_follower", Integer.parseInt(createdby), Integer.parseInt(going_to_unfollow_user_id), 0);
                        call.enqueue(new Callback<Get_unfollow_follower_output>() {
                            @Override
                            public void onResponse(Call<Get_unfollow_follower_output> call, Response<Get_unfollow_follower_output> response) {


                                if (response.body().getResponseMessage().equals("success")) {


                                    dismiss();

                                   /* context.startActivity(new Intent(context, Following_listActivity.class));
                                    ((AppCompatActivity) context).finish();*/

                                    Log.e("rl_unfollowing_yes", "onResponse: " + "Success");
//                                    deletePosition(position);
                                    updateUnfollowListener.updateFollowingList();

                                }

                            }

                            @Override
                            public void onFailure(Call<Get_unfollow_follower_output> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                                Log.e("rl_unfollowing_yes", "onResponse: " + t.getMessage());
                                updateUnfollowListener.updateFollowingList();
                                dismiss();


                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("rl_unfollowing_yes", "onResponse: " + e.toString());
                        updateUnfollowListener.updateFollowingList();
                        dismiss();

                    }

                    break;

                case R.id.rl_cancel:
                    dismiss();
                    break;
                default:
                    break;
            }
        }
    }


    public void removeAt(int position) {

        gettingfollowinglist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, gettingfollowinglist.size());

    }

    public static Drawable LoadImageFromWebURL(String url) {
        try {
            InputStream iStream = (InputStream) new URL(url).getContent();
            Drawable drawable = Drawable.createFromStream(iStream, "following name");
            return drawable;
        } catch (Exception e) {
            return null;
        }
    }




    @Override
    public int getItemCount() {

        return gettingfollowinglist.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_username, txt_userlastname, txt_fullname_user, txt_following, share, caption, captiontag, txt_created_on, txt_adapter_post;
        RelativeLayout rl_following, rl_likesnew_layout, rl_follower_lay, rl_follower_ayout, rlayout_following;

        ImageView img_following;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_userlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txt_fullname_user = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            txt_following = (TextView) itemView.findViewById(R.id.txt_following);
            img_following = (ImageView) itemView.findViewById(R.id.img_following);

            rl_follower_lay = (RelativeLayout) itemView.findViewById(R.id.rl_follower_lay);
            rl_follower_ayout = (RelativeLayout) itemView.findViewById(R.id.rl_follower_ayout);
            rlayout_following = (RelativeLayout) itemView.findViewById(R.id.rlayout_following);


        }


    }


}
