package com.kaspontech.foodwall.adapters.chat;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

public class Chat_from_adapter extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView txtMessageTime, txtChatMessage, txtChatDate, txtGroupUsername;
    RelativeLayout idRlChatSingle;
   EmojiconTextView emojiconTextView;
    ImageView imgUserSingle;

    private Chat_muliti_view_adapter.onRedirectPageListener onRedirectPageListener;

    private View viewSource;



    public Chat_from_adapter(View itemView) {
        super(itemView);

        txtMessageTime = (TextView) itemView.findViewById(R.id.txt_message_time);
        txtChatDate = (TextView) itemView.findViewById(R.id.txt_chat_date);

        emojiconTextView = (EmojiconTextView) itemView.findViewById(R.id.emojicon_text_view);
        txtChatMessage = (TextView) itemView.findViewById(R.id.txt_chat_message);
        imgUserSingle = (ImageView) itemView.findViewById(R.id.img_user_single);
        idRlChatSingle = (RelativeLayout) itemView.findViewById(R.id.id_rl_chat_single);


    }

    public Chat_from_adapter(View itemView, Chat_muliti_view_adapter.onRedirectPageListener onRedirectPageListener) {
        super(itemView);

        this.onRedirectPageListener = onRedirectPageListener;
        this.viewSource = itemView;

        txtMessageTime = (TextView) itemView.findViewById(R.id.txt_message_time);
        txtChatDate = (TextView) itemView.findViewById(R.id.txt_chat_date);

        emojiconTextView = (EmojiconTextView) itemView.findViewById(R.id.emojicon_text_view);
        txtChatMessage = (TextView) itemView.findViewById(R.id.txt_chat_message);
        imgUserSingle = (ImageView) itemView.findViewById(R.id.img_user_single);
        idRlChatSingle = (RelativeLayout) itemView.findViewById(R.id.id_rl_chat_single);

        emojiconTextView.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.emojicon_text_view:

                onRedirectPageListener.onRedirectPage(view,getAdapterPosition(),viewSource);
                break;
        }
    }
}
