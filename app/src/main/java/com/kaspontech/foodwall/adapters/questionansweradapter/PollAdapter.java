package com.kaspontech.foodwall.adapters.questionansweradapter;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter.Final_timeline_notification_adapter;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.questionspackage.QuestionAnswer;
import com.kaspontech.foodwall.R;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class PollAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {

    public EmojiconEditText edittxt_comment;
    public ImageButton ans_options;
    public CircleImageView userimage, userphoto_comment;
    public TextView userName, question, answeredOn, follow_count, txt_adapter_post, tv_view_all_comments, tv_headline_username, tv_question_headlinetext;

    public LinearLayout ll_answer_sharechat, ll_answer_section, ll_progress_section, ll_question_headline;
    public TextView tv_poll_answer_one, tv_poll_answer_two, tv_poll_answer_third, tv_poll_answer_four;
    public LinearLayout ll_poll_answer_one, ll_poll_answer_two, ll_poll_answer_third, ll_poll_answer_four;

    public LinearLayout ll_poll_progress_one, ll_poll_progress_two, ll_poll_progress_third, ll_poll_progress_four;
    public TextView poll_answer_one_percent, poll_answer_two_percent, poll_answer_third_percent, poll_answer_four_percent;
    public TextView poll_answer_one, poll_answer_two, poll_answer_third, poll_answer_four, tv_poll_undo, tv_poll_undo_timeline, total_poll_votes;
    public ImageView img_sharechat, poll_one_checked, poll_two_checked, poll_three_checked, poll_four_checked;
    public ProgressBar poll_answer_one_progress, poll_answer_two_progress, poll_answer_third_progress, poll_answer_four_progress, progressbar_review;

    QuestionAnswer questionAnswer;
    GetQuestionAllAdapter.onRefershListener onRefreshListener;
    Final_timeline_notification_adapter.onRefershListener onRefershListenerTimeLine;
    onCommentsPostListener onCommentsPostListenerView;
    View pollAdapterView;

    public PollAdapter(View itemView, QuestionAnswer questionAnswer1, Final_timeline_notification_adapter.onRefershListener onRefershListener, final onCommentsPostListener onCommentsPostListener) {

        super(itemView);


        this.questionAnswer = questionAnswer1;
        this.onRefershListenerTimeLine = onRefershListener;
        this.onCommentsPostListenerView = onCommentsPostListener;
        this.pollAdapterView = itemView;

        question = (TextView) itemView.findViewById(R.id.question);
        userName = (TextView) itemView.findViewById(R.id.user_name);
        answeredOn = (TextView) itemView.findViewById(R.id.answered_on);
        follow_count = (TextView) itemView.findViewById(R.id.follow_count);
        txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);
        tv_view_all_comments = (TextView) itemView.findViewById(R.id.tv_view_all_comments);
        tv_headline_username = (TextView) itemView.findViewById(R.id.tv_headline_username);
        tv_question_headlinetext = (TextView) itemView.findViewById(R.id.tv_question_headlinetext);


        userimage = (CircleImageView) itemView.findViewById(R.id.user_image);
        userphoto_comment = (CircleImageView) itemView.findViewById(R.id.userphoto_comment);
        ans_options = (ImageButton) itemView.findViewById(R.id.ans_options);

        edittxt_comment = (EmojiconEditText) itemView.findViewById(R.id.edittxt_comment);
        img_sharechat = (ImageView) itemView.findViewById(R.id.img_sharechat);


        // Display options
        ll_answer_section = (LinearLayout) itemView.findViewById(R.id.ll_answer_section);
        ll_question_headline = (LinearLayout) itemView.findViewById(R.id.ll_question_headline);
        ll_answer_sharechat = (LinearLayout) itemView.findViewById(R.id.ll_answer_sharechat_poll);


        // main bind
        ll_poll_answer_one = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_one);
        ll_poll_answer_two = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_two);
        ll_poll_answer_third = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_third);
        ll_poll_answer_four = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_four);

        // poll name
        tv_poll_answer_one = (TextView) itemView.findViewById(R.id.tv_poll_answer_one);
        tv_poll_answer_two = (TextView) itemView.findViewById(R.id.tv_poll_answer_two);
        tv_poll_answer_third = (TextView) itemView.findViewById(R.id.tv_poll_answer_third);
        tv_poll_answer_four = (TextView) itemView.findViewById(R.id.tv_poll_answer_four);


        // Display progress
        ll_progress_section = (LinearLayout) itemView.findViewById(R.id.ll_progress_section);

        // main bind
        ll_poll_progress_one = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_one);
        ll_poll_progress_two = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_two);
        ll_poll_progress_third = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_third);
        ll_poll_progress_four = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_four);

        // poll name
        poll_answer_one = (TextView) itemView.findViewById(R.id.polling_answer_one);
        poll_answer_two = (TextView) itemView.findViewById(R.id.polling_answer_two);
        poll_answer_third = (TextView) itemView.findViewById(R.id.polling_answer_three);
        poll_answer_four = (TextView) itemView.findViewById(R.id.polling_answer_four);
        tv_poll_undo_timeline = (TextView) itemView.findViewById(R.id.tv_poll_undo);
        total_poll_votes = (TextView) itemView.findViewById(R.id.total_poll_votes);

        // poll percent
        poll_answer_one_percent = (TextView) itemView.findViewById(R.id.poll_answer_one_percent);
        poll_answer_two_percent = (TextView) itemView.findViewById(R.id.poll_answer_two_percent);
        poll_answer_third_percent = (TextView) itemView.findViewById(R.id.poll_answer_third_percent);
        poll_answer_four_percent = (TextView) itemView.findViewById(R.id.poll_answer_four_percent);

        // poll user checked
        poll_one_checked = (ImageView) itemView.findViewById(R.id.poll_one_checked);
        poll_two_checked = (ImageView) itemView.findViewById(R.id.poll_two_checked);
        poll_three_checked = (ImageView) itemView.findViewById(R.id.poll_three_checked);
        poll_four_checked = (ImageView) itemView.findViewById(R.id.poll_four_checked);

        // poll progressbar
        poll_answer_one_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_one_progress);
        poll_answer_two_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_two_progress);
        poll_answer_third_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_third_progress);
        poll_answer_four_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_four_progress);
        progressbar_review = (ProgressBar) itemView.findViewById(R.id.progressbar_review);


        tv_poll_undo_timeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRefershListenerTimeLine.onRefereshedFragment(view, getAdapterPosition(), pollAdapterView);
            }
        });

        txt_adapter_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCommentsPostListenerView.onPostCommitedView(view, getAdapterPosition(), pollAdapterView);
            }
        });

        img_sharechat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCommentsPostListenerView.onPostCommitedView(view, getAdapterPosition(), pollAdapterView);
            }
        });
    }

    public PollAdapter(View pollView, QuestionAnswer questionAnswer, GetQuestionAllAdapter.onRefershListener onRefershListener) {

        super(pollView);


        this.questionAnswer = questionAnswer;
        this.onRefreshListener = onRefershListener;
        this.pollAdapterView = pollView;

        question = (TextView) itemView.findViewById(R.id.question);
        userName = (TextView) itemView.findViewById(R.id.user_name);
        answeredOn = (TextView) itemView.findViewById(R.id.answered_on);
        follow_count = (TextView) itemView.findViewById(R.id.follow_count);
        txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);
        tv_view_all_comments = (TextView) itemView.findViewById(R.id.tv_view_all_comments);
        tv_headline_username = (TextView) itemView.findViewById(R.id.tv_headline_username);
        tv_question_headlinetext = (TextView) itemView.findViewById(R.id.tv_question_headlinetext);


        userimage = (CircleImageView) itemView.findViewById(R.id.user_image);
        userphoto_comment = (CircleImageView) itemView.findViewById(R.id.userphoto_comment);
        ans_options = (ImageButton) itemView.findViewById(R.id.ans_options);

        edittxt_comment = (EmojiconEditText) itemView.findViewById(R.id.edittxt_comment);


        // Display options
        ll_answer_section = (LinearLayout) itemView.findViewById(R.id.ll_answer_section);
        ll_question_headline = (LinearLayout) itemView.findViewById(R.id.ll_question_headline);


        // main bind
        ll_poll_answer_one = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_one);
        ll_poll_answer_two = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_two);
        ll_poll_answer_third = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_third);
        ll_poll_answer_four = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_four);

        // poll name
        tv_poll_answer_one = (TextView) itemView.findViewById(R.id.tv_poll_answer_one);
        tv_poll_answer_two = (TextView) itemView.findViewById(R.id.tv_poll_answer_two);
        tv_poll_answer_third = (TextView) itemView.findViewById(R.id.tv_poll_answer_third);
        tv_poll_answer_four = (TextView) itemView.findViewById(R.id.tv_poll_answer_four);
        img_sharechat = (ImageView) itemView.findViewById(R.id.img_sharechat);


        // Display progress
        ll_progress_section = (LinearLayout) itemView.findViewById(R.id.ll_progress_section);

        // main bind
        ll_poll_progress_one = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_one);
        ll_poll_progress_two = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_two);
        ll_poll_progress_third = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_third);
        ll_poll_progress_four = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_four);

        // poll name
        poll_answer_one = (TextView) itemView.findViewById(R.id.polling_answer_one);
        poll_answer_two = (TextView) itemView.findViewById(R.id.polling_answer_two);
        poll_answer_third = (TextView) itemView.findViewById(R.id.polling_answer_three);
        poll_answer_four = (TextView) itemView.findViewById(R.id.polling_answer_four);
        tv_poll_undo = (TextView) itemView.findViewById(R.id.tv_poll_undo);
        total_poll_votes = (TextView) itemView.findViewById(R.id.total_poll_votes);

        // poll percent
        poll_answer_one_percent = (TextView) itemView.findViewById(R.id.poll_answer_one_percent);
        poll_answer_two_percent = (TextView) itemView.findViewById(R.id.poll_answer_two_percent);
        poll_answer_third_percent = (TextView) itemView.findViewById(R.id.poll_answer_third_percent);
        poll_answer_four_percent = (TextView) itemView.findViewById(R.id.poll_answer_four_percent);

        // poll user checked
        poll_one_checked = (ImageView) itemView.findViewById(R.id.poll_one_checked);
        poll_two_checked = (ImageView) itemView.findViewById(R.id.poll_two_checked);
        poll_three_checked = (ImageView) itemView.findViewById(R.id.poll_three_checked);
        poll_four_checked = (ImageView) itemView.findViewById(R.id.poll_four_checked);

        // poll progressbar
        poll_answer_one_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_one_progress);
        poll_answer_two_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_two_progress);
        poll_answer_third_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_third_progress);
        poll_answer_four_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_four_progress);
        progressbar_review = (ProgressBar) itemView.findViewById(R.id.progressbar_review);

        ll_answer_sharechat = (LinearLayout) itemView.findViewById(R.id.ll_answer_sharechat);

        tv_poll_undo.setOnClickListener(this);
        txt_adapter_post.setOnClickListener(this);
        edittxt_comment.setOnClickListener(this);
        img_sharechat.setOnClickListener(this);

    }

    public PollAdapter(View pollView, QuestionAnswer questionAnswer) {
        super(pollView);


        this.questionAnswer = questionAnswer;

        //this.onRefreshListener = onRefershListener;
        // this.pollAdapterView = pollView;
        question = (TextView) itemView.findViewById(R.id.question);
        userName = (TextView) itemView.findViewById(R.id.user_name);
        answeredOn = (TextView) itemView.findViewById(R.id.answered_on);
        follow_count = (TextView) itemView.findViewById(R.id.follow_count);
        txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);
        tv_view_all_comments = (TextView) itemView.findViewById(R.id.tv_view_all_comments);
        tv_headline_username = (TextView) itemView.findViewById(R.id.tv_headline_username);
        tv_question_headlinetext = (TextView) itemView.findViewById(R.id.tv_question_headlinetext);


        userimage = (CircleImageView) itemView.findViewById(R.id.user_image);
        userphoto_comment = (CircleImageView) itemView.findViewById(R.id.userphoto_comment);
        ans_options = (ImageButton) itemView.findViewById(R.id.ans_options);

        edittxt_comment = (EmojiconEditText) itemView.findViewById(R.id.edittxt_comment);


        // Display options
        ll_answer_section = (LinearLayout) itemView.findViewById(R.id.ll_answer_section);
        ll_question_headline = (LinearLayout) itemView.findViewById(R.id.ll_question_headline);


        // main bind
        ll_poll_answer_one = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_one);
        ll_poll_answer_two = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_two);
        ll_poll_answer_third = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_third);
        ll_poll_answer_four = (LinearLayout) itemView.findViewById(R.id.ll_poll_answer_four);

        // poll name
        tv_poll_answer_one = (TextView) itemView.findViewById(R.id.tv_poll_answer_one);
        tv_poll_answer_two = (TextView) itemView.findViewById(R.id.tv_poll_answer_two);
        tv_poll_answer_third = (TextView) itemView.findViewById(R.id.tv_poll_answer_third);
        tv_poll_answer_four = (TextView) itemView.findViewById(R.id.tv_poll_answer_four);


        // Display progress
        ll_progress_section = (LinearLayout) itemView.findViewById(R.id.ll_progress_section);

        // main bind
        ll_poll_progress_one = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_one);
        ll_poll_progress_two = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_two);
        ll_poll_progress_third = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_third);
        ll_poll_progress_four = (LinearLayout) itemView.findViewById(R.id.ll_poll_progress_four);

        // poll name
        poll_answer_one = (TextView) itemView.findViewById(R.id.polling_answer_one);
        poll_answer_two = (TextView) itemView.findViewById(R.id.polling_answer_two);
        poll_answer_third = (TextView) itemView.findViewById(R.id.polling_answer_three);
        poll_answer_four = (TextView) itemView.findViewById(R.id.polling_answer_four);
        tv_poll_undo = (TextView) itemView.findViewById(R.id.tv_poll_undo);
        total_poll_votes = (TextView) itemView.findViewById(R.id.total_poll_votes);

        // poll percent
        poll_answer_one_percent = (TextView) itemView.findViewById(R.id.poll_answer_one_percent);
        poll_answer_two_percent = (TextView) itemView.findViewById(R.id.poll_answer_two_percent);
        poll_answer_third_percent = (TextView) itemView.findViewById(R.id.poll_answer_third_percent);
        poll_answer_four_percent = (TextView) itemView.findViewById(R.id.poll_answer_four_percent);

        // poll user checked
        poll_one_checked = (ImageView) itemView.findViewById(R.id.poll_one_checked);
        poll_two_checked = (ImageView) itemView.findViewById(R.id.poll_two_checked);
        poll_three_checked = (ImageView) itemView.findViewById(R.id.poll_three_checked);
        poll_four_checked = (ImageView) itemView.findViewById(R.id.poll_four_checked);

        // poll progressbar
        poll_answer_one_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_one_progress);
        poll_answer_two_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_two_progress);
        poll_answer_third_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_third_progress);
        poll_answer_four_progress = (ProgressBar) itemView.findViewById(R.id.poll_answer_four_progress);
        progressbar_review = (ProgressBar) itemView.findViewById(R.id.progressbar_review);
        img_sharechat = (ImageView) itemView.findViewById(R.id.img_sharechat);


        // tv_poll_undo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tv_poll_undo:

                onRefreshListener.onRefereshedFragment(view, getAdapterPosition(), pollAdapterView);
                break;

            case R.id.txt_adapter_post:

                onRefreshListener.onRefereshedFragment(view, getAdapterPosition(), pollAdapterView);
                break;

            case R.id.edittxt_comment:
                onRefreshListener.onRefereshedFragment(view, getAdapterPosition(), pollAdapterView);
                break;

            case R.id.img_sharechat:
                onRefreshListener.onRefereshedFragment(view,getAdapterPosition(),pollAdapterView);
                break;
        }
    }
}