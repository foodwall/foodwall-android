package com.kaspontech.foodwall.adapters.events;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventsComments.EventsCommentsUserData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by balaji on 30/3/18.
 */

public class PostCommentEventAdapter extends RecyclerView.Adapter<PostCommentEventAdapter.MyViewHolder> {

    /**
     * Context
     **/
    Context context;

    /**
     * Arraylist
     **/
    List<EventsCommentsUserData> getEventsCommentsPojoList = new ArrayList<>();

    /**
     * Event pojo details
     **/
    EventsCommentsUserData getEventsCommentsPojo;

    /**
     * int variables
     **/
    int eventPosition;

    public PostCommentEventAdapter(Context context, List<EventsCommentsUserData> getEventsCommentsPojoList, int eventPosition) {
        this.context = context;
        this.getEventsCommentsPojoList = getEventsCommentsPojoList;
        this.eventPosition = eventPosition;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView userImage;
        TextView userName;
        TextView eventComments;
        TextView commentedTime;
        TextView postComment;
        TextView cancelButton;
        ImageButton evtCmtOptions;
        EditText edittxt_comment;
        LinearLayout ll_edit_comment;

        public MyViewHolder(View view) {
            super(view);

            userImage = (CircleImageView) view.findViewById(R.id.user_image);
            userName = (TextView) view.findViewById(R.id.user_name);
            eventComments = (TextView) view.findViewById(R.id.event_comments);
            commentedTime = (TextView) view.findViewById(R.id.commented_time);
            postComment = (TextView) view.findViewById(R.id.txt_adapter_post);
            cancelButton = (TextView) view.findViewById(R.id.txt_adapter_cancel);
            evtCmtOptions = (ImageButton) view.findViewById(R.id.evt_cmt_options);
            ll_edit_comment = (LinearLayout) view.findViewById(R.id.ll_edit_comment);
            edittxt_comment = (EditText) view.findViewById(R.id.edittxt_comment);


        }

    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_get_all_events_comments, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        getEventsCommentsPojo = getEventsCommentsPojoList.get(position);

        /*Image loading*/
        Utility.picassoImageLoader(getEventsCommentsPojo.getPicture(),0,holder.userImage,context);

        /*User name setting */
        holder.userName.setText(getEventsCommentsPojo.getFirstName() + " " + getEventsCommentsPojo.getLastName());


        /*User event comments setting */
        holder.eventComments.setText(getEventsCommentsPojo.getEvtComments());

        /*Variable*/

        String userid = Pref_storage.getDetail(context, "userId");
        String createdByid = getEventsCommentsPojoList.get(position).getCreatedBy();
        final int event_id = Integer.parseInt(getEventsCommentsPojoList.get(position).getEventId());
        final int comment_id = Integer.parseInt(getEventsCommentsPojoList.get(position).getCmmtEvtId());

        Log.e("eventscheck", "userid->" + userid);
        Log.e("eventscheck", "createdby->" + createdByid);



        if (createdByid.contains(userid)) {

            holder.evtCmtOptions.setVisibility(View.VISIBLE);


        } else {


            holder.evtCmtOptions.setVisibility(View.GONE);

        }

        /*Event comment options*/

        holder.evtCmtOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //creating a popup menu
                PopupMenu popup = new PopupMenu(context, holder.evtCmtOptions);

                //inflating menu from xml resource
                popup.inflate(R.menu.events_comments_options);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.menu0:

                                holder.ll_edit_comment.setVisibility(View.VISIBLE);
                                holder.eventComments.setVisibility(View.GONE);

                                break;

                            case R.id.menu1:

                                String cmt_id = getEventsCommentsPojoList.get(position).getCmmtEvtId();
                                int comment_id = Integer.parseInt(cmt_id);
                                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Are you sure to delete this comment?")
                                        .setConfirmText("Yes,delete it!")
                                        .setCancelText("No,cancel!")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                if(Utility.isConnected(context)){
                                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                                try {

                                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                                                    int cmt_id = Integer.parseInt(getEventsCommentsPojoList.get(position).getCmmtEvtId());
                                                    int evt_id = Integer.parseInt(getEventsCommentsPojoList.get(position).getEventId());

                                                    Call<CommonOutputPojo> call = apiService.deleteEventComment("create_delete_events_comments", cmt_id, evt_id, userid);

                                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                                        @Override
                                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                            String responseStatus = response.body().getResponseMessage();

                                                            Log.e("deleteEventComment", "responseStatus->" + responseStatus);


                                                            if (responseStatus.equals("success")) {

                                                                //Success
                                                                Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                                                                removeAt(position);
                                                               /* getEventsCommentsPojoList.remove(position);
                                                                notifyItemRemoved(position);
                                                                notifyItemRangeChanged(position, getEventsCommentsPojoList.size());*/

                                                                /*context.startActivity(new Intent(context, ViewEvent.class));
                                                                ((Activity)context).finish();*/

                                                            /*    Intent intent = new Intent(context, ViewEvent.class);
                                                                Bundle bundle = new Bundle();
                                                                bundle.putInt("position", eventPosition);
                                                                intent.putExtras(bundle);
                                                                context.startActivity(intent);
                                                                ((Activity) context).finish();*/


                                                            } else if (responseStatus.equals("nodata")) {


                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                            //Error
                                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                                        }
                                                    });

                                                } catch (Exception e) {

                                                    e.printStackTrace();

                                                }}else {
                                                    Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                                }


                                            }
                                        })
                                        .show();

                                break;

                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();


            }
        });

        /*Cancel button on click listener*/
        holder.cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.ll_edit_comment.setVisibility(View.GONE);
                holder.eventComments.setVisibility(View.VISIBLE);

            }
        });


        /*Post comment on click listener*/
        holder.postComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.edittxt_comment.getText().toString().length() > 0) {
                    if(Utility.isConnected(context)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiService.createEventComment("create_edit_events_comments", comment_id, event_id, holder.edittxt_comment.getText().toString(), userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("createEventComment", "responseStatus->" + responseStatus);


                                    if (responseStatus.equals("success")) {


                                        holder.eventComments.setText(holder.edittxt_comment.getText().toString());
                                        holder.ll_edit_comment.setVisibility(View.GONE);
                                        holder.eventComments.setVisibility(View.VISIBLE);
                                    /*notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, getEventsCommentsPojoList.size());
                                    Intent intent = new Intent(context, ViewEvent.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putInt("position", eventPosition);
                                    intent.putExtras(bundle);
                                    context.startActivity(intent);
                                    ((Activity) context).finish();*/

                                    } else if (responseStatus.equals("nodata")) {


                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }}else {
                        Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }


                } else {

                    holder.edittxt_comment.setError("Enter your comment");
                }


            }
        });

        /*Setting date*/
        String createdon = getEventsCommentsPojoList.get(position).getCreatedOn();

        Utility.setTimeStamp(createdon,holder.commentedTime);

        Log.e("createdon", "createdon\t" + createdon);




    }

    /*Remove postion*/
    public void removeAt(int position) {
        getEventsCommentsPojoList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getEventsCommentsPojoList.size());
    }

    /*private void createEventComment(int event_id,int createType){


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            int userid = Integer.parseInt(Pref_storage.getDetail(ViewEvent.this,"userId"));

//            RequestBody eventComment = RequestBody.create(MediaType.parse("text/plain"),edittxt_comment.getText().toString());

            Call<CommonOutputPojo> call = apiService.createEventComment( "create_edit_events_comments",createType,event_id,edittxt_comment.getText().toString(),userid);

            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    String responseStatus = response.body().getResponseMessage();

                    Log.e("editEventComment","responseStatus->"+responseStatus);


                    if (responseStatus.equals("success")) {

                        //Success

                    }else if(responseStatus.equals("nodata")){


                    }
                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError","FailureError"+t.getMessage());
                }
            });

        }catch (Exception e ){

            e.printStackTrace();

        }


    }
*/


    @Override
    public int getItemCount() {
        return getEventsCommentsPojoList.size();
    }
}
