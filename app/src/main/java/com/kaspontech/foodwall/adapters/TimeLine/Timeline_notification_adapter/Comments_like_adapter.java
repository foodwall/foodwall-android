package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Likes_pojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class Comments_like_adapter extends RecyclerView.Adapter<Comments_like_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     **/

    Context context;

    /**
     * Arraylist for likes
     **/

    ArrayList<Likes_pojo> gettinglikeslist = new ArrayList<>();

    ArrayList<Get_following_profile_pojo> gettingfollowerlist = new ArrayList<>();


    /**
     * Model class for like
     **/
    Likes_pojo likesPojo;
    /**
     * Model class for following
     **/
    Get_following_profile_pojo getFollowingProfilePojo;

    Handler doubleHandler;
    /**
     * Shared preference
     **/
    Pref_storage pref_storage;

    boolean doubleClick = false;


    public Comments_like_adapter(Context c, ArrayList<Likes_pojo> gettinglist) {
        this.context = c;
        this.gettinglikeslist = gettinglist;
    }


    @NonNull
    @Override
    public Comments_like_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_likes, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Comments_like_adapter.MyViewHolder vh = new Comments_like_adapter.MyViewHolder(v);

        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Comments_like_adapter.MyViewHolder holder, final int position) {
        /*Likes pojo initialization*/

        likesPojo = gettinglikeslist.get(position);

        /*Setting Likes activity fields*/

        /*Setting User first name*/

        holder.txt_usernamelike.setText(likesPojo.getFirstName());
        holder.txt_usernamelike.setTextColor(Color.parseColor("#000000"));

        /*Setting Last name*/

        holder.txt_userlastnamelike.setText(likesPojo.getLastName());
        holder.txt_userlastnamelike.setTextColor(Color.parseColor("#000000"));

        /*Setting Full name*/

        String fullname = gettinglikeslist.get(position).getFirstName().concat(" ").concat(gettinglikeslist.get(position).getLastName());
        holder.txt_fullname_userlike.setText(fullname);

        /*Setting user image*/

        Utility.picassoImageLoader(gettinglikeslist.get(position).getPicture(),
                0,holder.img_like, context);




//        if (Pref_storage.getDetail(context, "userId").equals(gettinglikeslist.get(position).getUserId())) {
//            holder.rl_follow.setVisibility(View.GONE);
//
//        } else {
//            holder.rl_follow.setVisibility(View.VISIBLE);
//        }


//        holder.rl_follow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String positionuser_id = gettinglikeslist.get(position).getUserId();
////        Toast.makeText(context, ""+positionuser_id, Toast.LENGTH_SHORT).show();
//
//                Pref_storage.setDetail(context, "ClickedLikeuserID", positionuser_id);
//
//                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//                try {
//
//                    String createdby = Pref_storage.getDetail(context, "userId");
//
//                    String going_to_follow_user_id = Pref_storage.getDetail(context, "ClickedLikeuserID");
//
//                    Call<Get_following_output_Pojo> call = apiService.create_follower("create_follower", Integer.parseInt(createdby), Integer.parseInt(going_to_follow_user_id), 1);
//                    call.enqueue(new Callback<Get_following_output_Pojo>() {
//                        @Override
//                        public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {
//
//                            if (response.body().getResponseCode() == 1) {
//
//
//                                for (int j = 0; j < response.body().getData().size(); j++) {
//
//
//                                    String timelineId = response.body().getData().get(j).getFollowing_id();
//
//                                    String createdOn = response.body().getData().get(j).getCreated_on();
//                                    String Total_followers = response.body().getData().get(j).getTotal_followers();
//                                    String total_followings = response.body().getData().get(j).getTotal_followings();
//                                    String userId = response.body().getData().get(j).getUser_id();
//                                    String oauthProvider = response.body().getData().get(j).getOauth_provider();
//                                    String oauthUid = response.body().getData().get(j).getOauth_uid();
//                                    String firstName1 = response.body().getData().get(j).getFirst_name();
//                                    String lastName1 = response.body().getData().get(j).getLast_name();
//                                    String location = response.body().getData().get(j).getLatitude();
//                                    String locationstate = response.body().getData().get(j).getLongitude();
//                                    String email1 = response.body().getData().get(j).getEmail();
//                                    String contNo = response.body().getData().get(j).getCont_no();
//                                    String gender = response.body().getData().get(j).getGender();
//                                    String dob = response.body().getData().get(j).getDob();
//                                    String following_picture = response.body().getData().get(j).getPicture();
//                                    holder.txt_follow.setText(R.string.following);
////                                                    holder.txt_follow.setText(R.string.follow);
//
//                                    /*         context.startActivity(new Intent(context, Followerlist_Activity.class));*/
////                                                    ((AppCompatActivity) context).finish();
//
//                                }
//
//
//                            }
//
//                        }
//
//                        @Override
//                        public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
//                            //Error
//                            Log.e("FailureError", "" + t.getMessage());
//                        }
//                    });
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        });


        /* Overall layout on click listener & redirecting to profile page of user's */
        holder.rl_likesnew_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_id = gettinglikeslist.get(position).getUserId();

                if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    context.startActivity(new Intent(context, Profile.class));

                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", user_id);
                    context.startActivity(intent);

                }


            }


        });


       /* String user_id= gettinglikeslist.get(position).getUser_id();


        String followeruser_id= gettingfollowerlist.get(position).getFollowing_id();
        if(Integer.parseInt(user_id)==Integer.parseInt(followeruser_id)){
            holder.txt_follow.setText(R.string.following);

        } else {
            holder.txt_follow.setText(R.string.follow);
        }*/


    }


    public void create_following() {


    }


    @Override
    public int getItemCount() {

        return gettinglikeslist.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_usernamelike, txt_userlastnamelike, txt_fullname_userlike, txt_follow, share, caption, captiontag, txt_created_on, txt_adapter_post;
        RelativeLayout rl_follow, rl_likesnew_layout;

        ImageView img_like;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_usernamelike = (TextView) itemView.findViewById(R.id.txt_usernamelike);
            txt_userlastnamelike = (TextView) itemView.findViewById(R.id.txt_userlastnamelike);
            txt_fullname_userlike = (TextView) itemView.findViewById(R.id.txt_fullname_userlike);
            txt_follow = (TextView) itemView.findViewById(R.id.txt_follow);
            img_like = (ImageView) itemView.findViewById(R.id.img_like);
            rl_follow = (RelativeLayout) itemView.findViewById(R.id.rl_follow);
            rl_likesnew_layout = (RelativeLayout) itemView.findViewById(R.id.rl_likesnew_layout);


        }


    }


}
