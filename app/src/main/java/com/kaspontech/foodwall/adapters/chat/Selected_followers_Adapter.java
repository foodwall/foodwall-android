/*
package com.kaspontech.foodwall.adapters.Chat;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.kaspontech.foodwall.adapters.Events.SelectCohostAdapter;
import com.kaspontech.foodwall.eventsPackage.CreateEvent;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Selected_followers_Adapter extends RecyclerView.Adapter<SelectCohostAdapter.MyViewHolder> {


    Context context;
    CreateEvent createEvent;
    List<GetFollowersData> getFollowersDataList = new ArrayList<>();
    HashMap<String, Integer> cohostNameIdList = new HashMap<>();

    GetFollowersData getFollowersData;
    static boolean checked = false;

    public Selected_followers_Adapter(Context context, List<GetFollowersData> getFollowersDataList, HashMap<String, Integer> cohostNameIdList) {
        this.context = context;
        this.getFollowersDataList = getFollowersDataList;
        this.cohostNameIdList = cohostNameIdList;
        createEvent = (CreateEvent) context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {


        CircleImageView followerPhoto;
        TextView userName;
        CheckBox selectFollowerBox;
        CreateEvent createEvent;

        public MyViewHolder(View view, CreateEvent createEvent) {
            super(view);

            this.createEvent = createEvent;
            followerPhoto = (CircleImageView)view.findViewById(R.id.user_image);
            userName = (TextView)view.findViewById(R.id.user_name);
            selectFollowerBox = (CheckBox)view.findViewById(R.id.followers_checked);
            selectFollowerBox.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            createEvent.getSelectedCoHosts(view,getFollowersDataList.get(getAdapterPosition()),getAdapterPosition());

        }
    }


    @NonNull
    @Override
    public SelectCohostAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_select_followers, parent, false);
        return new SelectCohostAdapter.MyViewHolder(itemView, createEvent);

    }

    @Override
    public void onBindViewHolder(@NonNull final SelectCohostAdapter.MyViewHolder holder, int position) {

        getFollowersData = getFollowersDataList.get(position);


     */
/*   Glide.with(context).load(getFollowersData.getPicture()).into(holder.followerPhoto);
        holder.userName.setText(getFollowersData.getFirstName() + " " + getFollowersData.getLastName());


        for (Object value : cohostNameIdList.values()) {

            if (getFollowersData.getFollowerId().contains(value.toString())) {

                holder.selectFollowerBox.setChecked(true);

            }

        }
*//*


    }

    @Override
    public int getItemCount() {
        return getFollowersDataList.size();
    }


}
*/
