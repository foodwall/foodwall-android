package com.kaspontech.foodwall.adapters.TimeLine;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Timeline_image_only_pojo {


    @SerializedName("timelineid")
    @Expose
    private String timelineid;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("timelineimage")
    @Expose
    private String timelineimage;

    /**
     * No args constructor for use in serialization
     */
    public Timeline_image_only_pojo() {
    }

    /**
     * @param timelineid
     * @param timelineimage
     * @param userid
     */
    public Timeline_image_only_pojo(String timelineid, String userid, String timelineimage) {
        super();
        this.timelineid = timelineid;
        this.userid = userid;
        this.timelineimage = timelineimage;
    }

    public String getTimelineid() {
        return timelineid;
    }

    public void setTimelineid(String timelineid) {
        this.timelineid = timelineid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getTimelineimage() {
        return timelineimage;
    }

    public void setTimelineimage(String timelineimage) {
        this.timelineimage = timelineimage;
    }
}
