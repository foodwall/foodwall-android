package com.kaspontech.foodwall.adapters.Review_Image_adapter;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kaspontech.foodwall.reviewpackage.allTab.AllFragment;
import com.kaspontech.foodwall.reviewpackage.deliveryTab.DeliveryFragment;
import com.kaspontech.foodwall.reviewpackage.dineInTab.NewDineInFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

/*
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new AllFragment(); //AllFragment at position 0
            case 1:
                return new NewDineInFragment(); //NewDineInFragment at position 1
            case 2:
                return new DeliveryFragment(); //DeliveryFragment at position 2
        }
        return null; //does not happen
    }

    @Override
    public int getCount() {
        return 3; //three fragments
    }*/



    private Fragment[] childFragments;

    @RequiresApi(api = Build.VERSION_CODES.M)
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        childFragments = new Fragment[] {
                new AllFragment(), //0
                new NewDineInFragment(), //1
                new DeliveryFragment() //2
        };
    }

    @Override
    public Fragment getItem(int position) {
        return childFragments[position];
    }

    @Override
    public int getCount() {
        return childFragments.length; //3 items
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = getItem(position).getClass().getName();


        if(title.contains("AllFragment")){
            title = "ALL";
        }else if(title.contains("NewDineInFragment")){
            title = "DINE IN";

        }else{
            title = "DELIVERY";

        }
        return title.subSequence(title.lastIndexOf(".") + 1, title.length());
    }
}