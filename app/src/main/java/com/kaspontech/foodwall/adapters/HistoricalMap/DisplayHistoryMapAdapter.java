package com.kaspontech.foodwall.adapters.HistoricalMap;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.DisplayPojo.GetHistoryMapData;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.TaggedPeoplePojo;
import com.kaspontech.foodwall.profilePackage.Image_self_activity;
import com.kaspontech.foodwall.profilePackage.profileTabFragments.viewFullImages.TimeLineImageFullView;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.GlideApp;


import java.util.ArrayList;
import java.util.List;

public class DisplayHistoryMapAdapter  extends RecyclerView.Adapter<DisplayHistoryMapAdapter.MyViewHolder> {


    private Context context;
    private List<GetHistoryMapData> getHistoryMapDataList;
    private GetHistoryMapData getHistoryMapData;
    private int prev,next;


    public DisplayHistoryMapAdapter(Context context, List<GetHistoryMapData> getHistoryMapDataList) {
        this.context = context;
        this.getHistoryMapDataList = getHistoryMapDataList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView thumb_image;
        LinearLayout ll_tagged_people,ll_view_post,ll_multi_img;
        TextView tv_hotel_name,tv_tagged_people,tv_visited_date,tv_img_count;


        public MyViewHolder(View view ) {
            super(view);


            tv_hotel_name = (TextView)view.findViewById(R.id.tv_hotel_name);
            tv_visited_date = (TextView)view.findViewById(R.id.tv_visited_date);
            tv_tagged_people = (TextView)view.findViewById(R.id.tv_tagged_people);
            tv_img_count = (TextView)view.findViewById(R.id.tv_img_count);
            ll_tagged_people = (LinearLayout)view.findViewById(R.id.ll_tagged_people);
            ll_view_post = (LinearLayout)view.findViewById(R.id.ll_view_post);
            ll_multi_img = (LinearLayout)view.findViewById(R.id.ll_multi_img);
            thumb_image = (ImageView)view.findViewById(R.id.thumb_image);



        }


    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_historical_map_cardview, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final DisplayHistoryMapAdapter.MyViewHolder holder, final int position) {

        getHistoryMapData = getHistoryMapDataList.get(position);


        final String userid = getHistoryMapDataList.get(position).getUserId() ;

        holder.tv_hotel_name.setText(getHistoryMapData.getHotelName());
        String day = getHistoryMapDataList.get(position).getCreatedOn().substring(8,10);

        holder.tv_visited_date.setText(day);


        holder.ll_view_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,Image_self_activity.class);
                intent.putExtra("Clickedfrom_profile",getHistoryMapDataList.get(position).getTimeline_id());
                intent.putExtra("Clickedfrom_hotelname",getHistoryMapDataList.get(position).getHotelName());
                intent.putExtra("Clickedfrom_profileuserid",getHistoryMapDataList.get(position).getUserId());
                context.startActivity(intent);

            }
        });




        List<TaggedPeoplePojo> taggedList = new ArrayList<>();

        if(getHistoryMapData.getWhom_count() != 0){


            holder.ll_tagged_people.setVisibility(View.VISIBLE);
            taggedList = getHistoryMapData.getWhom();

            List<String> taggedUserName = new ArrayList<>();

            for (TaggedPeoplePojo taggedPeople:taggedList) {

                String userName = taggedPeople.getFirstName()+" "+taggedPeople.getLastName();
                taggedUserName.add(userName);

            }

            String setUserName = "";

            if(taggedUserName.size() > 1){


                if(taggedUserName.size() == 2){

                    setUserName = taggedUserName.get(0) + " and "+taggedUserName.get(1);
                    holder.tv_tagged_people.setText(setUserName);

                }else{

                    setUserName = taggedUserName.get(0) + " and "+String.valueOf(taggedUserName.size()-1)+" Others";
                    holder.tv_tagged_people.setText(setUserName);
                }



            }else{

                setUserName = taggedUserName.get(0);
                holder.tv_tagged_people.setText(setUserName);

            }


        }else {

            holder.ll_tagged_people.setVisibility(View.GONE);

        }



        int imageCount = getHistoryMapData.getImageCount();

        if(imageCount != 0){

            if(imageCount == 1){

                holder.ll_multi_img.setVisibility(View.GONE);

                String imageUrl = getHistoryMapData.getImage().get(0).getImg();

                RequestOptions requestOptions = new RequestOptions();
                requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));




                GlideApp.with(context)
                        .load(imageUrl)
                        .apply(requestOptions)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.ic_no_post)
                        .thumbnail(0.1f)
                        .into(holder.thumb_image);

            }else if(imageCount > 1){

                String imageCountText = String.valueOf(imageCount);

                holder.ll_multi_img.setVisibility(View.VISIBLE);

                holder.tv_img_count.setText(imageCountText);

                String imageUrl = getHistoryMapData.getImage().get(0).getImg();

                RequestOptions requestOptions = new RequestOptions();
                requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));
                GlideApp.with(context)
                        .load(imageUrl)
                        .apply(requestOptions)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.ic_no_post)
                        .thumbnail(0.1f)
                        .into(holder.thumb_image);


            }




        }




    }


    private String getMonthName(String date) {

        String monthName = "";

        String month = date.substring(5, 7);

        switch (month) {


            case "01":
                monthName = "January";
                break;

            case "02":
                monthName = "February";
                break;

            case "03":
                monthName = "March";
                break;

            case "04":
                monthName = "April";
                break;

            case "05":
                monthName = "May";
                break;

            case "06":
                monthName = "June";
                break;

            case "07":
                monthName = "July";
                break;

            case "08":
                monthName = "August";
                break;

            case "09":
                monthName = "September";
                break;

            case "10":
                monthName = "October";
                break;

            case "11":
                monthName = "November";
                break;

            case "12":
                monthName = "December";
                break;
        }


        return monthName;
    }

    private String getDate(String date) {

        String day = date.substring(8, 10);
        return day;

    }

    private String getYear(String date) {

        String year = date.substring(0, 4);
        return year;

    }

    @Override
    public int getItemCount() {
        return getHistoryMapDataList.size();
    }


}
