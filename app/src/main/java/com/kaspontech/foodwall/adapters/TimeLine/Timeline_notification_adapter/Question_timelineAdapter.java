/*
package com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;


public class Question_timelineAdapter extends RecyclerView.ViewHolder {

    ImageView userimage, quest_user_photo, userphoto_comment, img_shareadapter;
    LinearLayout postAnswerLayout, viewQues, ll_quest_asked;
    EditText writeAnswer;
    ImageButton answerChecked, answerUnchecked, typeAnswer, ans_options,img_commentadapter;
    TextView question, answer, username, no_of_reviews, no_of_followers,
            answeredOn, postAnswer, total_answer, txt_adapter_post, commenttest, comment,
            View_all_comment, liketxt, like, txt_created_on,
            txt_view_quest_username, txt_view_quest_text, txt_view_quest_name;


    ImageButton img_like;

    EmojiconEditText edittxtComment;
    //Relative layout
    RelativeLayout rl_share, like_rl, rl_comment_adapter, rl_post_timeline,
            rl_share_adapter, rl_likes_layout, rl_comments_layout, rl_comment_timeline;

    public Question_timelineAdapter(View itemView) {
        super(itemView);
        //ImageView
        userimage = (ImageView) itemView.findViewById(R.id.user_image);
        quest_user_photo = (ImageView) itemView.findViewById(R.id.quest_user_photo);
        userphoto_comment = (ImageView) itemView.findViewById(R.id.userphoto_comment);
        img_shareadapter = (ImageView) itemView.findViewById(R.id.img_shareadapter);
        img_commentadapter = (ImageButton) itemView.findViewById(R.id.img_commentadapter);

        //Editext
        edittxtComment = (EmojiconEditText) itemView.findViewById(R.id.edittxtComment);
        //Like Button
        img_like = (ImageButton) itemView.findViewById(R.id.img_like);

        //TextView
        question = (TextView) itemView.findViewById(R.id.question);
        answer = (TextView) itemView.findViewById(R.id.answer);
        username = (TextView) itemView.findViewById(R.id.user_name);
        no_of_reviews = (TextView) itemView.findViewById(R.id.reviews_count);
        no_of_followers = (TextView) itemView.findViewById(R.id.follow_count);
        viewQues = (LinearLayout) itemView.findViewById(R.id.ll_view_ques);
        typeAnswer = (ImageButton) itemView.findViewById(R.id.typeAnswer);
        answeredOn = (TextView) itemView.findViewById(R.id.answered_on);
        total_answer = (TextView) itemView.findViewById(R.id.total_answer);


        txt_view_quest_username = (TextView) itemView.findViewById(R.id.txt_view_quest_username);
        txt_view_quest_text = (TextView) itemView.findViewById(R.id.txt_view_quest_text);
        txt_view_quest_name = (TextView) itemView.findViewById(R.id.txt_view_quest_name);

        txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);
        commenttest = (TextView) itemView.findViewById(R.id.commenttest);
        comment = (TextView) itemView.findViewById(R.id.comment);
        View_all_comment = (TextView) itemView.findViewById(R.id.View_all_comment);
        liketxt = (TextView) itemView.findViewById(R.id.liketxt);
        like = (TextView) itemView.findViewById(R.id.like);
        txt_created_on = (TextView) itemView.findViewById(R.id.txt_created_on);


//        postAnswerLayout = (LinearLayout) itemView.findViewById(R.id.add_user_answer);


//        writeAnswer = (EditText) itemView.findViewById(R.id.edittxt_answer);
        answerChecked = (ImageButton) itemView.findViewById(R.id.answer_checked);
        answerUnchecked = (ImageButton) itemView.findViewById(R.id.answer_unchecked);
        ans_options = (ImageButton) itemView.findViewById(R.id.ans_options);


        //Relative Layout
        rl_share = (RelativeLayout) itemView.findViewById(R.id.rl_share);
        like_rl = (RelativeLayout) itemView.findViewById(R.id.like_rl);
        rl_comment_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_comment_adapter);
        rl_post_timeline = (RelativeLayout) itemView.findViewById(R.id.rl_post_timeline);
        rl_likes_layout = (RelativeLayout) itemView.findViewById(R.id.rl_likes_layout);
        rl_comments_layout = (RelativeLayout) itemView.findViewById(R.id.rl_comments_layout);
        rl_share_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_share_adapter);

    }

}
*/
