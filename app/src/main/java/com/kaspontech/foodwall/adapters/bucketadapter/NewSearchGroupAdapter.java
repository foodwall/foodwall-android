package com.kaspontech.foodwall.adapters.bucketadapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.bucketlistpackage.SearchGroupBucketActivity;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.Get_grp_input_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.R;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class NewSearchGroupAdapter extends RecyclerView.Adapter<NewSearchGroupAdapter.MyViewHolder> {


    private Context context;

    private ArrayList<Get_grp_input_pojo> gettinggrouplist = new ArrayList<>();
    HashMap<String, Integer> selectedfollowerlistId = new HashMap<>();

    private Get_grp_input_pojo getGrpInputPojo;

    private Get_following_profile_pojo getFollowingProfilePojo;

    SearchGroupBucketActivity searchGroupBucketActivity;

    Handler doubleHandler;
    private Pref_storage pref_storage;
    boolean  doubleClick = false;


    public NewSearchGroupAdapter(Context c, ArrayList<Get_grp_input_pojo> gettinglist, HashMap<String, Integer> individualfollowerId) {
        this.context = c;
        this.gettinggrouplist = gettinglist;
        this.selectedfollowerlistId = individualfollowerId;
        searchGroupBucketActivity=(SearchGroupBucketActivity)context;
    }

    @NonNull
    @Override
    public NewSearchGroupAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_chat_followerlist, parent, false);
        // set the view's size, margins, paddings and layout parameters


        pref_storage = new Pref_storage();

        return new NewSearchGroupAdapter.MyViewHolder(v, searchGroupBucketActivity);
    }

    @Override
    public void onBindViewHolder(@NonNull final NewSearchGroupAdapter.MyViewHolder holder, final int position) {


        getGrpInputPojo = gettinggrouplist.get(position);

        holder.check_follower.setVisibility(View.VISIBLE);

        holder.txt_username.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(gettinggrouplist.get(position).getGroupName()));
        holder.txt_username.setTextColor(Color.parseColor("#000000"));
        holder.txt_userlastname.setVisibility(View.GONE);
        holder.txt_fullname_user.setVisibility(View.GONE);
//        holder.txt_userlastname.setText(getFollowerlistPojo.getLast_name());
//        holder.txt_userlastname.setTextColor(Color.parseColor("#000000"));

      /*  String fullname= getFollowerlistPojo.getFirst_name().concat(" ").concat(getFollowerlistPojo.getLast_name());

        holder.txt_fullname_user.setText(fullname);*/

        String ownuser_id= Pref_storage.getDetail(context, "userId");




// user image




        if( gettinggrouplist.get(position).getGroupIcon().equalsIgnoreCase("null") ||  gettinggrouplist.get(position).getGroupIcon().equalsIgnoreCase("0")) {
            Picasso.get()
                    .load(gettinggrouplist.get(position).getGroupIcon())
                    .placeholder(R.drawable.ic_group_inside)
                    .into(holder.img_following);

        }else
        {
            Picasso.get()
                    .load(R.drawable.ic_group_inside)
                     .into(holder.img_following);

        }


//        if (gettinggrouplist.get(position).getGroupIcon().equalsIgnoreCase("0")||gettinggrouplist.get(position).getGroupIcon().isEmpty()){
//            holder.img_following.setImageResource(R.drawable.ic_group_inside);
//        }else {
//            Picasso.get()
//                    .load(gettinggrouplist.get(position).getGroupIcon())
//                    .placeholder(R.drawable.ic_group_inside)
//                    .into(holder.img_following);
//        }


    }




//Remove position

    private  void removeatposition(int position){
        gettinggrouplist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, gettinggrouplist.size());
    }





    @Override
    public int getItemCount() {

        return gettinggrouplist.size();
    }





    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txt_username, txt_userlastname, txt_fullname_user, txt_follow, share, caption, captiontag, txt_created_on,txt_adapter_post;
        RelativeLayout rlayout_following;

        ImageView img_checked_follower,img_following;
        SearchGroupBucketActivity searchGroupBucketActivity;

        CheckBox check_follower;


        public MyViewHolder(View itemView,SearchGroupBucketActivity searchGroupBucketActivity1) {
            super(itemView);
            this.searchGroupBucketActivity =  searchGroupBucketActivity1;
            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_userlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txt_fullname_user = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            img_following = (ImageView) itemView.findViewById(R.id.img_following);
            img_checked_follower = (ImageView) itemView.findViewById(R.id.img_checked_follower);

            check_follower=(CheckBox) itemView.findViewById(R.id.check_follower);
            check_follower.setOnClickListener(this);

            rlayout_following = (RelativeLayout) itemView.findViewById(R.id.rlayout_following);


        }

        @Override
        public void onClick(View v) {
            searchGroupBucketActivity.getgrouplist(v, getAdapterPosition());
        }
    }


}


