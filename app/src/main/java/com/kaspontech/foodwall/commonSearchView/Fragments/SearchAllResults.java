package com.kaspontech.foodwall.commonSearchView.Fragments;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.adapters.CommonSearch.Events.GetSearchResultsEventsAdapter;
import com.kaspontech.foodwall.adapters.CommonSearch.People.GetSearchResultsPeopleAdapter;
import com.kaspontech.foodwall.adapters.CommonSearch.Questions.GetSearchResultsQuestionAdapter;
import com.kaspontech.foodwall.adapters.CommonSearch.Restaurant.GetSearchResultsRestaurantAdapter;
import com.kaspontech.foodwall.adapters.CommonSearch.Review.GetSearchResultsReviewAdapter;
import com.kaspontech.foodwall.adapters.CommonSearch.Timeline.GetSearchResultsFeedsAdapter;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.GetCommonSearchPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchEventPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchPersonPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchQuestionPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchRestaurantPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchTimelinePojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_hotel_all_inner_review_pojo;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.commonSearchView.CommonSearch.searchText;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchAllResults extends Fragment implements View.OnClickListener {

    View view;
    Context context;

    String searchKey;

    SwitchTab switchTab;

    List<GetCommonSearchPojo> getCommonSearchTypeDataList = new ArrayList<>();


    List<SearchPersonPojo> searchPersonPojoList = new ArrayList<>();
    List<SearchEventPojo> searchEventPojoList = new ArrayList<>();
    List<SearchQuestionPojo> searchQuestionPojoList = new ArrayList<>();
    ArrayList<Get_hotel_all_inner_review_pojo> searchReviewPojoList = new ArrayList<>();
    List<SearchTimelinePojo> searchTimelinePojoList = new ArrayList<>();
    List<SearchRestaurantPojo> searchRestaurantPojoList = new ArrayList<>();


    GetSearchResultsPeopleAdapter getSearchResultsPeopleAdapter;
    GetSearchResultsEventsAdapter getSearchResultsEventsAdapter;
    GetSearchResultsFeedsAdapter getSearchResultsFeedsAdapter;
    GetSearchResultsReviewAdapter getSearchResultsReviewAdapter;
    GetSearchResultsQuestionAdapter getSearchResultsQuestionAdapter;
    GetSearchResultsRestaurantAdapter getSearchResultsRestaurantAdapter;


    RecyclerView recycler_view_common_search_all_people, recycler_view_common_search_all_feeds,
            recycler_view_common_search_all_review, recycler_view_common_search_all_restaurant,
            recycler_view_common_search_all_events, recycler_view_common_search_all_question;

    LinearLayout ll_search_people, ll_search_feeds, ll_search_review, ll_search_events,
            ll_search_questions, ll_no_results, ll_see_all_questions,
            ll_see_all_events, ll_see_all_review, ll_see_all_feeds, ll_see_all_people, ll_search_restaurant, ll_see_all_restaurant;

    NestedScrollView ll_search_main;

    private static final String TAG = "SearchAllResults";

    public SearchAllResults() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search_all_results, container, false);
        context = view.getContext();

        ll_search_people = (LinearLayout) view.findViewById(R.id.ll_search_people);
        ll_search_feeds = (LinearLayout) view.findViewById(R.id.ll_search_feeds);
        ll_search_review = (LinearLayout) view.findViewById(R.id.ll_search_review);
        ll_search_events = (LinearLayout) view.findViewById(R.id.ll_search_events);
        ll_search_questions = (LinearLayout) view.findViewById(R.id.ll_search_questions);
        ll_no_results = (LinearLayout) view.findViewById(R.id.ll_no_results);
        ll_search_main = (NestedScrollView) view.findViewById(R.id.ll_search_main);
        ll_search_restaurant = (LinearLayout) view.findViewById(R.id.ll_search_restaurant);

        ll_see_all_questions = (LinearLayout) view.findViewById(R.id.ll_see_all_questions);
        ll_see_all_events = (LinearLayout) view.findViewById(R.id.ll_see_all_events);
        ll_see_all_review = (LinearLayout) view.findViewById(R.id.ll_see_all_review);
        ll_see_all_feeds = (LinearLayout) view.findViewById(R.id.ll_see_all_feeds);
        ll_see_all_people = (LinearLayout) view.findViewById(R.id.ll_see_all_people);
        ll_see_all_restaurant = (LinearLayout) view.findViewById(R.id.ll_see_all_restaurant);


        ll_see_all_people.setOnClickListener(this);
        ll_see_all_feeds.setOnClickListener(this);
        ll_see_all_review.setOnClickListener(this);
        ll_see_all_events.setOnClickListener(this);
        ll_see_all_questions.setOnClickListener(this);
        ll_see_all_restaurant.setOnClickListener(this);


        recycler_view_common_search_all_people = (RecyclerView) view.findViewById(R.id.recycler_view_common_search_all_people);
        recycler_view_common_search_all_feeds = (RecyclerView) view.findViewById(R.id.recycler_view_common_search_all_feeds);
        recycler_view_common_search_all_review = (RecyclerView) view.findViewById(R.id.recycler_view_common_search_all_review);
        recycler_view_common_search_all_events = (RecyclerView) view.findViewById(R.id.recycler_view_common_search_all_events);
        recycler_view_common_search_all_question = (RecyclerView) view.findViewById(R.id.recycler_view_common_search_all_question);
        recycler_view_common_search_all_restaurant = (RecyclerView) view.findViewById(R.id.recycler_view_common_search_all_restaurant);

        if (getArguments() != null) {
            searchKey = getArguments().getString("params");
        }


        Log.e("SearchPeople", "SearchPeople->" + searchText);

        //calling common search api
        callSearch(searchText);

        // People

        getSearchResultsPeopleAdapter = new GetSearchResultsPeopleAdapter(context, searchPersonPojoList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recycler_view_common_search_all_people.setLayoutManager(mLayoutManager);
        recycler_view_common_search_all_people.setNestedScrollingEnabled(false);
        recycler_view_common_search_all_people.setItemAnimator(new DefaultItemAnimator());
        recycler_view_common_search_all_people.setAdapter(getSearchResultsPeopleAdapter);
        recycler_view_common_search_all_people.hasFixedSize();

        // Feeds

        getSearchResultsFeedsAdapter = new GetSearchResultsFeedsAdapter(context, searchTimelinePojoList);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(context);
        recycler_view_common_search_all_feeds.setLayoutManager(mLayoutManager1);
        recycler_view_common_search_all_feeds.setNestedScrollingEnabled(false);
        recycler_view_common_search_all_feeds.setItemAnimator(new DefaultItemAnimator());
        recycler_view_common_search_all_feeds.addItemDecoration(new SimpleDividerItemDecoration(context));
        recycler_view_common_search_all_feeds.setAdapter(getSearchResultsFeedsAdapter);
        recycler_view_common_search_all_feeds.hasFixedSize();


        // Review

        getSearchResultsReviewAdapter = new GetSearchResultsReviewAdapter(context, searchReviewPojoList,"");
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(context);
        recycler_view_common_search_all_review.setLayoutManager(mLayoutManager2);
        recycler_view_common_search_all_review.setNestedScrollingEnabled(false);
        recycler_view_common_search_all_review.setItemAnimator(new DefaultItemAnimator());
        recycler_view_common_search_all_review.setAdapter(getSearchResultsReviewAdapter);
        recycler_view_common_search_all_review.hasFixedSize();


        // Events

        getSearchResultsEventsAdapter = new GetSearchResultsEventsAdapter(context, searchEventPojoList);
        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(context);
        recycler_view_common_search_all_events.setLayoutManager(mLayoutManager3);
        recycler_view_common_search_all_events.setNestedScrollingEnabled(false);
        recycler_view_common_search_all_events.setItemAnimator(new DefaultItemAnimator());
        recycler_view_common_search_all_events.setAdapter(getSearchResultsEventsAdapter);
        recycler_view_common_search_all_events.hasFixedSize();


        // Question

        getSearchResultsQuestionAdapter = new GetSearchResultsQuestionAdapter(context, searchQuestionPojoList);
        RecyclerView.LayoutManager mLayoutManager4 = new LinearLayoutManager(context);
        recycler_view_common_search_all_question.setLayoutManager(mLayoutManager4);
        recycler_view_common_search_all_question.setNestedScrollingEnabled(false);
        recycler_view_common_search_all_question.setItemAnimator(new DefaultItemAnimator());
        recycler_view_common_search_all_question.setAdapter(getSearchResultsQuestionAdapter);
        recycler_view_common_search_all_question.hasFixedSize();


        // Hotel

        getSearchResultsRestaurantAdapter = new GetSearchResultsRestaurantAdapter(context, searchRestaurantPojoList);
        RecyclerView.LayoutManager mLayoutManager5 = new LinearLayoutManager(context);
        recycler_view_common_search_all_restaurant.setLayoutManager(mLayoutManager5);
        recycler_view_common_search_all_restaurant.setItemAnimator(new DefaultItemAnimator());
        recycler_view_common_search_all_restaurant.setAdapter(getSearchResultsRestaurantAdapter);
        recycler_view_common_search_all_restaurant.hasFixedSize();





        return view;

    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            case R.id.ll_see_all_people:

                Log.e(TAG, "onClick: ll_see_all_people");

                switchTab.switchTab("People", searchText);

                break;


            case R.id.ll_see_all_feeds:

                switchTab.switchTab("Feeds", searchText);
                break;


            case R.id.ll_see_all_review:

                switchTab.switchTab("Reviews", searchText);

                break;

            case R.id.ll_see_all_restaurant:

                switchTab.switchTab("Restaurants", searchText);
                break;

            case R.id.ll_see_all_events:

                switchTab.switchTab("Events", searchText);


                break;

            case R.id.ll_see_all_questions:

                switchTab.switchTab("Questions", searchText);

                break;


        }

    }

    public interface SwitchTab {
        void switchTab(String searchTab, String searchText);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            switchTab = (SwitchTab) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }

    }

    public void callSearch(String query) {

        //Do searching

        if (isConnected(context)) {

            /*final AlertDialog dialog1 = new SpotsDialog.Builder().setContext(context).setMessage("").build();

            dialog1.show();*/

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                getCommonSearchTypeDataList.clear();

                searchPersonPojoList.clear();
                searchQuestionPojoList.clear();
                searchReviewPojoList.clear();
                searchTimelinePojoList.clear();
                searchRestaurantPojoList.clear();
                searchEventPojoList.clear();


                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Log.e("CommonSearchSize", "CommonSearchSize->" + query);

                Call<GetCommonSearchPojo> call = apiService.getCommonSearchResults("get_common_search_result", query, userid);

                call.enqueue(new Callback<GetCommonSearchPojo>() {
                    @Override
                    public void onResponse(Call<GetCommonSearchPojo> call, Response<GetCommonSearchPojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatusPeople", "responseStatusPeople->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success
                            String personSearch = response.body().getData().get(0).getPeople().get(0).getSearch_val();
                            String feedsSearch = response.body().getData().get(1).getTimeline().get(0).getSearch_val();
                            String reviewSearch = response.body().getData().get(2).getReview().get(0).getSearchVal();
                            String questionSearch = response.body().getData().get(3).getQuestion().get(0).getSearchVal();
                            String eventsSearch = response.body().getData().get(4).getEvents().get(0).getSearch_val();
                            String hotelSearch = response.body().getData().get(5).getHotel().get(0).getSearchVal();


                            if (personSearch.equals("0") && feedsSearch.equals("0") && reviewSearch.equals("0") && eventsSearch.equals("0") && questionSearch.equals("0") && hotelSearch.equals("0")) {

                                ll_no_results.setVisibility(View.VISIBLE);
                                ll_search_main.setVisibility(View.GONE);

                            } else {


                                ll_no_results.setVisibility(View.GONE);
                                ll_search_main.setVisibility(View.VISIBLE);

                            }
                            // People 0

                            if (response.body().getData().get(0).getPeople().size() > 3) {

                                searchPersonPojoList.addAll(response.body().getData().get(0).getPeople().subList(0, 3));

                            } else {

                                searchPersonPojoList.addAll(response.body().getData().get(0).getPeople());

                            }

                            if (searchPersonPojoList != null) {

                                if (searchPersonPojoList.get(0).getSearch_val().equals("0")) {

                                    ll_search_people.setVisibility(View.GONE);

                                } else {

                                    ll_search_people.setVisibility(View.VISIBLE);
                                    // People

                                    getSearchResultsPeopleAdapter = new GetSearchResultsPeopleAdapter(context, searchPersonPojoList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                    recycler_view_common_search_all_people.setLayoutManager(mLayoutManager);
                                    recycler_view_common_search_all_people.setNestedScrollingEnabled(false);
                                    recycler_view_common_search_all_people.setItemAnimator(new DefaultItemAnimator());
                                    recycler_view_common_search_all_people.setAdapter(getSearchResultsPeopleAdapter);
                                    recycler_view_common_search_all_people.hasFixedSize();
                                    getSearchResultsPeopleAdapter.notifyDataSetChanged();

                                }
                            }


                            // Feeds 1

                            if (response.body().getData().get(1).getTimeline().size() > 3) {

                                searchTimelinePojoList.addAll(response.body().getData().get(1).getTimeline().subList(0, 3));

                            } else {

                                searchTimelinePojoList.addAll(response.body().getData().get(1).getTimeline());

                            }

                            if (searchTimelinePojoList != null) {

                                if (searchTimelinePojoList.size() == 0) {

                                    ll_search_feeds.setVisibility(View.GONE);

                                } else {

                                    if (searchTimelinePojoList.get(0).getSearch_val().equals("0")) {

                                        ll_search_feeds.setVisibility(View.GONE);


                                    } else {

                                        ll_search_feeds.setVisibility(View.VISIBLE);
                                        // Feeds

                                        getSearchResultsFeedsAdapter = new GetSearchResultsFeedsAdapter(context, searchTimelinePojoList);
                                        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(context);
                                        recycler_view_common_search_all_feeds.setLayoutManager(mLayoutManager1);
                                        recycler_view_common_search_all_feeds.setNestedScrollingEnabled(false);
                                        recycler_view_common_search_all_feeds.setItemAnimator(new DefaultItemAnimator());
                                        recycler_view_common_search_all_feeds.addItemDecoration(new SimpleDividerItemDecoration(context));
                                        recycler_view_common_search_all_feeds.setAdapter(getSearchResultsFeedsAdapter);
                                        recycler_view_common_search_all_feeds.hasFixedSize();
                                        getSearchResultsFeedsAdapter.notifyDataSetChanged();

                                    }


                                }
                            }


                            // Review 2

                            if (response.body().getData().get(2).getReview().size() > 3) {

                                searchReviewPojoList.addAll(response.body().getData().get(2).getReview().subList(0, 3));

                            } else {

                                searchReviewPojoList.addAll(response.body().getData().get(2).getReview());

                            }

                            if (searchReviewPojoList != null) {

                                if (searchReviewPojoList.size() == 0) {

                                    ll_search_review.setVisibility(View.GONE);

                                } else {

                                    if (searchReviewPojoList.get(0).getSearchVal().equals("0")) {

                                        ll_search_review.setVisibility(View.GONE);


                                    } else {


                                        ll_search_review.setVisibility(View.VISIBLE);
                                        // Review

                                        getSearchResultsReviewAdapter = new GetSearchResultsReviewAdapter(context, searchReviewPojoList,"");
                                        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(context);
                                        recycler_view_common_search_all_review.setLayoutManager(mLayoutManager2);
                                        recycler_view_common_search_all_review.setNestedScrollingEnabled(false);
                                        recycler_view_common_search_all_review.setItemAnimator(new DefaultItemAnimator());
                                        recycler_view_common_search_all_review.setAdapter(getSearchResultsReviewAdapter);
                                        recycler_view_common_search_all_review.hasFixedSize();
                                        getSearchResultsReviewAdapter.notifyDataSetChanged();


                                    }


                                }
                            }


                            // Question 3

                            if (response.body().getData().get(3).getQuestion().size() > 3) {

                                searchQuestionPojoList.addAll(response.body().getData().get(3).getQuestion().subList(0, 3));

                            } else {

                                searchQuestionPojoList.addAll(response.body().getData().get(3).getQuestion());

                            }

                            if (searchQuestionPojoList != null) {

                                if (searchQuestionPojoList.size() == 0) {

                                    ll_search_questions.setVisibility(View.GONE);

                                } else {

                                    if (searchQuestionPojoList.get(0).getSearchVal().equals("0")) {

                                        ll_search_questions.setVisibility(View.GONE);


                                    } else {


                                        ll_search_questions.setVisibility(View.VISIBLE);
                                        // Question


                                        getSearchResultsQuestionAdapter = new GetSearchResultsQuestionAdapter(context, searchQuestionPojoList);
                                        RecyclerView.LayoutManager mLayoutManager4 = new LinearLayoutManager(context);
                                        recycler_view_common_search_all_question.setLayoutManager(mLayoutManager4);
                                        recycler_view_common_search_all_question.setNestedScrollingEnabled(false);
                                        recycler_view_common_search_all_question.setItemAnimator(new DefaultItemAnimator());
                                        recycler_view_common_search_all_question.setAdapter(getSearchResultsQuestionAdapter);
                                        recycler_view_common_search_all_question.hasFixedSize();
                                        getSearchResultsQuestionAdapter.notifyDataSetChanged();

                                    }


                                }
                            }


                            // Events 4

                            if (response.body().getData().get(4).getEvents().size() > 3) {

                                searchEventPojoList.addAll(response.body().getData().get(4).getEvents().subList(0, 3));

                            } else {

                                searchEventPojoList.addAll(response.body().getData().get(4).getEvents());

                            }

                            if (searchEventPojoList != null) {

                                if (searchEventPojoList.size() == 0) {

                                    ll_search_events.setVisibility(View.GONE);

                                } else {

                                    if (searchEventPojoList.get(0).getSearch_val().equals("0")) {

                                        ll_search_events.setVisibility(View.GONE);

                                    } else {

                                        ll_search_events.setVisibility(View.VISIBLE);
                                        // Events

                                        getSearchResultsEventsAdapter = new GetSearchResultsEventsAdapter(context, searchEventPojoList);
                                        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(context);
                                        recycler_view_common_search_all_events.setLayoutManager(mLayoutManager3);
                                        recycler_view_common_search_all_events.setNestedScrollingEnabled(false);
                                        recycler_view_common_search_all_events.setItemAnimator(new DefaultItemAnimator());
                                        recycler_view_common_search_all_events.setAdapter(getSearchResultsEventsAdapter);
                                        recycler_view_common_search_all_events.hasFixedSize();
                                       // getSearchResultsEventsAdapter.notifyDataSetChanged();

                                    }


                                }
                            }


                            // Hotel 5

                            if (response.body().getData().get(5).getHotel().size() > 3) {

                                searchRestaurantPojoList.addAll(response.body().getData().get(5).getHotel().subList(0, 3));

                            } else {

                                searchRestaurantPojoList.addAll(response.body().getData().get(5).getHotel());

                            }

                            if (searchRestaurantPojoList != null) {

                                if (searchRestaurantPojoList.size() == 0) {

                                    ll_search_restaurant.setVisibility(View.GONE);

                                } else {

                                    if (searchRestaurantPojoList.get(0).getSearchVal().equals("0")) {

                                        ll_search_restaurant.setVisibility(View.GONE);


                                    } else {


                                        ll_search_restaurant.setVisibility(View.VISIBLE);
                                        // Hotel

                                        getSearchResultsRestaurantAdapter = new GetSearchResultsRestaurantAdapter(context, searchRestaurantPojoList);
                                        RecyclerView.LayoutManager mLayoutManager5 = new LinearLayoutManager(context);
                                        recycler_view_common_search_all_restaurant.setLayoutManager(mLayoutManager5);
                                        recycler_view_common_search_all_restaurant.setItemAnimator(new DefaultItemAnimator());
                                        recycler_view_common_search_all_restaurant.setAdapter(getSearchResultsRestaurantAdapter);
                                        recycler_view_common_search_all_restaurant.hasFixedSize();
                                        getSearchResultsRestaurantAdapter.notifyDataSetChanged();


                                    }


                                }
                            }




                            ll_search_main.setVisibility(View.VISIBLE);
//                            dialog1.dismiss();

















                        } else if (responseStatus.equals("nodata")) {

//                            dialog1.dismiss();

                        }


                    }


                    @Override
                    public void onFailure(Call<GetCommonSearchPojo> call, Throwable t) {
                        //Error
                        Log.e("AllSearchFailureError", "" + t.getMessage());
                        ll_no_results.setVisibility(View.VISIBLE);
                        ll_search_main.setVisibility(View.GONE);
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {


        }


    }

    // Internet Check

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


}
