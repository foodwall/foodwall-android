package com.kaspontech.foodwall.commonSearchView;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.adapters.CommonSearch.CommonSearchListAdapter;
import com.kaspontech.foodwall.commonSearchView.Fragments.SearchAllResults;
import com.kaspontech.foodwall.commonSearchView.Fragments.SearchEvents;
import com.kaspontech.foodwall.commonSearchView.Fragments.SearchFeeds;
import com.kaspontech.foodwall.commonSearchView.Fragments.SearchPeople;
import com.kaspontech.foodwall.commonSearchView.Fragments.SearchQA;
import com.kaspontech.foodwall.commonSearchView.Fragments.SearchRestaurent;
import com.kaspontech.foodwall.commonSearchView.Fragments.SearchReview;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchList.GetCommonSearchTypeData;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchList.GetCommonSearchTypePojo;
import com.kaspontech.foodwall.utills.MyDividerItemDecoration;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommonSearch extends AppCompatActivity implements View.OnClickListener
        , SearchAllResults.SwitchTab {

    Toolbar actionBar;
    FrameLayout search_container;
    ImageButton imageButton;
    SearchView searchView;
    LinearLayout ll_not_found, ll_search_tabs;
    ScrollView common_search_scroll;
    AutoCompleteTextView auto_search;
    TabLayout searchTablayout;
    RelativeLayout rl_common_search;

    // Tabs
    Bundle bundle;
    Fragment searchAll, searchPeople, searchEvents, searchFeeds, searchQA, searchReview, searchRestaurent;
    public static String searchText;
    private int position;

    // Recylerview
    RecyclerView recycler_view_common_search;
    CommonSearchListAdapter commonSearchListAdapter;
    GetCommonSearchTypeData getCommonSearchTypeData;
    List<GetCommonSearchTypeData> getCommonSearchTypeDataList = new ArrayList<>();

    private static final String TAG = "CommonSearch";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_search);

        actionBar = (Toolbar) findViewById(R.id.action_bar_common_search);
        imageButton = (ImageButton) findViewById(R.id.back);
        ll_not_found = (LinearLayout) findViewById(R.id.ll_not_found);
        ll_search_tabs = (LinearLayout) findViewById(R.id.ll_search_tabs);
        common_search_scroll = (ScrollView) findViewById(R.id.common_search_scroll);
        auto_search = (AutoCompleteTextView) findViewById(R.id.auto_search);
        searchTablayout = (TabLayout) findViewById(R.id.tab_search);
        search_container = (FrameLayout) findViewById(R.id.search_container);
        rl_common_search = (RelativeLayout) findViewById(R.id.rl_common_search);

        imageButton.setOnClickListener(this);

        setSupportActionBar(actionBar);

        searchTablayout.addTab(searchTablayout.newTab().setText("All"));
        searchTablayout.addTab(searchTablayout.newTab().setText("People"));
        searchTablayout.addTab(searchTablayout.newTab().setText("Feeds"));
        searchTablayout.addTab(searchTablayout.newTab().setText("Review"));
        searchTablayout.addTab(searchTablayout.newTab().setText("Restaurant"));
        searchTablayout.addTab(searchTablayout.newTab().setText("Events"));
        searchTablayout.addTab(searchTablayout.newTab().setText("Q & A"));


        searchView = (SearchView) actionBar.findViewById(R.id.common_search_view);
        recycler_view_common_search = (RecyclerView) findViewById(R.id.recycler_view_common_search);


        commonSearchListAdapter = new CommonSearchListAdapter(this, getCommonSearchTypeDataList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view_common_search.setLayoutManager(mLayoutManager);
        recycler_view_common_search.setItemAnimator(new DefaultItemAnimator());
        recycler_view_common_search.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 20));
        recycler_view_common_search.setAdapter(commonSearchListAdapter);

        bundle = new Bundle();
        bundle.putString("params", searchText);

        Bundle bundles = getIntent().getExtras();

        if (bundles != null) {

            position = bundles.getInt("position");

        }

        searchAll = new SearchAllResults();
        searchPeople = new SearchPeople();
        searchFeeds = new SearchFeeds();
        searchEvents = new SearchEvents();
        searchQA = new SearchQA();
        searchReview = new SearchReview();
        searchRestaurent = new SearchRestaurent();

        searchTablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {


                    case 0:

                        searchAll.setArguments(bundle);
                        replaceFragment(searchAll);

                        break;

                    case 1:

                        searchPeople.setArguments(bundle);
                        replaceFragment(searchPeople);

                        break;

                    case 2:

                        searchFeeds.setArguments(bundle);
                        replaceFragment(searchFeeds);
                        break;

                    case 3:

                        searchReview.setArguments(bundle);
                        replaceFragment(searchReview);

                        break;

                    case 4:

                        searchRestaurent.setArguments(bundle);
                        replaceFragment(searchRestaurent);

                        break;

                    case 5:

                        searchEvents.setArguments(bundle);
                        replaceFragment(searchEvents);

                        break;

                    case 6:

                        searchQA.setArguments(bundle);
                        replaceFragment(searchQA);


                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.setQueryHint("Search");
        searchView.requestFocusFromTouch();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if(query.length() > 0){

                    recycler_view_common_search.setVisibility(View.VISIBLE);
                    ll_search_tabs.setVisibility(View.GONE);
//                    hideKeyboard(rl_common_search);

                    callSearch(query);

                }


              /*  if (query.length() != 0) {


                    recycler_view_common_search.setVisibility(View.GONE);
                    ll_search_tabs.setVisibility(View.VISIBLE);
                    hideKeyboard(rl_common_search);

                    searchText = query;
                    bundle.putString("params", query);
                    searchAll.setArguments(bundle);
                    TabLayout.Tab tab = searchTablayout.getTabAt(0);
                    assert tab != null;
                    tab.select();
                    replaceFragment(searchAll);


                    *//*switch (position) {

                        case 0:

                            searchText = query;
                            bundle.putString("params", query);
                            searchAll.setArguments(bundle);
                            TabLayout.Tab tab = searchTablayout.getTabAt(0);
                            assert tab != null;
                            tab.select();
                            replaceFragment(searchAll);

                            break;


                        case 1:

                            searchText = query;
                            bundle.putString("params", query);
                            searchReview.setArguments(bundle);
                            TabLayout.Tab tab1 = searchTablayout.getTabAt(3);
                            assert tab1 != null;
                            tab1.select();
                            replaceFragment(searchReview);

                            break;

                        case 2:

                            searchText = query;
                            bundle.putString("params", query);
                            searchQA.setArguments(bundle);
                            TabLayout.Tab tab2 = searchTablayout.getTabAt(6);
                            assert tab2 != null;
                            tab2.select();
                            replaceFragment(searchQA);

                            break;

                        default:

                            searchText = query;
                            bundle.putString("params", query);
                            searchAll.setArguments(bundle);
                            TabLayout.Tab tab4 = searchTablayout.getTabAt(0);
                            assert tab4 != null;
                            tab4.select();
                            replaceFragment(searchAll);

                            break;

                    }
*//*

                } else {


                    getCommonSearchTypeDataList.clear();
                    recycler_view_common_search.setAdapter(null);

                }*/


                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if(newText.length() > 0){

                    recycler_view_common_search.setVisibility(View.VISIBLE);
                    ll_search_tabs.setVisibility(View.GONE);
//                    hideKeyboard(rl_common_search);
                    callSearch(newText);

                }

                /*final List<String> searchResults = new ArrayList<>();


                if (newText.length() == 0) {

                    getCommonSearchTypeDataList.clear();
                    recycler_view_common_search.setAdapter(null);

                }

                if (newText.length() != 0) {

                    ll_search_tabs.setVisibility(View.GONE);

                    recycler_view_common_search.getVisibility();

                    if (recycler_view_common_search.getVisibility() == View.GONE) {

                        recycler_view_common_search.setVisibility(View.VISIBLE);
                    }


                    getCommonSearchTypeDataList.clear();


                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        int userid = Integer.parseInt(Pref_storage.getDetail(CommonSearch.this, "userId"));

                        Log.e("CommonSearchSize", "CommonSearchSize->" + newText);

                        Call<GetCommonSearchTypePojo> call = apiService.getCommonResults("get_common_search", newText);

                        call.enqueue(new Callback<GetCommonSearchTypePojo>() {
                            @Override
                            public void onResponse(Call<GetCommonSearchTypePojo> call, Response<GetCommonSearchTypePojo> response) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {

                                    ll_not_found.setVisibility(View.GONE);
                                    common_search_scroll.setVisibility(View.VISIBLE);

                                    //Success

                                    for (int i = 0; i < response.body().getData().size(); i++) {

                                        String searchtext = response.body().getData().get(i).getSearchtext();
                                        String searchtype = response.body().getData().get(i).getSearchtype();

                                        getCommonSearchTypeData = new GetCommonSearchTypeData(searchtext, searchtype);
                                        getCommonSearchTypeDataList.add(getCommonSearchTypeData);




                                    }


                                      *//*  ArrayAdapter arrayAdapter = new ArrayAdapter(CommonSearch.this,android.R.layout.simple_expandable_list_item_1,searchResults);
                                        auto_search.setAdapter(arrayAdapter);*//*

                                    CommonSearchListAdapter commonSearchListAdapter = new CommonSearchListAdapter(CommonSearch.this, getCommonSearchTypeDataList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CommonSearch.this);
                                    recycler_view_common_search.setLayoutManager(mLayoutManager);
                                    recycler_view_common_search.setItemAnimator(new DefaultItemAnimator());
                                    recycler_view_common_search.setAdapter(commonSearchListAdapter);
                                    recycler_view_common_search.hasFixedSize();
                                    Log.e("CommonSearchSize", "CommonSearchSize->" + getCommonSearchTypeDataList.size());

                                } else if (responseStatus.equals("nodata")) {

                                    ll_not_found.setVisibility(View.VISIBLE);
                                    common_search_scroll.setVisibility(View.GONE);

                                }

                            }


                            @Override
                            public void onFailure(Call<GetCommonSearchTypePojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();

                    }


                } else {

                    getCommonSearchTypeDataList.clear();
                    recycler_view_common_search.setAdapter(null);

                }
*/

                return true;
            }


        });

    }

    @Override
    public void switchTab(String searchTab, String searchtext) {

        recycler_view_common_search.setVisibility(View.GONE);
        ll_search_tabs.setVisibility(View.VISIBLE);

        Log.e(TAG, "getSelectedSearchResults:" + searchtext);
        Log.e(TAG, "getSelectedSearchResults:" + searchTab);


        searchText = searchtext;

        hideKeyboard(rl_common_search);

        Bundle bundle = new Bundle();
        bundle.putString("params", searchtext);

        /*searchAll.setArguments(bundle);
        TabLayout.Tab tab = searchTablayout.getTabAt(0);
        assert tab != null;
        tab.select();
        replaceFragment(searchAll);*/

        switch (searchTab) {


            case "People":

                searchPeople.setArguments(bundle);
                TabLayout.Tab tab = searchTablayout.getTabAt(1);
                assert tab != null;
                tab.select();
                replaceFragment(searchPeople);
                break;

            case "Feeds":

                searchFeeds.setArguments(bundle);
                TabLayout.Tab tab1 = searchTablayout.getTabAt(2);
                assert tab1 != null;
                tab1.select();
                replaceFragment(searchFeeds);
                break;


            case "Reviews":

                searchReview.setArguments(bundle);
                TabLayout.Tab tab2= searchTablayout.getTabAt(3);
                assert tab2 != null;
                tab2.select();
                replaceFragment(searchReview);
                break;


            case "Hotel":

                searchRestaurent.setArguments(bundle);
                TabLayout.Tab tab3 = searchTablayout.getTabAt(4);
                assert tab3 != null;
                tab3.select();
                replaceFragment(searchRestaurent);
                break;

            case "Events":

                searchEvents.setArguments(bundle);
                TabLayout.Tab tab4 = searchTablayout.getTabAt(5);
                assert tab4 != null;
                tab4.select();
                replaceFragment(searchEvents);
                break;

            case "Questions":

                searchQA.setArguments(bundle);
                TabLayout.Tab tab5 = searchTablayout.getTabAt(6);
                assert tab5 != null;
                tab5.select();
                replaceFragment(searchQA);
                break;


            default:

                searchAll.setArguments(bundle);
                TabLayout.Tab tab7 = searchTablayout.getTabAt(0);
                assert tab7 != null;
                tab7.select();
                replaceFragment(searchAll);

                break;

        }

        /*searchPeople.setArguments(bundle);
        replaceFragment(searchPeople);*/
        ll_search_tabs.setVisibility(View.VISIBLE);

    }

    public void getSelectedSearchResults(View view, String searchtext, String searchType) {

        // Hide the search list
        recycler_view_common_search.setVisibility(View.GONE);
        ll_search_tabs.setVisibility(View.VISIBLE);

        Log.e(TAG, "getSelectedSearchResults:" + searchtext);
        Log.e(TAG, "searchType:" + searchType);

        searchText = searchtext;
        hideKeyboard(rl_common_search);

        Bundle bundle = new Bundle();
        bundle.putString("params", searchtext);

       /* searchAll.setArguments(bundle);
        TabLayout.Tab tab = searchTablayout.getTabAt(0);
        assert tab != null;
        tab.select();
        replaceFragment(searchAll);*/

        switch (searchType) {

            case "People":

                searchPeople.setArguments(bundle);
                TabLayout.Tab tab = searchTablayout.getTabAt(1);
                assert tab != null;
                tab.select();
                replaceFragment(searchPeople);

                break;

            case "Timeline":

                searchFeeds.setArguments(bundle);
                TabLayout.Tab tab1 = searchTablayout.getTabAt(2);
                assert tab1 != null;
                tab1.select();
                replaceFragment(searchFeeds);
                break;


            case "Review":

                searchReview.setArguments(bundle);
                TabLayout.Tab tab2 = searchTablayout.getTabAt(3);
                assert tab2 != null;
                tab2.select();
                replaceFragment(searchReview);
                break;
            case "Hotel":

                searchRestaurent.setArguments(bundle);
                TabLayout.Tab tab3 = searchTablayout.getTabAt(4);
                assert tab3 != null;
                tab3.select();
                replaceFragment(searchRestaurent);
                break;

          /*  case "Hotel":

                searchReview.setArguments(bundle);
                TabLayout.Tab tab5 = searchTablayout.getTabAt(4);
                assert tab5 != null;
                tab5.select();
                replaceFragment(searchReview);
                break;
*/

            case "Events":

                searchEvents.setArguments(bundle);
                TabLayout.Tab tab4 = searchTablayout.getTabAt(5);
                assert tab4 != null;
                tab4.select();
                replaceFragment(searchEvents);
                break;

            case "Question":

                searchQA.setArguments(bundle);
                TabLayout.Tab tab_5 = searchTablayout.getTabAt(6);
                assert tab_5 != null;
                tab_5.select();
                replaceFragment(searchQA);
                break;

        }

        /*searchPeople.setArguments(bundle);
        replaceFragment(searchPeople);*/
        ll_search_tabs.setVisibility(View.VISIBLE);

    }


    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {


            case R.id.back:

                finish();

                break;

        }
    }

    //Hide keyboard
    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }

    public void callSearch(String query) {
        //Do searching

        if (isConnected(CommonSearch.this)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(CommonSearch.this, "userId"));

                Log.e("CommonSearchSize", "CommonSearchSize->" + query);

                Call<GetCommonSearchTypePojo> call = apiService.getCommonResults("get_common_search", query);

                call.enqueue(new Callback<GetCommonSearchTypePojo>() {
                    @Override
                    public void onResponse(Call<GetCommonSearchTypePojo> call, Response<GetCommonSearchTypePojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            ll_not_found.setVisibility(View.GONE);
                            common_search_scroll.setVisibility(View.VISIBLE);

                            //Success

                            getCommonSearchTypeDataList.clear();


                            // adding contacts to contacts list

                            for (int i = 0; i < response.body().getData().size(); i++) {

                                String searchtext = response.body().getData().get(i).getSearchtext();
                                String searchtype = response.body().getData().get(i).getSearchtype();

                                getCommonSearchTypeData = new GetCommonSearchTypeData(searchtext, searchtype);
                                getCommonSearchTypeDataList.add(getCommonSearchTypeData);


                            }


                            // refreshing recycler view
                            commonSearchListAdapter.notifyDataSetChanged();


                            /*CommonSearchListAdapter commonSearchListAdapter = new CommonSearchListAdapter(CommonSearch.this, getCommonSearchTypeDataList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CommonSearch.this);
                            recycler_view_common_search.setLayoutManager(mLayoutManager);
                            recycler_view_common_search.setItemAnimator(new DefaultItemAnimator());
                            recycler_view_common_search.setAdapter(commonSearchListAdapter);
                            recycler_view_common_search.hasFixedSize();
                            Log.e("CommonSearchSize", "CommonSearchSize->" + getCommonSearchTypeDataList.size());*/


                        } else if (responseStatus.equals("nodata")) {

                            ll_not_found.setVisibility(View.VISIBLE);
                            common_search_scroll.setVisibility(View.GONE);

                        }

                    }


                    @Override
                    public void onFailure(Call<GetCommonSearchTypePojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {


        }


    }

    public void replaceFragment(Fragment fragment) {

        try {

            FragmentManager fm = getSupportFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            FragmentTransaction ft = fm.beginTransaction();
            ft.detach(fragment);
            ft.attach(fragment);
            ft.replace(R.id.search_container, fragment);
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }


    /*public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.common_search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.common_search_options);

        SearchView searchView = (SearchView) searchItem.getActionView();
        ImageView searchClose = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);

        searchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(CommonSearch.this, "", Toast.LENGTH_SHORT).show();

            }
        });

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(null!=searchManager ) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }

        searchView.setIconifiedByDefault(false);
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.setQueryHint("Search");
        searchView.requestFocusFromTouch();

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {



                return false;
            }
        });



        return true;
    }
*/


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


}
