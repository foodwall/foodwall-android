package com.kaspontech.foodwall.commonSearchView.Fragments;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.adapters.CommonSearch.Events.GetSearchResultsEventsAdapter;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.GetCommonSearchPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchEventPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchPersonPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchQuestionPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchReviewPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchTimelinePojo;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.commonSearchView.CommonSearch.searchText;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchEvents extends Fragment {


    View view;
    Context context;
    String searchKey;

    LinearLayout ll_no_results;
    RecyclerView search_results_events_recylerview;
    GetSearchResultsEventsAdapter getSearchResultsEventsAdapter;

    List<GetCommonSearchPojo> getCommonSearchTypeDataList = new ArrayList<>();


    public SearchEvents() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_search_events, container, false);
        context = view.getContext();

        ll_no_results = (LinearLayout) view.findViewById(R.id.ll_no_results);
        search_results_events_recylerview = (RecyclerView)view.findViewById(R.id.search_results_events_recylerview);



        if (getArguments() != null) {
            searchKey = getArguments().getString("params");
        }


        callSearch(searchText);


        return view;
    }

    public void callSearch(String query) {

        //Do searching

        if (isConnected(context)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                getCommonSearchTypeDataList.clear();

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Log.e("CommonSearchSize", "CommonSearchSize->" + query);

                Call<GetCommonSearchPojo> call = apiService.getCommonSearchResults("get_common_search_result", query, userid);

                call.enqueue(new Callback<GetCommonSearchPojo>() {
                    @Override
                    public void onResponse(Call<GetCommonSearchPojo> call, Response<GetCommonSearchPojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatusPeople", "responseStatusPeople->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            List<SearchPersonPojo> searchPersonPojoList = new ArrayList<>();
                            List<SearchEventPojo> searchEventPojoList = new ArrayList<>();
                            List<SearchQuestionPojo> searchQuestionPojoList= new ArrayList<>();
                            List<SearchReviewPojo> searchReviewPojoList= new ArrayList<>();
                            List<SearchTimelinePojo> searchTimelinePojoList= new ArrayList<>();

                            //Success

                            searchEventPojoList = response.body().getData().get(4).getEvents();

                            if(searchEventPojoList != null){

                                if (searchEventPojoList.size() == 0){

                                    ll_no_results.setVisibility(View.VISIBLE);

                                }else{

                                    if(searchEventPojoList.get(0).getSearch_val().equals("0")){

                                        ll_no_results.setVisibility(View.VISIBLE);

                                    }else{

                                        ll_no_results.setVisibility(View.GONE);
                                        getSearchResultsEventsAdapter = new GetSearchResultsEventsAdapter(context, searchEventPojoList);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                        search_results_events_recylerview.setLayoutManager(mLayoutManager);
                                        search_results_events_recylerview.setItemAnimator(new DefaultItemAnimator());
                                        search_results_events_recylerview.setAdapter(getSearchResultsEventsAdapter);
                                        search_results_events_recylerview.hasFixedSize();
                                        getSearchResultsEventsAdapter.notifyDataSetChanged();
                                    }




                                }
                            }



                        } else if (responseStatus.equals("nodata")) {


                        }

                    }


                    @Override
                    public void onFailure(Call<GetCommonSearchPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {


        }


    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


}
