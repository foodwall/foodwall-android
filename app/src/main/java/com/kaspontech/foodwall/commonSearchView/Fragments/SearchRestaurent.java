package com.kaspontech.foodwall.commonSearchView.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.adapters.CommonSearch.Restaurant.GetSearchResultsRestaurantAdapter;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.GetCommonSearchPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchEventPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchPersonPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchQuestionPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchRestaurantPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchReviewPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.SearchTimelinePojo;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.commonSearchView.CommonSearch.searchText;


public class SearchRestaurent extends Fragment {


    View view;
    Context context;
    String searchKey;
    LinearLayout ll_no_results;
    RecyclerView restaurent_recylerview;
    List<GetCommonSearchPojo> getCommonSearchTypeDataList = new ArrayList<>();
    GetSearchResultsRestaurantAdapter getSearchResultsRestaurantAdapter;

    private static final String TAG = "SearchRestaurent";

    public SearchRestaurent() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           /* searchText = getArguments().getString(ARG_PARAM1);
            Log.e(TAG, "onCreate: "+searchText );*/
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_search_restaurent, container, false);
        context = view.getContext();

        ll_no_results = (LinearLayout) view.findViewById(R.id.ll_no_results);
        restaurent_recylerview = (RecyclerView)view.findViewById(R.id.restaurent_recylerview);

        if (getArguments() != null) {
            searchKey = getArguments().getString("params");
        }

        Log.e("SearchRestaurant","SearchRestaurant->"+searchText);
        Log.e("SearchRestaurant","SearchRestaurant->"+searchKey);

        callSearch(searchText);

        return view;
    }


    public void callSearch(String query) {

        //Do searching

        if (isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                getCommonSearchTypeDataList.clear();

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Log.e("CommonSearchSize", "CommonSearchSize->" + query);

                Call<GetCommonSearchPojo> call = apiService.getCommonSearchResults("get_common_search_result", query, userid);

                call.enqueue(new Callback<GetCommonSearchPojo>() {
                    @Override
                    public void onResponse(Call<GetCommonSearchPojo> call, Response<GetCommonSearchPojo> response) {


                        Log.e("responseStatusPeople", "responseStatusPeople->" + response);

                        if (response.body().getResponseMessage().equals("success")) {

                            List<SearchPersonPojo> searchPersonPojoList = new ArrayList<>();
                            List<SearchEventPojo> searchEventPojoList = new ArrayList<>();
                            List<SearchQuestionPojo> searchQuestionPojoList= new ArrayList<>();
                            List<SearchReviewPojo> searchReviewPojoList= new ArrayList<>();
                            List<SearchTimelinePojo> searchTimelinePojoList= new ArrayList<>();
                            List<SearchRestaurantPojo> searchRestaurantPojoList = new ArrayList<>();

                            //Success

                            searchRestaurantPojoList = response.body().getData().get(5).getHotel();


                            Log.e(TAG, "onResponse: "+searchRestaurantPojoList.size() );

                           /* if (searchRestaurantPojoList.size()==0){

                                ll_no_results.setVisibility(View.VISIBLE);

                            }else{*/

                                ll_no_results.setVisibility(View.GONE);
                                getSearchResultsRestaurantAdapter  = new GetSearchResultsRestaurantAdapter(context, searchRestaurantPojoList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                restaurent_recylerview.setLayoutManager(mLayoutManager);
                                restaurent_recylerview.setItemAnimator(new DefaultItemAnimator());
                                restaurent_recylerview.setAdapter(getSearchResultsRestaurantAdapter);
                                restaurent_recylerview.hasFixedSize();
                                getSearchResultsRestaurantAdapter.notifyDataSetChanged();


                           /* }*/


                        } else if (response.body().getResponseMessage().equals("nodata")) {


                        }

                    }


                    @Override
                    public void onFailure(Call<GetCommonSearchPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        ll_no_results.setVisibility(View.VISIBLE);
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {


        }


    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


}
