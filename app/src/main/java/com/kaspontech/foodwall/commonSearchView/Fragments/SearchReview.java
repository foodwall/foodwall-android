package com.kaspontech.foodwall.commonSearchView.Fragments;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.adapters.CommonSearch.Review.GetSearchResultsReviewAdapter;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.GetCommonSearchPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_hotel_all_inner_review_pojo;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.commonSearchView.CommonSearch.searchText;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchReview extends Fragment {


    View view;
    Context context;
    String searchKey;

    LinearLayout ll_no_results;
    RecyclerView search_results_review_recylerview;
    GetSearchResultsReviewAdapter getSearchResultsReviewAdapter;

    List<GetCommonSearchPojo> getCommonSearchTypeDataList = new ArrayList<>();


    public SearchReview() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_search_review, container, false);
        context = view.getContext();

        ll_no_results = (LinearLayout) view.findViewById(R.id.ll_no_results);
        search_results_review_recylerview = (RecyclerView) view.findViewById(R.id.search_results_review_recylerview);



        Log.e("SearchReview", "SearchReview->" + searchText);

        if (getArguments() != null) {
            searchKey = getArguments().getString("params");
        }


        callSearch(searchText);

        return view;
    }


    public void callSearch(String query) {

        //Do searching

        if (isConnected(context)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                getCommonSearchTypeDataList.clear();

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Log.e("CommonSearchSize", "CommonSearchSize->" + query);

                Call<GetCommonSearchPojo> call = apiService.getCommonSearchResults("get_common_search_result", query, userid);

                call.enqueue(new Callback<GetCommonSearchPojo>() {
                    @Override
                    public void onResponse(Call<GetCommonSearchPojo> call, Response<GetCommonSearchPojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatusPeople", "responseStatusPeople->" + responseStatus);

                        if (responseStatus.equals("success")) {

//                            List<SearchPersonPojo> searchPersonPojoList = new ArrayList<>();
//                            List<SearchEventPojo> searchEventPojoList = new ArrayList<>();
//                            List<SearchQuestionPojo> searchQuestionPojoList = new ArrayList<>();
                            ArrayList<Get_hotel_all_inner_review_pojo> searchReviewPojoList = new ArrayList<>();
//                            List<SearchTimelinePojo> searchTimelinePojoList = new ArrayList<>();

                            //Success

                            searchReviewPojoList = response.body().getData().get(2).getReview();

                            if (searchReviewPojoList != null) {

                                if (searchReviewPojoList.size() == 0) {

                                    ll_no_results.setVisibility(View.VISIBLE);

                                } else {

                                    if (searchReviewPojoList.get(0).getSearchVal().equals("0")) {

                                        ll_no_results.setVisibility(View.VISIBLE);


                                    } else {


                                        ll_no_results.setVisibility(View.GONE);
                                        getSearchResultsReviewAdapter = new GetSearchResultsReviewAdapter(context, searchReviewPojoList,"");
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                        search_results_review_recylerview.setLayoutManager(mLayoutManager);
                                        search_results_review_recylerview.setItemAnimator(new DefaultItemAnimator());
                                        search_results_review_recylerview.setAdapter(getSearchResultsReviewAdapter);
                                        search_results_review_recylerview.hasFixedSize();
                                        getSearchResultsReviewAdapter.notifyDataSetChanged();

                                    }


                                }
                            }


                        } else if (responseStatus.equals("nodata")) {

                            ll_no_results.setVisibility(View.VISIBLE);


                        }

                    }


                    @Override
                    public void onFailure(Call<GetCommonSearchPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {


        }


    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }
}
