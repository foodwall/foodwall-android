package com.kaspontech.foodwall.utills;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.kaspontech.foodwall.R;

public class ViewImage extends AppCompatActivity {



    String imageurl;
    ImageView imageView;
    ImageButton back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event_image);

        imageView = (ImageView)findViewById(R.id.eventImage);
        back = (ImageButton)findViewById(R.id.back);

        Intent intent = getIntent();
        imageurl = intent.getStringExtra("imageurl");

        GlideApp.with(ViewImage.this)
                .load(imageurl)
                .into(imageView);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }
}
