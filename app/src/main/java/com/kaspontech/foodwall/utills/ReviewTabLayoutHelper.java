package com.kaspontech.foodwall.utills;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.reviewpackage.deliveryTab.DeliveryFragment;
import com.kaspontech.foodwall.reviewpackage.dineInTab.DineInFragment;

/**
 * Created by balaji on 20/2/18.
 */

public class ReviewTabLayoutHelper {

    Context context;
    private static final String TAG = "ReviewTabLayoutHelper";

    TabLayout reviewsTablayout;

    // Fragments
    Fragment dineIn,delivery,veg,nonVeg,drinks;


    public ReviewTabLayoutHelper(TabLayout reviewsTablayout) {
        this.reviewsTablayout = reviewsTablayout;
    }

    public void setUpReviewFragments(){

        dineIn = new DineInFragment();
        delivery = new DeliveryFragment();

    }

    public void setUpReviewTabLayout(TabLayout reviewsTablayout){

        Log.d(TAG, "setUpReviewTabLayout: Tabs Added Sucessfully ");

        reviewsTablayout.addTab(reviewsTablayout.newTab().setText("Dine In"));
        reviewsTablayout.addTab(reviewsTablayout.newTab().setText("Delivery"));
        reviewsTablayout.addTab(reviewsTablayout.newTab().setText("Veg"));
        reviewsTablayout.addTab(reviewsTablayout.newTab().setText("Non Veg"));
        reviewsTablayout.addTab(reviewsTablayout.newTab().setText("Cafes & More"));
        reviewsTablayout.addTab(reviewsTablayout.newTab().setText("Drinks"));

    }




    public void bindReviewTabsWithAnEvent() {

        reviewsTablayout.addOnTabSelectedListener (new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {

                    case 0:
                        replaceFragment(dineIn);
                        break;
                    case 1:
                        replaceFragment(delivery);

                        break;

                    case 2:
                        break;

                    case 3:
                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }


        });
    }




    public void replaceFragment(Fragment fragment) {

        try {

            FragmentManager fragmentManager = fragment.getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.review_container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }





}
