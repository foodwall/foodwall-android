package com.kaspontech.foodwall.utills;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.kaspontech.foodwall.R;

public class universalLoaderClass extends Dialog {

    public AppCompatActivity c;
    public Dialog d;

    public universalLoaderClass(AppCompatActivity a) {
        super(a);
        // TODO Auto-generated constructor stub
        c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(true);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.universal_loader);

    }




}
