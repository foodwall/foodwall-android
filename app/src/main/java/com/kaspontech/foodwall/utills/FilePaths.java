package com.kaspontech.foodwall.utills;

import android.os.Environment;

/**
 * Created by vishnukm on 7/3/18.
 */

public class FilePaths {


    //"storage/emulated/0"
    public String ROOT_DIR = Environment.getExternalStorageDirectory().getPath();

    public String PICTURES = ROOT_DIR + "/Pictures";
    public String PICTURES2 = ROOT_DIR + "/storage/emulated/0/pictures";
    public String CAMERA = ROOT_DIR + "/DCIM/camera";
    public String STORIES = ROOT_DIR + "/Stories";

    public String FIREBASE_STORY_STORAGE = "stories/users";
    public String FIREBASE_IMAGE_STORAGE = "photos/users/";
}
