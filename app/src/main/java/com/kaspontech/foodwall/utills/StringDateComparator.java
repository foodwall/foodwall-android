package com.kaspontech.foodwall.utills;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

public class StringDateComparator implements Comparator<String> {

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    int date;

    public int compare(String lhs, String rhs)
    {
        try {


            date = dateFormat.parse(lhs).compareTo(dateFormat.parse(rhs));


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
