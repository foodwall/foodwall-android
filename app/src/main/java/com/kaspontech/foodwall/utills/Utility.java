package com.kaspontech.foodwall.utills;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dmax.dialog.SpotsDialog;

public class Utility {


    public static AlertDialog dialog = null;


    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;



    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * Validation for password
     **/
    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#!$%^&+=])(?=\\S+$).{8,15}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }


    public static void univerLoader(Context context){


        class UniversalLoaderDialogClass extends Dialog {

            public Context c;
            public Dialog d;

            TextView txt_loading;


            UniversalLoaderDialogClass(Context a) {
                super(a);
                // TODO Auto-generated constructor stub
                c = a;
            }

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                setCancelable(true);
                requestWindowFeature(Window.FEATURE_NO_TITLE);
                setContentView(R.layout.universal_loader);

//            txt_loading = (TextView) findViewById(R.id.txt_loading);




            }


            public  UniversalLoaderDialogClass loaderDialogClass;

        }



        final UniversalLoaderDialogClass loaderDialogClass = new UniversalLoaderDialogClass(context);
        loaderDialogClass.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loaderDialogClass.setCancelable(true);
        loaderDialogClass.show();

    }

    public void univerLoaderDismiss(Context context){
    }


    public static void showDottedDialog(Context context) {

        dialog = new SpotsDialog.Builder().setContext(context).setMessage("").build();
        dialog.show();

    }

    public static void dismissDottedDialog() {

        dialog.show();

    }


    /* e-mail address validation */
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    /*
     * Hide keyboard
     * */
    public final static void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }
    /*
     * Show keyboard
     * */
    public final static void showKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
    }


    /*
     * Image Loading using GlideApp
     * */
    public  final static void picassoImageLoader(String url, int style, ImageView imageView, Context context){



        if(style==0){

            GlideApp.with(context)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.01f)
                    .centerInside()
                    .placeholder(R.drawable.ic_add_photo)
                    .into(imageView);



        }else if(style==1){

            GlideApp.with(context)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.01f)
                    .centerCrop()
                    .placeholder(R.drawable.ic_add_photo)
                    .into(imageView);

        }else {

            GlideApp.with(context)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.01f)
                    .centerInside()
                    .into(imageView);

        }



    }

    /*
     * Internet connectivity checking
     * */

    public static boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }



    /**
     * Universal loader for the entire application*
     **/
    public static void universalLoader(Context context){

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater1 = LayoutInflater.from(context);
        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.universal_loader, null);
        dialogBuilder.setView(dialogView);

        dialogBuilder.setTitle("");

        dialog = dialogBuilder.create();

        dialog.show();

//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = 550;
//        lp.height = 200;
//        dialog.getWindow().setAttributes(lp);

    }

    /**
     * Universal loader dismiss for the entire application*
     **/

    public static void universalLoaderDismiss(){

        dialog.dismiss();


    }

    /**
     * TimeStamp setting
     **/

    public final static void setTimeStamp(String createdOn, TextView textView){


        try {

            //Input time

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            long time = sdf.parse(createdOn).getTime();


            Date currentTime = Calendar.getInstance().getTime();

            SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            String now_new= sdformat.format(currentTime);

            long nowdate = sdformat.parse(now_new).getTime();


            CharSequence ago = DateUtils.getRelativeTimeSpanString(time, nowdate, DateUtils.FORMAT_ABBREV_RELATIVE);

            if (ago.toString().equalsIgnoreCase("0 minutes ago")||ago.toString().equalsIgnoreCase("In 0 minutes")) {

                textView.setText(R.string.just_now);

            } else {

                textView.setText(ago);
            }

        } catch (ParseException e) {

            e.printStackTrace();

        }
    }






}