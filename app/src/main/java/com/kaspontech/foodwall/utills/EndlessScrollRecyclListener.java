package com.kaspontech.foodwall.utills;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class EndlessScrollRecyclListener extends RecyclerView.OnScrollListener {

    public static String TAG = EndlessScrollRecyclListener.class.getSimpleName();

    // use your LayoutManager instead
    private RecyclerView.LayoutManager llm;

    public EndlessScrollRecyclListener(RecyclerView.LayoutManager sglm) {
        this.llm = sglm;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (!recyclerView.canScrollVertically(1)) {
            onScrolledToEnd();
        }
    }

    public abstract void onScrolledToEnd();
}