package com.kaspontech.foodwall.utills;

import android.util.Log;


public class TimeAgo {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    private static String TAG="TimeAgo";

    public static String getTimeAgo_new(long time,long nowdate) {
        if (time < 1000000000000L) {
            time *= 1000;
        }

        Log.e(TAG, "getTimeAgo_new: time"+time );
        Log.e(TAG, "getTimeAgo_new: nowdate"+nowdate );

           /* if (time > nowdate || time <= 0) {


                return null;
            }*/


        final long diff = nowdate - time;
        Log.e(TAG, "getTimeAgo_new: diff"+diff );
        if (diff < MINUTE_MILLIS) {
            return "Just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "A minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "An hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "Yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }
}