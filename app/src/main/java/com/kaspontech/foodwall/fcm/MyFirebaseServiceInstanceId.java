package com.kaspontech.foodwall.fcm;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kaspontech.foodwall.utills.Pref_storage;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * Created by balaji on 6/3/18.
 */
public class MyFirebaseServiceInstanceId extends FirebaseInstanceIdService {

    // Data Storage
    Pref_storage pref_storage;

    public static final String TOKEN_BROADCAST = "FcmBroadcast";
    private static final String TAG = "MyFirebaseServiceInstan";


    //this method will be called
    //when the token is generated
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        //now we will have the token
        String deviceToken = FirebaseInstanceId.getInstance().getToken();
        //for now we are displaying the token in the log
        //copy it as this method is called only when the new token is generated
        //and usually new token is only generated when the app is reinstalled or the data is cleared
    }

  /*  @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN",s);

        //pref_storage = new Pref_storage();

        // Get updated InstanceID token.

        //getApplicationContext().sendBroadcast(new Intent(TOKEN_BROADCAST));

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
//        sendRegistrationToServer(refreshedToken);

       // storeRegistrationId(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

    }*/


    private void storeRegistrationId(String refreshedToken) {

        Pref_storage.setDetail(getApplicationContext(),"LoginFCM-key",refreshedToken);

    }


}