package com.kaspontech.foodwall.fcm;

public class Constants {
    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final int NOTIFY_ID = 1;

    public static final String CHANNEL_ID = "my_channel_01";
    public static final String CHANNEL_NAME = "Simplified Coding Notification";
    public static final String CHANNEL_DESCRIPTION = "www.simplifiedcoding.net";
    // id to handle the notification in the notification tray

    public static final String myBucketList = "You added a new bucket";
    public static final String TimeLineView = "TimeLineView";
    public static final String Reviews = "Reviews";
    public static final String questionAnswer = "questionAnswer";
    public static final String All = "allFrag";
    public static final String NewDineIN = "newDineIN";
    public static final String DeliveryView = "deliveryView";
    public static final String redirectView = "https://foodwall.app/index.php/timeline/timeline_inner/";
    public static final String qaredirectView = "https://foodwall.app/index.php/QA/qa_inner/";
}
