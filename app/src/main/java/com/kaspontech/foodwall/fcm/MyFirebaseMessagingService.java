package com.kaspontech.foodwall.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.R;
 import com.kaspontech.foodwall.eventspackage.Events;
import com.kaspontech.foodwall.loginPackage.Login;
import com.kaspontech.foodwall.profilePackage.Profile;

import org.json.JSONException;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    NotificationManager mNotificationManager;
    private NotificationCompat.Builder notificationBuilder;
    public Notification notification;
    NotificationChannel mChannel;



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);





        JSONObject json = new JSONObject(remoteMessage.getData());

        Log.i("MyFirebaseMsgService", "Data Payload: " + json);

        handleDataMessage(json);


        //data messages getdata object put as jsonobject
        //data messages using get json of remote message of data


      /*  JSONObject json = new JSONObject(remoteMessage.getData());
        //when data messages can receive them to show to method
        Log.i("MyFirebaseMsgService", "Data Payload: " + json);

        handleDataMessage(json);

        Log.i("MyFirebaseMsgService", "Data Payload: " + json);*/


    }

    private void handleDataMessage(JSONObject json) {


        //api response get jsonobject of message
        try {
            //  JSONObject data1  = json.getJSONObject("notification");
            Log.i("MyFirebaseMsgService", "Data Payload: " + json);

            //get the json object of message inside the title
            String title = json.getString("title");
            String badge = json.getString("badge");
            String image = json.getString("image");
            String sound = json.getString("sound");
            //get the json object of message inside the message
            String body = json.getString("body");
            String click_action = json.getString("click_action");
            Log.i("MyFirebaseMsgService", "Data Payload: " + json);

            Intent intent;


            if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.N) {

                PendingIntent resultIntent;
                // Do something for Navigations to Going Activity
                //if App is Foreground using Notification Sound show
                if (!NotificationUtils.isAppIsInBackground(this)) {

                    if (click_action.equalsIgnoreCase("follower_request")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Profile.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                    } else if (click_action.equalsIgnoreCase("follower_accept")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Profile.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("timeline_tag")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("bucket_individual")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "profile");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("bucket_group")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "profile");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("event_private")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Events.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("question_asked")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                       // intent.putExtra("redirect", "from_qa"); // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("answered_ques")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("review_hotel")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "from_review");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("review_shared")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "from_review");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("chat")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        startActivity(intent);   // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else {
                        intent = new Intent(this, Login.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                    }
                } else {

                    if (click_action.equalsIgnoreCase("follower_request")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Profile.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                    } else if (click_action.equalsIgnoreCase("follower_accept")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Profile.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("timeline_tag")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("bucket_individual")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "profile");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("bucket_group")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "profile");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("event_private")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Events.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("question_asked")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("answered_ques")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("review_hotel")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "from_review");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("review_shared")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "from_review");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("chat")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                         // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else {
                        intent = new Intent(this, Login.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                    }
                        /* // Create an Intent for the activity you want to start
                           intent = new Intent(this, Menupage.class);
                         // Create the TaskStackBuilder and add the intent, which inflates the back stack
                         TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                         stackBuilder.addNextIntentWithParentStack(intent);
                         // Get the PendingIntent containing the entire back stack
                         resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);*/
                }


                /* Showing notification with text only */
                //  Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.appicon);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                notificationBuilder = new NotificationCompat.Builder(this)
                        // .setStyle(inboxStyle)
                        // .setLargeIcon(icon)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(resultIntent);

                // Will display the notification in the notification bar
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notification = notificationBuilder.build();
                notificationManager.notify(Constants.NOTIFY_ID, notification);


            } else {

                //  Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.appicon);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationManager mNotific = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                int importanceHigh = NotificationManager.IMPORTANCE_HIGH;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    mChannel = new NotificationChannel(Constants.CHANNEL_ID, Constants.CHANNEL_ID, importanceHigh);
                    mChannel.setDescription(body);
                    mChannel.setLightColor(Color.GREEN);
                    mChannel.canShowBadge();
                    mChannel.setShowBadge(true);

                    mNotific.createNotificationChannel(mChannel);


                }

                PendingIntent resultIntent;
                if (!NotificationUtils.isAppIsInBackground(this)) {

                    if (click_action.equalsIgnoreCase("follower_request")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Profile.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                    } else if (click_action.equalsIgnoreCase("follower_accept")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Profile.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("timeline_tag")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("bucket_individual")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "profile");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("bucket_group")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "profile");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("event_private")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Events.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("question_asked")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("answered_ques")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("review_hotel")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "from_review");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("review_shared")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "from_review");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("chat")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                         // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Login.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    }

                } else {

                        /* // Create an Intent for the activity you want to start
                           intent = new Intent(this, Login.class);
                         // Create the TaskStackBuilder and add the intent, which inflates the back stack
                         TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                         stackBuilder.addNextIntentWithParentStack(intent);
                         // Get the PendingIntent containing the entire back stack
                         resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);*/
                    if (click_action.equalsIgnoreCase("follower_request")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Profile.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                    } else if (click_action.equalsIgnoreCase("follower_accept")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Profile.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("timeline_tag")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("bucket_individual")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "profile");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("bucket_group")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "profile");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("event_private")) {

                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Events.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("question_asked")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("answered_ques")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("review_hotel")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "from_review");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("review_shared")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                        intent.putExtra("redirect", "from_review");
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else if (click_action.equalsIgnoreCase("chat")) {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Home.class);
                         // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else {
                        // Create an Intent for the activity you want to start
                        intent = new Intent(this, Login.class);
                        // Create the TaskStackBuilder and add the intent, which inflates the back stack
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addNextIntentWithParentStack(intent);
                        // Get the PendingIntent containing the entire back stack
                        resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    }

                }


                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

                    Notification notification = new NotificationCompat.Builder(getApplicationContext(), Constants.CHANNEL_ID)
                            .setContentTitle(title)
                            .setContentText(body)
                            //.setBadgeIconType(R.drawable.appicon)
                            // .setLargeIcon(icon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setWhen(System.currentTimeMillis())
                            .setGroup(body)
                            .setSound(defaultSoundUri)
                            //.setStyle(inboxStyle)
                            .setAutoCancel(true)
                            .setContentIntent(resultIntent)
                            .build();

                    mNotific.notify(Constants.NOTIFY_ID, notification);


                } else {

                    /* Showing notification with text only */
                    notificationBuilder = new NotificationCompat.Builder(this)
                            // .setStyle(inboxStyle)
                            //  .setLargeIcon(icon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setDefaults(Notification.DEFAULT_SOUND)
                            .setWhen(System.currentTimeMillis())
                            .setGroup(body)
                            .setContentIntent(resultIntent);

                    // Will display the notification in the notification bar
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notification = notificationBuilder.build();
                    notificationManager.notify(Constants.NOTIFY_ID, notification);


                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}
