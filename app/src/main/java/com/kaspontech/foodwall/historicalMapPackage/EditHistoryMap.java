package com.kaspontech.foodwall.historicalMapPackage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.kaspontech.foodwall.adapters.HistoricalMap.EditHistoricalTagPeopleAdapter;
import com.kaspontech.foodwall.adapters.HistoricalMap.EditRestaurantSearchAdapter;
import com.kaspontech.foodwall.adapters.HistoricalMap.HistoricalTagPeopleAdapter;
import com.kaspontech.foodwall.adapters.HistoricalMap.HistoryImagesAdapter;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersOutput;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.Geometry;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.GetRestaurantsResult;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.Get_HotelStaticLocation_Response;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.OpeningHours;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.RestaurantDetails;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.RestaurantPhoto;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Image;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.TaggedPeoplePojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.profile_timeline_output_response;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.REST_API.API_interface_restaurant;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.REST_API.ProgressRequestBody;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import dmax.dialog.SpotsDialog;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import id.zelory.compressor.Compressor;
import mabbas007.tagsedittext.TagsEditText;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditHistoryMap extends AppCompatActivity implements
        View.OnClickListener,
        TagsEditText.TagsEditListener,
        ProgressRequestBody.UploadCallbacks,
        EditHistoricalTagPeopleAdapter.ContactsAdapterListener,
        HistoryImagesAdapter.PhotoRemovedListener {


    // Widgets


    private ViewPager mviewpager;

    private SearchView search_followers;

    private TagsEditText friendsListDisplay;

    private CircleIndicator circleIndicator;

    private Toolbar actionbar, selectActionBar;

    private EmojiconEditText description_of_visit;

    private TextView tv_createHistory, restaurantName, tv_add_photo;

    private RelativeLayout rl_add_img_tag, rl_create_post, empty_list;

    private RecyclerView hotel_historical_recylerview, follwersRecylerview;

    private EditText edit_search_hotel, date_of_restaurent_visit, friendsListDisplayTag;

    private LinearLayout ll_search_restaurant, ll_history_fields, ll_history_image_section, ll_tag_people, ll_add_photo;

    private ImageButton imgbtn_gallery, imgbtn_tagpeople, close, edit_restaurants, set_history_date, selectDone, selectClose, add_image_post, add_people;


    // Hotel Search

    // List

    private List<String> types;

    private List<Object> weekdayText;

    private List<RestaurantPhoto> photos;

    private List<String> htmlAttributions;

    private List<OpeningHours> openingHoursPojoList;

    private List<RestaurantDetails> restaurantOutputPojoList;

    private ArrayList<GetRestaurantsResult> getallRestaurantDetailsPojoArrayList = new ArrayList<>();

    private Geometry geometryPojo;

    private OpeningHours openingHoursPojo;


    // Google location search retrofit interface

    private API_interface_restaurant apiService;


    // Tag people function

    private List<Integer> cohostList = new ArrayList<>();

    private List<Integer> friendsList = new ArrayList<>();

    private List<TaggedPeoplePojo> whom = new ArrayList<>();

    private List<GetFollowersData> getFollowersDataList = new ArrayList<>();

    private HashMap<Integer, String> cohostNameIdListd = new HashMap<>();

    private EditHistoricalTagPeopleAdapter editHistoricalTagPeopleAdapter;


    // Camera & Gallery

    private int REQUEST_CAMERA = 0;

    private int PICK_IMAGE_MULTIPLE = 2;


    // Permission

    private static final int REQUEST_CAMERA_PERMISSION = 1;

    private static final int REQUEST_GALLERY_PERMISSION = 2;


    // Image viewpager

    private File file;

    private static int NUM_PAGES = 0;

    private static int currentPage = 0;

    private List<Image> image = new ArrayList<>();

    private List<String> deleteImageId = new ArrayList<>();

    public List<Image> imagesEncodedList = new ArrayList<>();


    // GPS

    GPSTracker gpsTracker;

    private static GoogleApiClient mGoogleApiClient;

    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;

    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";


    //  Variables

    private Uri imageUri;

    private double latitude, longitude;


    private boolean firstTimeTagged = false;


    private int userid, tag_status, image_status, whomCount, imageCount, count = 0;

    private String followerId, firstName, lastName, userPhoto, imageEncoded,
            latLngString, hotel_name, hotel_address, google_id, google_photo, place_id, timelineId, timelineDescription, userChoosenTask, imagepath;


    private static final String TAG = "EditHistoryMap";


    TextView tv_create_history;

    ProgressBar progress_timeline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_history_map);

        userid = Integer.parseInt(Pref_storage.getDetail(EditHistoryMap.this, "userId"));

        getFollowers(userid);


        initGoogleAPIClient();

        //Check Location Permission

        checkPermissions();

        initComponent();


        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            timelineId = bundle.getString("timelineId");

        }


        get_timeline_single();

        if(gpsTracker.isGPSEnabled) {
            // nearByRestaurants(restaurantName);
            nearByRestaurants("");

        }

        //nearByRestaurants();

        edit_search_hotel.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        edit_search_hotel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Your piece of code on keyboard search click

                    String getrestaurantName = edit_search_hotel.getText().toString();

                    String restaurantName = getrestaurantName.replace(" ", "+");

                   // fetchStores(restaurantName);
                    nearByRestaurants(restaurantName);


                    return true;
                }
                return false;
            }
        });

    }

    private void initComponent() {
        gpsTracker = new GPSTracker(EditHistoryMap.this);

        apiService = APIClient_restaurant.getClient().create(API_interface_restaurant.class);

        actionbar = (Toolbar) findViewById(R.id.action_bar_edit_timeline_post);
        close = (ImageButton) actionbar.findViewById(R.id.close);
        imgbtn_gallery = (ImageButton) findViewById(R.id.imgbtn_gallery);
        imgbtn_tagpeople = (ImageButton) findViewById(R.id.imgbtn_tagpeople);
        edit_restaurants = (ImageButton) findViewById(R.id.edit_restaurants);
        set_history_date = (ImageButton) findViewById(R.id.set_history_date);
        add_image_post = (ImageButton) findViewById(R.id.add_image_post);
        add_people = (ImageButton) findViewById(R.id.add_people);
        tv_createHistory = (TextView) actionbar.findViewById(R.id.tv_create_history);
        tv_add_photo = (TextView) findViewById(R.id.tv_add_photo);
        ll_search_restaurant = (LinearLayout) findViewById(R.id.ll_search_restaurant);
        ll_history_fields = (LinearLayout) findViewById(R.id.ll_history_fields);
        ll_history_image_section = (LinearLayout) findViewById(R.id.ll_history_image_section);
        ll_tag_people = (LinearLayout) findViewById(R.id.ll_tag_people);
        ll_add_photo = (LinearLayout) findViewById(R.id.ll_add_photo);
        hotel_historical_recylerview = (RecyclerView) findViewById(R.id.hotel_historical_recylerview);
        edit_search_hotel = (EditText) findViewById(R.id.edit_search_hotel);
        date_of_restaurent_visit = (EditText) findViewById(R.id.date_of_restaurent_visit);
        description_of_visit = (EmojiconEditText) findViewById(R.id.description_of_visit);
        friendsListDisplayTag = (EditText) findViewById(R.id.friendsListDisplayTag);
        restaurantName = (TextView) findViewById(R.id.restaurantName);
        friendsListDisplay = (TagsEditText) findViewById(R.id.friendsListDisplay);
        progress_timeline = (ProgressBar) findViewById(R.id.progress_timeline);

        mviewpager = (ViewPager) findViewById(R.id.mviewpager_post);
        circleIndicator = (CircleIndicator) findViewById(R.id.indicator_post);
        rl_add_img_tag = (RelativeLayout) findViewById(R.id.rl_add_img_tag);
        rl_create_post = (RelativeLayout) findViewById(R.id.rl_create_post);
        tv_create_history = (TextView) actionbar.findViewById(R.id.tv_create_history);
        tv_create_history.setText("Update");

        close.setOnClickListener(this);
        friendsListDisplay.setTagsListener(this);
        date_of_restaurent_visit.setOnClickListener(this);
        tv_createHistory.setOnClickListener(this);
        friendsListDisplay.setOnClickListener(this);
        edit_restaurants.setOnClickListener(this);
        set_history_date.setOnClickListener(this);
        imgbtn_tagpeople.setOnClickListener(this);
        imgbtn_gallery.setOnClickListener(this);
        friendsListDisplayTag.setOnClickListener(this);
        ll_tag_people.setOnClickListener(this);
        ll_add_photo.setOnClickListener(this);
        add_image_post.setOnClickListener(this);
        add_people.setOnClickListener(this);
        rl_create_post.setOnClickListener(this);
        restaurantName.setOnClickListener(this); }

    public void selectedEditRestaurantHandler(View view, int adapterPosition) {

        google_id = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getId();
        google_photo = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getRestaurantPhotos().get(0).getPhotoReference();
        Log.e(TAG, "selectedEditRestaurantHandler: google_photo" + google_photo);
        Log.e(TAG, "selectedEditRestaurantHandler: google_id" + google_id);
        place_id = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getPlaceId();
        Log.e(TAG, "selectedEditRestaurantHandler: place_id" + place_id);
        hotel_name = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getName();
        hotel_address = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getFormattedAddress();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(friendsListDisplay.getWindowToken(), 0);
        ll_search_restaurant.setVisibility(View.GONE);
        ll_history_fields.setVisibility(View.VISIBLE);
//        rl_add_img_tag.setVisibility(View.VISIBLE);
        restaurantName.setText(getallRestaurantDetailsPojoArrayList.get(adapterPosition).getName());
    }

    public void EditTaggedFollowerHistory(View view, int adapterPosition) {

        String followerName;
        followerName = getFollowersDataList.get(adapterPosition).getFirstName() + " " + getFollowersDataList.get(adapterPosition).getLastName();


        if (((CheckBox) view).isChecked()) {

            if (cohostNameIdListd.containsValue(Integer.valueOf(getFollowersDataList.get(adapterPosition).getFollowerId()))) {

                return;

            } else {

                cohostNameIdListd.put(Integer.valueOf(getFollowersDataList.get(adapterPosition).getFollowerId()), followerName);

            }


        } else {

            cohostNameIdListd.remove(followerName);

        }

    }

    private void fetchStores(String businessName) {


        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        latLngString = String.valueOf(latitude) + "," + String.valueOf(longitude);

        Log.e("FetchStores:", "businessName->" + businessName);
        Log.e("FetchStores:", "latLngString->" + latLngString);


        Call<RestaurantDetails> call = apiService.searchRestaurants(businessName, latLngString, "20000", "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<RestaurantDetails>() {
            @Override
            public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                RestaurantDetails restaurantDetails = response.body();


                if (response.isSuccessful()) {


                    Log.e("restaurantOutputPojo", "" + response.toString());

                    if (restaurantDetails.getStatus().equalsIgnoreCase("OK")) {


                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();

                        Log.e("hotel_size", "" + restaurantDetails.getGetRestaurantsResults().size());

                        for (int i = 0; i < restaurantDetails.getGetRestaurantsResults().size(); i++) {

                            String placeid = restaurantDetails.getGetRestaurantsResults().get(i).getPlaceId();
                            String Icon = restaurantDetails.getGetRestaurantsResults().get(i).getIcon();
                            String hotel_name = restaurantDetails.getGetRestaurantsResults().get(i).getName();
                            String Id = restaurantDetails.getGetRestaurantsResults().get(i).getId();
                            String reference = restaurantDetails.getGetRestaurantsResults().get(i).getReference();
                            String address = restaurantDetails.getGetRestaurantsResults().get(i).getFormattedAddress();
                            String rating = restaurantDetails.getGetRestaurantsResults().get(i).getRating();

                            try {

                                String photo_ref = restaurantDetails.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();
                                int width = restaurantDetails.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                int height = restaurantDetails.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();

                                RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
//                                Log.e("height1", "photo_ref1: " + photo_ref);
//                                Log.e("height1", "width1: " + width);
//                                Log.e("height1", "height1: " + height);
                                photos.add(restaurantPhoto);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            }


                            for (int kk = 0; kk < restaurantDetails.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantDetails.getGetRestaurantsResults().get(i).getTypes().toString());
                            }


                            try {
                                boolean open_status = restaurantDetails.getGetRestaurantsResults().get(i).getOpeningHours().getOpenNow();
                                Log.e("open_status", "open_status: " + open_status);

                                openingHoursPojo = new OpeningHours(open_status, weekdayText);
                                openingHoursPojoList.add(openingHoursPojo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                    hotel_name, openingHoursPojo, photos, placeid, rating, reference, types, address);
                            getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);

                            EditRestaurantSearchAdapter editRestaurantSearchAdapter = new EditRestaurantSearchAdapter(EditHistoryMap.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(EditHistoryMap.this, LinearLayoutManager.HORIZONTAL, true);
                            hotel_historical_recylerview.setLayoutManager(mLayoutManager);
                            mLayoutManager.setStackFromEnd(true);
                            mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                            hotel_historical_recylerview.setAdapter(editRestaurantSearchAdapter);
                            hotel_historical_recylerview.setItemAnimator(new DefaultItemAnimator());
                            hotel_historical_recylerview.setNestedScrollingEnabled(false);
                            hotel_historical_recylerview.setHorizontalScrollBarEnabled(true);
                            editRestaurantSearchAdapter.notifyDataSetChanged();


                        }

                    } else {
                        Toast.makeText(EditHistoryMap.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else if (!response.isSuccessful()) {
                    Toast.makeText(EditHistoryMap.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });


    }

    private void nearByRestaurants(String searchtext) {

        progress_timeline.setVisibility(View.VISIBLE);

          latitude = gpsTracker.getLatitude();
          longitude = gpsTracker.getLongitude();

        latLngString = String.valueOf(latitude) + "," + String.valueOf(longitude);


//        Call<GetNearByRestaurantOutputPojo> call = apiService.nearByRestaurants("restaurant", latLngString, "500", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        Call<RestaurantDetails> call = apiService.searchRestaurantsReviewWithnearby(searchtext, latLngString, "500", "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<RestaurantDetails>() {
            @Override
            public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                RestaurantDetails restaurantOutputPojo = response.body();


                if (response.isSuccessful()) {

                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {


                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();


                        Log.e("hotel_size", "" + restaurantOutputPojo.getGetRestaurantsResults().size());

                        for (int i = 0; i < restaurantOutputPojo.getGetRestaurantsResults().size(); i++) {


                            String placeid = restaurantOutputPojo.getGetRestaurantsResults().get(i).getPlaceId();
                            String Icon = restaurantOutputPojo.getGetRestaurantsResults().get(i).getIcon();
                            String hotel_name = restaurantOutputPojo.getGetRestaurantsResults().get(i).getName();
                            String Id = restaurantOutputPojo.getGetRestaurantsResults().get(i).getId();

                            String reference = restaurantOutputPojo.getGetRestaurantsResults().get(i).getReference();

                            String address = restaurantOutputPojo.getGetRestaurantsResults().get(i).getFormattedAddress();
//                            String scope = restaurantOutputPojo.getGetRestaurantsResults().get(i).getScope();
                            String rating = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRating();

                            try {

                                String photo_ref = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();
                                int width = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                int height = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();

                                RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
//                                Log.e("PhotosCount", "Photos->: " + photo_ref.substring(0, 20) + " " + i);
//                                Log.e("height1", "width1: " + width);
//                                Log.e("height1", "height1: " + height);
                                photos.add(restaurantPhoto);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            for (int kk = 0; kk < restaurantOutputPojo.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantOutputPojo.getGetRestaurantsResults().get(i).getTypes().toString());
                            }


                            GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                    hotel_name, openingHoursPojo, photos, placeid, rating, reference, types, address);
                            getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);

                            progress_timeline.setVisibility(View.GONE);


                            EditRestaurantSearchAdapter editRestaurantSearchAdapter = new EditRestaurantSearchAdapter(EditHistoryMap.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(EditHistoryMap.this, LinearLayoutManager.HORIZONTAL, true);
                            hotel_historical_recylerview.setLayoutManager(mLayoutManager);
                            mLayoutManager.setStackFromEnd(false);
                            mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                            hotel_historical_recylerview.setAdapter(editRestaurantSearchAdapter);
                            hotel_historical_recylerview.setItemAnimator(new DefaultItemAnimator());
                            hotel_historical_recylerview.setNestedScrollingEnabled(false);
                            hotel_historical_recylerview.setHorizontalScrollBarEnabled(true);
                            editRestaurantSearchAdapter.notifyDataSetChanged();


                        }

                    } else {
                        Toast.makeText(EditHistoryMap.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else if (!response.isSuccessful()) {
                    Toast.makeText(EditHistoryMap.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
                progress_timeline.setVisibility(View.GONE);
            }
        });


    }

    private void nearByRestaurantsStaticLatlong(double latitude, double longitude) {

        progress_timeline.setVisibility(View.VISIBLE);

        latLngString = String.valueOf(latitude) + "," + String.valueOf(longitude);


//        Call<GetNearByRestaurantOutputPojo> call = apiService.nearByRestaurants("restaurant", latLngString, "500", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        Call<RestaurantDetails> call = apiService.searchRestaurantsReviewWithnearby("", latLngString, "500", "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<RestaurantDetails>() {
            @Override
            public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                RestaurantDetails restaurantOutputPojo = response.body();


                if (response.isSuccessful()) {

                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {


                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();


                        Log.e("hotel_size", "" + restaurantOutputPojo.getGetRestaurantsResults().size());

                        for (int i = 0; i < restaurantOutputPojo.getGetRestaurantsResults().size(); i++) {


                            String placeid = restaurantOutputPojo.getGetRestaurantsResults().get(i).getPlaceId();
                            String Icon = restaurantOutputPojo.getGetRestaurantsResults().get(i).getIcon();
                            String hotel_name = restaurantOutputPojo.getGetRestaurantsResults().get(i).getName();
                            String Id = restaurantOutputPojo.getGetRestaurantsResults().get(i).getId();

                            String reference = restaurantOutputPojo.getGetRestaurantsResults().get(i).getReference();

                            String address = restaurantOutputPojo.getGetRestaurantsResults().get(i).getFormattedAddress();
//                            String scope = restaurantOutputPojo.getGetRestaurantsResults().get(i).getScope();
                            String rating = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRating();

                            try {

                                String photo_ref = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();
                                int width = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                int height = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();

                                RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
//                                Log.e("PhotosCount", "Photos->: " + photo_ref.substring(0, 20) + " " + i);
//                                Log.e("height1", "width1: " + width);
//                                Log.e("height1", "height1: " + height);
                                photos.add(restaurantPhoto);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            for (int kk = 0; kk < restaurantOutputPojo.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantOutputPojo.getGetRestaurantsResults().get(i).getTypes().toString());
                            }


                            GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                    hotel_name, openingHoursPojo, photos, placeid, rating, reference, types, address);
                            getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);

                            progress_timeline.setVisibility(View.GONE);


                            EditRestaurantSearchAdapter editRestaurantSearchAdapter = new EditRestaurantSearchAdapter(EditHistoryMap.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(EditHistoryMap.this, LinearLayoutManager.HORIZONTAL, true);
                            hotel_historical_recylerview.setLayoutManager(mLayoutManager);
                            mLayoutManager.setStackFromEnd(false);
                            mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                            hotel_historical_recylerview.setAdapter(editRestaurantSearchAdapter);
                            hotel_historical_recylerview.setItemAnimator(new DefaultItemAnimator());
                            hotel_historical_recylerview.setNestedScrollingEnabled(false);
                            hotel_historical_recylerview.setHorizontalScrollBarEnabled(true);
                            editRestaurantSearchAdapter.notifyDataSetChanged();


                        }

                    } else {
                        Toast.makeText(EditHistoryMap.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else if (!response.isSuccessful()) {
                    Toast.makeText(EditHistoryMap.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
                progress_timeline.setVisibility(View.GONE);
            }
        });


    }



    private void getFollowers(final int userId) {

        if (isConnected(EditHistoryMap.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<GetFollowersOutput> call = apiService.getFollower("get_follower", userId);

                call.enqueue(new Callback<GetFollowersOutput>() {
                    @Override
                    public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                userid = response.body().getData().get(i).getUserid();
                                followerId = response.body().getData().get(i).getFollowerId();
                                firstName = response.body().getData().get(i).getFirstName();
                                lastName = response.body().getData().get(i).getLastName();
                                userPhoto = response.body().getData().get(i).getPicture();

                                if (!followerId.contains(String.valueOf(userid))) {

                                    GetFollowersData getFollowersData = new GetFollowersData(userid, followerId, firstName, lastName, userPhoto);
                                    getFollowersDataList.add(getFollowersData);
                                }


                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {


            case R.id.add_image_post:


                break;

            case R.id.add_people:


                break;

            case R.id.rl_create_post:


                break;

            case R.id.close:


                finish();

                break;


            case R.id.edit_restaurants:

                ll_search_restaurant.setVisibility(View.VISIBLE);
                ll_history_fields.setVisibility(View.GONE);
                rl_add_img_tag.setVisibility(View.GONE);

                break;

            case R.id.restaurantName:

                ll_search_restaurant.setVisibility(View.VISIBLE);
                rl_add_img_tag.setVisibility(View.GONE);
                ll_history_fields.setVisibility(View.GONE);
                break;


            case R.id.ll_add_photo:
                Integer[] friendId = cohostNameIdListd.keySet().toArray(new Integer[cohostNameIdListd.keySet().size()]);
                friendsList = new ArrayList<Integer>(Arrays.asList(friendId));
                selectImage();

                break;

            case R.id.imgbtn_gallery:
                Integer[] friendId2 = cohostNameIdListd.keySet().toArray(new Integer[cohostNameIdListd.keySet().size()]);
                friendsList = new ArrayList<Integer>(Arrays.asList(friendId2));
                selectImage();

                break;


            case R.id.imgbtn_tagpeople:


                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EditHistoryMap.this);
                LayoutInflater inflater1 = LayoutInflater.from(EditHistoryMap.this);
                @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.activity_select_followers, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setTitle("");

                follwersRecylerview = (RecyclerView) dialogView.findViewById(R.id.user_followers_recylerview);
                selectActionBar = (Toolbar) dialogView.findViewById(R.id.action_bar_select_follower);
                selectClose = (ImageButton) selectActionBar.findViewById(R.id.close);
                selectDone = (ImageButton) selectActionBar.findViewById(R.id.tv_done);
                empty_list = (RelativeLayout) dialogView.findViewById(R.id.empty_list);
                search_followers = (SearchView) dialogView.findViewById(R.id.search_followers);

                search_followers.setQueryHint("Search");

                search_followers.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted
                        editHistoricalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        // filter recycler view when text is changed
                        editHistoricalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }
                });


                if (getFollowersDataList.isEmpty()) {

                    empty_list.setVisibility(View.VISIBLE);
                    follwersRecylerview.setVisibility(View.GONE);

                } else {

                    empty_list.setVisibility(View.GONE);
                    follwersRecylerview.setVisibility(View.VISIBLE);
                }


                editHistoricalTagPeopleAdapter = new EditHistoricalTagPeopleAdapter(EditHistoryMap.this, getFollowersDataList, cohostNameIdListd, this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(EditHistoryMap.this);
                follwersRecylerview.setLayoutManager(mLayoutManager);
                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                follwersRecylerview.setAdapter(editHistoricalTagPeopleAdapter);
                follwersRecylerview.hasFixedSize();
                editHistoricalTagPeopleAdapter.notifyDataSetChanged();

                final AlertDialog dialog = dialogBuilder.create();
                dialog.show();

                selectClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        /*Log.e(TAG, "onClick: "+firstTimeTagged);
                        if(!firstTimeTagged){
                            cohostNameIdListd.clear();
                            getFollowersDataList.clear();
                            getFollowers(userid);

                        }*/

                        Log.e(TAG, "onClick: " + cohostNameIdListd.toString());

                        dialog.dismiss();

                    }
                });

                selectDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


//                         Toast.makeText(EditHistoryMap.this, ""+cohostNameIdListd.values().size(), Toast.LENGTH_SHORT).show();


                        if (cohostNameIdListd.values().size() == 0) {

                            ll_tag_people.setVisibility(View.VISIBLE);
                            friendsListDisplayTag.setText(R.string.tag_people);
                            friendsList.clear();


                        } else {

//                            firstTimeTagged = true;
                            ll_tag_people.setVisibility(View.VISIBLE);
                            String[] friends = cohostNameIdListd.values().toArray(new String[cohostNameIdListd.values().size()]);

                            switch (cohostNameIdListd.values().size()) {

                                case 1:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1]);

                                    break;

                                case 2:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + friends[cohostNameIdListd.values().size() - 2]);

                                    break;
                            }

                            if (cohostNameIdListd.values().size() > 2) {

                                friendsListDisplayTag.setText("With - ");
                                friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + String.valueOf(cohostNameIdListd.values().size() - 1) + " Others");

                            }


                            Integer[] friendId = cohostNameIdListd.keySet().toArray(new Integer[cohostNameIdListd.keySet().size()]);
                            friendsList = new ArrayList<Integer>(Arrays.asList(friendId));

                        }

                        dialog.dismiss();

                    }
                });


                break;


            case R.id.friendsListDisplayTag:


                final AlertDialog.Builder dialogBuilder2 = new AlertDialog.Builder(EditHistoryMap.this);
                LayoutInflater inflater12 = LayoutInflater.from(EditHistoryMap.this);
                @SuppressLint("InflateParams") final View dialogView2 = inflater12.inflate(R.layout.activity_select_followers, null);
                dialogBuilder2.setView(dialogView2);
                dialogBuilder2.setTitle("");

                follwersRecylerview = (RecyclerView) dialogView2.findViewById(R.id.user_followers_recylerview);
                selectActionBar = (Toolbar) dialogView2.findViewById(R.id.action_bar_select_follower);
                selectClose = (ImageButton) selectActionBar.findViewById(R.id.close);
                selectDone = (ImageButton) selectActionBar.findViewById(R.id.tv_done);
                empty_list = (RelativeLayout) dialogView2.findViewById(R.id.empty_list);
                search_followers = (SearchView) dialogView2.findViewById(R.id.search_followers);

                search_followers.setQueryHint("Search");

                search_followers.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted
                        editHistoricalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        // filter recycler view when text is changed
                        editHistoricalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }
                });


                if (getFollowersDataList.isEmpty()) {

                    empty_list.setVisibility(View.VISIBLE);
                    follwersRecylerview.setVisibility(View.GONE);

                } else {

                    empty_list.setVisibility(View.GONE);
                    follwersRecylerview.setVisibility(View.VISIBLE);
                }


                editHistoricalTagPeopleAdapter = new EditHistoricalTagPeopleAdapter(EditHistoryMap.this, getFollowersDataList, cohostNameIdListd, this);
                RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(EditHistoryMap.this);
                follwersRecylerview.setLayoutManager(mLayoutManager2);
                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                follwersRecylerview.setAdapter(editHistoricalTagPeopleAdapter);
                follwersRecylerview.hasFixedSize();
                editHistoricalTagPeopleAdapter.notifyDataSetChanged();

                final AlertDialog dialog2 = dialogBuilder2.create();
                dialog2.show();

                selectClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        /*Log.e(TAG, "onClick: "+firstTimeTagged);
                        if(!firstTimeTagged){
                            cohostNameIdListd.clear();
                            getFollowersDataList.clear();
                            getFollowers(userid);

                        }*/

                        Log.e(TAG, "onClick: " + cohostNameIdListd.toString());

                        dialog2.dismiss();

                    }
                });

                selectDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


//                         Toast.makeText(EditHistoryMap.this, ""+cohostNameIdListd.values().size(), Toast.LENGTH_SHORT).show();


                        if (cohostNameIdListd.values().size() == 0) {

                            ll_tag_people.setVisibility(View.VISIBLE);
                            friendsListDisplayTag.setText(R.string.tag_people);
                            friendsList.clear();


                        } else {

//                            firstTimeTagged = true;
                            ll_tag_people.setVisibility(View.VISIBLE);
                            String[] friends = cohostNameIdListd.values().toArray(new String[cohostNameIdListd.values().size()]);

                            switch (cohostNameIdListd.values().size()) {

                                case 1:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1]);

                                    break;

                                case 2:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + friends[cohostNameIdListd.values().size() - 2]);

                                    break;
                            }

                            if (cohostNameIdListd.values().size() > 2) {

                                friendsListDisplayTag.setText("With - ");
                                friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + String.valueOf(cohostNameIdListd.values().size() - 1) + " Others");

                            }


                            Integer[] friendId = cohostNameIdListd.keySet().toArray(new Integer[cohostNameIdListd.keySet().size()]);
                            friendsList = new ArrayList<Integer>(Arrays.asList(friendId));

                        }

                        dialog2.dismiss();

                    }
                });


                break;

            case R.id.ic_add_people:


                final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(EditHistoryMap.this);
                LayoutInflater inflater2 = LayoutInflater.from(EditHistoryMap.this);
                @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.activity_select_followers, null);
                dialogBuilder1.setView(dialogView1);
                dialogBuilder1.setTitle("");

                follwersRecylerview = (RecyclerView) dialogView1.findViewById(R.id.user_followers_recylerview);
                selectActionBar = (Toolbar) dialogView1.findViewById(R.id.action_bar_select_follower);
                selectClose = (ImageButton) selectActionBar.findViewById(R.id.close);
                selectDone = (ImageButton) selectActionBar.findViewById(R.id.tv_done);
                empty_list = (RelativeLayout) dialogView1.findViewById(R.id.empty_list);
                search_followers = (SearchView) dialogView1.findViewById(R.id.search_followers);

                search_followers.setQueryHint("Search");

                search_followers.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted
                        editHistoricalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        // filter recycler view when text is changed
                        editHistoricalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }
                });


                if (getFollowersDataList.isEmpty()) {

                    empty_list.setVisibility(View.VISIBLE);
                    follwersRecylerview.setVisibility(View.GONE);

                } else {

                    empty_list.setVisibility(View.GONE);
                    follwersRecylerview.setVisibility(View.VISIBLE);
                }


                editHistoricalTagPeopleAdapter = new EditHistoricalTagPeopleAdapter(EditHistoryMap.this, getFollowersDataList, cohostNameIdListd, this);
                RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(EditHistoryMap.this);
                follwersRecylerview.setLayoutManager(mLayoutManager1);
                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                follwersRecylerview.setAdapter(editHistoricalTagPeopleAdapter);
                follwersRecylerview.hasFixedSize();
                editHistoricalTagPeopleAdapter.notifyDataSetChanged();

                final AlertDialog dialog1 = dialogBuilder1.create();
                dialog1.show();

                selectClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                       /* Log.e(TAG, "onClick: "+firstTimeTagged);
                        if(!firstTimeTagged){
                            cohostNameIdListd.clear();
                            getFollowersDataList.clear();
                            getFollowers(userid);

                        }*/

                        Log.e(TAG, "onClick: " + cohostNameIdListd.toString());

                        dialog1.dismiss();

                    }
                });

                selectDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        // Toast.makeText(CreateHistoricalMap.this, ""+cohostNameIdListd.values().size(), Toast.LENGTH_SHORT).show();


                        if (cohostNameIdListd.values().size() == 0) {

                            ll_tag_people.setVisibility(View.VISIBLE);
                            friendsListDisplayTag.setText(R.string.tag_people);
                            friendsList.clear();


                        } else {

                            /*firstTimeTagged = true;*/
                            ll_tag_people.setVisibility(View.VISIBLE);
                            String[] friends = cohostNameIdListd.values().toArray(new String[cohostNameIdListd.values().size()]);

                            switch (cohostNameIdListd.values().size()) {

                                case 1:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1]);

                                    break;

                                case 2:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + friends[cohostNameIdListd.values().size() - 2]);

                                    break;
                            }

                            if (cohostNameIdListd.values().size() > 2) {

                                friendsListDisplayTag.setText("With - ");
                                friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + String.valueOf(cohostNameIdListd.values().size() - 1) + " Others");

                            }


                            Integer[] friendId = cohostNameIdListd.keySet().toArray(new Integer[cohostNameIdListd.keySet().size()]);
                            friendsList = new ArrayList<Integer>(Arrays.asList(friendId));

                        }

                        dialog1.dismiss();

                    }
                });

                break;


            case R.id.tv_create_history:


                if (isConnected(EditHistoryMap.this)) {

                    if (timelineId != null) {

                        if (imagesEncodedList.size() != 0) {

                            final AlertDialog alertDialog = new SpotsDialog.Builder().setContext(EditHistoryMap.this).setMessage("").build();

                            alertDialog.show();

                            Log.e(TAG, "onClick:friendlist "+friendsList.toString() );

                            Log.e(TAG, "onClick:friendlist_size"+friendsList.size() );


                            if (friendsList.isEmpty() && cohostNameIdListd.keySet().size() == 0) {

                                tag_status = 0;

                            } else {

                                tag_status = 1;
                            }

//                        editPostDialog = new SpotsDialog.Builder().setContext(EditHistoryMap.this).setMessage("").build();
//                        editPostDialog.show();

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            userid = Integer.parseInt(Pref_storage.getDetail(EditHistoryMap.this, "userId"));


                            try {

                                if (google_id == null) {
                                    google_id = "0";
                                }
                                if (google_photo == null) {
                                    google_photo = "0";
                                }
                                if (place_id == null) {
                                    place_id = "0";
                                }
                                String friendlist=new Gson().toJson(friendsList);

                                Call<CommonOutputPojo> call = apiService.createTimeLinePost("create_timeline",
                                        Integer.parseInt(timelineId),
                                        google_id,
                                        google_photo,
                                        hotel_name,
                                        place_id,
                                        "",
                                        org.apache.commons.text.StringEscapeUtils.escapeJava(description_of_visit.getText().toString()),
                                        tag_status,
                                        hotel_address,
                                        "",
                                        friendlist,
                                        latitude,
                                        longitude,
                                        userid);

                                call.enqueue(new Callback<CommonOutputPojo>() {
                                    @Override
                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                        if (response.body() != null) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("success")) {

                                                //Success

                                                sendHistoryImages(response.body().getData());

                                                if (deleteImageId.size() > 0) {

                                                    create_delete_timeline_single_image();

                                                }

                                            }


                                        } else {

                                            Toast.makeText(EditHistoryMap.this, "Something went wrong please try again", Toast.LENGTH_SHORT).show();
//                                        editPostDialog.dismiss();

                                        }


                                    }

                                    @Override
                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "" + t.getMessage());
//                                    editPostDialog.dismiss();

                                    }
                                });

                            } catch (Exception e) {

                                alertDialog.dismiss();
                                e.printStackTrace();
//                            editPostDialog.dismiss();


                            }


                        } else {

                            Toast.makeText(EditHistoryMap.this, "Please choose atleast one photo", Toast.LENGTH_SHORT).show();

                        }

                    }


                } else {


                    Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();

                }


                break;

            case R.id.date_of_restaurent_visit:

                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(EditHistoryMap.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String frmdate;

                                if ((monthOfYear + 1) > 9) {

                                    frmdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                } else {

                                    frmdate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;

                                }


                                date_of_restaurent_visit.setText(frmdate);
                                date_of_restaurent_visit.setError(null);


                            }

                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

                break;

        }

    }


    @Override
    public void onContactSelected(View view, GetFollowersData contact, int position) {

        String followerName;
        followerName = contact.getFirstName() + " " + contact.getLastName();

//        Toast.makeText(this, ""+position, Toast.LENGTH_SHORT).show();

        if (position == 1) {

            Log.e(TAG, "onContactSelected: " + position);
            if (cohostNameIdListd.containsKey(Integer.valueOf(contact.getFollowerId()))) {

                return;

            } else {

                cohostNameIdListd.put(Integer.valueOf(contact.getFollowerId()), followerName);

            }


        } else if (position == 0) {

            Log.e(TAG, "onContactSelected: " + position);
            cohostNameIdListd.remove(Integer.valueOf(contact.getFollowerId()));

        }

        Log.e(TAG, "onContactSelected: " + cohostNameIdListd.toString());


    }

    private void get_timeline_single() {

        if (isConnected(this)) {


            if (timelineId != null) {

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                userid = Integer.parseInt(Pref_storage.getDetail(EditHistoryMap.this, "userId"));

                Call<profile_timeline_output_response> call = apiService.get_timeline_single("get_timeline_single",
                        Integer.parseInt(timelineId),
                        userid);

                call.enqueue(new Callback<profile_timeline_output_response>() {
                    @Override
                    public void onResponse(Call<profile_timeline_output_response> call, Response<profile_timeline_output_response> response) {

                        if (response.isSuccessful()) {


                            for (int j = 0; j < response.body().getData().size(); j++) {


                                timelineId = response.body().getData().get(j).getTimelineId();
                                hotel_name = response.body().getData().get(j).getHotel();
                                hotel_address = response.body().getData().get(j).getAddress();
                                timelineDescription = response.body().getData().get(j).getTimelineDescription().replace("\"", "");
                                firstName = response.body().getData().get(j).getFirstName();
                                lastName = response.body().getData().get(j).getLastName();
                                whom = response.body().getData().get(j).getWhom();
                                whomCount = response.body().getData().get(j).getWhomCount();
                                image = response.body().getData().get(j).getImage();
                                imageCount = response.body().getData().get(j).getImageCount();


                            }

                            restaurantName.setText(hotel_name);

                            if (timelineDescription.equals("0") || timelineDescription == null) {

                                description_of_visit.setText("");

                            } else {

                                description_of_visit.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(timelineDescription));

                            }


                            if (imageCount != 0) {

                                Log.e("CheckImage", "CheckImage->" + image.size());

                                for (int k = 0; k < image.size(); k++) {

                                    Image image1 = new Image(image.get(k).getImg(), image.get(k).getImg_id());
                                    image1.setLocalImage(false);
                                    imagesEncodedList.add(image1);

                                }

                                init();
                            }


                            if (whomCount != 0) {

                                ll_tag_people.setVisibility(View.VISIBLE);

                                switch (whom.size()) {

                                    case 1:

                                        String friendName = whom.get(0).getFirstName() + " " + whom.get(0).getLastName();
                                        friendsListDisplayTag.setText("With - ");
                                        friendsListDisplayTag.append(friendName);

                                        break;

                                    case 2:

                                        String friendName1 = whom.get(0).getFirstName() + " " + whom.get(0).getLastName();
                                        String friendName2 = whom.get(1).getFirstName() + " " + whom.get(1).getLastName();
                                        String totalFriends = friendName1 + " and " + friendName2;
                                        friendsListDisplayTag.setText("With - ");
                                        friendsListDisplayTag.append(totalFriends);
                                        break;
                                }

                                if (whom.size() > 2) {

                                    String friendName1 = whom.get(0).getFirstName() + " " + whom.get(0).getLastName();
                                    String friendName2 = " and " + String.valueOf(whom.size() - 1) + " Others";
                                    String totalFriends = friendName1 + "" + friendName2;
                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(totalFriends);

                                }


                                for (int i = 0; i < getFollowersDataList.size(); i++) {

                                    for (int j = 0; j < whom.size(); j++) {

                                        if (getFollowersDataList.get(i).getFollowerId().equals(whom.get(j).getUserId())) {

                                            String followerName;
                                            followerName = getFollowersDataList.get(i).getFirstName() + " " + getFollowersDataList.get(i).getLastName();
                                            cohostNameIdListd.put(Integer.valueOf(getFollowersDataList.get(i).getFollowerId()), followerName);
                                            getFollowersDataList.get(i).setSelected(true);

                                        }

                                    }


                                }


                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<profile_timeline_output_response> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());

                    }
                });
            }


        } else {


        }

    }

    private void sendHistoryImages(int hisId) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        userid = Integer.parseInt(Pref_storage.getDetail(EditHistoryMap.this, "userId"));

        for (int i = 0; i < imagesEncodedList.size(); i++) {


            if (imagesEncodedList.get(i).isLocalImage()) {

                String image = imagesEncodedList.get(i).getImg();

                Log.e(TAG, "sendHistoryImages: true" + image);

                try {

                    file = new File(image);

                    File compressedImageFile = null;
                    compressedImageFile = new Compressor(this).setQuality(100).compressToFile(file);

                    ProgressRequestBody fileBody = new ProgressRequestBody(compressedImageFile, this);
                    MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), fileBody);

                    Call<CommonOutputPojo> request = apiService.createHistoricalImage("create_timeline_image", hisId, userid, filePart);

                    final int finalI = i;
                    request.enqueue(new Callback<CommonOutputPojo>() {
                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {
                            Log.e(TAG, "onResponse: Images");
                            count++;

                            if (finalI == imagesEncodedList.size() - 1) {

                                Log.e(TAG, "onResponse: Image uploaded successfully");
//                        editPostDialog.dismiss();
                                Intent intent = new Intent(EditHistoryMap.this, Home.class);
                                startActivity(intent);
                                finish();


                            }

                        }

                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {

                            Log.e(TAG, "onFailure: Images->" + t.getLocalizedMessage());

                            if (finalI == imagesEncodedList.size() - 1) {

                                Intent intent = new Intent(EditHistoryMap.this, Home.class);
                                startActivity(intent);
                                finish();

                            }


                        }
                    });


                } catch (IOException e) {
                    e.printStackTrace();


                    if (i == imagesEncodedList.size() - 1) {

                        Intent intent = new Intent(EditHistoryMap.this, Home.class);
                        startActivity(intent);
                        finish();

                    }


                }

            } else {


                String image = imagesEncodedList.get(i).getImg();

                Log.e(TAG, "sendHistoryImages: false " + image);

                if (i == imagesEncodedList.size() - 1) {

                    Intent intent = new Intent(EditHistoryMap.this, Home.class);
                    startActivity(intent);
                    finish();

                }

            }


        }


    }

    private void selectImage() {

        validate();
        List<String> listItems = new ArrayList<String>();

        listItems.add("Take Photo");
        listItems.add("Choose from library");
        listItems.add("Cancel");

        if (image_status == 1) {

            listItems.remove(2);
            listItems.add("Remove all photos");
            listItems.add("Cancel");

        }

        final CharSequence[] items = listItems.toArray(new CharSequence[listItems.size()]);

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(EditHistoryMap.this);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(EditHistoryMap.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from library")) {
                    userChoosenTask = "Choose from library";
                    if (result) {

                        imagepath = null;
                        galleryIntent();

                    }


                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                } else if (items[item].equals("Remove all photos")) {

                    imagepath = null;
                    imagesEncodedList.clear();
                    init();
                    ll_history_image_section.setVisibility(View.GONE);
                    tv_add_photo.setText(R.string.add_photos);

                }
            }
        });
        builder.show();
    }

    private void validate() {

        if (imagepath == null && imagesEncodedList.isEmpty()) {

            image_status = 0;

        } else if (!imagesEncodedList.isEmpty()) {

            image_status = 1;

        }

    }

    private void init() {

        if (imagesEncodedList.size() == 0) {

            ll_history_image_section.setVisibility(View.GONE);
            tv_add_photo.setText(R.string.add_photos);

        } else {

            ll_history_image_section.setVisibility(View.VISIBLE);
            tv_add_photo.setText("Edit photos");

            mviewpager.setAdapter(new HistoryImagesAdapter(EditHistoryMap.this, imagesEncodedList, this));
            circleIndicator.setViewPager(mviewpager);
            NUM_PAGES = imagesEncodedList.size();

            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    mviewpager.setCurrentItem(currentPage++, true);
                }
            };

            // Pager listener over indicator
            circleIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;

                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int pos) {

                }
            });
        }


    }

    @Override
    public void removedImageLisenter(List<Image> imagesList, int position) {


        if (imagesEncodedList.get(position).getImg_id() == null) {

//            create_delete_timeline_single_image(0, position);


        } else {

            deleteImageId.add(imagesEncodedList.get(position).getImg_id());
//            create_delete_timeline_single_image(Integer.parseInt(imagesEncodedList.get(position).getImg_id()), position);

        }

        imagesEncodedList.remove(position);
        init();


        Log.e(TAG, "removedImageLisenter: " + imagesEncodedList.size());


    }


    private void create_delete_timeline_single_image() {


        for (String imageId : deleteImageId) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            userid = Integer.parseInt(Pref_storage.getDetail(EditHistoryMap.this, "userId"));

            Call<CommonOutputPojo> request = apiService.create_delete_timeline_single_image("create_delete_timeline_single_image", Integer.parseInt(imageId), Integer.parseInt(timelineId), userid);

            request.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {
                    Log.e(TAG, "onResponse: Images");


                    if (response.body().getResponseMessage().equals("success")) {

//                    Toast.makeText(EditHistoryMap.this, "Photo removed", Toast.LENGTH_SHORT).show();

                    }


                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {

                    Log.e(TAG, "onFailure: Images->" + t.getLocalizedMessage());

                }
            });
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        Log.d("onActivityResult()", "resultCode" + Integer.toString(resultCode));
        Log.d("onActivityResult()", "requestCode" + Integer.toString(requestCode));


        switch (requestCode) {

            case 1:

                switch (resultCode) {

                    case Activity.RESULT_OK: {
                        // All required changes were successfully made
//                        Toast.makeText(getApplicationContext(), "Location enabled by user!", Toast.LENGTH_LONG).show();

                       /* if (gpsTracker.isGPSEnabled == false) {


                            gpsTracker = new GPSTracker(EditHistoryMap.this);
                            latitude = gpsTracker.getLatitude();
                            longitude = gpsTracker.getLongitude();

                            Log.v("isGPSEnabled", "latitude->" + latitude);
                            Log.v("isGPSEnabled", "longitude->" + longitude);


                        } else {


                        }
*/
                       nearByRestaurants("");
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
//                        Toast.makeText(getApplicationContext(), "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();


                       // finish();// Need to alert to enable location
                         ApiInterface apiInterface=ApiClient.getClient().create(ApiInterface.class);

                        Call<Get_HotelStaticLocation_Response> call = apiInterface.get_hotel_static_location("get_hotel_static_location", userid);
                        call.enqueue(new Callback<Get_HotelStaticLocation_Response>() {
                            @Override
                            public void onResponse(Call<Get_HotelStaticLocation_Response> call, Response<Get_HotelStaticLocation_Response> response) {


                                if (response.isSuccessful()) {


                                    double latitude = response.body().getData().get(0).getLatitude();
                                    double longitude = response.body().getData().get(0).getLongitude();

                                    nearByRestaurantsStaticLatlong(latitude, longitude);

                                } else {
                                    Toast.makeText(EditHistoryMap.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Get_HotelStaticLocation_Response> call, Throwable t) {
                                // Log error here since request failed
                                call.cancel();
                            }
                        });

                        break;
                    }
                    default: {
                        break;
                    }
                }

                break;

        }

        try {

            if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);

            }
            // When an Image is picked
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && null != data && !(data.toString().equals("Intent { (has extras) }"))) {
                // Get the Image from data

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                if (data.getData() != null) {


                    Uri mImageUri = data.getData();
                    Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                    cursor.moveToFirst();
                    String document_id = cursor.getString(0);
                    document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                    cursor.close();

                    cursor = getContentResolver().query(
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                    cursor.moveToFirst();
                    imageEncoded = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    cursor.close();

                    Image image = new Image(imageEncoded);
                    image.setLocalImage(true);
                    imagesEncodedList.add(image);
                    init();

                    Log.v("LOG_TAG", "Selected Images" + imageEncoded);

                } else {

                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);

                            // Get the cursor

                            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                            cursor.moveToFirst();
                            String document_id = cursor.getString(0);
                            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                            cursor.close();

                            cursor = getContentResolver().query(
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                            cursor.moveToFirst();
                            imageEncoded = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                            Image image = new Image(imageEncoded);
                            image.setLocalImage(true);
                            imagesEncodedList.add(image);
                            init();

                            cursor.close();


                            Log.v("LOG_TAG", "Selected Images" + imagesEncodedList);
                        }
                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }
                }
            } else {
                /* Toast.makeText(this, "You haven't picked Image",Toast.LENGTH_LONG).show();*/
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }


    }

    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = null;
        try {
            thumbnail = getThumbnail(imageUri);
            saveBitmap(thumbnail);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "CameraCaptureError", Toast.LENGTH_SHORT).show();
            Log.e("CameraCaptureError", "CameraCaptureError->" + e.getMessage());
        }


    }

    private void saveBitmap(Bitmap bm) {


        String folder_main = "FoodWall";

        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }

        File newFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        Log.e("ImageFile", "ImageFile->" + newFile);

        try {

            FileOutputStream fileOutputStream = new FileOutputStream(newFile);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            imagepath = String.valueOf(newFile);
            Image image = new Image(imagepath);
            image.setLocalImage(true);
            imagesEncodedList.add(image);
            init();
            Log.e("MyPath", "MyImagePath->" + imagepath);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void cameraIntent() {

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        } else {

            /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);*/

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, REQUEST_CAMERA);

        }

    }

    public Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException {
        InputStream input = EditHistoryMap.this.getContentResolver().openInputStream(uri);
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 800) ? (originalSize / 800) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }

    private void galleryIntent() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_GALLERY_PERMISSION);
        } else {

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);

        }

    }

    /* Check Location Permission for Marshmallow Devices */
    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(EditHistoryMap.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else
                showSettingDialog();
        } else
            showSettingDialog();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        if (requestCode == ACCESS_FINE_LOCATION_INTENT_ID) {
            // do something
            showSettingDialog();

        } else if (requestCode == REQUEST_GALLERY_PERMISSION) {
            // do something else
            galleryIntent();
        } else if (requestCode == REQUEST_CAMERA_PERMISSION) {
            // do something else
            cameraIntent();
        }
    }

    private void showSettingDialog() {


        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.


                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(EditHistoryMap.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    /*  Show Popup to access User Permission  */
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(EditHistoryMap.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(EditHistoryMap.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(EditHistoryMap.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    //Run on UI
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            showSettingDialog();
        }
    };

    /* Broadcast receiver to check status of GPS */
    private BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //If Action is Location
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                //Check if GPS is turned ON or OFF
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Log.e("About GPS", "GPS is Enabled in your device");
                } else {
                    //If GPS turned OFF show Location Dialog
                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    // showSettingDialog();
                    Log.e("About GPS", "GPS is Disabled in your device");
                }

            }
        }
    };


    /* Initiate Google API Client  */
    private void initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(EditHistoryMap.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    public void onTagsChanged(Collection<String> tags) {

        /*if (tags.size() == 0) {

//            friendsListDisplay.setVisibility(View.GONE);

        } else {

//            friendsListDisplay.setVisibility(View.VISIBLE);

        }

        cohostList.clear();

        Log.d("FollowersId", "Tags changed: ");
        Log.d("FollowersId", Arrays.toString(tags.toArray()));

        Object[] ob = tags.toArray();

        for (Object value : ob) {

            System.out.println("Number = " + value);
            cohostList.add(cohostNameIdListd.get(value));

        }

        Iterator<String> iterator = cohostNameIdListd.keySet().iterator();
        while (iterator.hasNext()) {

            String certification = iterator.next();

            if (tags.contains(certification)) {

                Log.e("EventCheck", "EventCheck->" + certification);

            } else {

                Log.e("EventCheck", "Not Found");
                iterator.remove();

            }

        }*/

//        Toast.makeText(this, "" + cohostNameIdListd.values().toString(), Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onEditingFinished() {
        Log.d("FollowersId", "OnEditing finished");
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(friendsListDisplay.getWindowToken(), 0);
        friendsListDisplay.clearFocus();
    }


    @Override
    public void onProgressUpdate(int percentage) {
        Log.e(TAG, "onProgressUpdate: EditTimelinePost" + percentage);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }


}
