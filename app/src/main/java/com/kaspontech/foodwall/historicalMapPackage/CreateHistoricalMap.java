package com.kaspontech.foodwall.historicalMapPackage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.kaspontech.foodwall.adapters.HistoricalMap.HistoricalTagPeopleAdapter;
import com.kaspontech.foodwall.adapters.HistoricalMap.HistoryImagesAdapter;
import com.kaspontech.foodwall.adapters.HistoricalMap.RestaurantSearchHisMapAdapter;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersOutput;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.Geometry;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.GetRestaurantsResult;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.Get_HotelStaticLocation_Response;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.OpeningHours;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.RestaurantDetails;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.RestaurantPhoto;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.REST_API.API_interface_restaurant;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.REST_API.ProgressRequestBody;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Image;
import com.kaspontech.foodwall.reviewpackage.restaurantSearch.ReviewRestaurantSearch;
import com.kaspontech.foodwall.reviewpackage.restaurantSearch.ReviewRestaurantSearchAdapter;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;
import id.zelory.compressor.Compressor;
import mabbas007.tagsedittext.TagsEditText;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateHistoricalMap extends AppCompatActivity implements LocationListener,
        View.OnClickListener,
        TagsEditText.TagsEditListener,
        ProgressRequestBody.UploadCallbacks,
        HistoricalTagPeopleAdapter.ContactsAdapterListener,
        HistoryImagesAdapter.PhotoRemovedListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


    // Widgets

    private Toolbar actionbar, selectActionBar;

    private RecyclerView hotel_historical_recylerview, follwersRecylerview;

    private EditText edit_search_hotel, date_of_restaurent_visit, friendsListDisplayTag, description_of_visit;

    private ImageButton close, edit_restaurants, set_history_date, selectClose, selectDone;

    private TextView tv_createHistory, restaurantName;

    private LinearLayout ll_search_restaurant, ll_history_fields, ll_history_image_section, ll_tag_people, ll_not_found;

    private TagsEditText friendsListDisplay;

    private TextView tv_add_photo;

    private int image_status;

    private RelativeLayout rl_add_img_tag, rl_create_post, empty_list;

    // List

    private List<String> types;

    private List<Object> weekdayText;

    private List<RestaurantPhoto> photos;

    private List<String> htmlAttributions;

    private List<OpeningHours> openingHoursPojoList;

    private List<RestaurantDetails> restaurantOutputPojoList;

    private ArrayList<GetRestaurantsResult> getallRestaurantDetailsPojoArrayList = new ArrayList<>();

    private Geometry geometryPojo;

    private OpeningHours openingHoursPojo;


    // Google location search retrofit interface

    private API_interface_restaurant apiService;

    private SearchView search_followers;

    private ImageButton add_image_post, add_people, ic_add_people, ic_add_photo;

    private Button btn_create_post;

    private HistoricalTagPeopleAdapter historicalTagPeopleAdapter;

    private HashMap<Integer, String> cohostNameIdListd = new HashMap<>();

    private List<Integer> cohostList = new ArrayList<>();

    private List<Integer> friendsList = new ArrayList<>();

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    private ViewPager mviewpager;

    private CircleIndicator circleIndicator;

    private String userChoosenTask, imagepath, userImage;


    private static final int REQUEST_CAMERA_PERMISSION = 1;

    private static final int REQUEST_GALLERY_PERMISSION = 2;

    private static final int REQUEST_PERMISSION_SETTING = 5;


    private int PICK_IMAGE_MULTIPLE = 2;

    private String imageEncoded;

    private File file;

    private Location mylocation;

    public List<Image> imagesEncodedList = new ArrayList<>();

    private static int currentPage = 0;

    private static int NUM_PAGES = 0;

    // GPS
    private GPSTracker gpsTracker;

    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    private GoogleApiClient mGoogleApiClient, googleApiClient;

    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;

    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";

    final static int REQUEST_LOCATION = 199;

    //  Variables
    private double latitude, longitude;

    private Uri imageUri;

    private String latLngString, hotel_name, hotel_address, google_id, google_photo, place_id;

    private int eventId, userid, tag_status;

    private String followerId, firstName, lastName, userPhoto;

    private List<GetFollowersData> getFollowersDataList = new ArrayList<>();

    private List<GetFollowersData> tempFollowersDataList = new ArrayList<>();

    public static AlertDialog createPostDialog = null;

    private int count = 0;

    private boolean firstTimeTagged = false;

    private static final String TAG = "CreateHistoricalMap";


    private ApiInterface apiInterface;

    private AlertDialog dialog;

    private AlertDialog sweetDialog;


    ProgressBar progress_timeline;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_historical_map);

        userid = Integer.parseInt(Pref_storage.getDetail(CreateHistoricalMap.this, "userId"));

        setUpGClient();

        getFollowers(userid);

        //checkPermissions();//Check Location Permission

        initComponents();


        edit_search_hotel.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        edit_search_hotel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Your piece of code on keyboard search click

                    String getrestaurantName = edit_search_hotel.getText().toString();

                    int result = ContextCompat.checkSelfPermission(CreateHistoricalMap.this, Manifest.permission.ACCESS_FINE_LOCATION);

                    final String restaurantName = getrestaurantName.replace(" ", "+");

                    if (result == PackageManager.PERMISSION_GRANTED) {

                        if (gpsTracker.isGPSEnabled) {
                            /*Calling nearby restaurants API using edit text search results*/

                            //fetchStores(restaurantName);
                            nearByRestaurants(restaurantName);

                        } else {

                            nearByRestaurants(restaurantName);
                        }

                    } else if (result == PackageManager.PERMISSION_DENIED) {


                        nearByRestaurants(restaurantName);
                    }
                   /* String getrestaurantName = edit_search_hotel.getText().toString();

                    String restaurantName = getrestaurantName.replace(" ", "+");

                    // fetchStores(restaurantName);
                    nearByRestaurants(restaurantName);*/

                    return true;
                }
                return false;
            }
        });


        edit_search_hotel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                getallRestaurantDetailsPojoArrayList.clear();


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String getrestaurantName = edit_search_hotel.getText().toString();

                int result = ContextCompat.checkSelfPermission(CreateHistoricalMap.this, Manifest.permission.ACCESS_FINE_LOCATION);

                final String restaurantName = getrestaurantName.replace(" ", "+");

                if (result == PackageManager.PERMISSION_GRANTED) {

                    if (restaurantName.length() != 0) {
                        /*Calling nearby restaurants API using edit text search results*/

                        //fetchStores(restaurantName);
                        nearByRestaurants(restaurantName);

                    } else {

                        nearByRestaurants(restaurantName);
                    }

                } else if (result == PackageManager.PERMISSION_DENIED) {

                    nearByRestaurants(restaurantName);
                    //permissionDeniedSearch(restaurantName,userid);
                }
               /* String getrestaurantName = edit_search_hotel.getText().toString();

                String restaurantName = getrestaurantName.replace(" ", "+");

                if (restaurantName.length() != 0) {

                    // fetchStores(restaurantName);
                    nearByRestaurants(restaurantName);

                } else {


                }*/


            }
        });

    }

    private void permissionDeniedSearch(final String restaurantName, int userid) {

        Call<Get_HotelStaticLocation_Response> call = apiInterface.get_hotel_static_location("get_hotel_static_location", userid);
        call.enqueue(new Callback<Get_HotelStaticLocation_Response>() {
            @Override
            public void onResponse(Call<Get_HotelStaticLocation_Response> call, Response<Get_HotelStaticLocation_Response> response) {

                if (response.isSuccessful()) {

                    double latitude = response.body().getData().get(0).getLatitude();
                    double longitude = response.body().getData().get(0).getLongitude();
                    Log.e("View", "Latitude ---------> " + latitude);
                    Log.e("View", "Latitude ---------> " + longitude);
                    nearByRestaurants(restaurantName);

                } else {

                    Toast.makeText(CreateHistoricalMap.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Get_HotelStaticLocation_Response> call, Throwable t) {
                // Log error here since request failed
                call.cancel();

            }
        });
    }

    private void permissionCheckingCreateView() {


        if (Utility.isConnected(CreateHistoricalMap.this)) {

            getMyLocation();
            sweetDialog = new SpotsDialog.Builder().setContext(CreateHistoricalMap.this).setMessage("").build();
            sweetDialog.setCancelable(false);
            sweetDialog.show();
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        sleep(7000);
                        String rediusChecking = "500";
                        nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            thread.start();


        } else {

            Toast.makeText(CreateHistoricalMap.this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();

        }
    }

    private void initComponents() {
        gpsTracker = new GPSTracker(CreateHistoricalMap.this);
        apiService = APIClient_restaurant.getClient().create(API_interface_restaurant.class);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        actionbar = (Toolbar) findViewById(R.id.action_bar_create_historical_map);
        close = (ImageButton) actionbar.findViewById(R.id.close);
        edit_restaurants = (ImageButton) findViewById(R.id.edit_restaurants);
        set_history_date = (ImageButton) findViewById(R.id.set_history_date);
        add_image_post = (ImageButton) findViewById(R.id.add_image_post);
        add_people = (ImageButton) findViewById(R.id.add_people);
        ic_add_people = (ImageButton) findViewById(R.id.ic_add_people);
        ic_add_photo = (ImageButton) findViewById(R.id.ic_add_photo);
        tv_createHistory = (TextView) actionbar.findViewById(R.id.tv_create_history);
        tv_add_photo = (TextView) findViewById(R.id.tv_add_photo);
        ll_search_restaurant = (LinearLayout) findViewById(R.id.ll_search_restaurant);
        ll_history_fields = (LinearLayout) findViewById(R.id.ll_history_fields);
        ll_history_image_section = (LinearLayout) findViewById(R.id.ll_history_image_section);
        ll_not_found = (LinearLayout) findViewById(R.id.ll_not_found);
        ll_tag_people = (LinearLayout) findViewById(R.id.ll_tag_people);
        hotel_historical_recylerview = (RecyclerView) findViewById(R.id.hotel_historical_recylerview);
        edit_search_hotel = (EditText) findViewById(R.id.edit_search_hotel);
        date_of_restaurent_visit = (EditText) findViewById(R.id.date_of_restaurent_visit);
        description_of_visit = (EditText) findViewById(R.id.description_of_visit);
        friendsListDisplayTag = (EditText) findViewById(R.id.friendsListDisplayTag);
        restaurantName = (TextView) findViewById(R.id.restaurantName);
        friendsListDisplay = (TagsEditText) findViewById(R.id.friendsListDisplay);

        mviewpager = (ViewPager) findViewById(R.id.mviewpager_post);
        circleIndicator = (CircleIndicator) findViewById(R.id.indicator_post);
        rl_add_img_tag = (RelativeLayout) findViewById(R.id.rl_add_img_tag);
        rl_create_post = (RelativeLayout) findViewById(R.id.rl_create_post);
        progress_timeline = (ProgressBar) findViewById(R.id.progress_timeline);

        close.setOnClickListener(this);
        friendsListDisplay.setTagsListener(this);
        date_of_restaurent_visit.setOnClickListener(this);
        tv_createHistory.setOnClickListener(this);
        friendsListDisplay.setOnClickListener(this);
        edit_restaurants.setOnClickListener(this);
        restaurantName.setOnClickListener(this);
        set_history_date.setOnClickListener(this);
        friendsListDisplayTag.setOnClickListener(this);
        tv_add_photo.setOnClickListener(this);
        add_image_post.setOnClickListener(this);
        ic_add_people.setOnClickListener(this);
        ic_add_photo.setOnClickListener(this);
        add_people.setOnClickListener(this);
        rl_create_post.setOnClickListener(this);
    }

   /* @Override
    protected void onSwipeRight() {

    }

    @Override
    protected void onSwipeLeft() {
       *//* Intent intent = new Intent(this, Home.class);
        startActivity(intent);*//*

        finish();
        overridePendingTransition(R.anim.swipe_righttoleft, R.anim.swipe_lefttoright);
    }
*/

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
       /* Intent intent = new Intent(CreateHistoricalMap.this, HistoricalMap.class);
        startActivity(intent);
        finish();*/
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Confirm")
                .setContentText("Are you sure you want to skip")
                .setConfirmText("Yes")
                .setCancelText("No")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        if (googleApiClient != null) {
                            if (googleApiClient.isConnected()) {
                                googleApiClient.disconnect();
                            }
                        }

                        finish();
                        overridePendingTransition(R.anim.swipe_righttoleft, R.anim.swipe_lefttoright);

                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();

    }

    public void selectedRestaurantHandler(View view, int adapterPosition) {

        tv_createHistory.setVisibility(View.VISIBLE);
        google_id = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getId();
        google_photo = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getRestaurantPhotos().get(0).getPhotoReference();
        place_id = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getPlaceId();
        hotel_name = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getName();
        hotel_address = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getFormattedAddress();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(friendsListDisplay.getWindowToken(), 0);

        ll_search_restaurant.setVisibility(View.GONE);
        ll_history_fields.setVisibility(View.VISIBLE);
        restaurantName.setText(getallRestaurantDetailsPojoArrayList.get(adapterPosition).getName());

    }


    private void fetchStores(String businessName) {


        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        latLngString = String.valueOf(latitude) + "," + String.valueOf(longitude);

        Log.e("FetchStores:", "businessName->" + businessName);
        Log.e("FetchStores:", "latLngString->" + latLngString);


        Call<RestaurantDetails> call = apiService.searchRestaurants(businessName, latLngString, "20000", "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<RestaurantDetails>() {
            @Override
            public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                RestaurantDetails restaurantDetails = response.body();

                if (response.isSuccessful()) {


                    Log.e("restaurantOutputPojo", "" + response.toString());

                    if (restaurantDetails.getStatus().equalsIgnoreCase("OK")) {


                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();

                        Log.e("hotel_size", "" + response.body().getGetRestaurantsResults().size());

                        for (int i = 0; i < response.body().getGetRestaurantsResults().size(); i++) {

                            String placeid = response.body().getGetRestaurantsResults().get(i).getPlaceId();
                            String Icon = response.body().getGetRestaurantsResults().get(i).getIcon();
                            String hotel_name = response.body().getGetRestaurantsResults().get(i).getName();
                            String Id = response.body().getGetRestaurantsResults().get(i).getId();
                            String reference = response.body().getGetRestaurantsResults().get(i).getReference();
                            String address = response.body().getGetRestaurantsResults().get(i).getFormattedAddress();
                            String rating = response.body().getGetRestaurantsResults().get(i).getRating();

                            String photo_flag = "";

                            List<RestaurantPhoto> photosRestaurant = new ArrayList<>();

                            photosRestaurant = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos();

                            try {

                                String photo_ref = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();
                                int width = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                int height = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();
                                photo_flag = photo_ref;


                                if (photo_ref == null) {

                                } else {

                                    RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
                                    Log.e("height1", "photo_ref1: " + photo_ref);
                                    Log.e("height1", "width1: " + width);
                                    Log.e("height1", "height1: " + height);
                                    photos.add(restaurantPhoto);

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            for (int kk = 0; kk < restaurantDetails.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantDetails.getGetRestaurantsResults().get(i).getTypes().toString());
                            }


                            try {

                                boolean open_status = restaurantDetails.getGetRestaurantsResults().get(i).getOpeningHours().getOpenNow();
                                Log.e("open_status", "open_status: " + open_status);
                                openingHoursPojo = new OpeningHours(open_status, weekdayText);
                                openingHoursPojoList.add(openingHoursPojo);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            if (photosRestaurant != null) {

                                GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                        hotel_name, openingHoursPojo, photosRestaurant, placeid, rating, reference, types, address);
                                getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);

                            } else {


                            }
                        }

                        RestaurantSearchHisMapAdapter restaurantSearchHisMapAdapter = new RestaurantSearchHisMapAdapter(CreateHistoricalMap.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList);
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(CreateHistoricalMap.this,
                                LinearLayoutManager.HORIZONTAL, false);
                        hotel_historical_recylerview.setLayoutManager(mLayoutManager);
                        mLayoutManager.setStackFromEnd(true);
                        // mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                        hotel_historical_recylerview.setAdapter(restaurantSearchHisMapAdapter);
                        hotel_historical_recylerview.setItemAnimator(new DefaultItemAnimator());
                        hotel_historical_recylerview.setNestedScrollingEnabled(false);
                        hotel_historical_recylerview.setHorizontalScrollBarEnabled(true);
                        restaurantSearchHisMapAdapter.notifyDataSetChanged();
                        hotel_historical_recylerview.setVisibility(View.VISIBLE);
                        ll_not_found.setVisibility(View.GONE);


                    } else {

                        ll_not_found.setVisibility(View.VISIBLE);
                        hotel_historical_recylerview.setVisibility(View.GONE);

                    }

                } else {
                    Toast.makeText(CreateHistoricalMap.this, "Error " + response.code() + " found. Please try again later.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });


    }


//    public void noresults(int size){
//        if(size==1){
//            ll_not_found.setVisibility(View.VISIBLE);
//        }
//    }

    private void nearByRestaurants(String restaurantName) {


        progress_timeline.setVisibility(View.VISIBLE);
        //double latitude = gpsTracker.getLatitude();
        //double longitude = gpsTracker.getLongitude();

        int result = ContextCompat.checkSelfPermission(CreateHistoricalMap.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (result == PackageManager.PERMISSION_GRANTED) {

            latLngString = latitude + "," + longitude;

        } else {

            latitude = 13.0723153;
            longitude = 80.215563;
        }

        latLngString = latitude + "," + longitude;
        Call<RestaurantDetails> call = apiService.searchRestaurantsReviewWithnearby(restaurantName, latLngString, "500", "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<RestaurantDetails>() {
            @Override
            public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                RestaurantDetails restaurantOutputPojo = response.body();

                hotel_historical_recylerview.setVisibility(View.VISIBLE);

                progress_timeline.setVisibility(View.GONE);

                if (response.isSuccessful()) {


                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {


                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();


                        Log.e("hotel_size", "" + restaurantOutputPojo.getGetRestaurantsResults().size());

                        for (int i = 0; i < restaurantOutputPojo.getGetRestaurantsResults().size(); i++) {


                            String placeid = restaurantOutputPojo.getGetRestaurantsResults().get(i).getPlaceId();
                            String Icon = restaurantOutputPojo.getGetRestaurantsResults().get(i).getIcon();
                            String hotel_name = restaurantOutputPojo.getGetRestaurantsResults().get(i).getName();
                            String Id = restaurantOutputPojo.getGetRestaurantsResults().get(i).getId();

                            String reference = restaurantOutputPojo.getGetRestaurantsResults().get(i).getReference();

                            String address = restaurantOutputPojo.getGetRestaurantsResults().get(i).getFormattedAddress();
//                            String scope = restaurantOutputPojo.getGetRestaurantsResults().get(i).getScope();
                            String rating = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRating();

                            List<RestaurantPhoto> restaurantPhotos = new ArrayList<>();

                            restaurantPhotos = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos();

                            try {

                                String photo_ref = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();
                                int width = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                int height = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();

                                RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
                                Log.e("PhotosCount", "Photos->: " + photo_ref.substring(0, 20) + " " + i);
                                Log.e("height1", "width1: " + width);
                                Log.e("height1", "height1: " + height);
                                photos.add(restaurantPhoto);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            for (int kk = 0; kk < restaurantOutputPojo.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantOutputPojo.getGetRestaurantsResults().get(i).getTypes().toString());
                            }

                            if (restaurantPhotos != null) {
                                GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                        hotel_name, openingHoursPojo, restaurantPhotos, placeid, rating, reference, types, address);
                                getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);
                            }
                        }


                        RestaurantSearchHisMapAdapter restaurantSearchHisMapAdapter = new RestaurantSearchHisMapAdapter(CreateHistoricalMap.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList);
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(CreateHistoricalMap.this, LinearLayoutManager.HORIZONTAL, false);
                        hotel_historical_recylerview.setLayoutManager(mLayoutManager);
                        mLayoutManager.setStackFromEnd(false);
                        //mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                        hotel_historical_recylerview.setAdapter(restaurantSearchHisMapAdapter);
                        hotel_historical_recylerview.setItemAnimator(new DefaultItemAnimator());
                        hotel_historical_recylerview.setNestedScrollingEnabled(false);
                        hotel_historical_recylerview.setHorizontalScrollBarEnabled(true);
                        restaurantSearchHisMapAdapter.notifyDataSetChanged();


                    } else {

                        getallRestaurantDetailsPojoArrayList.clear();
                        Toast.makeText(CreateHistoricalMap.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else if (!response.isSuccessful()) {
                    Toast.makeText(CreateHistoricalMap.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });


    }

    private void nearByRestaurantsStaticLatlong(double latitude, double longitude, String radiusChecking) {

        //progress_timeline.setVisibility(View.VISIBLE);

        latLngString = latitude + "," + longitude;

        Call<RestaurantDetails> call = apiService.searchRestaurantsReviewWithnearby("", latLngString, radiusChecking, "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<RestaurantDetails>() {
            @Override
            public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                if (response.body() != null) {


                    RestaurantDetails restaurantOutputPojo = response.body();

                    sweetDialog.dismiss();

                    if (response.isSuccessful()) {

                        if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {

                            //  dialog.dismiss();
                            hotel_historical_recylerview.setVisibility(View.VISIBLE);

                            restaurantOutputPojoList = new ArrayList<>();
                            getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                            photos = new ArrayList<>();
                            types = new ArrayList<>();
                            htmlAttributions = new ArrayList<>();
                            openingHoursPojoList = new ArrayList<>();


                            Log.e("hotel_size", "" + restaurantOutputPojo.getGetRestaurantsResults().size());

                            for (int i = 0; i < restaurantOutputPojo.getGetRestaurantsResults().size(); i++) {


                                String placeid = restaurantOutputPojo.getGetRestaurantsResults().get(i).getPlaceId();
                                String Icon = restaurantOutputPojo.getGetRestaurantsResults().get(i).getIcon();
                                String hotel_name = restaurantOutputPojo.getGetRestaurantsResults().get(i).getName();
                                String Id = restaurantOutputPojo.getGetRestaurantsResults().get(i).getId();

                                String reference = restaurantOutputPojo.getGetRestaurantsResults().get(i).getReference();

                                String address = restaurantOutputPojo.getGetRestaurantsResults().get(i).getFormattedAddress();
                                String rating = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRating();

                                List<RestaurantPhoto> restaurantPhotos = new ArrayList<>();

                                restaurantPhotos = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos();

                                try {

                                    String photo_ref = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();
                                    int width = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                    int height = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();

                                    RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
                                    Log.e("PhotosCount", "Photos->: " + photo_ref.substring(0, 20) + " " + i);
                                    Log.e("height1", "width1: " + width);
                                    Log.e("height1", "height1: " + height);
                                    photos.add(restaurantPhoto);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                                for (int kk = 0; kk < restaurantOutputPojo.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                    types.add(restaurantOutputPojo.getGetRestaurantsResults().get(i).getTypes().toString());
                                }

                                if (restaurantPhotos != null) {
                                    GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                            hotel_name, openingHoursPojo, restaurantPhotos, placeid, rating, reference, types, address);
                                    getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);
                                }

                            }


                            RestaurantSearchHisMapAdapter restaurantSearchHisMapAdapter = new RestaurantSearchHisMapAdapter(CreateHistoricalMap.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(CreateHistoricalMap.this, LinearLayoutManager.HORIZONTAL, false);
                            hotel_historical_recylerview.setLayoutManager(mLayoutManager);
                            mLayoutManager.setStackFromEnd(false);
                            // mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                            hotel_historical_recylerview.setAdapter(restaurantSearchHisMapAdapter);
                            hotel_historical_recylerview.setItemAnimator(new DefaultItemAnimator());
                            hotel_historical_recylerview.setNestedScrollingEnabled(false);
                            hotel_historical_recylerview.setHorizontalScrollBarEnabled(true);
                            restaurantSearchHisMapAdapter.notifyDataSetChanged();


                        } else {
                            getallRestaurantDetailsPojoArrayList.clear();
                            Toast.makeText(CreateHistoricalMap.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                        }

                    } else if (!response.isSuccessful()) {
                        Toast.makeText(CreateHistoricalMap.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    sweetDialog.dismiss();
                    Toast.makeText(CreateHistoricalMap.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
                sweetDialog.cancel();
            }
        });


    }


    //Getting followers list
    private void getFollowers(final int userId) {

        if (Utility.isConnected(CreateHistoricalMap.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<GetFollowersOutput> call = apiService.getFollower("get_follower", userId);

                call.enqueue(new Callback<GetFollowersOutput>() {
                    @Override
                    public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                userid = response.body().getData().get(i).getUserid();
                                followerId = response.body().getData().get(i).getFollowerId();
                                firstName = response.body().getData().get(i).getFirstName();
                                lastName = response.body().getData().get(i).getLastName();
                                userPhoto = response.body().getData().get(i).getPicture();

                                if (!followerId.equals(String.valueOf(userid))) {

                                    GetFollowersData getFollowersData = new GetFollowersData(userid, followerId, firstName, lastName, userPhoto);
                                    getFollowersDataList.add(getFollowersData);

                                }


                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {
            Toast.makeText(this, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }

    }

    // Search nearby restaurants API

    private void nearByRestaurants(String restaurantName, double latitude, double longitude) {

        if (Utility.isConnected(getApplicationContext())) {

            progress_timeline.setVisibility(View.VISIBLE);


            latLngString = String.valueOf(latitude) + "," + String.valueOf(longitude);


            Call<RestaurantDetails> call = apiService.searchRestaurantsReviewWithnearby(restaurantName, latLngString, "500", "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
            call.enqueue(new Callback<RestaurantDetails>() {
                @Override
                public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                    RestaurantDetails restaurantOutputPojo = response.body();


                    if (response.isSuccessful()) {

                        progress_timeline.setVisibility(View.GONE);

                        if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {

                            restaurantOutputPojoList = new ArrayList<>();
                            getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                            photos = new ArrayList<>();
                            types = new ArrayList<>();
                            htmlAttributions = new ArrayList<>();
                            openingHoursPojoList = new ArrayList<>();

                            Log.e("hotel_size", "" + restaurantOutputPojo.getGetRestaurantsResults().size());

                            for (int i = 0; i < restaurantOutputPojo.getGetRestaurantsResults().size(); i++) {

                                String placeid = restaurantOutputPojo.getGetRestaurantsResults().get(i).getPlaceId();
                                String Icon = restaurantOutputPojo.getGetRestaurantsResults().get(i).getIcon();
                                String hotel_name = restaurantOutputPojo.getGetRestaurantsResults().get(i).getName();
                                String Id = restaurantOutputPojo.getGetRestaurantsResults().get(i).getId();
                                String reference = restaurantOutputPojo.getGetRestaurantsResults().get(i).getReference();
                                String address = restaurantOutputPojo.getGetRestaurantsResults().get(i).getFormattedAddress();
                                String rating = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRating();

                                List<RestaurantPhoto> restaurantPhotos;

                                restaurantPhotos = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos();

                                try {

                                    String photo_ref = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();

                                    Log.e("photo_size", "onResponse: " + restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().size());
                                    int width = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                    int height = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();

                                    RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
                                    photos.add(restaurantPhoto);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                                for (int kk = 0; kk < restaurantOutputPojo.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                    types.add(restaurantOutputPojo.getGetRestaurantsResults().get(i).getTypes().toString());
                                }

                                if (restaurantPhotos != null) {

                                    GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                            hotel_name, openingHoursPojo, restaurantPhotos, placeid, rating, reference, types, address);
                                    getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);
                                }


                            }

                            RestaurantSearchHisMapAdapter restaurantSearchHisMapAdapter = new RestaurantSearchHisMapAdapter(CreateHistoricalMap.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(CreateHistoricalMap.this, LinearLayoutManager.HORIZONTAL, false);
                            hotel_historical_recylerview.setLayoutManager(mLayoutManager);
                            hotel_historical_recylerview.setAdapter(restaurantSearchHisMapAdapter);
                            hotel_historical_recylerview.setItemAnimator(new DefaultItemAnimator());
                            hotel_historical_recylerview.setNestedScrollingEnabled(false);
                            hotel_historical_recylerview.setHorizontalScrollBarEnabled(true);
                            restaurantSearchHisMapAdapter.notifyDataSetChanged();


                        } else {

                            Toast.makeText(CreateHistoricalMap.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                            progress_timeline.setVisibility(View.GONE);

                        }

                    } else {

                    }
                }

                @Override
                public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                    // Log error here since request failed
                    call.cancel();
                    progress_timeline.setVisibility(View.GONE);
                }
            });

        } else {

            Toast.makeText(getApplicationContext(), "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }

    }

    /*Interface for contact selecting*/
    public void onContactSelected(View view, GetFollowersData contact, int checkedStatus) {


        String followerName;
        followerName = contact.getFirstName() + " " + contact.getLastName();


        if (checkedStatus == 1) {

            Log.e(TAG, "onContactSelected: " + checkedStatus);
            if (cohostNameIdListd.containsKey(Integer.valueOf(contact.getFollowerId()))) {

                return;

            } else {

                cohostNameIdListd.put(Integer.valueOf(contact.getFollowerId()), followerName);

            }


        } else if (checkedStatus == 0) {

//            Toast.makeText(this, ""+followerName, Toast.LENGTH_SHORT).show();


            Log.e(TAG, "onContactSelected: " + checkedStatus);
            cohostNameIdListd.remove(Integer.valueOf(contact.getFollowerId()));
//            tempFollowersDataList.add(contact);

        }

        Log.e(TAG, "onContactSelected: " + cohostNameIdListd.toString());


    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {


            case R.id.add_image_post:

                break;

            case R.id.add_people:

                break;

            case R.id.rl_create_post:

                break;

            case R.id.close:

//                onBackPressed();
               /* Intent intent = new Intent(this, Home.class);
                startActivity(intent);
                overridePendingTransition(R.anim.swipe_righttoleft, R.anim.swipe_lefttoright);*/

                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Confirm")
                        .setContentText("Are you sure you want to skip")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                if (googleApiClient != null) {
                                    if (googleApiClient.isConnected()) {
                                        googleApiClient.disconnect();
                                    }
                                }

                                finish();
                                overridePendingTransition(R.anim.swipe_righttoleft, R.anim.swipe_lefttoright);

                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


                break;

            case R.id.edit_restaurants:

                ll_search_restaurant.setVisibility(View.VISIBLE);
                ll_history_fields.setVisibility(View.GONE);
                rl_add_img_tag.setVisibility(View.GONE);

                break;

            case R.id.restaurantName:

                ll_search_restaurant.setVisibility(View.VISIBLE);
                ll_history_fields.setVisibility(View.GONE);
                rl_add_img_tag.setVisibility(View.GONE);

                break;

            case R.id.tv_add_photo:

                selectImage();

                break;

            case R.id.ic_add_photo:

                selectImage();

                break;

            case R.id.friendsListDisplayTag:


                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CreateHistoricalMap.this);
                LayoutInflater inflater1 = LayoutInflater.from(CreateHistoricalMap.this);
                @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.activity_select_followers, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setTitle("");

                follwersRecylerview = (RecyclerView) dialogView.findViewById(R.id.user_followers_recylerview);
                selectActionBar = (Toolbar) dialogView.findViewById(R.id.action_bar_select_follower);
                selectClose = (ImageButton) selectActionBar.findViewById(R.id.close);
                selectDone = (ImageButton) selectActionBar.findViewById(R.id.tv_done);
                empty_list = (RelativeLayout) dialogView.findViewById(R.id.empty_list);
                search_followers = (SearchView) dialogView.findViewById(R.id.search_followers);

                search_followers.setQueryHint("Search");

                search_followers.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted
                        historicalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        // filter recycler view when text is changed
                        historicalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }
                });


                if (getFollowersDataList.isEmpty()) {

                    empty_list.setVisibility(View.VISIBLE);
                    follwersRecylerview.setVisibility(View.GONE);

                } else {

                    empty_list.setVisibility(View.GONE);
                    follwersRecylerview.setVisibility(View.VISIBLE);
                }


                historicalTagPeopleAdapter = new HistoricalTagPeopleAdapter(CreateHistoricalMap.this, getFollowersDataList, cohostNameIdListd, this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CreateHistoricalMap.this);
                follwersRecylerview.setLayoutManager(mLayoutManager);
                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                follwersRecylerview.setAdapter(historicalTagPeopleAdapter);
                follwersRecylerview.hasFixedSize();
                historicalTagPeopleAdapter.notifyDataSetChanged();

                final AlertDialog dialog = dialogBuilder.create();
                dialog.show();

                selectClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Log.e(TAG, "onClick: " + firstTimeTagged);
                        if (!firstTimeTagged) {
                            cohostNameIdListd.clear();
                            getFollowersDataList.clear();
                            getFollowers(userid);

                        }
                        dialog.dismiss();

                    }
                });

                selectDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (cohostNameIdListd.values().size() == 0) {

                            ll_tag_people.setVisibility(View.VISIBLE);
                            friendsListDisplayTag.setText(R.string.tag_people);
                            friendsList.clear();


                        } else {

                            firstTimeTagged = true;
                            ll_tag_people.setVisibility(View.VISIBLE);
                            String[] friends = cohostNameIdListd.values().toArray(new String[cohostNameIdListd.values().size()]);

                            switch (cohostNameIdListd.values().size()) {

                                case 1:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1]);

                                    break;

                                case 2:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + friends[cohostNameIdListd.values().size() - 2]);

                                    break;
                            }

                            if (cohostNameIdListd.values().size() > 2) {

                                friendsListDisplayTag.setText("With - ");
                                friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + String.valueOf(cohostNameIdListd.values().size() - 1) + " Others");

                            }


                            Integer[] friendId = cohostNameIdListd.keySet().toArray(new Integer[cohostNameIdListd.keySet().size()]);
                            friendsList = new ArrayList<Integer>(Arrays.asList(friendId));

                        }

                        dialog.dismiss();

                    }
                });

                break;

            case R.id.ic_add_people:


                final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(CreateHistoricalMap.this);
                LayoutInflater inflater2 = LayoutInflater.from(CreateHistoricalMap.this);
                @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.activity_select_followers, null);
                dialogBuilder1.setView(dialogView1);
                dialogBuilder1.setTitle("");

                follwersRecylerview = (RecyclerView) dialogView1.findViewById(R.id.user_followers_recylerview);
                selectActionBar = (Toolbar) dialogView1.findViewById(R.id.action_bar_select_follower);
                selectClose = (ImageButton) selectActionBar.findViewById(R.id.close);
                selectDone = (ImageButton) selectActionBar.findViewById(R.id.tv_done);
                empty_list = (RelativeLayout) dialogView1.findViewById(R.id.empty_list);
                search_followers = (SearchView) dialogView1.findViewById(R.id.search_followers);

                search_followers.setQueryHint("Search");

                search_followers.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted
                        historicalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        // filter recycler view when text is changed
                        historicalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }
                });


                if (getFollowersDataList.isEmpty()) {

                    empty_list.setVisibility(View.VISIBLE);
                    follwersRecylerview.setVisibility(View.GONE);

                } else {

                    empty_list.setVisibility(View.GONE);
                    follwersRecylerview.setVisibility(View.VISIBLE);
                }


                historicalTagPeopleAdapter = new HistoricalTagPeopleAdapter(CreateHistoricalMap.this, getFollowersDataList, cohostNameIdListd, this);
                RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(CreateHistoricalMap.this);
                follwersRecylerview.setLayoutManager(mLayoutManager1);
                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                follwersRecylerview.setAdapter(historicalTagPeopleAdapter);
                follwersRecylerview.hasFixedSize();
                historicalTagPeopleAdapter.notifyDataSetChanged();

                final AlertDialog dialog1 = dialogBuilder1.create();
                dialog1.show();

                selectClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Log.e(TAG, "onClick: " + firstTimeTagged);
                        if (!firstTimeTagged) {
                            cohostNameIdListd.clear();
                            getFollowersDataList.clear();
                            getFollowers(userid);

                        }

                        Log.e(TAG, "onClick: " + cohostNameIdListd.toString());

                        dialog1.dismiss();

                    }
                });

                selectDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (cohostNameIdListd.values().size() == 0) {

                            ll_tag_people.setVisibility(View.VISIBLE);
                            friendsListDisplayTag.setText(R.string.tag_people);
                            friendsList.clear();


                        } else {

                            firstTimeTagged = true;
                            ll_tag_people.setVisibility(View.VISIBLE);
                            String[] friends = cohostNameIdListd.values().toArray(new String[cohostNameIdListd.values().size()]);

                            switch (cohostNameIdListd.values().size()) {

                                case 1:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1]);

                                    break;

                                case 2:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + friends[cohostNameIdListd.values().size() - 2]);

                                    break;
                            }

                            if (cohostNameIdListd.values().size() > 2) {

                                friendsListDisplayTag.setText("With - ");
                                friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + String.valueOf(cohostNameIdListd.values().size() - 1) + " Others");

                            }


                            Integer[] friendId = cohostNameIdListd.keySet().toArray(new Integer[cohostNameIdListd.keySet().size()]);
                            friendsList = new ArrayList<Integer>(Arrays.asList(friendId));

                        }

                        dialog1.dismiss();

                    }
                });


                break;


            case R.id.tv_create_history:


                if (isConnected(CreateHistoricalMap.this)) {

                    if (validateFields()) {


                        if (friendsList.isEmpty() && cohostNameIdListd.values().isEmpty()) {

                            tag_status = 0;

                        } else {

                            tag_status = 1;
                        }

                        createPostDialog = new SpotsDialog.Builder().setContext(CreateHistoricalMap.this).setMessage("Posting..").build();
                        createPostDialog.show();

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        userid = Integer.parseInt(Pref_storage.getDetail(CreateHistoricalMap.this, "userId"));


                        String friendlist = new Gson().toJson(friendsList);
                        Log.e(TAG, "onClick:google_id " + google_id);
                        Log.e(TAG, "onClick:google_photo " + google_photo);
                        Log.e(TAG, "onClick:hotel_name " + hotel_name);
                        Log.e(TAG, "onClick:place_id " + place_id);
                        Log.e(TAG, "onClick:place_id " + org.apache.commons.text.StringEscapeUtils.escapeJava(description_of_visit.getText().toString().trim()));
                        Log.e(TAG, "onClick:tag_status " + tag_status);
                        Log.e(TAG, "onClick:hotel_address " + hotel_address);
                        Log.e(TAG, "onClick:friendlist " + friendlist);
                        Log.e(TAG, "onClick:latitude " + latitude);
                        Log.e(TAG, "onClick:longitude " + longitude);
                        Log.e(TAG, "onClick:userid " + userid);

                        try {

                            Call<CommonOutputPojo> call = apiService.createTimeLinePost("create_timeline",
                                    0,
                                    google_id,
                                    google_photo,
                                    hotel_name,
                                    place_id,
                                    "",
                                    org.apache.commons.text.StringEscapeUtils.escapeJava(description_of_visit.getText().toString().trim()),
                                    tag_status,
                                    hotel_address,
                                    "",
                                    friendlist,
                                    latitude,
                                    longitude,
                                    userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            //Success
                                            sendHistoryImages(response.body().getData());


                                        }


                                    } else {

                                        Toast.makeText(CreateHistoricalMap.this, "Something went wrong please try again", Toast.LENGTH_SHORT).show();
                                        createPostDialog.dismiss();
                                    }


                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                    createPostDialog.dismiss();
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();
                            createPostDialog.dismiss();

                        }


                    }


                } else {


                    Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();

                }


                break;

            case R.id.date_of_restaurent_visit:

                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateHistoricalMap.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String frmdate;

                                if ((monthOfYear + 1) > 9) {

                                    frmdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                } else {

                                    frmdate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;

                                }


                                date_of_restaurent_visit.setText(frmdate);
                                date_of_restaurent_visit.setError(null);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

                break;

        }

    }

    private boolean validateFields() {

        boolean valid = true;

        String hotelname = restaurantName.getText().toString();


        if (hotelname.length() == 0) {
            edit_search_hotel.setError("Please select a hotel");
            valid = false;
        } else {
            edit_search_hotel.setError(null);
        }


        int imageCount = imagesEncodedList.size();

        if (imageCount == 0) {

            Toast.makeText(this, "Please choose atleast one photo", Toast.LENGTH_SHORT).show();
            valid = false;

        } else {


        }


        return valid;
    }


    public void sendHistoryImages(int id) {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        userid = Integer.parseInt(Pref_storage.getDetail(CreateHistoricalMap.this, "userId"));


        for (int i = 0; i < imagesEncodedList.size(); i++) {

            String image = imagesEncodedList.get(i).getImg();

            try {

                file = new File(image);

                File compressedImageFile = null;

                compressedImageFile = new Compressor(this).setQuality(100).compressToFile(file);

                ProgressRequestBody fileBody = new ProgressRequestBody(compressedImageFile, this);
                MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), fileBody);

                Call<CommonOutputPojo> request = apiService.createHistoricalImage("create_timeline_image", id, userid, filePart);

                request.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {
                        Log.e(TAG, "onResponse: Images" + response);
                        count++;

                        if (count == imagesEncodedList.size()) {

                            Log.e(TAG, "onResponse: Image uploaded successfully");
                            createPostDialog.dismiss();
                            Intent intent = new Intent(CreateHistoricalMap.this, Home.class);
                            startActivity(intent);
                            finish();


                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {

                        Log.e(TAG, "onFailure: Images->" + t.getLocalizedMessage());

                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


    private void selectImage() {

        validate();

        List<String> listItems = new ArrayList<String>();

        listItems.add("Take Photo");
        listItems.add("Choose from library");
        listItems.add("Cancel");

        if (image_status == 1) {

            listItems.remove(2);
            listItems.add("Remove all photos");
            listItems.add("Cancel");

        }

        final CharSequence[] items = listItems.toArray(new CharSequence[listItems.size()]);

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(CreateHistoricalMap.this);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(CreateHistoricalMap.this);

                if (items[item].equals("Take Photo")) {


                    try {

                        if (ActivityCompat.checkSelfPermission(CreateHistoricalMap.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(CreateHistoricalMap.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(CreateHistoricalMap.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {


                            Log.e(TAG, "onClick: else part");

                            if (ActivityCompat.checkSelfPermission(CreateHistoricalMap.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(CreateHistoricalMap.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                                Log.e(TAG, "onClick: camera");

                                cameraIntent();

                            } else {

                                Log.e(TAG, "onClick: cameraStorageGrantRequest");

                                cameraStorageGrantRequest();

                            }


                        } else {

                            ActivityCompat.requestPermissions(CreateHistoricalMap.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (items[item].equals("Choose from library")) {
                    if (result) {

                        imagepath = null;
                        galleryIntent();

                    }


                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                } else if (items[item].equals("Remove all photos")) {

                    image_status = 0;
                    imagepath = null;
                    imagesEncodedList.clear();
//                    init();
                    ll_history_image_section.setVisibility(View.GONE);
                    tv_add_photo.setText(R.string.add_photos);

                }
            }
        });
        builder.show();
    }

    private void validate() {

        if (imagepath == null && imagesEncodedList.isEmpty()) {

            image_status = 0;

        } else if (!imagesEncodedList.isEmpty()) {

            image_status = 1;

        }

    }


    private void init() {

        if (imagesEncodedList.size() == 0) {

            ll_history_image_section.setVisibility(View.GONE);
            tv_add_photo.setText(R.string.add_photos);

        } else {

            ll_history_image_section.setVisibility(View.VISIBLE);
            tv_add_photo.setText("Edit photos");
            mviewpager.setAdapter(new HistoryImagesAdapter(CreateHistoricalMap.this, imagesEncodedList, this));
            circleIndicator.setViewPager(mviewpager);
            NUM_PAGES = imagesEncodedList.size();
            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    mviewpager.setCurrentItem(currentPage++, true);
                }
            };

            // Pager listener over indicator
            circleIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;

                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int pos) {

                }
            });

        }

    }

    @Override
    public void removedImageLisenter(List<Image> imagesList, int position) {

        imagesEncodedList.remove(position);
        Log.e(TAG, "removedImageLisenter: " + imagesEncodedList.size());
        init();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        Log.d("onActivityResult()", "resultCode" + Integer.toString(resultCode));
        Log.d("onActivityResult()", "requestCode" + Integer.toString(requestCode));
        Log.d("onActivityResult()", "data" + data);


        switch (requestCode) {

            case 1:

                switch (resultCode) {

                    case Activity.RESULT_OK: {

                        Log.v("isGPSEnabled", "latitude->" + latitude);
                        Log.v("isGPSEnabled", "longitude->" + longitude);

                        if (Utility.isConnected(CreateHistoricalMap.this)) {

                            sweetDialog = new SpotsDialog.Builder().setContext(CreateHistoricalMap.this).setMessage("").build();
                            sweetDialog.setCancelable(false);
                            sweetDialog.show();
                            Thread thread = new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        sleep(7000);
                                        String rediusChecking = "500";
                                        Log.e("isGPSEnabled", "latitude->" + latitude);
                                        Log.e("isGPSEnabled", "longitude->" + longitude);
                                        nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            };
                            thread.start();

                        } else {

                            Toast.makeText(CreateHistoricalMap.this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();

                        }
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        // Toast.makeText(getApplicationContext(), "Location not enabled.", Toast.LENGTH_LONG).show();
                        //call restaurant api
                        //  get_hotel_static_location();

                        if (Utility.isConnected(CreateHistoricalMap.this)) {

                            sweetDialog = new SpotsDialog.Builder().setContext(CreateHistoricalMap.this).setMessage("").build();
                            sweetDialog.setCancelable(false);
                            sweetDialog.show();
                            Call<Get_HotelStaticLocation_Response> call = apiInterface.get_hotel_static_location("get_hotel_static_location", userid);
                            call.enqueue(new Callback<Get_HotelStaticLocation_Response>() {
                                @Override
                                public void onResponse(Call<Get_HotelStaticLocation_Response> call, Response<Get_HotelStaticLocation_Response> response) {


                                    if (response.isSuccessful()) {


                                        double latitude = response.body().getData().get(0).getLatitude();
                                        double longitude = response.body().getData().get(0).getLongitude();
                                        String rediusChecking = "500";

                                        nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);


                                    } else {
                                        Toast.makeText(CreateHistoricalMap.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Get_HotelStaticLocation_Response> call, Throwable t) {
                                    // Log error here since request failed
                                    call.cancel();
                                }
                            });

                            break;
                        } else {

                            Toast.makeText(this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    default: {
                        break;
                    }
                }

                break;
        }

        try {

            if (requestCode == REQUEST_CAMERA) {

                onCaptureImageResult(data);

            }
            // When an Image is picked
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && null != data && !(data.toString().equals("Intent { (has extras) }"))) {
                // Get the Image from data

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                if (data.getData() != null) {


                    Uri mImageUri = data.getData();
                    Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                    cursor.moveToFirst();
                    String document_id = cursor.getString(0);
                    document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                    cursor.close();

                    cursor = getContentResolver().query(
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                    cursor.moveToFirst();
                    imageEncoded = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    cursor.close();

                    Image image = new Image(imageEncoded);
                    imagesEncodedList.add(image);
                    ll_history_image_section.setVisibility(View.VISIBLE);
                    init();

                    Log.v("LOG_TAG", "Selected Images" + imageEncoded);

                } else {

                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);

                            // Get the cursor

                            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                            cursor.moveToFirst();
                            String document_id = cursor.getString(0);
                            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                            cursor.close();

                            cursor = getContentResolver().query(
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                            cursor.moveToFirst();
                            imageEncoded = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                            Image image = new Image(imageEncoded);
                            imagesEncodedList.add(image);
                            ll_history_image_section.setVisibility(View.VISIBLE);

                            init();

                            cursor.close();


                            Log.v("LOG_TAG", "Selected Images" + imagesEncodedList);
                        }
                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }
                }
            } else {
                /* Toast.makeText(this, "You haven't picked Image",Toast.LENGTH_LONG).show();*/
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }


    }

    // load static location
    private void get_hotel_static_location() {


    }

    private void cameraStorageGrantRequest() {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                CreateHistoricalMap.this);

        builder.setTitle("Food Wall would like to access your camera and storage")
                .setMessage("You previously chose not to use Android's camera or storage with Food Wall. To upload your photos, allow camera and storage permission in App Settings. Click Ok to allow.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // permission dialog
                        dialog.dismiss();
                        try {

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();


    }

    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = null;
        try {
            thumbnail = getThumbnail(imageUri);
            saveBitmap(thumbnail);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("CameraCaptureError", "CameraCaptureError->" + e.getMessage());
        }


    }

    private void saveBitmap(Bitmap bm) {


        String folder_main = "FoodWall";

        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }

        File newFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        Log.e("ImageFile", "ImageFile->" + newFile);

        try {

            FileOutputStream fileOutputStream = new FileOutputStream(newFile);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            imagepath = String.valueOf(newFile);
            Image image = new Image(imagepath);
            imagesEncodedList.add(image);
            ll_history_image_section.setVisibility(View.VISIBLE);
            init();
            Log.e("MyPath", "MyImagePath->" + imagepath);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void cameraIntent() {


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, REQUEST_CAMERA);

        } else {


            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);

        }


    }

    public Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException {

        InputStream input = CreateHistoricalMap.this.getContentResolver().openInputStream(uri);
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 800) ? (originalSize / 800) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }


    private void galleryIntent() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_GALLERY_PERMISSION);
        } else {

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);

        }

    }

    private void checkPermissions() {
        int permissionLocation = ContextCompat.checkSelfPermission(CreateHistoricalMap.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), ACCESS_FINE_LOCATION_INTENT_ID);
            }
        } else {
            if (gpsTracker.isGPSEnabled) {
                permissionCheckingCreateView();
            } else {
                getMyLocation();
            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == ACCESS_FINE_LOCATION_INTENT_ID) {

            int permissionSize = permissions.length;

            for (int i = 0; i < permissionSize; i++) {

                if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                    if (gpsTracker.isGPSEnabled) {

                        getMyLocation();
                        sweetDialog = new SpotsDialog.Builder().setContext(CreateHistoricalMap.this).setMessage("").build();
                        sweetDialog.setCancelable(false);
                        sweetDialog.show();
                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(7000);
                                    String rediusChecking = "500";
                                    nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        thread.start();

                    } else {

                        getMyLocation();
                    }

                } else if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                    if (Utility.isConnected(CreateHistoricalMap.this)) {

                        sweetDialog = new SpotsDialog.Builder().setContext(CreateHistoricalMap.this).setMessage("").build();
                        sweetDialog.setCancelable(false);
                        sweetDialog.show();
                        Call<Get_HotelStaticLocation_Response> call = apiInterface.get_hotel_static_location("get_hotel_static_location", userid);
                        call.enqueue(new Callback<Get_HotelStaticLocation_Response>() {
                            @Override
                            public void onResponse(Call<Get_HotelStaticLocation_Response> call, Response<Get_HotelStaticLocation_Response> response) {


                                sweetDialog.dismiss();

                                if (response.isSuccessful()) {


                                    double latitude = response.body().getData().get(0).getLatitude();
                                    double longitude = response.body().getData().get(0).getLongitude();
                                    String radiusChecking = "5000";

                                    nearByRestaurantsStaticLatlong(latitude, longitude, radiusChecking);

                                } else {

                                    Toast.makeText(CreateHistoricalMap.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Get_HotelStaticLocation_Response> call, Throwable t) {
                                // Log error here since request failed
                                call.cancel();
                            }
                        });
                    } else {

                        Toast.makeText(this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
                    }
                }

            }

        }

        int permissionSize = permissions.length;

        Log.e(TAG, "onRequestPermissionsResult:" + "requestCode->" + requestCode);

        switch (requestCode) {


            case 123:

                cameraStorageGrantRequest();
                break;

            case REQUEST_CAMERA_PERMISSION:

                for (int i = 0; i < permissionSize; i++) {

                    if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission granted");

                    } else if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission denied");
                        cameraStorageGrantRequest();
                    }

                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");

                        if (ActivityCompat.checkSelfPermission(CreateHistoricalMap.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraIntent();

                        }


                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");

                        if (ActivityCompat.checkSelfPermission(CreateHistoricalMap.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraStorageGrantRequest();

                        }

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

            case REQUEST_GALLERY_PERMISSION:

                for (int i = 0; i < permissionSize; i++) {


                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");
                        galleryIntent();

                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");
                        cameraStorageGrantRequest();

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

        }
    }


    private void getMyLocation() {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(CreateHistoricalMap.this, Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, CreateHistoricalMap.this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    mylocation = LocationServices.FusedLocationApi
                                            .getLastLocation(googleApiClient);

                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(CreateHistoricalMap.this,
                                                REQUEST_CHECK_SETTINGS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }


    /*  Show Popup to access User Permission  */
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(CreateHistoricalMap.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(CreateHistoricalMap.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(CreateHistoricalMap.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }


    public synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    public void onTagsChanged(Collection<String> tags) {

        /*if (tags.size() == 0) {

//            friendsListDisplay.setVisibility(View.GONE);

        } else {

//            friendsListDisplay.setVisibility(View.VISIBLE);

        }

        cohostList.clear();

        Log.d("FollowersId", "Tags changed: ");
        Log.d("FollowersId", Arrays.toString(tags.toArray()));

        Object[] ob = tags.toArray();

        for (Object value : ob) {

            System.out.println("Number = " + value);
            cohostList.add(cohostNameIdListd.get(value));

        }

        Iterator<String> iterator = cohostNameIdListd.values().iterator();
        while (iterator.hasNext()) {

            String certification = iterator.next();

            if (tags.contains(certification)) {

                Log.e("EventCheck", "EventCheck->" + certification);

            } else {

                Log.e("EventCheck", "Not Found");
                iterator.remove();

            }

        }

//        Toast.makeText(this, "" + cohostNameIdListd.values().toString(), Toast.LENGTH_SHORT).show();
*/

    }

    @Override
    public void onEditingFinished() {
        Log.d("FollowersId", "OnEditing finished");
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(friendsListDisplay.getWindowToken(), 0);
        friendsListDisplay.clearFocus();
    }


    @Override
    public void onProgressUpdate(int percentage) {
        Log.e(TAG, "onProgressUpdate: " + percentage);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }


    @Override
    public void onConnected(Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            latitude = mylocation.getLatitude();
            longitude = mylocation.getLongitude();
            Log.e("latttttt", "onCreate: " + latitude);
            Log.e("latttttt", "onCreate: " + longitude);

        } else {
            getMyLocation();
        }
    }
}