package com.kaspontech.foodwall.historicalMapPackage;

import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.adapters.HistoricalMap.DisplayHistoryMapAdapter;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.DisplayPojo.GetHistoricalMapOutputPojo;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.DisplayPojo.GetHistoryMapData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.RecyclerSectionItemDecoration;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoricalMap extends AppCompatActivity implements View.OnClickListener {


    // Action bar
    Toolbar actionbar;
    ImageButton back, add_history;
    private int prev,next;

    TextView tv_restaurant_count;
    NestedScrollView historical_main_scrollview;
    LinearLayout ll_historical_map,ll_not_visited;

    RecyclerView recyler_view_historical_map;
    DisplayHistoryMapAdapter displayHistoryMapAdapter;
    List<GetHistoryMapData> getHistoryMapDataList = new ArrayList<>();

    // Var

    int total_restaurants;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historical_map);

        actionbar = (Toolbar) findViewById(R.id.action_bar_historical_map);
        back = (ImageButton) actionbar.findViewById(R.id.back);
        add_history = (ImageButton) actionbar.findViewById(R.id.add_history);

        tv_restaurant_count =(TextView)findViewById(R.id.tv_restaurant_count);
        ll_historical_map = (LinearLayout)findViewById(R.id.ll_historical_map);
        ll_not_visited = (LinearLayout)findViewById(R.id.ll_not_visited);
        recyler_view_historical_map = (RecyclerView)findViewById(R.id.recyler_view_historical_map);
        historical_main_scrollview = (NestedScrollView)findViewById(R.id.historical_main_scrollview);

        back.setOnClickListener(this);
        add_history.setOnClickListener(this);


        get_historical();


    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {


            case R.id.back:

                finish();

                break;


            case R.id.add_history:

                Intent intent = new Intent(HistoricalMap.this, CreateHistoricalMap.class);
                startActivity(intent);
                finish();

                break;


        }

    }


    private void get_historical() {

        final AlertDialog dialog  = new SpotsDialog.Builder().setContext(HistoricalMap.this).setMessage("").build();

        dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        int userid = Integer.parseInt(Pref_storage.getDetail(HistoricalMap.this, "userId"));


        try {

            Call<GetHistoricalMapOutputPojo> call = apiService.getHistoricalMap("get_historical",
                     userid);

            call.enqueue(new Callback<GetHistoricalMapOutputPojo>() {
                @Override
                public void onResponse(Call<GetHistoricalMapOutputPojo> call, Response<GetHistoricalMapOutputPojo> response) {

                    String responseStatus = response.body().getResponseMessage();

                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                    if (responseStatus.equals("success")) {

                        //Success

                        historical_main_scrollview.setVisibility(View.VISIBLE);
                        ll_not_visited.setVisibility(View.GONE);

                        getHistoryMapDataList = response.body().getData();

                        /*for(int i = 0 ; i < response.body().getData().size() ; i++){

                            String timeline_id = response.body().getData().get(i).getTimeline_id();
                            String hisId =  response.body().getData().get(i).getHisId();
                            String hotelName = response.body().getData().get(i).getHotelName();
                            String hisDescripion=  response.body().getData().get(i).getHisDescripion();
                            String withWhom =  response.body().getData().get(i).getWithWhom();
                            String address =  response.body().getData().get(i).getAddress();
                            String createdBy =  response.body().getData().get(i).getCreatedBy();
                            String createdOn =  response.body().getData().get(i).getCreatedOn();
                            String firstName =  response.body().getData().get(i).getFirstName();
                            String lastName =  response.body().getData().get(i).getLastName();
                            String picture =  response.body().getData().get(i).getPicture();
                            List< GetHisMapImage> image =  response.body().getData().get(i).getImage();
                            List<TaggedPeoplePojo> whom = response.body().getData().get(i).getWhom();
                            int imageCount = response.body().getData().get(i).getImageCount();
                            int whom_count = response.body().getData().get(i).getWhom_count();
                            total_restaurants = Integer.parseInt(response.body().getData().get(i).getTotalHotel());

                            GetHistoryMapData getHistoryMapData = new GetHistoryMapData(timeline_id,hisId, hotelName,
                                    hisDescripion, withWhom, address, createdBy, createdOn, firstName, lastName
                                    , picture, image, imageCount,whom_count,whom);


                            getHistoryMapDataList.add(getHistoryMapData);


                        }*/

                        dialog.dismiss();
                        ll_historical_map.setVisibility(View.VISIBLE);

                        ValueAnimator animator = ValueAnimator.ofInt(0, total_restaurants);
                        animator.setDuration(1500);
                        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            public void onAnimationUpdate(ValueAnimator animation) {
                                tv_restaurant_count.setText(animation.getAnimatedValue().toString());
                            }
                        });
                        animator.start();


                        displayHistoryMapAdapter = new DisplayHistoryMapAdapter(HistoricalMap.this, getHistoryMapDataList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HistoricalMap.this);
                        recyler_view_historical_map.setLayoutManager(mLayoutManager);
                        recyler_view_historical_map.setItemAnimator(new DefaultItemAnimator());
                        recyler_view_historical_map.setAdapter(displayHistoryMapAdapter);
                        recyler_view_historical_map.hasFixedSize();
                        recyler_view_historical_map.setNestedScrollingEnabled(false);
                        displayHistoryMapAdapter.notifyDataSetChanged();

                        RecyclerSectionItemDecoration sectionItemDecoration =
                                new RecyclerSectionItemDecoration(getResources().getDimensionPixelSize(R.dimen.header),
                                        true,
                                        getSectionCallback(getHistoryMapDataList));
                        recyler_view_historical_map.addItemDecoration(sectionItemDecoration);


                    }else if(responseStatus.equals("nodata")){

                        historical_main_scrollview.setVisibility(View.GONE);
                        ll_not_visited.setVisibility(View.VISIBLE);
                        dialog.dismiss();

                    }

                }

                @Override
                public void onFailure(Call<GetHistoricalMapOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    historical_main_scrollview.setVisibility(View.GONE);
                    ll_not_visited.setVisibility(View.VISIBLE);
                    dialog.dismiss();

                }
            });

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    private RecyclerSectionItemDecoration.SectionCallback getSectionCallback(final List<GetHistoryMapData> getHistoryMapDataList) {
        return new RecyclerSectionItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {

                boolean show = false;

                if(position == 0){

                    show = true;

                }

                if(position != 0){

                    prev = Integer.parseInt(getHistoryMapDataList.get(position-1).getCreatedOn().substring(5,7));
                    next = Integer.parseInt(getHistoryMapDataList.get(position).getCreatedOn().substring(5,7));

                    if(prev > next){

                        show = true;

                    }else{

                        show = false;

                    }
                }

                return show;
            }

            @Override
            public CharSequence getSectionHeader(int position) {


                String date = getHistoryMapDataList.get(position).getCreatedOn();
                String displayDate = getMonthName(date)+" "+getYear(date);


                return displayDate;
            }
        };
    }


    private String getMonthName(String date) {

        String monthName = "";

        String month = date.substring(5, 7);

        switch (month) {


            case "01":
                monthName = "January";
                break;

            case "02":
                monthName = "February";
                break;

            case "03":
                monthName = "March";
                break;

            case "04":
                monthName = "April";
                break;

            case "05":
                monthName = "May";
                break;

            case "06":
                monthName = "June";
                break;

            case "07":
                monthName = "July";
                break;

            case "08":
                monthName = "August";
                break;

            case "09":
                monthName = "September";
                break;

            case "10":
                monthName = "October";
                break;

            case "11":
                monthName = "November";
                break;

            case "12":
                monthName = "December";
                break;
        }


        return monthName;
    }

    private String getDate(String date) {

        String day = date.substring(8, 10);
        return day;

    }

    private String getYear(String date) {

        String year = date.substring(0, 4);
        return year;

    }
}
