package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by vishnukm on 24/3/18.
 */

public class Popmenu_activity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

View v;
ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adapter_newsfeed_story);

    imageView= (ImageView)findViewById(R.id.img_view_more);



        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(Popmenu_activity.this, v);
                popup.setOnMenuItemClickListener(Popmenu_activity.this);
                popup.inflate(R.menu.options_menu_adapter_post);
                popup.show();
            }
        });
    }
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Toast.makeText(this, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.menu0:
                // do your code
                new SweetAlertDialog(Popmenu_activity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure to delete this post?")
                        .setContentText("Won't be able to recover this post anymore!")
                        .setConfirmText("Yes,delete it!")
                        .setCancelText("No,cancel!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                    create_delete_timeline();

                            }
                        })
                        .show();


                return true;
            case R.id.menu1:
                // do your code
                return true;
            case R.id.menu2:
                // do your code
                return true;
            case R.id.menu3:
                // do your code
                return true;
            case R.id.menu4:
                // do your code
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                Popmenu_activity.this.startActivity(Intent.createChooser(i, "Share via"));
                return true;

            default:
                return false;
        }

    }


    private void create_delete_timeline() {

      /*  ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            String createdby = Pref_storage.getDetail(Popmenu_activity.this,"userId");
            Call<CreateUserPojoOutput> call = apiService.create_delete_timeline("create_delete_timeline", 66, Integer.parseInt(createdby));
            call.enqueue(new Callback<CreateUserPojoOutput>() {
                @Override
                public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {
                    if (response.body().getResponseCode() == 1) {
                        new SweetAlertDialog(Popmenu_activity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Deleted!!")
                                .setContentText("Your post has been deleted.")

                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        Popmenu_activity.this.startActivity(new Intent(Popmenu_activity.this, Home.class));
                                        ((AppCompatActivity) Popmenu_activity.this).finish();


                                    }
                                })
                                .show();


                    }

                }

                @Override
                public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

*/
    }

}