package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.io.File;
import java.util.Arrays;

/**
 * Created by vishnukm on 9/3/18.
 */

public class Fullimage_activity extends AppCompatActivity implements View.OnClickListener {
    ImageView imageView_gallery;

    Toolbar actionBar;
    ImageButton back;
    TextView next;

    private String[] FilePathStrings;
    private String[] FileNameStrings;
    private File[] listFile;
    File file;
    String path;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_full_image);
        imageView_gallery = (ImageView) findViewById(R.id.full_image_view);

        actionBar = (Toolbar) findViewById(R.id.action_bar_post_gallery);

        back = (ImageButton) actionBar.findViewById(R.id.button_back);
        back.setOnClickListener(this);


        next = (TextView) actionBar.findViewById(R.id.toolbar_text);
        next.setOnClickListener(this);


        Intent i = getIntent();

        // Get the position
        int position = i.getExtras().getInt("position");

        // Get String arrays FilePathStrings
        String[] filepath = i.getStringArrayExtra("filepath");

        try {
            if (filepath[position] == null || filepath[position].equalsIgnoreCase("")) {
                Pref_storage.setDetail(this, "path", "");
            } else {
                path = filepath[position].replace("[", "").replace("]", "");
                Pref_storage.setDetail(this, "path", path);
            }
        }catch (Exception e){
            e.printStackTrace();
        }





//        setImage();

//        String image = Pref_storage.getDetail(this, "imagefromfragment");


        String imageCamera = Pref_storage.getDetail(this, "UserImage_post");



        if (path == null||path.equalsIgnoreCase("")) {
            Uri uri = Uri.parse(imageCamera);
            imageView_gallery.setImageURI(uri);
        } else if (imageCamera==null||imageCamera.equalsIgnoreCase("")){
            Uri uri = Uri.parse(path);
            imageView_gallery.setImageURI(uri);
        }





    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            case R.id.toolbar_text:

                Intent i = new Intent(getApplicationContext(), newsfeedPost.class);
                // Pass String arrays FilePathStrings
                i.putExtra("filepath", FilePathStrings);

                String path = Arrays.toString(FilePathStrings);

                // Pass String arrays FileNameStrings
                i.putExtra("filename", FileNameStrings);
                // Pass click position

                startActivity(i);

//                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                break;


            case R.id.button_back:
                finish();

                break;


        }


    }

    private void setImage() {
        Intent intent = getIntent();


        if (intent.hasExtra(getString(R.string.selected_bitmap))) {
            Uri uri = (Uri) intent.getParcelableExtra(getString(R.string.selected_bitmap));
            imageView_gallery.setImageURI(uri);
        }
      /*  else if(intent.hasExtra(getString(R.string.selected_bitmap))){
            Bitmap bitmap = (Bitmap) intent.getParcelableExtra(getString(R.string.selected_bitmap));
            imageView_gallery.setImageBitmap(bitmap);
        }*/
    }

}