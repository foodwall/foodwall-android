package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_stories_pojo_output {

    @SerializedName("methodName")
    @Expose
    private String methodName;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Data")
    @Expose
    private List<Get_all_stories_pojo> data = null;
    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("ResponseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("custom")
    @Expose
    private String custom;
    @SerializedName("noofrow")
    @Expose
    private Integer noofrow;

    public Get_stories_pojo_output(String methodName, Integer status, List<Get_all_stories_pojo> data, Integer responseCode, String responseMessage, String custom, Integer noofrow) {
        this.methodName = methodName;
        this.status = status;
        this.data = data;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.custom = custom;
        this.noofrow = noofrow;
    }

    public Integer getNoofrow() {
        return noofrow;
    }

    public void setNoofrow(Integer noofrow) {
        this.noofrow = noofrow;
    }

    public Get_stories_pojo_output(String methodName, Integer status, List<Get_all_stories_pojo> data, Integer responseCode, String responseMessage, String custom) {
        super();
        this.methodName = methodName;
        this.status = status;
        this.data = data;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.custom = custom;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Get_all_stories_pojo> getData() {
        return data;
    }

    public void setData(List<Get_all_stories_pojo> data) {
        this.data = data;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }


}
