package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.REST_API.ProgressRequestBody;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Create_story_output_pojo;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Utility;

import java.io.File;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class editStoriesActivity extends AppCompatActivity implements View.OnClickListener,
        ProgressRequestBody.UploadCallbacks {

    /*
     * Application TAG*/

    public String TAG = "editStoriesActivity";
    //Image
    ImageView img_story;
    //Toolbar
    android.support.v7.widget.Toolbar toolbar;
    //ImageButton
    ImageButton close;
    //Edit text
    EditText edt_story_descr;
    //RelativeLayout
    RelativeLayout rl_story_post;

    //TextView
    TextView tv_story_location;
    TextView story_share;

    //String
    String storyDesrResult;
    String file_camera;
    String file_gallery;
    String photo_upload;
    String edit_image;
    String edit_story_description;
    String edit_story_id;
    String edit_createdby;

    //Progressbaredit_story_description
    ProgressBar storyUploadProgress;

    //File
    File sourceFile;

    //FrameLayout
    FrameLayout frameStory;

    MultipartBody.Part image;
    RequestBody imageupload;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_full_image);

        /*Widget initialization*/

        img_story = findViewById(R.id.img_story);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        edt_story_descr = findViewById(R.id.edt_story_descr);

        tv_story_location = findViewById(R.id.tv_story_location);

        tv_story_location.setOnClickListener(this);

        story_share = findViewById(R.id.story_share);

        story_share.setOnClickListener(this);

        tv_story_location.setOnClickListener(this);

        rl_story_post = findViewById(R.id.rl_story_post);

        /*Loader*/
        storyUploadProgress = findViewById(R.id.story_upload_progress);

        /*Getting input values*/
        Intent intent = getIntent();
        edit_image = intent.getStringExtra("edit_image");
        edit_story_description = intent.getStringExtra("edit_story_description");
        edit_story_id = intent.getStringExtra("edit_story_id");
        edit_createdby = intent.getStringExtra("edit_createdby");


        if (edit_image == null) {
            file_camera = intent.getStringExtra("file_camera");
            file_gallery = intent.getStringExtra("file_gallery");
            edit_story_id = intent.getStringExtra("story_id_photo");
            edit_story_description = intent.getStringExtra("edit_story_description");
            edit_createdby = intent.getStringExtra("edit_createdby");

            if (file_camera == null) {
                img_story.setImageURI(Uri.parse(file_gallery));
                edt_story_descr.setText(edit_story_description);
            } else if (file_gallery == null) {
                img_story.setImageURI(Uri.parse(file_camera));
                edt_story_descr.setText(edit_story_description);
            }
        } else {
            edt_story_descr.setText(edit_story_description);

            GlideApp.with(editStoriesActivity.this).load(edit_image).diskCacheStrategy(DiskCacheStrategy.ALL).thumbnail(0.04f).
                    fitCenter().into(img_story);

        }


    }

    @Override
    public void onClick(View v) {

        int click = v.getId();

        switch (click) {
            case R.id.close:
                finish();
                break;

            case R.id.story_edit_text:
                edt_story_descr.setFocusable(true);

                break;
            case R.id.tv_story_location:
                Intent intent = new Intent(editStoriesActivity.this, storiesEditTakePhoto.class);
                intent.putExtra("from_edit_story_id", edit_story_id);
                intent.putExtra("from_edit_story_description", edit_story_description);
                intent.putExtra("from_edit_createdby", edit_createdby);
                startActivity(intent);

                break;

            case R.id.story_share:
                storyUploadProgress.setVisibility(View.VISIBLE);
                storyDesrResult = edt_story_descr.getText().toString();

                if (file_camera == null && file_gallery == null) {
                    photo_upload = edit_image;

                    editStories();

                } else if (file_camera == null) {
                    photo_upload = file_gallery;

                    editStories();

                } else if (file_gallery == null) {
                    photo_upload = file_camera;

                    editStories();
                }


                break;

            default:
                break;
        }

    }


    //API to edit upload story


    private void editStories() {

        if (Utility.isConnected(getApplicationContext())) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                sourceFile = new File(photo_upload);


                Log.e("sourceFile_story", "" + sourceFile.getName());


                if (file_gallery == null && file_camera == null) {

                    File compressedImageFile = new Compressor(this).setQuality(100).compressToFile(sourceFile);

                    ProgressRequestBody fileBody = new ProgressRequestBody(compressedImageFile, this);

                    image = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), fileBody);

                    /*imageupload = RequestBody.create(MediaType.parse("image/jpeg"), "");
                    image = MultipartBody.Part.createFormData("image", "", imageupload);*/
                } else {

                    File compressedImageFile2 = new Compressor(this).setQuality(100).compressToFile(sourceFile);

                    ProgressRequestBody fileBody1 = new ProgressRequestBody(compressedImageFile2, this);

                    image = MultipartBody.Part.createFormData("image", compressedImageFile2.getName(), fileBody1);

                   /* imageupload = RequestBody.create(MediaType.parse("image/jpeg"), sourceFile);
                    image = MultipartBody.Part.createFormData("image", sourceFile.getName(), imageupload);*/
                    edit_image = "";
                }

                RequestBody created_by = RequestBody.create(MediaType.parse("text/plain"), edit_createdby);

                RequestBody story_description = RequestBody.create(MediaType.parse("text/plain"), storyDesrResult);
                Call<Create_story_output_pojo> call = apiService.edit_stories("create_stories", Integer.parseInt(edit_story_id), story_description, image, edit_image, created_by);
                call.enqueue(new Callback<Create_story_output_pojo>() {
                    @Override
                    public void onResponse(Call<Create_story_output_pojo> call, Response<Create_story_output_pojo> response) {
                        if (response.body().getResponseCode() == 1) {
                            storyUploadProgress.setVisibility(View.GONE);
                            startActivity(new Intent(getApplicationContext(), Home.class));
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<Create_story_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Snackbar snackbar = Snackbar.make(frameStory, "Uploading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    @Override
    public void onProgressUpdate(int percentage) {

        Log.e(TAG, "onProgressUpdate: " + percentage + "%");

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
