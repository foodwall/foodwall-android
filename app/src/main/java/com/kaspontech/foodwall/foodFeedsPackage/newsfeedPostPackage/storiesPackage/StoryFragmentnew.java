package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.adapters.TimeLine.storyViewAdapter;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Create_story_output_pojo;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Get_all_stories_pojo;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Get_stories_pojo_output;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.StoriesPojo;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.storyPojo.storyViewInputPojo;
import com.kaspontech.foodwall.modelclasses.storyPojo.storyViewPojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.storylibrary.StoriesProgressView;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.OnSwipeTouchListener;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoryFragmentnew extends Fragment implements
        StoriesProgressView.StoriesListener,
        View.OnClickListener
        /* View.OnTouchListener,*/
        /*  View.OnLongClickListener*/ {

    /*
     * TAG
     * */
    private static final String TAG = "StoryFragment";

    /*
     * Action bar story
     * */
    Toolbar actionbarStory;

    /*
     * Back button
     * */
    ImageButton back;
    /*
     * Image delete story
     * */
    ImageButton imgDeleteStory;
    /*
     * Circle Image view
     * */
    CircleImageView imgStoriesUser;
    /*
     * Textview
     * */
    TextView txtStoryUsername;

    TextView txtStoryAgo;

    TextView txtStoryView;
    /*
     * Image view for story
     * */
    ImageView imgStory;
    /*
     * RelativeLayout for story caption
     * */
    RelativeLayout rlStoryCaption;

    RelativeLayout frameLayout;

    /*
     * Emoji edit text
     * */

    EmojiconTextView txtDescrip, txt_hotelname;

    LinearLayout llStoryOverall;

    View view;

    boolean pause = true;

    /*
     * Story progress view
     * */

    private StoriesProgressView storiesProgressView;

    long pressTime = 0L;
    long limit = 1500L;

    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();

                    return false;

                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
                    storiesProgressView.resume();

                    return limit < now - pressTime;


            }

            return false;
        }

    };


    private int previousFingerPosition = 0;
    private int img_storyPosition = 0;
    private int defaultViewHeight;

    private boolean isClosing = false;
    private boolean isScrollingUp = false;
    private boolean isScrollingDown = false;

    List<Get_all_stories_pojo> stories_image_list = new ArrayList<>();
    StoriesPojo storiesPojo;
    List<StoriesPojo> storiesPojoList = new ArrayList<>();
    Get_all_stories_pojo get_all_stories_pojo;
    ArrayList<storyViewInputPojo> storiesViewedList = new ArrayList<>();

     List<StoriesPojo> storiesPojoList1 = new ArrayList<>();

    String userType;
    ProgressBar storyProgress;
    private int counter = 0;
    private int position = 0, storiesCount = 0;
    private String story_id, created_by;




    /*Action bar*/


    Toolbar actionBar;

    /* back button*/

    ImageButton backView;


    /*likes Relative layout*/

    RelativeLayout rl_viewes;


    /*Recyclerview for likes*/

    RecyclerView rv_viewes;


    /* linearlayout manager
     */
    LinearLayoutManager llm;

    /* likes adapter
     */
    storyViewAdapter storyViewAdapter;


    /* Likes model class
     */

    storyViewInputPojo storyViewInputPojo;


    /* Loaders
     */
    ProgressBar progress_dialog_reply;


    /* Title text views
     */
    TextView toolbar_next;

    TextView toolbar_title;


    OnStoryViewedListener onStoryViewedListener;

    public void setOnStoryViewedListener(OnStoryViewedListener onStoryViewedListener) {
        this.onStoryViewedListener = onStoryViewedListener;
    }

    // Container Activity must implement this interface
    public interface OnStoryViewedListener {
        void onStoryCompleted(int position);
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_stories, container, false);
        /*Widget initialization*/

        storiesProgressView = rootView.findViewById(R.id.stories);
        storyProgress = rootView.findViewById(R.id.story_progress);
        actionbarStory = rootView.findViewById(R.id.actionbar_story);
        actionbarStory.setOnClickListener(this);
        back = actionbarStory.findViewById(R.id.back);
        imgDeleteStory = actionbarStory.findViewById(R.id.img_delete_story);
        imgStoriesUser = actionbarStory.findViewById(R.id.img_stories_user);
        txtStoryUsername = actionbarStory.findViewById(R.id.txt_story_username);
        txtStoryAgo = actionbarStory.findViewById(R.id.txt_story_ago);
        back.setOnClickListener(this);
        imgDeleteStory.setOnClickListener(this);

        imgStory = rootView.findViewById(R.id.img_story);
        rlStoryCaption = rootView.findViewById(R.id.rl_story_caption);
        llStoryOverall = rootView.findViewById(R.id.ll_story_overall);

        frameLayout = rootView.findViewById(R.id.frame_layout);
        txtDescrip = rootView.findViewById(R.id.txt_descrip);
        txt_hotelname = rootView.findViewById(R.id.txt_hotelname);
        txtStoryView = rootView.findViewById(R.id.txtStoryView);
        txtStoryView.setOnClickListener(this);
        View reverse = rootView.findViewById(R.id.reverse);
        View skip = rootView.findViewById(R.id.skip);
        View middle = rootView.findViewById(R.id.middle);

        storiesProgressView.setStoriesListener(this); // <- set listener


        /*Reverse tap listener*/
        reverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                storiesProgressView.reverse();

            }
        });

//        reverse.setOnTouchListener(onTouchListener);

        /*Skip/Forward tap listener*/

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                storiesProgressView.skip();


            }
        });

        /*Centere tap listener*/
        middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pause) {

                    storiesProgressView.pause();
                    pause = false;

                } else {

                    storiesProgressView.resume();
                    pause = false;

                }


            }
        });

        middle.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                storiesProgressView.pause();

                return false;
            }
        });

//        skip.setOnTouchListener(onTouchListener);

        middle.setOnTouchListener(onTouchListener);


//        /// Middle touch listeners
//
//        middle.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
//            public void onSwipeTop() {
//                getActivity().finish();
//
//
//
//            }
//            public void onSwipeRight() {
//            }
//            public void onSwipeLeft() {
//
//            }
//            public void onSwipeBottom() {
//
//                getActivity().finish();
//
//            }
//
//        });


        /// Skip touch listeners

        skip.setOnTouchListener(new OnSwipeTouchListener(getContext()) {

            public void onSwipeTop() {

//                getActivity().finish();

            }

            public void onSwipeRight() {

                storiesProgressView.reverse();
            }

            public void onSwipeLeft() {


                storiesProgressView.skip();

            }

            public void onSwipeBottom() {

                getActivity().finish();

            }

        });


        skip.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                switch (event.getAction()) {

                    case MotionEvent.ACTION_BUTTON_PRESS:

                        storiesProgressView.skip();

                        return false;


                    case MotionEvent.ACTION_POINTER_DOWN:

                        getActivity().finish();

                        return false;

                }

                return false;

            }
        });


        middle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                switch (event.getAction()) {

                    case MotionEvent.ACTION_POINTER_DOWN:

                        getActivity().finish();
                        return false;

                }

                return false;

            }
        });


        /// Reverse touch listeners

 /*       reverse.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                switch (event.getAction()) {

                    case MotionEvent.ACTION_BUTTON_PRESS:
//                        pressTime = System.currentTimeMillis();
                        storiesProgressView.reverse();

                        return false;

                    case MotionEvent.ACTION_POINTER_DOWN:
//                        pressTime = System.currentTimeMillis();
                        getActivity().finish();
                        return false;


                }

                return false;

            }
        });
*/

        reverse.setOnTouchListener(new OnSwipeTouchListener(getContext()) {

                                       @Override
                                       public void onClick() {
                                           super.onClick();
                                           Log.e(TAG, "onClick: ");
                                           // your on click here
                                           storiesProgressView.reverse();

                                       }

                                       @Override
                                       public void onDoubleClick() {
                                           super.onDoubleClick();
                                           Log.e(TAG, "onDoubleClick: ");

                                           // your on onDoubleClick here
                                       }

                                       @Override
                                       public void onLongClick() {
                                           super.onLongClick();
                                           Log.e(TAG, "onLongClick: ");
                                           // your on onLongClick here
                                       }

                                       @Override
                                       public void onSwipeUp() {
                                           super.onSwipeUp();
                                           // your swipe up here
                                           Log.e(TAG, "onSwipeUp: ");
                                       }

                                       @Override
                                       public void onSwipeDown() {
                                           super.onSwipeDown();
                                           Log.e(TAG, "onSwipeDown: ");
                                           getActivity().finish();
                                           // your swipe down here.
                                       }

                                       @Override
                                       public void onSwipeLeft() {
                                           super.onSwipeLeft();

                                           storiesProgressView.skip();

                                           Log.e(TAG, "onSwipeLeft: ");
                                           // your swipe left here.
                                       }

                                       @Override
                                       public void onSwipeRight() {
                                           super.onSwipeRight();
                                           Log.e(TAG, "onSwipeRight: ");
                                           storiesProgressView.reverse();

                                           // your swipe right here.
                                       }
                                   }
        );



        /*Skip on touch listener*/
        skip.setOnTouchListener(new OnSwipeTouchListener(getContext()) {

                                    @Override
                                    public void onClick() {
                                        super.onClick();
                                        Log.e(TAG, "onClick: ");

                                        storiesProgressView.skip();

                                        // your on click here
                                    }

                                    @Override
                                    public void onDoubleClick() {
                                        super.onDoubleClick();
                                        Log.e(TAG, "onDoubleClick: ");

                                        // your on onDoubleClick here
                                    }

                                    @Override
                                    public void onLongClick() {
                                        super.onLongClick();
                                        Log.e(TAG, "onLongClick: ");
                                        // your on onLongClick here
                                    }

                                    @Override
                                    public void onSwipeUp() {
                                        super.onSwipeUp();
                                        // your swipe up here
                                        Log.e(TAG, "onSwipeUp: ");
                                    }

                                    @Override
                                    public void onSwipeDown() {
                                        super.onSwipeDown();
                                        Log.e(TAG, "onSwipeDown: ");
                                        getActivity().finish();

                                        // your swipe down here.
                                    }

                                    @Override
                                    public void onSwipeLeft() {
                                        super.onSwipeLeft();

                                        storiesProgressView.reverse();

                                        Log.e(TAG, "onSwipeLeft: ");
                                        // your swipe left here.
                                    }

                                    @Override
                                    public void onSwipeRight() {
                                        super.onSwipeRight();
                                        Log.e(TAG, "onSwipeRight: ");
                                        storiesProgressView.skip();

                                        // your swipe right here.
                                    }
                                }
        );

        String pos = getArguments().getString("position");
        userType = getArguments().getString("userType");


        if (pos != null) {

            position = Integer.parseInt(pos);

        }


        Log.e("UberEats", "onCreateView: " + position);


        if (userType != null) {

            if (userType.equals("0")) {
                /*Stories self api call*/

                //getStoriesSelf();


                imgDeleteStory.setVisibility(View.VISIBLE);


            } else {

                /*Stories all api calling*/
                getStoriesAll();

            }

        }


        return rootView;


    }


    public void updatePageIndex(int index) {
        position = index;
    }


    @Override
    public void onStoryStart() {


        storyProgress.setVisibility(View.VISIBLE);
        storyProgress.setIndeterminate(true);
//        storiesProgressView.pause();

        // User profile

        String userName = stories_image_list.get(position).getFirstName() + " " + stories_image_list.get(position).getLastName();
        String userImage = stories_image_list.get(position).getPicture();
        String createdOn = stories_image_list.get(position).getStories().get(0).getCreatedOn();

        GlideApp.with(Objects.requireNonNull(getActivity()))
                .load(userImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(imgStoriesUser);

        txtStoryUsername.setText(userName);

        try {
            Utility.setTimeStamp(createdOn, txtStoryAgo);
        } catch (Exception e) {
            e.printStackTrace();
        }


        String storyCaption = org.apache.commons.text.StringEscapeUtils.unescapeJava(stories_image_list.get(position).getStories().get(0).getStoriesDescription());

        if (storyCaption.equalsIgnoreCase("0")) {
            txtDescrip.setText("");
            rlStoryCaption.setVisibility(View.GONE);
        } else {
            txtDescrip.setText(storyCaption);
        }


        String hotelname = org.apache.commons.text.StringEscapeUtils.unescapeJava(stories_image_list.get(position).getStories().get(0).getAddress());

        if (storyCaption.equalsIgnoreCase("0") || stories_image_list.get(position).getStories().get(0).getAddress().equalsIgnoreCase("Address")) {
            txt_hotelname.setText("");
            // rlStoryCaption.setVisibility(View.GONE);
        } else {
            txt_hotelname.setText(hotelname);
        }

        String image = stories_image_list.get(position).getStories().get(0).getStoriesImage();

        /// Load story images

        GlideApp.with(getActivity()).load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        storyProgress.setVisibility(View.GONE);
//                        storiesProgressView.resume();

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        storyProgress.setVisibility(View.GONE);
//                        storiesProgressView.resume();
                        return false;
                    }
                })
                .into(imgStory);


    }

    @Override
    public void onNext() {

        storyProgress.setVisibility(View.VISIBLE);
        storyProgress.setIndeterminate(true);
//      storiesProgressView.pause();

        int count = ++counter;

        Log.e(TAG, "onNext:" + counter);

        /*int storyId = Integer.parseInt(stories_image_list.get(position).getStories().get(count).getStoriesId());
        createStoriesView(storyId);*/

        // Username

        String userName = stories_image_list.get(position).getFirstName() + " " + stories_image_list.get(position).getLastName();
        txtStoryUsername.setText(userName);

        story_id = stories_image_list.get(position).getStories().get(count).getStoriesId();

        // UserImage

        String userImage = stories_image_list.get(position).getPicture();

        GlideApp.with(StoryFragmentnew.this)
                .load(userImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)
                .into(imgStoriesUser);

        // Created On
        String createdOn = stories_image_list.get(position).getStories().get(count).getCreatedOn();

        try {

            Utility.setTimeStamp(createdOn, txtStoryAgo);

        } catch (Exception e) {
            e.printStackTrace();
        }


        // Story Caption
        String storyCaption = stories_image_list.get(position).getStories().get(count).getStoriesDescription();

        storyCaption = org.apache.commons.text.StringEscapeUtils.unescapeJava(storyCaption);

        if (storyCaption.equalsIgnoreCase("0")) {

            txtDescrip.setText("");
            txtDescrip.setVisibility(View.GONE);
            rlStoryCaption.setVisibility(View.GONE);

        } else {

            txtDescrip.setText(storyCaption);
            txtDescrip.setVisibility(View.VISIBLE);

        }


        // Story Image
        String image = stories_image_list.get(position).getStories().get(count).getAddress();

        storyCaption = org.apache.commons.text.StringEscapeUtils.unescapeJava(storyCaption);

        if (storyCaption.equalsIgnoreCase("0") || image.equalsIgnoreCase("Address")) {

            txt_hotelname.setText("");
            // txt_hotelname.setVisibility(View.GONE);
            //rlStoryCaption.setVisibility(View.GONE);

        } else {

            txt_hotelname.setText(image);
            // txt_hotelname.setVisibility(View.VISIBLE);

        }
        String imageurl = stories_image_list.get(position).getStories().get(count).getStoriesImage();

        GlideApp.with(this)
                .load(imageurl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                storyProgress.setVisibility(View.GONE);

                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                storyProgress.setVisibility(View.GONE);

                return false;
            }
        })
                .into(imgStory);


    }

    @Override
    public void onPrev() {

        storyProgress.setVisibility(View.VISIBLE);
        storyProgress.setIndeterminate(true);
//        storiesProgressView.pause();


//        Log.e(TAG, "onPrev: called");

        if ((counter - 1) < 0) return;

        int count = --counter;


        // Username
        String userName = stories_image_list.get(position).getFirstName() + " " + stories_image_list.get(position).getLastName();
        txtStoryUsername.setText(userName);
        story_id = stories_image_list.get(position).getStories().get(count).getStoriesId();

        // UserImage
        String userImage = stories_image_list.get(position).getPicture();

        GlideApp.with(StoryFragmentnew.this)
                .load(userImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)
                .into(imgStoriesUser);

        // Created On
        String createdOn = stories_image_list.get(position).getStories().get(count).getCreatedOn();


        try {

            Utility.setTimeStamp(createdOn, txtStoryAgo);

        } catch (Exception e) {

            e.printStackTrace();
        }


        // Story Caption
        String storyCaption = stories_image_list.get(position).getStories().get(count).getStoriesDescription();
        storyCaption = org.apache.commons.text.StringEscapeUtils.unescapeJava(storyCaption);

        if (storyCaption.equalsIgnoreCase("0")) {
            txtDescrip.setText("");
            rlStoryCaption.setVisibility(View.GONE);

        } else {
            txtDescrip.setText(storyCaption);
        }

        // Story Image
        String image = stories_image_list.get(position).getStories().get(count).getStoriesImage();
        storyCaption = org.apache.commons.text.StringEscapeUtils.unescapeJava(storyCaption);

        if (storyCaption.equalsIgnoreCase("0") || image.equalsIgnoreCase("Address")) {
            txt_hotelname.setText("");
            // rlStoryCaption.setVisibility(View.GONE);

        } else {
            txt_hotelname.setText(image);
        }

        GlideApp.with(this)
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                storyProgress.setVisibility(View.GONE);
//                storiesProgressView.resume();

                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                storyProgress.setVisibility(View.GONE);
//                storiesProgressView.resume();

                return false;
            }
        })
                .into(imgStory);


    }


    @Override
    public void onComplete() {


        if (position == stories_image_list.size() - 1) {

            getActivity().finish();
           /* Intent intent=new Intent(getActivity(),Home.class);
            startActivity(intent);
*/

        } else {

            onStoryViewedListener.onStoryCompleted(position);

        }


    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {

            try {

                if (storiesProgressView != null) {
                    counter = 0;
                    int storiesCount = stories_image_list.get(position).getStoriesCount();
                    storiesProgressView.setStoriesCount(storiesCount);
//                    storiesProgressView.setStoryDuration(3500L);
                    storiesProgressView.setStoryDuration(10000L);
                    storiesProgressView.startStories();
                }

            } catch (Exception e) {


            }


        } else {


        }


    }

    @Override
    public void onDestroy() {
        storiesProgressView.destroy();
        super.onDestroy();

    }


    /*Stories all api calling*/

    private void getStoriesAll() {


        if (isAdded()) {


            if (Utility.isConnected(getActivity())) {

                stories_image_list.clear();
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                final String createuserid = Pref_storage.getDetail(getActivity(), "userId");

                Call<Get_stories_pojo_output> call = apiService.get_stories_all("get_stories_whole_users", Integer.parseInt(createuserid));

                call.enqueue(new Callback<Get_stories_pojo_output>() {
                    @Override
                    public void onResponse(@NonNull Call<Get_stories_pojo_output> call, @NonNull Response<Get_stories_pojo_output> response) {


                        for(int i=0;i<response.body().getData().size();i++) {

                            if (response.body() != null && response.body().getResponseCode() == 1) {

                                actionbarStory.setVisibility(View.VISIBLE);


                                int storycount=response.body().getData().get(i).getStoriesCount();
                                int viewedCount=response.body().getData().get(i).getViewedCount();
                                if(storycount==viewedCount ) {
                                    stories_image_list = response.body().getData();
                                    try {

                                        int storiesCount = stories_image_list.get(position).getStoriesCount();
                                        storiesProgressView.setStoriesCount(storiesCount);
                                        storiesProgressView.setStoryDuration(10000L);
                                        storiesProgressView.startStories();

                                        Log.e(TAG, "onResponse: stories_image_list"+stories_image_list);

                                    } catch (Exception e) {

                                        Log.e(TAG, "onResponse: Exception in response");
                                    }


                                }else {
                                    storiesPojoList.clear();

                                    String strId=response.body().getData().get(i).getStrId();
                                    String storiesImage=response.body().getData().get(i).getStoriesImage();
                                    String storiesDescription=response.body().getData().get(i).getStoriesDescription();
                                    String address=response.body().getData().get(i).getAddress();
                                    String area=response.body().getData().get(i).getArea();
                                    String city=response.body().getData().get(i).getCity();
                                    String createdBy=response.body().getData().get(i).getCreatedBy();
                                    String createdOn=response.body().getData().get(i).getCreatedOn();
                                    String userId=response.body().getData().get(i).getUserId();
                                    String firstName=response.body().getData().get(i).getFirstName();
                                    String lastName=response.body().getData().get(i).getLastName();
                                    String picture=response.body().getData().get(i).getPicture();
                                    String storiesView=response.body().getData().get(i).getStoriesView();
                                    int storiesCount1=response.body().getData().get(i).getStoriesCount();


                                     for(int j=0;j<response.body().getData().get(i).getStories().size();j++)
                                     {

                                             String storiesId=response.body().getData().get(i).getStories().get(j).getStoriesId();
                                             String storiesDescription1=response.body().getData().get(i).getStories().get(j).getStoriesDescription();
                                             String storiesImage1=response.body().getData().get(i).getStories().get(j).getStoriesImage();
                                             String address1=response.body().getData().get(i).getStories().get(j).getAddress();
                                             String city1=response.body().getData().get(i).getStories().get(j).getCity();
                                             String area1=response.body().getData().get(i).getStories().get(j).getArea();
                                             String createdBy1=response.body().getData().get(i).getStories().get(j).getCreatedBy();
                                             String createdOn1=response.body().getData().get(i).getStories().get(j).getCreatedOn();
                                             String totalView=response.body().getData().get(i).getStories().get(j).getTotalView();
                                             String firstName1=response.body().getData().get(i).getStories().get(j).getFirstName();
                                             String lastName1=response.body().getData().get(i).getStories().get(j).getLastName();
                                             String picture1=response.body().getData().get(i).getStories().get(j).getPicture();
                                             String storiesInrView=response.body().getData().get(i).getStories().get(j).getStoriesInrView();

                                             storiesPojo=new StoriesPojo(storiesId,storiesDescription1,storiesImage1,address1,city1,area1,createdBy1,createdOn1,totalView,firstName1,lastName1,picture1,storiesInrView);

                                             if(storiesInrView.equalsIgnoreCase("0")) {
                                                storiesPojoList.add(storiesPojo);


                                                 get_all_stories_pojo = new Get_all_stories_pojo(strId, storiesImage, storiesDescription, address, area, city, createdBy, createdOn, userId, firstName, lastName, picture, storiesView, storiesPojoList, viewedCount, storiesCount1);
                                                 stories_image_list.add(get_all_stories_pojo);
                                                 Log.e(TAG, "onResponse: vggstories_image_list"+stories_image_list);

                                                 try {

                                                     int storiesCount = stories_image_list.get(position).getViewedCount();
                                                     storiesProgressView.setStoriesCount(storiesCount);
                                                     storiesProgressView.setStoryDuration(10000L);
                                                     storiesProgressView.startStories();

                                                 } catch (Exception e) {

                                                     Log.e(TAG, "onResponse: Exception in response");
                                                 }

                                             }
                                     }


                                }


                                //stories_image_list = response.body().getData();


    /*
                                // User profile

                                String userName = stories_image_list.get(position).getFirstName() + " " + stories_image_list.get(position).getLastName();
                                String userImage = stories_image_list.get(position).getPicture();
                                String createdOn = stories_image_list.get(position).getStories().get(0).getCreatedOn();

                                if (isAdded()) {
                                    GlideApp.with(StoryFragment.this)
                                            .load(userImage)
                                            .placeholder(R.drawable.ic_add_photo)
                                            .into(imgStoriesUser);
                                }


                                txtStoryUsername.setText(userName);

                                try {

                                    //Input time
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    long time = sdf.parse(createdOn).getTime();

                                    //Current system time

                                    Date currentTime = Calendar.getInstance().getTime();
                                    SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    String now_new = sdformat.format(currentTime);
                                    long nowdate = sdformat.parse(now_new).getTime();


                                    CharSequence ago = DateUtils.getRelativeTimeSpanString(time, nowdate, DateUtils.FORMAT_ABBREV_RELATIVE);

                                    if (ago.toString().equalsIgnoreCase("0 minutes ago") || ago.toString().equalsIgnoreCase("In 0 minutes")) {
                                        txtStoryAgo.setText(R.string.just_now);
                                    } else {
                                        txtStoryAgo.setText(ago);
                                    }


                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                                String story_caption = org.apache.commons.text.StringEscapeUtils.unescapeJava(stories_image_list.get(position).getStories().get(0).getStoriesDescription());

                                if (story_caption.equalsIgnoreCase("0")) {
                                    txtDescrip.setText("");
                                } else {
                                    txtDescrip.setText(story_caption);
                                }


                                String image = response.body().getData().get(position).getStories().get(0).getStoriesImage();
                                storiesCount = response.body().getData().get(position).getStoriesCount();
    */


                                int storyId = Integer.parseInt(stories_image_list.get(position).getStories().get(0).getStoriesId());


                                /*Creating story view update*/

                                try {
                                    createStoriesView(storyId);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }

                        }


                    }

                    @Override
                    public void onFailure(Call<Get_stories_pojo_output> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });


            } else {

            }

        }


    }

    private void getStoriesView() {


        /* Arraylist for likes
         */
        final BottomSheetDialog dialog;

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.activity_likes_layout, null);
        dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        /*Widget initialization*/
        actionBar = (Toolbar) dialog.findViewById(R.id.actionbar_like);
        backView = (ImageButton) actionBar.findViewById(R.id.back);
        backView.setOnClickListener(this);


        toolbar_next = (TextView) actionBar.findViewById(R.id.toolbar_next);
        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);
        toolbar_title.setVisibility(View.VISIBLE);
        toolbar_title.setText("Views");
        toolbar_title.setGravity(View.TEXT_ALIGNMENT_CENTER);
        toolbar_next.setVisibility(View.GONE);

        rl_viewes = (RelativeLayout) dialog.findViewById(R.id.rl_likes);
        rv_viewes = (RecyclerView) dialog.findViewById(R.id.rv_likes);
        progress_dialog_reply = (ProgressBar) dialog.findViewById(R.id.progress_dialog_reply);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        /*Loader on*/


        progress_dialog_reply.setIndeterminate(true);


        if (isAdded()) {

            if (Utility.isConnected(getActivity())) {

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                final String createuserid = Pref_storage.getDetail(getActivity(), "userId");
                Call<storyViewPojo> call = apiService.getStoriesView("get_stories_view_user",
                        Integer.parseInt(createuserid),
                        Integer.parseInt(story_id));
                Log.i(TAG, "getStoriesView: createuserid" + Integer.parseInt(createuserid));
                Log.i(TAG, "getStoriesView: story_id" + Integer.parseInt(story_id));

                call.enqueue(new Callback<storyViewPojo>() {
                    @Override
                    public void onResponse(@NonNull Call<storyViewPojo> call, @NonNull Response<storyViewPojo> response) {

                        if (response.body() != null) {

                            if (response.body().getResponseCode() == 1) {
                                storiesViewedList.clear();

                                progress_dialog_reply.setIndeterminate(false);
                                progress_dialog_reply.setVisibility(View.GONE);

//                                storiesViewedList = response.body().getData();

                                for (int a = 0; a < response.body().getData().size(); a++) {

                                    if (response.body().getData().get(a).getFirstName() == null) {

                                    } else {

                                        String firstName = response.body().getData().get(a).getFirstName();
                                        String lastName = response.body().getData().get(a).getLastName();
                                        String storiesId = response.body().getData().get(a).getStoriesId();
                                        String userId = response.body().getData().get(a).getUserId();
                                        String storiesDescription = response.body().getData().get(a).getStoriesDescription();
                                        String storiesImage = response.body().getData().get(a).getStoriesImage();
                                        String gender = response.body().getData().get(a).getGender();
                                        String picture = response.body().getData().get(a).getPicture();
                                        String totalView = response.body().getData().get(a).getTotalView();

                                        storyViewInputPojo storyViewInputPojo = new storyViewInputPojo(storiesId, totalView, userId, firstName, lastName, "",
                                                "", gender, "", picture, storiesDescription, "", "", "", storiesImage);

                                        storiesViewedList.add(storyViewInputPojo);
                                    }


                                }



                                /* Adapter initialization*/

                                storyViewAdapter = new storyViewAdapter(getActivity(), storiesViewedList);
                                llm = new LinearLayoutManager(getActivity());
                                rv_viewes.setLayoutManager(llm);
                                rv_viewes.setAdapter(storyViewAdapter);
                                rv_viewes.setItemAnimator(new DefaultItemAnimator());
                                rv_viewes.setNestedScrollingEnabled(false);

                                dialog.show();


                            }

                        }


                    }

                    @Override
                    public void onFailure(@NonNull Call<storyViewPojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        txtStoryView.setVisibility(View.GONE);

                        progress_dialog_reply.setIndeterminate(false);
                        progress_dialog_reply.setVisibility(View.GONE);

                    }
                });


            } else {

                Toast.makeText(getActivity(), "No internet connection!", Toast.LENGTH_SHORT).show();

            }


        }


    }

    /*Storiew view count displaying API*/

    private void getStoriesViewCount() {


        if (isAdded()) {

            if (Utility.isConnected(getActivity())) {

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                final String createuserid = Pref_storage.getDetail(getActivity(), "userId");
                Call<storyViewPojo> call = apiService.getStoriesView("get_stories_view_user",
                        Integer.parseInt(createuserid),
                        Integer.parseInt(story_id));

                call.enqueue(new Callback<storyViewPojo>() {
                    @Override
                    public void onResponse(@NonNull Call<storyViewPojo> call, @NonNull Response<storyViewPojo> response) {

                        if (response.body() != null) {

                            if (response.body().getResponseCode() == 1) {

                                if (!response.body().getData().isEmpty()) {

                                    txtStoryView.setVisibility(View.VISIBLE);
                                    txtStoryView.setText(String.valueOf(response.body().getData().size()));
                                }


                            }

                        }


                    }

                    @Override
                    public void onFailure(@NonNull Call<storyViewPojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        txtStoryView.setVisibility(View.GONE);

                        // progress_dialog_reply.setIndeterminate(false);
                        // progress_dialog_reply.setVisibility(View.GONE);

                    }
                });


            } else {

                Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();

            }


        }


    }


    /*Stories self api call*/
    private void getStoriesSelf() {

        if (isAdded()) {

            if (Utility.isConnected(getActivity())) {

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                final String createuserid = Pref_storage.getDetail(getActivity(), "userId");
                Call<Get_stories_pojo_output> call = apiService.get_stories_all("get_stories_self", Integer.parseInt(createuserid));

                call.enqueue(new Callback<Get_stories_pojo_output>() {
                    @Override
                    public void onResponse(@NonNull Call<Get_stories_pojo_output> call, @NonNull Response<Get_stories_pojo_output> response) {

                        if (response.body() != null) {

                            if (response.body().getResponseCode() == 1) {

//                                Log.e("story-->", "size" + response.body().getData().size());


                                actionbarStory.setVisibility(View.VISIBLE);
//                                Log.e("story-->", "size" + response.body().getData().size());

                                stories_image_list = response.body().getData();
//                                Collections.reverse(stories_image_list);

                                // User profile

                                String userName = stories_image_list.get(position).getFirstName() + " " + stories_image_list.get(position).getLastName();
                                String userImage = stories_image_list.get(position).getPicture();
                                String createdOn = stories_image_list.get(position).getStories().get(0).getCreatedOn();

                                GlideApp.with(StoryFragmentnew.this)
                                        .load(userImage)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .thumbnail(0.1f)
                                        .placeholder(R.drawable.ic_add_photo)
                                        .into(imgStoriesUser);

                                txtStoryUsername.setText(userName);

                                try {

                                    //Input time
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    long time = sdf.parse(createdOn).getTime();

                                    //Current system time

                                    Date currentTime = Calendar.getInstance().getTime();
                                    SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    String now_new = sdformat.format(currentTime);
                                    long nowdate = sdformat.parse(now_new).getTime();

//            String timeAgo = com.kaspontech.foodwall.Utills.TimeAgo.getTimeAgo_new(time,nowdate);


                                    CharSequence ago = DateUtils.getRelativeTimeSpanString(time, nowdate, DateUtils.FORMAT_ABBREV_RELATIVE);
                                    txtStoryAgo.setText(ago);


                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                                String story_caption = org.apache.commons.text.StringEscapeUtils.unescapeJava(stories_image_list.get(position).getStories().get(0).getStoriesDescription());

                                if (story_caption.equalsIgnoreCase("0")) {
                                    txtDescrip.setText("");
                                    rlStoryCaption.setVisibility(View.GONE);

                                } else {
                                    txtDescrip.setText(story_caption);
                                }

                                //display restaurant  name


                                String hotelname = org.apache.commons.text.StringEscapeUtils.unescapeJava(stories_image_list.get(position).getStories().get(0).getAddress());

                                if (story_caption.equalsIgnoreCase("0") || stories_image_list.get(position).getStories().get(0).getAddress().equalsIgnoreCase("Address")) {
                                    txt_hotelname.setText("");
                                    //   rlStoryCaption.setVisibility(View.GONE);

                                } else {
                                    txt_hotelname.setText(hotelname);
                                }


                                String image = response.body().getData().get(position).getStories().get(0).getStoriesImage();
                                int storiesCount = response.body().getData().get(position).getStoriesCount();


                                int selfCount = response.body().getData().get(position).getStories().size();


                                Log.e(TAG, "onResponse: selfCount" + selfCount);

//                                if(selfCount!=0){
//
//                                    txtStoryView.setVisibility(View.VISIBLE);
//                                    txtStoryView.setText(String.valueOf(selfCount));
//                                }

                                if (selfCount != 0) {

                                    txtStoryView.setVisibility(View.VISIBLE);
                                    txtStoryView.setText(String.valueOf(selfCount));
                                }
                                storiesProgressView.setStoriesCount(storiesCount);
                                storiesProgressView.setStoryDuration(10000L);
                                storiesProgressView.startStories();

                                GlideApp.with(StoryFragmentnew.this)
                                        .load(image)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .thumbnail(0.1f).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        storyProgress.setVisibility(View.GONE);
//                                        storiesProgressView.resume();

                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        storyProgress.setVisibility(View.GONE);
//                                        storiesProgressView.resume();

                                        return false;
                                    }
                                })
                                        .into(imgStory);

                                int storyId = Integer.parseInt(stories_image_list.get(position).getStories().get(0).getStoriesId());

                                story_id = stories_image_list.get(position).getStories().get(0).getStoriesId();

/*
                                createStoriesView(storyId);
*/

                                /*Storiew view count displaying API*/
                                getStoriesViewCount();

                            }

                        }


                    }

                    @Override
                    public void onFailure(@NonNull Call<Get_stories_pojo_output> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });


            } else {

                Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();

            }


        }

    }


    /*Creating story view update*/

    private void createStoriesView(int storyId) {

        if (isAdded()) {
            if (Utility.isConnected(getActivity())) {
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                String createuserid = Pref_storage.getDetail(getActivity(), "userId");
                Call<CommonOutputPojo> call = apiService.create_stories_view("create_stories_view", storyId, Integer.parseInt(createuserid));

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseMessage().equals("success")) {


                        }


                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } else {
                Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();

            }


        }

    }

    /*Delete stories*/
    private void create_delete_stories() {


        if (isAdded()) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            final String createuserid = Pref_storage.getDetail(getActivity(), "userId");

            try {

                Call<Create_story_output_pojo> call = apiService.create_delete_stories("create_delete_stories", Integer.parseInt(story_id), Integer.parseInt(createuserid));

                call.enqueue(new Callback<Create_story_output_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Create_story_output_pojo> call, @NonNull Response<Create_story_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Deleted!!")
                                    //                                    .setContentText("Your story has been deleted.")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            startActivity(new Intent(getActivity(), Home.class));
                                            getActivity().finish();
                                        }
                                    })
                                    .show();

                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Create_story_output_pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        storiesProgressView.resume();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {


            case R.id.actionbar_story:

                if (stories_image_list.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(getActivity(), "userId"))) {

                    Intent intent = new Intent(getActivity(), Profile.class);
                    intent.putExtra("created_by", stories_image_list.get(position).getUserId());
                    Objects.requireNonNull(getActivity()).startActivity(intent);

                } else {

                    Intent intent = new Intent(getActivity(), User_profile_Activity.class);
                    intent.putExtra("created_by", stories_image_list.get(position).getUserId());
                    Objects.requireNonNull(getActivity()).startActivity(intent);
                }


                break;


            case R.id.back:

                if (isAdded()) {

                    getActivity().finish();

                }

                break;

            case R.id.txtStoryView:

                /*Call get stories view*/

                getStoriesView();

                break;


            case R.id.img_delete_story:

                storiesProgressView.pause();

                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to delete this story?")
                        .setContentText("You can't get back the story")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {

                                sDialog.dismissWithAnimation();

                                /*Delete stories*/

                                create_delete_stories();

                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {

                                sDialog.dismissWithAnimation();

                                storiesProgressView.resume();

                            }
                        }).show();

                break;

            default:
                break;


        }
    }

    private void getStoriesAlll() {


        if (isAdded()) {


            if (Utility.isConnected(getActivity())) {

                stories_image_list.clear();
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                final String createuserid = Pref_storage.getDetail(getActivity(), "userId");

                Call<Get_stories_pojo_output> call = apiService.get_stories_all("get_stories_whole_users", Integer.parseInt(createuserid));

                call.enqueue(new Callback<Get_stories_pojo_output>() {
                    @Override
                    public void onResponse(@NonNull Call<Get_stories_pojo_output> call, @NonNull Response<Get_stories_pojo_output> response) {


                        if (response.body() != null && response.body().getResponseCode() == 1) {

                          /*  for(int i=0;i<response.body().getData().size();i++) {


                                actionbarStory.setVisibility(View.VISIBLE);


                                storiesCount=response.body().getData().get(i).getStoriesCount();
                                int viewedCount=response.body().getData().get(i).getViewedCount();


                                    stories_image_list = response.body().getData();
                                    try {

                                        storiesProgressView.setStoriesCount(storiesCount);
                                        storiesProgressView.setStoryDuration(10000L);
                                        storiesProgressView.startStories();

                                        Log.e(TAG, "onResponse: stories_image_list"+stories_image_list);

                                    } catch (Exception e) {

                                        Log.e(TAG, "onResponse: Exception in response");
                                    }

                                *//*Creating story view update*//*

                                try {
                                    int storyId = Integer.parseInt(stories_image_list.get(position).getStories().get(0).getStoriesId());
                                     createStoriesView(storyId);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }*/

                            storiesPojoList.clear();
                            storiesPojoList1.clear();
                            stories_image_list.clear();

                            for(int i=0;i<response.body().getData().size();i++)
                            {
                                for(int j=0;j<response.body().getData().get(i).getStories().size();j++)
                                {



                                    if(response.body().getData().get(i).getStories().get(j).getStoriesInrView().equals("0"))
                                    {

                                        String storiesId=response.body().getData().get(i).getStories().get(j).getStoriesId();
                                        String storiesDescription=response.body().getData().get(i).getStories().get(j).getStoriesDescription();
                                        String storiesImage=response.body().getData().get(i).getStories().get(j).getStoriesImage();
                                        String address=response.body().getData().get(i).getStories().get(j).getAddress();
                                        String area=response.body().getData().get(i).getStories().get(j).getArea();
                                        String city=response.body().getData().get(i).getStories().get(j).getCity();
                                        String createdBy=response.body().getData().get(i).getStories().get(j).getCreatedBy();
                                        String createdOn=response.body().getData().get(i).getStories().get(j).getCreatedOn();
                                        String totalView=response.body().getData().get(i).getStories().get(j).getTotalView();
                                        String firstName=response.body().getData().get(i).getStories().get(j).getFirstName();
                                        String lastName=response.body().getData().get(i).getStories().get(j).getLastName();
                                        String picture=response.body().getData().get(i).getStories().get(j).getPicture();
                                        String storiesInrView=response.body().getData().get(i).getStories().get(j).getStoriesInrView();


                                        storiesPojo=new StoriesPojo(storiesId,storiesDescription,storiesImage,address,area,city,createdBy,createdOn,totalView,firstName,lastName,picture,storiesInrView);

                                        storiesPojoList.add(storiesPojo);
                                    }
                                    else
                                    {

                                        storiesPojoList1=response.body().getData().get(i).getStories();
                                    }


                                    // Collections.reverse(mergeStoriesPojoList);

                                    Log.e(TAG, "onResponse: "+storiesPojoList.size() );
                                    Log.e(TAG, "onResponse: "+storiesPojoList1.size() );

                                    // add values in stories_image_list

                                    storiesCount= response.body().getData().get(i).getViewedCount();
                                    get_all_stories_pojo=new Get_all_stories_pojo(response.body().getData().get(i).getStrId(),
                                            response.body().getData().get(i).getStoriesImage(),
                                            response.body().getData().get(i).getStoriesDescription(),
                                            response.body().getData().get(i).getAddress(),
                                            response.body().getData().get(i).getArea(),
                                            response.body().getData().get(i).getCity(),
                                            response.body().getData().get(i).getCreatedBy(),
                                            response.body().getData().get(i).getCreatedOn(),
                                            response.body().getData().get(i).getUserId(),
                                            response.body().getData().get(i).getFirstName(),
                                            response.body().getData().get(i).getLastName(),
                                            response.body().getData().get(i).getPicture(),
                                            response.body().getData().get(i).getStoriesView(),
                                            storiesPojoList,
                                            response.body().getData().get(i).getStoriesCount(),
                                            response.body().getData().get(i).getViewedCount());

                                    stories_image_list.add(get_all_stories_pojo);
                                    try {
                                        int storyId = Integer.parseInt(response.body().getData().get(i).getStories().get(j).getStoriesId());
                                        Log.e(TAG, "storyId"+storyId);
                                        createStoriesView(storyId);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                try {

                                    storiesProgressView.setStoriesCount(storiesCount);
                                    storiesProgressView.setStoryDuration(10000L);
                                    storiesProgressView.startStories();


                                } catch (Exception e) {

                                    Log.e(TAG, "onResponse: Exception in response");
                                }

                            }
                            Log.e(TAG, "onResponse: stories_image_listsizee"+stories_image_list.size());





                        }


                    }

                    @Override
                    public void onFailure(Call<Get_stories_pojo_output> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });


            } else {

            }

        }


    }

}