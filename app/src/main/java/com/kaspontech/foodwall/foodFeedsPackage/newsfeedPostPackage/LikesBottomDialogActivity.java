package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_follower_output_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.GetallLikesPojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Likes_2_pojo;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LikesBottomDialogActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LikesBottomDialogActivity";

    // Action Bar
    Toolbar actionBar;
    ImageButton back;
    RelativeLayout rl_likes;
    RecyclerView rv_likes;
    LinearLayoutManager llm;
    com.kaspontech.foodwall.adapters.TimeLine.likesAdapter likesAdapter;
    GetallLikesPojo getallLikesPojo;

    ProgressBar progress_dialog_reply;
    TextView toolbar_next,toolbar_title;

    String Likes_timelineid;

    //Bottom dialog likes
   BottomSheetDialogFragment bottomSheetDialogFragment;

   BottomSheetDialog bottomSheetDialog;
    BottomSheetBehavior sheetBehavior;

    ArrayList<GetallLikesPojo> likelist = new ArrayList<>();
    List<Get_followerlist_pojo> followerlist = new ArrayList<>();
    public String txt_usernamelike, txt_lastusernamelike, fulltxt_usernamelike, ago_like, likeuserimage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_likes_layout);

        BottomSheetDialogFragment bottomSheetDialogFragment = new BottomDialogFragment();

        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());




      /*


        actionBar = (Toolbar) findViewById(R.id.actionbar_like);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);


        toolbarNext = (TextView) actionBar.findViewById(R.id.toolbarNext);
        toolbarTitle = (TextView) actionBar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarNext.setVisibility(View.GONE);

        rlLikes = (RelativeLayout) findViewById(R.id.rlLikes);
        rvLikes = (RecyclerView) findViewById(R.id.rvLikes);
        progressDialogLikes = (ProgressBar) findViewById(R.id.progressDialogLikes);


        likesAdapter = new likesAdapter(LikesBottomDialogActivity.this, likelist);
        llm = new LinearLayoutManager(LikesBottomDialogActivity.this);
        rvLikes.setLayoutManager(llm);
        rvLikes.setAdapter(likesAdapter);
//                            rvLikes.setHasFixedSize(true);
        rvLikes.setItemAnimator(new DefaultItemAnimator());
        rvLikes.setNestedScrollingEnabled(false);


        //Getting timelineID from various adapter
        Intent intent = getIntent();
        likesTimelineid = intent.getStringExtra("likesTimelineid");


        progressDialogLikes.setIndeterminate(true);



        //Calling Likes All API

        get_timeline_likes_all();

*/



    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:

                finish();
                break;
        }


    }



    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }



    //Calling Likes API

    private void get_timeline_likes_all() {
        if(isConnected(this)){
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
//             likesTimelineid= Pref_storage.getDetail(likesActivity.this,"likesTimelineid");
                String createdby = Pref_storage.getDetail(LikesBottomDialogActivity.this, "userId");

                Call<Likes_2_pojo> call = apiService.get_timeline_likes_all("get_timeline_likes_all", Integer.parseInt(Likes_timelineid), Integer.parseInt(createdby));
                call.enqueue(new Callback<Likes_2_pojo>() {
                    @Override
                    public void onResponse(Call<Likes_2_pojo> call, Response<Likes_2_pojo> response) {

                        if (response.body().getResponseCode() == 1) {

                            progress_dialog_reply.setVisibility(View.GONE);
                            progress_dialog_reply.setIndeterminate(false);

                            for (int j = 0; j < response.body().getData().size(); j++) {

                                txt_usernamelike = response.body().getData().get(j).getFirstName();
                                String user_id = response.body().getData().get(j).getUserId();
                                String timelineid = response.body().getData().get(j).getTimelineId();
                                txt_lastusernamelike = response.body().getData().get(j).getLastName();

                                likeuserimage = response.body().getData().get(j).getPicture();


                                getallLikesPojo = new GetallLikesPojo("", "", "",
                                        "", "", likeuserimage, txt_usernamelike, "", "", "", txt_lastusernamelike,
                                        "", user_id, timelineid);

                                likelist.add(getallLikesPojo);

                            }

                            likesAdapter.notifyDataSetChanged();
                        }

                    }

                    @Override
                    public void onFailure(Call<Likes_2_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {

            Toast.makeText(getApplicationContext(), "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }


    }


    private void get_follower() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

            Call<Get_follower_output_pojo> call = apiService.get_follower("get_follower", Integer.parseInt(createdby));
            call.enqueue(new Callback<Get_follower_output_pojo>() {
                @Override
                public void onResponse(Call<Get_follower_output_pojo> call, Response<Get_follower_output_pojo> response) {

                    if (response.body().getResponseCode() == 1) {

                        for (int j = 0; j < response.body().getData().size(); j++) {

                            followerlist = response.body().getData();


                        }
                    }

                }

                @Override
                public void onFailure(Call<Get_follower_output_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {

        bottomSheetDialog.dismiss();

        finish();
    }
}
