package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.adapters.TimeLine.Full_image_slideadapter;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.R;

import java.io.File;

import me.relex.circleindicator.CircleIndicator;

import static com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Add_photo_for_post.imagesEncodedList;

public class Fullview_ImagePost extends AppCompatActivity implements View.OnClickListener {
    ImageView imageView_gallery;

    Toolbar actionBar;
    ImageButton back;
    TextView next;

    private String[] FilePathStrings;
    private String[] FileNameStrings;
    private File[] listFile;
    File file;
    String path;
    String userphoto, image_gallery;

    ViewPager mviewpager;
    CircleIndicator circleIndicator;

    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    RelativeLayout rl_dotindicator_post, rl_fullimage_post;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_full_image);
        imageView_gallery = (ImageView) findViewById(R.id.full_image_view);

        actionBar = (Toolbar) findViewById(R.id.action_bar_post_gallery);
        mviewpager = (ViewPager) findViewById(R.id.mviewpager_post);
        circleIndicator = (CircleIndicator) findViewById(R.id.indicator_post);
        rl_fullimage_post = (RelativeLayout) findViewById(R.id.rl_fullimage_post);
        rl_dotindicator_post = (RelativeLayout) findViewById(R.id.rl_dotindicator_post);


        back = (ImageButton) actionBar.findViewById(R.id.button_back);
        back.setOnClickListener(this);


        next = (TextView) actionBar.findViewById(R.id.toolbar_text);
        next.setOnClickListener(this);


        if (savedInstanceState == null) {

            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                userphoto = null;
                image_gallery = null;
            } else {
                userphoto = extras.getString("userimage_camera");
                image_gallery = extras.getString("userimage_gallery");
            }
        } else {
            userphoto = (String) savedInstanceState.getSerializable("userimage_camera");
            image_gallery = (String) savedInstanceState.getSerializable("userimage_gallery");
        }


        if (userphoto == null || userphoto.equalsIgnoreCase("")) {
            init();
        } else if (image_gallery == null || image_gallery.equalsIgnoreCase("")) {
            Uri uri = Uri.parse(userphoto);
            rl_fullimage_post.setVisibility(View.GONE);
            rl_dotindicator_post.setVisibility(View.GONE);
            imageView_gallery.setVisibility(View.VISIBLE);
            imageView_gallery.setImageURI(uri);
        }


    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            case R.id.toolbar_text:
                Intent i = new Intent(getApplicationContext(), newsfeedPost.class);

                i.putExtra("fromcamera", userphoto);

                i.putExtra("fromgallery", imagesEncodedList.toString());

                Log.e("fromgallery", "fromgallery" + imagesEncodedList.toString());
                startActivity(i);
//                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                finish();
                break;


            case R.id.button_back:
                startActivity(new Intent(Fullview_ImagePost.this, Home.class));
                finish();

                break;


        }


    }
    //View Pager

    private void init() {


        mviewpager.setAdapter(new Full_image_slideadapter(Fullview_ImagePost.this, imagesEncodedList));
        circleIndicator.setViewPager(mviewpager);
        NUM_PAGES = imagesEncodedList.size();
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mviewpager.setCurrentItem(currentPage++, true);
            }
        };

        // Pager listener over indicator
        circleIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Fullview_ImagePost.this, Home.class));
        finish();
    }
}