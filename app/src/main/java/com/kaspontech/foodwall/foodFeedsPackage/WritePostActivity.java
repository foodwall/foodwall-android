package com.kaspontech.foodwall.foodFeedsPackage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.kaspontech.foodwall.R;

public class WritePostActivity extends AppCompatActivity {


    private static final String TAG = "WritePostActivity";

    // Action Bar
    Toolbar actionBar;
    ImageButton back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_post);

        actionBar = (Toolbar) findViewById(R.id.action_bar_write_post);
        back = (ImageButton)actionBar.findViewById(R.id.back);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });


    }
}
