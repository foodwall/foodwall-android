package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Create_story_output_pojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.storylibrary.StoriesProgressView;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import org.apache.commons.text.StringEscapeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class storiesActivity extends AppCompatActivity implements
        View.OnClickListener, StoriesProgressView.StoriesListener {

    private static final int PROGRESS_COUNT = 6;

    /*Toolbar*/
    Toolbar storyToolbar;

    /*Image button back */
    ImageButton back;
    ImageButton btnOption;
    /* Image for story */
    ImageView imgStoriesUser;
    ImageView imgStory;
    /* Text descripton */

    EmojiconTextView txtStoryDescription;
    TextView txtStoryAgo;
    TextView txtStoryUsername;
    /* Storyprogress */
    StoriesProgressView img_stories_progressview;
    View v;
    String fname;
    String lname;
    String story_description;
    String created_on;
    String created_by;
    String story_id;
    String story_image;
    String picture;

    RelativeLayout frameLayout;

    RelativeLayout rlStoryCaption;

    ProgressBar storyTimer;
    CountDownTimer mCountDownTimer;
    int i = 0;
    ObjectAnimator animation;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stories_layout);


        storyToolbar = findViewById(R.id.actionbar_story);
        back = storyToolbar.findViewById(R.id.back);
        back.setOnClickListener(this);


        btnOption = findViewById(R.id.btn_option);
        btnOption.setOnClickListener(this);

        /* ImageView */
        imgStoriesUser = findViewById(R.id.img_stories_user);

        imgStory = findViewById(R.id.img_story);


        storyTimer = findViewById(R.id.story_timer);
        frameLayout = findViewById(R.id.frame_layout);
        rlStoryCaption = findViewById(R.id.rl_story_caption);

        storyTimer.setScaleY(0.5f);

        /* TextView */
        txtStoryUsername = findViewById(R.id.txt_story_username);
        txtStoryDescription = findViewById(R.id.txt_descrip);
        txtStoryAgo = findViewById(R.id.txt_story_ago);


        storyTimer.getProgressDrawable().setColorFilter(
                Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN);


        animation = ObjectAnimator.ofInt(storyTimer, "progress", 0, 100);
        animation.setDuration(4000);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                //do something when the countdown is complete
                finish();
            }

            @Override
            public void onAnimationCancel(Animator animator) {


            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animation.start();


        imgStory.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    animation.pause();
                    rlStoryCaption.setBackgroundColor(R.color.black_transparent);

                } else if (event.getAction() == MotionEvent.ACTION_UP) {

                    animation.resume();
                    rlStoryCaption.setBackgroundColor(0);


                }
                return true;

            }
        });

        /* storyTimer.setProgress(i);
        mCountDownTimer=new CountDownTimer(2000,1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                Log.v("Log_tag", "Tick of Progress"+ i+ millisUntilFinished);
                i++;
                storyTimer.setProgress((int)i*100/(5000/1000));

            }

            @Override
            public void onFinish() {
                //Do what you want
                i++;
                storyTimer.setProgress(100);
            }
        };
        mCountDownTimer.start();
*/
        //StoryProgressView
//        img_stories_progressview = (StoriesProgressView) findViewById(R.id.img_stories_progressview);

        // inputs

        Intent intent = getIntent();

        fname = intent.getStringExtra("fname");
        lname = intent.getStringExtra("lname");
        created_on = intent.getStringExtra("created_on");
        created_by = intent.getStringExtra("created_by");
        Log.e("created_on-->", "" + created_on);
        story_image = intent.getStringExtra("story_image");
        Log.e("story_image-->", "" + story_image);
        story_description = intent.getStringExtra("story_description");
        story_id = intent.getStringExtra("story_id");
        /*Log.e("story_id-->",""+story_id);*/
        picture = intent.getStringExtra("picture");

        /*Caption displaying*/
        String storyCaption = StringEscapeUtils.unescapeJava(story_description);

        txtStoryDescription.setText(storyCaption);

        /*Image Loading*/
        GlideApp.with(storiesActivity.this).load(story_image).fitCenter().
                diskCacheStrategy(DiskCacheStrategy.ALL).
                thumbnail(0.1f).
                into(imgStory);
        txtStoryUsername.setOnClickListener(this);

        txtStoryUsername.setText(fname.concat(" ").concat(lname));

        /*User image loading*/

        GlideApp.with(storiesActivity.this).load(picture).fitCenter().into(imgStoriesUser);

        //Created ago
        createdonAgo(created_on);

        if (created_by.equalsIgnoreCase(Pref_storage.getDetail(this, "userId"))) {
            btnOption.setVisibility(View.VISIBLE);
        } else {
            btnOption.setVisibility(View.GONE);
        }



/*
        img_stories_progressview.setStoriesCount(PROGRESS_COUNT); // <- set stories
        img_stories_progressview.setStoryDuration(1200L);         // <- set a story duration
        img_stories_progressview.setStoriesListener(this);        // <- set listener
        img_stories_progressview.startStories();
*/


    }

    @Override
    public void onClick(View v) {
        int vie = v.getId();
        switch (vie) {


            case R.id.back:


                finish();

                break;

            case R.id.txt_story_username:
                if (created_by.equalsIgnoreCase(Pref_storage.getDetail(this, "userId"))) {
                    Intent intent = new Intent(getApplicationContext(), Profile.class);
                    intent.putExtra("created_by", created_by);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), User_profile_Activity.class);
                    intent.putExtra("userid", created_by);
                    startActivity(intent);
                }
                break;

            case R.id.img_stories_user:
                if (created_by.equalsIgnoreCase(Pref_storage.getDetail(this, "userId"))) {
                    Intent intent = new Intent(getApplicationContext(), Profile.class);
                    intent.putExtra("created_by", created_by);
                    startActivity(intent);

                } else {

                    Intent intent = new Intent(getApplicationContext(), User_profile_Activity.class);
                    intent.putExtra("created_by", created_by);
                    startActivity(intent);
                }
                break;

            case R.id.btn_option:
                CustomDialogClass ccd = new CustomDialogClass(this);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccd.setCancelable(true);
                ccd.show();

                break;


                default:
                    break;
        }

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    //Custom alert

    private void create_delete_stories() {
        if(Utility.isConnected(getApplicationContext())){
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                Call<Create_story_output_pojo> call = apiService.create_delete_stories("create_delete_stories", Integer.parseInt(story_id), Integer.parseInt(created_by));
                call.enqueue(new Callback<Create_story_output_pojo>() {
                    @Override
                    public void onResponse(Call<Create_story_output_pojo> call, Response<Create_story_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            new SweetAlertDialog(storiesActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Deleted!!")
                                    //                                .setContentText("Your story has been deleted.")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            startActivity(new Intent(getApplicationContext(), Home.class));
                                            finish();
                                        }
                                    })
                                    .show();

                        }

                    }

                    @Override
                    public void onFailure(Call<Create_story_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            Toast.makeText(this, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }



    }
    //Created ago

    private void createdonAgo(String createdon) {


        try {

//Input time
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long time = sdf.parse(createdon).getTime();


            Date currentTime = Calendar.getInstance().getTime();
            SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String now_new = sdformat.format(currentTime);
            long nowdate = sdformat.parse(now_new).getTime();


            CharSequence ago = DateUtils.getRelativeTimeSpanString(time, nowdate, DateUtils.FORMAT_ABBREV_RELATIVE);

            if (ago.toString().equalsIgnoreCase("0 minutes ago") || ago.toString().equalsIgnoreCase("In 0 minutes")) {
                txtStoryAgo.setText(R.string.just_now);
            } else {
                txtStoryAgo.setText(ago);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onStoryStart() {

    }

    @Override
    public void onNext() {

    }

    @Override
    public void onPrev() {

    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public AppCompatActivity c;
        public Dialog d;
        TextView txt_edit_story, txt_delete_post;

        CustomDialogClass(AppCompatActivity a) {
            super(a);
            // TODO Auto-generated constructor stub
            c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.story_settings_layout);

            txt_edit_story = findViewById(R.id.txt_edit_story);
            txt_delete_post = findViewById(R.id.txt_delete_post);
            txt_edit_story.setOnClickListener(this);
            txt_delete_post.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_edit_story:
                    Intent intent = new Intent(storiesActivity.this, editStoriesActivity.class);
                    intent.putExtra("edit_image", story_image);
                    intent.putExtra("edit_story_description", story_description);
                    intent.putExtra("edit_story_id", story_id);
                    intent.putExtra("edit_createdby", created_by);
                    startActivity(intent);

                    break;
                case R.id.txt_delete_post:

                    new SweetAlertDialog(storiesActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to delete this story?")
                            .setContentText("Won't be able to recover this story anymore!")
                            .setConfirmText("Delete")
                            .setCancelText("Cancel")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    create_delete_stories();

                                }
                            })
                            .show();
                    dismiss();

                    break;

                default:
                    break;
            }
        }
    }
}
