package com.kaspontech.foodwall.foodFeedsPackage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.REST_API.API_interface_restaurant;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.REST_API.ProgressRequestBody;
import com.kaspontech.foodwall.adapters.HistoricalMap.HistoricalTagPeopleAdapter;
import com.kaspontech.foodwall.adapters.HistoricalMap.HistoryImagesAdapter;
import com.kaspontech.foodwall.adapters.HistoricalMap.RestaurantSearchHisMapRestNameAdapter;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Story_full_image_activity;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersOutput;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.Geometry;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.GetRestaurantsResult;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.Get_HotelStaticLocation_Response;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.OpeningHours;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.RestaurantDetails;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.RestaurantPhoto;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Image;
import com.kaspontech.foodwall.reviewpackage.restaurantSearch.ReviewRestaurantSearch;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import dmax.dialog.SpotsDialog;
import id.zelory.compressor.Compressor;
import mabbas007.tagsedittext.TagsEditText;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateHostoricalMapRestname extends AppCompatActivity implements
        View.OnClickListener,
        TagsEditText.TagsEditListener,
        ProgressRequestBody.UploadCallbacks,
        HistoricalTagPeopleAdapter.ContactsAdapterListener,
        HistoryImagesAdapter.PhotoRemovedListener, RestaurantSearchHisMapRestNameAdapter.onLoadRestaurantName, LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


// Widgets

    private Toolbar/* actionbar,*/ selectActionBar;

    private RecyclerView hotel_historical_recylerview, follwersRecylerview;

    private EditText edit_search_hotel, date_of_restaurent_visit, friendsListDisplayTag, description_of_visit;

    private ImageButton /*close,*/ edit_restaurants, set_history_date, selectClose, selectDone;

    private TextView tv_createHistory, restaurantName;

    private LinearLayout ll_search_restaurant, ll_history_fields, ll_history_image_section, ll_tag_people, ll_not_found;

    private TagsEditText friendsListDisplay;

    private TextView tv_add_photo;

    private int image_status;

    private RelativeLayout rl_add_img_tag, rl_create_post, empty_list;

// List

    private List<String> types;

    private List<Object> weekdayText;

    private List<RestaurantPhoto> photos;

    private List<String> htmlAttributions;

    private List<OpeningHours> openingHoursPojoList;

    private List<RestaurantDetails> restaurantOutputPojoList;

    private ArrayList<GetRestaurantsResult> getallRestaurantDetailsPojoArrayList = new ArrayList<>();

    private Geometry geometryPojo;

    private OpeningHours openingHoursPojo;


// Google location search retrofit interface

    private API_interface_restaurant apiService;

    private SearchView search_followers;

    private ImageButton add_image_post, add_people, ic_add_people, ic_add_photo;

    private Button btn_create_post;

    private HistoricalTagPeopleAdapter historicalTagPeopleAdapter;

    private HashMap<Integer, String> cohostNameIdListd = new HashMap<>();

    private List<Integer> cohostList = new ArrayList<>();

    private List<Integer> friendsList = new ArrayList<>();

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    private ViewPager mviewpager;

    private CircleIndicator circleIndicator;

    private String userChoosenTask, imagepath, userImage;


    private static final int REQUEST_CAMERA_PERMISSION = 1;

    private static final int REQUEST_GALLERY_PERMISSION = 2;

    private static final int REQUEST_PERMISSION_SETTING = 5;


    private int PICK_IMAGE_MULTIPLE = 2;

    private String imageEncoded;

    private File file;

    public List<Image> imagesEncodedList = new ArrayList<>();

    private static int currentPage = 0;

    private static int NUM_PAGES = 0;

    // GPS
    private GPSTracker gpsTracker;

    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    private static GoogleApiClient mGoogleApiClient;

    private GoogleApiClient googleApiClient;

    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;

    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";

    final static int REQUEST_LOCATION = 199;

    //  Variables
    private double latitude, longitude, latitudee, longitudee;

    private Uri imageUri;

    private String latLngString, hotel_name, hotel_address, google_id, google_photo, place_id;

    private int eventId, userid, tag_status;

    private String followerId, firstName, lastName, userPhoto;

    private List<GetFollowersData> getFollowersDataList = new ArrayList<>();

    private List<GetFollowersData> tempFollowersDataList = new ArrayList<>();

    public static AlertDialog createPostDialog = null;

    private int count = 0;

    private boolean firstTimeTagged = false;

    private static final String TAG = "CreateHostoricalMapRestname";


    private String file_camera, file_gallery, location, story_caption;

    private ApiInterface apiInterface;

    ProgressBar progress, progress_dialog;

    private Location mylocation;

    private AlertDialog sweetDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_hostorical_map_restname);

        userid = Integer.parseInt(Pref_storage.getDetail(CreateHostoricalMapRestname.this, "userId"));

        getFollowers(userid);

        setUpGClient();

        initComponents();


        //get image path and story caption
        Intent intent = getIntent();

        if (intent != null) {

            file_camera = intent.getStringExtra("file_camera");
            file_gallery = intent.getStringExtra("file_gallery");
            story_caption = intent.getStringExtra("story_caption");

        }

        Log.e("CheckingCaption", "CheckingLocation->" + story_caption);


        edit_search_hotel.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        edit_search_hotel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    String getrestaurantName = edit_search_hotel.getText().toString();

                    int result = ContextCompat.checkSelfPermission(CreateHostoricalMapRestname.this, Manifest.permission.ACCESS_FINE_LOCATION);

                    final String restaurantName = getrestaurantName.replace(" ", "+");


                    if (result == PackageManager.PERMISSION_GRANTED) {
                        /*Calling nearby restaurants API using edit text search results*/

                        if (gpsTracker.isGPSEnabled) {

                            // fetchStores(restaurantName);
                            nearByRestaurants(restaurantName, latitude, longitude);

                        } else {

                            latitude = 13.0723153;
                            longitude = 80.215563;

                            nearByRestaurants(restaurantName, latitude, longitude);
                        }
                    } else if (result == PackageManager.PERMISSION_DENIED) {

                        latitude = 13.0723153;
                        longitude = 80.215563;

                        nearByRestaurants(restaurantName, latitude, longitude);

                    }
                    return true;
                }
                return false;
            }
        });


        edit_search_hotel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                getallRestaurantDetailsPojoArrayList.clear();


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String getrestaurantName = edit_search_hotel.getText().toString();

                int result = ContextCompat.checkSelfPermission(CreateHostoricalMapRestname.this, Manifest.permission.ACCESS_FINE_LOCATION);

                final String restaurantName = getrestaurantName.replace(" ", "+");

                if (result == PackageManager.PERMISSION_GRANTED) {

                    if (restaurantName.length() != 0) {
                        /*Calling nearby restaurants API using edit text search results*/

                        //fetchStores(restaurantName);
                        nearByRestaurants(restaurantName, latitude, longitude);

                    } else {

                        latitude = 13.0723153;
                        longitude = 80.215563;

                        nearByRestaurants(restaurantName, latitude, longitude);
                    }

                } else if (result == PackageManager.PERMISSION_DENIED) {

                    latitude = 13.0723153;
                    longitude = 80.215563;

                    nearByRestaurants(restaurantName, latitude, longitude);
                }

            }
        });
    }

    private void initComponents() {

        gpsTracker = new GPSTracker(CreateHostoricalMapRestname.this);
        apiService = APIClient_restaurant.getClient().create(API_interface_restaurant.class);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        // actionbar = (Toolbar) findViewById(R.id.action_bar_create_historical_map);
        // close = (ImageButton) actionbar.findViewById(R.id.close);
        edit_restaurants = findViewById(R.id.edit_restaurants);
        set_history_date = findViewById(R.id.set_history_date);
        add_image_post = findViewById(R.id.add_image_post);
        add_people = findViewById(R.id.add_people);
        ic_add_people = findViewById(R.id.ic_add_people);
        ic_add_photo = findViewById(R.id.ic_add_photo);
        //  tv_createHistory = (TextView) actionbar.findViewById(R.id.tv_create_history);
        tv_add_photo = findViewById(R.id.tv_add_photo);
        ll_search_restaurant = findViewById(R.id.ll_search_restaurant);
        ll_history_fields = findViewById(R.id.ll_history_fields);
        ll_history_image_section = findViewById(R.id.ll_history_image_section);
        ll_not_found = findViewById(R.id.ll_not_found);
        ll_tag_people = findViewById(R.id.ll_tag_people);
        hotel_historical_recylerview = findViewById(R.id.hotel_historical_recylerview);
        edit_search_hotel = findViewById(R.id.edit_search_hotel);
        date_of_restaurent_visit = findViewById(R.id.date_of_restaurent_visit);
        description_of_visit = findViewById(R.id.description_of_visit);
        friendsListDisplayTag = findViewById(R.id.friendsListDisplayTag);
        restaurantName = findViewById(R.id.restaurantName);
        friendsListDisplay = findViewById(R.id.friendsListDisplay);

        mviewpager = findViewById(R.id.mviewpager_post);
        circleIndicator = findViewById(R.id.indicator_post);
        rl_add_img_tag = findViewById(R.id.rl_add_img_tag);
        rl_create_post = findViewById(R.id.rl_create_post);
        progress = findViewById(R.id.progress);

        //  close.setOnClickListener(this);
        friendsListDisplay.setTagsListener(this);
        date_of_restaurent_visit.setOnClickListener(this);
        //   tv_createHistory.setOnClickListener(this);
        friendsListDisplay.setOnClickListener(this);
        edit_restaurants.setOnClickListener(this);
        restaurantName.setOnClickListener(this);
        set_history_date.setOnClickListener(this);
        friendsListDisplayTag.setOnClickListener(this);
        tv_add_photo.setOnClickListener(this);
        add_image_post.setOnClickListener(this);
        ic_add_people.setOnClickListener(this);
        ic_add_photo.setOnClickListener(this);
        add_people.setOnClickListener(this);
        rl_create_post.setOnClickListener(this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                googleApiClient.disconnect();
            }
        }
        Intent intent = new Intent(CreateHostoricalMapRestname.this, Story_full_image_activity.class);
        intent.putExtra("file_camera", file_camera);
        intent.putExtra("file_gallery", file_gallery);
        intent.putExtra("location", location);
        intent.putExtra("story_caption", story_caption);
        startActivity(intent);
        finish();
    }


    private void fetchStores(String businessName) {


        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        latLngString = latitude + "," + longitude;

        Log.e("FetchStores:", "businessName->" + businessName);
        Log.e("FetchStores:", "latLngString->" + latLngString);


        Call<RestaurantDetails> call = apiService.searchRestaurants(businessName, latLngString, "20000", "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<RestaurantDetails>() {
            @Override
            public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                RestaurantDetails restaurantDetails = response.body();

                if (response.isSuccessful()) {


                    Log.e("restaurantOutputPojo", "" + response.toString());

                    if (restaurantDetails.getStatus().equalsIgnoreCase("OK")) {


                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();

                        Log.e("hotel_size", "" + response.body().getGetRestaurantsResults().size());

                        for (int i = 0; i < response.body().getGetRestaurantsResults().size(); i++) {

                            String placeid = response.body().getGetRestaurantsResults().get(i).getPlaceId();
                            String Icon = response.body().getGetRestaurantsResults().get(i).getIcon();
                            String hotel_name = response.body().getGetRestaurantsResults().get(i).getName();
                            String Id = response.body().getGetRestaurantsResults().get(i).getId();
                            String reference = response.body().getGetRestaurantsResults().get(i).getReference();
                            String address = response.body().getGetRestaurantsResults().get(i).getFormattedAddress();
                            String rating = response.body().getGetRestaurantsResults().get(i).getRating();

                            String photo_flag = "";

                            List<RestaurantPhoto> photosRestaurant = new ArrayList<>();

                            photosRestaurant = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos();

                            try {

                                String photo_ref = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();
                                int width = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                int height = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();
                                photo_flag = photo_ref;


                                if (photo_ref == null) {

                                } else {

                                    RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
                                    Log.e("height1", "photo_ref1: " + photo_ref);
                                    Log.e("height1", "width1: " + width);
                                    Log.e("height1", "height1: " + height);
                                    photos.add(restaurantPhoto);

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            for (int kk = 0; kk < restaurantDetails.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantDetails.getGetRestaurantsResults().get(i).getTypes().toString());
                            }


                            try {

                                boolean open_status = restaurantDetails.getGetRestaurantsResults().get(i).getOpeningHours().getOpenNow();
                                Log.e("open_status", "open_status: " + open_status);
                                openingHoursPojo = new OpeningHours(open_status, weekdayText);
                                openingHoursPojoList.add(openingHoursPojo);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            if (photosRestaurant != null) {

                                GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                        hotel_name, openingHoursPojo, photosRestaurant, placeid, rating, reference, types, address);
                                getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);

                            } else {


                            }

                            RestaurantSearchHisMapRestNameAdapter restaurantSearchHisMapAdapter = new RestaurantSearchHisMapRestNameAdapter(CreateHostoricalMapRestname.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList, CreateHostoricalMapRestname.this);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(CreateHostoricalMapRestname.this, LinearLayoutManager.VERTICAL, true);
                            hotel_historical_recylerview.setLayoutManager(mLayoutManager);
                            mLayoutManager.setStackFromEnd(true);
                            //  mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                            hotel_historical_recylerview.setAdapter(restaurantSearchHisMapAdapter);
                            hotel_historical_recylerview.setItemAnimator(new DefaultItemAnimator());
                            hotel_historical_recylerview.setNestedScrollingEnabled(false);
                            // hotel_historical_recylerview.setHorizontalScrollBarEnabled(true);
                            restaurantSearchHisMapAdapter.notifyDataSetChanged();
                            hotel_historical_recylerview.setVisibility(View.VISIBLE);
                            ll_not_found.setVisibility(View.GONE);


                        }

                    } else {

                        ll_not_found.setVisibility(View.VISIBLE);
                        hotel_historical_recylerview.setVisibility(View.GONE);
//                        Toast.makeText(CreateHostoricalMapRestname.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(CreateHostoricalMapRestname.this, "Error " + response.code() + " found. Please try again later.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });


    }


//    public void noresults(int size){
//        if(size==1){
//            ll_not_found.setVisibility(View.VISIBLE);
//        }
//    }

    private void nearByRestaurants(String restaurantName, double latitude, double longitudee) {

        progress.setVisibility(View.VISIBLE);


        latLngString = latitude + "," + longitudee;
        Log.e("latLngString", "" + latitude);
        Log.e("latLngString", "" + longitudee);
        Log.e("latLngString", "" + latLngString);

//        Call<GetNearByRestaurantOutputPojo> call = apiService.nearByRestaurants("restaurant", latLngString, "500", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        Call<RestaurantDetails> call = apiService.searchRestaurantsReviewWithnearby(restaurantName,
                latLngString, "500",
                "restaurant",
                APIClient_restaurant.GOOGLE_PLACE_API_KEY);

        call.enqueue(new Callback<RestaurantDetails>() {
            @Override
            public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                RestaurantDetails restaurantOutputPojo = response.body();

                Log.e("hotelsizeresponse", "" + response);

                progress.setVisibility(View.GONE);

                if (response.isSuccessful()) {

                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {


                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();


                        progress.setVisibility(View.GONE);

                        Log.e("hotel_size", "" + restaurantOutputPojo.getGetRestaurantsResults().size());

                        for (int i = 0; i < restaurantOutputPojo.getGetRestaurantsResults().size(); i++) {


                            String placeid = restaurantOutputPojo.getGetRestaurantsResults().get(i).getPlaceId();
                            String Icon = restaurantOutputPojo.getGetRestaurantsResults().get(i).getIcon();
                            String hotel_name = restaurantOutputPojo.getGetRestaurantsResults().get(i).getName();
                            String Id = restaurantOutputPojo.getGetRestaurantsResults().get(i).getId();

                            String reference = restaurantOutputPojo.getGetRestaurantsResults().get(i).getReference();

                            String address = restaurantOutputPojo.getGetRestaurantsResults().get(i).getFormattedAddress();
//                            String scope = restaurantOutputPojo.getGetRestaurantsResults().get(i).getScope();
                            String rating = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRating();

                            List<RestaurantPhoto> restaurantPhotos = new ArrayList<>();

                            restaurantPhotos = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos();

                            try {

                                String photo_ref = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();
                                int width = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                int height = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();

                                RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
                                Log.e("PhotosCount", "Photos->: " + photo_ref.substring(0, 20) + " " + i);
                                Log.e("height1", "width1: " + width);
                                Log.e("height1", "height1: " + height);
                                photos.add(restaurantPhoto);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            for (int kk = 0; kk < restaurantOutputPojo.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantOutputPojo.getGetRestaurantsResults().get(i).getTypes().toString());
                            }

                            if (restaurantPhotos != null) {
                                GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                        hotel_name, openingHoursPojo, restaurantPhotos, placeid, rating, reference, types, address);
                                getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);
                            }


                            //  hotel_historical_recylerview.setHorizontalScrollBarEnabled(true);
                            //  restaurantSearchHisMapAdapter.notifyDataSetChanged();


                        }

                        RestaurantSearchHisMapRestNameAdapter restaurantSearchHisMapAdapter = new RestaurantSearchHisMapRestNameAdapter(CreateHostoricalMapRestname.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList, CreateHostoricalMapRestname.this);
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(CreateHostoricalMapRestname.this, LinearLayoutManager.VERTICAL, false);
                        hotel_historical_recylerview.setLayoutManager(mLayoutManager);
                        mLayoutManager.setStackFromEnd(false);
                        //mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                        hotel_historical_recylerview.setAdapter(restaurantSearchHisMapAdapter);
                        hotel_historical_recylerview.setItemAnimator(new DefaultItemAnimator());
                        hotel_historical_recylerview.setNestedScrollingEnabled(false);

                    } else {
                        getallRestaurantDetailsPojoArrayList.clear();
                        Toast.makeText(CreateHostoricalMapRestname.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(CreateHostoricalMapRestname.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
                progress.setVisibility(View.GONE);
            }
        });


    }

    private void nearByRestaurantsStaticLatlong(double latitude, double longitude, String rediusChecking) {

        //progress.setVisibility(View.VISIBLE);

        latLngString = latitude + "," + longitude;

//        Call<GetNearByRestaurantOutputPojo> call = apiService.nearByRestaurants("restaurant", latLngString, "500", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        Call<RestaurantDetails> call = apiService.searchRestaurantsReviewWithnearby("", latLngString, rediusChecking, "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<RestaurantDetails>() {
            @Override
            public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                RestaurantDetails restaurantOutputPojo = response.body();

                sweetDialog.dismiss();

                if (response.isSuccessful()) {

                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {

                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();

                        progress.setVisibility(View.GONE);


                        Log.e("hotel_size", "" + restaurantOutputPojo.getGetRestaurantsResults().size());

                        for (int i = 0; i < restaurantOutputPojo.getGetRestaurantsResults().size(); i++) {


                            String placeid = restaurantOutputPojo.getGetRestaurantsResults().get(i).getPlaceId();
                            String Icon = restaurantOutputPojo.getGetRestaurantsResults().get(i).getIcon();
                            String hotel_name = restaurantOutputPojo.getGetRestaurantsResults().get(i).getName();
                            String Id = restaurantOutputPojo.getGetRestaurantsResults().get(i).getId();

                            String reference = restaurantOutputPojo.getGetRestaurantsResults().get(i).getReference();

                            String address = restaurantOutputPojo.getGetRestaurantsResults().get(i).getFormattedAddress();
//                            String scope = restaurantOutputPojo.getGetRestaurantsResults().get(i).getScope();
                            String rating = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRating();

                            List<RestaurantPhoto> restaurantPhotos = new ArrayList<>();

                            restaurantPhotos = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos();

                            try {

                                String photo_ref = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();
                                int width = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                int height = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();

                                RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
                                Log.e("PhotosCount", "Photos->: " + photo_ref.substring(0, 20) + " " + i);
                                Log.e("height1", "width1: " + width);
                                Log.e("height1", "height1: " + height);
                                photos.add(restaurantPhoto);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            for (int kk = 0; kk < restaurantOutputPojo.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantOutputPojo.getGetRestaurantsResults().get(i).getTypes().toString());
                            }

                            if (restaurantPhotos != null) {
                                GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                        hotel_name, openingHoursPojo, restaurantPhotos, placeid, rating, reference, types, address);
                                getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);
                            }
                        }

                        RestaurantSearchHisMapRestNameAdapter restaurantSearchHisMapAdapter = new RestaurantSearchHisMapRestNameAdapter(CreateHostoricalMapRestname.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList, CreateHostoricalMapRestname.this);
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(CreateHostoricalMapRestname.this, LinearLayoutManager.VERTICAL, false);
                        hotel_historical_recylerview.setLayoutManager(mLayoutManager);
                        mLayoutManager.setStackFromEnd(false);
                        //mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size());
                        hotel_historical_recylerview.setAdapter(restaurantSearchHisMapAdapter);
                        hotel_historical_recylerview.setItemAnimator(new DefaultItemAnimator());
                        hotel_historical_recylerview.setNestedScrollingEnabled(false);
                        //  hotel_historical_recylerview.setHorizontalScrollBarEnabled(true);
                        restaurantSearchHisMapAdapter.notifyDataSetChanged();

                    } else {

                        sweetDialog.dismiss();
                        getallRestaurantDetailsPojoArrayList.clear();
                        Toast.makeText(CreateHostoricalMapRestname.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else if (!response.isSuccessful()) {
                    sweetDialog.dismiss();
                    Toast.makeText(CreateHostoricalMapRestname.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                // Log error here since request failed
                call.cancel();

                sweetDialog.dismiss();
                //progress.setVisibility(View.GONE);
            }
        });


    }


//Getting followers list

    private void getFollowers(final int userId) {

        if (Utility.isConnected(CreateHostoricalMapRestname.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<GetFollowersOutput> call = apiService.getFollower("get_follower", userId);

                call.enqueue(new Callback<GetFollowersOutput>() {
                    @Override
                    public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                userid = response.body().getData().get(i).getUserid();
                                followerId = response.body().getData().get(i).getFollowerId();
                                firstName = response.body().getData().get(i).getFirstName();
                                lastName = response.body().getData().get(i).getLastName();
                                userPhoto = response.body().getData().get(i).getPicture();

                                if (!followerId.equals(String.valueOf(userid))) {

                                    GetFollowersData getFollowersData = new GetFollowersData(userid, followerId, firstName, lastName, userPhoto);
                                    getFollowersDataList.add(getFollowersData);

                                }


                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {
            Toast.makeText(this, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }

    }

    /*Interface for contact selecting*/
    public void onContactSelected(View view, GetFollowersData contact, int checkedStatus) {


        String followerName;
        followerName = contact.getFirstName() + " " + contact.getLastName();


        if (checkedStatus == 1) {

            Log.e("", "onContactSelected: " + checkedStatus);
            if (cohostNameIdListd.containsKey(Integer.valueOf(contact.getFollowerId()))) {

                return;

            } else {

                cohostNameIdListd.put(Integer.valueOf(contact.getFollowerId()), followerName);

            }


        } else if (checkedStatus == 0) {

//            Toast.makeText(this, ""+followerName, Toast.LENGTH_SHORT).show();


            Log.e("", "onContactSelected: " + checkedStatus);
            cohostNameIdListd.remove(Integer.valueOf(contact.getFollowerId()));
//            tempFollowersDataList.add(contact);

        }

        Log.e("", "onContactSelected: " + cohostNameIdListd.toString());


    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {


            case R.id.add_image_post:


                break;

            case R.id.add_people:


                break;

            case R.id.rl_create_post:


                break;

          /*  case R.id.close:

                //onBackPressed();
                finish();


                break;
*/

            case R.id.edit_restaurants:

                ll_search_restaurant.setVisibility(View.VISIBLE);
                ll_history_fields.setVisibility(View.GONE);
                rl_add_img_tag.setVisibility(View.GONE);

                break;

            case R.id.restaurantName:

                ll_search_restaurant.setVisibility(View.VISIBLE);
                ll_history_fields.setVisibility(View.GONE);
                rl_add_img_tag.setVisibility(View.GONE);

                break;

            case R.id.tv_add_photo:

                selectImage();

                break;

            case R.id.ic_add_photo:

                selectImage();

                break;


            case R.id.friendsListDisplayTag:


                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CreateHostoricalMapRestname.this);
                LayoutInflater inflater1 = LayoutInflater.from(CreateHostoricalMapRestname.this);
                @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.activity_select_followers, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setTitle("");

                follwersRecylerview = dialogView.findViewById(R.id.user_followers_recylerview);
                selectActionBar = dialogView.findViewById(R.id.action_bar_select_follower);
                selectClose = selectActionBar.findViewById(R.id.close);
                selectDone = selectActionBar.findViewById(R.id.tv_done);
                empty_list = dialogView.findViewById(R.id.empty_list);
                search_followers = dialogView.findViewById(R.id.search_followers);

                search_followers.setQueryHint("Search");

                search_followers.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted
                        historicalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        // filter recycler view when text is changed
                        historicalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }
                });


                if (getFollowersDataList.isEmpty()) {

                    empty_list.setVisibility(View.VISIBLE);
                    follwersRecylerview.setVisibility(View.GONE);

                } else {

                    empty_list.setVisibility(View.GONE);
                    follwersRecylerview.setVisibility(View.VISIBLE);
                }


                historicalTagPeopleAdapter = new HistoricalTagPeopleAdapter(CreateHostoricalMapRestname.this, getFollowersDataList, cohostNameIdListd, this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CreateHostoricalMapRestname.this);
                follwersRecylerview.setLayoutManager(mLayoutManager);
                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                follwersRecylerview.setAdapter(historicalTagPeopleAdapter);
                follwersRecylerview.hasFixedSize();
                historicalTagPeopleAdapter.notifyDataSetChanged();

                final AlertDialog dialog = dialogBuilder.create();
                dialog.show();

                selectClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Log.e("", "onClick: " + firstTimeTagged);
                        if (!firstTimeTagged) {
                            cohostNameIdListd.clear();
                            getFollowersDataList.clear();
                            getFollowers(userid);

                        }

                        /*if(firstTimeTagged){
                            getFollowersDataList.addAll(tempFollowersDataList);
                        }*/


                        dialog.dismiss();

                    }
                });

                selectDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        // Toast.makeText(CreateHostoricalMapRestname.this, ""+cohostNameIdListd.values().size(), Toast.LENGTH_SHORT).show();


                        if (cohostNameIdListd.values().size() == 0) {

                            ll_tag_people.setVisibility(View.VISIBLE);
                            friendsListDisplayTag.setText(R.string.tag_people);
                            friendsList.clear();


                        } else {

                            firstTimeTagged = true;
                            ll_tag_people.setVisibility(View.VISIBLE);
                            String[] friends = cohostNameIdListd.values().toArray(new String[cohostNameIdListd.values().size()]);

                            switch (cohostNameIdListd.values().size()) {

                                case 1:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1]);

                                    break;

                                case 2:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + friends[cohostNameIdListd.values().size() - 2]);

                                    break;
                            }

                            if (cohostNameIdListd.values().size() > 2) {

                                friendsListDisplayTag.setText("With - ");
                                friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + (cohostNameIdListd.values().size() - 1) + " Others");

                            }


                            Integer[] friendId = cohostNameIdListd.keySet().toArray(new Integer[cohostNameIdListd.keySet().size()]);
                            friendsList = new ArrayList<Integer>(Arrays.asList(friendId));

                        }

                        dialog.dismiss();

                    }
                });

                break;

            case R.id.ic_add_people:


                final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(CreateHostoricalMapRestname.this);
                LayoutInflater inflater2 = LayoutInflater.from(CreateHostoricalMapRestname.this);
                @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.activity_select_followers, null);
                dialogBuilder1.setView(dialogView1);
                dialogBuilder1.setTitle("");

                follwersRecylerview = dialogView1.findViewById(R.id.user_followers_recylerview);
                selectActionBar = dialogView1.findViewById(R.id.action_bar_select_follower);
                selectClose = selectActionBar.findViewById(R.id.close);
                selectDone = selectActionBar.findViewById(R.id.tv_done);
                empty_list = dialogView1.findViewById(R.id.empty_list);
                search_followers = dialogView1.findViewById(R.id.search_followers);

                search_followers.setQueryHint("Search");

                search_followers.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // filter recycler view when query submitted
                        historicalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        // filter recycler view when text is changed
                        historicalTagPeopleAdapter.getFilter().filter(query);
                        return false;
                    }
                });


                if (getFollowersDataList.isEmpty()) {

                    empty_list.setVisibility(View.VISIBLE);
                    follwersRecylerview.setVisibility(View.GONE);

                } else {

                    empty_list.setVisibility(View.GONE);
                    follwersRecylerview.setVisibility(View.VISIBLE);
                }


                historicalTagPeopleAdapter = new HistoricalTagPeopleAdapter(CreateHostoricalMapRestname.this, getFollowersDataList, cohostNameIdListd, this);
                RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(CreateHostoricalMapRestname.this);
                follwersRecylerview.setLayoutManager(mLayoutManager1);
                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                follwersRecylerview.setAdapter(historicalTagPeopleAdapter);
                follwersRecylerview.hasFixedSize();
                historicalTagPeopleAdapter.notifyDataSetChanged();

                final AlertDialog dialog1 = dialogBuilder1.create();
                dialog1.show();

                selectClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Log.e("", "onClick: " + firstTimeTagged);
                        if (!firstTimeTagged) {
                            cohostNameIdListd.clear();
                            getFollowersDataList.clear();
                            getFollowers(userid);

                        }

                        Log.e("", "onClick: " + cohostNameIdListd.toString());

                        dialog1.dismiss();

                    }
                });

                selectDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        // Toast.makeText(CreateHostoricalMapRestname.this, ""+cohostNameIdListd.values().size(), Toast.LENGTH_SHORT).show();


                        if (cohostNameIdListd.values().size() == 0) {

                            ll_tag_people.setVisibility(View.VISIBLE);
                            friendsListDisplayTag.setText(R.string.tag_people);
                            friendsList.clear();


                        } else {

                            firstTimeTagged = true;
                            ll_tag_people.setVisibility(View.VISIBLE);
                            String[] friends = cohostNameIdListd.values().toArray(new String[cohostNameIdListd.values().size()]);

                            switch (cohostNameIdListd.values().size()) {

                                case 1:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1]);

                                    break;

                                case 2:

                                    friendsListDisplayTag.setText("With - ");
                                    friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + friends[cohostNameIdListd.values().size() - 2]);

                                    break;
                            }

                            if (cohostNameIdListd.values().size() > 2) {

                                friendsListDisplayTag.setText("With - ");
                                friendsListDisplayTag.append(friends[cohostNameIdListd.values().size() - 1] + " and " + (cohostNameIdListd.values().size() - 1) + " Others");

                            }


                            Integer[] friendId = cohostNameIdListd.keySet().toArray(new Integer[cohostNameIdListd.keySet().size()]);
                            friendsList = new ArrayList<Integer>(Arrays.asList(friendId));

                        }

                        dialog1.dismiss();

                    }
                });


                break;


            case R.id.tv_create_history:


                if (isConnected(CreateHostoricalMapRestname.this)) {

                    if (validateFields()) {


                        if (friendsList.isEmpty() && cohostNameIdListd.values().isEmpty()) {

                            tag_status = 0;

                        } else {

                            tag_status = 1;
                        }

                        createPostDialog = new SpotsDialog.Builder().setContext(CreateHostoricalMapRestname.this).setMessage("Posting..").build();
                        createPostDialog.show();

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        userid = Integer.parseInt(Pref_storage.getDetail(CreateHostoricalMapRestname.this, "userId"));

                        String friendlist = new Gson().toJson(friendsList);
                        try {

                            Call<CommonOutputPojo> call = apiService.createTimeLinePost("create_timeline",
                                    0,
                                    google_id,
                                    google_photo,
                                    hotel_name,
                                    place_id, "",
                                    org.apache.commons.text.StringEscapeUtils.escapeJava(description_of_visit.getText().toString().trim()),
                                    tag_status,
                                    hotel_address,
                                    "",
                                    friendlist,
                                    latitude,
                                    longitude,
                                    userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {

                                            //Success
                                            sendHistoryImages(response.body().getData());


                                        }


                                    } else {

                                        Toast.makeText(CreateHostoricalMapRestname.this, "Something went wrong please try again", Toast.LENGTH_SHORT).show();
                                        createPostDialog.dismiss();
                                    }


                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());
                                    createPostDialog.dismiss();
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();
                            createPostDialog.dismiss();

                        }


                    }


                } else {


                    Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();

                }


                break;

            case R.id.date_of_restaurent_visit:

                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateHostoricalMapRestname.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String frmdate;

                                if ((monthOfYear + 1) > 9) {

                                    frmdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                } else {

                                    frmdate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;

                                }


                                date_of_restaurent_visit.setText(frmdate);
                                date_of_restaurent_visit.setError(null);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

                break;

        }

    }

    private boolean validateFields() {

        boolean valid = true;

        String hotelname = restaurantName.getText().toString();


        if (hotelname.length() == 0) {
            edit_search_hotel.setError("Please select a hotel");
            valid = false;
        } else {
            edit_search_hotel.setError(null);
        }


        int imageCount = imagesEncodedList.size();

        if (imageCount == 0) {

            Toast.makeText(this, "Please choose atleast one photo", Toast.LENGTH_SHORT).show();
            valid = false;

        } else {


        }


        return valid;
    }


    public void sendHistoryImages(int id) {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        userid = Integer.parseInt(Pref_storage.getDetail(CreateHostoricalMapRestname.this, "userId"));


        for (int i = 0; i < imagesEncodedList.size(); i++) {

            String image = imagesEncodedList.get(i).getImg();

            try {

                file = new File(image);

                File compressedImageFile = null;

                compressedImageFile = new Compressor(this).setQuality(100).compressToFile(file);

                ProgressRequestBody fileBody = new ProgressRequestBody(compressedImageFile, this);
                MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), fileBody);

                Call<CommonOutputPojo> request = apiService.createHistoricalImage("create_timeline_image", id, userid, filePart);

                request.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {
                        Log.e("", "onResponse: Images");
                        count++;

                        if (count == imagesEncodedList.size()) {

                            Log.e("", "onResponse: Image uploaded successfully");
                            createPostDialog.dismiss();
                            Intent intent = new Intent(CreateHostoricalMapRestname.this, Home.class);
                            startActivity(intent);
                            finish();


                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {

                        Log.e("", "onFailure: Images->" + t.getLocalizedMessage());

                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


    private void selectImage() {

        validate();

        List<String> listItems = new ArrayList<String>();

        listItems.add("Take Photo");
        listItems.add("Choose from library");
        listItems.add("Cancel");

        if (image_status == 1) {

            listItems.remove(2);
            listItems.add("Remove all photos");
            listItems.add("Cancel");

        }

        final CharSequence[] items = listItems.toArray(new CharSequence[listItems.size()]);

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(CreateHostoricalMapRestname.this);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(CreateHostoricalMapRestname.this);

                if (items[item].equals("Take Photo")) {


                    try {

                        if (ActivityCompat.checkSelfPermission(CreateHostoricalMapRestname.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(CreateHostoricalMapRestname.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(CreateHostoricalMapRestname.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {


                            Log.e("", "onClick: else part");

                            if (ActivityCompat.checkSelfPermission(CreateHostoricalMapRestname.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(CreateHostoricalMapRestname.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                                Log.e("", "onClick: camera");

                                cameraIntent();

                            } else {

                                Log.e("", "onClick: cameraStorageGrantRequest");

                                cameraStorageGrantRequest();

                            }


                        } else {

                            ActivityCompat.requestPermissions(CreateHostoricalMapRestname.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (items[item].equals("Choose from library")) {
                    if (result) {

                        imagepath = null;
                        galleryIntent();

                    }


                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                } else if (items[item].equals("Remove all photos")) {

                    image_status = 0;
                    imagepath = null;
                    imagesEncodedList.clear();
//                    init();
                    ll_history_image_section.setVisibility(View.GONE);
                    tv_add_photo.setText(R.string.add_photos);

                }
            }
        });
        builder.show();
    }

    private void validate() {

        if (imagepath == null && imagesEncodedList.isEmpty()) {

            image_status = 0;

        } else if (!imagesEncodedList.isEmpty()) {

            image_status = 1;

        }

    }


    private void init() {

        if (imagesEncodedList.size() == 0) {

            ll_history_image_section.setVisibility(View.GONE);
            tv_add_photo.setText(R.string.add_photos);

        } else {

            ll_history_image_section.setVisibility(View.VISIBLE);
            tv_add_photo.setText("Edit photos");
            mviewpager.setAdapter(new HistoryImagesAdapter(CreateHostoricalMapRestname.this, imagesEncodedList, this));
            circleIndicator.setViewPager(mviewpager);
            NUM_PAGES = imagesEncodedList.size();
// Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    mviewpager.setCurrentItem(currentPage++, true);
                }
            };

            // Pager listener over indicator
            circleIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;

                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int pos) {

                }
            });

        }

    }

    @Override
    public void removedImageLisenter(List<Image> imagesList, int position) {

        imagesEncodedList.remove(position);
        Log.e("", "removedImageLisenter: " + imagesEncodedList.size());
        init();


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        Log.d("onActivityResult()", "resultCode" + resultCode);
        Log.d("onActivityResult()", "requestCode" + requestCode);
        Log.d("onActivityResult()", "data" + data);


        switch (requestCode) {

            case 1:

                switch (resultCode) {

                    case Activity.RESULT_OK: {

                        Log.v("isGPSEnabled", "latitude->" + latitude);
                        Log.v("isGPSEnabled", "longitude->" + longitude);

                        if (Utility.isConnected(CreateHostoricalMapRestname.this)) {

                            sweetDialog = new SpotsDialog.Builder().setContext(CreateHostoricalMapRestname.this).setMessage("").build();
                            sweetDialog.setCancelable(false);
                            sweetDialog.show();
                            Thread thread = new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        sleep(7000);
                                        String rediusChecking = "500";
                                        Log.e("isGPSEnabled", "latitude->" + latitude);
                                        Log.e("isGPSEnabled", "longitude->" + longitude);
                                        nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            };
                            thread.start();

                        } else {

                            Toast.makeText(CreateHostoricalMapRestname.this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        // Toast.makeText(getApplicationContext(), "Location not enabled.", Toast.LENGTH_LONG).show();
                        //call restaurant api
                        //  get_hotel_static_location();

                        if (Utility.isConnected(CreateHostoricalMapRestname.this)) {

                            sweetDialog = new SpotsDialog.Builder().setContext(CreateHostoricalMapRestname.this).setMessage("").build();
                            sweetDialog.setCancelable(false);
                            sweetDialog.show();
                            Call<Get_HotelStaticLocation_Response> call = apiInterface.get_hotel_static_location("get_hotel_static_location", userid);
                            call.enqueue(new Callback<Get_HotelStaticLocation_Response>() {
                                @Override
                                public void onResponse(Call<Get_HotelStaticLocation_Response> call, Response<Get_HotelStaticLocation_Response> response) {


                                    if (response.isSuccessful()) {


                                        double latitude = response.body().getData().get(0).getLatitude();
                                        double longitude = response.body().getData().get(0).getLongitude();
                                        String rediusChecking = "500";

                                        nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);

                                    } else {

                                        Toast.makeText(CreateHostoricalMapRestname.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Get_HotelStaticLocation_Response> call, Throwable t) {
                                    // Log error here since request failed
                                    call.cancel();
                                    sweetDialog.dismiss();
                                }
                            });

                            break;
                        } else {

                            Toast.makeText(this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    default: {
                        break;
                    }
                }
                break;

            default: {
                break;
            }
        }

        try {

            if (requestCode == REQUEST_CAMERA) {

                onCaptureImageResult(data);

            }
            // When an Image is picked
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && null != data && !(data.toString().equals("Intent { (has extras) }"))) {
                // Get the Image from data

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                if (data.getData() != null) {


                    Uri mImageUri = data.getData();
                    Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                    cursor.moveToFirst();
                    String document_id = cursor.getString(0);
                    document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                    cursor.close();

                    cursor = getContentResolver().query(
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                    cursor.moveToFirst();
                    imageEncoded = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    cursor.close();

                    Image image = new Image(imageEncoded);
                    imagesEncodedList.add(image);
                    init();

                    Log.v("LOG_TAG", "Selected Images" + imageEncoded);

                } else {

                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);

                            // Get the cursor

                            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                            cursor.moveToFirst();
                            String document_id = cursor.getString(0);
                            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                            cursor.close();

                            cursor = getContentResolver().query(
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                            cursor.moveToFirst();
                            imageEncoded = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                            Image image = new Image(imageEncoded);
                            imagesEncodedList.add(image);
                            init();

                            cursor.close();


                            Log.v("LOG_TAG", "Selected Images" + imagesEncodedList);
                        }
                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }
                }
            } else {
                /* Toast.makeText(this, "You haven't picked Image",Toast.LENGTH_LONG).show();*/
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }


    }


    private void cameraStorageGrantRequest() {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                CreateHostoricalMapRestname.this);

        builder.setTitle("Food Wall would like to access your camera and storage")
                .setMessage("You previously chose not to use Android's camera or storage with Food Wall. To upload your photos, allow camera and storage permission in App Settings. Click Ok to allow.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // permission dialog
                        dialog.dismiss();
                        try {

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();


    }

    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = null;
        try {
            thumbnail = getThumbnail(imageUri);
            saveBitmap(thumbnail);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("CameraCaptureError", "CameraCaptureError->" + e.getMessage());
        }


    }

    private void saveBitmap(Bitmap bm) {


        String folder_main = "FoodWall";

        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }

        File newFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        Log.e("ImageFile", "ImageFile->" + newFile);

        try {

            FileOutputStream fileOutputStream = new FileOutputStream(newFile);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            imagepath = String.valueOf(newFile);
            Image image = new Image(imagepath);
            imagesEncodedList.add(image);
            init();
            Log.e("MyPath", "MyImagePath->" + imagepath);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void cameraIntent() {


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, REQUEST_CAMERA);

        } else {


            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);

        }


    }

    public Bitmap getThumbnail(Uri uri) throws IOException {

        InputStream input = CreateHostoricalMapRestname.this.getContentResolver().openInputStream(uri);
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 800) ? (originalSize / 800) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }


    private void galleryIntent() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_GALLERY_PERMISSION);
        } else {

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);

        }

    }

    /* Check Location Permission for Marshmallow Devices */
    private void checkPermissions() {

        int permissionLocation = ContextCompat.checkSelfPermission(CreateHostoricalMapRestname.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                getMyLocation();
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), ACCESS_FINE_LOCATION_INTENT_ID);
            }
        } else {
            if (gpsTracker.isGPSEnabled) {
                permissionCheckingCreateView();
            } else {
                getMyLocation();
            }
        }
    }


    private void permissionCheckingCreateView() {

        if (Utility.isConnected(CreateHostoricalMapRestname.this)) {

            getMyLocation();
            sweetDialog = new SpotsDialog.Builder().setContext(CreateHostoricalMapRestname.this).setMessage("").build();
            sweetDialog.setCancelable(false);
            sweetDialog.show();
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        sleep(7000);
                        String rediusChecking = "500";

                        nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            thread.start();


        } else {

            Toast.makeText(CreateHostoricalMapRestname.this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == ACCESS_FINE_LOCATION_INTENT_ID) {

            int permissionSize = permissions.length;
            for (int i = 0; i < permissionSize; i++) {

                if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                    if (gpsTracker.isGPSEnabled) {

                        getMyLocation();
                        sweetDialog = new SpotsDialog.Builder().setContext(CreateHostoricalMapRestname.this).setMessage("").build();
                        sweetDialog.setCancelable(false);
                        sweetDialog.show();
                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(7000);
                                    String rediusChecking = "500";
                                    nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        thread.start();

                    } else {

                        getMyLocation();
                    }

                } else if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                    if (Utility.isConnected(CreateHostoricalMapRestname.this)) {

                        sweetDialog = new SpotsDialog.Builder().setContext(CreateHostoricalMapRestname.this).setMessage("").build();
                        sweetDialog.setCancelable(false);
                        sweetDialog.show();
                        Call<Get_HotelStaticLocation_Response> call = apiInterface.get_hotel_static_location("get_hotel_static_location", userid);
                        call.enqueue(new Callback<Get_HotelStaticLocation_Response>() {
                            @Override
                            public void onResponse(Call<Get_HotelStaticLocation_Response> call, Response<Get_HotelStaticLocation_Response> response) {


                                if (response.isSuccessful()) {


                                    double latitude = response.body().getData().get(0).getLatitude();
                                    double longitude = response.body().getData().get(0).getLongitude();
                                    String radiusChecking = "5000";

                                    nearByRestaurantsStaticLatlong(latitude, longitude, radiusChecking);

                                } else {

                                    Toast.makeText(CreateHostoricalMapRestname.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Get_HotelStaticLocation_Response> call, Throwable t) {
                                // Log error here since request failed
                                call.cancel();

                                sweetDialog.dismiss();
                            }
                        });
                    } else {

                        Toast.makeText(this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
                    }
                }

            }

        }

        int permissionSize = permissions.length;

        Log.e("", "onRequestPermissionsResult:" + "requestCode->" + requestCode);

        switch (requestCode) {


            case 123:

                cameraStorageGrantRequest();

                break;

            case REQUEST_CAMERA_PERMISSION:

                for (int i = 0; i < permissionSize; i++) {

                    if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e("", "onRequestPermissionsResult:" + "CAMERA permission granted");

                    } else if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e("", "onRequestPermissionsResult:" + "CAMERA permission denied");
                        cameraStorageGrantRequest();
                    }

                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e("", "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");

                        if (ActivityCompat.checkSelfPermission(CreateHostoricalMapRestname.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraIntent();

                        }


                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e("", "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");

                        if (ActivityCompat.checkSelfPermission(CreateHostoricalMapRestname.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraStorageGrantRequest();

                        }

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e("", "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e("", "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

            case REQUEST_GALLERY_PERMISSION:

                for (int i = 0; i < permissionSize; i++) {


                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e("", "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");
                        galleryIntent();

                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e("", "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");
                        cameraStorageGrantRequest();

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e("", "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e("", "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

        }
    }

    private void showSettingDialog() {


        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.


                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(CreateHostoricalMapRestname.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    /*  Show Popup to access User Permission  */
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(CreateHostoricalMapRestname.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(CreateHostoricalMapRestname.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(CreateHostoricalMapRestname.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    //Run on UI
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            showSettingDialog();
        }
    };

    /* Broadcast receiver to check status of GPS */
    private BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //If Action is Location
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                //Check if GPS is turned ON or OFF
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Log.e("About GPS", "GPS is Enabled in your device");
                } else {
                    //If GPS turned OFF show Location Dialog
                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    // showSettingDialog();
                    Log.e("About GPS", "GPS is Disabled in your device");
                }

            }
        }
    };


    public synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            return (mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting());
        } else
            return false;
    }

    @Override
    public void onTagsChanged(Collection<String> tags) {

        /*if (tags.size() == 0) {

//            friendsListDisplay.setVisibility(View.GONE);

        } else {

//            friendsListDisplay.setVisibility(View.VISIBLE);

        }

        cohostList.clear();

        Log.d("FollowersId", "Tags changed: ");
        Log.d("FollowersId", Arrays.toString(tags.toArray()));

        Object[] ob = tags.toArray();

        for (Object value : ob) {

            System.out.println("Number = " + value);
            cohostList.add(cohostNameIdListd.get(value));

        }

        Iterator<String> iterator = cohostNameIdListd.values().iterator();
        while (iterator.hasNext()) {

            String certification = iterator.next();

            if (tags.contains(certification)) {

                Log.e("EventCheck", "EventCheck->" + certification);

            } else {

                Log.e("EventCheck", "Not Found");
                iterator.remove();

            }

        }

//        Toast.makeText(this, "" + cohostNameIdListd.values().toString(), Toast.LENGTH_SHORT).show();
*/

    }

    @Override
    public void onEditingFinished() {
        Log.d("FollowersId", "OnEditing finished");
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(friendsListDisplay.getWindowToken(), 0);
        friendsListDisplay.clearFocus();
    }


    @Override
    public void onProgressUpdate(int percentage) {
        Log.e("", "onProgressUpdate: " + percentage);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }


    @Override
    public void onRefreshcallback(View view, final int adapterPosition, View viewItem) {

        progress_dialog = viewItem.findViewById(R.id.progress_dialog);

        progress.setVisibility(View.VISIBLE);
        progress.setIndeterminate(true);

        // Simulate HTTP connection
        new Thread(new Runnable() {
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        google_id = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getId();
                        google_photo = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getRestaurantPhotos().get(0).getPhotoReference();
                        place_id = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getPlaceId();
                        hotel_name = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getName();
                        hotel_address = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getFormattedAddress();

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(friendsListDisplay.getWindowToken(), 0);
                        // ll_search_restaurant.setVisibility(View.GONE);
                        // ll_history_fields.setVisibility(View.VISIBLE);
                        restaurantName.setText(getallRestaurantDetailsPojoArrayList.get(adapterPosition).getName());

                        location = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getName();
                        String location = getallRestaurantDetailsPojoArrayList.get(adapterPosition).getFormattedAddress();

                        Log.e("currentaddress", "Latlong:location " + location);

                        //get city,area from full address
                        getLocationFromAddress(CreateHostoricalMapRestname.this, location);

                        //here you can modify UI here
                        //modifiying UI element happen here and at the end you cancel the progress dialog

                    }
                }); // runOnUIthread
            }
        }).start();
    }


    public void getLocationFromAddress(Context context, String strAddress) {

        progress.setVisibility(View.VISIBLE);
        progress.setIndeterminate(true);

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return;
            }

            Address locationn = address.get(0);
            p1 = new LatLng(locationn.getLatitude(), locationn.getLongitude());

            Log.e("currentaddress", "Latlong: " + p1);

            //get city , area from latlong
            //  getAddressFromLocation(p1.latitude, p1.longitude);
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(p1.latitude, p1.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5


                String addresss = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String area = addresses.get(0).getSubLocality();
                String premises = addresses.get(0).getPremises();
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                Intent intent = new Intent(CreateHostoricalMapRestname.this, Story_full_image_activity.class);
                intent.putExtra("file_camera", file_camera);
                intent.putExtra("file_gallery", file_gallery);
                intent.putExtra("location", location);
                intent.putExtra("area", area);
                intent.putExtra("city", city);
                intent.putExtra("story_caption", story_caption);
                startActivity(intent);
                //sweetDialog.dismiss();
                //finish();

                //progress_dialog.setVisibility(View.GONE);
                //progress_dialog.setIndeterminate(false);

                Log.e("currentaddress", "getAddressFromLocation: " + addresss);
                Log.e("currentaddress", "getAddressFromLocation: " + area);
                Log.e("currentaddress", "getAddressFromLocation: " + city);
                Log.e("currentaddress", "getAddressFromLocation: " + knownName);
                Log.e("currentaddress", "getAddressFromLocation: " + premises);

            } catch (IOException e) {
                e.printStackTrace();
            }


        } catch (IOException ex) {

            ex.printStackTrace();
        }

        //return p1;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            latitude = mylocation.getLatitude();
            longitude = mylocation.getLongitude();
            Log.e("latttttt", "onCreate: " + latitude);
            Log.e("latttttt", "onCreate: " + longitude);

        } else {

            getMyLocation();
        }
    }


    private void getMyLocation() {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(CreateHostoricalMapRestname.this, Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, CreateHostoricalMapRestname.this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    mylocation = LocationServices.FusedLocationApi
                                            .getLastLocation(googleApiClient);
                                    break;

                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(CreateHostoricalMapRestname.this,
                                                REQUEST_CHECK_SETTINGS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }
}