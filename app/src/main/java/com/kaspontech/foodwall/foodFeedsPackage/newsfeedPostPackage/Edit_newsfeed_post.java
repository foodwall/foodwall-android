package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.adapters.TimeLine.timelinefeed_adapter;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_edittimeline_datas_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_edittimeline_output_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Post_model;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

/**
 * Created by vishnukm on 31/3/18.
 */

public class Edit_newsfeed_post  extends AppCompatActivity implements View.OnClickListener {

    View view;
    private static final String TAG = "Edit_newsfeed_post";

    // Action Bar
    Toolbar actionBar;
    ImageButton back;
    Pref_storage pref_storage;
    LinearLayout ll_camera;
    LinearLayoutManager llm;
    ImageView img_camera,img_post;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private static final int PICK_FROM_GALLERY = 1;
    /* public  static ImageView img_post;*/
    TextView txt_camera, btn_post,toolbar_title,toolbar_post;
    EditText edit_writepost, edit_addlocation;
    public static String captionresult, locationresult;
    private static final int NUM_GRID_COLUMNS = 3;
    ArrayList<Post_model> gettingtimelinelist = new ArrayList<>();

    timelinefeed_adapter timeline_adapter;
    RecyclerView rv_newsfeed;
    Get_edittimeline_datas_pojo getEdittimelineDatasPojo;
    Calendar calander;
    Intent data;
    String Date;
    SimpleDateFormat simpledateformat;
    GPSTracker gpsTracker;

    double newlatituderesult,newlongituderesult;
    RelativeLayout rl_add_location, rl_editlocation,rl_postlayoutforedit;

    //widgets
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_post_layout);
        actionBar = findViewById(R.id.actionbar_write_post);
        toolbar_title=(TextView)findViewById(R.id.toolbar_title);
        toolbar_title.setText(R.string.editpost);
        pref_storage = new Pref_storage();
        gpsTracker = new GPSTracker(this);

        back = (ImageButton) actionBar.findViewById(R.id.button_back);
        back.setOnClickListener(this);
        img_camera = (ImageView) findViewById(R.id.img_post);

        edit_writepost = (EditText) findViewById(R.id.edit_writepost);
        edit_addlocation = (EditText)findViewById(R.id.edit_addlocation);

        rl_add_location =(RelativeLayout) findViewById(R.id.rl_add_location);
        rl_add_location.setOnClickListener(this);


        rl_editlocation = (RelativeLayout)findViewById(R.id.rl_editlocation);
        rl_editlocation.setOnClickListener(this);

        rl_postlayoutforedit =(RelativeLayout)findViewById(R.id.rl_postlayoutforedit);

       try {



           newlatituderesult = gpsTracker.getLatitude();
//           Toast.makeText(this, "Lati\n"+newlatituderesult, Toast.LENGTH_SHORT).show();
           newlongituderesult = gpsTracker.getLongitude();
//            img_camera.setImageURI(Uri.parse(Pref_storage.getDetail(getApplicationContext(), "UserImage")));
//

        } catch (Exception e) {
            e.printStackTrace();
        }

        ll_camera = (LinearLayout) findViewById(R.id.ll_image_layout);
        ll_camera.setOnClickListener(this);
        btn_post = findViewById(R.id.btn_post);
        toolbar_post = (TextView)actionBar.findViewById(R.id.toolbar_post);
        toolbar_post.setText(R.string.repost);
        toolbar_post.setOnClickListener(this);

        get_timeline_user();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ll_image_layout:
                camera_galleryalert();

                break;

            case R.id.button_back:
                finish();
                break;
            case R.id.rl_add_location:
                rl_editlocation.setVisibility(View.VISIBLE);
                break;

            case R.id.toolbar_post:
                captionresult = edit_writepost.getText().toString();
                locationresult = edit_addlocation.getText().toString();
                createuser_timeline_data();
                break;
        }

    }

    private void get_timeline_user() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final String createuserid= Pref_storage.getDetail(Edit_newsfeed_post.this,"userId");
        Call<Get_edittimeline_output_pojo> call = apiService.get_timeline_user("get_timeline_user",0,Integer.parseInt(createuserid));


        call.enqueue(new Callback<Get_edittimeline_output_pojo>() {
            @Override
            public void onResponse(Call<Get_edittimeline_output_pojo> call, Response<Get_edittimeline_output_pojo> response) {


                if (response.body().getResponseCode()==1) {

                    for (int j = 0; j < response.body().getData().size(); j++) {



                        String timelineId = response.body().getData().get(j).getTimeline_id();


                        Pref_storage.setDetail(Edit_newsfeed_post.this,"timelineid", String.valueOf(timelineId));
                        String  timelineDescription = response.body().getData().get(j).getTimeline_description().replace("\"","");
                        String  picture = response.body().getData().get(j).getPicture();
                        Pref_storage.setDetail(Edit_newsfeed_post.this,"Edit_post_picture",picture);

                        if (Pref_storage.getDetail(Edit_newsfeed_post.this,"clicked_timelineID").equalsIgnoreCase(timelineId)){
                            edit_writepost.setText(timelineDescription);
                            Utility.picassoImageLoader(picture,
                                    0,img_camera, getApplicationContext());

//                            GlideApp.with(Edit_newsfeed_post.this)
//                                    .load(picture)
//                                    .into(img_camera);



                        }





//                        String  createdBy = response.body().getData().get(j).getCreated_by();
//                        String createdOn = response.body().getData().get(j).getCreated_on();
//                        String totalLikes = response.body().getData().get(j).getTotal_likes();
//                        String totalComments = response.body().getData().get(j).getTotal_comments();
//                        String userId = response.body().getData().get(j).getUser_id();
//
//                        String firstName1 = response.body().getData().get(j).getFirst_name();
//                        String lastName1 = response.body().getData().get(j).getLast_name();



//                        for (int jj = 0; jj < response.body().getData().size(); jj++) {
//                            Toast.makeText(Edit_newsfeed_post.this, ""+response.body().getData().get(j).getDataimage().get(jj).getImage(), Toast.LENGTH_SHORT).show();
//                           /* String imageurlnew= response.body().getData().get(j).getDataimage().get(jj).getImage();
//                            Pref_storage.setDetail(Edit_newsfeed_post.this,"imageurlnew",imageurlnew);*/
//                        }








                    }

                }


            }

            @Override
            public void onFailure(Call<Get_edittimeline_output_pojo> call, Throwable t) {
                //Error
                Log.e("FailureError",""+t.getMessage());
            }
        });


    }


    public void camera_galleryalert(){

        final CharSequence[] items = {"Camera", "Select from Gallery",
                "Cancel"};

        AlertDialog.Builder front_builder = new AlertDialog.Builder(this);
        front_builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (items[which].equals("Camera")) {


                    try {
                        if (ActivityCompat.checkSelfPermission(Edit_newsfeed_post.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(Edit_newsfeed_post.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(Edit_newsfeed_post.this, new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);

                        } else {

                            cameraIntent();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else if (items[which].equals("Select from Gallery")) {

                    try {
                        if (ActivityCompat.checkSelfPermission(Edit_newsfeed_post.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(Edit_newsfeed_post.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);

                        } else {

                            galleryIntent();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                } else if (items[which].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        front_builder.show();
    }


    private void galleryIntent() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                String result= data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == REQUEST_CAMERA) {

                Bitmap photo1 = (Bitmap) data.getExtras().get("data");
                img_camera.setImageBitmap(photo1);

                Uri tempUri = getImageUri(getApplicationContext(), photo1);

                File f = new File(getRealPathFromURI(tempUri));

                Pref_storage.setDetail(getApplicationContext(), "Edit_post_picture",getRealPathFromURI(tempUri));


            } else if (requestCode == SELECT_FILE) {

                try {
                    onSelectFromGalleryResult(data);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Uri selectedImage = data.getData();


        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor c = getApplicationContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);

        c.moveToFirst();


        int columnIndex = c.getColumnIndex(filePathColumn[0]);
        String picturePath = c.getString(columnIndex);
        c.close();






//        Bitmap photo1 = (BitmapFactory.decodeFile(picturePath));
//        img_camera.setImageBitmap(photo1);

        Pref_storage.setDetail(getApplicationContext(), "Edit_post_picture",getRealPathFromURI(Uri.parse(picturePath)) /*picturePath*/);

        img_camera.setImageURI(Uri.parse(picturePath));

    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getApplicationContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
//            Toast.makeText(mContext, "vis"+result, Toast.LENGTH_SHORT).show();
            cursor.close();
        }
        return result;

    }

    private void createuser_timeline_data() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {



            String user = Pref_storage.getDetail(Edit_newsfeed_post.this, "Edit_post_picture");

            File sourceFile = new File(user);
            Log.e("path", "fdsfds" + sourceFile.toString());

          String clickedtimelineID=  Pref_storage.getDetail(Edit_newsfeed_post.this,"clicked_timelineID");


            RequestBody imageupload = RequestBody.create(MediaType.parse("Image/jpeg"), sourceFile);

            MultipartBody.Part image = MultipartBody.Part.createFormData("image", sourceFile.getName(), imageupload);
            String createdby = Pref_storage.getDetail(this, "userId");
            RequestBody created_by =RequestBody.create(MediaType.parse("text/plain"),createdby);
            String timeline_descriptionnew = edit_writepost.getText().toString();
            RequestBody timeline_description =RequestBody.create(MediaType.parse("text/plain"),timeline_descriptionnew);
            Call<CreateUserPojoOutput> call = apiService.create_timeline_user("create_timeline", Integer.parseInt(clickedtimelineID), timeline_description, newlatituderesult, newlongituderesult, created_by);
            call.enqueue(new Callback<CreateUserPojoOutput>() {
                @Override
                public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                    if (response.body().getResponseCode()==1) {
                        //Success
                        startActivity(new Intent(Edit_newsfeed_post.this, Home.class));
//                        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                        finish();
                    }else{
                        Snackbar snackbar = Snackbar.make(rl_postlayoutforedit, "Adding post failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        View view1 = snackbar.getView();
                        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                        textview.setTextColor(Color.WHITE);
                        snackbar.show();
                    }
                }

                @Override
                public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {

                    if (t.getMessage().contains("(No such file or directory)")){
                        Snackbar snackbar = Snackbar.make(rl_postlayoutforedit, "Image required to be reloaded", Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        View view1 = snackbar.getView();
                        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                        textview.setTextColor(Color.WHITE);
                        snackbar.show();
                    }
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void AddressFinder(double latitude, double longitude) {


        Geocoder geocoder;
        List<Address> addresses = new ArrayList<>();
        geocoder = new Geocoder(this, Locale.getDefault());

        try {


            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            newlatituderesult = addresses.get(0).getLatitude();


            newlongituderesult = addresses.get(0).getLongitude();


        } catch (IOException e) {

            e.printStackTrace();

        }
    }

}
