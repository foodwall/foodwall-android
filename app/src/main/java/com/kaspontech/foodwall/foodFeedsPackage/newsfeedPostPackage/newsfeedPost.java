package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.adapters.TimeLine.timelinefeed_adapter;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Post_model;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Add_photo_for_post.imagesEncodedList;

//

/**
 * Created by vishnukm on 6/3/18.
 */

public class newsfeedPost extends AppCompatActivity implements View.OnClickListener {
    View view;
    private static final String TAG = "newsfeedPost";

    // Action Bar
    Toolbar actionBar;
    ImageButton back;
    Pref_storage pref_storage;
    LinearLayout ll_camera;
    LinearLayoutManager llm;
    ImageView img_camera;
    /* public  static ImageView img_post;*/
    TextView txt_camera, btn_post, toolbar_post;
    EditText edit_writepost, edit_addlocation;
    public static String captionresult, locationresult;
    private static final int NUM_GRID_COLUMNS = 3;
    ArrayList<Post_model> gettingtimelinelist = new ArrayList<>();

    timelinefeed_adapter timeline_adapter;
    RecyclerView rv_newsfeed;
    Calendar calander;
    Intent data;
    String Date;
    SimpleDateFormat simpledateformat;
    double newlatituderesult, newlongituderesult;
    RelativeLayout rl_add_location, rl_editlocation;
    ProgressBar progress_dialog;
    ProgressWheel progressWheel;
    GPSTracker gpsTracker;
    String path, from_camera, from_gallery, photo_upload;
    File sourceFile;
    JSONObject student1;
    String[] test;
    //File
    File file;
    MultipartBody.Part multipart;

    ArrayList<String> newimage = new ArrayList<>();
    ArrayList<String> filePaths = new ArrayList<>();
    //widgets
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_post_layout);
        actionBar = findViewById(R.id.actionbar_write_post);
        pref_storage = new Pref_storage();
        gpsTracker = new GPSTracker(this);
        back = actionBar.findViewById(R.id.button_back);
        back.setOnClickListener(this);
        img_camera = findViewById(R.id.img_post);

        progress_dialog = (ProgressBar) findViewById(R.id.progress_dialog_post);

//        progressWheel = (ProgressWheel)findViewById(R.id.pro_wheel);

        toolbar_post = (TextView) actionBar.findViewById(R.id.toolbar_post);
        toolbar_post.setOnClickListener(this);

        edit_writepost = findViewById(R.id.edit_writepost);
        edit_addlocation = findViewById(R.id.edit_addlocation);

        rl_add_location = findViewById(R.id.rl_add_location);
        rl_add_location.setOnClickListener(this);


        rl_editlocation = findViewById(R.id.rl_editlocation);
        rl_editlocation.setOnClickListener(this);

        try {

            newlatituderesult = gpsTracker.getLatitude();

            newlongituderesult = gpsTracker.getLongitude();

//            AddressFinder(newlatituderesult,newlongituderesult );
        } catch (Exception e) {
            e.printStackTrace();
        }


        Intent i = getIntent();

        from_camera = i.getStringExtra("fromcamera");

        from_gallery = i.getStringExtra("fromgallery");


        student1 = new JSONObject();
        try {

//            student1.put("image", from_gallery);
            student1.put("", from_gallery.replace("[", "").replace("]", ""));


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();


        }

/*        String[] namesList = from_gallery.split(",");
        for (int a=0;a<namesList.length;a++) {
            student1= new JSONObject();
            try {
                student1.put("", from_gallery);


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();


            }
        }*/
        Log.e(TAG, "image_length" + student1);


        if (from_camera == null) {

//            Utility.picassoImageLoader(from_gallery.replace("[", "").replace("]", ""),
//                    1,img_camera, getApplicationContext());

            GlideApp.with(newsfeedPost.this)
                    .load(from_gallery.replace("[", "").replace("]", ""))
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.1f)
                    .into(img_camera);

        } else if (!from_camera.equalsIgnoreCase("")) {
            Uri uri = Uri.parse(from_camera);
            img_camera.setImageURI(uri);
        } else if (from_gallery == null) {
            Uri uri = Uri.parse(from_camera);
            img_camera.setImageURI(uri);
        }


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.button_back:
                finish();
                break;
            case R.id.rl_add_location:
                rl_editlocation.setVisibility(View.VISIBLE);
                break;

            case R.id.toolbar_post:
                captionresult = edit_writepost.getText().toString();
                locationresult = edit_addlocation.getText().toString();

//                progressWheel.setVisibility(View.VISIBLE);
                progress_dialog.setIndeterminate(true);
                progress_dialog.setVisibility(View.VISIBLE);

                if (edit_writepost.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Caption field is empty", Toast.LENGTH_SHORT).show();
                } else {


                    createuser_timeline_data();
                }


                break;
        }

    }

    private void createuser_timeline_data() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

           /* ArrayList<String> multiimage= new ArrayList<>();
//            multiimage.add("{"+path+"}".concat(",").concat("{"+usernew+"}"));
            multiimage.add(path.concat(",").concat(usernew));

            if( user==null||user.equalsIgnoreCase("")){

//                sourceFile= new File(path);
                sourceFile=new File(String.valueOf(multiimage));

            } else {
                 sourceFile = new File(user);
                sourceFile=new File(String.valueOf(multiimage));
            }*/


            if (from_camera == null) {

                photo_upload = String.valueOf(student1);

                Log.e(TAG, "gallery" + photo_upload);
            } else if (!from_camera.equalsIgnoreCase("")) {
                photo_upload = from_camera;
                Log.e(TAG, "camera" + photo_upload);

            } else if (from_gallery == null) {
                photo_upload = from_camera;
            }

//            sourceFile = new File(photo_upload.replace("[", "").replace("]", ""));
            sourceFile = new File(photo_upload);
            RequestBody imageupload = RequestBody.create(MediaType.parse("image/jpeg"), sourceFile);
            Log.e("sourceFile", "" + sourceFile.getName());

            MultipartBody.Part image = MultipartBody.Part.createFormData("image", sourceFile.getName(), imageupload);
            String createdby = Pref_storage.getDetail(this, "userId");
            RequestBody created_by = RequestBody.create(MediaType.parse("text/plain"), createdby);
            String timeline_descriptionnew = edit_writepost.getText().toString();
            RequestBody timeline_description = RequestBody.create(MediaType.parse("text/plain"), timeline_descriptionnew);
            Call<CreateUserPojoOutput> call = apiService.create_timeline_user("create_timeline", 0, timeline_description, newlatituderesult, newlongituderesult, created_by);
            call.enqueue(new Callback<CreateUserPojoOutput>() {
                @Override
                public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {
                    if (response.body().getResponseCode() == 1) {

                        String timelineid = response.body().getData();
                        Pref_storage.setDetail(newsfeedPost.this, "gettimelineid", timelineid);
                        progress_dialog.setIndeterminate(false);
                        progress_dialog.setVisibility(View.GONE);

                        create_timeline_image();

                    }
                }

                @Override
                public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //user image post


 /*   private void create_timeline_image() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {


            if (from_camera == null) {

                photo_upload = String.valueOf(student1);

                Log.e(TAG,"gallery"+photo_upload);
            } else if (!from_camera.equalsIgnoreCase("")) {
                photo_upload = from_camera;
                Log.e(TAG,"camera"+photo_upload);

            } else if (from_gallery == null) {
                photo_upload = from_camera;
            }

//            sourceFile = new File(photo_upload.replace("[", "").replace("]", ""));
            sourceFile = new File(photo_upload);

            RequestBody imageupload = RequestBody.create(MediaType.parse("image/jpeg"), sourceFile);


            Log.e("sourceFile", "" + sourceFile.getName());

          *//*  MultipartBody.Part[] Images = new MultipartBody.Part[multiimage.size()];


            for (int index = 0; index < multiimage.size(); index++) {

                File file = new File(String.valueOf(multiimage));
                RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);

            }*//*

            MultipartBody.Part image = MultipartBody.Part.createFormData("image", sourceFile.getName(), imageupload);
            String createdby = Pref_storage.getDetail(this, "userId");
            RequestBody created_by = RequestBody.create(MediaType.parse("text/plain"), createdby);
            String timeline_descriptionnew = edit_writepost.getText().toString();
            RequestBody timeline_description = RequestBody.create(MediaType.parse("text/plain"), timeline_descriptionnew);
            Call<CreateUserPojoOutput> call = apiService.create_timeline_image("create_timeline_image", 0,  created_by, image);
            call.enqueue(new Callback<CreateUserPojoOutput>() {
                @Override
                public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {
                    if (response.body().getResponseCode() == 1) {
                        progress_dialog.setIndeterminate(false);
                        progress_dialog.setVisibility(View.GONE);

                        //Success
                        startActivity(new Intent(newsfeedPost.this, Home.class));
                        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    */


    private void create_timeline_image() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {


            if (from_camera == null) {
                photo_upload = from_gallery;
                test = photo_upload.split(",");

//                newimage = new ArrayList<String>(Arrays.asList(test));
                newimage = new ArrayList<String>(Arrays.asList(photo_upload));





                Log.e(TAG, "gallery" + photo_upload);
            } else if (!from_camera.equalsIgnoreCase("")) {
                photo_upload = from_camera;

                Log.e(TAG, "camera" + photo_upload);

            } else if (from_gallery == null) {
                photo_upload = from_camera;
            }

           /* for (String image : newimage){
                Log.e("Viz",""+image.replace("[","").replace("]",""));
            }*/


       /*     ArrayList<String> filePaths_new = new ArrayList<>();

            filePaths_new.add("/storage/emulated/0/Pictures/1524725661539.jpg");
            filePaths_new.add("/storage/emulated/0/Pictures/1526619563333.jpg");*/
            /* Log.e(TAG,"filePaths"+filePaths.size());
             */


            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            //Timeline

            String gettimelineid = Pref_storage.getDetail(newsfeedPost.this, "gettimelineid");

            builder.addFormDataPart("timeline_id", gettimelineid);

            //UserID
            String createdby = Pref_storage.getDetail(this, "userId");
            builder.addFormDataPart("created_by", createdby);

            //Image to upload
            for (int i = 0; i < imagesEncodedList.size(); i++) {
                Log.e(TAG, "filePaths_new" + imagesEncodedList.toString());


                file = new File(imagesEncodedList.get(i));
//                file = new File(String.valueOf(filePaths_new).replace("[", "").replace("]", ""));


                Log.e(TAG, "Multi_image_file" + file.getName());
                Log.e(TAG, "Multi_image_name" + file.toString());


                builder.addFormDataPart("image", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));

                Log.e(TAG, "filevishnu" + file.getName());

                MultipartBody requestBody = builder.build();
//            Call<com.kaspontech.foodwall.Model_classes.TimeLinePojo.CreateUserPojoOutput> call = apiService.uploadMultiFile("create_timeline_image", requestBody);
                Call<CreateUserPojoOutput> call = apiService.uploadMultiFile("create_timeline_image", requestBody);

                call.enqueue(new Callback<CreateUserPojoOutput>() {
                    @Override
                    public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                        Log.d(TAG, "success " + response.code());


                        if (response.body().getResponseCode() == 1) {
                            //Success
                            startActivity(new Intent(newsfeedPost.this, Home.class));
//                            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                            finish();
                        }

                    }

                    @Override
                    public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {

                        Log.d(TAG, "Error " + t.getMessage());
                    }
                });


            }





         /*   sourceFile = new File(photo_upload);

            RequestBody imageupload = RequestBody.create(MediaType.parse("image/jpeg"), sourceFile);


            MultipartBody.Part image = MultipartBody.Part.createFormData("image", sourceFile.getName(), imageupload);

            RequestBody created_by = RequestBody.create(MediaType.parse("text/plain"), createdby);
            String timeline_descriptionnew = edit_writepost.getText().toString();
            RequestBody timeline_description = RequestBody.create(MediaType.parse("text/plain"), timeline_descriptionnew);
            Call<CreateUserPojoOutput> call = apiService.create_timeline_image("create_timeline_image", 0,  created_by, image);
            call.enqueue(new Callback<CreateUserPojoOutput>() {
                @Override
                public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {
                    if (response.body().getResponseCode() == 1) {
                        progress_dialog.setIndeterminate(false);
                        progress_dialog.setVisibility(View.GONE);

                        //Success
                        startActivity(new Intent(newsfeedPost.this, Home.class));
                        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void AddressFinder(double latitude, double longitude) {


        Geocoder geocoder;
        List<Address> addresses = new ArrayList<>();
        geocoder = new Geocoder(this, Locale.getDefault());

        try {


            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            newlatituderesult = addresses.get(0).getLatitude();


            newlongituderesult = addresses.get(0).getLongitude();


        } catch (IOException e) {

            e.printStackTrace();

        }
    }

}




