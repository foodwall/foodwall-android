/*
package com.kaspontech.foodwall.FoodFeedsPackage.SharePostPackage;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kaspontech.foodwall.FoodFeedsPackage.Newsfeed_post_Package.Fullimage_activity;
import com.kaspontech.foodwall.FoodFeedsPackage.Permissions;
import com.kaspontech.foodwall.R;

import java.io.ByteArrayOutputStream;

public class New_photo_fragment extends Fragment {
    private static final String TAG = "PhotoFragment";

    //constant
    private static final int PHOTO_FRAGMENT_NUM = 0;
    private static final int GALLERY_FRAGMENT_NUM = 2;
    private static final int  CAMERA_REQUEST_CODE = 5;
    Context context;


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);
        Log.d(TAG, "onCreateView: started.");


        if(((Post_sharing_gallery_fragment)getActivity()).getCurrentTabNumber() == PHOTO_FRAGMENT_NUM){
            if(((Post_sharing_gallery_fragment)getActivity()).checkPermissions(Permissions.CAMERA_PERMISSION[0])){
                Log.d(TAG, "onClick: starting camera");

                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
            }
            else{
                Intent intent = new Intent(getActivity(), Post_sharing_gallery_fragment.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }

*/
/*//*
/        Button btnLaunchCamera = (Button) view.findViewById(R.id.btnLaunchCamera);
        btnLaunchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.dialog(TAG, "onClick: launching camera.");

                if(((Post_sharing_gallery_fragment)getActivity()).getCurrentTabNumber() == PHOTO_FRAGMENT_NUM){
                    if(((Post_sharing_gallery_fragment)getActivity()).checkPermissions(Permissions.CAMERA_PERMISSION[0])){
                        Log.dialog(TAG, "onClick: starting camera");
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
                    }else{
                        Intent intent = new Intent(getActivity(), Post_sharing_gallery_fragment.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
            }
        });*//*


        return view;
    }

    private boolean isRootTask(){
        if(((Post_sharing_gallery_fragment)getActivity()).getTask() == 0){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CAMERA_REQUEST_CODE){
          */
/*  Log.dialog(TAG, "onActivityResult: done taking a photo.");
            Log.dialog(TAG, "onActivityResult: attempting to navigate to final share screen.");*//*


            Bitmap bitmap;
            bitmap = (Bitmap) data.getExtras().get("data");
          */
/*  Uri tempUri = getImageUri(getContext(), bitmap);
            Pref_storage.setDetail(getActivity(), "imagefromfragment", getRealPathFromURI(tempUri));*//*


            if(isRootTask()){
                try{
                    Log.d(TAG, "onActivityResult: received new bitmap from camera: " + bitmap);
                    Intent intent = new Intent(getActivity(), Fullimage_activity.class);
                    intent.putExtra(getString(R.string.selected_bitmap), bitmap);
                    */
/*intent.putExtra(getString(R.string.selected_bitmap),  getRealPathFromURI(tempUri));*//*

                    startActivity(intent);
                 */
/*   ((AppCompatActivity) context).finish();*//*


                }catch (NullPointerException e){
                    Log.d(TAG, "onActivityResult: NullPointerException: " + e.getMessage());
                }
            }else{
                try{
               */
/*     Log.d(TAG, "onActivityResult: received new bitmap from camera: " + bitmap);
                    Intent intent = new Intent(getActivity(), AccountSettingsActivity.class);
                    intent.putExtra(getString(R.string.selected_bitmap), bitmap);
                    intent.putExtra(getString(R.string.return_to_fragment), getString(R.string.edit_profile_fragment));
                    startActivity(intent);
                    getActivity().finish();*//*

                }catch (NullPointerException e){
                    Log.d(TAG, "onActivityResult: NullPointerException: " + e.getMessage());
                }
            }

        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
//            Toast.makeText(mContext, "vis"+result, Toast.LENGTH_SHORT).show();
            cursor.close();
        }
        return result;

    }
}

*/
