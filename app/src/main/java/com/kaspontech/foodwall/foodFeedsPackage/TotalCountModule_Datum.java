package com.kaspontech.foodwall.foodFeedsPackage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TotalCountModule_Datum {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("total_posts")
    @Expose
    private int totalPosts;
    @SerializedName("total_events")
    @Expose
    private int totalEvents;
    @SerializedName("total_review")
    @Expose
    private int totalReview;
    @SerializedName("total_question")
    @Expose
    private int totalQuestion;

    /**
     * No args constructor for use in serialization
     *
     */
    public TotalCountModule_Datum() {
    }

    /**
     *
     * @param totalQuestion
     * @param totalReview
     * @param totalPosts
     * @param userId
     * @param totalEvents
     */
    public TotalCountModule_Datum(String userId, int totalPosts, int totalEvents, int totalReview, int totalQuestion) {
        this.userId = userId;
        this.totalPosts = totalPosts;
        this.totalEvents = totalEvents;
        this.totalReview = totalReview;
        this.totalQuestion = totalQuestion;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getTotalPosts() {
        return totalPosts;
    }

    public void setTotalPosts(int totalPosts) {
        this.totalPosts = totalPosts;
    }

    public int getTotalEvents() {
        return totalEvents;
    }

    public void setTotalEvents(int totalEvents) {
        this.totalEvents = totalEvents;
    }

    public int getTotalReview() {
        return totalReview;
    }

    public void setTotalReview(int totalReview) {
        this.totalReview = totalReview;
    }

    public int getTotalQuestion() {
        return totalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        this.totalQuestion = totalQuestion;
    }
}