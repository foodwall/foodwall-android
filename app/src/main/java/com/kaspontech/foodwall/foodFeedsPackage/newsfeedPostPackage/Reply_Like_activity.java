package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter.Comments_like_adapter;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Comment_like_output_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Likes_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Reply_Like_activity extends AppCompatActivity implements View.OnClickListener {

    /**
     * Application Tag
     **/

    private static final String TAG = "Reply_Like_activity";
    /**
     * Action Bar
     **/

    Toolbar actionBar;

    /**
     * back button
     **/
    ImageButton back;

    /**
     * likes Relative layout
     **/
    RelativeLayout rl_likes;
    RelativeLayout rl_over_likes;
    /**
     * Recyclerview for likes
     **/

    RecyclerView rv_likes;
    /**
     * Toolbar title
     **/

    TextView toolbar_title;
    /**
     * Linear layout
     **/
    LinearLayoutManager llm;
    /**
     * Comments like adapter
     **/
    Comments_like_adapter comments_like_adapter;

    /**
     * Loader
     **/
    ProgressBar progress_dialog_reply;
    /**
     * String result for comment id
     **/
    String Comment_id;

    public static String txt_usernamelike;


    /**
     * Arraylist for likes
     **/
    ArrayList<Likes_pojo> likelist = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes_layout);

        /*Widget initialization*/

        actionBar = (Toolbar) findViewById(R.id.actionbar_like);

        /*Toolbar title*/

        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);
        toolbar_title.setVisibility(View.VISIBLE);

        /*Back button initialization*/
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        /*Likes layout initialization*/

        rl_likes = (RelativeLayout) findViewById(R.id.rl_likes);
        rl_over_likes = (RelativeLayout) findViewById(R.id.rl_over_likes);

        /*Likes recyclerview*/

        rv_likes = (RecyclerView) findViewById(R.id.rv_likes);

        /*Loader initialization*/

        progress_dialog_reply = (ProgressBar) findViewById(R.id.progress_dialog_reply);

        /*Setting adapter*/

        llm = new LinearLayoutManager(this);
        rv_likes.setLayoutManager(llm);

        /*Getting input values*/
        Intent intent = getIntent();
        Comment_id = intent.getStringExtra("Comment_id");



            /*Loader visibility*/
            progress_dialog_reply.setVisibility(View.VISIBLE);

        /*Calling timeline comments likes all API*/

            getTimelineCommentLikesAll();



    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            /*Back button on click listener*/
            case R.id.back:
                finish();
                break;
                default:
                    break;
        }


    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }



    /*Calling timeline comments likes all API*/

    private void getTimelineCommentLikesAll() {

        if(Utility.isConnected(this)){
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {


                Call<Comment_like_output_pojo> call = apiService.get_timeline_comment_likes_all("get_timeline_comment_likes_all", Integer.parseInt(Comment_id));
                call.enqueue(new Callback<Comment_like_output_pojo>() {
                    @Override
                    public void onResponse(Call<Comment_like_output_pojo> call, Response<Comment_like_output_pojo> response) {

                        if (response.body().getResponseCode() == 1) {
                            progress_dialog_reply.setVisibility(View.GONE);

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                String fname = response.body().getData().get(j).getFirstName();
                                String userId = response.body().getData().get(j).getUserId();
                                String commenttimelineid = response.body().getData().get(j).getCmmtTlId();
                                String lname = response.body().getData().get(j).getLastName();
                                String picture = response.body().getData().get(j).getPicture();
                                String timeline = response.body().getData().get(j).getTimelineId();
                                String tlcomments = response.body().getData().get(j).getTlComments();
                                String totalcmmntlikes = response.body().getData().get(j).getTotalCmmtLikes();
                                String cmmtlikesID = response.body().getData().get(j).getCmmtLikesId();
                                String totcmtlikes = response.body().getData().get(j).getTlCmmtLikes();
                                String createdon = response.body().getData().get(j).getCreatedOn();
                                String createdby = response.body().getData().get(j).getCreatedBy();


                                Likes_pojo likesPojo = new Likes_pojo(commenttimelineid, timeline, tlcomments,
                                        totalcmmntlikes, cmmtlikesID, totcmtlikes, createdby, createdon, userId, fname, lname,
                                        "", "", "", "", picture);

                                likelist.add(likesPojo);




                            }
                            /*Setting likes adapter*/
                            comments_like_adapter = new Comments_like_adapter(Reply_Like_activity.this, likelist);
                           llm = new LinearLayoutManager(Reply_Like_activity.this);
                            rv_likes.setLayoutManager(llm);
                            rv_likes.setHasFixedSize(true);
                            rv_likes.setItemAnimator(new DefaultItemAnimator());
                            rv_likes.setAdapter(comments_like_adapter);
                            rv_likes.setNestedScrollingEnabled(false);
                            comments_like_adapter.notifyDataSetChanged();
                        }

                    }

                    @Override
                    public void onFailure(Call<Comment_like_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else {
            Snackbar snackbar = Snackbar.make(rl_over_likes, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }



    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
