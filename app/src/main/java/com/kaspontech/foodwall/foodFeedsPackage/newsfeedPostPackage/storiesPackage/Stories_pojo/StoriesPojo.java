package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StoriesPojo implements Serializable {

    @SerializedName("stories_id")
    @Expose
    private String storiesId;
    @SerializedName("stories_description")
    @Expose
    private String storiesDescription;
    @SerializedName("stories_image")
    @Expose
    private String storiesImage;
    @Expose
    private String address;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("total_view")
    @Expose
    private String totalView;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("stories_inr_view")
    @Expose
    private String storiesInrView;

    public StoriesPojo(String storiesId, String storiesDescription, String storiesImage, String address, String area, String city, String createdBy, String createdOn, String totalView, String firstName, String lastName, String picture, String storiesInrView) {
        this.storiesId = storiesId;
        this.storiesDescription = storiesDescription;
        this.storiesImage = storiesImage;
        this.address = address;
        this.area = area;
        this.city = city;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.totalView = totalView;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.storiesInrView = storiesInrView;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStoriesInrView() {
        return storiesInrView;
    }

    public void setStoriesInrView(String storiesInrView) {
        this.storiesInrView = storiesInrView;
    }

    public String getStoriesId() {
        return storiesId;
    }

    public void setStoriesId(String storiesId) {
        this.storiesId = storiesId;
    }

    public String getStoriesDescription() {
        return storiesDescription;
    }

    public void setStoriesDescription(String storiesDescription) {
        this.storiesDescription = storiesDescription;
    }

    public String getStoriesImage() {
        return storiesImage;
    }

    public void setStoriesImage(String storiesImage) {
        this.storiesImage = storiesImage;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTotalView() {
        return totalView;
    }

    public void setTotalView(String totalView) {
        this.totalView = totalView;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
