package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.TimeLine.likesAdapter;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.GetallLikesPojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Likes_2_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vishnukm on 15/3/18.
 */

public class likesActivity extends AppCompatActivity implements View.OnClickListener {
    /**
     * Application Tag
     **/
    private static final String TAG = "likesActivity";

    /**
     * Action Bar
     **/
    Toolbar actionBar;
    /**
     * back button
     **/
    ImageButton back;

    /**
     * likes Relative layout
     **/

    RelativeLayout rlLikes;
    /**
     * Recyclerview for likes
     **/
    RecyclerView rvLikes;

    /**
     * linearlayout manager
     **/
    LinearLayoutManager llm;

    /**
     * likes adapter
     **/
    com.kaspontech.foodwall.adapters.TimeLine.likesAdapter likesAdapter;

    /**
     * Likes model class
     **/

    GetallLikesPojo getallLikesPojo;

    /**
     * Loaders
     **/
    ProgressBar progressDialogLikes;

    /**
     * Title text views
     **/
    TextView toolbarNext;

    TextView toolbarTitle;

    /**
     * String results
     **/
    String likesTimelineid,comment_profile, comment_username,cmt_timelineid,usernamee,comment_totallikes,comment_tllikes,
            comment_created_on, hotelname, comment_image, comment_posttpe,comment_timelinepicture;

    String txtUsernamelike;

    String txtLastusernamelike;

    String likeUserImage;

    /**
     * Arraylist for likes
     **/

    ArrayList<GetallLikesPojo> likelist = new ArrayList<>();

    //to diaplay timeline img
    CircleImageView userphoto_profile;
    TextView username,txt_created_on,like;
    ImageView img_timelineimg,img_like;

    /**
     * Linear layout
     * @param savedInstanceState
     */
    LinearLayout layout_one_foradapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes_layout);



        /*Widget initialization*/

        actionBar = findViewById(R.id.actionbar_like);
        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);


        toolbarNext = actionBar.findViewById(R.id.toolbar_next);
        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarNext.setVisibility(View.GONE);

        rlLikes = findViewById(R.id.rl_likes);
        rvLikes = findViewById(R.id.rv_likes);
        progressDialogLikes = findViewById(R.id.progress_dialog_reply);

        img_timelineimg=findViewById(R.id.img_timelineimg);
        username=findViewById(R.id.username);
        userphoto_profile=findViewById(R.id.userphoto_profile);
        txt_created_on=findViewById(R.id.txt_created_on);
        img_like=findViewById(R.id.img_like);
        like=findViewById(R.id.like);
        layout_one_foradapter=findViewById(R.id.layout_one_foradapter);
        layout_one_foradapter.setVisibility(View.GONE);

        /*
         * Adapter initialization
         */

        llm = new LinearLayoutManager(likesActivity.this);
        rvLikes.setLayoutManager(llm);
        rvLikes.setItemAnimator(new DefaultItemAnimator());
        rvLikes.setNestedScrollingEnabled(false);


        //Getting timelineID from various adapter
        Intent intent = getIntent();
        likesTimelineid = intent.getStringExtra("Likes_timelineid");
        comment_posttpe = intent.getStringExtra("comment_posttpe");
        cmt_timelineid = intent.getStringExtra("comment_timelineid");
        comment_profile = intent.getStringExtra("comment_profile");
        comment_created_on = intent.getStringExtra("comment_created_on");
        usernamee = intent.getStringExtra("username");
        comment_timelinepicture = intent.getStringExtra("comment_timelinepicture");
        comment_image = intent.getStringExtra("comment_image");
        comment_totallikes = intent.getStringExtra("comment_totallikes");
        comment_tllikes = intent.getStringExtra("comment_tllikes");

        /* Loader on*/
        progressDialogLikes.setIndeterminate(true);

        Log.i(TAG, "onCreate:comment_posttpe " + comment_posttpe);
        Log.i(TAG, "onCreate:comment_profile " + comment_profile);
        Log.i(TAG, "onCreate: username" + usernamee);
        Log.i(TAG, "onCreate: comment_image" + comment_image);
         Log.i("comment_timelinepicture", "onCreate: comment_timelinepicture" + comment_created_on);

        //User picture loading

        Utility.picassoImageLoader(comment_profile,
                1, userphoto_profile, getApplicationContext());

        //set timeline img
        Utility.picassoImageLoader(comment_image,
                1, img_timelineimg, getApplicationContext());


        // Timestamp for timeline
        try {
            Utility.setTimeStamp(comment_created_on, txt_created_on);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // User Like API

        try {

            if (comment_totallikes.equalsIgnoreCase("0")) {

                like.setVisibility(View.GONE);

            } else if (comment_totallikes.equalsIgnoreCase("1")) {

                like.setVisibility(View.VISIBLE);
                like.setText(String.valueOf(comment_totallikes) + " Like");

            } else {

                like.setVisibility(View.VISIBLE);
                like.setText(String.valueOf(comment_totallikes) + " Likes");
            }

            if (comment_tllikes.equals("0")) {

                img_like.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_redlike_24dp));


            } else {

                img_like.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_favorite_red_24dp));

            }


        }catch (Exception e)
        {
            e.printStackTrace();
        }



        //Calling Likes All API
        getTimelineLikesAll();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            finish();
        }


    }


    // Calling Likes All API

    private void getTimelineLikesAll() {

        if (Utility.isConnected(this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createdby = Pref_storage.getDetail(likesActivity.this, "userId");

                Call<Likes_2_pojo> call = apiService.get_timeline_likes_all("get_timeline_likes_all", Integer.parseInt(likesTimelineid), Integer.parseInt(createdby));

                call.enqueue(new Callback<Likes_2_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Likes_2_pojo> call, @NonNull Response<Likes_2_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            /*Loader off*/

                            progressDialogLikes.setVisibility(View.GONE);

                            progressDialogLikes.setIndeterminate(false);

                            likelist = response.body().getData();

                            /*
                             * Adapter initialization
                             */

                            likesAdapter = new likesAdapter(likesActivity.this, likelist);
                            rvLikes.setAdapter(likesAdapter);
                            likesAdapter.notifyDataSetChanged();

//                            for (int j = 0; j < response.body().getData().size(); j++) {
//
//                                txtUsernamelike = response.body().getData().get(j).getFirstName();
//                                String userId = response.body().getData().get(j).getUserId();
//                                String timelineId = response.body().getData().get(j).getTimelineId();
//                                txtLastusernamelike = response.body().getData().get(j).getLastName();
//                                likeUserImage = response.body().getData().get(j).getPicture();
//
//
//                                getallLikesPojo = new GetallLikesPojo(timelineId, "", "",
//                                        "", "", "", userId,
//                                        txtUsernamelike, txtLastusernamelike, "", "",
//                                        "", "", likeUserImage);
//
//                                likelist.add(getallLikesPojo);
//
//                                likesAdapter.notifyDataSetChanged();
//
//
//                            }
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Likes_2_pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e(TAG, "FailureError" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Toast.makeText(getApplicationContext(), "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
