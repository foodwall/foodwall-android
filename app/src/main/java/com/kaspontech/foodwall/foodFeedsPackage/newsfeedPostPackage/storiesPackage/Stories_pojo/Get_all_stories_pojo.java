package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Get_all_stories_pojo implements Serializable {

    @SerializedName("str_id")
    @Expose
    private String strId;
    @SerializedName("stories_image")
    @Expose
    private String storiesImage;
    @SerializedName("stories_description")
    @Expose
    private String storiesDescription;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("stories_view")
    @Expose
    private String storiesView;
    @SerializedName("stories")
    @Expose
    private List<StoriesPojo> stories = null;
    @SerializedName("viewed_count")
    @Expose
    private Integer viewedCount;
    @SerializedName("stories_count")
    @Expose
    private Integer storiesCount;

    public String getStoriesDescription() {
        return storiesDescription;
    }

    public void setStoriesDescription(String storiesDescription) {
        this.storiesDescription = storiesDescription;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Get_all_stories_pojo(String strId, String storiesImage, String storiesDescription, String address, String area, String city, String createdBy, String createdOn, String userId, String firstName, String lastName, String picture, String storiesView, List<StoriesPojo> stories, Integer viewedCount, Integer storiesCount) {
        this.strId = strId;
        this.storiesImage = storiesImage;
        this.storiesDescription = storiesDescription;
        this.address = address;
        this.area = area;
        this.city = city;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.storiesView = storiesView;
        this.stories = stories;
        this.viewedCount = viewedCount;
        this.storiesCount = storiesCount;
    }



    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    public String getStoriesImage() {
        return storiesImage;
    }

    public void setStoriesImage(String storiesImage) {
        this.storiesImage = storiesImage;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getStoriesView() {
        return storiesView;
    }

    public void setStoriesView(String storiesView) {
        this.storiesView = storiesView;
    }

    public List<StoriesPojo> getStories() {
        return stories;
    }

    public void setStories(List<StoriesPojo> stories) {
        this.stories = stories;
    }

    public Integer getViewedCount() {
        return viewedCount;
    }

    public void setViewedCount(Integer viewedCount) {
        this.viewedCount = viewedCount;
    }

    public Integer getStoriesCount() {
        return storiesCount;
    }

    public void setStoriesCount(Integer storiesCount) {
        this.storiesCount = storiesCount;
    }
}
