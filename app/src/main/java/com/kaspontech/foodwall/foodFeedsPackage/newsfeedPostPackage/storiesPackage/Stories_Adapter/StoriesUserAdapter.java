package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.storiesActivity;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Get_all_stories_pojo;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoriesUserAdapter extends RecyclerView.Adapter<StoriesUserAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;

    private List<Get_all_stories_pojo> getallstorieslist = new ArrayList<>();
    private Get_all_stories_pojo getAllStoriesPojo;

    private Pref_storage pref_storage;


    public StoriesUserAdapter(Context c, List<Get_all_stories_pojo> gettinglist) {
        this.context = c;
        this.getallstorieslist = gettinglist;
    }


    @NonNull
    @Override
    public StoriesUserAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_stories_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        StoriesUserAdapter.MyViewHolder vh = new StoriesUserAdapter.MyViewHolder(v);

        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final StoriesUserAdapter.MyViewHolder holder, final int position) {


        getAllStoriesPojo = getallstorieslist.get(position);

        String fullname = getallstorieslist.get(position).getFirstName().concat(" ").concat(getallstorieslist.get(position).getLastName());

        String createuserid = Pref_storage.getDetail(context, "userId");

        if(getAllStoriesPojo.getUserId().equalsIgnoreCase(createuserid))
        {
            holder.txt_username_story.setText("Your story");
            holder.txt_username_story.setTextColor(Color.parseColor("#000000"));

        }else
        {
            holder.txt_username_story.setText(fullname);
            holder.txt_username_story.setTextColor(Color.parseColor("#000000"));

        }
       // holder.txt_username_story.setText(fullname);
       // holder.txt_username_story.setTextColor(Color.parseColor("#000000"));

        String image = getallstorieslist.get(position).getStories().get(0).getPicture();

        GlideApp.with(context)
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .fitCenter()
                .placeholder(R.drawable.ic_add_photo_profile)
                .into(holder.image_stories);


        if(getallstorieslist.get(position).getStoriesView().equals("0")){

            holder.image_circle.setImageResource(R.drawable.circle_background);

        }else{

            holder.image_circle.setImageResource(R.drawable.grey_circle_background);

        }

        holder.image_stories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);


                //Calling stories view API
                if (Utility.isConnected(context)) {


                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    String createuserid = Pref_storage.getDetail(context, "userId");
                    String storyid = getallstorieslist.get(position).getStrId();
                    Call<CommonOutputPojo> call = apiService.create_stories_view("create_stories_view", Integer.parseInt(storyid),Integer.parseInt(createuserid));

                    call.enqueue(new Callback<CommonOutputPojo>() {
                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                            if (response.body() != null) {

                                Log.e("story-->", "message" + response.body().getResponseMessage());

                                if (response.body().getResponseCode() == 1) {


                                    Intent intent = new Intent(context, storiesActivity.class);
                                    intent.putExtra("position", position);
                                    intent.putExtra("userType","0");

                                    Bundle args = new Bundle();
                                    args.putSerializable("storyList",(Serializable)getallstorieslist);
                                    intent.putExtra("story",args);
                                    context.startActivity(intent);


                                }

                            }


                        }

                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });


                } else {
                    Toast.makeText(context, "No internet connection. Try again with connecting to internet.", Toast.LENGTH_SHORT).show();

                }



            }
        });


    }

    @Override
    public int getItemCount() {
        return getallstorieslist.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_username_story;
        RelativeLayout rl_stories;
        ImageView image_circle, image_stories;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_username_story = (TextView) itemView.findViewById(R.id.txt_username_story);
            image_circle = (ImageView) itemView.findViewById(R.id.image_circle);
            image_stories = (ImageView) itemView.findViewById(R.id.image_stories);
            rl_stories = (RelativeLayout) itemView.findViewById(R.id.rl_stories);


        }


    }


}
