package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.REST_API.ProgressRequestBody;
import com.kaspontech.foodwall.foodFeedsPackage.CreateHostoricalMapRestname;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Create_story_output_pojo;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.utills.universalLoaderClass;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.File;

import dmax.dialog.SpotsDialog;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Story_full_image_activity extends AppCompatActivity implements View.OnClickListener,
        ProgressRequestBody.UploadCallbacks {

    /*
     * Application TAG
     * */
    private static final String TAG = "Story_full_image";

    /*
     * Error request
     * */
    private static final int ERROR_DIALOG_REQUEST = 9001;

    /*
     * Imageview
     * */

    ImageView img_story;
    /*
     * Toolbar
     * */
    android.support.v7.widget.Toolbar toolbar;
    /*
     * ImageButton
     * */
    ImageButton close;
    ImageButton userphoto_chat;
    ImageButton add_story_location;
    ImageButton img_send;
    /*
     * Edit text
     * */
    EditText edt_story_descr;

    /*
     *RelativeLayout
     * */
    RelativeLayout rl_story_post;

    /*
     *String
     * */
    String story_desr_result;
    String file_camera;
    String file_gallery;
    String story_caption;
    String location = "";
    String area= "";
    String city="";
    String photo_upload;
    ;

    /*
     *Progressbar
     * */
    ProgressBar story_upload_progress;


    /*
     *File
     * */
    File sourceFile;
    /*
     *View story
     * */
    View rlStory;
    /*
     *Location story
     * */
    TextView tvStoryLocation;
    /*
     *Emojicon EditText
     * */
    EmojiconEditText emojiconEditText;

    EmojIconActions emojIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_full_image);

        /*Widget initialization*/

        img_story = (ImageView) findViewById(R.id.img_story);

        /*Image button initialization*/
        close = (ImageButton) findViewById(R.id.close);

        userphoto_chat = (ImageButton) findViewById(R.id.userphoto_chat);

        add_story_location = (ImageButton) findViewById(R.id.add_story_location);

        img_send = (ImageButton) findViewById(R.id.img_send);

        /*Button click listeners*/
        close.setOnClickListener(this);
        img_send.setOnClickListener(this);
        add_story_location.setOnClickListener(this);

        /*Location textview*/
        tvStoryLocation = (TextView) findViewById(R.id.tv_story_location);
        /*Desription edit text*/
        edt_story_descr = (EditText) findViewById(R.id.edt_story_descr);

        /*Emoji edit text initialization*/
        emojiconEditText = (EmojiconEditText) findViewById(R.id.emojicon_edit_text);

        rl_story_post = (RelativeLayout) findViewById(R.id.rl_story_post);
        rlStory = (View) findViewById(R.id.rl_story);
        rl_story_post.setOnClickListener(this);

        /*Loader*/
        story_upload_progress = (ProgressBar) findViewById(R.id.story_upload_progress);

        try {
            /*Getting input values*/
            Intent intent = getIntent();

            file_camera = intent.getStringExtra("file_camera");
            file_gallery = intent.getStringExtra("file_gallery");
            location = intent.getStringExtra("location");
            area = intent.getStringExtra("area");
            city = intent.getStringExtra("city");
            story_caption = intent.getStringExtra("story_caption");

            if (story_caption != null) {

                int length = story_caption.length();
                emojiconEditText.setText(story_caption);
                emojiconEditText.setSelection(length);


            }

            if (location != null) {

                tvStoryLocation.setText(location);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("CheckingLocation", "CheckingLocation->" + story_caption);
        Log.e("CheckingLocation", "CheckingLocation->" + location);

        if (file_camera == null) {
            img_story.setImageURI(Uri.parse(file_gallery));
        } else if (file_gallery == null) {
            img_story.setImageURI(Uri.parse(file_camera));
        }

        /*Emojicon edit text settings*/
        emojIcon = new EmojIconActions(Story_full_image_activity.this, rlStory, emojiconEditText, userphoto_chat);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_keyboard, R.drawable.ic_great);

        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e("EmojIconActions", "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
                Log.e("EmojIconActions", "Keyboard closed");
            }
        });


    }


    @Override
    public void onClick(View v) {

        int click = v.getId();

        switch (click) {
            case R.id.close:
                finish();
                break;

            case R.id.add_story_location:

                if (isServicesOK()) {

                    // Intent intent = new Intent(Story_full_image_activity.this, MapActivity.class);
                    Intent intent = new Intent(Story_full_image_activity.this, CreateHostoricalMapRestname.class);
                    intent.putExtra("file_camera", file_camera);
                    intent.putExtra("file_gallery", file_gallery);
                    intent.putExtra("story_caption", emojiconEditText.getText().toString());
                    startActivity(intent);
                    finish();


                }

                break;

            case R.id.story_edit_text:

                emojiconEditText.setFocusable(true);

                break;
            /*Send button click listener*/
            case R.id.img_send:

                story_upload_progress.setVisibility(View.GONE);
                story_desr_result = emojiconEditText.getText().toString();

                if (file_camera == null) {
                    photo_upload = file_gallery;
                } else if (file_gallery == null) {
                    photo_upload = file_camera;
                }


                Log.e(TAG, "onClick: " + photo_upload);

                /*Hiding keyboard*/

                Utility.hideKeyboard(v);

                //API to create upload story

                createStories();


                break;

            default:
                break;
        }

    }


    /*Services for checking google play services */
    public boolean isServicesOK() {

        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(Story_full_image_activity.this);

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(Story_full_image_activity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    //API to create upload story


    private void createStory() {


        if (Utility.isConnected(getApplicationContext())) {

            if (location == null) {
                location = "";
            }

            final AlertDialog dialog = new SpotsDialog.Builder().setContext(Story_full_image_activity.this).setMessage("").build();
            dialog.show();

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            int userid = Integer.parseInt(Pref_storage.getDetail(Story_full_image_activity.this, "userId"));

            String story_caption = org.apache.commons.text.StringEscapeUtils.escapeJava(emojiconEditText.getText().toString());

            MultipartBody.Builder builder = new MultipartBody.Builder();

            builder.setType(MultipartBody.FORM);

            //Timeline
            builder.addFormDataPart("stories_id", String.valueOf(0));

            builder.addFormDataPart("stories_description", story_caption);

            builder.addFormDataPart("image_exists", "");

            builder.addFormDataPart("address", location);

            builder.addFormDataPart("area", "");

            builder.addFormDataPart("city", "");

            //UserID
            builder.addFormDataPart("created_by", String.valueOf(userid));

            //Image to upload
            sourceFile = new File(photo_upload);

            builder.addFormDataPart("image", sourceFile.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), sourceFile));

            MultipartBody requestBody = builder.build();

            try {

                Call<CommonOutputPojo> call = apiService.createStories("create_stories",
                        requestBody);

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success
                            story_upload_progress.setIndeterminate(false);
                            story_upload_progress.setVisibility(View.GONE);
                            startActivity(new Intent(getApplicationContext(), Home.class));
                            finish();
                            dialog.dismiss();

                        }
                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        dialog.dismiss();

                    }
                });

            } catch (Exception e) {

                e.printStackTrace();
                dialog.dismiss();


            }


        }
    }

    //API to create upload story

    private void createStories() {


        if (Utility.isConnected(getApplicationContext())) {

            if (location == null) {
                location = "Address";
            }

            if(area==null)
            {
                area="area";
            }

            if(city==null)
            {
                city="city";
            }

            String storyCaption = org.apache.commons.text.StringEscapeUtils.escapeJava(emojiconEditText.getText().toString());


            /*Loader*/
            final universalLoaderClass ccd = new universalLoaderClass(this);
            ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            ccd.setCancelable(true);
            ccd.show();


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {


                String userid = Pref_storage.getDetail(this, "userId");

                RequestBody storyDescription = RequestBody.create(MediaType.parse("text/plain"), storyCaption);
                RequestBody locationStory = RequestBody.create(MediaType.parse("text/plain"), location);
                RequestBody areaa = RequestBody.create(MediaType.parse("text/plain"), city);
                RequestBody cityy = RequestBody.create(MediaType.parse("text/plain"), city);
                RequestBody imageExists = RequestBody.create(MediaType.parse("text/plain"), "");
                RequestBody createdBy = RequestBody.create(MediaType.parse("text/plain"), userid);



                sourceFile = new File(photo_upload);

                File compressedImageFile = new Compressor(this).setQuality(100).compressToFile(sourceFile);

                ProgressRequestBody fileBody = new ProgressRequestBody(compressedImageFile, this);

                MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), fileBody);

                Log.e("sourceFile_story", "" + compressedImageFile.getName());

                Call<Create_story_output_pojo> call = apiService.create_stories("create_stories",
                        0,
                        storyDescription,
                        imageExists,
                        locationStory,
                        areaa,
                        cityy,
                        1,
                        createdBy,
                        filePart);
                call.enqueue(new Callback<Create_story_output_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Create_story_output_pojo> call, @NonNull Response<Create_story_output_pojo> response) {

                        ccd.dismiss();
                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);
                        if (responseStatus.equals("success")) {

                            //Success
                            story_upload_progress.setIndeterminate(false);
                            story_upload_progress.setVisibility(View.GONE);


                            startActivity(new Intent(getApplicationContext(), Home.class));
                            finish();


                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Create_story_output_pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        story_upload_progress.setIndeterminate(false);
                        story_upload_progress.setVisibility(View.GONE);
                        ccd.dismiss();
                        startActivity(new Intent(getApplicationContext(), Home.class));
                        finish();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                ccd.dismiss();

            }

        } else {

            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();

        }


    }

    /*private void edit_stories() {

        if (isConnected(getApplicationContext())) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {


                String userid = Pref_storage.getDetail(this, "userId");
                sourceFile = new File(photo_upload);

                RequestBody imageupload = RequestBody.create(MediaType.parse("image/jpeg"), sourceFile);

                Log.e("sourceFile_story", "" + sourceFile.getName());


                RequestBody story_description = RequestBody.create(MediaType.parse("text/plain"), storyDesrResult);
                RequestBody locationStory = RequestBody.create(MediaType.parse("text/plain"), location);
                RequestBody area = RequestBody.create(MediaType.parse("text/plain"), "");
                RequestBody city = RequestBody.create(MediaType.parse("text/plain"), "");
                RequestBody image_exists = RequestBody.create(MediaType.parse("text/plain"), "");
                RequestBody created_by = RequestBody.create(MediaType.parse("text/plain"), userid);

                MultipartBody.Part image = MultipartBody.Part.createFormData("image", sourceFile.getName(), imageupload);

                Call<Create_story_output_pojo> call = apiService.createStories("createStories",
                        0, story_description, image, image_exists, locationStory, area, city, created_by);


               *//*Call<Create_story_output_pojo> call = apiService.createStories("createStories",
                        0, story_description, image, "",location,"","", created_by);
*//*


                call.enqueue(new Callback<Create_story_output_pojo>() {
                    @Override
                    public void onResponse(Call<Create_story_output_pojo> call, Response<Create_story_output_pojo> response) {
                        if (response.body().getResponseCode() == 1) {
                            storyUploadProgress.setIndeterminate(false);
                            storyUploadProgress.setVisibility(View.GONE);
                            startActivity(new Intent(getApplicationContext(), Home.class));
                            finish();

                        }
                    }

                    @Override
                    public void onFailure(Call<Create_story_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            *//*Snackbar snackbar = Snackbar.make(frameStory, "Uploading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();*//*
        }


    }*/


    @Override
    public void onProgressUpdate(int percentage) {
        Log.e(TAG, "onProgressUpdate: " + percentage);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }


}
