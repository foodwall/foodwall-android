package com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter.Comments_adapter;
import com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter.ShowTaggedPeopleAdapter;
import com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter.TimelineAdapter;
import com.kaspontech.foodwall.adapters.TimeLine.ViewPagerAdapter;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Get_all_stories_pojo;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.StoryFragment;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.CreateEditTimeline_Comments;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.GetallCommentsPojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Image;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Reply_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.comments_2;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.reviewpackage.Review_in_detail_activity;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

/**
 * Created by vishnukm on 15/3/18.
 */

public class Comments_activity extends _SwipeActivityClass
        implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    /**
     * Application Tag
     **/
    private static final String TAG = "Comments_activity";


    /**
     * Action Bar
     **/
    Toolbar actionBar;

    /**
     * Widgets
     **/

    EmojiconEditText edittxt_commentnew;

    EmojIconActions emojIcon;

    CircleImageView userphoto_commentnew;

    CircleImageView imgSelfComment;

    TextView txt_postnew;

    TextView txt_replying_to;

    TextView tv_no_comments;


    /*Self comments widgets*/

    TextView txtSelfUsername;

    TextView txtSelfAgo;

    TextView txtSelfComments;


    ImageButton back;

    ImageButton img_btn_close;

    LinearLayout rl_comment;
    RelativeLayout rl_postlayoutnew;
    RelativeLayout rl_replying;
    RelativeLayout rl_commentactivity;

    LinearLayout ll_comment_nodata;
    RecyclerView rv_comments;

    /**
     * Comments Adapter
     **/
    Comments_adapter commentsAdapter;

    /**
     * Comments pojo class
     **/
    GetallCommentsPojo getallCommentsPojo;

    /**
     * Comments Arraylists (Reply and comments list)
     **/
    List<Reply_pojo> replylist = new ArrayList<>();

    ArrayList<GetallCommentsPojo> commentlist = new ArrayList<>();

    /**
     * Loader
     **/
    ProgressBar progress_dialog,progress_timeline;

    /**
     * String results
     **/
    public String methodname,username_comment, comments, lastusername_comment, ago_comment, cmt_timelineid, comment_profile, comment_username,
            comment_created_on, username, hotelname, comment_image, comment_posttpe,comment_timelinepicture;

    public String comment_eventfirstname,comment_eventname;
    /**
     * From Adapter (Interface)
     **/

    int frmAdaptertl_cmmntid, frm_adap_userId, frm_adap_timelineId, clickedforreply, clickedforedit, clickedforeditreply, clickededitreplyid;

    /**
     * public ViewPager mviewpager;
     *
     * @param savedInstanceState
     */
    public int NUM_PAGES = 0;
    public int currentPage = 0;
    public ViewPager mviewpager;
    public CircleIndicator circleIndicatortimeline;

    //Timeline layout
    LinearLayout ll_timelinelayout;
    CircleImageView userphoto_profile;
    TextView txt_username, txt_created_on;
    ImageView timeline_img;

    //Event layout
    CircleImageView eventview_user_photo;
    TextView txt_view_event_text,txt_view_event_username,txt_view_event_name,txt_eventcreated_on,event_date,event_name,event_location;
    ImageView event_img;
    LinearLayout ll_eventlayout;

    List<Image> timeline_image_list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments_layout);


        initComponents();

        /*User image URL from shared preference*/
        String userdp = (Pref_storage.getDetail(Comments_activity.this, "picture_user_login"));



        /* Redirect from various adapter (Input values of timeline ID)*/

        /**
         * get event details
         */
        Intent intent = getIntent();
        comment_posttpe = intent.getStringExtra("comment_posttpe");
        cmt_timelineid = intent.getStringExtra("comment_timelineid");
        comment_profile = intent.getStringExtra("comment_profile");
        comment_created_on = intent.getStringExtra("comment_created_on");
        username = intent.getStringExtra("username");
        comment_timelinepicture = intent.getStringExtra("comment_timelinepicture");

        /**
         * get event details
         */
        comment_image = intent.getStringExtra("comment_image");
        comment_eventname = intent.getStringExtra("comment_eventname");



        Log.i(TAG, "onCreate:comment_posttpe " + cmt_timelineid);
        Log.i(TAG, "onCreate:comment_posttpe " + comment_posttpe);
        Log.i(TAG, "onCreate:comment_profile " + comment_profile);
        Log.i(TAG, "onCreate: username" + username);
        Log.i(TAG, "onCreate: comment_eventname" + comment_eventname);
        Log.i("comment_timelinepicture", "onCreate: comment_timelinepicture" + comment_created_on);


        //User picture loading

        Utility.picassoImageLoader(userdp,
                1, userphoto_commentnew, getApplicationContext());


        try {
            // check post type

            if (comment_posttpe.equalsIgnoreCase("0")) {

                ll_timelinelayout.setVisibility(View.VISIBLE);
                ll_eventlayout.setVisibility(View.GONE);

            } else if (comment_posttpe.equalsIgnoreCase("1")) {

                txt_view_event_text.setText("Created this event ");

                ll_timelinelayout.setVisibility(View.GONE);
                ll_eventlayout.setVisibility(View.VISIBLE);


            } else if (comment_posttpe.equalsIgnoreCase("2")) {
                txt_view_event_text.setText("Going to this event ");

                ll_timelinelayout.setVisibility(View.GONE);
                ll_eventlayout.setVisibility(View.VISIBLE);
            } else if (comment_posttpe.equalsIgnoreCase("3")) {
                txt_view_event_text.setText("Interested in ");
                ll_timelinelayout.setVisibility(View.GONE);
                ll_eventlayout.setVisibility(View.VISIBLE);

            } else {
                ll_timelinelayout.setVisibility(View.GONE);
                ll_eventlayout.setVisibility(View.GONE);
            }


        }catch (Exception e)
        {
            e.printStackTrace();
        }
        /**
         *  set timeline details
         */
        //timeline user image loading
        GlideApp.with(this)
                .load(comment_profile)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)
                .into(userphoto_profile);


        // Displaying user name
        txt_username.setText(String.valueOf(username));
        txt_username.setMovementMethod(LinkMovementMethod.getInstance());

        // Timestamp for timeline
        try {
            Utility.setTimeStamp(comment_created_on, txt_created_on);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //set timeline image
        GlideApp.with(this)
                .load(comment_image)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)
                .into(timeline_img);


        /**
         * set Event details
         */
        //timeline user image loading
        GlideApp.with(this)
                .load(comment_profile)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)
                .into(eventview_user_photo);


        txt_view_event_username.setText(username);
        txt_view_event_name.setText(comment_eventname);
         try {
            Utility.setTimeStamp(comment_created_on, txt_eventcreated_on);
        } catch (Exception e) {
            e.printStackTrace();
        }




        //timeline user image loading
        GlideApp.with(this)
                .load(comment_image)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)
                .into(event_img);












        /* Calling comments all API */

        progress_dialog.setVisibility(View.VISIBLE);

        //API to get comments all

        get_timeline_comments_all();

       /* if(comment_posttpe.equalsIgnoreCase("0"))
        {
            methodname="get_timeline_comments_all";
            get_timeline_comments_all();

        }else if(comment_posttpe.equalsIgnoreCase("1")) {
            methodname = "get_events_comments_all";

            get_timeline_comments_all();


        }*/
        //Edit text change listener for posting comments

        edittxt_commentnew.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                /*Post button visibility condition checking*/

                if (edittxt_commentnew.getText().toString().trim().length() != 0) {
                    txt_postnew.setVisibility(View.VISIBLE);
                } else {
                    txt_postnew.setVisibility(View.GONE);
                }


            }
        });


    }

    @Override
    public void onSwipeRight() {
        finish();
    }

    @Override
    protected void onSwipeLeft() {

    }


    private void initComponents() {
        /*Widget initialization*/
        actionBar = findViewById(R.id.actionbar_comment);

        back = actionBar.findViewById(R.id.button_back);
        back = actionBar.findViewById(R.id.button_back);

        img_btn_close = findViewById(R.id.img_btn_close);

        /*On Click listeners*/
        back.setOnClickListener(this);
        img_btn_close.setOnClickListener(this);

        /*Relative layout initialization*/
        rl_comment = findViewById(R.id.rl_commentnew);
        rl_commentactivity = findViewById(R.id.rl_commentactivity);
        rl_postlayoutnew = findViewById(R.id.rl_postlayoutnew);
        rl_replying = findViewById(R.id.rl_replying);

        /*Linear layout initialization*/

        ll_comment_nodata = findViewById(R.id.ll_comment_nodata);
        /*Loader*/
        progress_dialog = findViewById(R.id.progress_dialog_comment);
        progress_timeline = findViewById(R.id.progress_timeline);

        /*ImageView's*/
        userphoto_commentnew = findViewById(R.id.userphoto_commentnew);

        imgSelfComment = findViewById(R.id.img_comment);

        edittxt_commentnew = findViewById(R.id.edittxt_commentnew);

        edittxt_commentnew.setImeOptions(EditorInfo.IME_ACTION_GO);

        /*Smiley keyboard*/
        emojIcon = new EmojIconActions(Comments_activity.this, rl_commentactivity, edittxt_commentnew, userphoto_commentnew);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

        /*Textview's*/
        txt_replying_to = findViewById(R.id.txt_replying_to);

        txt_postnew = findViewById(R.id.txt_adapter_postnew);

        tv_no_comments = findViewById(R.id.tv_no_comments);

        txtSelfUsername = findViewById(R.id.txt_username);

        txtSelfAgo = findViewById(R.id.txt_ago);

        txtSelfComments = findViewById(R.id.txt_comments);


        /*Comments recyclerview*/
        rv_comments = findViewById(R.id.rv_commentsnew);

        /*On click listener*/
        txt_postnew.setOnClickListener(this);


        //timeline layout
        ll_timelinelayout = findViewById(R.id.ll_timelinelayout);
        //profile image
        userphoto_profile = findViewById(R.id.userphoto_profile);

        //to load username with timeline details
        txt_username = findViewById(R.id.username);
        // set created date
        txt_created_on = findViewById(R.id.txt_created_on);
        //timeline image
        timeline_img = findViewById(R.id.timeline_img);

        /**
         * event layout
         */
        ll_eventlayout=findViewById(R.id.ll_eventlayout);
        eventview_user_photo=findViewById(R.id.eventview_user_photo);
        txt_view_event_text=findViewById(R.id.txt_view_event_text);
        txt_view_event_username=findViewById(R.id.txt_view_event_username);
        txt_view_event_name=findViewById(R.id.txt_view_event_name);
        txt_eventcreated_on=findViewById(R.id.txt_eventcreated_on);
        event_date=findViewById(R.id.event_date);
        event_name=findViewById(R.id.event_name);
        event_location=findViewById(R.id.event_location);
        event_img=findViewById(R.id.event_img);



    }
    /* Setting the images in the view pager */


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
            /*Back button press*/
            case R.id.button_back:
                 finish();
                break;
            /*Close button for reply*/
            case R.id.img_btn_close:
                rl_replying.setVisibility(View.GONE);
                // Clearing all int values

                clickedforreply = 0;
                clickedforedit = 0;
                clickedforeditreply = 0;
                /*Hide Keyboard*/
                Utility.hideKeyboard(edittxt_commentnew);

                break;
            /*Post button click listener*/
            case R.id.txt_adapter_postnew:
                /*Animated post button*/
                final Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.modal_in);
                v.startAnimation(anim);

                if (org.apache.commons.text.StringEscapeUtils.escapeJava(edittxt_commentnew.getText().toString().trim()).equalsIgnoreCase("")) {
                    //Hiding keyboard
                    Utility.hideKeyboard(v);
                } else {
                    //Hiding keyboard
                    Utility.hideKeyboard(v);

                    Log.e(TAG, "clickedforreply-->" + clickedforreply);
                    Log.e(TAG, "clickedforEdit-->" + clickedforedit);
                    Log.e(TAG, "clickedforEditreply-->" + clickedforeditreply);


                    //Posting reply

                    if (clickedforreply == 11) {
                        //Calling comments reply API
                        createEditTimelineCommentsReply(frmAdaptertl_cmmntid);

                    } else if (clickedforedit == 22) {
                        //Calling Edit API

                        editTimelineComments(frmAdaptertl_cmmntid, frm_adap_timelineId);

                    } else if (clickedforeditreply == 44) {
                        //Calling reply API

                        editTimelineCommentsReply(frm_adap_timelineId, frmAdaptertl_cmmntid);

                    } else {
                        //Calling create comment API

                        createTimelineComments(cmt_timelineid);
                    }
                }


                break;
        }

    }


    // Interface listener

    /*
     *
     * Comments_adapter
     *
     * */



    /*Calling comments all API if size of list is zero*/

    public void check_timeline_reply_size(int size) {
//        Log.e("Vishnu", "check_reply_size" + size);
        if (size == 0) {

            /*Calling comments all API if size of list is zero*/
            get_timeline_comments_all();
        }
    }


    /*Checking size for displaying no data image*/
    public void checksize(int size) {
        if (size == 0) {

            ll_comment_nodata.setVisibility(View.VISIBLE);
        } else {
            ll_comment_nodata.setVisibility(View.GONE);
        }
        /*Setting comments list size to shared preference*/
        Pref_storage.setDetail(Comments_activity.this, "Comments_size", String.valueOf(size));
//        Log.e("Comments_size", "-->" + size);
    }


    /*User name click interface listener*/
    public void clicked_username(View view, String username, int position) {

        frmAdaptertl_cmmntid = position;
        rl_replying.setVisibility(View.VISIBLE);
        txt_replying_to.setText("Replying to ".concat(username));
        edittxt_commentnew.requestFocus();

//        Log.e("clicked_reply", "Comment_id" + frmAdaptertl_cmmntid);

    }

    /*Clicked comment positin for ID*/

    public void clickedcommentposition(View view, int position) {
        frmAdaptertl_cmmntid = position;
    }

    /*Reply click listener*/
    public void clickedforreply(View view, int position) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_commentnew, InputMethodManager.SHOW_IMPLICIT);
        clickedforreply = position;
        clickedforedit = 0;
        edittxt_commentnew.setText("");

//        Log.e("Vishnu", "clickedforreply" + clickedforreply);

    }


    /*Edit click listener*/
    public void clickedfor_edit(View view, int position) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_commentnew, InputMethodManager.SHOW_IMPLICIT);
        rl_replying.setVisibility(View.GONE);
        clickedforedit = position;
        clickedforreply = 0;


//        Log.e("Vishnu", "clickedforedit" + clickedforedit);

    }




    /*
     * Comments_reply_adapter
     *
     * */

    /* edit commment timeline id*/
    public void clickedcommentposition_timelineid(View view, String comments, int position, int timelineid) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_commentnew, InputMethodManager.SHOW_IMPLICIT);
        frmAdaptertl_cmmntid = position;
        frm_adap_timelineId = timelineid;
        edittxt_commentnew.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(comments));
        int pos = edittxt_commentnew.getText().length();
        edittxt_commentnew.setSelection(pos);
        edittxt_commentnew.requestFocus();
    }

    /* Reply edit  commment text */

    public void clickedcommentposition_reply_edit(View view, String comments, int replyid, int comment_tl_id) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_commentnew, InputMethodManager.SHOW_IMPLICIT);
        frmAdaptertl_cmmntid = comment_tl_id;
        frm_adap_timelineId = replyid;
        edittxt_commentnew.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(comments));
        int pos = edittxt_commentnew.getText().length();
        edittxt_commentnew.setSelection(pos);
        edittxt_commentnew.requestFocus();
    }

    /*Reply edit click listener*/

    public void clickedfor_replyedit(View view, int position) {

        rl_replying.setVisibility(View.GONE);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_commentnew, InputMethodManager.SHOW_IMPLICIT);
        clickedforeditreply = position;

        Log.e("Vishnu", "clickedforeditreply" + clickedforeditreply);

    }

    /*Reply id click listener*/


    public void clickedfor_replyid(View view, int position) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_commentnew, InputMethodManager.SHOW_IMPLICIT);
        clickededitreplyid = position;
        clickedforedit = 0;
        clickedforreply = 0;


        Log.e("Vishnu", "clickededitreplyid" + clickededitreplyid);

    }

    /*Comment clicked user id */

    public void clickedcomment_userid_position(View view, int position) {
        frm_adap_userId = position;
        Log.e("Vishnu", "clickeduserId" + frm_adap_userId);

    }

    /*Delete click id listener*/
    public void clickedfordelete() {

        edittxt_commentnew.setText("");
        rl_replying.setVisibility(View.GONE);

    }

    //API to get comments all

    private void get_timeline_comments_all( ) {

        if (Utility.isConnected(getApplicationContext())) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(Comments_activity.this, "userId");
                Call<comments_2> call = apiService.get_timeline_comments_all("get_timeline_comments_all",
                        Integer.parseInt(cmt_timelineid),
                        Integer.parseInt(createuserid));
                call.enqueue(new Callback<comments_2>() {
                    @Override
                    public void onResponse(Call<comments_2> call, Response<comments_2> response) {

                        if (response.body().getResponseCode() == 1) {

                            progress_dialog.setVisibility(View.GONE);

                            /*Arraylist clear for removing duplicates removing*/
                            commentlist.clear();

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                username_comment = response.body().getData().get(j).getFirstName();

                                lastusername_comment = response.body().getData().get(j).getLastName();
                                String timelineid = response.body().getData().get(j).getTimelineId();
                                comments = response.body().getData().get(j).getTlComments();

                                ago_comment = response.body().getData().get(j).getCreatedOn();
                                String picture = response.body().getData().get(j).getPicture();
                                String tl_cmmt_id = response.body().getData().get(j).getCmmtTlId();
                                String createdby = response.body().getData().get(j).getCreatedBy();
                                String total_comments = response.body().getData().get(j).getTotalComments();
                                String time_description = response.body().getData().get(j).getTimelineDescription();
                                String userid = response.body().getData().get(j).getUserId();
                                String CmmtLikesId = response.body().getData().get(j).getCmmtLikesId();
                                String totalCmmtLikes = response.body().getData().get(j).getTotalCmmtLikes();
                                String TlCmmtLikes = response.body().getData().get(j).getTlCmmtLikes();
                                String TotalCmmtReply = response.body().getData().get(j).getTotalCmmtReply();


                                getallCommentsPojo = new GetallCommentsPojo(timelineid, time_description, total_comments,
                                        tl_cmmt_id, comments,
                                        totalCmmtLikes, TotalCmmtReply,
                                        createdby, ago_comment, userid,
                                        username_comment, lastusername_comment, "",
                                        "", "", "",
                                        picture, CmmtLikesId, TlCmmtLikes, replylist);


                                commentlist.add(getallCommentsPojo);


                                /*Settings self user comments*/


//                                if(userid.equalsIgnoreCase(Pref_storage.getDetail(getApplicationContext(),"userId"))){
//
//                                    Utility.picassoImageLoader(picture,0,imgSelfComment,getApplicationContext());
//
//                                    Utility.setTimeStamp(ago_comment,txtSelfAgo);
//
//                                    txtSelfComments.setText(comments);
//
//                                    txtSelfUsername.setText(Pref_storage.getDetail(getApplicationContext(),"firstName").concat(" ").concat(Pref_storage.getDetail(getApplicationContext(),"lastName")));
//
//                                }


                            }




                            /*Setting up adapter*/

                            commentsAdapter = new Comments_adapter(Comments_activity.this, commentlist);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Comments_activity.this);
                            linearLayoutManager.findFirstVisibleItemPosition();
                            rv_comments.setLayoutManager(linearLayoutManager);
                            rv_comments.setHasFixedSize(true);
                            rv_comments.setAdapter(commentsAdapter);
                            rv_comments.setNestedScrollingEnabled(false);
                            commentsAdapter.notifyDataSetChanged();


                        } else {

                            /*No data visiblity*/

                            ll_comment_nodata.setVisibility(View.VISIBLE);
                            progress_dialog.setVisibility(View.GONE);

                        }

                    }

                    @Override
                    public void onFailure(Call<comments_2> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        ll_comment_nodata.setVisibility(View.VISIBLE);
                        progress_dialog.setVisibility(View.GONE);

                    }
                });
            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            /*Loader off */
            progress_dialog.setVisibility(View.GONE);
            Toast.makeText(this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();

        }


    }

    //Create Comments API

    private void createTimelineComments(String cmtTimelineid) {

        if (Utility.isConnected(getApplicationContext())) {

            progress_timeline.setVisibility(View.VISIBLE);
            progress_timeline.setIndeterminate(true);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(Comments_activity.this, "userId");
                Call<CreateEditTimeline_Comments> call = apiService.create_edit_timeline_comments1("create_edit_timeline_comments", 0, Integer.parseInt(cmtTimelineid), org.apache.commons.text.StringEscapeUtils.escapeJava(edittxt_commentnew.getText().toString().trim()), Integer.parseInt(createuserid));
                call.enqueue(new Callback<CreateEditTimeline_Comments>() {
                    @Override
                    public void onResponse(Call<CreateEditTimeline_Comments> call, Response<CreateEditTimeline_Comments> response) {

                        if (response.body().getResponseCode() == 1) {

                            progress_timeline.setVisibility(View.GONE);
                            progress_timeline.setIndeterminate(false);
                            /*Clear text*/

                            edittxt_commentnew.setText("");
                            /*Refresh comments*/
                            get_timeline_comments_all();
                        }

                    }

                    @Override
                    public void onFailure(Call<CreateEditTimeline_Comments> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        progress_timeline.setVisibility(View.GONE);
                        progress_timeline.setIndeterminate(false);}
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }


    }

    //Comments edit API

    private void editTimelineComments(int commentid, int timelineid) {

        if (Utility.isConnected(getApplicationContext())) {

            progress_timeline.setVisibility(View.VISIBLE);
            progress_timeline.setIndeterminate(true);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(Comments_activity.this, "userId");
                Call<CreateEditTimeline_Comments> call = apiService.create_edit_timeline_comments1("create_edit_timeline_comments", commentid, timelineid, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxt_commentnew.getText().toString().trim()), Integer.parseInt(createuserid));
                call.enqueue(new Callback<CreateEditTimeline_Comments>() {
                    @Override
                    public void onResponse(Call<CreateEditTimeline_Comments> call, Response<CreateEditTimeline_Comments> response) {

                        if (response.body().getResponseCode() == 1) {

                            progress_timeline.setVisibility(View.GONE);
                            progress_timeline.setIndeterminate(false);

                            /*Clear text*/

                            edittxt_commentnew.setText("");
                            /*Resetting to default values*/
                            frmAdaptertl_cmmntid = 0;
                            frm_adap_timelineId = 0;
                            clickedforedit = 0;

                            /*Refresh comments*/

                            get_timeline_comments_all();
                        }

                    }

                    @Override
                    public void onFailure(Call<CreateEditTimeline_Comments> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        progress_timeline.setVisibility(View.GONE);
                        progress_timeline.setIndeterminate(false);  }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
            Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }


    }

    //Comments reply API

    private void createEditTimelineCommentsReply(final int comment_id) {

        if (Utility.isConnected(getApplicationContext())) {
            progress_timeline.setVisibility(View.VISIBLE);
            progress_timeline.setIndeterminate(true);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createdby = Pref_storage.getDetail(Comments_activity.this, "userId");
                Call<CreateUserPojoOutput> call = apiService.create_edit_timeline_comments_reply("create_edit_timeline_comments_reply", 0, comment_id, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxt_commentnew.getText().toString().trim()), Integer.parseInt(createdby));
                call.enqueue(new Callback<CreateUserPojoOutput>() {
                    @Override
                    public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                        if (response.body().getResponseCode() == 1) {

                            progress_timeline.setVisibility(View.GONE);
                            progress_timeline.setIndeterminate(false);
                            /*Clear text*/
                            edittxt_commentnew.setText("");
                            rl_replying.setVisibility(View.GONE);


                            /*Resetting to default values*/

                            frmAdaptertl_cmmntid = 0;
                            clickedforreply = 0;

                            /*Refresh comments*/

                            get_timeline_comments_all();


                        }

                    }

                    @Override
                    public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        progress_timeline.setVisibility(View.GONE);
                        progress_timeline.setIndeterminate(false); }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }


    }

    //Comments reply edit API

    private void editTimelineCommentsReply(int replyid, int timelineid) {

        if (Utility.isConnected(getApplicationContext())) {

            progress_timeline.setVisibility(View.VISIBLE);
            progress_timeline.setIndeterminate(true);

            String userid = Pref_storage.getDetail(getApplicationContext(), "userId");
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                Call<CreateUserPojoOutput> call = apiService.edit_timeline_comments_reply("create_edit_timeline_comments_reply", replyid, timelineid, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxt_commentnew.getText().toString().trim()), Integer.parseInt(userid));
                call.enqueue(new Callback<CreateUserPojoOutput>() {
                    @Override
                    public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                        if (response.body().getResponseCode() == 1) {


                            progress_timeline.setVisibility(View.GONE);
                            progress_timeline.setIndeterminate(false);
                            /*Clear text*/
                            edittxt_commentnew.setText("");

                            /*Resetting to default values*/

                            clickedforeditreply = 0;
                            frm_adap_timelineId = 0;
                            frmAdaptertl_cmmntid = 0;

                            /*Refresh comments*/

                            get_timeline_comments_all();
                        }

                    }

                    @Override
                    public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        progress_timeline.setVisibility(View.GONE);
                        progress_timeline.setIndeterminate(false); }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }


    }


    //Delete comments API

    private void create_delete_timeline_comments() {

        if (Utility.isConnected(getApplicationContext())) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(Comments_activity.this, "delete_userid");
                String timeline_commentid = Pref_storage.getDetail(Comments_activity.this, "delete_timelineid");
                String commentid = Pref_storage.getDetail(Comments_activity.this, "delete_commentid");
                Call<CreateUserPojoOutput> call = apiService.create_delete_timeline_comments("create_delete_timeline_comments", Integer.parseInt(commentid), Integer.parseInt(timeline_commentid), Integer.parseInt(createuserid));
                call.enqueue(new Callback<CreateUserPojoOutput>() {
                    @Override
                    public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                        if (response.body().getResponseCode() == 1) {

                            Pref_storage.setDetail(Comments_activity.this, "delete_userid", "");
                            Pref_storage.setDetail(Comments_activity.this, "delete_timelineid", "");
                            Pref_storage.setDetail(Comments_activity.this, "delete_commentid", "");
                            //get_timeline_comments_all();
                        }

                    }

                    @Override
                    public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @Override
    public void onRefresh() {

    }


}




