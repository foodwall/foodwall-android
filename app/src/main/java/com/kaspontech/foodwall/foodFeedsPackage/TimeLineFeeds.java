package com.kaspontech.foodwall.foodFeedsPackage;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.AppTotalCountModuleService;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter.Final_timeline_notification_adapter;
import com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter.TimelineHeaderHolder;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.commonSearchView.CommonSearch;
import com.kaspontech.foodwall.fcm.Constants;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Comments_activity;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Fullimage_activity;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_Adapter.StoriesAdapter;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_Adapter.StoriesUserAdapter;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Get_all_stories_pojo;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Get_stories_pojo_output;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Story_take_photo;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.historicalMapPackage.CreateHistoricalMap;
import com.kaspontech.foodwall.menuPackage.FindFriends;
import com.kaspontech.foodwall.modelclasses.AnswerPojoClass;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.CreateEditEventsComments;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.TaggedPeoplePojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.TimelineLastCmt;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestionAnswerPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Poll.create_delete_answer_poll_undoPOJO;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.PollComments.GetPollCommentsOutput;
import com.kaspontech.foodwall.modelclasses.Review_pojo.ReviewCommentsPojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Image;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.MultiCount;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.New_Getting_timeline_all_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Newtimeline_output_response;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.create_edit_timeline_comments_Response;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.questionspackage.ViewQuestionAnswer;
import com.kaspontech.foodwall.questionspackage.pollcomments.ViewPollComments;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewInputAllPojo;
//
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.VISIBLE;


/**
 * A simple {@link Fragment} subclass.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class TimeLineFeeds extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener,
        Final_timeline_notification_adapter.onRefershListener, onCommentsPostListener, TimelineHeaderHolder.TimelineHeaderItemListener {

    /**
     * Log Tag
     **/

    private static final String TAG = "TimeLineFeeds";
    int timelinenewpost;
    /**
     * View
     **/
    View view;
    /**
     * Context
     **/
    Context context;

    /**
     * Widgets
     **/

    /**
     * Action Bar
     **/
    private Toolbar actionBar;

    /**
     * No post layout
     **/
    //private LinearLayout llNoPost;

    /**
     * Story profile Image view
     **/
    //private ImageView imgStsUpdate;

    /**
     * Main timeline relative layout
     **/
    private RelativeLayout rlTimefeed;
    /**
     * Circle Image view for user comment
     **/
    //private CircleImageView userphotoComment;

    /**
     * Timeline and story recycler view's
     **/
    public static RecyclerView rv_newsfeed;
    //private RecyclerView rv_stories;
    private RecyclerView user_stories;
    long currentVisiblePosition = 0;
    /**
     * Swipe refresh layouts
     **/
    private SwipeRefreshLayout swipeRefreshLayout;
    /**
     * Button for camera, chat and adding post
     **/
    private ImageButton button_camera;

    private ImageButton chat;

    /**
     * Title text for timeline and Story
     **/
    private TextView toolbarTitle;
    // private TextView txtStoryAdd;
    /**
     * Edit text view's for common search and comment text
     **/
    private EditText editCommonSearch;

    // private EditText edittxtComment;

    /**
     * Layout manager for timeline and story feeds
     **/

    private LinearLayoutManager llm;

    private LinearLayoutManager linearLayoutManager;

    /**
     * Status imageview
     **/
    //public static ImageView img_status_timeline;
    /**
     * Post click image
     **/
    public static ImageView img_post;

    /**
     * Timeline feed scroll view
     **/
    //public static ScrollView scrollView;

    /**
     * Loaders for timeline feeds
     **/

    ProgressBar progressTimeline/*,pageLoader*/;

    LottieAnimationView noInternetLoader;

    /**
     * Loading display button
     **/
    Button pageLoader;
    public static Button btnSizeCheck;

    /**
     * Pagination boolean
     **/

    int loaderInt = 0;

    /**
     * String results
     **/

    private String picture;
    public static String imageuri;

    /**
     * Bitmap user photo
     **/
    public static Bitmap photo1;
    /**
     * Uri user photo
     **/
    public static Uri selectedImage;

    /**
     * Request integer
     **/
    private int requestCamera = 0;
    private int selectFile = 1;

    /**
     * Story adapters and timeline feed adapter
     **/
    //private StoriesAdapter storiesAdapter;

    private StoriesUserAdapter storiesUserAdapter;

    /*
     * Timeline all pojo
     * */

    New_Getting_timeline_all_pojo newGettingTimelineAllPojo;

    private static Final_timeline_notification_adapter finalTimelineNotificationAdapter;

    /**
     * Array list for various pojo's
     **/
    private List<String> timeLineImagesList = new ArrayList<>();
    private ArrayList<New_Getting_timeline_all_pojo> gettingtimelinealllist = new ArrayList<>();

    /**
     * Multiimage pojo arraylist
     **/

    // private List<Get_all_stories_pojo> stories_image_list = new ArrayList<>();
    // private List<Get_all_stories_pojo> get_all_stories_pojoList = new ArrayList<>();


    /**
     * Loader Page increment default values
     **/
    int pagecount = 1;

    int TIME = 1200000; //20000 ms (20 Seconds)


    public TimeLineFeeds() {
        // Required empty public constructor
    }

    private boolean undoPoll = false;

    private EmojiconEditText editText_comment, review_comment;

    private ProgressBar comments_progressbar, comments_review, progress_dialog_follower;

    private TextView tv_view_all_comments, tv_view_all_comments_review, commenttest, comment, username_latestviewed, created_on, recently_commented;

    String eventcount, reviewcount, questioncount, postcount;

    private RelativeLayout rl_comments_layout, recently_commented_ll;

    private CircleImageView userphoto;


    TextView txt_adapter_post, answer;
    public EmojiconEditText edittxt_comment;

    private AlertDialog dialog1;

    GPSTracker gpsTracker;

    Double latitude;

    Double longitude;

    public static void updateImgStatusTimeLine(String url) {
        finalTimelineNotificationAdapter.updateImageStatus(url);
    }

    //  @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_time_line_feeds, container, false);
        context = view.getContext();

        /* Widgets initialization */
        initComponents();
        // Setting defaults for paginations in shared preference

        Pref_storage.setDetail(context, "Page_count", "1");

        Pref_storage.setDetail(context, "timelinesize", "1");

        try {
            timelinenewpost = Integer.parseInt(Pref_storage.getDetail(getActivity(), "timeline"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("timelinelist", "oncreate: " + gettingtimelinealllist.size());

        Pref_storage.setDetail(context, "shareView", null);


        get_total_count_modules();

        gettimelineallCount();

        get_stories_all();

        //get_stories_self();

        // Page Resume old method
        if (gettingtimelinealllist.isEmpty()) {

            // Timeline API CALL

            Log.e(TAG, "OnTabChange: size_empty " + gettingtimelinealllist.size());

            gettingtimelinealllist.add(new New_Getting_timeline_all_pojo(true));
            finalTimelineNotificationAdapter = new Final_timeline_notification_adapter(context, gettingtimelinealllist, "ReviewDeliveryDetails", TimeLineFeeds.this, TimeLineFeeds.this, this);
            linearLayoutManager = new LinearLayoutManager(context);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rv_newsfeed.setLayoutManager(linearLayoutManager);
            rv_newsfeed.setVisibility(View.VISIBLE);
            rv_newsfeed.setAdapter(finalTimelineNotificationAdapter);
            rv_newsfeed.invalidate();
            rv_newsfeed.setNestedScrollingEnabled(false);
            rv_newsfeed.setHasFixedSize(true);
            rv_newsfeed.setItemViewCacheSize(20);

            progressTimeline.setVisibility(View.GONE);


            try {

                getTimelineAll_api();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {

            // Timeline resume

            Log.e(TAG, "OnTabChange: size_notempty" + gettingtimelinealllist.size());

            finalTimelineNotificationAdapter = new Final_timeline_notification_adapter(context, gettingtimelinealllist, "ReviewDeliveryDetails", TimeLineFeeds.this, TimeLineFeeds.this, this);
            linearLayoutManager = new LinearLayoutManager(context);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rv_newsfeed.setLayoutManager(linearLayoutManager);
            rv_newsfeed.setVisibility(View.VISIBLE);
            rv_newsfeed.setAdapter(finalTimelineNotificationAdapter);
            rv_newsfeed.invalidate();
            rv_newsfeed.setNestedScrollingEnabled(false);
            rv_newsfeed.setHasFixedSize(true);
            rv_newsfeed.setItemViewCacheSize(20);

        }

        rv_newsfeed.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Hidekeyboard
                    Utility.hideKeyboard(v);
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    // Hidekeyboard
                    Utility.hideKeyboard(v);
                }

                return false;
            }
        });

        rv_newsfeed.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    // Recycle view scrolling down...
                    if (!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)) {
                        loaderInt++;

                        Log.e(TAG, "onScrollChange: loaderInt --->" + loaderInt);

                        if (loaderInt == 2) {
                            //scroll view is at bottom
                            Log.e(TAG, "onScrollChange: scroll view is at bottom");

                            //  Toast.makeText(context, "Bottom reached", Toast.LENGTH_SHORT).show();
                            if (pagecount == 1) {

                            } else {

                                if (Pref_storage.getDetail(context, "Page_count").isEmpty() || Pref_storage.getDetail(context, "Page_count") == null) {

                                } else {
                                    pagecount = Integer.parseInt(Pref_storage.getDetail(context, "Page_count"));
                                    Log.e(TAG, "onScrollChange: pagecount " + pagecount);
                                    //Calling timeline Pagination API
                                    pageLoader.setVisibility(View.VISIBLE);
                                    //gettimelineall_pagination(pagecount);
                                    new gettimelineall_pagination().execute();
                                    loaderInt = 0;
                                }
                            }
                        }
                    }
                }
            }

            /*@Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.d(TAG,"onScrollStateChanged = " + newState);
                if(getActivity()!=null && getActivity() instanceof _SwipeActivityClass){
                    ((_SwipeActivityClass) getActivity()).setOverlookSwipe(false);
                }
            }*/
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                gettimelineall();

                get_total_count_modules();

                //scrollView.smoothScrollTo(0, 0);
                // get_stories_all();
                new allstories().execute();

                // get_stories_self();

                //get all stories
                // new yourstories().execute();

                swipeRefreshLayout.setRefreshing(false);
            }
        });
        //Checking new data avaliable timer

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                // Call API for total size.
                gettimelineall_size();

            }
        }, TIME);


        try {
            //get timeline total count

            Log.e(TAG, "timelinenewpost: " + timelinenewpost);
            if (timelinenewpost == 1) {
                btnSizeCheck.setVisibility(View.VISIBLE);
            } else {
                btnSizeCheck.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;

    }

    private void getTimelineAll_api() {

        Log.e(TAG, "called get timelineall api");

        if (Utility.isConnected(context)) {

            progressTimeline.setVisibility(VISIBLE);
            progressTimeline.setIndeterminate(true);

            noInternetLoader.setVisibility(View.GONE);


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String userId = Pref_storage.getDetail(context, "userId");

            Log.e(TAG, "calling get timelineall enque");

            Call<Newtimeline_output_response> call = apiService.gettimelineall("get_timeline_all",
                    Integer.parseInt(userId), pagecount);

            call.enqueue(new Callback<Newtimeline_output_response>() {
                @Override
                public void onResponse(@NonNull Call<Newtimeline_output_response> call, @NonNull Response<Newtimeline_output_response> response) {


                    Log.e(TAG, "timelineall api response");

                    if (response.body() != null) {

                        if (response.body().getResponseMessage().equals("success")) {


                            Pref_storage.setDetail(context, "timelinesize", String.valueOf(response.body().getData().size()));

                            //llNoPost.setVisibility(View.GONE);
                            finalTimelineNotificationAdapter.notifyItemChanged(0, false);

//                                progressTimeline.setVisibility(View.GONE);


                            if (response.body().getData().size() > 0) {
                                pagecount += 1;

                                Pref_storage.setDetail(context, "Page_count", String.valueOf(pagecount));

                                Log.e(TAG, "onResponse:shared_page-->" + Pref_storage.getDetail(context, "Page_count"));
                            } else {

                            }

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                String timelineId = response.body().getData().get(j).getTimelineId();
                                String timelineHotel = response.body().getData().get(j).getTimelineHotel();
                                String timelineDescription = response.body().getData().get(j).getTimelineDescription();
                                String address = response.body().getData().get(j).getAddress();
                                String createdBy = response.body().getData().get(j).getCreatedBy();
                                String createdOn = response.body().getData().get(j).getCreatedOn();
                                int totalLikes = response.body().getData().get(j).getTotalLikes();
                                String totalComments = response.body().getData().get(j).getTotalComments();
                                String userId = response.body().getData().get(j).getUserId();
                                String visibilityType = response.body().getData().get(j).getVisibilityType();
                                String oauthProvider = response.body().getData().get(j).getOauthProvider();
                                String oauthUid = response.body().getData().get(j).getOauthUid();
                                String firstName1 = response.body().getData().get(j).getFirstName();
                                String lastName1 = response.body().getData().get(j).getLastName();
                                String email1 = response.body().getData().get(j).getEmail();
                                String imei1 = response.body().getData().get(j).getImei();
                                String contNo = response.body().getData().get(j).getContNo();
                                String gender = response.body().getData().get(j).getGender();
                                String dob = response.body().getData().get(j).getDob();
                                picture = response.body().getData().get(j).getPicture();
                                String latitude = response.body().getData().get(j).getLatitude();
                                String longitude = response.body().getData().get(j).getLongitude();
                                String totalPosts = response.body().getData().get(j).getTotalPosts();
                                String totalFollowers = response.body().getData().get(j).getTotalFollowers();
                                String bio = response.body().getData().get(j).getBioDescription();
                                String totalFollowings = response.body().getData().get(j).getTotalFollowings();
                                String postType = response.body().getData().get(j).getPostType();
                                String likesTlId = response.body().getData().get(j).getLikesTlId();
                                String tlLikes = response.body().getData().get(j).getTlLikes();
                                String eventId = response.body().getData().get(j).getEventId();
                                String eventname = response.body().getData().get(j).getEventName();
                                String eventimage = response.body().getData().get(j).getEventImage();
                                String eventdescr = response.body().getData().get(j).getEventDescription();
                                String startdate = response.body().getData().get(j).getStartDate();
                                String enddate = response.body().getData().get(j).getEndDate();
                                String locationn = response.body().getData().get(j).getLocation();
                                String followingId = response.body().getData().get(j).getFollowingId();
                                String followuserId = response.body().getData().get(j).getFollowuserId();
                                String followUserfname = response.body().getData().get(j).getFollowuserFirstname();
                                String followUserLastfname = response.body().getData().get(j).getFollowuserLastname();
                                String followUserpicture = response.body().getData().get(j).getFollowuserPicture();
                                String hotel = response.body().getData().get(j).getHotel();
                                String hotelId = response.body().getData().get(j).getHotelId();
                                String hotelPhoto = response.body().getData().get(j).getHotelPhoto();
                                String googleId = response.body().getData().get(j).getGoogleId();
                                String placeId = response.body().getData().get(j).getPlaceId();
                                String openTimes = response.body().getData().get(j).getOpenTimes();
                                String phone = response.body().getData().get(j).getPhone();
                                String reviewId = response.body().getData().get(j).getReviewId();
                                String review = response.body().getData().get(j).getReview();
                                String reviewComments = response.body().getData().get(j).getReviewComments();
                                String questId = response.body().getData().get(j).getQuestId();
                                String askQuest = response.body().getData().get(j).getAskQuestion();
                                String quesType = response.body().getData().get(j).getQuesType();
                                String ansId = response.body().getData().get(j).getAnsId();
                                String askAns = response.body().getData().get(j).getAskAnswer();
                                String gngEvtId = response.body().getData().get(j).getGngEvtId();
                                String likeEvtId = response.body().getData().get(j).getLikeEvtId();
                                String follUserid = response.body().getData().get(j).getFollUserid();
                                String likeRevId = response.body().getData().get(j).getLikeRevId();
                                String cmmtRevId = response.body().getData().get(j).getCmmtRevId();
                                String ansQuesId = response.body().getData().get(j).getAnsQuesId();
                                String upAnsId = response.body().getData().get(j).getUpAnsId();
                                int timelineLastCmtCount = response.body().getData().get(j).getTimelineLastCmtCount();
                                int whomcount = response.body().getData().get(j).getWhomCount();
                                // int multiCount = response.body().getData().get(j).getMultiCount();

                                String redirect_url = response.body().getData().get(j).getRedirect_url();

                                List<TaggedPeoplePojo> whom = response.body().getData().get(j).getWhom();
                                List<Image> imageList = response.body().getData().get(j).getImage();
                                List<ReviewInputAllPojo> reviewTimelineList = response.body().getData().get(j).getReviewInp();
                                List<QuestionAnswerPojo> questionAnswerPojoList = response.body().getData().get(j).getQuestionInp();
                                List<TimelineLastCmt> timelineLastCmtList = response.body().getData().get(j).getTimelineLastCmt();
                                List<MultiCount> multiCountList = response.body().getData().get(j).getMulti();


                                int imagecount = response.body().getData().get(j).getImageCount();

                                if (imagecount != 0) {

                                    for (int n = 0; n < response.body().getData().get(j).getImage().size(); n++) {

                                        timeLineImagesList.add(response.body().getData().get(j).getImage().get(n).getImg());
                                    }

                                }

                                newGettingTimelineAllPojo = new New_Getting_timeline_all_pojo(timelineId, redirect_url, timelineHotel, timelineDescription, address, createdBy, createdOn, totalLikes, totalComments, latitude,
                                        longitude, visibilityType, postType, userId, oauthProvider, oauthUid, firstName1, lastName1, email1, imei1, contNo, gender, dob, bio, totalFollowers, totalFollowings, totalPosts, picture, likesTlId, tlLikes
                                        , eventId, eventname, eventdescr, eventimage, startdate, enddate, locationn, followingId, followuserId, followUserfname, followUserLastfname, followUserpicture, hotelId, hotel
                                        , hotelPhoto, googleId, placeId, openTimes, phone, reviewId, review, reviewComments, questId, askQuest, quesType, ansId, askAns, gngEvtId, likeEvtId
                                        , follUserid, likeRevId, cmmtRevId, ansQuesId, upAnsId, timelineLastCmtCount, whom, whomcount, imageList, imagecount, reviewTimelineList, questionAnswerPojoList, timelineLastCmtList,
                                        multiCountList
                                        /* ,multiCount*/);


                                gettingtimelinealllist.add(newGettingTimelineAllPojo);


//                            HashSet hs = new HashSet<New_Getting_timeline_all_pojo>();
//                            hs.addAll(gettingtimelinealllist);
//                            gettingtimelinealllist.clear();
//                            gettingtimelinealllist.addAll(hs);
                            }

                            gettingtimelinealllist.set(0, new New_Getting_timeline_all_pojo(true));
                            finalTimelineNotificationAdapter.notifyDataSetChanged();
                            Log.e(TAG, "timeline: 1sttime_size" + gettingtimelinealllist.size());

                            progressTimeline.setVisibility(View.GONE);
                            progressTimeline.setIndeterminate(false);


                        } else if (response.body().getResponseMessage().equals("nodata")) {


                            //TODO if response has no data set llNopost to visible
                            //llNoPost.setVisibility(View.VISIBLE);

                        }
                    }
                }

                @Override
                public void onFailure(Call<Newtimeline_output_response> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "timelineall api" + t.getMessage());

                    //TODO if response has no data set llNopost to visible
                    //llNoPost.setVisibility(View.VISIBLE);

                    progressTimeline.setVisibility(View.GONE);
                    progressTimeline.setIndeterminate(false);

                }
            });
        } else {


            noInternetLoader.setVisibility(View.VISIBLE);

            Toast.makeText(context, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

        }
    }

    private void get_total_count_modules() {

        try {
            if (Utility.isConnected(getActivity())) {


                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {

                    String userid = Pref_storage.getDetail(getActivity(), "userId");

                    Call<TotalCountModule_Response> call = apiService.get_total_count_modules("get_total_count_modules",
                            Integer.parseInt(userid));
                    call.enqueue(new Callback<TotalCountModule_Response>() {
                        @Override
                        public void onResponse(Call<TotalCountModule_Response> call, Response<TotalCountModule_Response> response) {

                            if (response.body().getResponseMessage().equalsIgnoreCase("success")) {

                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    //save total timeline count
                                    Pref_storage.setDetail(getActivity(), "TotalPostCount", String.valueOf(response.body().getData().get(i).getTotalPosts()));

                                    Log.e(TAG, "count total: post" + response.body().getData().get(i).getTotalPosts());

                                    Pref_storage.setDetail(getActivity(), "TotalReviewCount", String.valueOf(response.body().getData().get(i).getTotalReview()));

                                    Log.e(TAG, "counttotal:review " + response.body().getData().get(i).getTotalReview());
                                    Pref_storage.setDetail(getActivity(), "TotalEventCount", String.valueOf(response.body().getData().get(i).getTotalEvents()));

                                    Log.e(TAG, "counttotal:event " + response.body().getData().get(i).getTotalReview());

                                    Pref_storage.setDetail(getActivity(), "TotalQuestionCount", String.valueOf(response.body().getData().get(i).getTotalQuestion()));

                                    Log.e(TAG, "counttotal:question " + response.body().getData().get(i).getTotalQuestion());

                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<TotalCountModule_Response> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void gettimelineallCount() {


        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String userId = Pref_storage.getDetail(context, "userId");

            Call<Newtimeline_output_response> call = apiService.gettimelineall("get_timeline_all", Integer.parseInt(userId), 1);

            call.enqueue(new Callback<Newtimeline_output_response>() {
                @Override
                public void onResponse(@NonNull Call<Newtimeline_output_response> call, @NonNull Response<Newtimeline_output_response> response) {

                    if (response.body() != null) {
                        if (response.body().getResponseMessage().equals("success")) {


                            Pref_storage.setDetail(getActivity(), "TotaltimelineCount", String.valueOf(response.body().getData().size()));


                        }
                    }
                }

                @Override
                public void onFailure(Call<Newtimeline_output_response> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());


                }
            });
        } else {

            Toast.makeText(context, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

        }

        pageLoader.setVisibility(View.GONE);
    }


    @Override
    public void onPause() {
        super.onPause();

        currentVisiblePosition = ((LinearLayoutManager) rv_newsfeed.getLayoutManager()).findFirstCompletelyVisibleItemPosition();

    }

    private void initComponents() {

        context = view.getContext();

//        Fresco.initialize(context);


        actionBar = view.findViewById(R.id.action_bar_home);

        chat = actionBar.findViewById(R.id.chat);

        button_camera = actionBar.findViewById(R.id.button_camera);

        editCommonSearch = actionBar.findViewById(R.id.edit_common_search);


        //userphotoComment = view.findViewById(R.id.userphoto_comment);

        toolbarTitle = view.findViewById(R.id.toolbar_title);

        progressTimeline = view.findViewById(R.id.progress_timeline);

        noInternetLoader = view.findViewById(R.id.img_no_internet);

        pageLoader = view.findViewById(R.id.page_loader);

        btnSizeCheck = view.findViewById(R.id.btn_size_check);

        // scrollView = view.findViewById(R.id.scrollviewtimeline);

        //edittxtComment = view.findViewById(R.id.edittxt_comment);

        //img_status_timeline = view.findViewById(R.id.img_status_timeline);

        //imgStsUpdate = view.findViewById(R.id.img_sts_update);

        //txtStoryAdd = view.findViewById(R.id.txt_story_add);


        rv_newsfeed = view.findViewById(R.id.rv_newsfeed);

        rlTimefeed = view.findViewById(R.id.rl_timefeed);

        swipeRefreshLayout = view.findViewById(R.id.swipe);

        //rv_stories = view.findViewById(R.id.rv_stories);

        //user_stories = view.findViewById(R.id.user_stories);

        //llNoPost = view.findViewById(R.id.ll_no_post);

        //add_post = view.findViewById(R.id.add_post);


        // OnClick listeners

        chat.setOnClickListener(this);

        toolbarTitle.setOnClickListener(this);

        button_camera.setOnClickListener(this);

        editCommonSearch.setOnClickListener(this);

        swipeRefreshLayout.setOnRefreshListener(this);

        pageLoader.setOnClickListener(this);

        btnSizeCheck.setOnClickListener(this);

    }


    /**
     * Resume method
     **/

    @Override
    public void onResume() {
        super.onResume();
        ((LinearLayoutManager) rv_newsfeed.getLayoutManager()).scrollToPosition((int) currentVisiblePosition);
        currentVisiblePosition = 0;

        get_total_count_modules();


    }

    /**
     * On Swipe refresh calling all API's (Timeline feeds and stories API)
     **/
    @Override
    public void onRefresh() {


        rv_newsfeed.scrollToPosition(0);
        // get_stories_all();
        new allstories().execute();

        // new yourstories().execute();


    }

    /**
     * Timeline header view click listeners
     */
    @Override
    public void onAddPostClicked() {
        // Add Post image click to redirect for create timeline data
        Intent intent3 = new Intent(getActivity(), CreateHistoricalMap.class);
        startActivity(intent3);
    }

    @Override
    public void txtStoryAddClicked() {
        // Textview click to redirect for create story data
        Intent storyintent1 = new Intent(getActivity(), Story_take_photo.class);
        startActivity(storyintent1);
    }

    @Override
    public void imgStsUpdateClicked() {
        Intent storyintent = new Intent(getActivity(), Story_take_photo.class);
        startActivity(storyintent);
    }

    @Override
    public void edittxtCommentClicked() {
        Intent intent2 = new Intent(getActivity(), CreateHistoricalMap.class);
        startActivity(intent2);
    }

    @Override
    public void imgStatusTimelineClicked() {
        Intent chatintent = new Intent(getActivity(), Story_take_photo.class);
        startActivity(chatintent);
    }

    /**
     * Timeline no item view click listeners
     */
    @Override
    public void onNoItemYetClicked() {
        Intent intent6 = new Intent(getActivity(), FindFriends.class);
        startActivity(intent6);
    }

    /**
     * On clikc method
     **/

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {

            // Redirecting to Common search page

            case R.id.edit_common_search:

                Intent intent1 = new Intent(getActivity(), CommonSearch.class);
                startActivity(intent1);

                break;
            // Scrolling to top when title is clicked

            case R.id.toolbar_title:
                rv_newsfeed.scrollToPosition(0);
                break;

            // Camera button click to redirect for create timeline data
            case R.id.button_camera:

                Intent intent = new Intent(getActivity(), CreateHistoricalMap.class);
                startActivity(intent);

                break;


            // Chat image button click to redirect chat activity

            case R.id.chat:
                startActivity(new Intent(getActivity(), ChatActivity.class));
                break;

            // Button size check for new data listening


            case R.id.btn_size_check:

                btnSizeCheck.setVisibility(View.GONE);

                // Pref_storage.setDetail(getActivity(), "timeline", "0");

                get_total_count_modules();


                // Calling API for new data incoming
                getMoredata();

                rv_newsfeed.scrollToPosition(0);


                break;

        }

    }


    // Api calls


    //Timeline API calling
    private void gettimelineall() {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String user_id = Pref_storage.getDetail(context, "userId");

            Call<Newtimeline_output_response> call = apiService.gettimelineall(
                    "get_timeline_all",
                    Integer.parseInt(user_id),
                    1);

            call.enqueue(new Callback<Newtimeline_output_response>() {
                @Override
                public void onResponse(Call<Newtimeline_output_response> call, Response<Newtimeline_output_response> response) {

                    if (response.body().getResponseMessage().equals("success")) {


                        Pref_storage.setDetail(context, "timelinesize", String.valueOf(response.body().getData().size()));


                        if (comments_review != null) {

                            comments_review.setVisibility(View.GONE);
                            comments_review.setIndeterminate(false);

                        } else {
                            //TODO implement llNOPost login
                            //llNoPost.setVisibility(View.GONE);
                            finalTimelineNotificationAdapter.notifyItemChanged(0, false);
                        }

                        gettingtimelinealllist.clear();
                        gettingtimelinealllist.add(new New_Getting_timeline_all_pojo(true));
                        if (response.body().getData().size() > 0) {
                            // pagecount = 2;
                            // pagecount += 1;

                            //  Pref_storage.setDetail(context, "Page_count", String.valueOf(pagecount));

                            Log.e(TAG, "onResponse:shared_page-->" + Pref_storage.getDetail(context, "Page_count"));
                        } else {

                        }

                        for (int j = 0; j < response.body().getData().size(); j++) {


                            String timelineId = response.body().getData().get(j).getTimelineId();
                            String redirect_url = response.body().getData().get(j).getRedirect_url();
                            String timelineHotel = response.body().getData().get(j).getTimelineHotel();
                            String timelineDescription = response.body().getData().get(j).getTimelineDescription();
                            String address = response.body().getData().get(j).getAddress();
                            String createdBy = response.body().getData().get(j).getCreatedBy();
                            String createdOn = response.body().getData().get(j).getCreatedOn();
                            int totalLikes = response.body().getData().get(j).getTotalLikes();
                            String totalComments = response.body().getData().get(j).getTotalComments();
                            String userId = response.body().getData().get(j).getUserId();
                            String visibilityType = response.body().getData().get(j).getVisibilityType();
                            String oauthProvider = response.body().getData().get(j).getOauthProvider();
                            String oauthUid = response.body().getData().get(j).getOauthUid();
                            String firstName1 = response.body().getData().get(j).getFirstName();
                            String lastName1 = response.body().getData().get(j).getLastName();
                            String email1 = response.body().getData().get(j).getEmail();
                            String imei1 = response.body().getData().get(j).getImei();
                            String contNo = response.body().getData().get(j).getContNo();
                            String gender = response.body().getData().get(j).getGender();
                            String dob = response.body().getData().get(j).getDob();
                            picture = response.body().getData().get(j).getPicture();
                            String latitude = response.body().getData().get(j).getLatitude();
                            String longitude = response.body().getData().get(j).getLongitude();
                            String totalPosts = response.body().getData().get(j).getTotalPosts();
                            int self_liked = response.body().getData().get(j).getTotalLikes();
                            String totalFollowers = response.body().getData().get(j).getTotalFollowers();
                            int isfollowing = Integer.parseInt(response.body().getData().get(j).getFollowingId());
                            String bio = response.body().getData().get(j).getBioDescription();
                            String totalFollowings = response.body().getData().get(j).getTotalFollowings();
                            String postType = response.body().getData().get(j).getPostType();
                            String likesTlId = response.body().getData().get(j).getLikesTlId();
                            String tlLikes = response.body().getData().get(j).getTlLikes();
                            String eventId = response.body().getData().get(j).getEventId();
                            String eventname = response.body().getData().get(j).getEventName();
                            String eventimage = response.body().getData().get(j).getEventImage();
                            String eventdescr = response.body().getData().get(j).getEventDescription();
                            String startdate = response.body().getData().get(j).getStartDate();
                            String enddate = response.body().getData().get(j).getEndDate();
                            String locationn = response.body().getData().get(j).getLocation();
                            String followingId = response.body().getData().get(j).getFollowingId();
                            String followuserId = response.body().getData().get(j).getFollowuserId();
                            String followUserfname = response.body().getData().get(j).getFollowuserFirstname();
                            String followUserLastfname = response.body().getData().get(j).getFollowuserLastname();
                            String followUserpicture = response.body().getData().get(j).getFollowuserPicture();
                            String hotel = response.body().getData().get(j).getHotel();
                            String hotelId = response.body().getData().get(j).getHotelId();
                            String hotelPhoto = response.body().getData().get(j).getHotelPhoto();
                            String googleId = response.body().getData().get(j).getGoogleId();
                            String placeId = response.body().getData().get(j).getPlaceId();
                            String openTimes = response.body().getData().get(j).getOpenTimes();
                            String phone = response.body().getData().get(j).getPhone();
                            String reviewId = response.body().getData().get(j).getReviewId();
                            String review = response.body().getData().get(j).getReview();
                            String reviewComments = response.body().getData().get(j).getReviewComments();
                            String questId = response.body().getData().get(j).getQuestId();
                            String askQuest = response.body().getData().get(j).getAskQuestion();
                            String quesType = response.body().getData().get(j).getQuesType();
                            String ansId = response.body().getData().get(j).getAnsId();
                            String askAns = response.body().getData().get(j).getAskAnswer();
                            String gngEvtId = response.body().getData().get(j).getGngEvtId();
                            String likeEvtId = response.body().getData().get(j).getLikeEvtId();
                            String follUserid = response.body().getData().get(j).getFollUserid();
                            String likeRevId = response.body().getData().get(j).getLikeRevId();
                            String cmmtRevId = response.body().getData().get(j).getCmmtRevId();
                            String ansQuesId = response.body().getData().get(j).getAnsQuesId();
                            String upAnsId = response.body().getData().get(j).getUpAnsId();
                            int timelineLastCmtCount = response.body().getData().get(j).getTimelineLastCmtCount();
                            int whomcount = response.body().getData().get(j).getWhomCount();
                            // int multiCount = response.body().getData().get(j).getMultiCount();


                            List<TaggedPeoplePojo> whom = response.body().getData().get(j).getWhom();
                            List<Image> imageList = response.body().getData().get(j).getImage();
                            List<ReviewInputAllPojo> reviewTimelineList = response.body().getData().get(j).getReviewInp();
                            List<QuestionAnswerPojo> questionAnswerPojoList = response.body().getData().get(j).getQuestionInp();
                            List<TimelineLastCmt> timelineLastCmtList = response.body().getData().get(j).getTimelineLastCmt();
                            List<MultiCount> multiCountList = response.body().getData().get(j).getMulti();


                            int imagecount = response.body().getData().get(j).getImageCount();

                            if (imagecount != 0) {

                                for (int n = 0; n < response.body().getData().get(j).getImage().size(); n++) {

                                    timeLineImagesList.add(response.body().getData().get(j).getImage().get(n).getImg());
                                }

                            }

                            newGettingTimelineAllPojo = new New_Getting_timeline_all_pojo(timelineId, redirect_url, timelineHotel, timelineDescription, address, createdBy, createdOn, totalLikes, totalComments, latitude,
                                    longitude, visibilityType, postType, userId, oauthProvider, oauthUid, firstName1, lastName1, email1, imei1, contNo, gender, dob, bio, totalFollowers, totalFollowings, totalPosts, picture, likesTlId, tlLikes
                                    , eventId, eventname, eventdescr, eventimage, startdate, enddate, locationn, followingId, followuserId, followUserfname, followUserLastfname, followUserpicture, hotelId, hotel
                                    , hotelPhoto, googleId, placeId, openTimes, phone, reviewId, review, reviewComments, questId, askQuest, quesType, ansId, askAns, gngEvtId, likeEvtId
                                    , follUserid, likeRevId, cmmtRevId, ansQuesId, upAnsId, timelineLastCmtCount, whom, whomcount, imageList, imagecount, reviewTimelineList, questionAnswerPojoList, timelineLastCmtList,
                                    multiCountList
                                    /* ,multiCount*/);


                            gettingtimelinealllist.add(newGettingTimelineAllPojo);


                            progressTimeline.setVisibility(View.GONE);
                            progressTimeline.setIndeterminate(false);
                            Log.e(TAG, "timeline: 1sttime_size" + gettingtimelinealllist.size());


                        }


                        finalTimelineNotificationAdapter.notifyDataSetChanged();

                    } else if (response.body().getResponseMessage().equals("nodata")) {

                        if (comments_review != null) {

                            comments_review.setVisibility(View.GONE);
                            comments_review.setIndeterminate(false);

                        } else {

                            //llNoPost.setVisibility(View.VISIBLE);
                            finalTimelineNotificationAdapter.notifyItemChanged(0, true);

                        }


                    }
                }

                @Override
                public void onFailure(Call<Newtimeline_output_response> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    if (comments_review != null) {

                        comments_review.setVisibility(View.GONE);
                        comments_review.setIndeterminate(false);

                    } else {

                        progressTimeline.setIndeterminate(false);
                        //llNoPost.setVisibility(View.VISIBLE);
                        finalTimelineNotificationAdapter.notifyItemChanged(0, true);

                    }

                }
            });
        } else {

            Toast.makeText(context, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

        }

    }

    /**
     * New data avalable data checking API for timeline feeds
     **/
    private void getMoredata() {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String userId = Pref_storage.getDetail(context, "userId");

            Call<Newtimeline_output_response> call = apiService.gettimelineall("get_timeline_all",
                    Integer.parseInt(userId), 1);


            call.enqueue(new Callback<Newtimeline_output_response>() {
                @Override
                public void onResponse(Call<Newtimeline_output_response> call, Response<Newtimeline_output_response> response) {

                    if (response.body().getResponseMessage().equals("success")) {


                        Pref_storage.setDetail(context, "timelinesize", String.valueOf(response.body().getData().size()));


                        //llNoPost.setVisibility(View.GONE);
                        finalTimelineNotificationAdapter.notifyItemChanged(0, false);
                        progressTimeline.setVisibility(View.GONE);
                        progressTimeline.setIndeterminate(false);

                        Pref_storage.setDetail(getContext(), "Event", "0");
                        Pref_storage.setDetail(getContext(), "Post", "0");
                        Pref_storage.setDetail(getContext(), "Question", "0");
                        Pref_storage.setDetail(getContext(), "Review", "0");


                        //List clear

                        timeLineImagesList.clear();
                        gettingtimelinealllist.clear();
                        gettingtimelinealllist.add(new New_Getting_timeline_all_pojo(true));

                        if (response.body().getData().size() > 0) {

                            Pref_storage.setDetail(context, "Page_count", String.valueOf(2));
                        } else {

                        }
                        rv_newsfeed.scrollToPosition(0);


                        for (int j = 0; j < response.body().getData().size(); j++) {


                            String timelineId = response.body().getData().get(j).getTimelineId();
                            String timelineHotel = response.body().getData().get(j).getTimelineHotel();
                            String timelineDescription = response.body().getData().get(j).getTimelineDescription();
                            String address = response.body().getData().get(j).getAddress();
                            String createdBy = response.body().getData().get(j).getCreatedBy();
                            String createdOn = response.body().getData().get(j).getCreatedOn();
                            int totalLikes = response.body().getData().get(j).getTotalLikes();
                            String totalComments = response.body().getData().get(j).getTotalComments();
                            String userId = response.body().getData().get(j).getUserId();
                            String visibilityType = response.body().getData().get(j).getVisibilityType();
                            String oauthProvider = response.body().getData().get(j).getOauthProvider();
                            String oauthUid = response.body().getData().get(j).getOauthUid();
                            String firstName1 = response.body().getData().get(j).getFirstName();
                            String lastName1 = response.body().getData().get(j).getLastName();
                            String email1 = response.body().getData().get(j).getEmail();
                            String imei1 = response.body().getData().get(j).getImei();
                            String contNo = response.body().getData().get(j).getContNo();
                            String gender = response.body().getData().get(j).getGender();
                            String dob = response.body().getData().get(j).getDob();
                            picture = response.body().getData().get(j).getPicture();
                            String latitude = response.body().getData().get(j).getLatitude();
                            String longitude = response.body().getData().get(j).getLongitude();
                            String totalPosts = response.body().getData().get(j).getTotalPosts();
                            int self_liked = response.body().getData().get(j).getTotalLikes();
                            String totalFollowers = response.body().getData().get(j).getTotalFollowers();
                            int isfollowing = Integer.parseInt(response.body().getData().get(j).getFollowingId());
                            String bio = response.body().getData().get(j).getBioDescription();
                            String totalFollowings = response.body().getData().get(j).getTotalFollowings();
                            String postType = response.body().getData().get(j).getPostType();
                            String likesTlId = response.body().getData().get(j).getLikesTlId();
                            String tlLikes = response.body().getData().get(j).getTlLikes();
                            String eventId = response.body().getData().get(j).getEventId();
                            String eventname = response.body().getData().get(j).getEventName();
                            String eventimage = response.body().getData().get(j).getEventImage();
                            String eventdescr = response.body().getData().get(j).getEventDescription();
                            String startdate = response.body().getData().get(j).getStartDate();
                            String enddate = response.body().getData().get(j).getEndDate();
                            String locationn = response.body().getData().get(j).getLocation();
                            String followingId = response.body().getData().get(j).getFollowingId();
                            String followuserId = response.body().getData().get(j).getFollowuserId();
                            String followUserfname = response.body().getData().get(j).getFollowuserFirstname();
                            String followUserLastfname = response.body().getData().get(j).getFollowuserLastname();
                            String followUserpicture = response.body().getData().get(j).getFollowuserPicture();
                            String hotel = response.body().getData().get(j).getHotel();
                            String hotelId = response.body().getData().get(j).getHotelId();
                            String hotelPhoto = response.body().getData().get(j).getHotelPhoto();
                            String googleId = response.body().getData().get(j).getGoogleId();
                            String placeId = response.body().getData().get(j).getPlaceId();
                            String openTimes = response.body().getData().get(j).getOpenTimes();
                            String phone = response.body().getData().get(j).getPhone();
                            String reviewId = response.body().getData().get(j).getReviewId();
                            String review = response.body().getData().get(j).getReview();
                            String reviewComments = response.body().getData().get(j).getReviewComments();
                            String questId = response.body().getData().get(j).getQuestId();
                            String askQuest = response.body().getData().get(j).getAskQuestion();
                            String quesType = response.body().getData().get(j).getQuesType();
                            String ansId = response.body().getData().get(j).getAnsId();
                            String askAns = response.body().getData().get(j).getAskAnswer();
                            String gngEvtId = response.body().getData().get(j).getGngEvtId();
                            String likeEvtId = response.body().getData().get(j).getLikeEvtId();
                            String follUserid = response.body().getData().get(j).getFollUserid();
                            String likeRevId = response.body().getData().get(j).getLikeRevId();
                            String cmmtRevId = response.body().getData().get(j).getCmmtRevId();
                            String ansQuesId = response.body().getData().get(j).getAnsQuesId();
                            String upAnsId = response.body().getData().get(j).getUpAnsId();
                            int timelineLastCmtCount = response.body().getData().get(j).getTimelineLastCmtCount();
                            int whomcount = response.body().getData().get(j).getWhomCount();
                            // int multiCount = response.body().getData().get(j).getMultiCount();


                            List<TaggedPeoplePojo> whom = response.body().getData().get(j).getWhom();
                            List<Image> imageList = response.body().getData().get(j).getImage();
                            List<ReviewInputAllPojo> reviewTimelineList = response.body().getData().get(j).getReviewInp();
                            List<QuestionAnswerPojo> questionAnswerPojoList = response.body().getData().get(j).getQuestionInp();
                            List<TimelineLastCmt> timelineLastCmtList = response.body().getData().get(j).getTimelineLastCmt();
                            List<MultiCount> multiCountList = response.body().getData().get(j).getMulti();


                            int imagecount = response.body().getData().get(j).getImageCount();

                            if (imagecount != 0) {

                                for (int n = 0; n < response.body().getData().get(j).getImage().size(); n++) {

                                    timeLineImagesList.add(response.body().getData().get(j).getImage().get(n).getImg());
                                }

                            }
                            String redirect_url = response.body().getData().get(j).getRedirect_url();

                            newGettingTimelineAllPojo = new New_Getting_timeline_all_pojo(timelineId, redirect_url, timelineHotel, timelineDescription, address, createdBy, createdOn, totalLikes, totalComments, latitude,
                                    longitude, visibilityType, postType, userId, oauthProvider, oauthUid, firstName1, lastName1, email1, imei1, contNo, gender, dob, bio, totalFollowers, totalFollowings, totalPosts, picture, likesTlId, tlLikes
                                    , eventId, eventname, eventdescr, eventimage, startdate, enddate, locationn, followingId, followuserId, followUserfname, followUserLastfname, followUserpicture, hotelId, hotel
                                    , hotelPhoto, googleId, placeId, openTimes, phone, reviewId, review, reviewComments, questId, askQuest, quesType, ansId, askAns, gngEvtId, likeEvtId
                                    , follUserid, likeRevId, cmmtRevId, ansQuesId, upAnsId, timelineLastCmtCount, whom, whomcount, imageList, imagecount, reviewTimelineList, questionAnswerPojoList, timelineLastCmtList,
                                    multiCountList
                                    /* ,multiCount*/);


                            gettingtimelinealllist.add(newGettingTimelineAllPojo);


//                            HashSet hs = new HashSet<New_Getting_timeline_all_pojo>();
//                            hs.addAll(gettingtimelinealllist);
//                            gettingtimelinealllist.clear();
//                            gettingtimelinealllist.addAll(hs);


                            Log.e(TAG, "timeline: 1sttime_size" + gettingtimelinealllist.size());


                        }

                        /// Duplicate objects removing using Linked  hash set

                        LinkedHashSet<New_Getting_timeline_all_pojo> s = new LinkedHashSet<>(gettingtimelinealllist);
                        s.addAll(gettingtimelinealllist);
                        gettingtimelinealllist.clear();
                        gettingtimelinealllist.add(new New_Getting_timeline_all_pojo(true));
                        gettingtimelinealllist.addAll(s);

                        finalTimelineNotificationAdapter.notifyDataSetChanged();

                    } else if (response.body().getResponseMessage().equals("nodata")) {

                        //llNoPost.setVisibility(View.VISIBLE);
                        finalTimelineNotificationAdapter.notifyItemChanged(0, true);
                    }
                }

                @Override
                public void onFailure(Call<Newtimeline_output_response> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    progressTimeline.setIndeterminate(false);
                    //llNoPost.setVisibility(View.VISIBLE);
                    finalTimelineNotificationAdapter.notifyItemChanged(0, true);

                }
            });
        } else {

            Toast.makeText(context, "No internet connection.", Toast.LENGTH_SHORT).show();

        }

    }


    @Override
    public void onRefereshedFragment(View view, int adapterPosition, View pollAdapterView) {

        comments_review = (ProgressBar) pollAdapterView.findViewById(R.id.comments_review);

        int totalVotes = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
        totalVotes = totalVotes - 1;

        if (totalVotes <= 0) {

            gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).setTotalPollUser(totalVotes);
            TextView total_poll_votes = (TextView) pollAdapterView.findViewById(R.id.total_poll_votes);
            total_poll_votes.setText("No Votes");

        } else if (totalVotes == 1) {

            gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).setTotalPollUser(totalVotes);
            TextView total_poll_votes = (TextView) pollAdapterView.findViewById(R.id.total_poll_votes);
            total_poll_votes.setText(totalVotes + " Vote");

        } else {

            gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).setTotalPollUser(totalVotes);
            TextView total_poll_votes = (TextView) pollAdapterView.findViewById(R.id.total_poll_votes);
            total_poll_votes.setText(totalVotes + " Vote");

        }

        for (int i = 0; i < gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().size(); i++) {

            if (gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPollId() == gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(i).getPollId()) {

                if (!undoPoll) {

                    int totalPoll = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(i).getTotalPoll();
                    totalPoll = totalPoll - 1;
                    gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(i).setTotalPoll(totalPoll);
                    undoPoll = true;

                }

            }

        }

        // Display progress
        final LinearLayout ll_progress_section = pollAdapterView.findViewById(R.id.ll_progress_section);
        final LinearLayout ll_answer_section = pollAdapterView.findViewById(R.id.ll_answer_section);
        final TextView tv_poll_undo = pollAdapterView.findViewById(R.id.tv_poll_undo);
        final ImageView poll_one_checked = pollAdapterView.findViewById(R.id.poll_one_checked);
        final ImageView poll_two_checked = pollAdapterView.findViewById(R.id.poll_two_checked);
        final ImageView poll_three_checked = pollAdapterView.findViewById(R.id.poll_three_checked);
        final ImageView poll_four_checked = pollAdapterView.findViewById(R.id.poll_four_checked);


        ll_progress_section.setVisibility(View.GONE);
        ll_answer_section.setVisibility(View.VISIBLE);
        tv_poll_undo.setVisibility(View.GONE);
        poll_one_checked.setVisibility(View.GONE);
        poll_two_checked.setVisibility(View.GONE);
        poll_three_checked.setVisibility(View.GONE);
        poll_four_checked.setVisibility(View.GONE);


        if (Utility.isConnected(getActivity())) {

            comments_review.setVisibility(VISIBLE);
            comments_review.setIndeterminate(true);
            // Undo API call

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Call<create_delete_answer_poll_undoPOJO> call = apiService.undoPollVote("create_delete_answer_poll_undo",
                        Integer.parseInt(gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getQuestId()),
                        userid);

                call.enqueue(new Callback<create_delete_answer_poll_undoPOJO>() {

                    @Override
                    public void onResponse(Call<create_delete_answer_poll_undoPOJO> call, Response<create_delete_answer_poll_undoPOJO> response) {

                        if (response.body() != null) {


                            String responseStatus = response.body().getResponseMessage();

                            Log.e("Balaji", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                //Success

                          /*  ll_progress_section.setVisibility(View.GONE);
                            ll_answer_section.setVisibility(View.VISIBLE);
                            tv_poll_undo.setVisibility(View.GONE);
                            poll_one_checked.setVisibility(View.GONE);
                            poll_two_checked.setVisibility(View.GONE);
                            poll_three_checked.setVisibility(View.GONE);
                            poll_four_checked.setVisibility(View.GONE);
*/
                            /*Intent intent = new Intent(context, Home.class);
                            Bundle bundle = new Bundle();
                            bundle.putInt("launchType", 3);
                            intent.putExtras(bundle);
                            context.startActivity(intent);*/
                                //((AppCompatActivity) context).finish();*//*

//                                            getAllQuesData();

                                gettimelineall();

                            }

                        } else {

                            comments_review.setVisibility(View.GONE);
                            comments_review.setIndeterminate(false);

                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<create_delete_answer_poll_undoPOJO> call, Throwable t) {
                        //Error
                        Log.e("Balaji", "" + t.getMessage());

                        comments_review.setVisibility(View.GONE);
                        comments_review.setIndeterminate(false);
                    }
                });

            } catch (Exception e) {

                comments_review.setVisibility(View.GONE);
                comments_review.setIndeterminate(false);

                e.printStackTrace();

            }
        } else {

            Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressLint("CutPasteId")
    @Override
    public void onPostCommitedView(View view, final int adapterPosition, View hotelDetailsView) {

        if (gettingtimelinealllist.get(adapterPosition).getPostType().equals("5") ||
                gettingtimelinealllist.get(adapterPosition).getPostType().equals("6") ||
                gettingtimelinealllist.get(adapterPosition).getPostType().equals("7")) {

            review_comment = (EmojiconEditText) hotelDetailsView.findViewById(R.id.review_comment);

        } else {

            editText_comment = (EmojiconEditText) hotelDetailsView.findViewById(R.id.edittxt_comment);
        }

        if (gettingtimelinealllist.get(adapterPosition).getPostType().equals("8")) {
            comments_review = (ProgressBar) hotelDetailsView.findViewById(R.id.comments_review);

        } else {
            comments_progressbar = (ProgressBar) hotelDetailsView.findViewById(R.id.comments_progressbar);

        }

        tv_view_all_comments = (TextView) hotelDetailsView.findViewById(R.id.View_all_comment);
        tv_view_all_comments_review = (TextView) hotelDetailsView.findViewById(R.id.tv_view_all_comments);

        comment = (TextView) hotelDetailsView.findViewById(R.id.comment);
        username_latestviewed = (TextView) hotelDetailsView.findViewById(R.id.username_latestviewed);
        created_on = (TextView) hotelDetailsView.findViewById(R.id.created_on);
        recently_commented = (TextView) hotelDetailsView.findViewById(R.id.recently_commented);

        commenttest = (TextView) hotelDetailsView.findViewById(R.id.commenttest);

        rl_comments_layout = (RelativeLayout) hotelDetailsView.findViewById(R.id.rl_comments_layout);
        recently_commented_ll = (RelativeLayout) hotelDetailsView.findViewById(R.id.recently_commented_ll);

        userphoto = (CircleImageView) hotelDetailsView.findViewById(R.id.userphoto);
        edittxt_comment = (EmojiconEditText) hotelDetailsView.findViewById(R.id.edittxt_comment);
        txt_adapter_post = (TextView) hotelDetailsView.findViewById(R.id.txt_adapter_post);

        String postType = gettingtimelinealllist.get(adapterPosition).getPostType();


        switch (postType) {

            case "0":

                switch (view.getId()) {

                    case R.id.txt_adapter_post:

                        if (Utility.isConnected(getActivity())) {

                            if (editText_comment.getText().toString().isEmpty()) {

                                Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();

                            } else {

                                comments_progressbar.setVisibility(VISIBLE);
                                comments_progressbar.setIndeterminate(true);

                                Utility.hideKeyboard(view);

                                //api call
                                createEditTimelineComments(gettingtimelinealllist.get(adapterPosition).getTimelineId(), editText_comment.getText().toString(), adapterPosition);
                            }

                        } else {

                            Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                        break;


                    case R.id.img_timelinesharechat:

                        chagePageView(adapterPosition);
                        break;
                }
                break;

            case "1":

                switch (view.getId()) {

                    case R.id.txt_adapter_post:

                        if (Utility.isConnected(getActivity())) {

                            if (editText_comment.getText().toString().isEmpty()) {

                                Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();

                            } else {

                                comments_progressbar.setVisibility(VISIBLE);
                                comments_progressbar.setIndeterminate(true);

                                Utility.hideKeyboard(view);

                                // api call
                                createEditEventsComments(gettingtimelinealllist.get(adapterPosition).getEventId(), editText_comment.getText().toString(), adapterPosition);
                            }

                        } else {

                            Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.img_eventssharechat:

                        chagePageView(adapterPosition);
                        break;
                }
                break;

            case "2":

                switch (view.getId()) {

                    case R.id.txt_adapter_post:

                        if (Utility.isConnected(getActivity())) {

                            if (editText_comment.getText().toString().isEmpty()) {

                                Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();

                            } else {

                                comments_progressbar.setVisibility(VISIBLE);
                                comments_progressbar.setIndeterminate(true);

                                Utility.hideKeyboard(view);


                                createEditEventsComments(gettingtimelinealllist.get(adapterPosition).getEventId(), editText_comment.getText().toString(), adapterPosition);
                            }

                        } else {

                            Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.img_eventsgoingsharechat:

                        chagePageView(adapterPosition);
                        break;
                }
                break;

            case "3":

                switch (view.getId()) {

                    case R.id.txt_adapter_post:

                        if (Utility.isConnected(getActivity())) {

                            if (editText_comment.getText().toString().isEmpty()) {

                                Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();

                            } else {

                                comments_progressbar.setVisibility(VISIBLE);
                                comments_progressbar.setIndeterminate(true);

                                Utility.hideKeyboard(view);

                                createEditEventsComments(gettingtimelinealllist.get(adapterPosition).getEventId(), editText_comment.getText().toString(), adapterPosition);
                            }

                        } else {

                            Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.img_eventsinterestedsharechat:

                        chagePageView(adapterPosition);
                        break;
                }
                break;


            case "5":

                switch (view.getId()) {

                    case R.id.txt_adapter_post:

                        if (Utility.isConnected(context)) {

                            if (review_comment.getText().toString().isEmpty()) {

                                Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();

                            } else {

                                comments_progressbar.setVisibility(VISIBLE);
                                comments_progressbar.setIndeterminate(true);

                                Utility.hideKeyboard(view);

                                createEditReviewComments(gettingtimelinealllist.get(adapterPosition).getHotelId(),
                                        review_comment.getText().toString(),
                                        gettingtimelinealllist.get(adapterPosition).getReviewId(), adapterPosition);
                            }

                        } else {

                            Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.img_reviewsharechat:

                        chagePageView(adapterPosition);
                        break;
                }
                break;

            case "6":

                switch (view.getId()) {

                    case R.id.txt_adapter_post:

                        if (Utility.isConnected(context)) {

                            if (review_comment.getText().toString().isEmpty()) {

                                Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();

                            } else {

                                comments_progressbar.setVisibility(VISIBLE);
                                comments_progressbar.setIndeterminate(true);

                                Utility.hideKeyboard(view);

                                createEditReviewComments(gettingtimelinealllist.get(adapterPosition).getHotelId(),
                                        review_comment.getText().toString(),
                                        gettingtimelinealllist.get(adapterPosition).getReviewId(), adapterPosition);
                            }

                        } else {

                            Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.img_reviewsharechat:

                        chagePageView(adapterPosition);
                        break;
                }
                break;

            case "7":

                switch (view.getId()) {

                    case R.id.txt_adapter_post:

                        if (Utility.isConnected(context)) {

                            if (review_comment.getText().toString().isEmpty()) {

                                Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();

                            } else {

                                comments_progressbar.setVisibility(VISIBLE);
                                comments_progressbar.setIndeterminate(true);

                                Utility.hideKeyboard(view);

                                createEditReviewComments(gettingtimelinealllist.get(adapterPosition).getHotelId(),
                                        review_comment.getText().toString(),
                                        gettingtimelinealllist.get(adapterPosition).getReviewId(), adapterPosition);
                            }

                        } else {

                            Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.img_reviewsharechat:

                        chagePageView(adapterPosition);
                        break;
                }
                break;

            case "8":

                if (gettingtimelinealllist.get(adapterPosition).getPostType().equalsIgnoreCase("8") && gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getQuesType().equalsIgnoreCase("1")) {

                    if (Utility.isConnected(Objects.requireNonNull(getActivity()))) {

                        if (editText_comment.getText().toString().isEmpty()) {

                            Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();

                        } else {
                            createEditPollComments(gettingtimelinealllist.get(adapterPosition).getQuestId(), adapterPosition);

                        }

                    } else {

                        Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    switch (view.getId()) {

                        case R.id.ll_answer:

                            getAnswerCreatePopupView(adapterPosition, hotelDetailsView);
                            break;

                        case R.id.img_sharechat:

                            chagePageView(adapterPosition);
                            break;
                    }
                }
                break;


            case "9":

                switch (view.getId()) {

                    case R.id.ll_answer:

                        getAnswerCreatePopupView(adapterPosition, hotelDetailsView);
                        break;

                    case R.id.img_sharechat:

                        chagePageView(adapterPosition);
                        break;
                }
                break;

            case "10":

                switch (view.getId()) {

                    case R.id.ll_answer:

                        getAnswerCreatePopupView(adapterPosition, hotelDetailsView);
                        break;

                    case R.id.img_sharechat:

                        chagePageView(adapterPosition);
                        break;
                }
                break;


            case "11":

                if (view.getId() == R.id.img_sharechat) {
                    chagePageView(adapterPosition);
                }
                break;

            default:

                break;

        }

    }

    private void getAnswerCreatePopupView(final int adapterPosition, View hotelDetailsView) {

        final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(context);
        LayoutInflater inflater11 = LayoutInflater.from(context);
        @SuppressLint("InflateParams") final View dialogView1 = inflater11.inflate(R.layout.alert_answer_layout, null);
        dialogBuilder1.setView(dialogView1);
        dialogBuilder1.setCancelable(false);
        dialogBuilder1.setTitle("");

        answer = (TextView) hotelDetailsView.findViewById(R.id.answer);

        final EditText edittxt_comment;
        ImageView userphoto_comment;
        final TextView user_answer;

        edittxt_comment = (EditText) dialogView1.findViewById(R.id.edittxt_comment);
        userphoto_comment = (ImageView) dialogView1.findViewById(R.id.userphoto_comment);
        user_answer = (TextView) dialogView1.findViewById(R.id.user_answer);
        txt_adapter_post = (TextView) dialogView1.findViewById(R.id.txt_adapter_post);
        progress_dialog_follower = (ProgressBar) dialogView1.findViewById(R.id.progress_dialog_follower);

        if (progress_dialog_follower.getVisibility() == VISIBLE) {
            dialogBuilder1.setCancelable(false);
        } else {
            dialogBuilder1.setCancelable(true);
        }

        dialog1 = dialogBuilder1.create();
        dialog1.show();
        // User Image loading
        GlideApp.with(context)
                .load(Pref_storage.getDetail(context, "picture_user_login"))
                .placeholder(R.drawable.ic_add_photo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(userphoto_comment);

        user_answer.setText(gettingtimelinealllist.get(adapterPosition).getAskQuestion());

        edittxt_comment.setHint("Add your answer");

        // Edittext comment listener
        edittxt_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edittxt_comment.getText().toString().trim().length() != 0) {
                    txt_adapter_post.setVisibility(View.VISIBLE);
                } else {
                    txt_adapter_post.setVisibility(View.GONE);
                }
            }
        });

        txt_adapter_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edittxt_comment.getText().toString().isEmpty()) {
                    edittxt_comment.setError("Enter your answer");
                } else {
                    progress_dialog_follower.setVisibility(VISIBLE);
                    progress_dialog_follower.setIndeterminate(true);
                    createAnswer(adapterPosition, edittxt_comment);
                    Utility.hideKeyboard(view);
                }
            }
        });
    }

    private void createAnswer(final int adapterPosition, EditText edittxt_comment) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            gpsTracker = new GPSTracker(context);
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

            Call<AnswerPojoClass> call = apiService.createAnswerView("create_answer", 0,
                    Integer.parseInt(gettingtimelinealllist.get(adapterPosition).getQuestId()),
                    0, 0, edittxt_comment.getText().toString(), latitude, longitude, userid);

            call.enqueue(new Callback<AnswerPojoClass>() {
                @Override
                public void onResponse(Call<AnswerPojoClass> call, Response<AnswerPojoClass> response) {

                    if (response.body() != null) {

                        int responseStatus = response.body().getResponseCode();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus == 1) {

                            //Toast.makeText(context, "Answer created successfully", Toast.LENGTH_SHORT).show();
                            //Success

                            dialog1.dismiss();

                            progress_dialog_follower.setVisibility(View.GONE);
                            progress_dialog_follower.setIndeterminate(false);

                            answer.setText(response.body().getData().get(0).getAskAnswer());

                            Intent intent = new Intent(context, ViewQuestionAnswer.class);
                            intent.putExtra("position", String.valueOf(adapterPosition));
                            intent.putExtra("quesId", gettingtimelinealllist.get(adapterPosition).getQuestId());
                            intent.putExtra("firstname", gettingtimelinealllist.get(adapterPosition).getFirstName() + " " + gettingtimelinealllist.get(adapterPosition).getLastName());
                            intent.putExtra("createdon", gettingtimelinealllist.get(adapterPosition).getCreatedOn());
                            intent.putExtra("profile", gettingtimelinealllist.get(adapterPosition).getPicture());
                            startActivity(intent);
                        }

                    } else {


                        progress_dialog_follower.setVisibility(View.GONE);
                        progress_dialog_follower.setIndeterminate(false);
                        dialog1.dismiss();


                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                    }


                }

                @Override
                public void onFailure(Call<AnswerPojoClass> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                    dialog1.dismiss();

                    progress_dialog_follower.setVisibility(View.GONE);
                    progress_dialog_follower.setIndeterminate(false);

                }
            });
        } catch (Exception e) {
            Log.e("FailureError", "FailureError" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void createEditPollComments(String hotelId, final int adapterPosition) {


        comments_review.setVisibility(VISIBLE);
        comments_review.setIndeterminate(true);


        Utility.hideKeyboard(view);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

            Call<GetPollCommentsOutput> call = apiService.get_create_edit_question_comments("create_edit_question_comments",

                    Integer.parseInt(hotelId),
                    0, userid,
                    editText_comment.getText().toString());

            call.enqueue(new Callback<GetPollCommentsOutput>() {

                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<GetPollCommentsOutput> call, Response<GetPollCommentsOutput> response) {

                    editText_comment.setText("");

                    if (response.code() == 500) {

                        comments_review.setVisibility(View.GONE);
                        comments_review.setIndeterminate(false);

                        Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();

                    } else {

                        comments_review.setVisibility(View.GONE);
                        comments_review.setIndeterminate(false);


                        if (response.body() != null) {

                            if (Objects.requireNonNull(response.body()).getResponseMessage().equals("success")) {

                                if (response.body().getData().size() > 0) {

                                    if (tv_view_all_comments_review.getVisibility() == View.GONE) {

                                        tv_view_all_comments_review.setVisibility(View.VISIBLE);
                                        //String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                        String comments = getString(R.string.view_one_comment);
                                        tv_view_all_comments_review.setText(comments);

                                    } else {

                                        String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                        tv_view_all_comments_review.setText(comments);
                                    }


                                    Intent intent = new Intent(context, ViewPollComments.class);

                                    intent.putExtra("comment_quesId", String.valueOf(gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getQuestId()));
                                    intent.putExtra("comment_question", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getAskQuestion());
                                    intent.putExtra("comment_picture", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPicture());
                                    intent.putExtra("comment_firstname", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getFirstName() + " " + gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getLastName());
                                    intent.putExtra("comment_createdon", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getCreatedOn());
                                    intent.putExtra("comment_pollid", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPollId());
                                    intent.putExtra("pollsize", String.valueOf(gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().size()));

                                    try {

                                        double userVotesTwo = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser();
                                        intent.putExtra("userVotes", String.valueOf(userVotesTwo));

                                        if (gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().size() == 2) {
                                            double totalVotesOne = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();
                                            double totalVotesTwo = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();

                                            intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                            intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                            intent.putExtra("comment_poll1", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).getPollList());
                                            intent.putExtra("comment_poll2", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(1).getPollList());

                                        } else if (gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().size() == 3) {

                                            double totalVotesOne = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();
                                            double totalVotesTwo = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();
                                            double totalVotesThree = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();

                                            intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                            intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                            intent.putExtra("totalVotesThree", String.valueOf(totalVotesThree));
                                            intent.putExtra("comment_poll1", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).getPollList());
                                            intent.putExtra("comment_poll2", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(1).getPollList());
                                            intent.putExtra("comment_poll3", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(2).getPollList());
                                        } else if (gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().size() == 4) {


                                            double totalVotesOne = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).getTotalPoll();
                                            double totalVotesTwo = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(1).getTotalPoll();
                                            double totalVotesThree = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(2).getTotalPoll();
                                            double totalVotesFour = gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(3).getTotalPoll();

                                            intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                            intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                            intent.putExtra("totalVotesThree", String.valueOf(totalVotesThree));
                                            intent.putExtra("totalVotesFour", String.valueOf(totalVotesFour));

                                            intent.putExtra("comment_poll1", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).getPollList());
                                            intent.putExtra("comment_poll2", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(1).getPollList());
                                            intent.putExtra("comment_poll3", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(2).getPollList());
                                            intent.putExtra("comment_poll4", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(3).getPollList());
                                        }

                                        intent.putExtra("comment_votecount", gettingtimelinealllist.get(adapterPosition).getQuestionInp().get(0).getPoll().get(0).getTotalPollUser() + " votes ");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                    startActivity(intent);


                                } else {

                                }

                            } else {

                            }
                        }

                    }
                }

                @Override
                public void onFailure(Call<GetPollCommentsOutput> call, Throwable t) {
                    //Error

                    comments_review.setVisibility(View.GONE);
                    comments_review.setIndeterminate(false);

                    Log.e("Balaji", "" + t.getMessage());
                }
            });

        } catch (Exception e) {

            Log.e("Balaji", "" + e.getMessage());
            e.printStackTrace();

        }

    }

    private void createEditReviewComments(String hotelId, String toString, String getReviewId, final int adapterPosition) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {
            String createuserid = Pref_storage.getDetail(context, "userId");

            Call<ReviewCommentsPojo> call = apiService.get_create_edit_hotel_review_comments("create_edit_hotel_review_comments", Integer.parseInt(hotelId),
                    0, Integer.parseInt(createuserid), toString, Integer.parseInt(getReviewId));
            call.enqueue(new Callback<ReviewCommentsPojo>() {
                @Override
                public void onResponse(Call<ReviewCommentsPojo> call, Response<ReviewCommentsPojo> response) {

                    review_comment.setText("");

                    if (response.code() == 500) {

                        comments_progressbar.setVisibility(View.GONE);
                        comments_progressbar.setIndeterminate(false);

                        Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();

                    } else {

                        comments_progressbar.setVisibility(View.GONE);
                        comments_progressbar.setIndeterminate(false);


                        if (response.body() != null) {

                            if (Objects.requireNonNull(response.body()).getResponseMessage().equals("success")) {

                                if (response.body().getData().size() > 0) {

                                    if (tv_view_all_comments_review.getVisibility() == View.GONE) {

                                        tv_view_all_comments_review.setVisibility(View.VISIBLE);
                                        //String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                        String comments = getString(R.string.view_one_comment);
                                        tv_view_all_comments_review.setText(comments);

                                    } else {

                                        String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                        tv_view_all_comments_review.setText(comments);
                                    }
                                    Intent intent = new Intent(context, Reviews_comments_all_activity.class);
                                    intent.putExtra("posttype", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getCategoryType());
                                    intent.putExtra("hotelid", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getHotelId());
                                    intent.putExtra("reviewid", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getRevratId());
                                    intent.putExtra("userid", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getUserId());
                                    intent.putExtra("hotelname", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getHotelName());
                                    intent.putExtra("overallrating", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getFoodExprience());

                                    if (gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getCategoryType().equalsIgnoreCase("2")) {
                                        intent.putExtra("totaltimedelivery", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getTotalTimedelivery());

                                    } else {
                                        intent.putExtra("totaltimedelivery", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getAmbiance());

                                    }
                                    intent.putExtra("taste_count", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getTaste());
                                    intent.putExtra("vfm_rating", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getValueMoney());
                                    if (gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getCategoryType().equalsIgnoreCase("2")) {
                                        intent.putExtra("package_rating", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).get_package());

                                    } else {
                                        intent.putExtra("package_rating", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getService());

                                    }
                                    intent.putExtra("hotelname", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getHotelName());
                                    intent.putExtra("reviewprofile", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getPicture());
                                    intent.putExtra("createdon", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getCreatedOn());
                                    intent.putExtra("firstname", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getFirstName());
                                    intent.putExtra("lastname", gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getLastName());
                                    if (gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getCategoryType().equalsIgnoreCase("2")) {
                                        intent.putExtra("title_ambience", "Packaging");
                                    } else {
                                        intent.putExtra("title_ambience", "Hotel Ambience");
                                    }
                                    if (gettingtimelinealllist.get(adapterPosition).getReviewInp().get(0).getBucket_list() != 0) {
                                        intent.putExtra("addbucketlist", "yes");
                                    } else {
                                        intent.putExtra("addbucketlist", "no");
                                    }
                                    startActivity(intent);


                                } else {

                                }

                            } else {

                            }
                        }

                    }

                }

                @Override
                public void onFailure(Call<ReviewCommentsPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                    comments_progressbar.setVisibility(View.GONE);
                    comments_progressbar.setIndeterminate(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void createEditEventsComments(String timelineId, String toString, final int adapterposition) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            String createuserid = Pref_storage.getDetail(context, "userId");

            Call<CreateEditEventsComments> call = apiService.create_edit_events_comments("create_edit_events_comments",
                    0, Integer.parseInt(timelineId), toString, Integer.parseInt(createuserid));
            call.enqueue(new Callback<CreateEditEventsComments>() {
                @Override
                public void onResponse(@NonNull Call<CreateEditEventsComments> call, @NonNull Response<CreateEditEventsComments> response) {

                    editText_comment.setText("");

                    if (response.code() == 500) {

                        comments_progressbar.setVisibility(View.GONE);
                        comments_progressbar.setIndeterminate(false);

                        Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();
                    } else {

                        comments_progressbar.setVisibility(View.GONE);
                        comments_progressbar.setIndeterminate(false);

                        if (response.body() != null) {

                            if (Objects.requireNonNull(response.body()).getResponseMessage().equals("success")) {

                                if (response.body().getData().size() > 0) {


                                    int Cmnt = Integer.parseInt(response.body().getData().get(0).getTotalComments());

                                    rl_comments_layout.setVisibility(VISIBLE);
                                    recently_commented_ll.setVisibility(VISIBLE);

                                    if (Cmnt == 1) {

                                        tv_view_all_comments.setText(R.string.view);
                                        tv_view_all_comments.setVisibility(VISIBLE);
                                        commenttest.setText(R.string.commenttt);
                                        comment.setVisibility(View.GONE);
                                        commenttest.setVisibility(VISIBLE);

                                    } else if (Cmnt > 1) {

                                        tv_view_all_comments.setVisibility(VISIBLE);
                                        commenttest.setVisibility(VISIBLE);
                                        comment.setVisibility(VISIBLE);
                                        tv_view_all_comments.setText(getString(R.string.view_all));
                                        comment.setText(String.valueOf(Cmnt));
                                        //comment.setText(getString(R.string.comments));
                                        comment.setVisibility(VISIBLE);

                                    }

                                    recently_commented.setText(response.body().getData().get(0).getEvtComments());
                                    String firstLanstName = response.body().getData().get(0).getFirstName() + response.body().getData().get(0).getLastName();
                                    username_latestviewed.setText(firstLanstName);


                                    /* Posted date functionality */
                                    String createdon = response.body().getData().get(0).getCreatedOn();

                                    Utility.setTimeStamp(createdon, created_on);

                                    if (response.body().getData().get(0).getPicture() != null) {

                                        GlideApp.with(context)
                                                .load(response.body().getData().get(0).getPicture())
                                                .centerCrop()
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .thumbnail(0.1f)
                                                .placeholder(R.drawable.ic_add_photo)
                                                .into(userphoto);

                                    }

                                   /* Intent intent = new Intent(context, Comments_activity.class);
                                    intent.putExtra("comment_timelinepicture", gettingtimelinealllist.get(adapterposition).getPicture());
                                    intent.putExtra("comment_posttpe", gettingtimelinealllist.get(adapterposition).getPostType());
                                    intent.putExtra("comment_timelineid", gettingtimelinealllist.get(adapterposition).getEventId());
                                    intent.putExtra("username", gettingtimelinealllist.get(adapterposition).getFirstName());
                                    intent.putExtra("comment_eventname", gettingtimelinealllist.get(adapterposition).getEventName());
                                    intent.putExtra("comment_created_on", gettingtimelinealllist.get(adapterposition).getCreatedOn());
                                    intent.putExtra("comment_image", gettingtimelinealllist.get(adapterposition).getEventImage());
                                    intent.putExtra("comment_profile", gettingtimelinealllist.get(adapterposition).getPicture());

                                    startActivity(intent);
*/
                                } else {

                                }

                            } else {

                            }
                        }
                    }

                }

                @Override
                public void onFailure(@NonNull Call<CreateEditEventsComments> call, @NonNull Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createEditTimelineComments(String timelineId, String toString, final int adapterPosition) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {


            String createuserid = Pref_storage.getDetail(context, "userId");

            Call<create_edit_timeline_comments_Response> call = apiService.create_edit_timeline_commentss("create_edit_timeline_comments",
                    0, Integer.parseInt(timelineId), toString, Integer.parseInt(createuserid));

            call.enqueue(new Callback<create_edit_timeline_comments_Response>() {
                @Override
                public void onResponse(@NonNull Call<create_edit_timeline_comments_Response> call, @NonNull Response<create_edit_timeline_comments_Response> response) {


                    editText_comment.setText("");

                    if (response.code() == 500) {

                        comments_progressbar.setVisibility(View.GONE);
                        comments_progressbar.setIndeterminate(false);

                        Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();

                    } else {

                        comments_progressbar.setVisibility(View.GONE);
                        comments_progressbar.setIndeterminate(false);

                        if (response.body() != null) {

                            if (Objects.requireNonNull(response.body()).getResponseMessage().equals("success")) {

                                if (response.body().getData().size() > 0) {


                                    int Cmnt = Integer.parseInt(response.body().getData().get(0).getTotalComments());

                                    rl_comments_layout.setVisibility(VISIBLE);
                                    recently_commented_ll.setVisibility(VISIBLE);

                                    if (Cmnt == 1) {

                                        tv_view_all_comments.setText(R.string.view);
                                        tv_view_all_comments.setVisibility(VISIBLE);
                                        commenttest.setText(R.string.commenttt);
                                        comment.setVisibility(View.GONE);
                                        commenttest.setVisibility(VISIBLE);

                                    } else if (Cmnt >= 1) {

                                        tv_view_all_comments.setVisibility(VISIBLE);
                                        commenttest.setVisibility(VISIBLE);
                                        comment.setVisibility(VISIBLE);
                                        comment.setText(String.valueOf(Cmnt));
                                        comment.setVisibility(VISIBLE);

                                    }

                                    recently_commented.setText(response.body().getData().get(0).getTlComments());

                                    String firstLanstName = response.body().getData().get(0).getFirstName() + response.body().getData().get(0).getLastName();
                                    username_latestviewed.setText(firstLanstName);


                                    /* Posted date functionality */
                                    String createdon = response.body().getData().get(0).getCreatedOn();
                                    Utility.setTimeStamp(createdon, created_on);

                                    if (response.body().getData().get(0).getPicture() != null) {

                                        GlideApp.with(context)
                                                .load(response.body().getData().get(0).getPicture())
                                                .centerCrop()
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .thumbnail(0.1f)
                                                .placeholder(R.drawable.ic_add_photo)
                                                .into(userphoto);

                                    }


                                   /* Intent intent = new Intent(context, Comments_activity.class);
                                    intent.putExtra("comment_timelinepicture", gettingtimelinealllist.get(adapterPosition).getPicture());
                                    intent.putExtra("comment_posttpe", gettingtimelinealllist.get(adapterPosition).getPostType());
                                    intent.putExtra("comment_timelineid", gettingtimelinealllist.get(adapterPosition).getEventId());
                                    intent.putExtra("username", gettingtimelinealllist.get(adapterPosition).getFirstName());
                                    intent.putExtra("comment_eventname", gettingtimelinealllist.get(adapterPosition).getEventName());
                                    intent.putExtra("comment_created_on", gettingtimelinealllist.get(adapterPosition).getCreatedOn());
                                    intent.putExtra("comment_image", gettingtimelinealllist.get(adapterPosition).getEventImage());
                                    intent.putExtra("comment_profile", gettingtimelinealllist.get(adapterPosition).getPicture());
                                    try {
                                        intent.putExtra("comment_image", gettingtimelinealllist.get(adapterPosition).getImage().get(0).getImg());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    startActivity(intent);*/

                                } else {

                                }

                            } else {

                            }
                        }

                    }


                }

                @Override
                public void onFailure(@NonNull Call<create_edit_timeline_comments_Response> call, @NonNull Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    comments_progressbar.setVisibility(View.GONE);
                    comments_progressbar.setIndeterminate(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * New data avalable data checking pagination API for timeline feeds
     **/
    private class gettimelineall_pagination extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {


            try {
                // Thread.sleep(3000);
                if (Utility.isConnected(context)) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    String userId = Pref_storage.getDetail(context, "userId");
                    int page_count = Integer.parseInt(Pref_storage.getDetail(context, "Page_count"));

                    Call<Newtimeline_output_response> call = apiService.gettimelineall("get_timeline_all", Integer.parseInt(userId), page_count);

                    call.enqueue(new Callback<Newtimeline_output_response>() {
                        @Override
                        public void onResponse(Call<Newtimeline_output_response> call, Response<Newtimeline_output_response> response) {

                            if (response.body().getResponseMessage().equalsIgnoreCase("success")) {


                                //llNoPost.setVisibility(View.GONE);
                                finalTimelineNotificationAdapter.notifyItemChanged(0, false);
                                progressTimeline.setVisibility(View.GONE);
                                pageLoader.setVisibility(View.GONE);

                                loaderInt = 0;

                                // loader_gettingtimelinealllist.clear();


                                if (Pref_storage.getDetail(context, "Page_count").isEmpty() || Pref_storage.getDetail(context, "Page_count") == null) {

                                } else {

                                    int page_incre = Integer.parseInt(Pref_storage.getDetail(context, "Page_count"));

                                    Log.e(TAG, "onResponsePagination: page_incre-->" + page_incre);


                                    int page_incre_shared = (page_incre) + 1;

                                    Pref_storage.setDetail(context, "Page_count", String.valueOf(page_incre_shared));

                                    Log.e(TAG, "onResponsePagination: page_count_incre-->" + Integer.parseInt(Pref_storage.getDetail(context, "Page_count")));

                                    pagecount = Integer.parseInt(Pref_storage.getDetail(context, "Page_count"));
                                }


                                for (int j = 0; j < response.body().getData().size(); j++) {


                                    String timelineId = response.body().getData().get(j).getTimelineId();
                                    String timelineHotel = response.body().getData().get(j).getTimelineHotel();
                                    String timelineDescription = response.body().getData().get(j).getTimelineDescription();
                                    String address = response.body().getData().get(j).getAddress();
                                    String createdBy = response.body().getData().get(j).getCreatedBy();
                                    String createdOn = response.body().getData().get(j).getCreatedOn();
                                    int totalLikes = response.body().getData().get(j).getTotalLikes();
                                    String totalComments = response.body().getData().get(j).getTotalComments();
                                    String userId = response.body().getData().get(j).getUserId();
                                    String visibilityType = response.body().getData().get(j).getVisibilityType();
                                    String oauthProvider = response.body().getData().get(j).getOauthProvider();
                                    String oauthUid = response.body().getData().get(j).getOauthUid();
                                    String firstName1 = response.body().getData().get(j).getFirstName();
                                    String lastName1 = response.body().getData().get(j).getLastName();
                                    String email1 = response.body().getData().get(j).getEmail();
                                    String imei1 = response.body().getData().get(j).getImei();
                                    String contNo = response.body().getData().get(j).getContNo();
                                    String gender = response.body().getData().get(j).getGender();
                                    String dob = response.body().getData().get(j).getDob();
                                    picture = response.body().getData().get(j).getPicture();
                                    String latitude = response.body().getData().get(j).getLatitude();
                                    String longitude = response.body().getData().get(j).getLongitude();
                                    String totalPosts = response.body().getData().get(j).getTotalPosts();
                                    int self_liked = response.body().getData().get(j).getTotalLikes();
                                    String totalFollowers = response.body().getData().get(j).getTotalFollowers();
                                    int isfollowing = Integer.parseInt(response.body().getData().get(j).getFollowingId());
                                    String bio = response.body().getData().get(j).getBioDescription();
                                    String totalFollowings = response.body().getData().get(j).getTotalFollowings();
                                    String postType = response.body().getData().get(j).getPostType();
                                    String likesTlId = response.body().getData().get(j).getLikesTlId();
                                    String tlLikes = response.body().getData().get(j).getTlLikes();
                                    String eventId = response.body().getData().get(j).getEventId();
                                    String eventname = response.body().getData().get(j).getEventName();
                                    String eventimage = response.body().getData().get(j).getEventImage();
                                    String eventdescr = response.body().getData().get(j).getEventDescription();
                                    String startdate = response.body().getData().get(j).getStartDate();
                                    String enddate = response.body().getData().get(j).getEndDate();
                                    String locationn = response.body().getData().get(j).getLocation();
                                    String followingId = response.body().getData().get(j).getFollowingId();
                                    String followuserId = response.body().getData().get(j).getFollowuserId();
                                    String followUserfname = response.body().getData().get(j).getFollowuserFirstname();
                                    String followUserLastfname = response.body().getData().get(j).getFollowuserLastname();
                                    String followUserpicture = response.body().getData().get(j).getFollowuserPicture();
                                    String hotel = response.body().getData().get(j).getHotel();
                                    String hotelId = response.body().getData().get(j).getHotelId();
                                    String hotelPhoto = response.body().getData().get(j).getHotelPhoto();
                                    String googleId = response.body().getData().get(j).getGoogleId();
                                    String placeId = response.body().getData().get(j).getPlaceId();
                                    String openTimes = response.body().getData().get(j).getOpenTimes();
                                    String phone = response.body().getData().get(j).getPhone();
                                    String reviewId = response.body().getData().get(j).getReviewId();
                                    String review = response.body().getData().get(j).getReview();
                                    String reviewComments = response.body().getData().get(j).getReviewComments();
                                    String questId = response.body().getData().get(j).getQuestId();
                                    String askQuest = response.body().getData().get(j).getAskQuestion();
                                    String quesType = response.body().getData().get(j).getQuesType();
                                    String ansId = response.body().getData().get(j).getAnsId();
                                    String askAns = response.body().getData().get(j).getAskAnswer();
                                    String gngEvtId = response.body().getData().get(j).getGngEvtId();
                                    String likeEvtId = response.body().getData().get(j).getLikeEvtId();
                                    String follUserid = response.body().getData().get(j).getFollUserid();
                                    String likeRevId = response.body().getData().get(j).getLikeRevId();
                                    String cmmtRevId = response.body().getData().get(j).getCmmtRevId();
                                    String ansQuesId = response.body().getData().get(j).getAnsQuesId();
                                    String upAnsId = response.body().getData().get(j).getUpAnsId();
                                    int timelineLastCmtCount = response.body().getData().get(j).getTimelineLastCmtCount();
                                    int whomcount = response.body().getData().get(j).getWhomCount();
                                    //  int multiCount = response.body().getData().get(j).getMultiCount();


                                    List<TaggedPeoplePojo> whom = response.body().getData().get(j).getWhom();
                                    List<Image> imageList = response.body().getData().get(j).getImage();
                                    List<ReviewInputAllPojo> reviewTimelineList = response.body().getData().get(j).getReviewInp();
                                    List<QuestionAnswerPojo> questionAnswerPojoList = response.body().getData().get(j).getQuestionInp();
                                    List<TimelineLastCmt> timelineLastCmtList = response.body().getData().get(j).getTimelineLastCmt();
                                    List<MultiCount> multiCountList = response.body().getData().get(j).getMulti();


                                    int imagecount = response.body().getData().get(j).getImageCount();

                                    if (imagecount != 0) {

                                        for (int n = 0; n < response.body().getData().get(j).getImage().size(); n++) {

                                            timeLineImagesList.add(response.body().getData().get(j).getImage().get(n).getImg());
                                        }

                                    }

                                    String redirect_url = response.body().getData().get(j).getRedirect_url();
                                    newGettingTimelineAllPojo = new New_Getting_timeline_all_pojo(timelineId, redirect_url, timelineHotel, timelineDescription, address, createdBy, createdOn, totalLikes, totalComments, latitude,
                                            longitude, visibilityType, postType, userId, oauthProvider, oauthUid, firstName1, lastName1, email1, imei1, contNo, gender, dob, bio, totalFollowers, totalFollowings, totalPosts, picture, likesTlId, tlLikes
                                            , eventId, eventname, eventdescr, eventimage, startdate, enddate, locationn, followingId, followuserId, followUserfname, followUserLastfname, followUserpicture, hotelId, hotel
                                            , hotelPhoto, googleId, placeId, openTimes, phone, reviewId, review, reviewComments, questId, askQuest, quesType, ansId, askAns, gngEvtId, likeEvtId
                                            , follUserid, likeRevId, cmmtRevId, ansQuesId, upAnsId, timelineLastCmtCount, whom, whomcount, imageList, imagecount, reviewTimelineList, questionAnswerPojoList, timelineLastCmtList,
                                            multiCountList
                                            /* ,multiCount*/);


                                    gettingtimelinealllist.add(newGettingTimelineAllPojo);


                                    Log.e(TAG, "timeline: 1sttime_size" + gettingtimelinealllist.size());


                                }


                                LinkedHashSet<New_Getting_timeline_all_pojo> mainSet = new LinkedHashSet<>(gettingtimelinealllist);
                                gettingtimelinealllist.clear();
                                gettingtimelinealllist.add(new New_Getting_timeline_all_pojo(true));
                                gettingtimelinealllist.addAll(mainSet);


                                rv_newsfeed.setNestedScrollingEnabled(false);

                                finalTimelineNotificationAdapter.notifyDataSetChanged();


                                Log.e(TAG, "timeline: Pagination_Size" + gettingtimelinealllist.size());


                            } else if (response.body().getResponseMessage().equals("nodata")) {

                                //llNoPost.setVisibility(View.VISIBLE);
                                finalTimelineNotificationAdapter.notifyItemChanged(0, true);

                            }
                        }

                        @Override
                        public void onFailure(Call<Newtimeline_output_response> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
//                    //llNoPost.setVisibility(View.VISIBLE);
                            finalTimelineNotificationAdapter.notifyItemChanged(0, true);
                            pageLoader.setVisibility(View.GONE);

                            Toast.makeText(context, "You've seen all data.. No new data found.", Toast.LENGTH_SHORT).show();


                        }
                    });

                } else {

                    Toast.makeText(context, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();

                }


                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }


            } catch (Exception e) {
                Thread.interrupted();
            }

            return "Executed";
        }


        @Override
        protected void onPostExecute(String result) {


        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }


    /**
     * Calling stories all API
     **/

    private void get_stories_all() {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String createuserid = Pref_storage.getDetail(context, "userId");

            Call<Get_stories_pojo_output> call = apiService.get_stories_all("get_stories_whole_users", Integer.parseInt(createuserid));

            call.enqueue(new Callback<Get_stories_pojo_output>() {
                @Override
                public void onResponse(Call<Get_stories_pojo_output> call, Response<Get_stories_pojo_output> response) {

                    if (response.body() != null) {

                        if (response.body().getResponseCode() == 1) {

                            finalTimelineNotificationAdapter.updateStoriesPojoList(response.body().getData());

                        }

                    }


                }

                @Override
                public void onFailure(Call<Get_stories_pojo_output> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });


        } else {

        }


    }

    /**
     * Calling stories self API
     **/
    /*private void get_stories_self() {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String createuserid = Pref_storage.getDetail(context, "userId");
            Call<Get_stories_pojo_output> call = apiService.get_stories_all("get_stories_self", Integer.parseInt(createuserid));
            call.enqueue(new Callback<Get_stories_pojo_output>() {
                @Override
                public void onResponse(Call<Get_stories_pojo_output> call, Response<Get_stories_pojo_output> response) {

                    if (response.body() != null) {

                        if (response.body().getResponseCode() == 1) {

                            Log.e("story-->", "size" + response.body().getData().size());


                            get_all_stories_pojoList = response.body().getData();


                            storiesUserAdapter = new StoriesUserAdapter(context, get_all_stories_pojoList);
                            llm = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true);
                            user_stories.setLayoutManager(llm);
                            user_stories.setAdapter(storiesUserAdapter);
                            user_stories.setItemAnimator(new DefaultItemAnimator());
                            storiesUserAdapter.notifyDataSetChanged();
                            rv_stories.invalidate();
                            rv_stories.setNestedScrollingEnabled(false);
                            rv_stories.setHasFixedSize(true);
                            rv_stories.setItemViewCacheSize(20);

                        }

                    }


                }

                @Override
                public void onFailure(Call<Get_stories_pojo_output> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });


        } else {

        }


    }*/

    /**
     * Calling timeline size checking API
     **/
    private void gettimelineall_size() {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String userId = Pref_storage.getDetail(context, "userId");

            Call<Newtimeline_output_response> call = apiService.gettimelineall("get_timeline_all", Integer.parseInt(userId));


            call.enqueue(new Callback<Newtimeline_output_response>() {
                @Override
                public void onResponse(@NonNull Call<Newtimeline_output_response> call, @NonNull Response<Newtimeline_output_response> response) {


                    if (response.body() != null && response.body().getResponseMessage().equals("success")) {


                        if (Pref_storage.getDetail(context, "timelinesize") == null) {

                            Pref_storage.setDetail(context, "timelinesize", String.valueOf(response.body().getData().size()));

                        } else {

                            if (response.body().getData().size() > Integer.parseInt(Pref_storage.getDetail(context, "timelinesize"))) {

                                btnSizeCheck.setVisibility(View.VISIBLE);

                            } else {

                            }
                        }


                    }


                }

                @Override
                public void onFailure(@NonNull Call<Newtimeline_output_response> call, @NonNull Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                    //llNoPost.setVisibility(View.VISIBLE);
                    finalTimelineNotificationAdapter.notifyItemChanged(0, true);

                    progressTimeline.setIndeterminate(false);


                }
            });
        } else {

            Toast.makeText(context, "No internet connection.", Toast.LENGTH_SHORT).show();

        }

    }

    /**
     * Request code for multiple permission
     **/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == requestCamera) {

                photo1 = (Bitmap) data.getExtras().get("data");
//                userPhoto.setImageBitmap(photo1);
                Uri tempUri = getImageUri(context, photo1);

                Pref_storage.setDetail(context, "UserImage", getImageUrlWithAuthority1(context, tempUri));

            } else if (requestCode == selectFile) {

                try {
                    onSelectFromGalleryResult(data);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Getting Image URL authority
     **/
    public String getImageUrlWithAuthority1(Context context, Uri uri) {
        InputStream is = null;
        if (uri.getAuthority() != null) {
            try {
                is = context.getContentResolver().openInputStream(uri);
                Bitmap bmp = BitmapFactory.decodeStream(is);
                return writeToTempImageAndGetPathUri(context, bmp).toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * Getting Image URL path
     **/
    public Uri writeToTempImageAndGetPathUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    /**
     * Getting Image URI
     **/

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    /**
     * Getting Image from gallery
     **/
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        selectedImage = data.getData();

        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor c = context.getContentResolver().query(selectedImage, filePathColumn, null, null, null);

        c.moveToFirst();

        int columnIndex = c.getColumnIndex(filePathColumn[0]);
        String picturePath = c.getString(columnIndex);
        c.close();

        Bitmap photo1 = (BitmapFactory.decodeFile(picturePath));
        Pref_storage.setDetail(context, "UserImage", picturePath);
        startActivity(new Intent(context, Fullimage_activity.class));


    }


    //load your story api
    /*private class yourstories extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {


            try {
                // Thread.sleep(3000);
                if (Utility.isConnected(context)) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    String createuserid = Pref_storage.getDetail(context, "userId");
                    Call<Get_stories_pojo_output> call = apiService.get_stories_all("get_stories_self",
                            Integer.parseInt(createuserid));

                    call.enqueue(new Callback<Get_stories_pojo_output>() {
                        @Override
                        public void onResponse(Call<Get_stories_pojo_output> call, Response<Get_stories_pojo_output> response) {

                            if (response.body() != null) {

                                if (response.body().getResponseCode() == 1) {

                                    Log.e("story-->", "size" + response.body().getData().size());


                                    get_all_stories_pojoList = response.body().getData();


                                    storiesUserAdapter = new StoriesUserAdapter(context, get_all_stories_pojoList);
                                    llm = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true);
                                    user_stories.setLayoutManager(llm);
                                    user_stories.setAdapter(storiesUserAdapter);
                                    user_stories.setItemAnimator(new DefaultItemAnimator());
                                    storiesUserAdapter.notifyDataSetChanged();
                                    rv_stories.invalidate();
                                    rv_stories.setNestedScrollingEnabled(false);
                                    rv_stories.setHasFixedSize(true);
                                    rv_stories.setItemViewCacheSize(20);

                                }

                            }


                        }

                        @Override
                        public void onFailure(Call<Get_stories_pojo_output> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });


                } else {

                }

                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }


            } catch (Exception e) {
                Thread.interrupted();
            }

            return "Executed";
        }


        @Override
        protected void onPostExecute(String result) {
            //  storiesUserAdapter.notifyDataSetChanged();


        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }*/

    //load all story api
    private class allstories extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {


            try {
                if (Utility.isConnected(context)) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    String createuserid = Pref_storage.getDetail(context, "userId");

                    Call<Get_stories_pojo_output> call = apiService.get_stories_all("get_stories_whole_users", Integer.parseInt(createuserid));

                    call.enqueue(new Callback<Get_stories_pojo_output>() {
                        @Override
                        public void onResponse(Call<Get_stories_pojo_output> call, Response<Get_stories_pojo_output> response) {

                            if (response.body() != null) {

                                if (response.body().getResponseCode() == 1) {


                                    Log.e("story-->", "size" + response.body().getData().size());


                                    finalTimelineNotificationAdapter.updateStoriesPojoList(response.body().getData());


//                            Collections.reverse(stories_image_list);

                                }

                            }


                        }

                        @Override
                        public void onFailure(Call<Get_stories_pojo_output> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });


                } else {

                }


                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }


            } catch (Exception e) {
                Thread.interrupted();
            }

            return "Executed";
        }


        @Override
        protected void onPostExecute(String result) {
            //storiesAdapter.notifyDataSetChanged();


        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }


    private void chagePageView(int adapterPosition) {

        Intent intent1 = new Intent(context, ChatActivity.class);
        if (gettingtimelinealllist.get(adapterPosition).getRedirect_url() != null) {
            intent1.putExtra("redirect_url", gettingtimelinealllist.get(adapterPosition).getRedirect_url());
        }
        Pref_storage.setDetail(getActivity(), "shareView", Constants.TimeLineView);
        context.startActivity(intent1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}