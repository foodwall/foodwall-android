package com.kaspontech.foodwall.followingPackage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.adapters.Follow_following.Follower_adapter;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_follower_output_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.MyDividerItemDecoration;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Followerlist_user_activity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Followerlist_user_activity";

    // Action Bar

    String userid;
    Toolbar actionBar;
    ImageButton back;
    TextView toolbar_title;
    TextView tv_no_followers;
    RelativeLayout rl_follower;
    RelativeLayout rl_followerlay;
    RecyclerView rv_followers;

    Follower_adapter followerAdapter;

    ProgressBar progress_dialog_follower;

    List<Get_followerlist_pojo> followerlist = new ArrayList<>();

    //    public static  String txtUsernamelike,txtLastusernamelike,fulltxt_usernamelike,ago_like,likeUserImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followerslist);

        //Toolbar

        actionBar = (Toolbar) findViewById(R.id.actionbar_follower);
        //ImageButton
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        //TextView
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        tv_no_followers = (TextView) findViewById(R.id.tv_no_followers);
        toolbar_title.setVisibility(View.VISIBLE);
        toolbar_title.setText(R.string.followers);
        //RelativeLayout

        rl_follower = (RelativeLayout) findViewById(R.id.rl_follower);
        rl_followerlay = (RelativeLayout) findViewById(R.id.rl_followerlay);

        //ProgressBar
        progress_dialog_follower = (ProgressBar) findViewById(R.id.progress_dialog_follower);


        //RecyclerView

        rv_followers = (RecyclerView) findViewById(R.id.rv_followers);

        followerAdapter = new Follower_adapter(this, followerlist,1);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_followers.setLayoutManager(mLayoutManager);
        rv_followers.setItemAnimator(new DefaultItemAnimator());
        rv_followers.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 20));
        rv_followers.setAdapter(followerAdapter);


        Intent intent = getIntent();

        if (intent != null) {

            userid = intent.getStringExtra("created_by");

        }


        //API calling
        try {

            rl_follower.setVisibility(View.GONE);
            progress_dialog_follower.setVisibility(View.VISIBLE);

            get_follower();
//            get_following();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:

                finish();
                break;
        }


    }


    /*Getting followers list*/
    private void get_follower() {


        followerlist.clear();

        if (isConnected(Followerlist_user_activity.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {


                Call<Get_follower_output_pojo> call = apiService.get_follower("get_follower", Integer.parseInt(userid));
                call.enqueue(new Callback<Get_follower_output_pojo>() {
                    @Override
                    public void onResponse(Call<Get_follower_output_pojo> call, Response<Get_follower_output_pojo> response) {

                        if (response.body().getResponseMessage().equals("success")) {

                            followerlist.addAll(response.body().getData());

                            followerAdapter.notifyDataSetChanged();

                            rl_follower.setVisibility(View.VISIBLE);
                            progress_dialog_follower.setVisibility(View.GONE);

                        }

                    }

                    @Override
                    public void onFailure(Call<Get_follower_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        if(followerlist.isEmpty()){
                            tv_no_followers.setVisibility(View.VISIBLE);
                        }else{

                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Snackbar snackbar = Snackbar.make(rl_followerlay, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

}