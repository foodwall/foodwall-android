package com.kaspontech.foodwall.followingPackage;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.Follow_following.following_adapter_self;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.MyDividerItemDecoration;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Following_listActivity extends AppCompatActivity implements View.OnClickListener,
        following_adapter_self.UpdateUnfollowListener{
    private static final String TAG = "Following_listActivity";

    // Action Bar
    Toolbar actionBar;
    ImageButton back;
    TextView toolbar_title,tv_no_followers;
    RelativeLayout rl_following, rl_following_list;
    RecyclerView rv_following;
    LinearLayoutManager llm;
    Get_following_profile_pojo getFollowingProfilePojo;
    ProgressBar progressBar;
    String userId;

    following_adapter_self followingAdapterSelf;
    ArrayList<Get_following_profile_pojo> followinglist = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following_list);
        actionBar = (Toolbar) findViewById(R.id.actionbar_following);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        tv_no_followers = (TextView) findViewById(R.id.tv_no_followers);
        toolbar_title.setText(R.string.following);
        toolbar_title.setVisibility(View.VISIBLE);
        rl_following = (RelativeLayout) findViewById(R.id.rl_following);
        rl_following_list = (RelativeLayout) findViewById(R.id.rl_following_list);
        rv_following = (RecyclerView) findViewById(R.id.rv_following);
        progressBar = (ProgressBar) findViewById(R.id.progress_dialog_following);


        followingAdapterSelf = new following_adapter_self(this, followinglist,this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_following.setLayoutManager(mLayoutManager);
        rv_following.setItemAnimator(new DefaultItemAnimator());
        rv_following.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 20));
        rv_following.setAdapter(followingAdapterSelf);

        try {
            rl_following.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            get_following();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void get_following() {

        if (isConnected(Following_listActivity.this)) {

            followinglist.clear();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                userId = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_following_output_Pojo> call = apiService.get_following("get_following", Integer.parseInt(userId)); /*onlineCreateUserModel.getTimelineDescription(),onlineCreateUserModel.getCreatedBy(),onlineCreateUserModel.getImage()*/
                call.enqueue(new Callback<Get_following_output_Pojo>() {
                    @Override
                    public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {

                        if (response.body().getResponseMessage().equals("success")) {

                            tv_no_followers.setVisibility(View.GONE);
                            followinglist.addAll(response.body().getData());
                            followingAdapterSelf.notifyDataSetChanged();
                            rl_following.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);


                        }else{

                            tv_no_followers.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);


                        }

                    }

                    @Override
                    public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        tv_no_followers.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        rv_following.setVisibility(View.GONE);

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Snackbar snackbar = Snackbar.make(rl_following_list, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }



    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:

                finish();

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    public void updateFollowingList() {

        get_following();

    }
}
