package com.kaspontech.foodwall.followingPackage;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.Follow_following.Following_adapter;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.MyDividerItemDecoration;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Following_list_user_Activity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "Following_list_user_Activity";

    // Action Bar
    Toolbar actionBar;
    //Back pressed
    ImageButton back;
    //Title text
    TextView toolbar_title;

    TextView tv_no_followers;
    //RelativeLayout
    RelativeLayout rl_following, rl_following_list;
    //RecyclerView
    RecyclerView rv_following;

    LinearLayoutManager llm;
    //Following pojo
    Get_following_profile_pojo getFollowingProfilePojo;
    //String
    String userId;
    //Follow Adapter
    Following_adapter followingAdapter;
    //Follow list
    ArrayList<Get_following_profile_pojo> followinglist = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following_list);
        actionBar = (Toolbar) findViewById(R.id.actionbar_following);
        back = (ImageButton)actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        toolbar_title=(TextView)findViewById(R.id.toolbar_title);

        tv_no_followers=(TextView)findViewById(R.id.tv_no_followers);

        toolbar_title.setText(R.string.following);
        toolbar_title.setVisibility(View.VISIBLE);
        rl_following= (RelativeLayout) findViewById(R.id.rl_following);
        rl_following_list = (RelativeLayout) findViewById(R.id.rl_following_list);
        rv_following=(RecyclerView)findViewById(R.id.rv_following);


        followingAdapter = new Following_adapter(this, followinglist);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_following.setLayoutManager(mLayoutManager);
        rv_following.setItemAnimator(new DefaultItemAnimator());
        rv_following.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 20));
        rv_following.setAdapter(followingAdapter);

        Intent intent = getIntent();

        if(intent != null){

            userId = intent.getStringExtra("created_by");

        }

        //API calling
        try {
            get_following();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void get_following() {

        followinglist.clear();
        if (isConnected(Following_list_user_Activity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {



                Call<Get_following_output_Pojo> call = apiService.get_following("get_following", Integer.parseInt(userId)); /*onlineCreateUserModel.getTimelineDescription(),onlineCreateUserModel.getCreatedBy(),onlineCreateUserModel.getImage()*/
                call.enqueue(new Callback<Get_following_output_Pojo>() {
                    @Override
                    public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {

                        if (response.body().getResponseCode() == 1) {

                            followinglist.addAll(response.body().getData());
                            followingAdapter.notifyDataSetChanged();

                        }

                    }

                    @Override
                    public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());

                        if(followinglist.size()==0){
                            tv_no_followers.setVisibility(View.VISIBLE);
                        }else{

                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {


            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();

         /*   Snackbar snackbar = Snackbar.make(rl_following_list, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();*/
        }


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:

                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


}
