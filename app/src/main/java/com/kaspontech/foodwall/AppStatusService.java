package com.kaspontech.foodwall;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_online_status_pojo;
import com.kaspontech.foodwall.utills.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppStatusService extends Service implements LocationListener {
    private static final String TODO = "AppStaus";

     Context context;
    //Online checking
    TelephonyManager telephonyManager;
    LocationManager locationManager;
    Location location;

    double latitude_online, longitude_online;

    String  imei, createdby,mprovider;
    static final Integer PHONESTATS = 0x1;

    public AppStatusService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

//        askForPermission(Manifest.permission.READ_PHONE_STATE, PHONESTATS);

        // Getting latitude and longitude
        getLatLong();

        // Getting Imei for API calling

        getImeiNumber();


        String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");
        String imei = Pref_storage.getDetail(getApplicationContext(), "IMEI");

        //Back ground service for app offline API calling

        createChatLoginSessiontimeOffline(createdby,imei);

//        Toast.makeText(this, "closed", Toast.LENGTH_SHORT).show();
    }


    //Back ground service for app offline API calling

    private void createChatLoginSessiontimeOffline(String createdby, String imei) {

        if (Utility.isConnected(getApplicationContext())) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                if (imei==null){
                    imei="";
                }


                Call<Get_chat_online_status_pojo> call = apiService.create_chat_login_sessiontime("create_chat_login_sessiontime",
                        0,
                        Integer.parseInt(createdby),
                        imei,
                        latitude_online,
                        longitude_online);
                call.enqueue(new Callback<Get_chat_online_status_pojo>() {
                    @Override
                    public void onResponse(Call<Get_chat_online_status_pojo> call, Response<Get_chat_online_status_pojo> response) {

                        if (response.body().getResponseCode() == 1) {

                            String loggedout = response.body().getData();

                            Log.e("loggedout", "loggedout:" + loggedout);

                        }

                    }

                    @Override
                    public void onFailure(Call<Get_chat_online_status_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
           // Toast.makeText(getApplicationContext(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();

        }


    }



    // Getting latitude and longitude

    private void getLatLong() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        mprovider = locationManager.getBestProvider(criteria, false);

        if (mprovider != null && !mprovider.equals("")) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = locationManager.getLastKnownLocation(mprovider);
            locationManager.requestLocationUpdates(mprovider, 15000, 1, this);

            if (location != null)
                onLocationChanged(location);
           // else
              //  Toast.makeText(getBaseContext(), "No Location Provider Found.", Toast.LENGTH_SHORT).show();
        }

    }


    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(AppStatusService.this, permission) != PackageManager.PERMISSION_GRANTED) {

            // Should show an explanation
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission)) {

                ActivityCompat.requestPermissions((Activity) context, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions((Activity) context, new String[]{permission}, requestCode);
            }
        } else {
            imei = getImeiNumber();

            Log.e("AppStatus", "onCreate:imei " + imei);

        }
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    imei = getImeiNumber();


                } else {

                    Toast.makeText(AppStatusService.this, "You have Denied the Permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    // Getting Imei for API calling

    private String getImeiNumber() {
        final TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //getDeviceId() is Deprecated so for android O we can use getImei() method
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return TODO;
            }
            return telephonyManager.getImei();
        }
        else {
            return telephonyManager.getDeviceId();
        }


    }

    @Override
    public void onLocationChanged(Location location) {
        latitude_online = location.getLatitude();
        longitude_online = location.getLongitude();

        Log.e("AppStatus", "getLocation: lati"+latitude_online );
        Log.e("AppStatus", "getLocation: longi"+longitude_online );
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
