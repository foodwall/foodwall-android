package com.kaspontech.foodwall.REST_API;


import com.google.gson.JsonArray;
import com.kaspontech.foodwall.adapters.TimeLine.Timeline_image_only_output_pojo;
import com.kaspontech.foodwall.bucketlistpackage.GetMyBucketListPojo;
import com.kaspontech.foodwall.bucketlistpackage.GetmybucketOutputpojo;
import com.kaspontech.foodwall.bucketlistpackage.GroupBucketList_Datum;
import com.kaspontech.foodwall.bucketlistpackage.GroupBucketList_Response;
import com.kaspontech.foodwall.bucketlistpackage.SearchBucketOutputPojo;
import com.kaspontech.foodwall.foodFeedsPackage.TotalCountModule_Response;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Create_story_output_pojo;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.storiesPackage.Stories_pojo.Get_stories_pojo_output;
import com.kaspontech.foodwall.modelclasses.AnswerPojoClass;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.BucketIndividualuser_Response;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.Get_grp_bucket_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ChatAddMemberOutput;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ChatAddmemberOutputPojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ChatSearch_Response;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_history_single_output;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ClearChatResponse_Pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_history_output;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_online_status_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_typing_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_group_frds_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_group_history_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_group_member_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.NewChatPojo.NewChatOuputPojo;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchList.GetCommonSearchTypePojo;
import com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView.GetCommonSearchPojo;
import com.kaspontech.foodwall.modelclasses.EventShare.GetEventShareOutput;
import com.kaspontech.foodwall.modelclasses.EventShare.GetEventsCommentsAll_Response;
import com.kaspontech.foodwall.modelclasses.EventsPojo.CreateEditEventsComments;
import com.kaspontech.foodwall.modelclasses.EventsPojo.CreateEventOutput;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventLikes.DiscussionLikesOututPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventLikes.GetEventLikesUserPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventsCohost.GetEventCohostOutput;
import com.kaspontech.foodwall.modelclasses.EventsPojo.EventsComments.GetEventsCommentsPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetAllEventsOutput;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetEventsDisCmtsAllOutput;
import com.kaspontech.foodwall.modelclasses.EventsPojo.GetEventsDiscussionOutput;
import com.kaspontech.foodwall.modelclasses.FindFriends.GetFindFriendsOutput;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersOutput;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.DisplayPojo.GetHistoricalMapOutputPojo;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.Get_HotelStaticLocation_Response;
import com.kaspontech.foodwall.modelclasses.LoginPojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.modelclasses.LoginPojo.UpdateNewOtp_Response;
import com.kaspontech.foodwall.modelclasses.LoginPojo.VerifyOTP_response;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.CreateEditTimeline_Comments;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_follower_output_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_timeline_output_response;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Profile_get_username_output;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.User_image_update_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.profile_timeline_output_response;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Answer.GetAnswerOutput;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Answer.create_answer_votesResponse;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerComments.GetAnswerCmtsOutput;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.GetFollowerRequestOutput;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestionOutputPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewQuestionAnswer.Answer.GetQuestionAnswerOutput;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewQuestionAnswer.Poll.GetQuestionPollOutput;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Poll.GetPollDataPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Poll.create_delete_answer_poll_undoPOJO;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.PollComments.GetPollCommentsOutput;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Question.CreateQuestionOutput;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Question.GetQuestionsAllOutput;
import com.kaspontech.foodwall.modelclasses.ReportResponse;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Create_review_output_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.GetHotelAllOuputPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.GetHotelReviewImage_output;
import com.kaspontech.foodwall.modelclasses.Review_pojo.GetTimelineTaggedImage_Response;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_Deliverymode_Response;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_ambience_image_pojo_output;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_all_output;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_hotel_likes_output_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.GetAllHotelOutputPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.ReviewCommentsPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Review_comments_all_output;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Comment_like_output_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Comments_reply_output_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_edittimeline_output_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_multi_image_output;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Likes_2_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Newtimeline_output_response;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Profile_output_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Timeline_create_output_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.comments_2;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.create_edit_timeline_comments_Response;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.image_for_timeline;
import com.kaspontech.foodwall.modelclasses.logout.Logout_Response;
import com.kaspontech.foodwall.modelclasses.storyPojo.storyViewPojo;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_Timeline_Notification_Response;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_follower_req_count_output;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_follower_req_output;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_notification_count_output;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_unfollow_follower_output;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Notification_output_pojo;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.View_update_notification_output;
import com.kaspontech.foodwall.reviewpackage.ProfileReviewPojo.GetReviewProfileImageOutput;
import com.kaspontech.foodwall.reviewpackage.ReviewDeliveryDetailsPojo.ReviewDeliveyDetailsOutput;
import com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo.ReviewInDetailsOutput;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewOutputResponsePojo;
import com.kaspontech.foodwall.reviewpackage.Review_adapter_folder.Getall_hotel_review_output_pojo;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {





    /*------------------------------------Events API Starts --------------------------------------*/


    @Multipart
    @POST("webservice?")
    Call<CreateEventOutput> createEvent(
            @Query("methodname") String methodname,
            @Part("event_id") int event_id,
            @Part("event_name") RequestBody eventName,
            @Part("event_description") RequestBody eventDescription,
            @Part("event_type") int eventType,
            @Part("share_type") int share_type,
            @Part("image_exists") RequestBody image_exists,
            @Part("ticket_url") RequestBody ticketUrl,
            @Part("location") RequestBody location,
            @Part("start_date") RequestBody startDate,
            @Part("end_date") RequestBody endDate,
            @Part("latitude") double latitute,
            @Part("longitude") double longitude,
            @Part("created_by") int user_id,
            @Part("cohost_status") int cohost_status,
            @Part("cohost") RequestBody cohost,
            @Part("friends") RequestBody friends,
            @Part("img_status") int img_status,
            @Part MultipartBody.Part file
    );

    @Multipart
    @POST("webservice?")
    Call<CreateEventOutput> editEvent(
            @Query("methodname") String methodname,
            @Part("event_id") int event_id,
            @Part("event_name") RequestBody eventName,
            @Part("event_description") RequestBody eventDescription,
            @Part("event_type") int eventType,
            @Part("share_type") int share_type,
            @Part("image_exists") RequestBody image_exists,
            @Part("ticket_url") RequestBody ticketUrl,
            @Part("location") RequestBody location,
            @Part("start_date") RequestBody startDate,
            @Part("end_date") RequestBody endDate,
            @Part("latitude") double latitute,
            @Part("longitude") double longitude,
            @Part("created_by") int user_id,
            @Part("cohost_status") int cohost_status,
            @Part("cohost[]") List<Integer> cohost,
            @Part("friends[]") List<Integer> friends,
            @Part("file") RequestBody file
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> deleteEvents(
            @Query("methodname") String methodname,
            @Field("eventid") int eventid,
            @Field("created_by") int created_by
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetAllEventsOutput> getAllEvents(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("event_type") int event_type
    );
    //Single Event

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetAllEventsOutput> get_events_single(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("eventid") int eventid
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetAllEventsOutput> getUserEvents(
            @Query("methodname") String methodname,
            @Field("deleted") int deleted,
            @Field("userid") int userid

    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createEventsLikes(
            @Query("methodname") String methodname,
            @Field("eventid") int eventid,
            @Field("likes") int likes,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetEventLikesUserPojo> getEventsLikesUser(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("eventid") int eventid
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createEventsGoing(
            @Query("methodname") String methodname,
            @Field("eventid") int eventid,
            @Field("going") int going,
            @Field("created_by") int created_by
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createEventComment(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("eventid") int eventid,
            @Field("comments") String comments,
            @Field("created_by") int created_by
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> deleteEventComment(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("eventid") int eventid,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetEventsCommentsPojo> getAllEventComments(
            @Query("methodname") String methodname,
            @Field("eventid") int eventid,
            @Field("userid") int userid
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetEventCohostOutput> getEventCohost(
            @Query("methodname") String methodname,
            @Field("eventid") int eventid
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetEventShareOutput> getEventGuests(
            @Query("methodname") String methodname,
            @Field("eventid") int eventid,
            @Field("userid") int userid
    );


    @Multipart
    @POST("webservice?")
    Call<CommonOutputPojo> createEventDiscussion(
            @Query("methodname") String methodname,
            @Part("discss_id") int discss_id,
            @Part("event_id") int event_id,
            @Part("discuss_type") int discuss_type,
            @Part("dis_description") RequestBody dis_description,
            @Part("img_status") int img_status,
            @Part("img_del") int img_del,
            @Part("image_exists") RequestBody image_exists,
            @Part("created_by") int created_by,
            @Part("deleted") int deleted,
            @Part("image") RequestBody image
    );

    @Multipart
    @POST("webservice?")
    Call<CommonOutputPojo> createEventDiscussionWithImage(
            @Query("methodname") String methodname,
            @Part("discss_id") int discss_id,
            @Part("event_id") int event_id,
            @Part("discuss_type") int discuss_type,
            @Part("dis_description") RequestBody dis_description,
            @Part("img_status") int img_status,
            @Part("img_del") int img_del,
            @Part("image_exists") String image_exists,
            @Part("created_by") int created_by,
            @Part("deleted") int deleted,
            @Part MultipartBody.Part file
    );

    @Multipart
    @POST("webservice?")
    Call<CommonOutputPojo> editEventDiscussionWithImage(
            @Query("methodname") String methodname,
            @Part("discss_id") int discss_id,
            @Part("event_id") int event_id,
            @Part("discuss_type") int discuss_type,
            @Part("dis_description") RequestBody dis_description,
            @Part("img_status") int img_status,
            @Part("image_exists") RequestBody image_exists,
            @Part("created_by") int created_by,
            @Part("deleted") int deleted,
            @Part("image") RequestBody requestBodyImage
    );

    @Multipart
    @POST("webservice?")
    Call<CommonOutputPojo> deleteEventDiscussion(
            @Query("methodname") String methodname,
            @Part("discss_id") int discss_id,
            @Part("event_id") int event_id,
            @Part("created_by") int created_by,
            @Part("deleted") int deleted

    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetEventsDiscussionOutput> getEventsDiscussion(
            @Query("methodname") String methodname,
            @Field("eventid") int eventid,
            @Field("userid") int userid
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createEventsDiscussionLikes(
            @Query("methodname") String methodname,
            @Field("dis_evt_id") int dis_evt_id,
            @Field("likes") int likes,
            @Field("created_by") int created_by
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createEventsDiscussionCommentsLikes(
            @Query("methodname") String methodname,
            @Field("cmmt_dis_id") int dis_evt_id,
            @Field("likes") int likes,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createEventShare(
            @Query("methodname") String methodname,
            @Field("eventid") int eventid,
            @Field("userid") int userid,
            @Field("friends[]") List<Integer> friendList
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createEditEventDiscussionComment(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("diss_id") int diss_id,
            @Field("comments") String comments,
            @Field("created_by") int created_by,
            @Field("deleted") int deleted
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> deleteEventDiscussionComment(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("diss_id") int diss_id,
            @Field("created_by") int created_by,
            @Field("deleted") int deleted
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetEventsDisCmtsAllOutput> getEventsDisComments(
            @Query("methodname") String methodname,
            @Field("discuss_id") int discuss_id,
            @Field("userid") int userid
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<DiscussionLikesOututPojo> getEventsDiscussLikes(
            @Query("methodname") String methodname,
            @Field("discuss_id") int discuss_id,
            @Field("userid") int userid
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createEditEventDiscussionCommentReply(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("cmmt_dis_id") int cmmt_dis_id,
            @Field("comments") String comments,
            @Field("created_by") int created_by,
            @Field("deleted") int deleted
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> deleteEventDiscussionCommentReply(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("cmmt_dis_id") int diss_id,
            @Field("created_by") int created_by,
            @Field("deleted") int deleted
    );








    /*------------------------------------Events API ends --------------------------------------*/


    /*------------------------------------Q&A API starts --------------------------------------*/


    @FormUrlEncoded
    @POST("webservice?")
    Call<CreateQuestionOutput> createNormalQuestion(
            @Query("methodname") String methodname,
            @Field("questid") int questid,
            @Field("ask_question") String ask_question,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("created_by") int created_by,
            @Field("ques_type") int ques_type,
            @Field("poll") int poll_default

    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CreateQuestionOutput> createPollQuestion(
            @Query("methodname") String methodname,
            @Field("questid") int questid,
            @Field("ask_question") String ask_question,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("created_by") int created_by,
            @Field("ques_type") int ques_type,
            @Field("poll") String pollList

    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> deleteQuestion(
            @Query("methodname") String methodname,
            @Field("quesid") int quesid,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetQuestionsAllOutput> getQuestionAll(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<QuestionOutputPojo> getQuestionAll1(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<QuestionOutputPojo> getQuestionSingle(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("questid") int quesId
    );


    //Pagination API
    @FormUrlEncoded
    @POST("webservice?")
    Call<QuestionOutputPojo> getQuestionAll1(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("page") int page);

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetQuestionAnswerOutput> getQuestionAnswer(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetQuestionPollOutput> getQuestionPoll(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetAnswerOutput> getAnswerAll(
            @Query("methodname") String methodname,
            @Field("quesid") int quesid,
            @Field("userid") int userid
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetPollDataPojo> getPollData(
            @Query("methodname") String methodname,
            @Field("quesid") int quesid,
            @Field("userid") int userid
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<AnswerPojoClass> createAnswer(
            @Query("methodname") String methodname,
            @Field("ansid") int ansid,
            @Field("question_id") int ques_id,
            @Field("poll_id") int poll_id,
            @Field("ques_type") int ques_type,
            @Field("ask_answer") String answer,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("created_by") int created_by
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<AnswerPojoClass> createAnswerView(
            @Query("methodname") String methodname,
            @Field("ansid") int ansid,
            @Field("question_id") int ques_id,
            @Field("poll_id") int poll_id,
            @Field("ques_type") int ques_type,
            @Field("ask_answer") String answer,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> deleteAnswer(
            @Query("methodname") String methodname,
            @Field("ansid") int ansid,
            @Field("question_id") int question_id,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createQuesFollow(
            @Query("methodname") String methodname,
            @Field("quesid") int quesid,
            @Field("ques_created_by") int ques_created_by,
            @Field("follow") int follow,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<create_answer_votesResponse> createAnswerVotes(
            @Query("methodname") String methodname,
            @Field("ansid") int ansid,
            @Field("vote") int vote,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createQuestionRequest(
            @Query("methodname") String methodname,
            @Field("questid") int questid,
            @Field("created_by") int created_by,
            @Field("friends[]") List<Integer> friends
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createCommentVotes(
            @Query("methodname") String methodname,
            @Field("ans_cmmt_id") int ans_cmmt_id,
            @Field("likes") int likes,
            @Field("created_by") int friends
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> deleteAnswerComment(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("ansid") int ansid,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> deleteAnswerCommentReply(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("ansid") int ansid,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetFollowerRequestOutput> getRequestedFollower(
            @Query("methodname") String methodname,
            @Field("quesid") int questid,
            @Field("userid") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createAnswerComment(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("ansid") int ansid,
            @Field("comments") String comments,
            @Field("created_by") int created_by
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createAnswerCommentReply(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("cmmt_ans_id") int cmmt_ans_id,
            @Field("comments") String comments,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetAnswerCmtsOutput> getAnswerCommentsAll(
            @Query("methodname") String methodname,
            @Field("ansid") int questid,
            @Field("userid") int userid
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<create_delete_answer_poll_undoPOJO> undoPollVote(
            @Query("methodname") String methodname,
            @Field("quesid") int quesid,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetPollCommentsOutput> createEditPollComments(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("quesid") int quesid,
            @Field("comments") String comments,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createEditPollCommentsReply(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("cmmt_ques_id") int cmmt_ques_id,
            @Field("comments") String comments,
            @Field("created_by") int created_by
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> deletePollComments(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("quesid") int quesid,
            @Field("created_by") int created_by
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> deletePollCommentsReply(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("cmmt_ques_id") int quesid,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetPollCommentsOutput> getPollCommentsAll(
            @Query("methodname") String methodname,
            @Field("quesid") int quesid,
            @Field("userid") int userid
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createPollCommentsLikes(
            @Query("methodname") String methodname,
            @Field("ques_cmmt_id") int ques_cmmt_id,
            @Field("likes") int likes,
            @Field("created_by") int created_by
    );


    /*------------------------------------Q&A API ends --------------------------------------*/


    /*------------------------------------ Historical Map API starts --------------------------------------*/


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createHistoricalMap(
            @Query("methodname") String methodname,
            @Field("his_id") int his_id,
            @Field("hotel_name") String hotel_name,
            @Field("his_description") String his_descripion,
            @Field("with_whom") int with_whom,
            @Field("address") String address,
            @Field("created_on") String created_on,
            @Field("friends[]") List<Integer> friendsList,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("created_by") int created_by,
            @Field("deleted") int deleted

    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetHistoricalMapOutputPojo> getHistoricalMap(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );

    @Multipart
    @POST("webservice?")
    Call<CommonOutputPojo> createHistoricalImage(
            @Query("methodname") String methodname,
            @Part("timeline_id") int timeline_id,
            @Part("created_by") int created_by,
            @Part MultipartBody.Part file);

   /* @POST("webservice?")
    Call<CommonOutputPojo> createHistoricalImage(
            @Query("methodname") String methodname,
            @Body MultipartBody image
    );*/



    /*------------------------------------ Historical Map API ends --------------------------------------*/

    /*------------------------------------ Common Search API starts --------------------------------------*/


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetCommonSearchPojo> getCommonSearchResults(
            @Query("methodname") String methodname,
            @Field("search_text") String search_text,
            @Field("userid") int userid
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetCommonSearchTypePojo> getCommonResults(
            @Query("methodname") String methodname,
            @Field("search_text") String search_text
    );




    /*------------------------------------ Common Search API ends --------------------------------------*/





    /*------------------------------------Review API --------------------------------------*/

    //Create Review
    @FormUrlEncoded
    @POST("webservice?")
    Call<Create_review_output_pojo> create_hotel_review(

            @Query("methodname") String methodname,
            @Field("google_id") String google_id,
            @Field("hotel_name") String hotel_name,
            @Field("hotel_review") String hotel_review,
            @Field("category_type") int category_type,
            @Field("veg_nonveg") int veg_nonveg,
            @Field("place_id") String place_id,
            @Field("open_times") String open_times,
            @Field("address") String address,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("phone") int phone,
            @Field("upload_photo") int upload_photo,
            @Field("image") String image,
            @Field("google_photo") String google_photo,
            @Field("food_exprience") int food_exprience,
            @Field("ambiance") int ambiance,
            @Field("taste") int taste,
            @Field("service") int service,
            @Field("value_money") int value_money,
            @Field("created_by") int created_by

    );

    //Create Review delivery
    @FormUrlEncoded
    @POST("webservice?")
    Call<Create_review_output_pojo> create_review_delivery(
            @Query("methodname") String methodname,
            @Field("google_id") String google_id,
            @Field("hotel_name") String hotel_name,
            @Field("hotel_review") String hotel_review,
            @Field("category_type") int category_type,
            @Field("veg_nonveg") int veg_nonveg,
            @Field("place_id") String place_id,
            @Field("open_times") String open_times,
            @Field("address") String address,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("phone") int phone,
            @Field("upload_photo") int upload_photo,
            @Field("image") String image,
            @Field("google_photo") String google_photo,
            @Field("food_exprience") int food_exprience,
            @Field("ambiance") int timedelivery,
            @Field("taste") int taste,
            @Field("service") int packages,
            @Field("delivery_mode") int delivery_mode,
            @Field("value_money") int value_money,
            @Field("created_by") int created_by);
/*

//create_hotel_review_dish_image


    @Multipart
    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_dish_image(
            @Query("methodname") String methodname,
            @Part("hotel_id") int hotel_id,
            @Part("review_id") int review_id,
            @Part("dishname") String dishname,
            @Part("dishtype") int dishtype,
            @Part("created_by") int created_by,
            @Part("img_status") int img_status

    );

*/


    //Hotel dish image upload

    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_dish_image_new(
            @Query("methodname") String methodname,
            @Body MultipartBody image
    );

    //create_hotel_review_dish_without_image
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_dish_without_image(
            @Query("methodname") String methodname,
            @Field("hotel_id") String hotel_id,
            @Field("review_id") String review_id,
            @Field("dishname") String dishname,
            @Field("dishtype") int dishtype,
            @Field("created_by") int created_by

    );


    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_ambiance_image(
            @Query("methodname") String methodname,
            @Body MultipartBody image
    );

    //Get ambience image all
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_ambience_image_pojo_output> get_hotel_review_ambiance_image_all(
            @Query("methodname") String methodname,
            @Field("reviewid") int review_id,
            @Field("created_by") int created_by);

    //get_hotel_dish_type_all
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_dish_type_all_output> get_hotel_dish_type_all(
            @Query("methodname") String methodname,
            @Field("reviewid") int review_id,
            @Field("dish_type") int dish_type,
            @Field("userid") int userid);

    //get_hotel_details_all
    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewInDetailsOutput> get_hotel_all(
            @Query("methodname") String methodname,
            @Field("hotelid") int hotelid,
            @Field("userid") int userid);


    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewOutputResponsePojo> get_hotel_review_delivery_mode(
            @Query("methodname") String methodname,
            @Field("cate_type") int cate_type,
            @Field("delivery_mode") int delivery_mode,
            @Field("userid") int userid);

    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewDeliveyDetailsOutput> get_hotel_review_delivery_mode_image(
            @Query("methodname") String methodname,
            @Field("delivery_mode") int delivery_mode,
            @Field("userid") int userid);


    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewOutputResponsePojo> get_hotel_review_single(
            @Query("methodname") String methodname,
            @Field("reviewid") int reviewid,
            @Field("userid") int userid);


    //Delete hotel review

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> update_delete_hotel_review(
            @Query("methodname") String methodname,
            @Field("reviewid") int review_id,
            @Field("created_by") int created_by);

    //create_bucket_myself


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_bucket_myself(
            @Query("methodname") String methodname,
            @Field("hotel_id") int hotel_id,
            @Field("reviewid") int review_id,
            @Field("timelineid") int timelineid,
            @Field("bucket_username") String bucket_username,
            @Field("userid") int userid,
            @Field("post_type") int post_type);


    //create_bucket_individual_user

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_bucket_individual_user(
            @Query("methodname") String methodname,
            @Field("reviewid") int review_id,
            @Field("hotel_id") int hotel_id,
            @Field("timelineid") int timelineid,
            @Field("bucket_username") String bucket_username,
            @Field("userid") int userid,
            @Field("friends") String friends,
            @Field("post_type") int post_type);


//create_bucket_group


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_bucket_group(
            @Query("methodname") String methodname,
            @Field("reviewid") int review_id,
            @Field("hotel_id") int hotel_id,
            @Field("timelineid") int timelineid,
            @Field("bucket_username") String bucket_username,
            @Field("userid") int userid,
            @Field("group") String friends,
            @Field("post_type") int post_type);


    //Get all hotel review
    @FormUrlEncoded
    @POST("webservice?")
    Call<Getall_hotel_review_output_pojo> get_hotel_review_category_type(
            @Query("methodname") String methodname,
            @Field("cate_type") int cate_type,
            @Field("userid") int userid);

    //Get veg_review
    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewOutputResponsePojo> get_hotel_review_veg(
            @Query("methodname") String methodname,
            @Field("cate_type") int cate_type,
            @Field("veg_nonveg") int veg_nonveg,
            @Field("userid") int userid);

    //Get veg_review
    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewOutputResponsePojo> get_hotel_review_sort(
            @Query("methodname") String methodname,
            @Field("cate_type") int cate_type,
            @Field("veg_nonveg") int veg_nonveg,
            @Field("rate_wise") int rate_wise,
            @Field("userid") int userid);


    //Get veg_review page_count

    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewOutputResponsePojo> get_hotel_review_sort(
            @Query("methodname") String methodname,
            @Field("cate_type") int cate_type,
            @Field("veg_nonveg") int veg_nonveg,
            @Field("rate_wise") int rate_wise,
            @Field("userid") int userid,
            @Field("page") int page);


    //Get non_veg_review
    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewOutputResponsePojo> get_hotel_review_nonveg(
            @Query("methodname") String methodname,
            @Field("cate_type") int cate_type,
            @Field("veg_nonveg") int veg_nonveg,
            @Field("userid") int userid);


    //Review create comment


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_edit_hotel_review_comments_reply(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("cmmt_rev_id") int cmmt_rev_id,
            @Field("comments") String comments,
            @Field("created_by") int created_by);

    @FormUrlEncoded
    @POST("webservice?")
    Call<Review_comments_all_output> get_hotel_review_comments_all(
            @Query("methodname") String methodname,
            @Field("hotelid") int hotelid,
            @Field("reviewid") int reviewid,
            @Field("userid") int userid);


    //Get hotel all
    @FormUrlEncoded
    @POST("webservice?")
    Call<GetAllHotelOutputPojo> get_hotel_all(
            @Query("methodname") String methodname,
            @Field("userid") int userid);

    //create_hotel_review_likes
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_likes(
            @Query("methodname") String methodname,
            @Field("hotelid") int hotelId,
            @Field("reviewid") int reviewid,
            @Field("likes") int likes,
            @Field("created_by") int created_by
    );


    //create_hotel_review_comments_likes
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_comments_likes(
            @Query("methodname") String methodname,
            @Field("rev_cmmt_id") int rev_cmmt_id,
            @Field("likes") int likes,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_delete_hotel_review_comments(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("hotelid") int hotelid,
            @Field("reviewid") int reviewid,
            @Field("created_by") int created_by
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_delete_hotel_review_comments_reply(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("cmmt_rev_id") int cmmt_rev_id,
            @Field("created_by") int created_by
    );


    //create_hotel_review_comments
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_edit_hotel_review_comments(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("hotelid") int hotelid,
            @Field("reviewid") int reviewid,
            @Field("created_by") int created_by
    );


    //Edit_hotel_review_comments
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> edit_hotel_review_comments(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("hotelid") int hotelid,
            @Field("reviewid") int reviewid,
            @Field("created_by") int created_by
    );

    //get_hotel_review_likes_all
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_hotel_likes_output_pojo> get_hotel_review_likes_all(
            @Query("methodname") String methodname,
            @Field("hotelid") int hotelId,
            @Field("reviewid") int reviewid,
            @Field("userid") int userid

    );


    //get_hotel_review_likes_all
    @FormUrlEncoded
    @POST("webservice?")
    Call<GetHotelReviewImage_output> get_hotel_review_image_all(
            @Query("methodname") String methodname,
            @Field("reviewid") int reviewid

    );


    //Get hotel_review
    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewOutputResponsePojo> get_hotel_review(
            @Query("methodname") String methodname,
            @Field("hotelid") int hotelid,
            @Field("userid") int userid);













    /*------------------------------------Review API  ends--------------------------------------*/


    /*------------------------------------Notification API starts --------------------------------------*/
    //Get_notifications_all
    @FormUrlEncoded
    @POST("webservice?")
    Call<Notification_output_pojo> get_notifications_all(
            @Query("methodname") String methodname,
            @Field("userid") int userid);


    //Pagination API
    @FormUrlEncoded
    @POST("webservice?")
    Call<Notification_output_pojo> get_notifications_all(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("page") int page);


    //Get_notifications_count
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_notification_count_output> get_notifications_count(
            @Query("methodname") String methodname,
            @Field("userid") int userid);

    //Create_notification_view_update
    @FormUrlEncoded
    @POST("webservice?")
    Call<View_update_notification_output> create_notification_view_update(
            @Query("methodname") String methodname,
            @Field("userid") int userid);




    /*------------------------------------Notification API  API  ends--------------------------------------*/


    /*------------------------------------BucketList  API  Starts--------------------------------------*/

    //Get_bucket_myself API

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetMyBucketListPojo> get_bucket_myself(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );


    //Delete bucket

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> update_delete_bucket(
            @Query("methodname") String methodname,
            @Field("reviewid") int review_id,
            @Field("userid") int userid);


    // Get_bucket_individual_user API

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetmybucketOutputpojo> get_bucket_individual_user(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );




    /*------------------------------------BucketList  API  ends--------------------------------------*/





    /*------------------------------------CHAT API starts --------------------------------------*/

    //create_chat_login_sessiontime
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_chat_online_status_pojo> create_chat_login_sessiontime(
            @Query("methodname") String methodname,
            @Field("login_type") int login_type,
            @Field("user_id") int user_id,
            @Field("imei") String imei,
            @Field("latitude") double latitude,
            @Field("logitude") double logitude);


    //create_individual_chat
    @FormUrlEncoded
    @POST("webservice?")
    Call<Chat_output_pojo> create_individual_chat(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("friendid") int friendid,
            @Field("chat_message") String chat_message);

    //create_individual_group_chat
    @FormUrlEncoded
    @POST("webservice?")
    Call<Chat_output_pojo> create_group_chat(
            @Query("methodname") String methodname,
            @Field("groupid") int groupid,
            @Field("userid") int userid,
            @Field("chat_message") String chat_message);


    //Create group for chat
    @FormUrlEncoded
    @POST("webservice?")
    Call<NewChatOuputPojo> create_group(
            @Query("methodname") String methodname,
            @Field("group_name") String group_name,
            @Field("img_status") int img_status,
            @Field("created_by") int created_by,
            @Field("friends") String friends);


    //Create group for chat
    @FormUrlEncoded
    @POST("webservice?")
    Call<Chat_output_pojo> create_groupp(
            @Query("methodname") String methodname,
            @Field("group_name") String group_name,
            @Field("img_status") int img_status,
            @Field("created_by") int created_by,
            @Field("friends") String jsonstring);

    //Create group for chat test
    @FormUrlEncoded
    @POST("webservice?")
    Call<NewChatOuputPojo> create_grouptest(
            @Query("methodname") String methodname,
            @Field("group_name") String group_name,
            @Field("img_status") int img_status,
            @Field("created_by") int created_by,
            @Field("friends") String jsonstring);

    //Create group for chat_with icon
    @Multipart
    @POST("webservice?")
    Call<NewChatOuputPojo> create_group_with_icon(
            @Query("methodname") String methodname,
            @Part("group_name") RequestBody group_name,
            @Part("img_status") int img_status,
            @Part MultipartBody.Part image,
            @Part("created_by") int created_by,
            @Part("friends[]") List<Integer> friends);

    //Create group for chat_with icon test
    @Multipart
    @POST("webservice?")
    Call<NewChatOuputPojo> create_group_with_icon_test(
            @Query("methodname") String methodname,
            @Part("group_name") RequestBody group_name,
            @Part("img_status") int img_status,
            @Part MultipartBody.Part image,
            @Part("created_by") int created_by,
            @Part("friends") String friends); //Create group for chat_with icon

    @Multipart
    @POST("webservice?")
    Call<Chat_output_pojo> create_group_with_icon(
            @Query("methodname") String methodname,
            @Part("group_name") RequestBody group_name,
            @Part("img_status") int img_status,
            @Part MultipartBody.Part image,
            @Part("created_by") int created_by,
            @Part("friends") String friends);

    @POST("webservice?")
    Call<Chat_output_pojo> create_group_with_icon(@Query("methodname") String create_group,
                                                  @Body MultipartBody multipartBody);
    //update_chat_group
    @Multipart
    @POST("webservice?")
    Call<CommonOutputPojo> update_chat_group(
            @Query("methodname") String methodname,
            @Part("sessionid") int sessionid,
            @Part("groupid") int groupid,
            @Part("group_name") RequestBody group_name,
            @Part("img_status") int img_status,
            @Part MultipartBody.Part image,
            @Part("userid") int userid,
            @Part("which_change") int which_change);

    //update_chat_group w/o image
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> update_chat_group_no_image(
            @Query("methodname") String methodname,
            @Field("sessionid") int sessionid,
            @Field("groupid") int groupid,
            @Field("group_name") String group_name,
            @Field("img_status") int img_status,
            @Field("userid") int userid,
            @Field("which_change") int which_change);


    //Get_group_friends_user for chat
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_group_frds_output_pojo> get_group_friends_user(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("groupid") int groupid
    );


    //Chat_history
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_chat_history_output> get_chat_history(
            @Query("methodname") String methodname,
            @Field("userid") int userid

    );

    //Get single_chat

    @FormUrlEncoded
    @POST("webservice?")
    Call<Chat_history_single_output> get_chat_history_single(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("groupid") int groupid,
            @Field("sessionid") int sessionid
    );
    //Get group_chat_history

    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_group_history_output_pojo> get_chat_history_group(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("groupid") int groupid,
            @Field("sessionid") int sessionid
    );


//Chat Typing indicator

    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_chat_typing_output_pojo> get_chat_typing_status(
            @Query("methodname") String methodname,
            @Field("sessionid") int sessionid,
            @Field("userid") int userid
    );


    //Chat Typing indicator API hitting

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> update_chat_typing_status(
            @Query("methodname") String methodname,
            @Field("sessionid") int sessionid,
            @Field("yourid") int userid,
            @Field("type_status") int type_status
    );


    //Chat_user_delete
    @FormUrlEncoded
    @POST("webservice?")
    Call<com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput> update_delete_chat_history(
            @Query("methodname") String methodname,
            @Field("session_id") int session_id,
            @Field("userid") int userid
    );

    //Chat_Single_user_delete
    @FormUrlEncoded
    @POST("webservice?")
    Call<com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput> update_delete_single_chat(
            @Query("methodname") String methodname,
            @Field("chat_id") int chat_id,
            @Field("userid") int userid
    );


    //Chat_get_following

    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_follower_output_pojo> get_chat_follower(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );

//Chat_get_group_member

    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_group_member_output_pojo> get_group_friends(
            @Query("methodname") String methodname,
            @Field("groupid") int groupid,
            @Field("userid") int userid
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput> update_exit_chat_group_user(
            @Query("methodname") String methodname,
            @Field("groupid") int groupid,
            @Field("exist_user_id") int exist_user_id
    );




    /*------------------------------------CHAT API ends--------------------------------------*/


    // Create timeline post
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> createTimeLinePost(

            @Query("methodname") String methodname,
            @Field("timeline_id") int timeline_id,
            @Field("google_id") String google_id,
            @Field("google_photo") String google_photo,
            @Field("hotel_name") String hotel_name,
            @Field("place_id") String place_id,
            @Field("open_times") String open_times,
            @Field("timeline_description") String timeline_description,
            @Field("with_whom") int with_whom,
            @Field("address") String address,
            @Field("phone") String phone,
            @Field("friends") String friendsList,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("created_by") int created_by

    );


    // delete timeline image single
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_delete_timeline_single_image(
            @Query("methodname") String methodname,
            @Field("img_id") int img_id,
            @Field("timelineid") int timelineid,
            @Field("created_by") int created_by
    );

    //Create Likes
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_timeline_likes(
            @Query("methodname") String methodname,
            @Field("timelineid") int timelineid,
            @Field("likes") int likes,
            @Field("created_by") int created_by
    );


    //Create follower
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_following_output_Pojo> create_follower(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("followerid") int followerid,
            @Field("follow") int follow);

    //create_follower_request
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_follower_request(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("followerid") int followerid,
            @Field("follow") int follow);


    //Create unfollow
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_unfollow_follower_output> create_unfollow(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("followerid") int followerid,
            @Field("follow") int follow);

    //Get all timeline likes
    @FormUrlEncoded
    @POST("webservice?")
    Call<Likes_2_pojo> get_timeline_likes_all(
            @Query("methodname") String methodname,
            @Field("timelineid") int timelineid,
            @Field("userid") int userid);


    @FormUrlEncoded
    @POST("webservice?")
    Call<Likes_2_pojo> get_timeline_likes_user(
            @Query("methodname") String methodname,
            @Field("timelineid") int timelineid,
            @Field("userid") int userid);


    @FormUrlEncoded
    @POST("webservice?")
    Call<image_for_timeline> get_image_all(
            @Query("methodname") String methodname,
            @Field("deleted") String deleted);

    //Get_timeline_user
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_edittimeline_output_pojo> get_timeline_user(
            @Query("methodname") String methodname,
            @Field("deleted") int deleted,
            @Field("userid") int userid);


    //MultiImage
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_multi_image_output> get_timeline_image(
            @Query("methodname") String methodname,
            @Field("timelineid") int timelineid
    );

    //Dismiss follower
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_unfollow_follower_output> dismiss_follower(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("followerid") int followerid,
            @Field("follow") int follow);


    //get_follower_request_count
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_follower_req_count_output> get_follower_request_count(
            @Query("methodname") String methodname,
            @Field("userid") int userid);


    //get_follower_request
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_follower_req_output> get_follower_request(
            @Query("methodname") String methodname,
            @Field("userid") int userid);

    //Get_timeline_image_all
    @FormUrlEncoded
    @POST("webservice?")
    Call<Timeline_image_only_output_pojo> Get_timeline_image_all(
            @Query("methodname") String methodname,
            @Field("userid") int userid);

    //Get all comments
    @FormUrlEncoded
    @POST("webservice?")
    Call<comments_2> get_timeline_comments_all(
            @Query("methodname") String methodname,
            @Field("timelineid") int timelineid,
            @Field("userid") int userid);

    //Get all reply  comments
    @FormUrlEncoded
    @POST("webservice?")
    Call<Comment_like_output_pojo> get_timeline_comment_likes_all(
            @Query("methodname") String methodname,
            @Field("cmmt_tl_id") int cmmt_tl_id);


    //Get all reply  comments likes
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_timeline_comments_likes(
            @Query("methodname") String methodname,
            @Field("tl_cmmt_id") int tl_cmmt_id,
            @Field("created_by") int created_by,
            @Field("likes") int likes
    );


    //Get all comments_reply
    @FormUrlEncoded
    @POST("webservice?")
    Call<Comments_reply_output_pojo> get_timeline_comment_reply_all(
            @Query("methodname") String methodname,
            @Field("cmmt_tl_id") int cmmt_tl_id
    );

    //Create comment,edit
    @FormUrlEncoded
    @POST("webservice?")
    Call<com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput> create_edit_timeline_comments(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("timelineid") int timelineid,
            @Field("comments") String comments,
            @Field("created_by") int created_by);

    //CreateEditTimeline_Comments

    @FormUrlEncoded
    @POST("webservice?")
    Call<CreateEditTimeline_Comments> create_edit_timeline_comments1(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("timelineid") int timelineid,
            @Field("comments") String comments,
            @Field("created_by") int created_by);

    @FormUrlEncoded
    @POST("webservice?")
    Call<create_edit_timeline_comments_Response> create_edit_timeline_commentss(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("timelineid") int timelineid,
            @Field("comments") String comments,
            @Field("created_by") int created_by);
    @FormUrlEncoded
    @POST("webservice?")
    Call<CreateEditEventsComments> create_edit_events_comments(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("eventid") int timelineid,
            @Field("comments") String comments,
            @Field("created_by") int created_by);

    @FormUrlEncoded
    @POST("webservice?")
    Call<CreateEditEventsComments> create_edit_hotel_review_comments(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("hotelid") int hotelid,
            @Field("reviewid") int reviewid,
            @Field("comments") String comments,
            @Field("created_by") int created_by);
    //Create comment reply
    @FormUrlEncoded
    @POST("webservice?")
    Call<com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput> create_edit_timeline_comments_reply(
            @Query("methodname") String methodname,
            @Field("reply_id") int reply_id,
            @Field("tl_cmmt_id") int tl_cmmt_id,
            @Field("comments") String comments,
            @Field("created_by") int created_by);


    // Reply edit
    @FormUrlEncoded
    @POST("webservice?")
    Call<com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput> edit_timeline_comments_reply(
            @Query("methodname") String methodname,
            @Field("reply_id") int comment_id,
            @Field("tl_cmmt_id") int tl_cmmt_id,
            @Field("comments") String comments,
            @Field("created_by") int created_by);


    //Comment delete

    @FormUrlEncoded
    @POST("webservice?")
    Call<com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput> create_delete_timeline_comments(
            @Query("methodname") String methodname,
            @Field("comment_id") int comment_id,
            @Field("timelineid") int timelineid,
            @Field("created_by") int created_by);
    //Comment delete


    @FormUrlEncoded
    @POST("webservice?")
    Call<com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput> create_delete_timeline_comments_reply(
            @Query("methodname") String methodname,
            @Field("reply_id") int reply_id,
            @Field("tl_cmmt_id") int tl_cmmt_id,
            @Field("created_by") int created_by);




    //Create timeline user(Post)
    @Multipart
    @POST("webservice?")
    Call<com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput> create_timeline_user(
            @Query("methodname") String methodname,
            @Part("timeline_id") int timeline_id,
            @Part("timeline_description") RequestBody timeline_description,
            @Part("latitude") double latitude,
            @Part("longitude") double longitude,
            @Part("created_by") RequestBody created_by
            /* @Part MultipartBody.Part image*/
    );
//Create timeline user post image

    @POST("webservice?")
    Call<com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput> uploadMultiFile(
            @Query("methodname") String methodname,
            @Body MultipartBody image);

    @Multipart
    @POST("webservice?")
    Call<com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput> create_timeline_image(
            @Query("methodname") String methodname,
            @Part("timeline_id") int timeline_id,
            @Part("created_by") RequestBody created_by,
            @Part MultipartBody.Part image
    );


    //Delete timeline post
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_delete_timeline(
            @Query("methodname") String methodname,
            @Field("timelineid") int timelineid,
            @Field("created_by") int created_by

    );


    //Delete story
    @FormUrlEncoded
    @POST("webservice?")
    Call<Create_story_output_pojo> create_delete_stories(
            @Query("methodname") String methodname,
            @Field("stories_id") int stories_id,
            @Field("created_by") int created_by

    );


    /*---------------------------------Stores API Call---------------------------------------*/


    //Create stories
    @Multipart
    @POST("webservice?")
    Call<Create_story_output_pojo> create_stories(
            @Query("methodname") String methodname,
            @Part("stories_id") int stories_id,
            @Part("stories_description") RequestBody stories_description,
            @Part("image_exists") RequestBody image_exists,
            @Part("address") RequestBody address,
            @Part("area") RequestBody area,
            @Part("city") RequestBody city,
            @Part("mob_ios") int mob_ios,
            @Part("created_by") RequestBody created_by,
            @Part MultipartBody.Part image
    );

    @POST("webservice?")
    Call<CommonOutputPojo> createStories(
            @Query("methodname") String methodname,
            @Body MultipartBody image
    );


    //Edit stories

    @Multipart
    @POST("webservice?")
    Call<Create_story_output_pojo> edit_stories(
            @Query("methodname") String methodname,
            @Part("stories_id") int stories_id,
            @Part("stories_description") RequestBody stories_description,
            @Part MultipartBody.Part image,
            @Part("image_exists") String image_exists,
            @Part("created_by") RequestBody created_by);

    //Get all stories
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_stories_pojo_output> get_stories_all(
            @Query("methodname") String methodname,
            @Field("userid") int userid);


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_stories_view(
            @Query("methodname") String methodname,
            @Field("stories_id") int stories_id,
            @Field("userid") int userid
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<storyViewPojo> getStoriesView(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("stories_id") int stories_id
    );


    //Timelineall
    @FormUrlEncoded
    @POST("webservice?")
    Call<Newtimeline_output_response> gettimelineall(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("page") int page);


    //Old Timeline call
    @FormUrlEncoded
    @POST("webservice?")
    Call<Newtimeline_output_response> gettimelineall(
            @Query("methodname") String methodname,
            @Field("userid") int userid);

//Timeline single user

    @FormUrlEncoded
    @POST("webservice?")
    Call<profile_timeline_output_response> get_timeline_single(
            @Query("methodname") String methodname,
            @Field("timelineid") int timelineid,
            @Field("userid") int userid);


    //Profile
    @FormUrlEncoded
    @POST("webservice?")
    Call<Profile_output_pojo> get_profile(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("yourid") int yourid
    );


    //Get_all_images
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_timeline_output_response> get_all_timeline_images(
            @Query("methodname") String methodname,
            @Field("userid") int userid);

    //Get_all_images_user
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_timeline_output_response> get_alluser_timeline_images(
            @Query("methodname") String methodname,
            @Field("userid") int userid);


//update_user_profile_image

    @Multipart
    @POST("webservice?")
    Call<User_image_update_pojo> update_user_profile_image(
            @Query("methodname") String methodname,
            @Part("userid") int userid,
            @Part MultipartBody.Part image);


    //Update Password
    @FormUrlEncoded
    @POST("webservice?")
    Call<Timeline_create_output_pojo> update_change_password(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("old_password") String old_password,
            @Field("new_password") String new_password
    );


    //Get_username
    @FormUrlEncoded
    @POST("webservice?")
    Call<Profile_get_username_output> get_username(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("username") String username);


    //get_followers_list
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_follower_output_pojo> get_follower(
            @Query("methodname") String methodname,
            @Field("userid") int userid);

    //get_follower_search
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_follower_output_pojo> get_follower_search(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("search_text") String search_text);


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetFindFriendsOutput> getFindFriends(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );


    //	get_group_search
    @FormUrlEncoded
    @POST("webservice?")
    Call<SearchBucketOutputPojo> get_group_search(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("search_text") String search_text);


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetFollowersOutput> getFollower(
            @Query("methodname") String methodname,
            @Field("userid") int userid);


    //get_following_list
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_following_output_Pojo> get_following(
            @Query("methodname") String methodname,
            @Field("userid") int userid);


    //Editprofile
    @Multipart
    @POST("webservice?")
    Call<Timeline_create_output_pojo> update_user_profile(
            @Query("methodname") String methodname,
            @Part("userid") int userid,
            @Part("username") RequestBody user_name_change,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("gender") RequestBody gender,
            @Part("dob") RequestBody dob,
            @Part("bio_description") RequestBody bio_description,
            @Part("profile_status") RequestBody profile_status);

//Editprofile
//    @FormUrlEncoded
//    @POST("webservice?")
//    Call<Timeline_create_output_pojo> update_user_profile(
//            @Query("methodname") String methodname,
//            @Field("userid") int  userid,
//            @Field("first_name") String  first_name,
//            @Field("last_name") String  last_name,
//            @Field("gender") String  gender,
//            @Field("dob") String  dob);

    //ForgotPassword
    @FormUrlEncoded
    @POST("webservice?")
    Call<Timeline_create_output_pojo> update_forgot_password(
            @Query("methodname") String methodname,
            @Field("email") String email);

    //Verifycode_password
    @FormUrlEncoded
    @POST("webservice?")
    Call<Timeline_create_output_pojo> update_verifycode(
            @Query("methodname") String methodname,
            @Field("email") String email,
            @Field("verifycode") int verifycode);


    //Update_new_password
    @FormUrlEncoded
    @POST("webservice?")
    Call<Timeline_create_output_pojo> update_new_password(
            @Query("methodname") String methodname,
            @Field("email") String email,
            @Field("verifycode") int verifycode,
            @Field("new_password") String new_password);


    //Create user
    //Create user
    @FormUrlEncoded
    @POST("webservice?")
    Call<CreateUserPojoOutput> createUser(
            @Query("methodname") String methodname,
            @Field("imei") String imei,
            @Field("gcmkey") String gcmkey,
            @Field("oauthprovider") String oauthprovider,
            @Field("oauth_uid") String oauth_uid,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("gender") String gender,
            @Field("dob") String dob,
            @Field("locale") String locale,
            @Field("link") String link,
            @Field("ip") String ip,
            @Field("bio_descrip") String bio_descrip,
            @Field("picture") String picture,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude,
            @Field("register_type") String register_type,
            @Field("mobile_os") String mobile_os,
            @Field("logintype") String logintype,
            @Field("image") String Image,
            @Field("countrycode") String countrycode,
            @Field("country") String country

    );




/*
    @Multipart
    @POST("webservice?")
    Call<CreateUserPojoOutput> signUp(
            @Query("methodname") String methodname,
            @Part("imei") RequestBody imei,
            @Part("gcmkey") RequestBody gcmkey,
            @Part("oauthprovider") RequestBody oauthprovider,
            @Part("oauth_uid") RequestBody oauth_uid,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("gender") RequestBody gender,
            @Part("dob") RequestBody dob,
            @Part("locale") RequestBody locale,
            @Part("link") String link,
            @Part("ip") String ip,
            @Part("picture") String picture,
            @Part("latitude") double latitude,
            @Part("longitude") double longitude,
            @Part("register_type") int register_type,
            @Part("mobile_os") int mobile_os,
            @Part("logintype") int logintype,
            @Part MultipartBody.Part Image

    );*/

    //User SignUp
    @Multipart
    @POST("webservice?")
    Call<CreateUserPojoOutput> signUpWithProfilePhoto(
            @Query("methodname") String methodname,
            @Part("imei") RequestBody imei,
            @Part("gcmkey") RequestBody gcmkey,
            @Part("oauthprovider") RequestBody oauthprovider,
            @Part("oauth_uid") RequestBody oauth_uid,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("gender") RequestBody gender,
            @Part("dob") RequestBody dob,
            @Part("locale") RequestBody locale,
            @Part("link") String link,
            @Part("ip") String ip,
            @Part("bio_descrip") String bio_descrip,
            @Part("picture") String picture,
            @Part("latitude") double latitude,
            @Part("longitude") double longitude,
            @Part("register_type") int register_type,
            @Part("mobile_os") int mobile_os,
            @Part("logintype") int logintype,
            @Part("img_status") int img_status,
            @Part("profile_status") int profile_status,
            @Part MultipartBody.Part image

    );





    //Verifycode_password
    @FormUrlEncoded
    @POST("webservice?")
    Call<UpdateNewOtp_Response> update_new_register_email_verifycode(
            @Query("methodname") String methodname,
            @Field("email") String email,
            @Field("verifycode") String verifycode);


    @Multipart
    @POST("webservice?")
    Call<CreateUserPojoOutput> signUpWithOutProfilePhoto(
            @Query("methodname") String methodname,
            @Part("imei") RequestBody imei,
            @Part("gcmkey") String gcmkey,
            @Part("oauthprovider") RequestBody oauthprovider,
            @Part("oauth_uid") RequestBody oauth_uid,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("gender") RequestBody gender,
            @Part("dob") RequestBody dob,
            @Part("locale") RequestBody locale,
            @Part("link") String link,
            @Part("ip") String ip,
            @Part("bio_descrip") String bio_descrip,
            @Part("picture") String picture,
            @Part("latitude") double latitude,
            @Part("longitude") double longitude,
            @Part("register_type") int register_type,
            @Part("mobile_os") int mobile_os,
            @Part("logintype") int logintype,
            @Part("img_status") int img_status,
            @Part("profile_status") int profile_status

    );


    //User Login
    @FormUrlEncoded
    @POST("webservice?")
    Call<CreateUserPojoOutput> userLogin(
            @Query("methodname") String methodname,
            @Field("username") String username,
            @Field("password") String password,
            @Field("latitude") double latitute,
            @Field("longitude") double longitude,
            @Field("imei") String imei,
            @Field("gcmkey") String gcmkey
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> checkValidEmail(
            @Query("methodname") String methodname,
            @Field("username") String username

    );


    /*------------------------------------Events API ends --------------------------------------*/


    /*------------------------------------Q&A API starts --------------------------------------*/










    /*------------------------------------Q&A API ends --------------------------------------*/


    /*------------------------------------Review API --------------------------------------*/


//create_hotel_review_dish_image_top(Without Image)

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_dish_image_top(
            @Query("methodname") String methodname,
            @Field("hotel_id") int hotel_id,
            @Field("review_id") int review_id,
            @Field("dishname") String dishname,
            @Field("dishtype") int dishtype,
            @Field("img_status") int img_status,
            @Field("created_by") int created_by
    );


    //create_hotel_review_dish_image_avoid(Without Image)

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_dish_image_avoid(
            @Query("methodname") String methodname,
            @Field("hotel_id") int hotel_id,
            @Field("review_id") int review_id,
            @Field("dishname") String dishname,
            @Field("dishtype") int dishtype,
            @Field("img_status") int img_status,
            @Field("created_by") int created_by
    );


    @FormUrlEncoded
    @POST("webservice?")
    Call<GetReviewProfileImageOutput> getProfileReviewImagesUser(
            @Query("methodname") String methodname,
            @Field("userid") int userid

    );


    //Get ambience image all new
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_ambience_image_pojo_output> get_hotel_review_ambiance_image_all_new(
            @Query("methodname") String methodname,
            @Field("reviewid") int review_id
    );


    //Get all hotel review new
    @FormUrlEncoded
    @POST("webservice?")
    Call<GetHotelAllOuputPojo> get_hotel_review_category_type_new(
            @Query("methodname") String methodname,
            @Field("cate_type") int cate_type,
            @Field("userid") int userid);


    //Get all hotel review new
    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewOutputResponsePojo> get_hotel_review_category_type_updated(
            @Query("methodname") String methodname,
            @Field("cate_type") int cate_type,
            @Field("userid") int userid);


    //Get all hotel review new pagination
    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewOutputResponsePojo> get_hotel_review_category_type_updated(
            @Query("methodname") String methodname,
            @Field("cate_type") int cate_type,
            @Field("userid") int userid,
            @Field("page") int page);


    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewOutputResponsePojo> get_hotel_review_user(
            @Query("methodname") String methodname,
            @Field("userid") int userid);


    @FormUrlEncoded
    @POST("webservice?")
    Call<Comment_like_output_pojo> get_hotel_review_comment_likes_all(
            @Query("methodname") String methodname,
            @Field("cmmt_rev_id") int cmmt_rev_id);













    /*------------------------------------Review API  ends--------------------------------------*/



    /*------------------------------------CHAT API starts --------------------------------------*/


    //create_group_friends
    @FormUrlEncoded
    @POST("webservice?")
    Call<ChatAddMemberOutput> create_group_friends(
            @Query("methodname") String methodname,
            @Field("groupid") int groupid,
            @Field("userid") int userid,
            @Field("friends[]") List<Integer> friends);


//get_chat_friend_group

    @FormUrlEncoded
    @POST("webservice?")
    Call<ChatAddmemberOutputPojo> get_chat_friend_group(
            @Query("methodname") String methodname,
            @Field("groupid") int groupid,
            @Field("yourid") int yourid
    );


//get_bucket_group

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetmybucketOutputpojo> get_bucket_group_all(
            @Query("methodname") String methodname,
            @Field("groupid") int groupid,
            @Field("userid") int userid

    );






    /*------------------------------------Notification API  API  ends--------------------------------------*/


//Get_bucket_group API

    @FormUrlEncoded
    @POST("webservice?")
    Call<GroupBucketList_Response> get_bucket_group(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );


    //get_group_user API

    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_grp_bucket_pojo> get_group_user(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );
//get_group_user search API

   /* @FormUrlEncoded
    @POST("webservice?")
    Call<SearchBucketOutputPojo> get_group_user_search(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );
*/


    /*//create_bucket_group

    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_bucket_group(
            @Query("methodname") String methodname,
            @Field("reviewid") int review_id,
            @Field("timelineid") int timelineid,
            @Field("bucket_username") String bucket_username,
            @Field("userid") int userid,
            @Field("group[]") List<Integer> friends,
            @Field("post_type") int post_type);*/


    /*------------------------------------BucketList  API  ends--------------------------------------*/


    //Create stories
    @Multipart
    @POST("webservice?")
    Call<Create_story_output_pojo> create_stories(
            @Query("methodname") String methodname,
            @Part("stories_id") int stories_id,
            @Part("stories_description") RequestBody stories_description,
            @Part MultipartBody.Part image,
            @Part("image_exists") String image_exists,
            @Part("created_by") RequestBody created_by);


    /*---------------------------------- total count for each module----------------------------------*/


    //create get_total_count_modules

    @FormUrlEncoded
    @POST("webservice?")
    Call<TotalCountModule_Response> get_total_count_modules(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );


    /* ------------------------------------update_delete_chat_clear api -----------------------------*/

    //create update_delete_chat_clear api

    @FormUrlEncoded
    @POST("webservice?")
    Call<ClearChatResponse_Pojo> update_delete_chat_clear(
            @Query("methodname") String methodname,
            @Field("session_id") int session_id,
            @Field("userid") int userid,
            @Field("groupid") int groupid
    );


    /* -------------------------------------- get_timeline_image_tagged_user---------------------------*/

    //create update_delete_chat_clear api

    @FormUrlEncoded
    @POST("webservice?")
    Call<GetTimelineTaggedImage_Response> get_timeline_image_tagged_user(
            @Query("methodname") String methodname,
             @Field("userid") int userid
     );


    /*---------------------------------------------create_report_email--------------------------------- */

    //create_report_email
    @FormUrlEncoded
    @POST("webservice?")
    Call<ReportResponse> create_report_email(
            @Query("methodname") String methodname,
            @Field("page") int page,  /* 1- timeline , 2-events , 3- review ,4-qa  */
            @Field("comment") String comment,
            @Field("userid") int userid,
            @Field("module_id") String module_id, /*( timeline,events,qa, review ) */
            @Field("create_by") int create_by
    );



    /* ------------------------------------------  get_chat_user_search ---------------------------------------  */

    //create_report_email
    @FormUrlEncoded
    @POST("webservice?")
    Call<ChatSearch_Response> get_chat_user_search(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("searchtext") String searchtext
    );


    /* ------------------------------------------------get_hotel_static_location----------------------------------*/

    //get_hotel_static_location
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_HotelStaticLocation_Response> get_hotel_static_location(
            @Query("methodname") String methodname,
            @Field("userid") int userid
    );


 /* ------------------------------------------------  get_delivery_mode----------------------------------*/
    //get_delivery_mode
    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_Deliverymode_Response> get_delivery_mode(
            @Query("methodname") String methodname,
            @Field("deleted") int userid
    );

    @FormUrlEncoded
    @POST("webservice?")
    Call<Get_Timeline_Notification_Response> get_timeline_singleNotification(
            @Query("methodname") String methodname,
            @Field("timelineid") int timelineid,
            @Field("userid") int userid);



  /* ------------------------ get_bucket_individual_user_inside api ----------------------------------*/

    @FormUrlEncoded
    @POST("webservice?")
    Call<BucketIndividualuser_Response> get_bucket_individual_user_inside(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("inv_userid") int inv_userid,
            @Field("page") int page
    );

    /* ------------------------ get_bucket_group_inside api ----------------------------------*/

    @FormUrlEncoded
    @POST("webservice?")
    Call<BucketIndividualuser_Response> get_bucket_group_inside(
            @Query("methodname") String methodname,
            @Field("userid") int userid,
            @Field("groupid") int inv_userid,
            @Field("page") int page
    );

   /* create_email_verify_otp api */
   @FormUrlEncoded
   @POST("webservice?")
   Call<VerifyOTP_response> generateOTP(
           @Query("methodname") String methodname,
           @Field("email") String userid
   );




    /* update_logout api */
    @FormUrlEncoded
    @POST("webservice?")
    Call<Logout_Response> update_logout(
            @Query("methodname") String methodname,
            @Field("userid") String userid
    );


    //Get all hotel review new
    @FormUrlEncoded
    @POST("webservice?")
    Call<ReviewCommentsPojo> get_create_edit_hotel_review_comments(
            @Query("methodname") String methodname,
            @Field("hotelid") int hotelid,
            @Field("comment_id") int comment_id,
            @Field("created_by") int created_by,
            @Field("comments") String comments,
            @Field("reviewid") int reviewid
            );

    //Get all hotel review new
    @FormUrlEncoded
    @POST("webservice?")
    Call<GetPollCommentsOutput> get_create_edit_question_comments(
            @Query("methodname") String methodname,
            @Field("quesid") int hotelid,
            @Field("comment_id") int comment_id,
            @Field("created_by") int created_by,
            @Field("comments") String comments
    );

    // get_timeline_comments_all
    @FormUrlEncoded
    @POST("webservice?")
    Call<GetEventsCommentsAll_Response> get_events_comments_all(
            @Query("methodname") String methodname,
            @Field("userid") int hotelid,
            @Field("eventid") int comment_id
    );


    //new apis

    //create_hotel_review_dish_withoutimage
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_dish_imageWithoutImage(
            @Query("methodname") String methodname,
            @Field("img_type") int img_type,
            @Field("img_status") int img_status,
            @Field("hotel_id") String hotel_id,
            @Field("review_id") String review_id,
            @Field("created_by") String created_by,
            @Field("dishtype") int dishtype,
            @Field("dishname") String dishname
    );


    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_dish_imageWithImage(
            @Query("methodname") String methodname,
            @Body MultipartBody image
    );

    // ambience create_hotel_review_dish_image
    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_dish_imageWithImageambience(
            @Query("methodname") String methodname,
            @Body MultipartBody image
    );

    // ambience create_hotel_review_dish_image
    @FormUrlEncoded
    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_dish_imageWithImageamb(
            @Query("methodname") String methodname,
            @Field("img_type") int img_type,
            @Field("hotel_id") int hotel_id,
            @Field("review_id") int review_id,
            @Field("created_by") int created_by,
            @Field("amb_image") String pack_image
    );
    @POST("webservice?")
    Call<CommonOutputPojo> create_hotel_review_dish_imageWithImagepackaging(
            @Query("methodname") String methodname,
            @Body MultipartBody image
    );
/*
    @Multipart
    @POST("webservice?")
    Call<CreateUserPojoOutput> signUp(
            @Query("methodname") String methodname,
            @Part("imei") RequestBody imei,
            @Part("gcmkey") RequestBody gcmkey,
            @Part("oauthprovider") RequestBody oauthprovider,
            @Part("oauth_uid") RequestBody oauth_uid,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("gender") RequestBody gender,
            @Part("dob") RequestBody dob,
            @Part("locale") RequestBody locale,
            @Part("link") String link,
            @Part("ip") String ip,
            @Part("picture") String picture,
            @Part("latitude") double latitude,
            @Part("longitude") double longitude,
            @Part("register_type") int register_type,
            @Part("mobile_os") int mobile_os,
            @Part("logintype") int logintype,
            @Part MultipartBody.Part Image

    );*/


}