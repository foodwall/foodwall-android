package com.kaspontech.foodwall.REST_API;

import com.kaspontech.foodwall.eventspackage.PlaceSearchDetails;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.NearByRestaurant.GetNearByRestaurantOutputPojo;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.RestaurantDetails;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.GetAllRestaurantsOutputPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Restaurant_output_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.ResultDistanceMatrix;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface API_interface_restaurant {

/*
    @GET("place/nearbysearch/json?")
    Call<PlacesPOJO.Root> doPlaces(
            @Query(value = "type", encoded = true)
                    String type,
            @Query(value = "location", encoded = true)
                    String location, @Query(value = "name", encoded = true)
                    String name, @Query(value = "opennow", encoded = true) boolean opennow,
            @Query(value = "rankby", encoded = true)
                    String rankby,
            @Query(value = "key", encoded = true)
            String key);
    */


    @GET("place/nearbysearch/json?")
    Call<Restaurant_output_pojo> doPlaces(
            @Query(value = "type", encoded = true)
                    String type,
            @Query(value = "location", encoded = true)
                    String location,
            @Query(value = "name", encoded = true)
                    String name,
            @Query(value = "opennow", encoded = true)
                    boolean opennow,
            @Query(value = "rankby", encoded = true)
                    String rankby,
            @Query(value = "key", encoded = true)
                    String key);

    @GET("place/nearbysearch/json?")
    Call<GetNearByRestaurantOutputPojo> nearByRestaurants(
            @Query(value = "type", encoded = true)
                    String type,
            @Query(value = "location", encoded = true)
                    String location,
            @Query(value = "radius", encoded = true)
                    String radius,
            @Query(value = "key", encoded = true)
                    String key);


    @GET("place/nearbysearch/json?")
    Call<GetAllRestaurantsOutputPojo> nearByRestaurants_review(
            @Query(value = "type", encoded = true)
                    String type,
            @Query(value = "location", encoded = true)
                    String location,
            @Query(value = "radius", encoded = true)
                    String radius,
            @Query(value = "key", encoded = true)
                    String key);

    @GET("place/textsearch/json?")
    Call<RestaurantDetails> searchRestaurantsReviewWithnearby(
            @Query(value = "query", encoded = true)
                    String query,
            @Query(value = "location", encoded = true)
                    String location,
            @Query(value = "radius", encoded = true)
                    String radius,
            @Query(value = "type", encoded = true)
                    String type,
            @Query(value = "key", encoded = true)
                    String key);

    @GET("place/textsearch/json?")
    Call<RestaurantDetails> searchRestaurants(
            @Query(value = "query", encoded = true)
                    String query,
            @Query(value = "location", encoded = true)
                    String location,
            @Query(value = "radius", encoded = true)
                    String radius,
            @Query(value = "type", encoded = true)
                    String type,
            @Query(value = "key", encoded = true)
                    String key);


    @GET("place/textsearch/json?")
    Call<Restaurant_output_pojo> searchRestaurants_reivew(
            @Query(value = "query", encoded = true)
                    String query,
            @Query(value = "location", encoded = true)
                    String location,
            @Query(value = "radius", encoded = true)
                    String radius,
            @Query(value = "type", encoded = true)
                    String type,
            @Query(value = "key", encoded = true)
                    String key);





    @GET("place/photo?")
    Call<Restaurant_output_pojo> places_photo(
            @Query(value = "maxwidth", encoded = true)
                    int maxwidth,
            @Query(value = "photoreference", encoded = true)
                    String photoreference,
            @Query(value = "key", encoded = true)
                    String key);


    //Location based search
    @GET("place/textsearch/json?")
    Call<Restaurant_output_pojo> location_search(
            @Query(value = "query", encoded = true)
                    String type,
            @Query(value = "key", encoded = true)
                    String key);


    //Location based search next
    @GET("place/textsearch/json?")
    Call<Restaurant_output_pojo> nextlocation(
            @Query(value = "query", encoded = true)
                    String type,
            @Query(value = "key", encoded = true)
                    String key,
            @Query(value = "pagetoken", encoded = true)
                    String next_page_token);

    @GET("place/nearbysearch/json?")
    Call<Restaurant_output_pojo> searchPlacesType(
            @Query(value = "type", encoded = true)
                    String type,
            @Query(value = "name", encoded = true)
                    String name,
            @Query(value = "opennow", encoded = true)
                    boolean opennow,
            @Query(value = "rankby", encoded = true)
                    String rankby,
            @Query(value = "key", encoded = true)
                    String key);

    @GET("place/nearbysearch/json?")
    Call<Restaurant_output_pojo> searchPlaces(
            @Query(value = "location", encoded = true)
                    String location,
            @Query(value = "opennow", encoded = true) boolean opennow,

            @Query(value = "rankby", encoded = true)
                    String rankby,
            @Query(value = "key", encoded = true)
                    String key);


    @GET("distancematrix/json")
     // origins/destinations:  LatLng as string
    Call<ResultDistanceMatrix> getDistance(@Query("key") String key,
                                           @Query("origins") String origins,
                                           @Query("destinations") String destinations);

    @GET("place/queryautocomplete/json?")
    Call<PlaceSearchDetails> searchRestaurantsReviewWithnearbyAutoComplete(
            @Query(value = "key", encoded = true)
                    String key,
            @Query(value = "input", encoded = true)
                    String query);
}
