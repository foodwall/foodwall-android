package com.kaspontech.foodwall.chatpackage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.adapters.chat.Chat_muliti_view_adapter;
import com.kaspontech.foodwall.fcm.Constants;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_details_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_history_single_details;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_history_single_output;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_typing_output_pojo;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.profilePackage.Image_self_activity;
import com.kaspontech.foodwall.profilePackage.ReviewSinglePage;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.questionspackage.ViewQuestionAnswer;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class chatInsideActivity extends _SwipeActivityClass implements View.OnClickListener, LocationListener, Chat_muliti_view_adapter.onRedirectPageListener {

    /**
     * Application TAG
     **/
    private static final String TAG = "chatInsideActivity";

    /**
     * ActionBar
     **/
    Toolbar actionBar;

    /**
     * Chat active Text view
     **/
    TextView chat_active;

    /**
     * No chat's textview
     **/

    public static TextView txt_no_chats;
    /**
     * Image button back
     **/
    ImageButton back;
    /**
     * Image button send
     **/
    ImageButton imgSend;
    /**
     * Edit text chat single
     **/
    EditText edittxtChatSingle;
    /**
     * Image view user photo
     **/
    ImageView userphotoChat;

    /**
     * Image view chat user photo
     **/

    ImageView imgChatUser;
    /**
     * Recycler view single chat
     **/
    RecyclerView rvSingleChat;

    /**
     * Linear layout manager
     **/

    LinearLayoutManager llm;

    /**
     * Group chat history details pojo
     **/

    Chat_history_single_details chatHistorySingleDetails;

    /**
     * Group chat history details array list
     **/

    ArrayList<Chat_history_single_details> getChatHistoryDetailsPojoArrayList = new ArrayList<>();

    ArrayList<Chat_history_single_details> getChatHistoryDetailsPojoArrayList_updated = new ArrayList<>();


    /**
     * int variables
     **/
    int user_ID;
    int groupid;
    int sessionid;
    int sessionid_reply;
    int friendid;
    int active_status;

    /**
     * String variables - new friend id
     **/
    String newfriendid, search_chat, redirect_url;

    /**
     * loader
     **/

    ProgressBar progressChatSingle;

    /**
     * Chat muti view adapter
     **/

    Chat_muliti_view_adapter chatMulitiViewAdapter;

    /**
     * Chat details pojo
     **/

    Chat_details_pojo chatDetailsPojo;

    String touserid;
    String picture;
    String groupname;
    String group_createdon;
    String topicture;
    String username;
    String toonline;
    String lastseen;
    String single_createdon;
    String single_createddate;
    String encodedEdittext;
    String imei;
    String created_by;

    /**
     * Edit text for emoji
     **/
    EmojiconEditText emojiconEditText;
    EmojiconTextView chatUsername;
    EmojIconActions emojIcon;

    /**
     * View
     **/
    View rl_single_chat;


    EmojiconTextView emojicon_text_view;

    String myStringRedirectId;
    /**
     * Online checking
     **/

    TelephonyManager telephonyManager;
    LocationManager locationManager;
    Location location;
    RelativeLayout rl_active;
    /**
     * Latitude & Longitude
     **/

    double latitude_online;
    double longitude_online;
    /**
     * Handler's
     **/
    Handler mHandler;

    TimerTask timerTask;

    Timer timer;


    @SuppressLint("HardwareIds")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_chat);

        touserid = Pref_storage.getDetail(getApplicationContext(), "userId");

        /*Widgets initialization*/
        actionBar = findViewById(R.id.actionbar_single_chat);
        back = actionBar.findViewById(R.id.back);
        chatUsername = actionBar.findViewById(R.id.chat_username);
        chat_active = actionBar.findViewById(R.id.chat_active);
        txt_no_chats = actionBar.findViewById(R.id.txt_no_chats);


        imgSend = findViewById(R.id.img_send);
        back.setOnClickListener(this);
        imgSend.setOnClickListener(this);

        userphotoChat = findViewById(R.id.userphoto_chat);
        imgChatUser = actionBar.findViewById(R.id.img_chat_user);
        userphotoChat.setOnClickListener(this);


        rl_single_chat = findViewById(R.id.rl_single_chat);

        rvSingleChat = findViewById(R.id.rv_single_chat);
        rl_active = findViewById(R.id.rl_active);
        rl_active.setOnClickListener(this);

        progressChatSingle = findViewById(R.id.progress_chat_single);


        emojiconEditText = findViewById(R.id.emojicon_edit_text);
        emojiconEditText.setImeOptions(EditorInfo.IME_ACTION_GO);


        emojIcon = new EmojIconActions(chatInsideActivity.this, rl_single_chat, emojiconEditText, userphotoChat);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_keyboard_grey, R.drawable.ic_smiley_green_24dp);

        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e(TAG, "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
                Log.e(TAG, "Keyboard closed");
            }
        });

        edittxtChatSingle = findViewById(R.id.edittxt_chat_single);

        /* Real time typing status */
        emojiconEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (emojiconEditText.getText().toString().length() != 0) {
                    imgSend.setVisibility(View.VISIBLE);
                    /*Calling typing status on api*/

                    updateChatTypingStatusOn();
                } else {
                    imgSend.setVisibility(View.GONE);

                    /*Calling typing status off api*/

                    updateChatTypingStatusOff();
                }
            }
        });

        /* IME action send */
        emojiconEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_GO) {

                    encodedEdittext = org.apache.commons.text.StringEscapeUtils.escapeJava(emojiconEditText.getText().toString().trim());


                    if (encodedEdittext.length() == 0) {
                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();

                    }/*else  if(encodedEdittext.length() > 0){

                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();
                    }*/ else {
                        /* Creating new chat API */
                        createIndividualChat();
                    }


                }
                return false;
            }
        });

        /* Getting input values */
        Intent intent = getIntent();

        user_ID = Integer.parseInt(intent.getStringExtra("userid"));
        groupid = Integer.parseInt(intent.getStringExtra("groupid"));
        sessionid = Integer.parseInt(intent.getStringExtra("sessionid"));
        created_by = intent.getStringExtra("created_by");
        Log.e("user_ID", "user_ID-->" + user_ID);
        Log.e("user_ID", "groupid-->" + groupid);
        Log.e("user_ID", "sessionid-->" + sessionid);

        groupname = intent.getStringExtra("groupname");
        group_createdon = intent.getStringExtra("groupCreatedon");


        username = intent.getStringExtra("username");
        newfriendid = intent.getStringExtra("friendid");
        friendid = Integer.parseInt(intent.getStringExtra("friendid"));
        active_status = Integer.parseInt(intent.getStringExtra("active_status"));
        Log.e("friendid", "friendid" + friendid);

        picture = intent.getStringExtra("picture");
        topicture = intent.getStringExtra("topicture");
        single_createdon = intent.getStringExtra("single_createdon");
        single_createddate = intent.getStringExtra("single_createddate");
        search_chat = intent.getStringExtra("search_chat");
        redirect_url = intent.getStringExtra("redirect_url");

        userphotoChat.setImageResource(R.drawable.ic_greateorange);


        //To picture displaying
        Utility.picassoImageLoader(topicture,
                0, imgChatUser, getApplicationContext());


        try {
            if (redirect_url.length() != 0)
                emojiconEditText.setText(redirect_url);
            else
                emojiconEditText.setText("");
        } catch (Exception e) {
            e.printStackTrace();
        }

      //Chat history continuous loop
        /*
        mHandler =new Handler();
        runnable=new Runnable() {
            @Override
            public void run() {

               getChatHistorySingleChat();

                getChatTypingStatus();

                try {
                    if (Integer.parseInt(toonline) == 1) {
                        chatActive.setText(R.string.active_now);
                    } else if (Integer.parseInt(toonline) == 0) {
                        lastvisited(lastseen);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                mHandler.postDelayed(runnable,500);
            }
        };*/



        /* Chat history API */
        getChatHistorySingle();


        mHandler = new Handler();
        timer = new Timer();

        timerTask = new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    public void run() {
                        try {

                            getChatHistorySingleChat();
                            getChatTypingStatus();

                            try {
                                if (Integer.parseInt(toonline) == 1) {
                                    // chat_active.setText(R.string.active_now);
                                } else if (Integer.parseInt(toonline) == 0) {
                                    //lastvisited(lastseen);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } catch (Exception e) {
                            // error, do something
                        }
                    }
                });
            }
        };

        timer.schedule(timerTask, 0, 1000);  // interval of one minute


        if (toonline != null) {
            if (Integer.parseInt(toonline) == 1) {
                // chat_active.setText(R.string.active_now);
            } else if (Integer.parseInt(toonline) == 0) {
                lastvisited(lastseen);
            }
        } else {


        }


        //Setting user name
        chatUsername.setText(username);


        //Getting IMIE number


        telephonyManager = (TelephonyManager) chatInsideActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(chatInsideActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        imei = Settings.Secure.getString(chatInsideActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);

    }

    @Override
    public void onSwipeRight() {
        finish();
    }

    @Override
    protected void onSwipeLeft() {

    }


    //Chat last visited
    public void lastvisited(String lastseen) {


        try {
            //Input time
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long time = sdf.parse(lastseen).getTime();

            //Cuurent system time

            Date currentTime = Calendar.getInstance().getTime();
            SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String now_new = sdformat.format(currentTime);
            long nowdate = sdformat.parse(now_new).getTime();

//            String timeAgo = com.kaspontech.foodwall.Utills.TimeAgo.getTimeAgo_new(time,nowdate);


            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(time, nowdate, DateUtils.FORMAT_ABBREV_RELATIVE);

            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago") || relavetime1.toString().equalsIgnoreCase("In 0 minutes")) {
                // chat_active.setText(R.string.just_now);

            } else if (relavetime1.toString().contains("hours ago")) {
                //  chat_active.setText("Last seen " + relavetime1.toString());
            } else if (relavetime1.toString().contains("days ago")) {
                //  chat_active.setText("Last seen " + relavetime1.toString());
            } else {
                // chat_active.setText(relavetime1);
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
       /* try {
            long now = System.currentTimeMillis();

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convertedDate = dateFormat.parse(lastseen);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now, DateUtils.FORMAT_ABBREV_RELATIVE);

            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                chatActive.setText(R.string.just_now);

            } else if (relavetime1.toString().contains("hours ago")) {
                chatActive.setText("Last seen " + relavetime1.toString().replace("hours ago", "").concat("h"));
            } else if (relavetime1.toString().contains("days ago")) {
                chatActive.setText("Last seen " + relavetime1.toString().replace("days ago", "").concat("dialog"));
            } else {
                chatActive.setText(relavetime1);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }*/

    }


    /* Real time typing status API */
    private void getChatTypingStatus() {
        if (Utility.isConnected(chatInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<Get_chat_typing_output_pojo> call = apiService.get_chat_typing_status("get_chat_typing_status", sessionid, userid);
                call.enqueue(new Callback<Get_chat_typing_output_pojo>() {
                    @Override
                    public void onResponse(Call<Get_chat_typing_output_pojo> call, Response<Get_chat_typing_output_pojo> response) {

                        if (response.body() != null) {
                            if (response.body().getResponseCode() == 1 && response.body().getData().get(0).getYourFirstname() != null) {
                                String userid = response.body().getData().get(0).getTypeYourid();
                                String username = response.body().getData().get(0).getYourFirstname().concat(" ").concat(response.body().getData().get(0).getYourLastname());


                                if (userid.equalsIgnoreCase(Pref_storage.getDetail(getApplicationContext(), "userId"))) {

                                } else {
                                    // chat_active.setText(R.string.typing);
                                }

                            } else {

                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Get_chat_typing_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rl_single_chat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }


    //Update real time typing status ON API

    private void updateChatTypingStatusOn() {
        if (Utility.isConnected(chatInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<CommonOutputPojo> call = apiService.update_chat_typing_status("update_chat_typing_status", sessionid, userid, 1);
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getData() == 1) {

                            Log.e("User typing", "User typing");
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rl_single_chat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    //Update real time typing status OFF API
    private void updateChatTypingStatusOff() {
        if (Utility.isConnected(chatInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<CommonOutputPojo> call = apiService.update_chat_typing_status("update_chat_typing_status", sessionid, userid, 0);
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getData() == 1) {
                            Log.e("User not typing", "User not typing");
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rl_single_chat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    //Updating chat history
    private void getChatHistorySingle() {

        if (Utility.isConnected(chatInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<Chat_history_single_output> call = apiService.get_chat_history_single("get_chat_history_single",
                        userid, groupid, sessionid);
                call.enqueue(new Callback<Chat_history_single_output>() {
                    @Override
                    public void onResponse(Call<Chat_history_single_output> call, Response<Chat_history_single_output> response) {

                        if (response.body().getResponseCode() == 1) {
                            getChatHistoryDetailsPojoArrayList.clear();

                            for (int j = 0; j < response.body().getData().size(); j++) {
                                progressChatSingle.setVisibility(View.GONE);

                                String chatid = response.body().getData().get(j).getChatid();
                                String sessionid = response.body().getData().get(j).getSessionid();

                                sessionid_reply = Integer.parseInt(sessionid);

                                String fromuserid = response.body().getData().get(j).getFromuserid();
                                String group_id = response.body().getData().get(j).getGroupid();

                                groupid = Integer.parseInt(group_id);
                                toonline = response.body().getData().get(j).getToOnlineStatus();
                                String tolastvisited = response.body().getData().get(j).getToLastvisited();
                                String created_on = response.body().getData().get(j).getCreatedOn();
                                String message = response.body().getData().get(j).getMessage();
                                String status = response.body().getData().get(j).getStatus();

                                String response_2 = response.body().getData().get(j).getResponse();
                                String msgdate = response.body().getData().get(j).getMsgDate();
                                lastseen = response.body().getData().get(j).getLastseen();
                                String from_firstname = response.body().getData().get(j).getFromFirstname();
                                String from_lastname = response.body().getData().get(j).getFromLastname();
                                String from_picture = response.body().getData().get(j).getFromPicture();
                                String to_firstname = response.body().getData().get(j).getToFirstname();
                                String to_lastname = response.body().getData().get(j).getToLastname();
                                String to_picture = response.body().getData().get(j).getToPicture();
                                String group_name = response.body().getData().get(j).getGroupName();

                                String group_createdby = response.body().getData().get(j).getGroupCreatedby();
                                String created_date = response.body().getData().get(j).getCreatedDate();


                                String type_message = response.body().getData().get(j).getTypeMessage();
                                String type_status = response.body().getData().get(j).getTypeStatus();
                                String group_icon = response.body().getData().get(j).getGroupIcon();
                                String changed_which = response.body().getData().get(j).getChangedWhich();
                                String reviewid = response.body().getData().get(j).getReviewid();
                                String timelineid = response.body().getData().get(j).getTimelineId();
                                String deleted_message = response.body().getData().get(j).getDeleted_message();


                                chatHistorySingleDetails = new Chat_history_single_details(chatid,
                                        sessionid, group_id, fromuserid, touserid,
                                        message, type_message, status, response_2, msgdate, created_on, lastseen,
                                        reviewid, timelineid, from_firstname, from_lastname, from_picture, to_firstname,
                                        to_lastname, to_picture, toonline,
                                        tolastvisited, group_name, group_icon, changed_which, group_createdby, created_date, "", "", "",
                                        type_status, "", "", "", "", "", deleted_message);


                                if (deleted_message.equalsIgnoreCase("0")) {
                                    getChatHistoryDetailsPojoArrayList.add(chatHistorySingleDetails);
                                } else {

                                }

                            }

                            Pref_storage.setDetail(getApplicationContext(), "Chat_single_size", String.valueOf(getChatHistoryDetailsPojoArrayList.size()));
                            chatMulitiViewAdapter = new Chat_muliti_view_adapter(chatInsideActivity.this, getChatHistoryDetailsPojoArrayList/*,chatDetailsPojoArrayList*/, chatInsideActivity.this);
                            llm = new LinearLayoutManager(chatInsideActivity.this);
                            rvSingleChat.setLayoutManager(llm);
                            llm.setStackFromEnd(true);
                            rvSingleChat.setHasFixedSize(true);
                            rvSingleChat.setItemAnimator(new DefaultItemAnimator());
                            rvSingleChat.setAdapter(chatMulitiViewAdapter);
                            rvSingleChat.smoothScrollToPosition(getChatHistoryDetailsPojoArrayList.size() + 1);
                            rvSingleChat.setNestedScrollingEnabled(false);
                            chatMulitiViewAdapter.notifyDataSetChanged();
                        }

                    }

                    @Override
                    public void onFailure(Call<Chat_history_single_output> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rl_single_chat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }


    //createIndividualChat

    private void createIndividualChat() {

        if (Utility.isConnected(chatInsideActivity.this)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");


                Call<Chat_output_pojo> call = apiService.create_individual_chat("create_individual_chat", Integer.parseInt(createdby), friendid, encodedEdittext);
                call.enqueue(new Callback<Chat_output_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Chat_output_pojo> call, @NonNull Response<Chat_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {
                            emojiconEditText.setText("");


                            for (int j = 0; j < response.body().getData().size(); j++) {

                                chatDetailsPojo = new Chat_details_pojo("", "", "", "", "", 0, "");


                                sessionid_reply = response.body().getData().get(j).getSessionid();


                                Log.e(TAG, "onResponse: " + sessionid_reply);

                               /*     String groupid=response.body().getData().get(j).getGroupid();
                                    String fromuserid=response.body().getData().get(j).getFromuserid();
                                    String touserid=response.body().getData().get(j).getTouserid();
                                    String message=response.body().getData().get(j).getMessage();
                                    String msg_date=response.body().getData().get(j).getMsgDate();
                                    String status=response.body().getData().get(j).getStatus();
                                    String created_on=response.body().getData().get(j).getCreatedOn();
                                    String lastseen=response.body().getData().get(j).getLastseen();
                                    String myself_firstname=response.body().getData().get(j).getMyselfFirstname();
                                    String myself_lastname=response.body().getData().get(j).getMyselfLastname();
                                    String friend_firstname=response.body().getData().get(j).getFriendFirstname();
                                    String friend_lastname=response.body().getData().get(j).getFriendLastname();
                                    String response_new=response.body().getData().get(j).getResponse();


                                    Chat_details_pojo chatDetailsPojo_result= new Chat_details_pojo(chat_id,sessionid,groupid,
                                            fromuserid,touserid,message,msg_date,
                                            status,response_new,created_on,lastseen,
                                            myself_firstname,myself_lastname,friend_firstname,friend_lastname);
                                    chatDetailsPojoArrayList.add(chatDetailsPojo_result);

    */
                                /*Reload chat history single api*/
                                getChatHistorySingleChat();


                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Chat_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Snackbar snackbar = Snackbar.make(rl_single_chat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }




    /* Real time chat history update */

    private void getChatHistorySingleChat() {

        if (Utility.isConnected(chatInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<Chat_history_single_output> call = apiService.get_chat_history_single("get_chat_history_single", userid, groupid, sessionid);
                call.enqueue(new Callback<Chat_history_single_output>() {
                    @Override
                    public void onResponse(Call<Chat_history_single_output> call, Response<Chat_history_single_output> response) {
                        if (response.body() != null) {

                            try {
                                if (response.body().getData().size() <= (Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "Chat_single_size")))) {

                                } else if (response.body().getData().size() > (Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "Chat_single_size")))) {


                                    Pref_storage.setDetail(getApplicationContext(), "Chat_single_size", String.valueOf(response.body().getData().size()));


                                    getChatHistoryDetailsPojoArrayList.clear();

                                    for (int j = 0; j < response.body().getData().size(); j++) {

                                        String chatid = response.body().getData().get(j).getChatid();
                                        String sessionid = response.body().getData().get(j).getSessionid();

                                        sessionid_reply = Integer.parseInt(sessionid);

                                        String fromuserid = response.body().getData().get(j).getFromuserid();
                                        touserid = response.body().getData().get(j).getTouserid();
                                        String group_id = response.body().getData().get(j).getGroupid();
                                        groupid = Integer.parseInt(group_id);

                                        String created_on = response.body().getData().get(j).getCreatedOn();
                                        String message = response.body().getData().get(j).getMessage();
                                        String status = response.body().getData().get(j).getStatus();
                                        String response_2 = response.body().getData().get(j).getResponse();
                                        String msgdate = response.body().getData().get(j).getMsgDate();
                                        lastseen = response.body().getData().get(j).getLastseen();

                                        String from_firstname = response.body().getData().get(j).getFromFirstname();
                                        String from_lastname = response.body().getData().get(j).getFromLastname();
                                        String from_picture = response.body().getData().get(j).getFromPicture();
                                        String to_firstname = response.body().getData().get(j).getToFirstname();
                                        String to_lastname = response.body().getData().get(j).getToLastname();
                                        String to_picture = response.body().getData().get(j).getToPicture();
                                        String group_name = response.body().getData().get(j).getGroupName();
                                        String group_createdby = response.body().getData().get(j).getGroupCreatedby();
                                        String created_date = response.body().getData().get(j).getCreatedDate();
                                        toonline = response.body().getData().get(j).getToOnlineStatus();

                                        String tolastvisited = response.body().getData().get(j).getToLastvisited();


                                        String type_message = response.body().getData().get(j).getTypeMessage();
                                        String type_status = response.body().getData().get(j).getTypeStatus();
                                        String group_icon = response.body().getData().get(j).getGroupIcon();
                                        String changed_which = response.body().getData().get(j).getChangedWhich();
                                        String reviewid = response.body().getData().get(j).getReviewid();
                                        String timelineid = response.body().getData().get(j).getTimelineId();
                                        String deleted_message = response.body().getData().get(j).getDeleted_message();

                                        chatHistorySingleDetails = new Chat_history_single_details(chatid,
                                                sessionid, group_id, fromuserid, touserid,
                                                message, type_message, status, response_2, msgdate, created_on, lastseen,
                                                reviewid, timelineid, from_firstname, from_lastname, from_picture, to_firstname,
                                                to_lastname, to_picture, toonline,
                                                tolastvisited, group_name, group_icon, changed_which, group_createdby, created_date, "", "", "",
                                                type_status, "", "", "", "", "", deleted_message
                                        );
                                        if (deleted_message.equalsIgnoreCase("0")) {
                                            getChatHistoryDetailsPojoArrayList.add(chatHistorySingleDetails);
                                        }
                                    }

                                    /*Setting adapter*/

                                    chatMulitiViewAdapter = new Chat_muliti_view_adapter(chatInsideActivity.this, getChatHistoryDetailsPojoArrayList, chatInsideActivity.this);
                                    llm = new LinearLayoutManager(chatInsideActivity.this);
                                    rvSingleChat.setLayoutManager(llm);
                                    llm.setStackFromEnd(true);
                                    rvSingleChat.setHasFixedSize(true);
                                    rvSingleChat.setItemAnimator(new DefaultItemAnimator());
                                    rvSingleChat.setAdapter(chatMulitiViewAdapter);
                                    rvSingleChat.smoothScrollToPosition(getChatHistoryDetailsPojoArrayList.size() + 1);
                                    rvSingleChat.setNestedScrollingEnabled(false);
                                    chatMulitiViewAdapter.notifyDataSetChanged();

                                } else {

                                    Snackbar snackbar = Snackbar.make(rl_single_chat, "Loading failed. Please try again later.", Snackbar.LENGTH_LONG);
                                    snackbar.setActionTextColor(Color.RED);
                                    View view1 = snackbar.getView();
                                    TextView textview = view1.findViewById(R.id.snackbar_text);
                                    textview.setTextColor(Color.WHITE);
                                    snackbar.show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    }

                    @Override
                    public void onFailure(Call<Chat_history_single_output> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rl_single_chat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(chatInsideActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRedirectPage(View view, int adapterPosition, View viewItem) {

        if (Integer.parseInt(getChatHistoryDetailsPojoArrayList.get(adapterPosition).getTypeMessage()) == 1) {

            switch (view.getId()) {

                case R.id.emojicon_text_view:

                    String sharePageRedirect = Pref_storage.getDetail(chatInsideActivity.this, "shareView");

                    if (sharePageRedirect != null) {

                        if (sharePageRedirect.equals(Constants.TimeLineView)) {

                            Intent intent = new Intent(new Intent(chatInsideActivity.this, Home.class));
                            startActivity(intent);
                            finish();

                        } else if (sharePageRedirect.equals(Constants.Reviews)) {

                            String allFRagView = Pref_storage.getDetail(chatInsideActivity.this, "shareViewPage");

                            if (allFRagView != null) {

                                if (allFRagView.equals(Constants.All)) {

                                    Intent intent = new Intent(new Intent(chatInsideActivity.this, Home.class));
                                    intent.putExtra("redirect", "from_review");
                                    intent.putExtra("redirectView", "All_View");
                                    startActivity(intent);
                                    finish();

                                } else if (allFRagView.equals(Constants.NewDineIN)) {

                                    Intent intent = new Intent(new Intent(chatInsideActivity.this, Home.class));
                                    intent.putExtra("redirect", "from_review");
                                    intent.putExtra("redirectView", "NewDinein_View");
                                    startActivity(intent);
                                    finish();
                                } else if (allFRagView.equals(Constants.DeliveryView)) {

                                    Intent intent = new Intent(new Intent(chatInsideActivity.this, Home.class));
                                    intent.putExtra("redirect", "from_review");
                                    intent.putExtra("redirectView", "delivery_View");
                                    startActivity(intent);
                                    finish();

                                }
                            } else {


                                Intent intent = new Intent(new Intent(chatInsideActivity.this, Home.class));
                                intent.putExtra("redirect", "from_review");
                                intent.putExtra("redirectView", "All_View");
                                startActivity(intent);
                                finish();

                            }

                        } else if (sharePageRedirect.equals(Constants.questionAnswer)) {

                            Intent intent = new Intent(new Intent(chatInsideActivity.this, Home.class));
                            intent.putExtra("redirect", "qa");
                            startActivity(intent);
                            finish();
                        }
                    }
                    break;

            }

        } else {


            emojicon_text_view = (EmojiconTextView) viewItem.findViewById(R.id.emojicon_text_view);
            myStringRedirectId = emojicon_text_view.getText().toString();
            String viewText1 = myStringRedirectId.substring(0, myStringRedirectId.length() - 1);
            Log.e("View","------------------> " + viewText1);

            if (viewText1.equals(Constants.redirectView) ||
                myStringRedirectId.substring(0, myStringRedirectId.length() - 2).equals(Constants.redirectView) ||
                    myStringRedirectId.substring(0, myStringRedirectId.length() - 3).equals(Constants.redirectView)) {

                String timeLineIdView = myStringRedirectId.substring(myStringRedirectId.lastIndexOf("/") + 1);
                Intent intent = new Intent(chatInsideActivity.this, Image_self_activity.class);
                intent.putExtra("Clickedfrom_profile", timeLineIdView);
                intent.putExtra("Clickedfrom_profileuserid", touserid);
                startActivity(intent);


            } else if (viewText1.equals(Constants.qaredirectView) ||
                    myStringRedirectId.substring(0, myStringRedirectId.length() - 2).equals(Constants.qaredirectView) ||
                    myStringRedirectId.substring(0, myStringRedirectId.length() - 3).equals(Constants.qaredirectView)) {

                String timeLineIdView = myStringRedirectId.substring(myStringRedirectId.lastIndexOf("/") + 1);
                Intent intent = new Intent(chatInsideActivity.this, ViewQuestionAnswer.class);
                intent.putExtra("quesId", timeLineIdView);
                startActivity(intent);

            } else {

                String timeLineIdView = myStringRedirectId.substring(myStringRedirectId.lastIndexOf("/") + 1);
                Intent intent = new Intent(chatInsideActivity.this, ReviewSinglePage.class);
                intent.putExtra("reviewId", timeLineIdView);
                intent.putExtra("userId", touserid);
                startActivity(intent);
            }

        }
    }


    //GetRunningAppProcesses
    boolean isNamedProcessRunning(String processName) {
        if (processName == null)
            return false;

        ActivityManager manager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processes = manager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo process : processes) {
            if (processName.equals(process.processName)) {
                return true;
            }
        }
        return false;
    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            return (mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting());
        } else
            return false;
    }


    @Override
    public void onClick(View v) {

        int newid = v.getId();

        switch (newid) {
            case R.id.img_send:
                //Send button Animation


                if (groupid != 0) {
                    //Starting animation for send click

                    final Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.modal_in);
                    v.startAnimation(anim);
                    encodedEdittext = org.apache.commons.text.StringEscapeUtils.escapeJava(emojiconEditText.getText().toString().trim());
                    Log.e(TAG, "onClick: length" + encodedEdittext.length());
                    Log.e(TAG, "onClick: contains" + encodedEdittext);

                    if (encodedEdittext.length() == 0) {
                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();

                    }/*else if(encodedEdittext.length() > 0 && encodedEdittext.contains("")){
                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();

                    }*/ else {
                        /* Creating new chat API */

                        createIndividualChat();

                    }

                } else {
                    //Starting animation for send click

                    final Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.modal_in);
                    v.startAnimation(anim);
                    encodedEdittext = org.apache.commons.text.StringEscapeUtils.escapeJava(emojiconEditText.getText().toString().trim());


                    Log.e(TAG, "onClick: length" + encodedEdittext.length());
                    Log.e(TAG, "onClick: contains" + encodedEdittext);

                    if (encodedEdittext.length() == 0) {
                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();

                    }/*else if(encodedEdittext.length() > 0 && encodedEdittext.contains("")){
                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();

                    }*/ else {

                        /* Creating new chat API */
                        createIndividualChat();
                    }

                }


                break;
            case R.id.back:

                mHandler.removeCallbacksAndMessages(null);

                timerTask.cancel();
                timer.cancel();
                timer.purge();

                finish();

                break;

            case R.id.userphoto_chat:


                break;

            case R.id.rl_active:

                Intent intent = new Intent(chatInsideActivity.this, chatUserProfileActivity.class);
                intent.putExtra("pro_username", username);
                intent.putExtra("pro_picture", topicture);
                intent.putExtra("pro_friendid", newfriendid);
                intent.putExtra("pro_createdon", single_createdon);
                intent.putExtra("pro_createdby", single_createddate);
                intent.putExtra("groupid", String.valueOf(groupid));
                intent.putExtra("sessionid", String.valueOf(sessionid));
                intent.putExtra("userid", String.valueOf(user_ID));
                intent.putExtra("created_by", created_by);
                startActivity(intent);

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mHandler.removeCallbacksAndMessages(null);

        timerTask.cancel();

        timer.cancel();
        timer.purge();

        finish();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
        timerTask.cancel();
        timer.cancel();
        timer.purge();

    }
}
