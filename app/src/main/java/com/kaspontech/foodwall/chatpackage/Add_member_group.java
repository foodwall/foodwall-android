package com.kaspontech.foodwall.chatpackage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.chat.ChatMemberFollowerAdapter;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ChatAddMemberOutput;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ChatAddmemberInput;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ChatAddmemberInputPojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ChatAddmemberOutputPojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_member_group extends AppCompatActivity implements View.OnClickListener {

    /**
     * Application TAG
     **/
    private static final String TAG = "Add_member_group";

    /**
     * Action bar
     **/
    Toolbar actionBar;

    /**
     * Text view widget
     **/
    TextView toolbarTitle;
    TextView toolbarNext;
    TextView txtNoMember;
    TextView txtNoDish;
    /**
     * Image button widget
     **/
    ImageButton back;
    ImageButton imgChatAdd;
    ImageButton followerNext;


    /**
     * Relative layout widget
     **/
    RelativeLayout rlFollowerlayChat;
    RecyclerView rvFollowersChat;

    /**
     * Loader
     **/
    ProgressBar progressChatFollower;

    /**
     *String variable's
     **/

    String memberGrpname;

    String memberGrpIcon;

    String memberGrpid;

    String memberGrpSessionid;

    int friendid;

    /**
     * Chat add member input pojo
     **/

    ChatAddmemberInput chatAddmemberInput;

    /**
     * Chat add member input pojo
     **/
    ChatAddmemberInputPojo chatAddmemberInputPojo;

    /**
     * Chat add member input pojo array list
     **/

    ArrayList<ChatAddmemberInput> chatAddmemberInputArrayList = new ArrayList<>();


    /**
     * Chat add member input pojo array list
     **/

    ArrayList<ChatAddmemberInputPojo> getChatFollowerPojoArrayList = new ArrayList<>();


    ArrayList<Integer> clickedfollowerlistid = new ArrayList<>();

    HashMap<String, Integer> selectedfollowerlistId = new HashMap<>();


    /**
     * Chat add member adapter
     **/

    ChatMemberFollowerAdapter chatMemberFollowerAdapter;


    /**
     * No data layout
     **/

    LinearLayout llNodata;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_followers_list);

        /*Widget initialization*/

        actionBar = findViewById(R.id.actionbar_chat_follower);

        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        llNodata = findViewById(R.id.ll_nodata);

        followerNext = actionBar.findViewById(R.id.follower_next);
        followerNext.setOnClickListener(this);

        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(R.string.add_new_members);

        toolbarNext = actionBar.findViewById(R.id.toolbar_next);

//        txtNoDish = findViewById(R.id.txt_no_dish);
//        txtNoDish.setText(R.string.no_data_avialable);

        toolbarNext.setOnClickListener(this);

        rlFollowerlayChat = findViewById(R.id.rl_followerlay_chat);
        progressChatFollower = findViewById(R.id.progress_chat_follower);

        rvFollowersChat = findViewById(R.id.rv_followers_chat);


        /*Getting variable's input from various adapters*/

        Intent intent = getIntent();
        memberGrpname = intent.getStringExtra("member_grpname");

        memberGrpIcon = intent.getStringExtra("member_grp_icon");

        memberGrpid = intent.getStringExtra("member_grpid");

        memberGrpSessionid = intent.getStringExtra("member_grp_sessionid");



        /*Chat friend group calling api*/

        getChatFriendGroup();


    }


    /*Chat friend group calling api*/

    private void getChatFriendGroup() {

        if (Utility.isConnected(Add_member_group.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String userId = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<ChatAddmemberOutputPojo> call = apiService.get_chat_friend_group("get_chat_friend_group", Integer.parseInt(memberGrpid), Integer.parseInt(userId));
                call.enqueue(new Callback<ChatAddmemberOutputPojo>() {
                    @Override
                    public void onResponse(@NonNull Call<ChatAddmemberOutputPojo> call, @NonNull Response<ChatAddmemberOutputPojo> response) {
                        if (response.body() != null) {
                            if (response.body().getData() == null) {
                                txtNoMember.setVisibility(View.VISIBLE);
                                progressChatFollower.setVisibility(View.GONE);
                            } else if (response.body().getResponseCode() == 1 && response.body().getData() != null) {

                                for (int j = 0; j < response.body().getData().size(); j++) {


                                    String friendId = response.body().getData().get(j).getFriendid();

                                    String txtUsername = response.body().getData().get(j).getUsername();

                                    String txtFname = response.body().getData().get(j).getFirstName();
                                    String txtLname = response.body().getData().get(j).getLastName();
                                    String picture = response.body().getData().get(j).getPicture();


                                    chatAddmemberInputPojo = new ChatAddmemberInputPojo(friendId, txtFname, txtLname,
                                            txtUsername, picture);


                                    getChatFollowerPojoArrayList.add(chatAddmemberInputPojo);


                                    /*Setting adapter*/

                                    chatMemberFollowerAdapter = new ChatMemberFollowerAdapter(Add_member_group.this, getChatFollowerPojoArrayList, selectedfollowerlistId);
                                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(Add_member_group.this);
                                    rvFollowersChat.setLayoutManager(mLayoutManager);
                                    rvFollowersChat.setHasFixedSize(true);
                                    rvFollowersChat.setItemAnimator(new DefaultItemAnimator());
                                    rvFollowersChat.setAdapter(chatMemberFollowerAdapter);
                                    rvFollowersChat.setNestedScrollingEnabled(false);
                                    chatMemberFollowerAdapter.notifyDataSetChanged();
                                    rvFollowersChat.setVisibility(View.VISIBLE);
                                    progressChatFollower.setVisibility(View.GONE);

                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<ChatAddmemberOutputPojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        llNodata.setVisibility(View.VISIBLE);
                        progressChatFollower.setVisibility(View.GONE);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Snackbar snackbar = Snackbar.make(rlFollowerlayChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }




    /* Interface for group selecting */

    public void getgrpmemberList(View view, int position) {


        String followerName;
        followerName = getChatFollowerPojoArrayList.get(position).getFirstName() + " " + getChatFollowerPojoArrayList.get(position).getLastName();


        if (((CheckBox) view).isChecked()) {

            if (selectedfollowerlistId.containsValue(Integer.valueOf(getChatFollowerPojoArrayList.get(position).getFriendid()))) {

                return;

            } else {

                selectedfollowerlistId.put(followerName, Integer.valueOf(getChatFollowerPojoArrayList.get(position).getFriendid()));

//                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                clickedfollowerlistid.add(Integer.valueOf(getChatFollowerPojoArrayList.get(position).getFriendid()));


                Log.e("Add_member", "getSelectedfrd:" + selectedfollowerlistId.toString());
                Log.e("Add_member", "frdlistid:" + clickedfollowerlistid.toString());

//                followerlistid.add(userid);

            }


        } else {
            clickedfollowerlistid.remove(Integer.valueOf(getChatFollowerPojoArrayList.get(position).getFriendid()));
            selectedfollowerlistId.remove(followerName);

            Log.e("remove_member", "getSelectedfrd:" + selectedfollowerlistId.toString());
            Log.e("remove_member", "frdlistid:" + clickedfollowerlistid.toString());

        }

    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (i) {

            case R.id.back:

                finish();
                break;


            case R.id.toolbar_next:


                if (clickedfollowerlistid.isEmpty()) {

                    Toast.makeText(this, "Atleast 1 member required to add in this group", Toast.LENGTH_SHORT).show();

                } else {

                    /* Create group chat api calling */

                    createGroupFriends();


                }
                break;


            case R.id.follower_next:
                startActivity(new Intent(Add_member_group.this, New_chat_activity.class));
                finish();
                break;

                default:
                    break;

        }


    }


    /* Create group chat api calling */

    private void createGroupFriends() {

        if (Utility.isConnected(Add_member_group.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<ChatAddMemberOutput> call = apiService.create_group_friends("create_group_friends", Integer.parseInt(memberGrpid), Integer.parseInt(createdby), clickedfollowerlistid);

                call.enqueue(new Callback<ChatAddMemberOutput>() {
                    @Override
                    public void onResponse(@NonNull Call<ChatAddMemberOutput> call, @NonNull Response<ChatAddMemberOutput> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {

                                String groupid = response.body().getData().get(j).getGroupid();

                                String userid = response.body().getData().get(j).getUserid();

                                chatAddmemberInput = new ChatAddmemberInput(groupid, userid);
                                chatAddmemberInputArrayList.add(chatAddmemberInput);


                                  /*  Intent intent= new Intent(Add_member_group.this,chatGroupInsideActivity.class);


                                    intent.putExtra("groupid",groupid);
                                    intent.putExtra("groupIcon",groupIcon);
                                    intent.putExtra("group_name",group_name);
                                    intent.putExtra("sessionid",sessionid);
                                    intent.putExtra("groupCreatedon",groupCreatedon);
                                    intent.putExtra("groupCreatedby",groupCreatedby);
                                    startActivity(intent);*/
                                finish();

                            }
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<ChatAddMemberOutput> call, @NonNull Throwable t) {
                        //Error
                        Log.e(TAG, "FailureError" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Snackbar snackbar = Snackbar.make(rlFollowerlayChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
