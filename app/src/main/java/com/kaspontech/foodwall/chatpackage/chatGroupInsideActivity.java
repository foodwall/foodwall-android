package com.kaspontech.foodwall.chatpackage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.adapters.chat.Chat_multi_view_group_adapter;
 import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_details_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_history_single_details;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_group_history_details;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_typing_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_group_frds_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_group_history_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_group_member_inside_pojo;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class chatGroupInsideActivity extends _SwipeActivityClass implements View.OnClickListener, LocationListener {
    /**
     * Application TAG
     **/
    private static final String TAG = "Chat_group_inside";
    /**
     * ActionBar
     **/
    Toolbar actionBar;
    /**
     * Active now Textview
     **/
    TextView chatActive;
    /**
     * No member Textview
     **/
    TextView txtChatNoMember;
    /**
     * New message Textview
     **/
    TextView txtNewMessage;
    /**
     * No chat's textview
     **/
    public static TextView txtNoChats;
    /**
     * Image button back
     **/
    ImageButton back;

    /*
    * Image button for bucket list/short cut
    * */

    ImageButton btnChatBucketList;
    /**
     * Image button send
     **/
    ImageButton imgSend;
    /**
     * Edit text chat single
     **/
    EditText edittxtChatSingle;
    /**
     * Image view user photo
     **/
    ImageView userphotoChat;
    /**
     * Image view chat user photo
     **/
    ImageView imgChatUser;
    /**
     * Active relative layout
     **/

    RelativeLayout rlActive;
    /**
     * Post layout
     **/
    RelativeLayout rlPostlayoutnew;
    /**
     * Recycler view single chat
     **/
    RecyclerView rvSingleChat;
    /**
     * Linear layout manager
     **/
    LinearLayoutManager llm;

    /**
     * Group chat history details pojo
     **/

    Get_chat_group_history_details getChatGroupHistoryDetails;

    /**
     * Group chat history details array list
     **/

    ArrayList<Get_chat_group_history_details> getChatHistoryDetailsPojoArrayList = new ArrayList<>();

    /**
     * Group chat history member inside details array list
     **/
    ArrayList<Get_group_member_inside_pojo> getchatgroupmemberlist = new ArrayList<>();

    /**
     * Group user details array list
     **/

    ArrayList<String> groupUserslist = new ArrayList<>();
    ArrayList<String> groupUsersIdlist = new ArrayList<>();

    ArrayList<Chat_history_single_details> getChatHistoryDetailsPojoArrayList_updated = new ArrayList<>();

    /**
     * int variables
     **/
    int user_ID;
    int groupid;
    int sessionid;
    int sessionidReply;
    int friendid;
    int activeStatus;

    /**
     * Loaders
     **/
    ProgressBar progressChatSingle;

    /**
     * Adapters
     **/
    Chat_multi_view_group_adapter chatMulitiViewAdapter;

    /**
     * Chat details pojo
     **/

    Chat_details_pojo chatDetailsPojo;

    /**
     * Chat details pojo
     **/

    String touserid;
    String created_by;
    String proGroupid;
    String picture;
    String groupname;
    String groupIcon;
    String groupCreatedon;
    String groupCreatedby;
    String proSessionid;
    String proCreatedby;
    String proCreatedon;
    String proPicture;
    String proUsername;
    String proCreatedbyname;
    String groupCreatedbyname;
    String topicture;
    String username;
    String toonline;
    String lastseen;
    String encodedEdittext;
    String decodedText;
    String newgroupid;
    String imei;
    String newfriendid;
    /**
     * Edit text for emoji
     **/
    EmojiconEditText emojiconEditText;

    EmojiconTextView chatUsername;

    EmojIconActions emojIcon;


    /**
     * View
     **/
    View rlSingleChat;
    View view;


    /**
     * Online checking
     **/

    TelephonyManager telephonyManager;
    LocationManager locationManager;
    Location location;
    /**
     * Latitude & Longitude
     **/

    double latitudeOnline;
    double longitudeOnline;

    /**
     * Handler's
     **/
    Handler mHandler;
    Timer timer;
    TimerTask timerTask;

    /**
     * boolean
     **/
    private boolean loading = true;

    int pastVisiblesItems;
    int visibleItemCount;
    int totalItemCount;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("HardwareIds")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_chat);

        /*Widgets initialization*/

        actionBar = findViewById(R.id.actionbar_single_chat);

        back = actionBar.findViewById(R.id.back);

        chatUsername = actionBar.findViewById(R.id.chat_username);

        chatActive = actionBar.findViewById(R.id.chat_active);

        txtChatNoMember = findViewById(R.id.txt_chat_no_member);

        txtNewMessage = findViewById(R.id.txt_new_message);

        txtNewMessage.setOnClickListener(this);

        txtNoChats = actionBar.findViewById(R.id.txt_no_chats);


        imgSend = findViewById(R.id.img_send);

        btnChatBucketList = findViewById(R.id.chat_bucket_list);
        btnChatBucketList.setVisibility(View.VISIBLE);

        btnChatBucketList.setOnClickListener(this);

        back.setOnClickListener(this);

        imgSend.setOnClickListener(this);

        userphotoChat = findViewById(R.id.userphoto_chat);

        imgChatUser = actionBar.findViewById(R.id.img_chat_user);

        userphotoChat.setOnClickListener(this);


        rlSingleChat = findViewById(R.id.rl_single_chat);

        view = findViewById(R.id.view);

        rvSingleChat = findViewById(R.id.rv_single_chat);


        rlActive = findViewById(R.id.rl_active);

        rlPostlayoutnew = findViewById(R.id.rl_postlayoutnew);

        rlActive.setOnClickListener(this);


        progressChatSingle = findViewById(R.id.progress_chat_single);


        emojiconEditText = findViewById(R.id.emojicon_edit_text);
        emojiconEditText.setImeOptions(EditorInfo.IME_ACTION_GO);


        emojIcon = new EmojIconActions(chatGroupInsideActivity.this, rlSingleChat, emojiconEditText, userphotoChat);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_keyboard_grey, R.drawable.ic_smiley_green_24dp);

        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
            }

            @Override
            public void onKeyboardClose() {
            }
        });

        edittxtChatSingle = findViewById(R.id.edittxt_chat_single);

        /* Real time typing status */

        emojiconEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (emojiconEditText.getText().toString().length() != 0) {
                    imgSend.setVisibility(View.VISIBLE);
                    /*Calling typing status on api*/
                    updateChatTypingStatusOn();
                } else {
                    imgSend.setVisibility(View.GONE);
                    /*Calling typing status off api*/
                    updateChatTypingStatusOff();
                }
            }
        });

        /* IME action send */
        emojiconEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_GO) {

                    encodedEdittext = org.apache.commons.text.StringEscapeUtils.escapeJava(emojiconEditText.getText().toString().trim());

                    if(encodedEdittext.length() > 0 && encodedEdittext.contains("")){

                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();
                    } else {
                        /*Create group chat api calling*/

                        createGroupChat();
                    }

                }
                return false;
            }
        });



        /*Getting input variables from various adapter*/
        try {

            Intent intent = getIntent();

            groupid = Integer.parseInt(intent.getStringExtra("groupid"));
            user_ID = Integer.parseInt(intent.getStringExtra("userid"));
            newgroupid = intent.getStringExtra("groupid");
            groupname = intent.getStringExtra("group_name");
            groupname = intent.getStringExtra("group_name");
            created_by = intent.getStringExtra("created_by");
            Log.e("vishnu_grpname", "-->" + groupname);
            Log.e("vishnu_grpname", "newgroupid-->" + newgroupid);

            groupIcon = intent.getStringExtra("group_icon");
            groupCreatedon = intent.getStringExtra("group_createdon");
            groupCreatedby = intent.getStringExtra("group_createdby");
            groupCreatedbyname = intent.getStringExtra("group_createdbyname");
            username = intent.getStringExtra("username");
            friendid = Integer.parseInt(intent.getStringExtra("friendid"));
            activeStatus = Integer.parseInt(intent.getStringExtra("active_status"));
            sessionid = Integer.parseInt(intent.getStringExtra("sessionid"));
            picture = intent.getStringExtra("picture");
            topicture = intent.getStringExtra("topicture");

            Log.e("vishnu_grpname", "sessionid-->" + sessionid);

            /* From groupname layout */

            proGroupid = intent.getStringExtra("pro_groupid");
            newfriendid = intent.getStringExtra("pro_friendid");
            proCreatedbyname = intent.getStringExtra("pro_createdbyname");

            Log.e(TAG, "onCreate:createdbyname "+ proCreatedbyname);
            proUsername = intent.getStringExtra("pro_username");
            proPicture = intent.getStringExtra("pro_picture");
            proCreatedon = intent.getStringExtra("pro_createdon");
            proCreatedby = intent.getStringExtra("pro_createdby");
            proSessionid = intent.getStringExtra("pro_sessionid");

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*User photo smiley setting*/
        userphotoChat.setImageResource(R.drawable.ic_greateorange);

        /*Chat user name setting*/
        chatUsername.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(groupname));


        Log.e("groupIcon", "groupIcon " + groupIcon);


        /*Loading group image*/

        Utility.picassoImageLoader(groupIcon,1,imgChatUser,getApplicationContext());



        /* getGroupFriendsUser */

        getGroupFriendsUser();


        /* Chat history continuous loop */

        mHandler = new Handler();
        timer = new Timer();

        timerTask = new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    public void run() {
                        try {
                            getChatHistorySingleChat();
                            getChatTypingStatus();

                        } catch (Exception e) {
                            // error, do something
                        }
                    }
                });
            }
        };

        timer.schedule(timerTask, 0, 1000);  // interval of one minute


        /* Chat history */
        getChatHistorySingle();


        /* TelephonyManager (IMIE) */
        telephonyManager = (TelephonyManager) chatGroupInsideActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(chatGroupInsideActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        imei = Settings.Secure.getString(chatGroupInsideActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);


    }

    @Override
    public void onSwipeRight() {
        finish();
    }

    @Override
    protected void onSwipeLeft() {

    }


    //Chat last visited
    public void lastvisited(String lastseen) {
        try {
            long now = System.currentTimeMillis();

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convertedDate = dateFormat.parse(lastseen);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now, DateUtils.FORMAT_ABBREV_RELATIVE);

            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                chatActive.setText(R.string.just_now);

            } else if (relavetime1.toString().contains("hours ago")) {
                chatActive.setText("Last seen " + relavetime1.toString().replace("hours ago", "").concat("h"));
            } else if (relavetime1.toString().contains("days ago")) {
                chatActive.setText("Last seen " + relavetime1.toString().replace("days ago", "").concat("dialog"));
            } else {
                chatActive.setText(relavetime1);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    /* GPS */
    public void getLocation() {
        try {
            locationManager = (LocationManager) chatGroupInsideActivity.this.getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
            latitudeOnline = location.getLatitude();
            longitudeOnline = location.getLongitude();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    /* Real time typing status API */
    private void getChatTypingStatus() {
        if (Utility.isConnected(chatGroupInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<Get_chat_typing_output_pojo> call = apiService.get_chat_typing_status("get_chat_typing_status", sessionid, userid);
                call.enqueue(new Callback<Get_chat_typing_output_pojo>() {
                    @Override
                    public void onResponse(Call<Get_chat_typing_output_pojo> call, Response<Get_chat_typing_output_pojo> response) {

                        if (response.body().getResponseCode() == 1 && response.body().getData().get(0).getYourFirstname() != null) {
                            String userid = response.body().getData().get(0).getTypeYourid();
                            String username = response.body().getData().get(0).getYourFirstname().concat(" ").concat(response.body().getData().get(0).getYourLastname());


                            if (userid.equalsIgnoreCase(Pref_storage.getDetail(getApplicationContext(), "userId"))) {

                            } else {
                                chatActive.setText(username.concat(" ").concat("typing..."));
                            }

                        } else {

                        }

                    }

                    @Override
                    public void onFailure(Call<Get_chat_typing_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlSingleChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    //Get group friends API
    private void getGroupFriendsUser() {
        if (Utility.isConnected(chatGroupInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<Get_group_frds_output_pojo> call = apiService.get_group_friends_user("get_group_friends_user", userid, Integer.parseInt(newgroupid));
                call.enqueue(new Callback<Get_group_frds_output_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Get_group_frds_output_pojo> call, @NonNull Response<Get_group_frds_output_pojo> response) {
                        groupUserslist.clear();

                        if (response.body() != null) {
                            if (response.body().getResponseCode() == 1) {
                                for (int j = 0; j < response.body().getData().size(); j++) {


                                    String userFname = response.body().getData().get(j).getFirstName();
                                    String userLname = response.body().getData().get(j).getLastName();
                                    String userid = response.body().getData().get(j).getUserid();


                                    groupUserslist.add(userFname +" "+ userLname );


                                    Log.e(TAG, "onResponse: GroupUsernames"+groupUserslist.toString() );

                                    Collections.reverse(groupUserslist);


                                   /* if (groupUserslist.toString().contains(Pref_storage.getDetail(chatGroupInsideActivity.this, "firstName")+" "+Pref_storage.getDetail(chatGroupInsideActivity.this, "lastName"))) {

                                        String you = groupUserslist.toString().replace(Pref_storage.getDetail(chatGroupInsideActivity.this, "firstName"), "You").replace("]", "").replace("[", "");

                                        chatActive.setText(you);
                                    } else {

                                        chatActive.setText(groupUserslist.toString().replace("[", "").replace("]", ""));

                                    }*/

                                }

                                String userFullName= Pref_storage.getDetail(chatGroupInsideActivity.this, "firstName")+" "+Pref_storage.getDetail(chatGroupInsideActivity.this, "lastName");

                                for(int a=0;a<groupUserslist.size();a++){

                                    if(groupUserslist.get(a).equalsIgnoreCase(userFullName)){


                                        groupUserslist.remove(a);
                                        groupUserslist.add("You");

                                    }else {

                                        chatActive.setText(groupUserslist.toString().replace("[", "").replace("]", ""));

                                    }

                                }



                            } else {

                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Get_group_frds_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlSingleChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }


    /* Update real time typing status ON API */

    private void updateChatTypingStatusOn() {
        if (Utility.isConnected(chatGroupInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<CommonOutputPojo> call = apiService.update_chat_typing_status("update_chat_typing_status", sessionid, userid, 1);
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(@NonNull Call<CommonOutputPojo> call, @NonNull Response<CommonOutputPojo> response) {

                        if (response.body() != null && response.body().getData() == 1) {

                            Log.e("User typing", "User typing");
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<CommonOutputPojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlSingleChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    //Update real time typing status OFF API
    private void updateChatTypingStatusOff() {
        if (Utility.isConnected(chatGroupInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<CommonOutputPojo> call = apiService.update_chat_typing_status("update_chat_typing_status", sessionid, userid, 0);
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(@NonNull Call<CommonOutputPojo> call, @NonNull Response<CommonOutputPojo> response) {

                        if (response.body() != null && response.body().getData() == 1) {

                            Log.e("User not typing", "User not typing");
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<CommonOutputPojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlSingleChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    //Updating chat history
    private void getChatHistorySingle() {

        if (Utility.isConnected(chatGroupInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<Get_group_history_output_pojo> call = apiService.get_chat_history_group("get_chat_history_single", userid, Integer.parseInt(newgroupid), sessionid);
                call.enqueue(new Callback<Get_group_history_output_pojo>() {
                    @Override
                    public void onResponse(Call<Get_group_history_output_pojo> call, Response<Get_group_history_output_pojo> response) {

                        if (response.body().getResponseCode() == 1) {
                            getChatHistoryDetailsPojoArrayList.clear();
                            getchatgroupmemberlist.clear();

                            for (int j = 0; j < response.body().getData().size(); j++) {
                                progressChatSingle.setVisibility(View.GONE);


                                String chatid = response.body().getData().get(j).getChatid();
                                String session_id = response.body().getData().get(j).getSessionid();

                                sessionidReply = Integer.parseInt(session_id);
                                String from_userid = response.body().getData().get(j).getFromuserid();
                                String to_userid = response.body().getData().get(j).getTouserid();
                                String groupid = response.body().getData().get(j).getGroupid();
                                String group_icon = response.body().getData().get(j).getGroupIcon();
                                String lastmessage = response.body().getData().get(j).getMessage();
                                String type_message = response.body().getData().get(j).getTypeMessage();
                                String status = response.body().getData().get(j).getStatus();
                                String response2 = response.body().getData().get(j).getResponse();
                                String msg_date = response.body().getData().get(j).getMsgDate();
                                String created_on = response.body().getData().get(j).getCreatedOn();
                                String lastseen = response.body().getData().get(j).getLastseen();


                                String from_firstname = response.body().getData().get(j).getFromFirstname();
                                String from_lastname = response.body().getData().get(j).getFromLastname();
                                String from_picture = response.body().getData().get(j).getFromPicture();
                                String to_firstname = response.body().getData().get(j).getToFirstname();
                                String to_lastname = response.body().getData().get(j).getToLastname();
                                String to_picture = response.body().getData().get(j).getToPicture();
                                String to_online_status = response.body().getData().get(j).getToOnlineStatus();
                                String last_visitied = response.body().getData().get(j).getToLastvisited();
                                String group_name = response.body().getData().get(j).getGroupName();

                                String group_created_fname = response.body().getData().get(j).getGroupCreatedFirstname();
                                String group_created_lname = response.body().getData().get(j).getGroupCreatedLastname();
                                String group_createdby = response.body().getData().get(j).getGroupCreatedby();
                                String created_date = response.body().getData().get(j).getCreatedDate();
                                String typeur_id = response.body().getData().get(j).getTypeYourid();
                                String your_fname = response.body().getData().get(j).getYourFirstname();
                                String your_lname = response.body().getData().get(j).getYourLastname();
                                String typestatus = response.body().getData().get(j).getTypeStatus();
                                String exist_groupuserid = response.body().getData().get(j).getExistGroupUserid();
                                String group_exists = response.body().getData().get(j).getGroupExits();
                                String group_exist_time = response.body().getData().get(j).getGroupExitsTime();
                                String changed_which = response.body().getData().get(j).getChangedWhich();

                                chatUsername.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(group_name));

                                if (exist_groupuserid.equalsIgnoreCase(Pref_storage.getDetail(getApplicationContext(), "userId"))) {
                                    txtChatNoMember.setVisibility(View.VISIBLE);
                                    imgSend.setVisibility(View.GONE);
                                    emojiconEditText.setVisibility(View.GONE);
                                    userphotoChat.setVisibility(View.GONE);
                                    view.setVisibility(View.GONE);

                                }


                               /* try {


                                    String grpfrd_id = response.body().getData().get(j).getMember().get(j).getGrpfrndId();
                                    String grp_groupid = response.body().getData().get(j).getMember().get(j).getGroupid();
                                    String grp_groupId = response.body().getData().get(j).getMember().get(j).getGroupId();
                                    String grp_userid = response.body().getData().get(j).getMember().get(j).getUserid();
                                    String grp_friendid = response.body().getData().get(j).getMember().get(j).getFriendid();
                                    String grp_fname = response.body().getData().get(j).getMember().get(j).getFirstName();

                                    String grp_lname = response.body().getData().get(j).getMember().get(j).getLastName();
                                    String grp_picture = response.body().getData().get(j).getMember().get(j).getPicture();
                                    String grp_name = response.body().getData().get(j).getMember().get(j).getGroupName();

                                    String grp_createdon = response.body().getData().get(j).getMember().get(j).getCreatedOn();
                                    String grp_createdby = response.body().getData().get(j).getMember().get(j).getCreatedBy();
                                    String grp_active = response.body().getData().get(j).getMember().get(j).getActive();
                                    String grp_exists = response.body().getData().get(j).getMember().get(j).getGroupExits();
                                    String grp_exist_time = response.body().getData().get(j).getMember().get(j).getGroupExitsTime();

                                    chatGroupMemberPojo = new Get_group_member_inside_pojo(grpfrd_id, grp_groupid, grp_userid,
                                            grp_friendid, grp_exists, grp_exist_time,
                                            "", "", grp_fname, grp_lname,
                                            "", "", "", "", "",
                                            grp_picture, grp_groupId, grp_name,
                                            grp_createdby, grp_createdon, grp_active, "");

                                    getchatgroupmemberlist.add(chatGroupMemberPojo);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }*/

                                getChatGroupHistoryDetails = new Get_chat_group_history_details(chatid, session_id,
                                        groupid,
                                        from_userid, touserid,
                                        lastmessage, type_message, status, response2, msg_date, created_on, lastseen, from_firstname,
                                        from_lastname, from_picture, to_firstname,
                                        to_lastname, to_picture, to_online_status, last_visitied,
                                        group_name, group_icon, changed_which, group_createdby,
                                        created_date, group_created_fname, group_created_lname, typeur_id,
                                        typestatus, your_fname, your_lname,
                                        exist_groupuserid, group_exists, group_exist_time, getchatgroupmemberlist, 0);

                                getChatHistoryDetailsPojoArrayList.add(getChatGroupHistoryDetails);


                            }


                            Pref_storage.setDetail(getApplicationContext(), "Chat_group_size", String.valueOf(getChatHistoryDetailsPojoArrayList.size()));
                            chatMulitiViewAdapter = new Chat_multi_view_group_adapter(chatGroupInsideActivity.this, getChatHistoryDetailsPojoArrayList/*,chatDetailsPojoArrayList*/);
                            llm = new LinearLayoutManager(chatGroupInsideActivity.this);
                            rvSingleChat.setLayoutManager(llm);
                            llm.setStackFromEnd(true);
                            rvSingleChat.setHasFixedSize(true);
                            rvSingleChat.setItemAnimator(new DefaultItemAnimator());
                            rvSingleChat.setAdapter(chatMulitiViewAdapter);
                            rvSingleChat.smoothScrollToPosition(getChatHistoryDetailsPojoArrayList.size() + 1);
                            rvSingleChat.setNestedScrollingEnabled(false);
                            chatMulitiViewAdapter.notifyDataSetChanged();

                        }

                    }

                    @Override
                    public void onFailure(Call<Get_group_history_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlSingleChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }



    /*Create group chat api calling*/

    private void createGroupChat() {

        if (Utility.isConnected(chatGroupInsideActivity.this)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");


                Call<Chat_output_pojo> call = apiService.create_group_chat("create_group_chat", groupid, Integer.parseInt(createdby), encodedEdittext);
                call.enqueue(new Callback<Chat_output_pojo>() {
                    @Override
                    public void onResponse(Call<Chat_output_pojo> call, Response<Chat_output_pojo> response) {

                        if (response.body().getResponseCode() == 1) {


                            emojiconEditText.setText("");


                            for (int j = 0; j < response.body().getData().size(); j++) {


                                chatDetailsPojo = new Chat_details_pojo("", "", "", "", "", 0, "");


                                sessionidReply = response.body().getData().get(j).getSessionid();


                                Log.e(TAG, "onResponse: " + sessionidReply);


                                firstChat();


//                                getChatHistorySingleChat();


                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Chat_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Snackbar snackbar = Snackbar.make(rlSingleChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    /* Real time chat history update */

    private void getChatHistorySingleChat() {

        if (Utility.isConnected(chatGroupInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<Get_group_history_output_pojo> call = apiService.get_chat_history_group("get_chat_history_single", userid, Integer.parseInt(newgroupid), sessionidReply);
                call.enqueue(new Callback<Get_group_history_output_pojo>() {
                    @Override
                    public void onResponse(Call<Get_group_history_output_pojo> call, final Response<Get_group_history_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {


                           /* if (response.body().getData().size() <= (Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "Chat_group_size")))) {


                            } else if (response.body().getData().size() > (Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "Chat_group_size")))) {

                                Pref_storage.setDetail(getApplicationContext(), "Chat_group_size", String.valueOf(response.body().getData().size()));

*/
                            getChatHistoryDetailsPojoArrayList.clear();
                            getchatgroupmemberlist.clear();
                            for (int j = 0; j < response.body().getData().size(); j++) {
                                progressChatSingle.setVisibility(View.GONE);


                                String chatid = response.body().getData().get(j).getChatid();
                                String session_id = response.body().getData().get(j).getSessionid();

                                sessionidReply = Integer.parseInt(session_id);
                                String from_userid = response.body().getData().get(j).getFromuserid();
                                String to_userid = response.body().getData().get(j).getTouserid();
                                String groupid = response.body().getData().get(j).getGroupid();
                                String group_icon = response.body().getData().get(j).getGroupIcon();
                                String lastmessage = response.body().getData().get(j).getMessage();
                                String type_message = response.body().getData().get(j).getTypeMessage();
                                String status = response.body().getData().get(j).getStatus();
                                String response2 = response.body().getData().get(j).getResponse();
                                String msg_date = response.body().getData().get(j).getMsgDate();
                                String created_on = response.body().getData().get(j).getCreatedOn();
                                String lastseen = response.body().getData().get(j).getLastseen();


                                String from_firstname = response.body().getData().get(j).getFromFirstname();
                                String from_lastname = response.body().getData().get(j).getFromLastname();
                                String from_picture = response.body().getData().get(j).getFromPicture();
                                String to_firstname = response.body().getData().get(j).getToFirstname();
                                String to_lastname = response.body().getData().get(j).getToLastname();
                                String to_picture = response.body().getData().get(j).getToPicture();
                                String to_online_status = response.body().getData().get(j).getToOnlineStatus();
                                String last_visitied = response.body().getData().get(j).getToLastvisited();
                                String group_name = response.body().getData().get(j).getGroupName();

                                String group_created_fname = response.body().getData().get(j).getGroupCreatedFirstname();
                                String group_created_lname = response.body().getData().get(j).getGroupCreatedLastname();
                                String group_createdby = response.body().getData().get(j).getGroupCreatedby();
                                String created_date = response.body().getData().get(j).getCreatedDate();
                                String typeur_id = response.body().getData().get(j).getTypeYourid();
                                String your_fname = response.body().getData().get(j).getYourFirstname();
                                String your_lname = response.body().getData().get(j).getYourLastname();
                                String typestatus = response.body().getData().get(j).getTypeStatus();
                                String exist_groupuserid = response.body().getData().get(j).getExistGroupUserid();
                                String group_exists = response.body().getData().get(j).getGroupExits();
                                String group_exist_time = response.body().getData().get(j).getGroupExitsTime();
                                String changed_which = response.body().getData().get(j).getChangedWhich();


                                getChatGroupHistoryDetails = new Get_chat_group_history_details(chatid, session_id,
                                        groupid,
                                        from_userid, touserid,
                                        lastmessage, type_message, status, response2, msg_date, created_on, lastseen, from_firstname,
                                        from_lastname, from_picture, to_firstname,
                                        to_lastname, to_picture, to_online_status, last_visitied,
                                        group_name, group_icon, changed_which, group_createdby,
                                        created_date, group_created_fname, group_created_lname, typeur_id,
                                        typestatus, your_fname, your_lname,
                                        exist_groupuserid, group_exists, group_exist_time, getchatgroupmemberlist, 0);


                                getChatHistoryDetailsPojoArrayList.add(getChatGroupHistoryDetails);


                            }

                            chatMulitiViewAdapter = new Chat_multi_view_group_adapter(chatGroupInsideActivity.this, getChatHistoryDetailsPojoArrayList);//*,chatDetailsPojoArrayList*//*);
                            llm = new LinearLayoutManager(chatGroupInsideActivity.this);
                            rvSingleChat.setLayoutManager(llm);
                            llm.setStackFromEnd(true);
                            rvSingleChat.setHasFixedSize(true);
                            rvSingleChat.setItemAnimator(new DefaultItemAnimator());
                            rvSingleChat.setAdapter(chatMulitiViewAdapter);
                            //                                rvSingleChat.smoothScrollToPosition(getChatHistoryDetailsPojoArrayList.size() + 1);
                            rvSingleChat.setNestedScrollingEnabled(false);
                            chatMulitiViewAdapter.notifyDataSetChanged();



                            /* }



                             */
                        }
                    }

                    @Override
                    public void onFailure(Call<Get_group_history_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlSingleChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    private void firstChat() {

        if (Utility.isConnected(chatGroupInsideActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<Get_group_history_output_pojo> call = apiService.get_chat_history_group("get_chat_history_single", userid, Integer.parseInt(newgroupid), sessionidReply);
                call.enqueue(new Callback<Get_group_history_output_pojo>() {
                    @Override
                    public void onResponse(Call<Get_group_history_output_pojo> call, final Response<Get_group_history_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                                getChatHistoryDetailsPojoArrayList.clear();
                                getchatgroupmemberlist.clear();
                                for (int j = 0; j < response.body().getData().size(); j++) {
                                    progressChatSingle.setVisibility(View.GONE);


                                    String chatid = response.body().getData().get(j).getChatid();
                                    String session_id = response.body().getData().get(j).getSessionid();

                                    sessionidReply = Integer.parseInt(session_id);
                                    String from_userid = response.body().getData().get(j).getFromuserid();
                                    String to_userid = response.body().getData().get(j).getTouserid();
                                    String groupid = response.body().getData().get(j).getGroupid();
                                    String group_icon = response.body().getData().get(j).getGroupIcon();
                                    String lastmessage = response.body().getData().get(j).getMessage();
                                    String type_message = response.body().getData().get(j).getTypeMessage();
                                    String status = response.body().getData().get(j).getStatus();
                                    String response2 = response.body().getData().get(j).getResponse();
                                    String msg_date = response.body().getData().get(j).getMsgDate();
                                    String created_on = response.body().getData().get(j).getCreatedOn();
                                    String lastseen = response.body().getData().get(j).getLastseen();


                                    String from_firstname = response.body().getData().get(j).getFromFirstname();
                                    String from_lastname = response.body().getData().get(j).getFromLastname();
                                    String from_picture = response.body().getData().get(j).getFromPicture();
                                    String to_firstname = response.body().getData().get(j).getToFirstname();
                                    String to_lastname = response.body().getData().get(j).getToLastname();
                                    String to_picture = response.body().getData().get(j).getToPicture();
                                    String to_online_status = response.body().getData().get(j).getToOnlineStatus();
                                    String last_visitied = response.body().getData().get(j).getToLastvisited();
                                    String group_name = response.body().getData().get(j).getGroupName();

                                    String group_created_fname = response.body().getData().get(j).getGroupCreatedFirstname();
                                    String group_created_lname = response.body().getData().get(j).getGroupCreatedLastname();
                                    String group_createdby = response.body().getData().get(j).getGroupCreatedby();
                                    String created_date = response.body().getData().get(j).getCreatedDate();
                                    String typeur_id = response.body().getData().get(j).getTypeYourid();
                                    String your_fname = response.body().getData().get(j).getYourFirstname();
                                    String your_lname = response.body().getData().get(j).getYourLastname();
                                    String typestatus = response.body().getData().get(j).getTypeStatus();
                                    String exist_groupuserid = response.body().getData().get(j).getExistGroupUserid();
                                    String group_exists = response.body().getData().get(j).getGroupExits();
                                    String group_exist_time = response.body().getData().get(j).getGroupExitsTime();
                                    String changed_which = response.body().getData().get(j).getChangedWhich();


                                    getChatGroupHistoryDetails = new Get_chat_group_history_details(chatid, session_id,
                                            groupid,
                                            from_userid, touserid,
                                            lastmessage, type_message, status, response2, msg_date, created_on, lastseen, from_firstname,
                                            from_lastname, from_picture, to_firstname,
                                            to_lastname, to_picture, to_online_status, last_visitied,
                                            group_name, group_icon, changed_which, group_createdby,
                                            created_date, group_created_fname, group_created_lname, typeur_id,
                                            typestatus, your_fname, your_lname,
                                            exist_groupuserid, group_exists, group_exist_time, getchatgroupmemberlist, 0);


                                    getChatHistoryDetailsPojoArrayList.add(getChatGroupHistoryDetails);


                                    chatMulitiViewAdapter = new Chat_multi_view_group_adapter(chatGroupInsideActivity.this, getChatHistoryDetailsPojoArrayList);//*,chatDetailsPojoArrayList*//*);
                                    llm = new LinearLayoutManager(chatGroupInsideActivity.this);
                                    rvSingleChat.setLayoutManager(llm);
                                    llm.setStackFromEnd(true);
                                    rvSingleChat.setHasFixedSize(true);
                                    rvSingleChat.setItemAnimator(new DefaultItemAnimator());
                                    rvSingleChat.setAdapter(chatMulitiViewAdapter);
                                    //                                rvSingleChat.smoothScrollToPosition(getChatHistoryDetailsPojoArrayList.size() + 1);
                                    rvSingleChat.setNestedScrollingEnabled(false);
                                    chatMulitiViewAdapter.notifyDataSetChanged();
                                }







                        }

                    }

                    @Override
                    public void onFailure(Call<Get_group_history_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlSingleChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(chatGroupInsideActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }


    //GetRunningAppProcesses
    boolean isNamedProcessRunning(String processName) {
        if (processName == null)
            return false;

        ActivityManager manager =
                (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processes = manager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo process : processes) {
            if (processName.equals(process.processName)) {
                return true;
            }
        }
        return false;
    }



    @Override
    public void onClick(View v) {

        int newid = v.getId();

        switch (newid) {
            case R.id.img_send:
                //Send button Animation
                final Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.modal_in);
                v.startAnimation(anim);
                encodedEdittext = org.apache.commons.text.StringEscapeUtils.escapeJava(emojiconEditText.getText().toString().trim());
                if(encodedEdittext.length() ==0){

                    Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();
                } else {
                    /*Create group chat api calling*/
                    createGroupChat();
                }


                break;
            case R.id.txt_new_message:

                txtNewMessage.setVisibility(View.GONE);
//                chatMulitiViewAdapter = new Chat_multi_view_group_adapter(chatGroupInsideActivity.this, getChatHistoryDetailsPojoArrayList);//*,chatDetailsPojoArrayList*//*);
//                llm = new LinearLayoutManager(chatGroupInsideActivity.this);
//                rvSingleChat.setLayoutManager(llm);
//                llm.setStackFromEnd(true);
//                rvSingleChat.setHasFixedSize(true);
//                rvSingleChat.setItemAnimator(new DefaultItemAnimator());
//                rvSingleChat.setAdapter(chatMulitiViewAdapter);
//
//                rvSingleChat.setNestedScrollingEnabled(false);
//                chatMulitiViewAdapter.notifyDataSetChanged();


                break;

            case R.id.back:
                mHandler.removeCallbacksAndMessages(null);

                timerTask.cancel();
                timer.cancel();
                timer.purge();

                finish();
                break;

            case R.id.rl_active:
                Intent intent = new Intent(chatGroupInsideActivity.this, chatGroupUserProfileActivity.class);
//                intent.putExtra("proUsername",proUsername);
                intent.putExtra("created_by", created_by);
                intent.putExtra("userid", String.valueOf(user_ID));
                intent.putExtra("pro_username", groupname);
//                intent.putExtra("proUsername",username);
//                intent.putExtra("proGroupid",proGroupid);

                if (newgroupid == null) {
                    intent.putExtra("pro_groupid", proGroupid);
                } else {
                    intent.putExtra("pro_groupid", newgroupid);
                }


                intent.putExtra("pro_picture", groupIcon);
//                intent.putExtra("proCreatedon",proCreatedon);
                intent.putExtra("pro_createdon", groupCreatedon);
                intent.putExtra("pro_sessionid", proSessionid);

//                intent.putExtra("proCreatedby",proCreatedby);
                intent.putExtra("pro_createdby", groupCreatedby);
//                intent.putExtra("proCreatedbyname",proCreatedbyname);
                intent.putExtra("pro_createdbyname", proCreatedbyname);
                startActivity(intent);


                break;

            case R.id.chat_bucket_list:

                Intent intent1=new Intent(this, Home.class);
                intent1.putExtra("redirect","profile");
                intent1.putExtra("redirectbucket","3");
                startActivity(intent1);
                break;

                default:
                    break;

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mHandler.removeCallbacksAndMessages(null);
        timerTask.cancel();
        timer.cancel();
        timer.purge();

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timerTask.cancel();
        timer.cancel();
        timer.purge();

        mHandler.removeCallbacksAndMessages(null);

    }

    @Override
    protected void onResume() {
        super.onResume();

        /* getGroupFriendsUser */

        getGroupFriendsUser();
    }


}
