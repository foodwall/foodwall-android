package com.kaspontech.foodwall.chatpackage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kaspontech.foodwall.adapters.chat.Chat_follower_adapter;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_details_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.NewChatPojo.NewChatOuputPojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateGroupmember extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "New_chat_activity";

    // Action Bar
    Toolbar actionBar;
    TextView toolbar_title,toolbar_next;
    ImageButton back,img_chat_add,smiley,follower_next;
    ImageView groupphoto_chat;
    View rl_chat,rl_followerlay_chat;
    RecyclerView rv_followers_chat;
    LinearLayoutManager llm;
    EditText edittxt_chat;
    TextView txt_chat_send;

    ProgressBar progress_chat_follower;

    String chat_message;

    Chat_details_pojo chatDetailsPojo;
    ArrayList<Chat_details_pojo> chatDetailsPojoArrayList = new ArrayList<>();


    //Following pojo
    Get_followerlist_pojo getFollowerlistPojo;
    ArrayList<Get_followerlist_pojo> getChatFollowerPojoArrayList = new ArrayList<>();
    List<Integer> friendsList= new ArrayList<>();

    Chat_follower_adapter chatFollowingAdapter;

    EmojiconEditText emoji_edit_text;
    EmojIconActions emojIcon;
    String groupname_result,camera_image,gallery_image,upload_image, followerid,group_createdon,group_createdby, groupid,userid,group_name,group_icon;
    int  friendid,sessionid;
    //Group image
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1 ;
    private static final int PICK_FROM_GALLERY = 1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group_layout);

        actionBar = (Toolbar) findViewById(R.id.actionbar_chat_follower);
        back = (ImageButton)actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);
        follower_next = (ImageButton)actionBar.findViewById(R.id.follower_next);
        follower_next.setOnClickListener(this);

        smiley = (ImageButton)findViewById(R.id.smiley);
        smiley.setOnClickListener(this);

        toolbar_title=(TextView) actionBar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(R.string.create_group);

        toolbar_next=(TextView) actionBar.findViewById(R.id.toolbar_next);
        toolbar_next.setText(R.string.create);

        toolbar_next.setOnClickListener(this);


        rl_followerlay_chat=findViewById(R.id.rl_followerlay_chat);

        progress_chat_follower= (ProgressBar) findViewById(R.id.progress_chat_follower);
        progress_chat_follower.setVisibility(View.GONE);

        groupphoto_chat= (ImageView) findViewById(R.id.groupphoto_chat);
        groupphoto_chat.setOnClickListener(this);


        rv_followers_chat=(RecyclerView)findViewById(R.id.rv_followers_chat);



        emoji_edit_text = (EmojiconEditText) findViewById(R.id.emoji_edit_text);
        emoji_edit_text.setImeOptions(EditorInfo.IME_ACTION_SEND);


        emojIcon = new EmojIconActions(CreateGroupmember.this, rl_followerlay_chat, emoji_edit_text, smiley);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
//                Log.e(TAG, "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
//                Log.e(TAG, "Keyboard closed");
            }
        });


        //sent button visiblity

        emoji_edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {




            }
        });

        //Intent
        Intent intent= getIntent();

        followerid= intent.getStringExtra("followerid_array");
        Log.e("followerid_array", "-->"+followerid );

        String seperated= followerid.replace("[","").replace("]","").trim();
        List<String> elephantList = Arrays.asList(seperated.split(","));

        for(int i=0;i<elephantList.size();i++){
            friendsList.add(Integer.parseInt(elephantList.get(i).trim()));
        }


        Log.e("friendsList", "-->"+friendsList.toString() );

    }




    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }






    //create_group_chat
    private void create_group() {
        if (isConnected(CreateGroupmember.this)){

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(),"userId");


                String JSON= new Gson().toJson(friendsList);

                Call<NewChatOuputPojo> call = apiService.create_group( "create_group",
                        groupname_result,
                        0,
                        Integer.parseInt(createdby),
                        JSON);
                call.enqueue(new Callback<NewChatOuputPojo>() {
                    @Override
                    public void onResponse(Call<NewChatOuputPojo>call, Response<NewChatOuputPojo> response) {

                        if (response.body().getResponseCode()==1){


                            int sessionid=response.body().getData().getNewChatgroupinputPojo().getSessionid();
                            String groupid=response.body().getData().getNewChatgroupinputPojo().getGroupid();
                            String userid=response.body().getData().getNewChatgroupinputPojo().getUserid();
                            String group_icon=response.body().getData().getNewChatgroupinputPojo().getGroupicon();
                            String group_name=response.body().getData().getNewChatgroupinputPojo().getGroupname();

                            String group_createdby=response.body().getData().getNewChatgroupinputPojo().getGroupCreatedBy();
                            String group_createdon=response.body().getData().getNewChatgroupinputPojo().getGroupCreatedOn();


                            Chat_details_pojo chatDetailsPojo_result= new Chat_details_pojo(groupid,group_name,group_icon,
                                    group_createdby,group_createdon,sessionid,userid);
                            chatDetailsPojoArrayList.add(chatDetailsPojo_result);


                            Intent intent= new Intent(CreateGroupmember.this, chatGroupInsideActivity.class);


                            intent.putExtra("groupid",groupid);
                            intent.putExtra("groupIcon",group_icon);
                            intent.putExtra("group_name",group_name);
                            intent.putExtra("sessionid",sessionid);
                            intent.putExtra("groupCreatedon",group_createdon);
                            intent.putExtra("groupCreatedby",group_createdby);
                            startActivity(intent);
                            finish();

                     /*       for (int j = 0; j < response.body().getData().size(); j++) {

                                 sessionid=response.body().getData().get(j).getSessionid();


                                 groupid=response.body().getData().get(j).getGroupid();
                                 userid=response.body().getData().get(j).getUserid();
                                 groupIcon=response.body().getData().get(j).getGroupicon();
                                 group_name=response.body().getData().get(j).getGroupname();


                                 groupCreatedby=response.body().getData().get(j).getGroupCreatedBy();
                                 groupCreatedon=response.body().getData().get(j).getGroupCreatedOn();

//                          Pref_storage.setDetail(New_chat_activity.this,"group_name",group_name);
//                          Pref_storage.setDetail(New_chat_activity.this,"sessionid", String.valueOf(sessionid));


                                Chat_details_pojo chatDetailsPojo_result= new Chat_details_pojo(groupid,group_name,groupIcon,
                                        groupCreatedby,groupCreatedon,sessionid,userid);
                                chatDetailsPojoArrayList.add(chatDetailsPojo_result);

//                          friendsList_chat.clear();


                                Intent intent= new Intent(CreateGroupmember.this,chatGroupInsideActivity.class);


                                intent.putExtra("groupid",groupid);
                                intent.putExtra("groupIcon",groupIcon);
                                intent.putExtra("group_name",group_name);
                                intent.putExtra("sessionid",sessionid);
                                intent.putExtra("groupCreatedon",groupCreatedon);
                                intent.putExtra("groupCreatedby",groupCreatedby);
                                startActivity(intent);
                                finish();

                            }*/
                        }

                    }

                    @Override
                    public void onFailure(Call<NewChatOuputPojo> call, Throwable t) {

                        if(t.getMessage().contains("Expected BEGIN_ARRAY")){
                            Intent intent= new Intent(CreateGroupmember.this, chatGroupInsideActivity.class);


                            intent.putExtra("groupid",groupid);
                            intent.putExtra("groupIcon",group_icon);
                            intent.putExtra("group_name",group_name);
                            intent.putExtra("sessionid",sessionid);
                            intent.putExtra("groupCreatedon",group_createdon);
                            intent.putExtra("groupCreatedby",group_createdby);
                            startActivity(intent);
                            finish();
                        }



                        //Error
                        Log.e("FailureError",""+t.getMessage());
                    }
                });
            }catch (Exception e ){
                e.printStackTrace();
            }

        }else {

            Snackbar snackbar = Snackbar.make(rl_followerlay_chat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    //create_group_chat
    private void create_group_withImage() {
        if (isConnected(CreateGroupmember.this)){

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(),"userId");
                if(camera_image==null){
                    upload_image=gallery_image;
                }else if(gallery_image==null){
                    upload_image=camera_image;
                }

                File sourceFile = new File(upload_image);
                RequestBody imageupload = RequestBody.create(MediaType.parse("Image/jpeg"), sourceFile);
                RequestBody groupname = RequestBody.create(MediaType.parse("text/plain"), groupname_result);
                MultipartBody.Part image = MultipartBody.Part.createFormData("image",
                        sourceFile.getName(), imageupload);


                Call<NewChatOuputPojo> call = apiService.create_group_with_icon( "create_group",groupname,1,image,Integer.parseInt(createdby),friendsList);
                call.enqueue(new Callback<NewChatOuputPojo>() {
                    @Override
                    public void onResponse(Call<NewChatOuputPojo>call, Response<NewChatOuputPojo> response) {

                        if (response.body().getResponseCode()==1){

                            int sessionid=response.body().getData().getNewChatgroupinputPojo().getSessionid();
                            String groupid=response.body().getData().getNewChatgroupinputPojo().getGroupid();
                            String userid=response.body().getData().getNewChatgroupinputPojo().getUserid();
                            String group_icon=response.body().getData().getNewChatgroupinputPojo().getGroupicon();
                            String group_name=response.body().getData().getNewChatgroupinputPojo().getGroupname();

                            String group_createdby=response.body().getData().getNewChatgroupinputPojo().getGroupCreatedBy();
                            String group_createdon=response.body().getData().getNewChatgroupinputPojo().getGroupCreatedOn();


                            Chat_details_pojo chatDetailsPojo_result= new Chat_details_pojo(groupid,group_name,group_icon,
                                    group_createdby,group_createdon,sessionid,userid);
                            chatDetailsPojoArrayList.add(chatDetailsPojo_result);



                            Intent intent= new Intent(CreateGroupmember.this, chatGroupInsideActivity.class);

                            intent.putExtra("groupid",groupid);
                            intent.putExtra("groupIcon",group_icon);
                            intent.putExtra("group_name",group_name);
                            intent.putExtra("sessionid",sessionid);
                            intent.putExtra("groupCreatedon",group_createdon);
                            intent.putExtra("groupCreatedby",group_createdby);

                            startActivity(intent);
                            finish();


            /*          for (int j = 0; j < response.body().getData().size(); j++) {



                          String groupid=response.body().getData().get(j).getGroupid();
                          String userid=response.body().getData().get(j).getUserid();
                          String groupIcon=response.body().getData().get(j).getGroupicon();
                          String group_name=response.body().getData().get(j).getGroupname();
                          String groupCreatedby=response.body().getData().get(j).getGroupCreatedBy();
                          String groupCreatedon=response.body().getData().get(j).getGroupCreatedOn();

                          int sessionid=response.body().getData().get(j).getSessionid();

                          Chat_details_pojo chatDetailsPojo_result= new Chat_details_pojo(groupid,group_name,groupIcon,
                                  groupCreatedby,groupCreatedon,sessionid,userid);
                          chatDetailsPojoArrayList.add(chatDetailsPojo_result);

                          Intent intent= new Intent(New_chat_activity.this,chatGroupInsideActivity.class);

                          intent.putExtra("groupid",groupid);
                          intent.putExtra("groupIcon",groupIcon);
                          intent.putExtra("group_name",group_name);
                          intent.putExtra("sessionid",sessionid);
                          intent.putExtra("groupCreatedon",groupCreatedon);
                          intent.putExtra("groupCreatedby",groupCreatedby);

                          startActivity(intent);
                          finish();



                      }*/
                        }

                    }

                    @Override
                    public void onFailure(Call<NewChatOuputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError",""+t.getMessage());
                    }
                });
            }catch (Exception e ){
                e.printStackTrace();
            }

        }else {

            Snackbar snackbar = Snackbar.make(rl_followerlay_chat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }

    @Override
    public void onClick(View v) {
        int i= v.getId();
        switch (i){

            case R.id.back:
                finish();
                break;
            case R.id.toolbar_next:
                groupname_result= org.apache.commons.text.StringEscapeUtils.escapeJava(emoji_edit_text.getText().toString());

                if(camera_image==null&&gallery_image==null&&!groupname_result.equalsIgnoreCase("")){
                    create_group();

                }else if (camera_image!=null||gallery_image!=null&&!groupname_result.equalsIgnoreCase("")){
                    create_group_withImage();
                }else {
                    Toast.makeText(this, "Group name is empty.", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.groupphoto_chat:

             break;

    }




}
}

