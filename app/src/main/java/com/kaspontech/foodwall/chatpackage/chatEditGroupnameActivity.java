package com.kaspontech.foodwall.chatpackage;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.REST_API.ProgressRequestBody;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;

import dmax.dialog.SpotsDialog;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class chatEditGroupnameActivity extends AppCompatActivity implements View.OnClickListener ,ProgressRequestBody.UploadCallbacks{

    /**
     * Application TAG
     **/
    private static final String TAG = "Chat_edit_activity";

    /**
     * Action Bar
     **/
    Toolbar actionBar;

    /**
     * Toolbar title & next
     **/
    TextView toolbarTitle;

    TextView toolbarNext;


    /**
     * Image button back , smiley, next, group chat
     **/
    ImageButton back;

    ImageButton smiley;

    ImageButton followerNext;

    ImageView groupphotoChat;


    View rlFollowerlayChat;

    /**
     * Emoji edit text
     **/
    EmojiconEditText emojiEditText;

    EmojIconActions emojIcon;
    /**
     * String variables
     **/
    String groupnameResult;
    String cameraImage;
    String galleryImage;
    String uploadImage;

    String editGrpIcon;
    String editGrpname;
    String editGrpid;
    String editGrpSessionid;

    /**
     * int variables
     **/
    int friendid;
    int whichChange;

    private int requestCamera = 0;
    private int selectFile = 1;
    private static final int PICK_FROM_GALLERY = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_edit_chat_groupname);

        /*Widget initialization*/
        actionBar = findViewById(R.id.actionbar_chat_grpname);

        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        followerNext = actionBar.findViewById(R.id.follower_next);
        followerNext.setOnClickListener(this);

        toolbarNext = actionBar.findViewById(R.id.toolbar_next);
        toolbarNext.setText(R.string.done);

        toolbarNext.setOnClickListener(this);

        smiley = findViewById(R.id.smiley);
        smiley.setOnClickListener(this);

        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(R.string.Edit_grp_info);


        rlFollowerlayChat = findViewById(R.id.rl_followerlay_chat);


        groupphotoChat = findViewById(R.id.groupphoto_chat);
        groupphotoChat.setOnClickListener(this);


        emojiEditText = findViewById(R.id.emoji_edit_text);
        emojiEditText.setImeOptions(EditorInfo.IME_ACTION_SEND);

        /*Emoji icon edit text setting*/
        emojIcon = new EmojIconActions(chatEditGroupnameActivity.this, rlFollowerlayChat, emojiEditText, smiley);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e(TAG, "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
                Log.e(TAG, "Keyboard closed");
            }
        });



        /*Getting input from adapters*/

        Intent intent = getIntent();

        editGrpname = intent.getStringExtra("edit_grpname");

        editGrpIcon = intent.getStringExtra("edit_grp_icon");

        editGrpid = intent.getStringExtra("edit_grpid");

        editGrpSessionid = intent.getStringExtra("edit_grp_sessionid");


        /*Group name setting*/

        emojiEditText.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(editGrpname));


        /*Chat profile image loading*/

        Utility.picassoImageLoader(editGrpIcon,0,groupphotoChat,getApplicationContext());

    }





    //create_group_chat
    private void createGroup() {
        if (Utility.isConnected(chatEditGroupnameActivity.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");


                Call<CommonOutputPojo> call = apiService.update_chat_group_no_image("update_chat_group", Integer.parseInt(editGrpSessionid), Integer.parseInt(editGrpid), groupnameResult, 0, Integer.parseInt(createdby), 1);

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(@NonNull Call<CommonOutputPojo> call, @NonNull Response<CommonOutputPojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            Toast.makeText(chatEditGroupnameActivity.this, "Group name has been changed successfully.", Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(chatEditGroupnameActivity.this, ChatActivity.class));
                            finish();

                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<CommonOutputPojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Snackbar snackbar = Snackbar.make(rlFollowerlayChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    //create_group_chat
    private void updateChatGroup() {
        if (Utility.isConnected(chatEditGroupnameActivity.this)) {

            /*Loader*/
            final AlertDialog dialog = new SpotsDialog.Builder().setContext(chatEditGroupnameActivity.this).setMessage("").build();
            dialog.show();

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");
                if (cameraImage == null) {
                    uploadImage = galleryImage;
                } else if (galleryImage == null) {
                    uploadImage = cameraImage;
                }

                File sourceFile = new File(uploadImage);


                File compressedImageFile = new Compressor(this).setQuality(100).compressToFile(sourceFile);

                ProgressRequestBody fileBody = new ProgressRequestBody(compressedImageFile, this);



                MultipartBody.Part image = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), fileBody);



//                RequestBody imageupload = RequestBody.create(MediaType.parse("Image/jpeg"), sourceFile);
                
                RequestBody groupname = RequestBody.create(MediaType.parse("text/plain"), groupnameResult);
                
//                MultipartBody.Part image = MultipartBody.Part.createFormData("image",
//                        sourceFile.getName(), imageupload);

                Log.e(TAG, "" + groupnameResult);
                Log.e(TAG, "editGrpname" + editGrpname);
                Log.e(TAG, "galleryImage" + galleryImage);
                Log.e(TAG, "cameraImage" + cameraImage);
                Log.e(TAG, "uploadImage" + uploadImage);

                if (groupnameResult.equalsIgnoreCase(editGrpname)) {
                    
                    whichChange = 2;
                    
                } else {
                    
                    whichChange = 3;
                    
                }


                Call<CommonOutputPojo> call = apiService.update_chat_group("update_chat_group",
                        Integer.parseInt(editGrpSessionid),
                        Integer.parseInt(editGrpid),
                        groupname,
                        1,
                        image,
                        Integer.parseInt(createdby),
                        whichChange);
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(@NonNull Call<CommonOutputPojo> call, @NonNull Response<CommonOutputPojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            dialog.dismiss();
                            if (whichChange == 2) {
                                Toast.makeText(chatEditGroupnameActivity.this, "Group image has been changed successfully.", Toast.LENGTH_SHORT).show();
                            } else if (whichChange == 3) {
                                Toast.makeText(chatEditGroupnameActivity.this, "Group name and image has been changed successfully.", Toast.LENGTH_SHORT).show();
                            }

                            startActivity(new Intent(chatEditGroupnameActivity.this, ChatActivity.class));
                            finish();


                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<CommonOutputPojo> call, @NonNull Throwable t) {
                        //Error

                        dialog.dismiss();

                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Snackbar snackbar = Snackbar.make(rlFollowerlayChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (i) {

            case R.id.back:
                finish();
                break;
            case R.id.toolbar_next:

                /*Edit group calling api*/
                groupnameResult = org.apache.commons.text.StringEscapeUtils.escapeJava(emojiEditText.getText().toString());

                if (groupnameResult.equalsIgnoreCase("") || groupnameResult == null) {

                    Snackbar snackbar = Snackbar.make(rlFollowerlayChat, "Group name can't be empty!", Snackbar.LENGTH_LONG);
                    snackbar.setActionTextColor(Color.RED);
                    View view1 = snackbar.getView();
                    TextView textview = view1.findViewById(R.id.snackbar_text);
                    textview.setTextColor(Color.WHITE);
                    snackbar.show();

                } else {

                    if (cameraImage == null && galleryImage == null) {

                        /*Creating group*/
                        createGroup();

                    } else if (cameraImage != null || galleryImage != null) {

                        /*Updating group*/
                        updateChatGroup();
                    }
                }


                break;

            case R.id.groupphoto_chat:

                final CharSequence[] items = {"Camera", "Select from Gallery",
                        "Cancel"};

                AlertDialog.Builder front_builder = new AlertDialog.Builder(chatEditGroupnameActivity.this);
                front_builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (items[which].equals("Camera")) {


                            try {
                                if (ActivityCompat.checkSelfPermission(chatEditGroupnameActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                        && ActivityCompat.checkSelfPermission(chatEditGroupnameActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(chatEditGroupnameActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCamera);

                                } else {

                                    cameraIntent();

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (items[which].equals("Select from Gallery")) {

                            try {
                                if (ActivityCompat.checkSelfPermission(chatEditGroupnameActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(chatEditGroupnameActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);

                                } else {

                                    /*Redirect to gallery intent*/
                                    galleryIntent();

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (items[which].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                front_builder.show();
                break;


                default:
                    break;

        }


    }
    /*Redirect to gallery intent*/

    private void galleryIntent() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, selectFile);
    }
    /*Redirect to camera intent*/

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, requestCamera);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == requestCamera) {

                Bitmap photo1 = (Bitmap) data.getExtras().get("data");
                groupphotoChat.setImageBitmap(photo1);

                Uri tempUri = getImageUri(getApplicationContext(), photo1);

                File f = new File(getRealPathFromURI(tempUri));

                cameraImage = f.getAbsolutePath();

                Log.e("imageName", "imageName" + cameraImage);
                Pref_storage.setDetail(getApplicationContext(), "Group_icon", getRealPathFromURI(tempUri));

            } else if (requestCode == selectFile) {

                try {
                    onSelectFromGalleryResult(data);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*Getting image URI*/

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Uri selectedImage = data.getData();


        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor c = getApplicationContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        c.moveToFirst();


        int columnIndex = c.getColumnIndex(filePathColumn[0]);
        String picturePath = c.getString(columnIndex);
        c.close();

        Log.e("imageName", "picturePath" + picturePath);
        galleryImage = picturePath;
        Bitmap photo1 = (BitmapFactory.decodeFile(picturePath));
        groupphotoChat.setImageBitmap(photo1);

        Pref_storage.setDetail(getApplicationContext(), "Group_icon", picturePath);


    }

    /*Getting real path from URI*/

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;

    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
