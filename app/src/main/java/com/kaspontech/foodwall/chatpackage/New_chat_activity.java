package com.kaspontech.foodwall.chatpackage;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaspontech.foodwall.REST_API.ProgressRequestBody;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_details_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.NewChatPojo.NewChatOuputPojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.questionspackage.CreateQuestion;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;
import com.kaspontech.foodwall.utills.universalLoaderClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dmax.dialog.SpotsDialog;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class New_chat_activity extends AppCompatActivity implements View.OnClickListener, ProgressRequestBody.UploadCallbacks {

    /**
     * Application TAG
     **/
    private static final String TAG = "New_chat_activity";
    /**
     * Action Bar
     **/
    Toolbar actionBar;
    /**
     * Tool bar title
     **/
    TextView toolbarTitle;
    /**
     * Tool bar Next
     **/
    TextView toolbarNext;
    /**
     * Image button back
     **/
    ImageButton back;
    /**
     * Image button smiley
     **/
    ImageButton smiley;
    /**
     * Image button follower next
     **/
    ImageButton followerNext;
    /**
     * Image view group chat
     **/
    ImageView groupphotoChat;
    /**
     * View followers layout
     **/
    View rlFollowerlayChat;

    /**
     * Recycler view for followers chat
     **/
    RecyclerView rvFollowersChat;
    /**
     * Loader for followers
     **/
    ProgressBar progressChatFollower;

    /**
     * Chat details array list
     **/
    ArrayList<Chat_details_pojo> chatDetailsPojoArrayList = new ArrayList<>();

    /**
     * Followings Friends list
     **/
    List<Integer> friendsList = new ArrayList<>();
    List<String> friendsList_new = new ArrayList<>();

    /**
     * Emojicon edit text
     **/

    EmojiconEditText emojiconEditText;
    EmojIconActions emojIcon;
    /**
     * String results
     **/
    String resultfriendlist;
    String groupnameResult;
    String cameraImage;
    String galleryImage;
    String uploadImage;
    String followerId;
    int friendid;
    /**
     * int results
     **/

    private String follwerGroupCreate;

    private int requestCamera = 0;
    private int selectFile = 1;
    private static final int PICK_FROM_GALLERY = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group_layout);

        /*Widget intialization*/

        /*Action bar initialization*/

        actionBar = findViewById(R.id.actionbar_chat_follower);

        /*Back button initialization*/
        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        /*Followers next button initialization*/
        followerNext = actionBar.findViewById(R.id.follower_next);
        followerNext.setOnClickListener(this);

        Pref_storage.setDetail(New_chat_activity.this, "Group_icon", null);

        /*Smiley button initialization*/

        smiley = findViewById(R.id.smiley);
        smiley.setOnClickListener(this);

        /*Toolbar title initialization*/
        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(R.string.create_group);


        /*Toolbar create initialization*/

        toolbarNext = actionBar.findViewById(R.id.toolbar_next);
        toolbarNext.setText(R.string.create);

        toolbarNext.setOnClickListener(this);

        /*Follower layout initialization*/
        rlFollowerlayChat = findViewById(R.id.rl_followerlay_chat);

        /*Loader*/
        progressChatFollower = findViewById(R.id.progress_chat_follower);
        progressChatFollower.setVisibility(View.GONE);
        /*Group photo chat view*/
        groupphotoChat = findViewById(R.id.groupphoto_chat);
        groupphotoChat.setOnClickListener(this);

        /*Followers recycler view*/
        rvFollowersChat = findViewById(R.id.rv_followers_chat);

        /*Emojicon edit text*/
        emojiconEditText = findViewById(R.id.emoji_edit_text);
        emojiconEditText.setImeOptions(EditorInfo.IME_ACTION_SEND);


        emojIcon = new EmojIconActions(New_chat_activity.this, rlFollowerlayChat, emojiconEditText, smiley);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
            }

            @Override
            public void onKeyboardClose() {
            }
        });


        //Gettting input variables

        Intent intent = getIntent();

        followerId = intent.getStringExtra("followerid_array");

        resultfriendlist = intent.getStringExtra("followerid_array_new").replace("\\", "");

        friendsList_new.add(resultfriendlist);


        Log.e("followerid_array", "-->" + followerId);
        Log.e("followerid_array", "resultfriendlist-->" + resultfriendlist);


        String createdByuser = Pref_storage.getDetail(getApplicationContext(), "userId");

        String seperated = followerId.replace("[", "").replace("]", "").trim();
        List<String> stringList = Arrays.asList(seperated.split(","));

        friendsList.add(0, Integer.parseInt(createdByuser));
        for (int i = 0; i < stringList.size(); i++) {

            friendsList.add(Integer.parseInt(stringList.get(i).trim()));
        }

        follwerGroupCreate = new Gson().toJson(friendsList);

        Log.e("friendsList", "-->" + friendsList.toString());
        Log.e("friendsListarray", "-->" + follwerGroupCreate);

    }


    //create_group_chat
    private void createGroup() {
        if (Utility.isConnected(New_chat_activity.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                /*Loader*/
                final universalLoaderClass loaderClass = new universalLoaderClass(this);
                loaderClass.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                loaderClass.setCancelable(true);
                loaderClass.show();
                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                //friendsList.add(Integer.valueOf(createdby));
                //String JSON = new Gson().toJson(friendsList);

                Log.e(TAG, "createGroup: createdby" + createdby);
                Log.e(TAG, "createGroup: JSON" + new Gson().toJson(friendsList));

                Call<NewChatOuputPojo> call = apiService.create_group("create_group",
                        groupnameResult, 0, Integer.parseInt(createdby), new Gson().toJson(friendsList));

                call.enqueue(new Callback<NewChatOuputPojo>() {
                    @Override
                    public void onResponse(Call<NewChatOuputPojo> call, Response<NewChatOuputPojo> response) {

                        if (response.body().getResponseMessage().equalsIgnoreCase("success")) {

                            int sessionid = response.body().getData().getNewChatgroupinputPojo().getSessionid();
                            String groupid = response.body().getData().getNewChatgroupinputPojo().getGroupid();
                            String userid = response.body().getData().getNewChatgroupinputPojo().getUserid();
                            String group_icon = response.body().getData().getNewChatgroupinputPojo().getGroupicon();
                            String groupName = response.body().getData().getNewChatgroupinputPojo().getGroupname();

                            String groupCreatedBy = response.body().getData().getNewChatgroupinputPojo().getGroupCreatedBy();
                            String groupCreatedOn = response.body().getData().getNewChatgroupinputPojo().getGroupCreatedOn();


                            Chat_details_pojo chatDetailsPojo_result = new Chat_details_pojo(groupid, groupName, group_icon,
                                    groupCreatedBy, groupCreatedOn, sessionid, userid);
                            chatDetailsPojoArrayList.add(chatDetailsPojo_result);


                            loaderClass.dismiss();

                            Toast.makeText(New_chat_activity.this, "Group created successfully.", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    }

                    @Override
                    public void onFailure(Call<NewChatOuputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        loaderClass.dismiss();
                        //Toast.makeText(New_chat_activity.this, "Group created successfully.", Toast.LENGTH_SHORT).show();
                        //finish();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Snackbar snackbar = Snackbar.make(rlFollowerlayChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    //create_group_chat
    private void create_group_withImage() {

        if (Utility.isConnected(New_chat_activity.this)) {


            /*Loader*/
            final universalLoaderClass loaderClass = new universalLoaderClass(this);
            loaderClass.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            loaderClass.setCancelable(true);
            loaderClass.show();

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int createdby = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));
                String createdByuser = Pref_storage.getDetail(getApplicationContext(), "userId");
                if (cameraImage == null) {
                    uploadImage = galleryImage;
                } else if (galleryImage == null) {
                    uploadImage = cameraImage;
                }


                MultipartBody.Builder builder = new MultipartBody.Builder();
                builder.setType(MultipartBody.FORM);

                //Timeline
                builder.addFormDataPart("group_name", groupnameResult);
                builder.addFormDataPart("created_by", String.valueOf(createdby));
                builder.addFormDataPart("friends", new Gson().toJson(friendsList));


                if (uploadImage != null) {

                    builder.addFormDataPart("img_status", String.valueOf(1));
                    File sourceFile = new File(uploadImage);
                    builder.addFormDataPart("image", sourceFile.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), sourceFile));
                    Log.e("imagefile", "filePaths_new" + sourceFile);


                } else {

                    builder.addFormDataPart("img_status", String.valueOf(0));
                }

                MultipartBody requestBody = builder.build();

                /*File sourceFile = new File(uploadImage);

                File compressedImageFile = new Compressor(this).setQuality(100).compressToFile(sourceFile);

                ProgressRequestBody fileBody = new ProgressRequestBody(compressedImageFile, this);

                MultipartBody.Part image = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), fileBody);

                //friendsList.add(Integer.valueOf(createdByuser));
                //String JSON = new Gson().toJson(friendsList);

                Log.e("test", "createGroup: JSON" + new Gson().toJson(friendsList));

                Gson gson = new Gson();
                Type listType = new TypeToken<List<Integer>>(){}.getType();
                List<Integer> posts = gson.fromJson(follwerGroupCreate, listType);

                Log.e("test", "createGroup: JSON" + posts);*/

                /*RequestBody imageupload = RequestBody.create(MediaType.parse("Image/jpeg"), sourceFile);


                MultipartBody.Part image = MultipartBody.Part.createFormData("image",
                        sourceFile.getName(), imageupload);*/


                // RequestBody groupname = RequestBody.create(MediaType.parse("text/plain"), groupnameResult);

/*
                Call<Chat_output_pojo> call = apiService.create_group_with_icon("create_group", groupname, 1,
                        image, createdby,

                       Integer.parseInt(new Gson().toJson(friendsList)));*/

                Call<Chat_output_pojo> call = apiService.create_group_with_icon("create_group", requestBody);
                call.enqueue(new Callback<Chat_output_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Chat_output_pojo> call, @NonNull Response<Chat_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            int sessionid = response.body().getData().get(0).getSessionid();
                            String groupid = response.body().getData().get(0).getGroupid();
                            String userid = response.body().getData().get(0).getUserid();
                            String groupIcon = response.body().getData().get(0).getGroupicon();
                            String group_name = response.body().getData().get(0).getGroupname();

                            String groupCreatedBy = response.body().getData().get(0).getGroupCreatedBy();
                            String createdOn = response.body().getData().get(0).getGroupCreatedOn();


                            Chat_details_pojo chatDetailsPojo_result = new Chat_details_pojo(groupid, group_name, groupIcon,
                                    groupCreatedBy, createdOn, sessionid, userid);
                            chatDetailsPojoArrayList.add(chatDetailsPojo_result);


                            loaderClass.dismiss();

                            Toast.makeText(New_chat_activity.this, "Group created successfully.", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    }

                    @Override
                    public void onFailure(Call<Chat_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                 /* Intent intent= new Intent(New_chat_activity.this,chatGroupInsideActivity.class);
                  startActivity(intent);*/
                        loaderClass.dismiss();

                        //Toast.makeText(New_chat_activity.this, "Group created successfully.", Toast.LENGTH_SHORT).show();
                        //finish();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Snackbar snackbar = Snackbar.make(rlFollowerlayChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (i) {

            case R.id.back:
                finish();
                break;
            case R.id.toolbar_next:

                Utility.hideKeyboard(v);

                groupnameResult = org.apache.commons.text.StringEscapeUtils.escapeJava(emojiconEditText.getText().toString().trim());
                //String cameraPath = Pref_storage.getDetail(New_chat_activity.this,"Group_icon");

                if (groupnameResult.isEmpty()) {

                    Toast.makeText(this, "Group name is empty.", Toast.LENGTH_SHORT).show();

                } else {

                    create_group_withImage();

                }

                /*if (cameraImage == null && galleryImage == null && !groupnameResult.equalsIgnoreCase("")) {
                 *//*Create group api*//*
                    createGroup();


                } else if (cameraImage != null || galleryImage != null && !groupnameResult.equalsIgnoreCase("")) {

                    *//*Create group with image api*//*
                    create_group_withImage();

                } else {
                    Toast.makeText(this, "Group name is empty.", Toast.LENGTH_SHORT).show();
                }*/

                break;

            case R.id.groupphoto_chat:
                final CharSequence[] items = {"Camera", "Select from Gallery",
                        "Cancel"};
                /*Dialog initialization*/
                AlertDialog.Builder frontBuilder = new AlertDialog.Builder(New_chat_activity.this);
                frontBuilder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (items[which].equals("Camera")) {


                            try {
                                if (ActivityCompat.checkSelfPermission(New_chat_activity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                        && ActivityCompat.checkSelfPermission(New_chat_activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(New_chat_activity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCamera);

                                } else {
                                    /*Camera Intent*/

                                    cameraIntent();

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (items[which].equals("Select from Gallery")) {

                            try {
                                if (ActivityCompat.checkSelfPermission(New_chat_activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(New_chat_activity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);

                                } else {
                                    /*Gallery Intent*/
                                    galleryIntent();

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (items[which].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                frontBuilder.show();
                break;
            default:
                break;
        }


    }

    /*Gallery Intent*/
    private void galleryIntent() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, selectFile);
    }

    /*Camera Intent*/

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, requestCamera);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == requestCamera) {

                Bitmap photo1 = (Bitmap) data.getExtras().get("data");
                groupphotoChat.setImageBitmap(photo1);

                Uri tempUri = getImageUri(getApplicationContext(), photo1);

                File f = new File(getRealPathFromURI(tempUri));

                cameraImage = f.getAbsolutePath();

                Log.e("imageName", "imageName" + cameraImage);
                Pref_storage.setDetail(getApplicationContext(), "Group_icon", getRealPathFromURI(tempUri));

            } else if (requestCode == selectFile) {

                try {
                    onSelectFromGalleryResult(data);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*Get image URI*/
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    /*Select from gallery*/

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Uri selectedImage = data.getData();


        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor c = getApplicationContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);

        c.moveToFirst();


        int columnIndex = c.getColumnIndex(filePathColumn[0]);
        galleryImage = c.getString(columnIndex);
        c.close();

        //Log.e("imageName", "picturePath" + picturePath);
        //galleryImage = picturePath;
        Bitmap photo1 = (BitmapFactory.decodeFile(galleryImage));
        groupphotoChat.setImageBitmap(photo1);

        Pref_storage.setDetail(getApplicationContext(), "Group_icon", galleryImage);


    }

    /*Get real path from URI*/

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;

    }

    @Override
    public void onProgressUpdate(int percentage) {
        Log.e(TAG, "onProgressUpdate: " + percentage);

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}