package com.kaspontech.foodwall.chatpackage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.adapters.chat.Chat_adapter;
import com.kaspontech.foodwall.adapters.chat.SearchChat_adapter;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ChatSearch_Datum;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ChatSearch_Response;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_group_member_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_history_details_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_history_output;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.RecyclerTouchListener;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends _SwipeActivityClass implements View.OnClickListener {
    /*
     * Application TAG
     * */
    private static final String TAG = "ChatActivity";

    /*
     * Text view no chats
     * */
    public TextView txt_no_chats;

    /*
     * Action bar
     * */

    Toolbar actionBar;


    /*
     * Image buttons
     * */

    ImageButton back;

    ImageButton imgChatAdd;

    ImageButton imgChatAddGroup;


    /*
     * Edit text for chat search*/
    EditText editSearchChat;

    /*
     * RelativeLayout
     * */

    RelativeLayout rlChat;
    RelativeLayout chatOverall;
    RelativeLayout rlGrpBtn;
    RelativeLayout rlSingleBtn;


    /*
     * RecyclerView
     * */

    RecyclerView rvChat, rv_chatsearch;

    /*
     * Chat history pojo
     * */


    Get_chat_history_details_pojo getChatHistoryDetailsPojo1;
    ChatSearch_Datum getChatHistoryDetailsPojo2;

    /*
     * Chat array list
     * */


    ArrayList<Get_chat_history_details_pojo> getChatHistoryDetailsPojoArrayList = new ArrayList<>();
    ArrayList<ChatSearch_Datum> getChatHistoryDetailsPojoArrayList2 = new ArrayList<>();

    ArrayList<Chat_group_member_pojo> getchatgroupmemberlist = new ArrayList<>();


    int friendid;

    int groupid;

    int totalMember;
    /*
     * Chat loader
     * */

    ProgressBar progressChat;

    /*
     * Chat adapter
     * */
    Chat_adapter chatAdapter;
    SearchChat_adapter searchChat_adapter;

    /*
     * Search view
     * */
    SearchView searchView;
    /*
     * No data linear layout
     * */

    LinearLayout llNodata;

    /*ll_nodata
     * Handler
     * */
    Handler mHandler;
    Timer timer;
    TimerTask timerTask;

    String notificationtype, searchtext, redirect_url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chatactivity_layout);

        //Action bar
        actionBar = findViewById(R.id.actionbar_chat);
        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        //Image button for single chat
        imgChatAdd = actionBar.findViewById(R.id.img_chat_add);
        imgChatAdd.setOnClickListener(this);

        //Image button for group chat
        imgChatAddGroup = actionBar.findViewById(R.id.img_chat_add_group);
        imgChatAddGroup.setOnClickListener(this);


        rlChat = findViewById(R.id.rl_chat);

        editSearchChat = findViewById(R.id.editSearchChat);

        chatOverall = findViewById(R.id.chat_overall);
        llNodata = findViewById(R.id.ll_nodata);

        rlGrpBtn = actionBar.findViewById(R.id.rl_grp_btn);
        rlGrpBtn.setOnClickListener(this);
        rlSingleBtn = actionBar.findViewById(R.id.rl_single_btn);
        rlSingleBtn.setOnClickListener(this);

        rvChat = findViewById(R.id.rv_chat);
        rv_chatsearch = findViewById(R.id.rv_chatsearch);

        progressChat = findViewById(R.id.progress_chat);


        txt_no_chats = findViewById(R.id.txt_no_chats);
//
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        // toolbar fancy stuff
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("");


        //Chat API
        get_chat_user_search("0");

        try {

            notificationtype = getIntent().getStringExtra("notificationtype");
            redirect_url = getIntent().getStringExtra("redirect_url");


        } catch (Exception e) {
            e.printStackTrace();
        }

        //Chat
        if (getChatHistoryDetailsPojoArrayList.size() > 0) {
            //Calling chat history API
            chatAdapter = new Chat_adapter(ChatActivity.this, getChatHistoryDetailsPojoArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
            rvChat.setLayoutManager(mLayoutManager);
            rvChat.setItemAnimator(new DefaultItemAnimator());
            rvChat.setAdapter(chatAdapter);
            rvChat.setNestedScrollingEnabled(false);


        } else {

            chatAdapter = new Chat_adapter(ChatActivity.this, getChatHistoryDetailsPojoArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
            rvChat.setLayoutManager(mLayoutManager);
            rvChat.setItemAnimator(new DefaultItemAnimator());
            rvChat.setAdapter(chatAdapter);
            rvChat.setNestedScrollingEnabled(false);
            progressChat.setVisibility(View.VISIBLE);


            getChatHistory();


        }


        /** On  press on animation RecyclerView item * */
        rvChat.addOnItemTouchListener(new RecyclerTouchListener(this,
                rvChat, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        /*Edit text for search chat listener*/

        editSearchChat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

//                chatAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

               /* if(s.toString().equalsIgnoreCase("")){

                    chatAdapter = new Chat_adapter(ChatActivity.this, getChatHistoryDetailsPojoArrayList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
                    rvChat.setLayoutManager(mLayoutManager);
//                 rvChat.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
                    rvChat.setItemAnimator(new DefaultItemAnimator());
                    rvChat.setAdapter(chatAdapter);
                    rvChat.setNestedScrollingEnabled(false);


                }
                else {
                    chatAdapter.getFilter().filter(s.toString());
                }*/

                if (s.toString().equalsIgnoreCase("")) {

                    chatAdapter = new Chat_adapter(ChatActivity.this, getChatHistoryDetailsPojoArrayList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
                    rvChat.setLayoutManager(mLayoutManager);
                    rvChat.setItemAnimator(new DefaultItemAnimator());
                    rvChat.setAdapter(chatAdapter);
                    rvChat.setNestedScrollingEnabled(false);

                    chatAdapter.notifyDataSetChanged();
                    rvChat.setVisibility(View.VISIBLE);
                    rv_chatsearch.setVisibility(View.GONE);
                    getChatHistoryDetailsPojoArrayList2.clear();


                } else {

                    searchtext = s.toString();
                    rvChat.setVisibility(View.GONE);

                    get_chat_user_search(searchtext);

                    if (searchChat_adapter != null) {
                        searchChat_adapter.getFilter().filter(s.toString());
                    }

                }

            }
        });


        //Chat history continuous loop
        mHandler = new Handler();
        timer = new Timer();

        timerTask = new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    public void run() {

                        try {
                            get_chat_history_loop();

                        } catch (Exception e) {
                            // error, do something
                        }
                    }
                });
            }
        };

        timer.schedule(timerTask, 0, 1000);  // interval of one minute


    }

    @Override
    public void onSwipeRight() {
      /*  Intent intent = new Intent(ChatActivity.this, Home.class);
        intent.putExtra("redirect","timeline");
        startActivity(intent);*/
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_right);

    }

    @Override
    protected void onSwipeLeft() {

    }

    private void get_chat_user_search(String searchtext) {

        if (Utility.isConnected(ChatActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");


                Call<ChatSearch_Response> call = apiService.get_chat_user_search("get_chat_user_search",
                        Integer.parseInt(createdby),
                        searchtext);

                call.enqueue(new Callback<ChatSearch_Response>() {
                    @Override
                    public void onResponse(Call<ChatSearch_Response> call, Response<ChatSearch_Response> response) {

                        if (response.body() != null) {

                            if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {
                                llNodata.setVisibility(View.GONE);

                                Pref_storage.setDetail(ChatActivity.this, "Chat_history_size", String.valueOf(response.body().getData().size()));

                                getChatHistoryDetailsPojoArrayList2.clear();
                                getchatgroupmemberlist.clear();


                                if (response.body().getData().size() != 0) {

                                    for (int j = 0; j < response.body().getData().size(); j++) {

                                        String userid = response.body().getData().get(j).getUserid();
                                        String histId = response.body().getData().get(j).getHistId();
                                        String sessionid = response.body().getData().get(j).getSessionid();
                                        String groupid = response.body().getData().get(j).getGroupid();
                                        String createdOn = response.body().getData().get(j).getCreatedOn();
                                        String typeYourid = response.body().getData().get(j).getTypeYourid();
                                        String typeStatus = response.body().getData().get(j).getTypeStatus();
                                        String typeMessage = response.body().getData().get(j).getTypeMessage();
                                        String yourFirstname = String.valueOf(response.body().getData().get(j).getYourFirstname());
                                        String yourLastname = String.valueOf(response.body().getData().get(j).getYourLastname());
                                        String lastmessage = response.body().getData().get(j).getLastmessage();
                                        String lastseen = response.body().getData().get(j).getLastseen();
                                        String fromFirstname = response.body().getData().get(j).getFromFirstname();
                                        String fromLastname = response.body().getData().get(j).getFromLastname();
                                        String fromPicture = response.body().getData().get(j).getFromPicture();
                                        String friendid = response.body().getData().get(j).getFriendid();
                                        String toFirstname = response.body().getData().get(j).getToFirstname();
                                        String toLastname = response.body().getData().get(j).getToLastname();
                                        String toPicture = response.body().getData().get(j).getToPicture();
                                        String toOnlineStatus = response.body().getData().get(j).getToOnlineStatus();
                                        String toLastvisited = response.body().getData().get(j).getToLastvisited();
                                        String groupName = String.valueOf(response.body().getData().get(j).getGroupName());
                                        String groupIcon = String.valueOf(response.body().getData().get(j).getGroupIcon());
                                        String changedWhich = String.valueOf(response.body().getData().get(j).getChangedWhich());
                                        String groupCreatedby = String.valueOf(response.body().getData().get(j).getGroupCreatedby());
                                        String createdDate = String.valueOf(response.body().getData().get(j).getCreatedDate());
                                        String groupCreatedFirstname = String.valueOf(response.body().getData().get(j).getGroupCreatedFirstname());
                                        String groupCreatedLastname = String.valueOf(response.body().getData().get(j).getGroupCreatedLastname());
                                        String existGroupUserid = String.valueOf(response.body().getData().get(j).getExistGroupUserid());
                                        String groupExits = String.valueOf(response.body().getData().get(j).getGroupExits());
                                        String groupExitsTime = String.valueOf(response.body().getData().get(j).getGroupExitsTime());

                                        getChatHistoryDetailsPojo2 = new ChatSearch_Datum(userid, histId, sessionid, groupid, createdOn, typeYourid, typeStatus, typeMessage,
                                                yourFirstname, yourLastname, lastmessage, lastseen, fromFirstname, fromLastname, fromPicture, friendid, toFirstname, toLastname,
                                                toPicture, toOnlineStatus, toLastvisited, groupName, groupIcon, changedWhich, groupCreatedby, createdDate, groupCreatedFirstname,
                                                groupCreatedLastname, existGroupUserid, groupExits, groupExitsTime);


                                        getChatHistoryDetailsPojoArrayList2.add(getChatHistoryDetailsPojo2);

                                        searchChat_adapter = new SearchChat_adapter(ChatActivity.this, getChatHistoryDetailsPojoArrayList2);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
                                        rv_chatsearch.setLayoutManager(mLayoutManager);
                                        rv_chatsearch.setItemAnimator(new DefaultItemAnimator());
                                        rv_chatsearch.setAdapter(searchChat_adapter);
                                        rv_chatsearch.setNestedScrollingEnabled(false);

                                        searchChat_adapter.notifyDataSetChanged();


                                    }

                                } else {
                                    Toast.makeText(ChatActivity.this, "No Data found", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                            }

                        }


                    }

                    @Override
                    public void onFailure(Call<ChatSearch_Response> call, Throwable t) {
                        //  llNodata.setVisibility(View.VISIBLE);

                        //Error
                        Log.e(TAG, "FailureError" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Snackbar snackbar = Snackbar.make(chatOverall, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }


    //Chat history API

    private void getChatHistory() {


        if (Utility.isConnected(ChatActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_chat_history_output> call = apiService.get_chat_history("get_chat_history", Integer.parseInt(createdby));
                call.enqueue(new Callback<Get_chat_history_output>() {
                    @Override
                    public void onResponse(Call<Get_chat_history_output> call, Response<Get_chat_history_output> response) {

                        if (response.code() == 500) {



                        } else {

                            llNodata.setVisibility(View.GONE);
                            if (response.body() != null) {

                                if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {

                                    Pref_storage.setDetail(ChatActivity.this, "Chat_history_size", String.valueOf(response.body().getData().size()));

                                    progressChat.setVisibility(View.GONE);


                                    getChatHistoryDetailsPojoArrayList.clear();
                                    getchatgroupmemberlist.clear();


                                    for (int j = 0; j < response.body().getData().size(); j++) {

                                        String histId = response.body().getData().get(j).getHistId();
                                        String sessionid = response.body().getData().get(j).getSessionid();
                                        String friendid = response.body().getData().get(j).getFriendid();
                                        String userid = response.body().getData().get(j).getUserid();
                                        String groupid = response.body().getData().get(j).getGroupid();
                                        String groupIcon = response.body().getData().get(j).getGroup_icon();
                                        String typeurId = response.body().getData().get(j).getTypeYourid();
                                        String yourFname = response.body().getData().get(j).getYourFirstname();
                                        String yourLname = response.body().getData().get(j).getYourLastname();
                                        String typestatus = response.body().getData().get(j).getTypeStatus();
                                        String createdOn = response.body().getData().get(j).getCreatedOn();
                                        String lastmessage = response.body().getData().get(j).getLastmessage();
                                        String lastseen = response.body().getData().get(j).getLastseen();
                                        String fromFirstname = response.body().getData().get(j).getFromFirstname();
                                        String fromLastname = response.body().getData().get(j).getFromLastname();
                                        String fromPicture = response.body().getData().get(j).getFromPicture();
                                        String toFirstname = response.body().getData().get(j).getToFirstname();
                                        String toLastname = response.body().getData().get(j).getToLastname();
                                        String toPicture = response.body().getData().get(j).getToPicture();
                                        String toOnlineStatus = response.body().getData().get(j).getToOnlineStatus();
                                        String toLastvisited = response.body().getData().get(j).getToLastvisited();
                                        String groupName = response.body().getData().get(j).getGroupName();
                                        String groupCreatedFirstname = response.body().getData().get(j).getGroupCreatedFirstname();
                                        String groupCreatedLastname = response.body().getData().get(j).getGroupCreatedLastname();
                                        String groupCreatedby = response.body().getData().get(j).getGroupCreatedby();
                                        String createdDate = response.body().getData().get(j).getCreatedDate();

                                        getChatHistoryDetailsPojo1 = new Get_chat_history_details_pojo(redirect_url, histId, sessionid, groupid, groupIcon,
                                                createdOn, typeurId, typestatus, yourFname, yourLname,
                                                lastmessage, lastseen, userid,
                                                fromFirstname, fromLastname, fromPicture, friendid, toFirstname, toLastname, toPicture, toOnlineStatus, toLastvisited, groupName, groupCreatedby, createdDate, groupCreatedFirstname, groupCreatedLastname, getchatgroupmemberlist, 5);

                                        getChatHistoryDetailsPojoArrayList.add(getChatHistoryDetailsPojo1);
                                        chatAdapter.notifyDataSetChanged();



                           /*         chatAdapter = new Chat_adapter(ChatActivity.this, getChatHistoryDetailsPojoArrayList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
                                    rvChat.setLayoutManager(mLayoutManager);
                                    rvChat.setItemAnimator(new DefaultItemAnimator());
//                              rvChat.addItemDecoration(new MyDividerItemDecoration(ChatActivity.this, LinearLayoutManager.VERTICAL, 16));
                                    rvChat.setAdapter(chatAdapter);
                                    rvChat.setNestedScrollingEnabled(false);
                                    chatAdapter.notifyDataSetChanged();*/

//                              progressChat.setVisibility(View.GONE);


                                    }


                                } else {

                                }

                            }
                        }


                    }

                    @Override
                    public void onFailure(Call<Get_chat_history_output> call, Throwable t) {
                        llNodata.setVisibility(View.VISIBLE);
                        progressChat.setVisibility(View.GONE);

                        //Error
                        Log.e(TAG, "FailureError" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Snackbar snackbar = Snackbar.make(chatOverall, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

//Chat history API Loop

    private void get_chat_history_loop() {

        if (Utility.isConnected(ChatActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_chat_history_output> call = apiService.get_chat_history("get_chat_history", Integer.parseInt(createdby));
                call.enqueue(new Callback<Get_chat_history_output>() {
                    @Override
                    public void onResponse(Call<Get_chat_history_output> call, Response<Get_chat_history_output> response) {

                        if (response.body() != null) {
                            if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {

                                Pref_storage.setDetail(ChatActivity.this, "Chat_history_size", String.valueOf(response.body().getData().size()));

                                getChatHistoryDetailsPojoArrayList.clear();
                                getchatgroupmemberlist.clear();


                                for (int j = 0; j < response.body().getData().size(); j++) {

                                    String histId = response.body().getData().get(j).getHistId();
                                    String sessionid = response.body().getData().get(j).getSessionid();
                                    String friendid = response.body().getData().get(j).getFriendid();
                                    String userid = response.body().getData().get(j).getUserid();
                                    String groupid = response.body().getData().get(j).getGroupid();
                                    String groupIcon = response.body().getData().get(j).getGroup_icon();
                                    String typeurId = response.body().getData().get(j).getTypeYourid();
                                    String yourFname = response.body().getData().get(j).getYourFirstname();
                                    String yourLname = response.body().getData().get(j).getYourLastname();
                                    String typestatus = response.body().getData().get(j).getTypeStatus();
                                    String createdOn = response.body().getData().get(j).getCreatedOn();
                                    String lastmessage = response.body().getData().get(j).getLastmessage();
                                    String lastseen = response.body().getData().get(j).getLastseen();
                                    String fromFirstname = response.body().getData().get(j).getFromFirstname();
                                    String fromLastname = response.body().getData().get(j).getFromLastname();
                                    String fromPicture = response.body().getData().get(j).getFromPicture();
                                    String toFirstname = response.body().getData().get(j).getToFirstname();
                                    String toLastname = response.body().getData().get(j).getToLastname();
                                    String toPicture = response.body().getData().get(j).getToPicture();
                                    String toOnlineStatus = response.body().getData().get(j).getToOnlineStatus();
                                    String toLastvisited = response.body().getData().get(j).getToLastvisited();
                                    String groupName = response.body().getData().get(j).getGroupName();
                                    String groupCreatedFirstname = response.body().getData().get(j).getGroupCreatedFirstname();
                                    String groupCreatedLastname = response.body().getData().get(j).getGroupCreatedLastname();
                                    String groupCreatedby = response.body().getData().get(j).getGroupCreatedby();
                                    String createdDate = response.body().getData().get(j).getCreatedDate();

                                    getChatHistoryDetailsPojo1 = new Get_chat_history_details_pojo(redirect_url, histId, sessionid, groupid, groupIcon,
                                            createdOn, typeurId, typestatus, yourFname, yourLname,
                                            lastmessage, lastseen, userid,
                                            fromFirstname, fromLastname, fromPicture, friendid, toFirstname, toLastname, toPicture, toOnlineStatus, toLastvisited, groupName, groupCreatedby, createdDate, groupCreatedFirstname, groupCreatedLastname, getchatgroupmemberlist, 5);

                                    getChatHistoryDetailsPojoArrayList.add(getChatHistoryDetailsPojo1);

//                                    progressChat.setVisibility(View.GONE);

                                    chatAdapter.notifyDataSetChanged();



                           /*         chatAdapter = new Chat_adapter(ChatActivity.this, getChatHistoryDetailsPojoArrayList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
                                    rvChat.setLayoutManager(mLayoutManager);
                                    rvChat.setItemAnimator(new DefaultItemAnimator());
//                              rvChat.addItemDecoration(new MyDividerItemDecoration(ChatActivity.this, LinearLayoutManager.VERTICAL, 16));
                                    rvChat.setAdapter(chatAdapter);
                                    rvChat.setNestedScrollingEnabled(false);
                                    chatAdapter.notifyDataSetChanged();*/

//                              progressChat.setVisibility(View.GONE);


                                }


                            } else {

                            }

                        }


                    }

                    @Override
                    public void onFailure(Call<Get_chat_history_output> call, Throwable t) {
//                        llNodata.setVisibility(View.VISIBLE);

                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Snackbar snackbar = Snackbar.make(chatOverall, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }


    private void get_chat_history_resume() {

        if (Utility.isConnected(ChatActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_chat_history_output> call = apiService.get_chat_history("get_chat_history", Integer.parseInt(createdby));
                call.enqueue(new Callback<Get_chat_history_output>() {
                    @Override
                    public void onResponse(Call<Get_chat_history_output> call, Response<Get_chat_history_output> response) {

                        if (response.body() != null) {

                            Log.e(TAG, "onResponse: data_size" + response.body().getData().size());
                            Log.e(TAG, "onResponse: saved_size" + Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "Chat_history_size")));


                            if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {

                                if (response.body().getData().size() <= (Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "Chat_history_size")))) {
                                 /*   for (int j = 0; j < response.body().getData().size(); j++) {
                                        getChatHistoryDetailsPojoArrayList.clear();
                                        getchatgroupmemberlist.clear();
                                        String hist_id = response.body().getData().get(j).getHistId();
                                        String sessionid = response.body().getData().get(j).getSessionid();
                                        String friendid = response.body().getData().get(j).getFriendid();
                                        String userid = response.body().getData().get(j).getUserid();
                                        String groupid = response.body().getData().get(j).getGroupid();
                                        String groupIcon = response.body().getData().get(j).getGroup_icon();
                                        String typeur_id = response.body().getData().get(j).getTypeYourid();
                                        String your_fname = response.body().getData().get(j).getYourFirstname();
                                        String your_lname = response.body().getData().get(j).getYourLastname();
                                        String typestatus = response.body().getData().get(j).getTypeStatus();
                                        String created_on = response.body().getData().get(j).getCreatedOn();
                                        String lastmessage = response.body().getData().get(j).getLastmessage();
                                        String lastseen = response.body().getData().get(j).getLastseen();
                                        String from_firstname = response.body().getData().get(j).getFromFirstname();
                                        String from_lastname = response.body().getData().get(j).getFromLastname();
                                        String from_picture = response.body().getData().get(j).getFromPicture();
                                        String to_firstname = response.body().getData().get(j).getToFirstname();
                                        String to_lastname = response.body().getData().get(j).getToLastname();
                                        String to_picture = response.body().getData().get(j).getToPicture();
                                        String online_status = response.body().getData().get(j).getToOnlineStatus();
                                        String last_visitied = response.body().getData().get(j).getToLastvisited();
                                        String group_name = response.body().getData().get(j).getGroupName();
                                        String group_created_fname = response.body().getData().get(j).getGroupCreatedFirstname();
                                        String group_created_lname = response.body().getData().get(j).getGroupCreatedLastname();
                                        String groupCreatedby = response.body().getData().get(j).getGroupCreatedby();
                                        String created_date = response.body().getData().get(j).getCreatedDate();


                                        getChatHistoryDetailsPojo1 = new Get_chat_history_details_pojo(hist_id, sessionid, groupid, groupIcon,
                                                created_on, typeur_id, typestatus, your_fname, your_lname,
                                                lastmessage, lastseen, userid,
                                                from_firstname, from_lastname, from_picture, friendid, to_firstname, to_lastname, to_picture, online_status, last_visitied, group_name, groupCreatedby, created_date, group_created_fname, group_created_lname, getchatgroupmemberlist, 5);

                                        getChatHistoryDetailsPojoArrayList.add(getChatHistoryDetailsPojo1);
                                        chatAdapter.notifyDataSetChanged();

                                    }*/
                                } else if (response.body().getData().size() > (Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "Chat_history_size")))) {


                                    Pref_storage.setDetail(getApplicationContext(), "Chat_history_size", String.valueOf(response.body().getData().size()));
                                    getChatHistoryDetailsPojoArrayList.clear();
                                    getchatgroupmemberlist.clear();

                                    for (int j = 0; j < response.body().getData().size(); j++) {

                                        String histId = response.body().getData().get(j).getHistId();
                                        String sessionid = response.body().getData().get(j).getSessionid();
                                        String friendid = response.body().getData().get(j).getFriendid();
                                        String userid = response.body().getData().get(j).getUserid();
                                        String groupid = response.body().getData().get(j).getGroupid();
                                        String groupIcon = response.body().getData().get(j).getGroup_icon();
                                        String typeurId = response.body().getData().get(j).getTypeYourid();
                                        String yourFname = response.body().getData().get(j).getYourFirstname();
                                        String yourLname = response.body().getData().get(j).getYourLastname();
                                        String typestatus = response.body().getData().get(j).getTypeStatus();
                                        String createdOn = response.body().getData().get(j).getCreatedOn();
                                        String lastmessage = response.body().getData().get(j).getLastmessage();
                                        String lastseen = response.body().getData().get(j).getLastseen();
                                        String fromFirstname = response.body().getData().get(j).getFromFirstname();
                                        String fromLastname = response.body().getData().get(j).getFromLastname();
                                        String fromPicture = response.body().getData().get(j).getFromPicture();
                                        String toFirstname = response.body().getData().get(j).getToFirstname();
                                        String toLastname = response.body().getData().get(j).getToLastname();
                                        String toPicture = response.body().getData().get(j).getToPicture();
                                        String onlineStatus = response.body().getData().get(j).getToOnlineStatus();
                                        String toLastvisited = response.body().getData().get(j).getToLastvisited();
                                        String groupName = response.body().getData().get(j).getGroupName();
                                        String groupCreatedFirstname = response.body().getData().get(j).getGroupCreatedFirstname();
                                        String groupCreatedLastname = response.body().getData().get(j).getGroupCreatedLastname();
                                        String groupCreatedby = response.body().getData().get(j).getGroupCreatedby();
                                        String createdDate = response.body().getData().get(j).getCreatedDate();


                                        getChatHistoryDetailsPojo1 = new Get_chat_history_details_pojo(redirect_url, histId, sessionid, groupid, groupIcon,
                                                createdOn, typeurId, typestatus, yourFname, yourLname,
                                                lastmessage, lastseen, userid,
                                                fromFirstname, fromLastname, fromPicture, friendid, toFirstname, toLastname, toPicture, onlineStatus, toLastvisited, groupName, groupCreatedby, createdDate, groupCreatedFirstname, groupCreatedLastname, getchatgroupmemberlist, 5);


                                        getChatHistoryDetailsPojoArrayList.add(getChatHistoryDetailsPojo1);
                                        chatAdapter.notifyDataSetChanged();


//                                             chatAdapter = new Chat_adapter(ChatActivity.this, getChatHistoryDetailsPojoArrayList);
//                                             RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
//                                             rvChat.setLayoutManager(mLayoutManager);
//                                             rvChat.setItemAnimator(new DefaultItemAnimator());
////                                       rvChat.addItemDecoration(new MyDividerItemDecoration(ChatActivity.this, LinearLayoutManager.VERTICAL, 16));
//                                             rvChat.setAdapter(chatAdapter);
//                                             rvChat.setNestedScrollingEnabled(false);
//                                             chatAdapter.notifyDataSetChanged();
//                                       progressChat.setVisibility(View.GONE);


                                    }


                                }

                            }
                        } else {

                        }


                    }

                    @Override
                    public void onFailure(@NonNull Call<Get_chat_history_output> call, @NonNull Throwable t) {
//                        llNodata.setVisibility(View.VISIBLE);

                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Snackbar snackbar = Snackbar.make(chatOverall, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();


//        chatAdapter = new Chat_adapter(ChatActivity.this, getChatHistoryDetailsPojoArrayList);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
//        rvChat.setLayoutManager(mLayoutManager);
//        rvChat.setItemAnimator(new DefaultItemAnimator());
//        rvChat.setAdapter(chatAdapter);
//
//
//
//        get_chat_history_resume();

    /*    if (getChatHistoryDetailsPojoArrayList.size() == 0) {
            getChatHistory();
        } else {
            chatAdapter = new Chat_adapter(ChatActivity.this, getChatHistoryDetailsPojoArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
            rvChat.setLayoutManager(mLayoutManager);
            rvChat.setHasFixedSize(true);
            rvChat.setItemAnimator(new DefaultItemAnimator());
            rvChat.setAdapter(chatAdapter);
//            rvChat.setNestedScrollingEnabled(false);
            chatAdapter.notifyDataSetChanged();
        }*/


    }


    //size based nodata visibility

    public void sizecheck(int size) {

        Log.e(TAG, "sizecheck: " + size);

        if (size == 0) {

//            llNodata.setVisibility(View.VISIBLE);

//            editSearchChat.setVisibility(View.GONE);


        } else {

//            llNodata.setVisibility(View.GONE);

//            editSearchChat.setVisibility(View.VISIBLE);

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        mHandler.removeCallbacksAndMessages(null);
        timerTask.cancel();
        timer.cancel();
        timer.purge();

//        mHandler.removeCallbacksAndMessages(null);
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        getMenuInflater().inflate(R.menu.search, menu);
//
//        // Associate searchable configuration with the SearchView
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        searchView = (SearchView) menu.findItem(R.id.action_search)
//                .getActionView();
//        searchView.setSearchableInfo(searchManager
//                .getSearchableInfo(getComponentName()));
//        searchView.setMaxWidth(Integer.MAX_VALUE);
//        searchView.setQueryHint("Search..");
//
//        // listening to search query text change
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                // filter recycler view when query submitted
//
//
//                chatAdapter.getFilter().filter(query);
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String query) {
//
//                mHandler.removeCallbacksAndMessages(null);
//                timerTask.cancel();
//                timer.cancel();
//                timer.purge();
//                Log.e(TAG, "onQueryTextChange: "+query );
//                // filter recycler view when text is changed
//                chatAdapter.getFilter().filter(query);
//                return false;
//            }
//        });
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_search) {
//            return true;
//        }
//
//        if (id == android.R.id.home) {
//
//            this.finish();
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        mHandler.removeCallbacksAndMessages(null);
        timerTask.cancel();
        timer.cancel();
        timer.purge();
//        mHandler.removeCallbacksAndMessages(null);
        /*overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);*/
        //Intent intent = new Intent(ChatActivity.this, Home.class);
        //intent.putExtra("redirect", "profile");
        //startActivity(intent);

        finish();
        // overridePendingTransition(R.anim.swipe_lefttoright, R.anim.swipe_righttoleft);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_right);

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {

            case R.id.back:
               /* Intent intent = new Intent(ChatActivity.this, Home.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);*/

              /* if(notificationtype==null)
               {*/
                //  Intent intent = new Intent(ChatActivity.this, Home.class);
                //intent.putExtra("redirect", "profile");
                // startActivity(intent);
               /*}else
               {
                 //  finish();
               }*/
                finish();
                // overridePendingTransition(R.anim.swipe_lefttoright, R.anim.swipe_righttoleft);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_right);

                break;

            case R.id.rl_single_btn:

                startActivity(new Intent(ChatActivity.this, chatfollowerslistsingle.class));
//                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                break;


            case R.id.rl_grp_btn:

                startActivity(new Intent(ChatActivity.this, Chat_followers_list_group.class));
//                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                break;


            case R.id.img_chat_add:

                startActivity(new Intent(ChatActivity.this, chatfollowerslistsingle.class));
//                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                break;


            case R.id.img_chat_add_group:
                startActivity(new Intent(ChatActivity.this, Chat_followers_list_group.class));
//                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                break;
            default:
                break;

        }


    }
}
