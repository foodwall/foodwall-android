package com.kaspontech.foodwall.chatpackage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.kaspontech.foodwall.adapters.chat.Chat_follower_adapter_single;
import com.kaspontech.foodwall.menuPackage.FindFriends;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_follower_output_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.adapters.chat.Chat_follower_adapter_single.single_frd_id;

public class chatfollowerslistsingle extends AppCompatActivity implements View.OnClickListener{

    /**
     * Application TAG
     **/
    private static final String TAG = "Chat_followers_list";

    /**
     * Action Bar
     **/
    Toolbar actionBar;

    /**
     * Toolbar title
     **/
    TextView toolbarTitle;

    /*
    * Edit text search follower

    * */
    EditText editSearchFollowers;
    /**
     * Toolbar title
     **/
    TextView toolbarNext;
    /**
     * Back buton
     **/
    ImageButton back;
    /**
     * Image button next
     **/
    ImageButton followerNext;
    /**
     * Followers layout
     **/

    RelativeLayout rlFollowerlayChat;
    /**
     * Recyclerview for followers
     **/
    RecyclerView rvFollowersChat;

    /**
     * Loader
     **/

    ProgressBar progressChatFollower;
    /**
     * Chat followers array list
     **/

    List<Get_followerlist_pojo> getChatFollowerPojoArrayList = new ArrayList<>();

    /**
     * Chat follower adapter
     **/

    Chat_follower_adapter_single chatFollowingAdapter;

    /**
     * No data available linear layout
     **/

    LinearLayout llNodata;

    /*
    * Lottie animation */

    LottieAnimationView animFriends;
    /**
     * Friend id
     **/

    int  friendid;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_followers_list);

        actionBar = findViewById(R.id.actionbar_chat_follower);
        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);


        followerNext = actionBar.findViewById(R.id.follower_next);
        followerNext.setOnClickListener(this);

        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(R.string.new_message);

        toolbarNext = actionBar.findViewById(R.id.toolbar_next);
        toolbarNext.setVisibility(View.GONE);
        toolbarNext.setOnClickListener(this);

        llNodata = findViewById(R.id.ll_nodata);
        llNodata.setOnClickListener(this);

        editSearchFollowers = findViewById(R.id.editSearchFollowers);
        editSearchFollowers.setVisibility(View.GONE);

        rlFollowerlayChat = findViewById(R.id.rl_followerlay_chat);

        progressChatFollower = findViewById(R.id.progress_chat_follower);

        rvFollowersChat = findViewById(R.id.rv_followers_chat);


        editSearchFollowers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.toString().equalsIgnoreCase("")){

                    /*Settings adapter*/
//                    chatFollowingAdapter.getFilter().filter(s.toString());



                    chatFollowingAdapter = new Chat_follower_adapter_single(chatfollowerslistsingle.this, getChatFollowerPojoArrayList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(chatfollowerslistsingle.this);
                    rvFollowersChat.setLayoutManager(mLayoutManager);
                    rvFollowersChat.setHasFixedSize(true);
                    rvFollowersChat.setItemAnimator(new DefaultItemAnimator());
                    rvFollowersChat.setAdapter(chatFollowingAdapter);
                    rvFollowersChat.setNestedScrollingEnabled(false);
                    chatFollowingAdapter.notifyDataSetChanged();
                    rvFollowersChat.setVisibility(View.VISIBLE);
                    progressChatFollower.setVisibility(View.GONE);

                }else {

                    chatFollowingAdapter.getFilter().filter(s.toString());
                }

            }
        });


        /*Get followers api calling*/

        getFollowers();


    }


    /*Get followers api calling*/

    private void getFollowers() {

        if (Utility.isConnected(chatfollowerslistsingle.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                String userId = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_follower_output_pojo> call = apiService.get_chat_follower("get_follower", Integer.parseInt(userId));
                call.enqueue(new Callback<Get_follower_output_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Get_follower_output_pojo> call, @NonNull Response<Get_follower_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                if (response.body().getData().get(j).getFollowerId() == null) {

                                } else {


                                    getChatFollowerPojoArrayList = response.body().getData();


                                    /*Settings adapter*/

                                    chatFollowingAdapter = new Chat_follower_adapter_single(chatfollowerslistsingle.this, getChatFollowerPojoArrayList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(chatfollowerslistsingle.this);
                                    rvFollowersChat.setLayoutManager(mLayoutManager);
                                    rvFollowersChat.setHasFixedSize(true);
                                    rvFollowersChat.setItemAnimator(new DefaultItemAnimator());
                                    rvFollowersChat.setAdapter(chatFollowingAdapter);
                                    rvFollowersChat.setNestedScrollingEnabled(false);
                                    chatFollowingAdapter.notifyDataSetChanged();
                                    rvFollowersChat.setVisibility(View.VISIBLE);
                                    progressChatFollower.setVisibility(View.GONE);
                                }

                            }

//                            if(!getChatFollowerPojoArrayList.isEmpty()){
//
//                                editSearchFollowers.setVisibility(View.VISIBLE);
//
//                            }
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Get_follower_output_pojo> call, @NonNull Throwable t) {

                        llNodata.setVisibility(View.VISIBLE);
                        progressChatFollower.setVisibility(View.GONE);
//                        editSearchFollowers.setVisibility(View.GONE);
                        //Error
                        Log.e(TAG, "FailureError" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Snackbar snackbar = Snackbar.make(rlFollowerlayChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }



    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

        single_frd_id.clear();

        finish();
    }

    @Override
    public void onClick(View v) {
        int i= v.getId();

        switch (i){

            case R.id.back:

                single_frd_id.clear();

                finish();
                break;

            case R.id.ll_nodata:

                startActivity(new Intent(chatfollowerslistsingle.this, FindFriends.class));

                break;


                case R.id.toolbar_next:

                    if(single_frd_id.isEmpty()){

                        Toast.makeText(this, "Atleast 1 member required to create a chat", Toast.LENGTH_SHORT).show();

                    }else{

                        startActivity(new Intent(chatfollowerslistsingle.this,Create_new_chat_single.class));
                        finish();

                    }

                break;


                case R.id.follower_next:

                    if(single_frd_id.isEmpty()){


                        Toast.makeText(this, "Atleast 1 member required to create a chat", Toast.LENGTH_SHORT).show();
                    }else{

                        startActivity(new Intent(chatfollowerslistsingle.this,Create_new_chat_single.class));
                        finish();

                    }
                    break;

                    default:
                        break;

        }

    }
}
