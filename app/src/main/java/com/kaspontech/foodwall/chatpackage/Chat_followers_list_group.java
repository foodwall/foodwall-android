package com.kaspontech.foodwall.chatpackage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.gson.Gson;
import com.kaspontech.foodwall.adapters.chat.Chat_follower_adapter;
import com.kaspontech.foodwall.adapters.chat.Chat_follower_adapter_single;
import com.kaspontech.foodwall.menuPackage.FindFriends;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_follower_output_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.adapters.chat.Chat_follower_adapter.friendsList_chat;

public class Chat_followers_list_group extends AppCompatActivity implements View.OnClickListener{



    /**
     * Application TAG
     **/
    private static final String TAG = "Chat_followers_list";


    /**
     * Action Bar
     **/
    Toolbar actionBar;

    /**
     * Tool bar title
     **/
    TextView toolbarTitle;

    /**
     * Tool bar next
     **/

    TextView toolbarNext;
    /**
     * Image button back
     **/

    ImageButton back;
    /**
     * Image button follow next
     **/

    ImageButton followerNext;

    /**
     * Relative layout for follower
     **/

    RelativeLayout rlFollowerlayChat;

    /**
     * Recyclerview for follower
     **/
    RecyclerView rvFollowersChat;
    /**
     * Loader
     **/

    ProgressBar progressChatFollower;


    /**
     * Following pojo array list
     **/
    List<Get_followerlist_pojo> getChatFollowerPojoArrayList = new ArrayList<>();

    /**
     * Chat Following adapter
     **/
    Chat_follower_adapter chatFollowingAdapter;

    /**
     * Chat Following array list
     **/

    ArrayList<Integer> clickedfollowerlistid = new ArrayList<>();


    ArrayList<String> clickedfollowerlistidNew = new ArrayList<>();

    /**
     * Select Followers array list
     **/

    HashMap<String, Integer> selectedfollowerlistId = new HashMap<>();

    /**
     * No data linear layout
     **/

    /*
     * Edit text search follower

     * */
    EditText editSearchFollowers;


    LinearLayout llNodata;

    /*
     * Lottie animation */

    LottieAnimationView animFriends;

    /**
     * int variable's - friend id
     **/
    int  friendid;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_followers_list);

        /*Widget initialization*/

        actionBar = findViewById(R.id.actionbar_chat_follower);

        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        followerNext = actionBar.findViewById(R.id.follower_next);
        followerNext.setOnClickListener(this);

        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(R.string.new_group);

        toolbarNext = actionBar.findViewById(R.id.toolbar_next);



        toolbarNext.setOnClickListener(this);
        toolbarNext.setClickable(true);
        toolbarNext.setFocusable(true);


        rlFollowerlayChat = findViewById(R.id.rl_followerlay_chat);

        llNodata = findViewById(R.id.ll_nodata);
        llNodata.setOnClickListener(this);

        editSearchFollowers = findViewById(R.id.editSearchFollowers);
        editSearchFollowers.setVisibility(View.GONE);

        progressChatFollower = findViewById(R.id.progress_chat_follower);

        rvFollowersChat = findViewById(R.id.rv_followers_chat);




        editSearchFollowers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


//                chatFollowingAdapter.getFilter().filter(s.toString());


//
                if(s.toString().equalsIgnoreCase("")){
                    /*Settings adapter*/


                    chatFollowingAdapter = new Chat_follower_adapter(Chat_followers_list_group.this, getChatFollowerPojoArrayList, selectedfollowerlistId);
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(Chat_followers_list_group.this);
                    rvFollowersChat.setLayoutManager(mLayoutManager);
                    rvFollowersChat.setHasFixedSize(true);
                    rvFollowersChat.setItemAnimator(new DefaultItemAnimator());
                    rvFollowersChat.setAdapter(chatFollowingAdapter);
                    rvFollowersChat.setNestedScrollingEnabled(false);
                    chatFollowingAdapter.notifyDataSetChanged();
                    rvFollowersChat.setVisibility(View.VISIBLE);
                    progressChatFollower.setVisibility(View.GONE);



                }else {

                    chatFollowingAdapter.getFilter().filter(s.toString());

                    /* Calling get followers API */

//                    getFollowers();
                }

            }
        });


        /* Calling get followers API */

        getFollowers();



    }





    /* Get_followers */

    private void getFollowers() {

        if (Utility.isConnected(Chat_followers_list_group.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String userId = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_follower_output_pojo> call = apiService.get_chat_follower("get_follower", Integer.parseInt(userId));
                call.enqueue(new Callback<Get_follower_output_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Get_follower_output_pojo> call, @NonNull Response<Get_follower_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {

                                getChatFollowerPojoArrayList = response.body().getData();


                                /*Setting adapter*/

                                chatFollowingAdapter = new Chat_follower_adapter(Chat_followers_list_group.this, getChatFollowerPojoArrayList, selectedfollowerlistId);
                                LinearLayoutManager mLayoutManager = new LinearLayoutManager(Chat_followers_list_group.this);
                                rvFollowersChat.setLayoutManager(mLayoutManager);
                                rvFollowersChat.setHasFixedSize(true);
                                rvFollowersChat.setItemAnimator(new DefaultItemAnimator());
                                rvFollowersChat.setAdapter(chatFollowingAdapter);
                                rvFollowersChat.setNestedScrollingEnabled(false);
                                chatFollowingAdapter.notifyDataSetChanged();
                                rvFollowersChat.setVisibility(View.VISIBLE);
                                progressChatFollower.setVisibility(View.GONE);
//
//                                if(!getChatFollowerPojoArrayList.isEmpty()){
//
//                                    editSearchFollowers.setVisibility(View.VISIBLE);
//
//                                }



                            }
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Get_follower_output_pojo> call, @NonNull Throwable t) {

                        llNodata.setVisibility(View.VISIBLE);

//                        editSearchFollowers.setVisibility(View.GONE);

                        if(getChatFollowerPojoArrayList.isEmpty()){

                         editSearchFollowers.setVisibility(View.GONE);

                            toolbarNext.setVisibility(View.GONE);
                        }


                        progressChatFollower.setVisibility(View.GONE);


//                        Snackbar snackbar = Snackbar.make(rlFollowerlayChat, "Loading failed. Please try later.", Snackbar.LENGTH_LONG);
//                        snackbar.setActionTextColor(Color.RED);
//                        View view1 = snackbar.getView();
//                        TextView textview = view1.findViewById(R.id.snackbar_text);
//                        textview.setTextColor(Color.WHITE);
//                        snackbar.show();

                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Snackbar snackbar = Snackbar.make(rlFollowerlayChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }



    }



    //size based nodata visibility

//    public void sizecheck( int size) {
//
//        Log.e("Followers", "sizecheck: "+ size);
//
//        if(size==0){
//            llNodata.setVisibility(View.VISIBLE);
////            editSearchFollowers.setVisibility(View.GONE);
//
//
//        }else {
//            llNodata.setVisibility(View.GONE);
////            editSearchFollowers.setVisibility(View.VISIBLE);
//
//        }
//    }




    /* Interface for group selecting */

    public void getgrpmemberList(View view, int position) {


        String followerName;

        followerName = getChatFollowerPojoArrayList.get(position).getFirstName() + " " + getChatFollowerPojoArrayList.get(position).getLastName();

        if (((CheckBox) view).isChecked()) {

            if (selectedfollowerlistId.containsValue(Integer.valueOf(getChatFollowerPojoArrayList.get(position).getFollowerId()))) {

                return;

            } else {

                selectedfollowerlistId.put(followerName, Integer.valueOf(getChatFollowerPojoArrayList.get(position).getFollowerId()));

//                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(),"userId"));


                String id = "\""+getChatFollowerPojoArrayList.get(position).getFollowerId().concat("\"");

                clickedfollowerlistidNew.add("\""+getChatFollowerPojoArrayList.get(position).getFollowerId().concat("\""));

                clickedfollowerlistid.add(Integer.valueOf(getChatFollowerPojoArrayList.get(position).getFollowerId()));

                String json = new Gson().toJson(clickedfollowerlistid);
                JSONArray jsArray = new JSONArray(clickedfollowerlistid);

                Log.e(TAG, "getSelectedfollower:"+selectedfollowerlistId.toString());
                Log.e(TAG, "followerlistid:"+clickedfollowerlistid.toString() );
                Log.e(TAG, "followerlistidnew:"+ clickedfollowerlistidNew.toString());
                Log.e(TAG, "json:"+json );
                Log.e(TAG, "id:"+id );
                Log.e(TAG, "jsArray:"+jsArray );



//                followerlistid.add(userid);

            }


        } else {
            clickedfollowerlistid.remove(Integer.valueOf(getChatFollowerPojoArrayList.get(position).getFollowerId()));
            clickedfollowerlistidNew.remove("\""+getChatFollowerPojoArrayList.get(position).getFollowerId().concat("\""));
            selectedfollowerlistId.remove(followerName);

        }

    }


    @Override
    public void onClick(View v) {
        int i= v.getId();
        switch (i){


            case R.id.back:

                finish();

                break;

            /*Chat group create redirecting*/

            case R.id.toolbar_next:

                if(clickedfollowerlistid.isEmpty()){

                    Toast.makeText(this, "Atleast 1 member required to create a group", Toast.LENGTH_SHORT).show();

                } else {

                    Intent intent = new Intent(Chat_followers_list_group.this,New_chat_activity.class);
                    intent.putExtra("followerid_array",clickedfollowerlistid.toString());
                    intent.putExtra("followerid_array_new", clickedfollowerlistidNew.toString());
                    startActivity(intent);
                    finish();
                }
                break;

                /*Chat activity redirecting*/
                case R.id.follower_next:

                startActivity(new Intent(Chat_followers_list_group.this,New_chat_activity.class));

                finish();

                break;

            case R.id.ll_nodata:
                startActivity(new Intent(Chat_followers_list_group.this, FindFriends.class));
                break;

            default:

                break;

        }




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        friendsList_chat.clear();
        finish();
    }
}
