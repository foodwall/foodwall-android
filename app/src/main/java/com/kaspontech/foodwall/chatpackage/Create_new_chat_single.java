package com.kaspontech.foodwall.chatpackage;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kaspontech.foodwall.adapters.chat.Chat_muliti_view_adapter;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_details_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_history_single_details;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_history_single_output;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Chat_output_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

public class Create_new_chat_single extends AppCompatActivity implements View.OnClickListener {

    /**
     * Application TAG
     **/
    private static final String TAG = "Create_new_chat_single";
    /**
     * Action Bar
     **/
    Toolbar actionBar;

    TextView chatUsername;

    TextView chatActive;
    /**
     * Image button back
     **/
    ImageButton back;
    /**
     * Image button send
     **/
    ImageButton imgSend;
    /**
     * Edit text chat single
     **/

    EditText edittxtChatSingle;
    /**
     * Image view user photo
     **/
    ImageView userphoto_chat;
    /**
     * Image view chat user photo
     **/
    ImageView img_chat_user;
    /**
     * Recycler view single chat
     **/
    RecyclerView rv_single_chat;
    /**
     * Linear layout manager
     **/
    LinearLayoutManager llm;
    /**
     * Arraylists
     **/
    ArrayList<Chat_history_single_details> getChatHistoryDetailsPojoArrayList = new ArrayList<>();
    ArrayList<Chat_history_single_details> getChatHistoryDetailsPojoArrayList_updated = new ArrayList<>();
    ArrayList<Chat_details_pojo> chatDetailsPojoArrayList = new ArrayList<>();

    int groupid;
    int sessionid;
    int sessionidReply;
    int friendid;

    /**
     * Loader
     **/
    ProgressBar progressChatSingle;
    /**
     * Adapter class
     **/
    Chat_muliti_view_adapter chatMulitiViewAdapter;
    /**
     * Chat pojo class
     **/
    Chat_details_pojo chatDetailsPojo;
    /**
     * String results
     **/
    String touserid;
    String picture;
    String topicture;
    String username;
    String toonline;
    String lastseen;
    String imei;
    String encodedEdittext;

    /**
     * Edit text with emoji
     **/
    EmojiconEditText emojiconEditText;
    EmojiconTextView emojiconTextView;
    EmojIconActions emojIcon;
    /**
     * View
     **/

    View rlSingleChat;


    @SuppressLint("HardwareIds")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_chat);

        /* Widget initialization */

        actionBar = findViewById(R.id.actionbar_single_chat);
        back = actionBar.findViewById(R.id.back);
        chatUsername = actionBar.findViewById(R.id.chat_username);
        chatActive = actionBar.findViewById(R.id.chat_active);

        /*Image button*/

        imgSend = findViewById(R.id.img_send);
        back.setOnClickListener(this);
        imgSend.setOnClickListener(this);

        /*Image view*/
        userphoto_chat = findViewById(R.id.userphoto_chat);
        img_chat_user = actionBar.findViewById(R.id.img_chat_user);
        userphoto_chat.setOnClickListener(this);


        rlSingleChat = findViewById(R.id.rl_single_chat);
        /*Recyclerview chat*/
        rv_single_chat = findViewById(R.id.rv_single_chat);

        progressChatSingle = findViewById(R.id.progress_chat_single);
        progressChatSingle.setVisibility(View.GONE);


        emojiconEditText = findViewById(R.id.emojicon_edit_text);
        emojiconEditText.setImeOptions(EditorInfo.IME_ACTION_GO);

        /*Emoji edit text setting*/
        emojIcon = new EmojIconActions(Create_new_chat_single.this, rlSingleChat, emojiconEditText, userphoto_chat);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e(TAG, "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
                Log.e(TAG, "Keyboard closed");
            }
        });

        edittxtChatSingle = findViewById(R.id.edittxt_chat_single);

        //Real time typing status
        emojiconEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                /*Send button visibility*/
                if (emojiconEditText.getText().toString().length() != 0) {
                    imgSend.setVisibility(View.VISIBLE);
                } else {
                    imgSend.setVisibility(View.GONE);
                }
            }
        });

        /* IME action send */
        emojiconEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_GO) {
                    encodedEdittext = org.apache.commons.text.StringEscapeUtils.escapeJava(emojiconEditText.getText().toString().trim());
                    createIndividualChat();

                }
                return false;
            }
        });



        /*Getting input values*/

        Intent intent = getIntent();


        username = intent.getStringExtra("username");
        friendid = Integer.parseInt(intent.getStringExtra("friendid"));

        Log.e("friendid", "friendid" + friendid);

        picture = intent.getStringExtra("picture");


        userphoto_chat.setImageResource(R.drawable.ic_greateorange);
        chatUsername.setText(username);

        /*Profile image loader*/
        Utility.picassoImageLoader(picture,
                0, img_chat_user, getApplicationContext());


//        new check_message_background().execute("");

       /* final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                get_chat_history_single_chat();
                get_chat_typing_status();

                try {
                    if (Integer.parseInt(toonline) == 1) {
                        chatActive.setText(R.string.active_now);
                    } else {

                        lastvisited(lastseen);

                        Log.e("lastseen-->",""+lastseen);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                handler.postDelayed(this, 1000);
            }
        }, 2000);*/

        try {
            if (Integer.parseInt(toonline) == 1) {

                chatActive.setText(R.string.active_now);

            } else {

                lastvisited(lastseen);

                Log.e("lastseen-->", "" + lastseen);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    /* Chat last visited */
    public void lastvisited(String lastseen) {


        try {
            //Input time
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long time = sdf.parse(lastseen).getTime();

            //Cuurent system time

            Date currentTime = Calendar.getInstance().getTime();
            SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String now_new = sdformat.format(currentTime);
            long nowdate = sdformat.parse(now_new).getTime();

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(time, nowdate, DateUtils.FORMAT_ABBREV_RELATIVE);

            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago") || relavetime1.toString().equalsIgnoreCase("In 0 minutes")) {
                chatActive.setText(R.string.just_now);

            } else if (relavetime1.toString().contains("hours ago")) {
                chatActive.setText("Last seen " + relavetime1.toString());
            } else if (relavetime1.toString().contains("days ago")) {
                chatActive.setText("Last seen " + relavetime1.toString());
            } else {
                chatActive.setText(relavetime1);
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
    }



    /* Updating chat history */

    private void getChatHistorySingle() {

        if (Utility.isConnected(Create_new_chat_single.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                Call<Chat_history_single_output> call = apiService.get_chat_history_single("get_chat_history_single", userid, groupid, sessionid);
                call.enqueue(new Callback<Chat_history_single_output>() {
                    @Override
                    public void onResponse(@NonNull Call<Chat_history_single_output> call, @NonNull Response<Chat_history_single_output> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {
                            getChatHistoryDetailsPojoArrayList.clear();

                            for (int j = 0; j < response.body().getData().size(); j++) {

                                String chatid = response.body().getData().get(j).getChatid();
                                String sessionId = response.body().getData().get(j).getSessionid();

                                sessionidReply = Integer.parseInt(sessionId);

                                String fromuserid = response.body().getData().get(j).getFromuserid();
                                String groupId = response.body().getData().get(j).getGroupid();
                                groupid = Integer.parseInt(groupId);

                                String tolastvisited = response.body().getData().get(j).getToLastvisited();
                                String createdOn = response.body().getData().get(j).getCreatedOn();
                                String message = response.body().getData().get(j).getMessage();
                                String status = response.body().getData().get(j).getStatus();
                                String response2 = response.body().getData().get(j).getResponse();
                                String msgdate = response.body().getData().get(j).getMsgDate();
                                String lastSeen = response.body().getData().get(j).getLastseen();
                                String fromFirstname = response.body().getData().get(j).getFromFirstname();
                                String fromLastname = response.body().getData().get(j).getFromLastname();
                                String fromPicture = response.body().getData().get(j).getFromPicture();
                                String toFirstname = response.body().getData().get(j).getToFirstname();
                                String toLastname = response.body().getData().get(j).getToLastname();
                                String toPicture = response.body().getData().get(j).getToPicture();
                                String groupName = response.body().getData().get(j).getGroupName();
                                String groupCreatedby = response.body().getData().get(j).getGroupCreatedby();
                                String createdDate = response.body().getData().get(j).getCreatedDate();
                                String deleted_message = response.body().getData().get(j).getDeleted_message();


                                toonline = response.body().getData().get(j).getToOnlineStatus();


                                String typeMessage = response.body().getData().get(j).getTypeMessage();
                                String typeStatus = response.body().getData().get(j).getTypeStatus();
                                String groupIcon = response.body().getData().get(j).getGroupIcon();
                                String changedWhich = response.body().getData().get(j).getChangedWhich();
                                String reviewid = response.body().getData().get(j).getReviewid();
                                String timelineid = response.body().getData().get(j).getTimelineId();


                                Chat_history_single_details chatHistorySingleDetails1 = new Chat_history_single_details(chatid,
                                        sessionId, groupId, fromuserid, touserid,
                                        message, typeMessage, status, response2, msgdate, createdOn, lastSeen,
                                        reviewid, timelineid, fromFirstname, fromLastname, fromPicture, toFirstname,
                                        toLastname, toPicture, toonline,
                                        tolastvisited, groupName, groupIcon, changedWhich, groupCreatedby, createdDate, "", "", "",
                                        typeStatus, "", "", "", "", "", deleted_message);



                                /*Setting adapter*/
                                if (deleted_message.equalsIgnoreCase("0")) {

                                    getChatHistoryDetailsPojoArrayList.add(chatHistorySingleDetails1);
                                }

                                chatMulitiViewAdapter = new Chat_muliti_view_adapter(Create_new_chat_single.this, getChatHistoryDetailsPojoArrayList/*,chatDetailsPojoArrayList*/);
                                llm = new LinearLayoutManager(Create_new_chat_single.this);
                                rv_single_chat.setLayoutManager(llm);
                                llm.setStackFromEnd(true);
                                rv_single_chat.setHasFixedSize(true);
                                rv_single_chat.setItemAnimator(new DefaultItemAnimator());
                                rv_single_chat.setAdapter(chatMulitiViewAdapter);
                                rv_single_chat.smoothScrollToPosition(getChatHistoryDetailsPojoArrayList.size() + 1);
                                rv_single_chat.setNestedScrollingEnabled(false);
                                chatMulitiViewAdapter.notifyDataSetChanged();


                            }
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Chat_history_single_output> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlSingleChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }


    /* Create Individual Chat api calling */

    private void createIndividualChat() {

        if (Utility.isConnected(Create_new_chat_single.this)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Chat_output_pojo> call = apiService.create_individual_chat("create_individual_chat", Integer.parseInt(createdby), friendid, encodedEdittext);
                call.enqueue(new Callback<Chat_output_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Chat_output_pojo> call, @NonNull Response<Chat_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {
                            emojiconEditText.setText("");

                            for (int j = 0; j < response.body().getData().size(); j++) {

                                chatDetailsPojo = new Chat_details_pojo("", "", "", "", "", 0, "");


                                String groupId = response.body().getData().get(j).getGroupid();
                                sessionid = response.body().getData().get(j).getSessionid();
                                groupid = Integer.parseInt(groupId);


                                /*Calling chat historu api*/
                                getChatHistorySingle();

                            }
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Chat_output_pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Snackbar snackbar = Snackbar.make(rlSingleChat, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    //GetRunningAppProcesses
    boolean isNamedProcessRunning(String processName) {
        if (processName == null)
            return false;

        ActivityManager manager =
                (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processes = manager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo process : processes) {
            if (processName.equals(process.processName)) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void onClick(View v) {

        int newid = v.getId();

        switch (newid) {
            case R.id.img_send:
                //Send button Animation

                final Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.modal_in);
                v.startAnimation(anim);

                encodedEdittext = org.apache.commons.text.StringEscapeUtils.escapeJava(emojiconEditText.getText().toString().trim());

                createIndividualChat();

                break;
            case R.id.back:
                startActivity(new Intent(Create_new_chat_single.this, ChatActivity.class));
                /*overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);*/
                finish();
                break;

            case R.id.userphoto_chat:


                break;

            default:
                break;

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Create_new_chat_single.this, ChatActivity.class));
        finish();
    }


}
