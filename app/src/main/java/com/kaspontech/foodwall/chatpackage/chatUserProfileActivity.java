package com.kaspontech.foodwall.chatpackage;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.adapters.chat.Chat_user_grp_adapter;
import com.kaspontech.foodwall.adapters.chat.Group_bucketAdapter;
import com.kaspontech.foodwall.bucketlistpackage.GetmyBucketInputPojo;
import com.kaspontech.foodwall.bucketlistpackage.GetmybucketOutputpojo;
import com.kaspontech.foodwall.foodFeedsPackage.TotalCountModule_Response;
import com.kaspontech.foodwall.menuPackage.UserMenu;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.Get_grp_bucket_pojo;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.Get_grp_input_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ClearChatResponse_Pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.RecyclerTouchListener;
import com.kaspontech.foodwall.utills.Utility;
import com.kaspontech.foodwall.utills.ViewImage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//


/**
 * Created by Vishnu on 30-06-2018.
 */

public class chatUserProfileActivity extends AppCompatActivity implements View.OnClickListener {
    /*
     * Widgets
     * */


    /*
     * Chat layout
     * */

    CoordinatorLayout coChatLayout;
    /*
     * App bar chat layout
     * */
    AppBarLayout appBarLayout;
    /*
     * Collapsing bar chat layout
     * */
    CollapsingToolbarLayout collapsingToolbarLayout;
    /*
     * Image view user chat
     * */
    ImageView imgUsrProChat;

    ImageView imgUsrPro;

    /*
     * Chat tool bar
     * */
    android.support.v7.widget.Toolbar chatToolbar;

    /*
     * Chat progress bar
     * */
    ProgressBar viewChatProgress;

    /*
     * Nested scroll view
     * */
    NestedScrollView chatScroll;
    /*
     * Recyclerview individual chat
     * */

    RecyclerView rvIndiChat;

    /*
     * Recyclerview individual group
     * */

    RecyclerView rvIndiGrp;

    /*
     * Text view widgets
     * */

    TextView idTxtAbout;
    TextView txtCreatedon;
    TextView txtBucket;
    TextView txtGrpCommon;

    /*
     * String results
     * */
    String proUsername;
    String proPicture;
    String proCreatedon;
    String proCreatedby;
    String proFriendid,groupid,userid,sessionid,created_by;


    /*
     * Group bucket adapter
     * */
    Group_bucketAdapter groupBucketAdapter;
    /*
     * User bucket adapter
     * */
    Chat_user_grp_adapter chatUserGrpAdapter;

    /*
     * Group input pojo
     * */
    Get_grp_input_pojo getGrpInputPojo;

    /*
     * Group input arraylist
     * */
    ArrayList<GetmyBucketInputPojo> getmyBucketInputPojoArrayList = new ArrayList<>();

    ArrayList<Get_grp_input_pojo> getGrpInputPojoArrayList = new ArrayList<>();


    Button clear_chat,exit_group,btn_goto_profile;

    TextView txt_nodata;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_user_profile_layout);

        /*Widget initialization*/

        initComponents();


        setSupportActionBar(chatToolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        chatToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        /*Getting input values*/

        Intent intent = getIntent();
        proUsername = intent.getStringExtra("pro_username");
        proPicture = intent.getStringExtra("pro_picture");
        proFriendid = intent.getStringExtra("pro_friendid");
        proCreatedon = intent.getStringExtra("pro_createdon");
        proCreatedby = intent.getStringExtra("pro_createdby");
        userid = intent.getStringExtra("userid");
        sessionid = intent.getStringExtra("sessionid");
        groupid = intent.getStringExtra("groupid");
        created_by = intent.getStringExtra("created_by");


        Log.e("responseStatus", "session->" + sessionid);
        Log.e("responseStatus", "groupid->" + groupid);
        Log.e("responseStatus", "userid->" + userid);
        Log.e("responseStatus", "proFriendidviji->" + proFriendid);



//        makeCollapsingToolbarLayoutLooksGood(collapsingToolbarLayout);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(org.apache.commons.text.StringEscapeUtils.unescapeJava(proUsername));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(org.apache.commons.text.StringEscapeUtils.unescapeJava(proUsername));
                    //carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });


/*
  On  press on animation RecyclerView item
  */

        rvIndiGrp.addOnItemTouchListener(new RecyclerTouchListener(this,
                rvIndiGrp, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        rvIndiChat.addOnItemTouchListener(new RecyclerTouchListener(this,
                rvIndiChat, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        //User image displaying

        //Image loader

        Utility.picassoImageLoader(proPicture, 0, imgUsrProChat, getApplicationContext());




        Utility.picassoImageLoader(proPicture, 0, imgUsrPro, getApplicationContext());


        idTxtAbout.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(proUsername));

//        date_created(proCreatedon);

        //Getting bucket list details API

        getBucketIndividualUser(proFriendid);

       //Getting group user API

        getGroupUser(proFriendid);

    }

    private void initComponents() {

        //CoordinatorLayout

        txt_nodata = findViewById(R.id.txt_nodata);
        coChatLayout = findViewById(R.id.co_chat_layout);

        //AppBarLayout

        appBarLayout = findViewById(R.id.app_bar_chat_layout);

        //CollapsingToolbarLayout

        collapsingToolbarLayout = findViewById(R.id.chat_collapsingtoolbar);
        //ImageView

        imgUsrProChat = findViewById(R.id.img_usr_pro_chat);
        imgUsrProChat.setOnClickListener(this);

        imgUsrPro = findViewById(R.id.img_usr_pro);
        imgUsrPro.setOnClickListener(this);

        //Toolbar

        chatToolbar = findViewById(R.id.chat_toolbar);

        //Loader
        viewChatProgress = findViewById(R.id.view_chat_progress);
        //NestedScrollView

        chatScroll = findViewById(R.id.chat_scroll);
        //TextView

        idTxtAbout = findViewById(R.id.id_txt_about);
        txtBucket = findViewById(R.id.txt_bucket);
        txtGrpCommon = findViewById(R.id.txt_grp_common);
        txtCreatedon = findViewById(R.id.txt_createdon);

        // Recycler view for individual & group
        rvIndiChat = findViewById(R.id.rv_indi_chat);
        rvIndiGrp = findViewById(R.id.rv_indi_grp);

        clear_chat = findViewById(R.id.clear_chat);
        exit_group = findViewById(R.id.exit_group);
        btn_goto_profile = findViewById(R.id.btn_goto_profile);
        clear_chat.setOnClickListener(this);
        exit_group.setOnClickListener(this);
        btn_goto_profile.setOnClickListener(this);

    }


    private void makeCollapsingToolbarLayoutLooksGood(CollapsingToolbarLayout collapsingToolbarLayout) {

        collapsingToolbarLayout.setTitle(proUsername);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBarPlus1);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarPlus1);
    }


    private void date_created(String createdon) {
        try {
            long now = System.currentTimeMillis();

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convertedDate = dateFormat.parse(createdon);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now,

                    DateUtils.FORMAT_ABBREV_RELATIVE);

            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                txtCreatedon.setText(R.string.just_now);
            } else if (relavetime1.toString().contains("hours ago")) {
                txtCreatedon.setText(relavetime1.toString()/*.replace("hours ago", "").concat("h")*/);
            } else if (relavetime1.toString().contains("days ago")) {
                txtCreatedon.setText(relavetime1.toString()/*.replace("days ago", "").concat("d")*/);
            } else {
                txtCreatedon.append(relavetime1 + "\n\n");

            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


//Getting bucket list details API

    private void getBucketIndividualUser(String proFriendid) {

        if (Utility.isConnected(chatUserProfileActivity.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {


                Call<GetmybucketOutputpojo> call = apiService.get_bucket_individual_user("get_bucket_individual_user",
                        Integer.parseInt(proFriendid));

                call.enqueue(new Callback<GetmybucketOutputpojo>() {
                    @Override
                    public void onResponse(@NonNull Call<GetmybucketOutputpojo> call, @NonNull Response<GetmybucketOutputpojo> response) {


                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {
                            getmyBucketInputPojoArrayList.clear();
                            //Success

                            if(response.body().getData().size()>0) {
                                txt_nodata.setVisibility(View.GONE);

                                getmyBucketInputPojoArrayList = response.body().getData();

                                Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                           /* for (int i = 0; i < response.body().getData().size(); i++) {



                                String hotel_id = response.body().getData().get(i).getHotelId();

                                String google_id = response.body().getData().get(i).getGoogleId();

                                String hotel_name = response.body().getData().get(i).getHotelName();

                                String hotel_address = response.body().getData().get(i).getAddress();

                                String photo_reference = response.body().getData().get(i).getPhotoReference();

                                String category_type = response.body().getData().get(i).getCategoryType();

                                String bucket_id = response.body().getData().get(i).getBucketId();
                                String place_id = response.body().getData().get(i).getPlaceId();

                                String buck_description = response.body().getData().get(i).getBuckDescription();

                                String reviewid = response.body().getData().get(i).getReviewid();

                                String individual_userid = response.body().getData().get(i).getIndividualUserid();

                                String group_id = response.body().getData().get(i).getGroupId();

                                String created_by = response.body().getData().get(i).getCreatedBy();

                                String created_on = response.body().getData().get(i).getCreatedOn();

                                String hotel_review = response.body().getData().get(i).getHotelReview();


                                String total_likes = response.body().getData().get(i).getTotalLikes();

                                String total_comments = response.body().getData().get(i).getTotalComments();
                                String total_reviews = response.body().getData().get(i).getTotalReview();
                                String total_review_user = response.body().getData().get(i).getTotalReviewUsers();
                                String total_foodexp = response.body().getData().get(i).getTotalFoodExprience();
                                String total_ambience = response.body().getData().get(i).getTotalAmbiance();
                                String total_taste = response.body().getData().get(i).getTotalTaste();
                                String total_service = response.body().getData().get(i).getTotalService();
                                String total_package = response.body().getData().get(i).getTotalPackage();
                                String total_timedelivery = response.body().getData().get(i).getTotalTimedelivery();
                                String total_valuemoney = response.body().getData().get(i).getTotalValueMoney();
                                String total_good = response.body().getData().get(i).getTotalGood();
                                String total_bad= response.body().getData().get(i).getTotalBad();
                                String total_good_bad_user = response.body().getData().get(i).getTotalGoodBadUser();

                                String user_id = response.body().getData().get(i).getUserId();

                                String post_type = response.body().getData().get(i).getPostType();

                                String first_name = response.body().getData().get(i).getFirstName();

                                String last_name = response.body().getData().get(i).getLastName();

                                String email = response.body().getData().get(i).getEmail();

                                String picture = response.body().getData().get(i).getPicture();



                                getmyBucketInputPojo= new GetmyBucketInputPojo(hotel_id,google_id,hotel_name,
                                        hotel_address,photo_reference,place_id,category_type,bucket_id,buck_description,reviewid,post_type,
                                        individual_userid,group_id,created_by,created_on,hotel_review,total_likes,total_comments,total_reviews,total_review_user,
                                        total_foodexp,total_ambience,total_taste,total_service,total_package,total_timedelivery,total_valuemoney,total_good,
                                        total_bad,total_good_bad_user,user_id,first_name,last_name,"","","","",picture);





                            }*/


//                            getmyBucketInputPojoArrayList.add(getmyBucketInputPojo);

                                /*Setting adapter*/

                                groupBucketAdapter = new Group_bucketAdapter(chatUserProfileActivity.this, getmyBucketInputPojoArrayList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(chatUserProfileActivity.this, LinearLayoutManager.HORIZONTAL, true);
                                rvIndiChat.setLayoutManager(mLayoutManager);
                                Collections.reverse(getmyBucketInputPojoArrayList);
                                mLayoutManager.scrollToPosition(getmyBucketInputPojoArrayList.size() - 1);
                                rvIndiChat.setItemAnimator(new DefaultItemAnimator());
                                rvIndiChat.setAdapter(groupBucketAdapter);
                                groupBucketAdapter.notifyDataSetChanged();

                            }else
                            {
                               // txt_nodata.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetmybucketOutputpojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());

                        if (getmyBucketInputPojoArrayList.isEmpty()) {
                            txtBucket.setVisibility(View.GONE);
                           // txt_nodata.setVisibility(View.VISIBLE);
                        }
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(chatUserProfileActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();

        }


    }


    //Getting group user API

    private void getGroupUser(String proFriendid) {


        if (Utility.isConnected(chatUserProfileActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {


                Call<Get_grp_bucket_pojo> call = apiService.get_group_user("get_group_user", Integer.parseInt(proFriendid));
                call.enqueue(new Callback<Get_grp_bucket_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Get_grp_bucket_pojo> call, @NonNull Response<Get_grp_bucket_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {

                                String grpuserid = response.body().getData().get(j).getUserid();
                                String grpFrmfname = response.body().getData().get(j).getFromFirstname();
                                String grpFrmlname = response.body().getData().get(j).getFromLastname();
                                String grpFrmpicture = response.body().getData().get(j).getFromPicture();
                                String groupname = response.body().getData().get(j).getGroupName();
                                String groupId = response.body().getData().get(j).getGroup_id();
                                String groupicon = response.body().getData().get(j).getGroupIcon();
                                String changedWhich = response.body().getData().get(j).getChangedWhich();
                                String groupCreatedBy = response.body().getData().get(j).getGroupCreatedby();
                                String groupCreatedDate = response.body().getData().get(j).getCreatedDate();
                                String groupCreatedFname = response.body().getData().get(j).getGroupCreatedFirstname();
                                String groupCreatedLname = response.body().getData().get(j).getGroupCreatedLastname();


                                getGrpInputPojo = new Get_grp_input_pojo(grpuserid, grpFrmfname, grpFrmlname,
                                        grpFrmpicture, groupId, groupname, groupicon, changedWhich,
                                        groupCreatedBy, groupCreatedDate, groupCreatedFname, groupCreatedLname);

                                getGrpInputPojoArrayList.add(getGrpInputPojo);

                                /*Setting adapter*/

                                chatUserGrpAdapter = new Chat_user_grp_adapter(chatUserProfileActivity.this, getGrpInputPojoArrayList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(chatUserProfileActivity.this);
                                rvIndiGrp.setLayoutManager(mLayoutManager);
                                rvIndiGrp.setHasFixedSize(true);
                                rvIndiGrp.setItemAnimator(new DefaultItemAnimator());
                                rvIndiGrp.setAdapter(chatUserGrpAdapter);
                                rvIndiGrp.setNestedScrollingEnabled(false);
                                rvIndiGrp.setVisibility(View.VISIBLE);


                            }


                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Get_grp_bucket_pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        if (getGrpInputPojoArrayList.isEmpty()) {
                            txtGrpCommon.setVisibility(View.GONE);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(chatUserProfileActivity.this, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
//            Snackbar snackbar = Snackbar.make(rl_create_bucket_list, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
//            snackbar.setActionTextColor(Color.RED);
//            View view1 = snackbar.getView();
//            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
//            textview.setTextColor(Color.WHITE);
//            snackbar.show();
        }

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.img_usr_pro_chat:

                Intent intent4 = new Intent(chatUserProfileActivity.this, ViewImage.class);
                intent4.putExtra("imageurl", proPicture);
                startActivity(intent4);
                break;

            case R.id.img_usr_pro:

                Intent intent = new Intent(chatUserProfileActivity.this, ViewImage.class);
                intent.putExtra("imageurl", proPicture);
                startActivity(intent);
                break;

            case R.id.btn_goto_profile:
                Intent intentt = new Intent(this, User_profile_Activity.class);
                intentt.putExtra("created_by", created_by);
                startActivity(intentt);
                break;

            case R.id.clear_chat:

                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to clear this message?")
                        .setContentText("Won't be able to recover this message anymore!")
                        .setConfirmText("Yes,delete it!")
                        .setCancelText("No,cancel!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                update_delete_chat_clear();
                            }
                        })
                        .show();
                break;


                default:
                    break;
        }

    }

    private void update_delete_chat_clear() {
        if (Utility.isConnected(chatUserProfileActivity.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {


                Call<ClearChatResponse_Pojo> call = apiService.update_delete_chat_clear("update_delete_chat_clear",
                        Integer.parseInt(sessionid),
                        Integer.parseInt(userid),
                        Integer.parseInt(groupid));

                call.enqueue(new Callback<ClearChatResponse_Pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<ClearChatResponse_Pojo> call, @NonNull Response<ClearChatResponse_Pojo> response) {


                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if(response.body().getResponseMessage().equalsIgnoreCase("Success"))
                        {
                            Intent intent=new Intent(chatUserProfileActivity.this,ChatActivity.class);
                            startActivity(intent);
                        }
                     }

                    @Override
                    public void onFailure(@NonNull Call<ClearChatResponse_Pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());

                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(chatUserProfileActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
