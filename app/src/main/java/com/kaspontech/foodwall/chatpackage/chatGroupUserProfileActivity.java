package com.kaspontech.foodwall.chatpackage;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.chat.Group_bucketAdapter;
import com.kaspontech.foodwall.adapters.chat.Group_member_adapter;
import com.kaspontech.foodwall.bucketlistpackage.GetmyBucketInputPojo;
import com.kaspontech.foodwall.bucketlistpackage.GetmybucketOutputpojo;
import com.kaspontech.foodwall.menuPackage.UserMenu;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ClearChatResponse_Pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_group_member_output_pojo;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_grp_member_input_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.RecyclerTouchListener;
import com.kaspontech.foodwall.utills.Utility;
import com.kaspontech.foodwall.utills.ViewImage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class chatGroupUserProfileActivity extends AppCompatActivity implements View.OnClickListener {
    /*
     * Widgets
     * */

    /*
     * Chat layout
     * */
    CoordinatorLayout coChatLayout;
    /*
     * App bar chat layout
     * */
    AppBarLayout appBarChatLayout;

    /*
     * Collapsing bar chat layout
     * */
    CollapsingToolbarLayout chatCollapsingtoolbar;

    /*
     * Image view user chat
     * */
    ImageView imgUsrProChat;

    ImageView imgUsrPro;
    /*
     * Image button add member
     * */
    ImageButton imgAddMember;
    /*
     * Image button group edit
     * */
    ImageButton imgGrpEdit;

    /*
     * Chat tool bar
     * */
    android.support.v7.widget.Toolbar chatToolbar;
    /*
     * Chat progress bar
     * */
    ProgressBar viewChatProgress;
    /*
     * Nested scroll view
     * */
    NestedScrollView nestedScrollView;

    /*
     * Text view widgets
     * */

    TextView idTxtAbout;

    TextView txtCreatedon;

    TextView txtCreatedby;

    Button exitGroup, clear_chat;

    TextView txtBucketlist;

    TextView members;

    /*
     * String results
     * */

    String proUsername, userid;

    String proPicture;

    String proCreatedon;

    String proCreatedby;

    String proGroupid;

    String proSessionid;

    String proCreatedbyname, created_by;


    /*
     * Recyclerview members group
     * */

    RecyclerView rvMembersGrp;

    /*
     * Recyclerview members bucket group
     * */


    RecyclerView rvBucketGrp;
    /*
     * Get members pojo
     * */


    Get_grp_member_input_pojo getGrpMemberInputPojo;


    /*
     * Get members arraylist
     * */

    ArrayList<Get_grp_member_input_pojo> getGrpMemberInputPojoArrayList = new ArrayList<>();


    /*
     * Get members adapter
     * */

    Group_member_adapter groupMemberAdapter;

    /*
     * Admin check hash map
     * */


    public HashMap<String, String> admincheck = new HashMap<>();
    /*
     * Group bucket adapter
     * */
    Group_bucketAdapter groupBucketAdapter;

    /*
     * Get bucket input arraylist
     * */
    ArrayList<GetmyBucketInputPojo> getmyBucketInputPojoArrayList = new ArrayList<>();

    boolean admin = false;

    TextView txt_nodata;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_groupuser_profile_layout);


        //CoordinatorLayout
        coChatLayout = findViewById(R.id.co_chat_layout);
        //AppBarLayout
        appBarChatLayout = findViewById(R.id.app_bar_chat_layout);
        //CollapsingToolbarLayout
        chatCollapsingtoolbar = findViewById(R.id.chat_collapsingtoolbar);
        //ImageView
        imgUsrProChat = findViewById(R.id.img_usr_pro_chat);
        imgUsrProChat.setOnClickListener(this);
        imgUsrPro = findViewById(R.id.img_usr_pro);
        imgUsrPro.setOnClickListener(this);
        //ImageButton
        imgAddMember = findViewById(R.id.img_add_member);
        imgAddMember.setOnClickListener(this);


        //Toolbar
        chatToolbar = findViewById(R.id.chat_toolbar);

        imgGrpEdit = findViewById(R.id.img_grp_edit);
        imgGrpEdit.setOnClickListener(this);

        viewChatProgress = findViewById(R.id.view_chat_progress);
        //NestedScrollView
        nestedScrollView = findViewById(R.id.chat_scroll);
        txt_nodata = findViewById(R.id.txt_nodata);


        //TextView
        idTxtAbout = findViewById(R.id.id_txt_about);
        txtCreatedon = findViewById(R.id.txt_createdon);
        txtCreatedby = findViewById(R.id.txt_createdby);
        exitGroup = findViewById(R.id.exit_group);
        clear_chat = findViewById(R.id.clear_chat);
        txtBucketlist = findViewById(R.id.txt_bucketlist);
        members = findViewById(R.id.members);

        exitGroup.setOnClickListener(this);
        clear_chat.setOnClickListener(this);


        rvMembersGrp = findViewById(R.id.rv_members_grp);
        rvBucketGrp = findViewById(R.id.rv_bucket_grp);


        setSupportActionBar(chatToolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        chatToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        /*Getting input values*/

        Intent intent = getIntent();
        created_by = intent.getStringExtra("created_by");
        userid = intent.getStringExtra("userid");
        proUsername = intent.getStringExtra("pro_username");
        proGroupid = intent.getStringExtra("pro_groupid");
        proPicture = intent.getStringExtra("pro_picture");
        proCreatedon = intent.getStringExtra("pro_createdon");
        proCreatedby = intent.getStringExtra("pro_createdby");
        proCreatedbyname = intent.getStringExtra("pro_createdbyname");
        proSessionid = intent.getStringExtra("pro_sessionid");


        Log.e("proGroupid", "proGroupid: " + proGroupid);
        /*Offset bar chat layout*/
        appBarChatLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    chatCollapsingtoolbar.setTitle(org.apache.commons.text.StringEscapeUtils.unescapeJava(proUsername));
                    chatCollapsingtoolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
                    isShow = true;
                } else if (isShow) {
                    chatCollapsingtoolbar.setTitle(org.apache.commons.text.StringEscapeUtils.unescapeJava(proUsername));

                    chatCollapsingtoolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
                    //carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });


        /*
          On  press on animation RecyclerView item
          */


        rvMembersGrp.addOnItemTouchListener(new RecyclerTouchListener(this,
                rvMembersGrp, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        rvBucketGrp.addOnItemTouchListener(new RecyclerTouchListener(this,
                rvBucketGrp, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        //User image displaying


        // Image loading

        Utility.picassoImageLoader(proPicture, 0, imgUsrProChat, getApplicationContext());

        Utility.picassoImageLoader(proPicture, 0, imgUsrPro, getApplicationContext());

        /*user name about text view*/
        idTxtAbout.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(proUsername));

        /*Created by text view*/

        txtCreatedby.setText(proCreatedbyname);

        /*Created on text view*/

        dateCreated(proCreatedon);

        /* get_chat_friend_group_list */

        getChatFriendGroup();

        /* Bucket list API */

        getBucketGroup();


    }


/*
    //Self check of user in group or not

    public void checkselfid(String userid){
        Log.e("selfcheck", "checkselfid: "+userid);

        if(!userid.equalsIgnoreCase(Pref_storage.getDetail(getApplicationContext(),"userId"))){
            exitGroup.setVisibility(View.GONE);
            imgAddMember.setVisibility(View.GONE);
            imgGrpEdit.setVisibility(View.GONE);
        }else {
            exitGroup.setVisibility(View.VISIBLE);
            imgAddMember.setVisibility(View.VISIBLE);
            imgGrpEdit.setVisibility(View.VISIBLE);
        }

    }
*/

    /*
     * Created on
     * */
    private void dateCreated(String createdon) {
        try {
            long now = System.currentTimeMillis();

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convertedDate = dateFormat.parse(createdon);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now, DateUtils.FORMAT_ABBREV_RELATIVE);

            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                txtCreatedon.setText(R.string.just_now);
            } else if (relavetime1.toString().contains("hours ago")) {
                txtCreatedon.setText(relavetime1.toString()/*.replace("hours ago", "").concat("h")*/);
            } else if (relavetime1.toString().contains("days ago")) {
                txtCreatedon.setText(relavetime1.toString()/*.replace("days ago", "").concat("d")*/);
            } else {
                txtCreatedon.append(relavetime1 + "\n\n");

            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    /* Get Chat Friend Group API */

    private void getChatFriendGroup() {

        if (Utility.isConnected(chatGroupUserProfileActivity.this)) {

            final String userId = Pref_storage.getDetail(chatGroupUserProfileActivity.this, "userId");

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<Get_group_member_output_pojo> call = apiService.get_group_friends("get_group_friends", Integer.parseInt(proGroupid), Integer.parseInt(userId));
                call.enqueue(new Callback<Get_group_member_output_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Get_group_member_output_pojo> call, @NonNull Response<Get_group_member_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {

                                String grpfriendid = response.body().getData().get(j).getGrpfrndId();
                                String groupid = response.body().getData().get(j).getGroupid();
                                String groupname = response.body().getData().get(j).getGroupName();
                                String createdBy = response.body().getData().get(j).getCreatedBy();
                                String createdOn = response.body().getData().get(j).getCreatedOn();
                                String friendid = response.body().getData().get(j).getFriendid();


                                String usrId = response.body().getData().get(j).getUserid();
                                String groupExits = response.body().getData().get(j).getGroupExits();
                                String groupExitsTime = response.body().getData().get(j).getGroupExitsTime();

                                String firstName = response.body().getData().get(j).getFirstName();
                                String lastName = response.body().getData().get(j).getLastName();

                                String picture = response.body().getData().get(j).getPicture();


                                getGrpMemberInputPojo = new Get_grp_member_input_pojo(grpfriendid, groupid, usrId,
                                        friendid, groupExits, groupExitsTime, "", "", firstName, lastName,
                                        "", "", "", "", "", picture, groupid, groupname, createdBy, createdOn,
                                        "", "");

                                getGrpMemberInputPojoArrayList.add(getGrpMemberInputPojo);


                                if (friendid.equalsIgnoreCase(Pref_storage.getDetail(getApplicationContext(), "userId"))) {
                                    admincheck.put("admin", friendid);

                                    Log.e("admin", "onResponse:true " + admincheck);


                                } else {

                                    Log.e("admin", "onResponse:false " + admincheck);
                                }

                                /* Admin status check */

                                Log.e("adminsize", "onResponse: " + admincheck.size());

                                if (admincheck.size() != 0) {
                                    exitGroup.setVisibility(View.VISIBLE);
                                    imgAddMember.setVisibility(View.VISIBLE);
                                    imgGrpEdit.setVisibility(View.VISIBLE);

                                } else {
                                    exitGroup.setVisibility(View.GONE);
                                    imgAddMember.setVisibility(View.GONE);
                                    imgGrpEdit.setVisibility(View.GONE);
                                }


                            }

                            groupMemberAdapter = new Group_member_adapter(chatGroupUserProfileActivity.this, getGrpMemberInputPojoArrayList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(chatGroupUserProfileActivity.this);
                            rvMembersGrp.setLayoutManager(mLayoutManager);
                            rvMembersGrp.setHasFixedSize(true);
                            rvMembersGrp.setItemAnimator(new DefaultItemAnimator());
                            rvMembersGrp.setAdapter(groupMemberAdapter);
                            rvMembersGrp.setNestedScrollingEnabled(false);
                            groupMemberAdapter.notifyDataSetChanged();
                            rvMembersGrp.setVisibility(View.VISIBLE);

                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Get_group_member_output_pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("", "FailureError" + t.getMessage());

                        if (getGrpMemberInputPojoArrayList.isEmpty()) {
                            members.setVisibility(View.GONE);
                        }

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Snackbar snackbar = Snackbar.make(coChatLayout, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }


//getBucketGroup API

    private void getBucketGroup() {

        if (Utility.isConnected(chatGroupUserProfileActivity.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                String userId = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<GetmybucketOutputpojo> call = apiService.get_bucket_group_all("get_bucket_group_all", Integer.parseInt(proGroupid), Integer.parseInt(userId));
                call.enqueue(new Callback<GetmybucketOutputpojo>() {
                    @Override
                    public void onResponse(@NonNull Call<GetmybucketOutputpojo> call, @NonNull Response<GetmybucketOutputpojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {

                            getmyBucketInputPojoArrayList.clear();

                            getmyBucketInputPojoArrayList = response.body().getData();

                            Log.e("grp_bck_size", "-->" + response.body().getData().size());


                                   /* for (int i = 0; i < response.body().getData().size(); i++) {


                                        String hotel_id = response.body().getData().get(i).getHotelId();

                                        String google_id = response.body().getData().get(i).getGoogleId();

                                        String hotel_name = response.body().getData().get(i).getHotelName();

                                        String hotel_address = response.body().getData().get(i).getAddress();

                                        String photo_reference = response.body().getData().get(i).getPhotoReference();

                                        String category_type = response.body().getData().get(i).getCategoryType();

                                        String bucket_id = response.body().getData().get(i).getBucketId();
                                        String place_id = response.body().getData().get(i).getPlaceId();

                                        String buck_description = response.body().getData().get(i).getBuckDescription();

                                        String reviewid = response.body().getData().get(i).getReviewid();

                                        String individual_userid = response.body().getData().get(i).getIndividualUserid();

                                        String group_id = response.body().getData().get(i).getGroupId();

                                        String created_by = response.body().getData().get(i).getCreatedBy();

                                        String created_on = response.body().getData().get(i).getCreatedOn();

                                        String hotel_review = response.body().getData().get(i).getHotelReview();

                                        String total_likes = response.body().getData().get(i).getTotalLikes();

                                        String total_comments = response.body().getData().get(i).getTotalComments();
                                        String total_reviews = response.body().getData().get(i).getTotalReview();
                                        String total_review_user = response.body().getData().get(i).getTotalReviewUsers();
                                        String total_foodexp = response.body().getData().get(i).getTotalFoodExprience();
                                        String total_ambience = response.body().getData().get(i).getTotalAmbiance();
                                        String total_taste = response.body().getData().get(i).getTotalTaste();
                                        String total_service = response.body().getData().get(i).getTotalService();
                                        String total_package = response.body().getData().get(i).getTotalPackage();
                                        String total_timedelivery = response.body().getData().get(i).getTotalTimedelivery();
                                        String total_valuemoney = response.body().getData().get(i).getTotalValueMoney();
                                        String total_good = response.body().getData().get(i).getTotalGood();
                                        String total_bad= response.body().getData().get(i).getTotalBad();
                                        String total_good_bad_user = response.body().getData().get(i).getTotalGoodBadUser();

                                        String user_id = response.body().getData().get(i).getUserId();

                                        String post_type = response.body().getData().get(i).getPostType();

                                        String first_name = response.body().getData().get(i).getFirstName();

                                        String last_name = response.body().getData().get(i).getLastName();

                                        String email = response.body().getData().get(i).getEmail();

                                        String picture = response.body().getData().get(i).getPicture();

                                          getmyBucketInputPojo= new GetmyBucketInputPojo(hotel_id,google_id,hotel_name,
                                                  hotel_address,photo_reference,place_id,category_type,bucket_id,buck_description,reviewid,post_type,
                                                  individual_userid,group_id,created_by,created_on,hotel_review,total_likes,total_comments,total_reviews,total_review_user,
                                                  total_foodexp,total_ambience,total_taste,total_service,total_package,total_timedelivery,total_valuemoney,total_good,
                                                  total_bad,total_good_bad_user,user_id,first_name,last_name,"","","","",picture);


                                        getmyBucketInputPojoArrayList.add(getmyBucketInputPojo);




                                }*/


                            if (getmyBucketInputPojoArrayList.size() == 1) {

                                txt_nodata.setVisibility(View.GONE);

                                /*Setting adapter*/

                                groupBucketAdapter = new Group_bucketAdapter(getApplicationContext(), getmyBucketInputPojoArrayList);
                                LinearLayoutManager mLayoutManager = new LinearLayoutManager(chatGroupUserProfileActivity.this, LinearLayoutManager.HORIZONTAL, true);
                                rvBucketGrp.setLayoutManager(mLayoutManager);
                                rvBucketGrp.setHasFixedSize(true);
                                rvBucketGrp.setItemAnimator(new DefaultItemAnimator());
                                rvBucketGrp.setAdapter(groupBucketAdapter);
                                rvBucketGrp.setNestedScrollingEnabled(false);
                                groupBucketAdapter.notifyDataSetChanged();
                                rvBucketGrp.setVisibility(View.VISIBLE);

                            } else if (getmyBucketInputPojoArrayList.size() > 1) {
                                txt_nodata.setVisibility(View.GONE);

                                /*Setting adapter*/

                                groupBucketAdapter = new Group_bucketAdapter(getApplicationContext(), getmyBucketInputPojoArrayList);
                                LinearLayoutManager mLayoutManager = new LinearLayoutManager(chatGroupUserProfileActivity.this, LinearLayoutManager.HORIZONTAL, true);
                                rvBucketGrp.setLayoutManager(mLayoutManager);
                                Collections.reverse(getmyBucketInputPojoArrayList);
                                mLayoutManager.scrollToPosition(getmyBucketInputPojoArrayList.size() - 1);
                                rvBucketGrp.setHasFixedSize(true);
                                rvBucketGrp.setItemAnimator(new DefaultItemAnimator());
                                rvBucketGrp.setAdapter(groupBucketAdapter);
                                rvBucketGrp.setNestedScrollingEnabled(false);
                                groupBucketAdapter.notifyDataSetChanged();
                                rvBucketGrp.setVisibility(View.VISIBLE);
                            } else {
                                txt_nodata.setVisibility(View.VISIBLE);
                            }

                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<GetmybucketOutputpojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());

                        if (getmyBucketInputPojoArrayList.isEmpty()) {

                            txtBucketlist.setVisibility(View.GONE);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Snackbar snackbar = Snackbar.make(coChatLayout, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.img_add_member:

                Intent intent1 = new Intent(chatGroupUserProfileActivity.this, Add_member_group.class);
                intent1.putExtra("member_grpname", proUsername);
                intent1.putExtra("member_grpid", proGroupid);
                intent1.putExtra("member_grp_icon", proPicture);
                intent1.putExtra("member_grp_sessionid", proSessionid);
                startActivity(intent1);
                finish();


                break;


            case R.id.img_grp_edit:

                Intent intent = new Intent(chatGroupUserProfileActivity.this, chatEditGroupnameActivity.class);
                intent.putExtra("edit_grpname", proUsername);
                intent.putExtra("edit_grpid", proGroupid);
                intent.putExtra("edit_grp_icon", proPicture);
                intent.putExtra("edit_grp_sessionid", proSessionid);
                startActivity(intent);
                finish();

                break;

            case R.id.img_usr_pro_chat:

                Intent intent4 = new Intent(chatGroupUserProfileActivity.this, ViewImage.class);
                intent4.putExtra("imageurl", proPicture);
                startActivity(intent4);

                break;

            case R.id.img_usr_pro:

                Intent intent2 = new Intent(chatGroupUserProfileActivity.this, ViewImage.class);
                intent2.putExtra("imageurl", proPicture);
                startActivity(intent2);

                break;

            case R.id.exit_group:

                new SweetAlertDialog(chatGroupUserProfileActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to exit this group?")
                        .setContentText("Won't be able to chat in this group anymore!")
                        .setConfirmText("Yes,exit!")
                        .setCancelText("No,cancel!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                if (Utility.isConnected(getApplicationContext())) {

                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                    try {

                                        String createdby = Pref_storage.getDetail(chatGroupUserProfileActivity.this, "userId");
                                        Call<CreateUserPojoOutput> call = apiService.update_exit_chat_group_user("update_exit_chat_group_user", Integer.parseInt(proGroupid), Integer.parseInt(createdby));
                                        call.enqueue(new Callback<CreateUserPojoOutput>() {
                                            @Override
                                            public void onResponse(Call<CreateUserPojoOutput> call, @NonNull Response<CreateUserPojoOutput> response) {

                                                if (response.body() != null && response.body().getResponseCode() == 1) {


                                                    new SweetAlertDialog(chatGroupUserProfileActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                            .setTitleText("Done!!")
                                                            .setContentText("You're no longer a member in this group.")

                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                @Override
                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                    sDialog.dismissWithAnimation();
                                                                    finish();

                                                                    startActivity(new Intent(chatGroupUserProfileActivity.this, ChatActivity.class));

                                                                }
                                                            })
                                                            .show();


                                                }
                                            }

                                            @Override
                                            public void onFailure(@NonNull Call<CreateUserPojoOutput> call, @NonNull Throwable t) {
                                                //Error
                                                Log.e("FailureError", "" + t.getMessage());
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Toast.makeText(chatGroupUserProfileActivity.this, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .show();
                break;

            case R.id.clear_chat:

                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure want to clear this message?")
                        .setContentText("Won't be able to recover this message anymore!")
                        .setConfirmText("Yes,delete it!")
                        .setCancelText("No,cancel!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                update_delete_chat_clear();
                            }
                        })
                        .show();
                break;


        }
    }

    private void update_delete_chat_clear() {

        if (Utility.isConnected(chatGroupUserProfileActivity.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {


                Call<ClearChatResponse_Pojo> call = apiService.update_delete_chat_clear("update_delete_chat_clear",
                        Integer.parseInt(proSessionid),
                        Integer.parseInt(userid),
                        Integer.parseInt(proGroupid));

                call.enqueue(new Callback<ClearChatResponse_Pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<ClearChatResponse_Pojo> call, @NonNull Response<ClearChatResponse_Pojo> response) {


                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (response.body().getResponseMessage().equalsIgnoreCase("Success")) {
                            Intent intent = new Intent(chatGroupUserProfileActivity.this, ChatActivity.class);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ClearChatResponse_Pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());


                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(chatGroupUserProfileActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
