package com.kaspontech.foodwall.questionspackage;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.adapters.questionansweradapter.GetAllAnswersAdapter;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.modelclasses.AnswerPojoClass;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersOutput;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Answer.create_answer_votesResponse;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.GetFollowerRequestOutput;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.PollData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestLastCmt;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestionAnswerPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestionOutputPojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.New_Getting_timeline_all_pojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;


import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ViewQuestionAnswer extends _SwipeActivityClass implements View.OnClickListener, GetAllAnswersAdapter.onUpvotedownvoteUpdate {


    /*
     * Application log Tag
     * */

    public String TAG = "ViewQuestionAnswer";
    /**
     * Action bar
     **/
    Toolbar actionBar;

    /**
     * Back button
     **/
    ImageButton back;

    /**
     * Done button
     **/
    ImageButton selectDone;

    /**
     * Close button
     **/
    ImageButton selectClose;
    /**
     * Selection bar
     **/
    Toolbar selectActionBar;

    /**
     * Widgets
     **/
    /**
     * Circle image view
     **/

    CircleImageView userimage;
    CircleImageView answerUserimage;
    CircleImageView userphotoComment;
    CircleImageView cmtUserImage;

    /**
     * Loader
     **/

    ProgressBar progressBar;

    /**
     * Emoji edit text
     **/
    EmojiconEditText writeAnswer;
    EmojIconActions emojIcon;
    /**
     * Edit text for answer comment and reply
     **/

    EditText edittxtAnswerCmt;
    EditText edittxtAnswerCmtRpl;

    /**
     * Main answer relative layout
     **/
    RelativeLayout rlViewAnswerMain;

    /**
     * Question layout
     **/
    LinearLayout viewQuestionlayout;
    LinearLayout llAnsweredBy;
    LinearLayout llCmtUserDetails;
    /**
     * Image button
     **/
    ImageButton answer_button;
    ImageButton follow_button;
    ImageButton request_button;
    ImageButton share_button;
    ImageButton upVoteBtn;
    ImageButton downVoteBtn;
    /**
     * Textview's
     **/
    TextView question;
    TextView answer;
    TextView username;
    TextView answerUsername;
    TextView totalAnswers;
    TextView noOfFollowers;
    TextView answeredOn;
    TextView answerAnsweredOn;
    TextView viewQues;
    TextView typeAnswer;
    TextView postAnswer;
    TextView tvFollow;
    TextView tvRequest;
    TextView ques_follow_count;
    TextView cmtUserName;
    TextView userAnswer;

    /**
     * Request Answer
     **/
    RecyclerView follwersRecylerview;
    RelativeLayout empty_list;
    RequestAnswerAdapter requestAnswerAdapter;
    List<GetFollowersData> getFollowersDataList = new ArrayList<>();
    List<Integer> friendsList = new ArrayList<>();
    GetFollowersData getFollowersData;
    /**
     * Variable's
     **/

    int questionPosition;

    int quesId;

    int userid;
    int quesFollowCount;

    private double latitude;
    private double longitude;
    /**
     * Flag's
     **/

    boolean upVoted_flag = false;
    boolean downVoted_flag = false;
    boolean following_flag = false;

    boolean upVotedFlag = false;
    boolean downVotedFlag = false;
    TextView tv_upVote, tv_downVote;
    ProgressBar progress_answer, qaprogress;
    /**
     * GPS tracker
     **/

    GPSTracker gpsTracker;

    /**
     * Arraylist for Q&A
     **/

    List<QuestionAnswerPojo> getQuestionAllDataList = new ArrayList<>();

    /**
     * Q&A pojo
     **/

    QuestionAnswerPojo questionAnswerPojo;

    List<AnswerData> answerDataList = new ArrayList<>();
    AnswerData answerData;

    List<PollData> pollDataArrayList = new ArrayList<>();
    List<QuestLastCmt> QuestLastCmtList = new ArrayList<>();
    PollData pollData;

    // Recyclerview
    RecyclerView answer_recyclerview;
    ScrollView answerScrollView;
    GetAllAnswersAdapter getAllAnswersAdapter;
    /*List<GetAnswerData> getAnswerDataList = new ArrayList<>();
    GetAnswerData getAnswerData;*/

    CircleImageView user_image;
    String firstname, createdon, profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_question_answer);


        userid = Integer.parseInt(Pref_storage.getDetail(ViewQuestionAnswer.this, "userId"));

        //widget initialization
        intComponent();

       /* upVoteBtn.setOnClickListener(this);
        downVoteBtn.setOnClickListener(this);*/


        if (getIntent().getStringExtra("position") != null) {
            questionPosition = Integer.parseInt(Objects.requireNonNull(getIntent().getStringExtra("position")));
        }

        quesId = Integer.parseInt(Objects.requireNonNull(getIntent().getStringExtra("quesId")));

        if (getIntent().getStringExtra("firstname") != null) {
            firstname = getIntent().getStringExtra("firstname");
            username.setText(firstname);
        }
        if (getIntent().getStringExtra("createdon") != null) {
            createdon = getIntent().getStringExtra("createdon");
        }
        if (getIntent().getStringExtra("profile") != null) {
            profile = getIntent().getStringExtra("profile");
            GlideApp.with(this)
                    .load(profile)
                    .placeholder(R.drawable.ic_add_photo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.1f)
                    .into(user_image);
        }

        Log.e("view", "onCreate: quesId" + quesId);



        // Utility.setTimeStamp(createdon, answeredOn);

        /*Calling followers api */

        getFollowers(userid);


        questionPosition = 0;

        /*Calling question all api */
        getAllQuesData();


    }// OnCreate ends here

    @Override
    public void onSwipeRight() {
        finish();
    }

    @Override
    protected void onSwipeLeft() {

    }

    private void intComponent() {

        actionBar = findViewById(R.id.action_bar_view_question_answer);
        back = actionBar.findViewById(R.id.back);

        gpsTracker = new GPSTracker(ViewQuestionAnswer.this);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        rlViewAnswerMain = findViewById(R.id.rl_view_answer_main);
        userimage = findViewById(R.id.user_image);
        username = findViewById(R.id.user_name);
        answeredOn = findViewById(R.id.answered_on);
        cmtUserName = findViewById(R.id.cmt_user_name);
        userAnswer = findViewById(R.id.user_answer);

        answerUserimage = findViewById(R.id.answer_user_image);
        userphotoComment = findViewById(R.id.userphoto_comment);
        cmtUserImage = findViewById(R.id.cmt_user_image);
        answerUsername = findViewById(R.id.answer_user_name);
        answerAnsweredOn = findViewById(R.id.answer_answered_on);
        ques_follow_count = findViewById(R.id.ques_follow_count);

        question = findViewById(R.id.question);
        answer = findViewById(R.id.answer);
        tvFollow = findViewById(R.id.tv_follow);
        tvRequest = findViewById(R.id.tv_request);
        totalAnswers = findViewById(R.id.total_answers);
        noOfFollowers = findViewById(R.id.followers_count);
        postAnswer = findViewById(R.id.txt_adapter_post);
        qaprogress = findViewById(R.id.qaprogress);
        writeAnswer = findViewById(R.id.edittxt_comment);

        emojIcon = new EmojIconActions(ViewQuestionAnswer.this, rlViewAnswerMain, writeAnswer, userphotoComment);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);


        edittxtAnswerCmt = findViewById(R.id.edittxt_answer_cmt);
        edittxtAnswerCmtRpl = findViewById(R.id.edittxt_answer_cmt_rpl);
        answer_button = findViewById(R.id.answer_button);
        follow_button = findViewById(R.id.follow_button);
        request_button = findViewById(R.id.request_button);
        share_button = findViewById(R.id.share_button);
        /*upVoteBtn = (ImageButton) findViewById(R.id.upVoteBtn);
        downVoteBtn = (ImageButton) findViewById(R.id.downVoteBtn);*/
        viewQuestionlayout = findViewById(R.id.ll_view_questions);
        llAnsweredBy = findViewById(R.id.ll_answered_by);
        llCmtUserDetails = findViewById(R.id.ll_cmt_user_details);
//        votingLayout = (LinearLayout) findViewById(R.id.votingLayout);
        progressBar = findViewById(R.id.progressBar);
        answer_recyclerview = findViewById(R.id.answers_recyclerview);
        answerScrollView = findViewById(R.id.answerScrollView);
        user_image = findViewById(R.id.user_image);

       /* postAnswerLayout = (LinearLayout) findViewById(R.id.add_user_answer);
        answerChecked = (ImageButton) findViewById(R.id.answer_checked);
        answerUnchecked = (ImageButton) findViewById(R.id.answer_unchecked);*/

        answerDataList.clear();


        postAnswer.setOnClickListener(this);
        answer_button.setOnClickListener(this);
        follow_button.setOnClickListener(this);
        request_button.setOnClickListener(this);
        share_button.setOnClickListener(this);
        back.setOnClickListener(this);
        userimage.setOnClickListener(this);
        username.setOnClickListener(this);
    }

    /*Calling question all api */

    private void getFollowers(final int userId) {

        if (Utility.isConnected(ViewQuestionAnswer.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<GetFollowersOutput> call = apiService.getFollower("get_follower", userId);

                call.enqueue(new Callback<GetFollowersOutput>() {
                    @Override
                    public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                int userid = response.body().getData().get(i).getUserid();
                                String followerId = response.body().getData().get(i).getFollowerId();
                                String firstName = response.body().getData().get(i).getFirstName();
                                String lastName = response.body().getData().get(i).getLastName();
                                String userPhoto = response.body().getData().get(i).getPicture();

                                Log.e("TestingFollower", "onResponse: " + followerId);
                                Log.e("TestingFollower", "onResponse: " + userid);

                                if (!followerId.equals(String.valueOf(userid))) {

                                    GetFollowersData getFollowersData = new GetFollowersData(userid, followerId, firstName, lastName, userPhoto);
                                    getFollowersDataList.add(getFollowersData);
                                }

                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }

    }


    @Override
    public void onClick(View v) {

        int id = v.getId();


        try {
            String user_id = getQuestionAllDataList.get(questionPosition).getUserId();

            switch (id) {


                case R.id.user_image:


                    if (user_id.equalsIgnoreCase(Pref_storage.getDetail(ViewQuestionAnswer.this, "userId"))) {
                        startActivity(new Intent(ViewQuestionAnswer.this, Profile.class));
                    } else {

                        Intent intent = new Intent(ViewQuestionAnswer.this, User_profile_Activity.class);
                        intent.putExtra("created_by", user_id);
                        startActivity(intent);

                    }

                    break;


                case R.id.user_name:


                    if (user_id.equalsIgnoreCase(Pref_storage.getDetail(ViewQuestionAnswer.this, "userId"))) {
                        startActivity(new Intent(ViewQuestionAnswer.this, Profile.class));
                    } else {

                        Intent intent = new Intent(ViewQuestionAnswer.this, User_profile_Activity.class);
                        intent.putExtra("created_by", user_id);
                        startActivity(intent);

                    }

                    break;


                case R.id.txt_adapter_post:

                   // answer_recyclerview.setVisibility(View.GONE);
                    Utility.hideKeyboard(rlViewAnswerMain);

                    postAnswer();


                    break;

                case R.id.answer_button:

                    writeAnswer.requestFocus();
                    InputMethodManager inputMethodManager =
                            (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.toggleSoftInputFromWindow(
                            rlViewAnswerMain.getApplicationWindowToken(),
                            InputMethodManager.SHOW_FORCED, 0);
                    break;

                case R.id.follow_button:


                    for (int i = 0; i < getQuestionAllDataList.size(); i++) {


                        if (quesId == Integer.parseInt(getQuestionAllDataList.get(i).getQuestId())) {

                            Log.e("CheckingFollowing", "quesId->" + quesId);
                            Log.e("CheckingFollowing", "isFollowing->" + getQuestionAllDataList.get(i).isFollowing());

                            if (!getQuestionAllDataList.get(i).isFollowing()) {

                                quesFollowCount = getQuestionAllDataList.get(i).getTotalQuestFollow();

                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(ViewQuestionAnswer.this, "userId"));

                                    Call<CommonOutputPojo> call = apiService.createQuesFollow("create_delete_question_follower", quesId,
                                            Integer.parseInt(getQuestionAllDataList.get(i).getCreatedBy()),
                                            1, userid);

                                    final int finalI = i;
                                    final int finalI2 = i;
                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @SuppressLint("ResourceAsColor")
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                if (responseStatus.equals("success")) {

                                                    //Success
                                                    follow_button.setImageDrawable(ContextCompat.getDrawable(ViewQuestionAnswer.this, R.drawable.ic_followed_answer));
                                                    tvFollow.setText("Following");
                                                    tvFollow.setTextColor(ContextCompat.getColor(ViewQuestionAnswer.this, R.color.blue));
                                                    getQuestionAllDataList.get(finalI).setFollowing(true);
                                                    ques_follow_count.setText(String.valueOf(++quesFollowCount));
                                                    ques_follow_count.setVisibility(View.VISIBLE);

                                                } else {


                                                }

                                            } else {

                                                Toast.makeText(ViewQuestionAnswer.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();
                                            }


                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }


                            } else if (getQuestionAllDataList.get(i).isFollowing()) {

                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(ViewQuestionAnswer.this, "userId"));

                                    Call<CommonOutputPojo> call = apiService.createQuesFollow("create_delete_question_follower", quesId,
                                            Integer.parseInt(getQuestionAllDataList.get(i).getCreatedBy()), 0, userid);

                                    final int finalI1 = i;
                                    call.enqueue(new Callback<CommonOutputPojo>() {
                                        @SuppressLint("ResourceAsColor")
                                        @Override
                                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                if (responseStatus.equals("success")) {

                                                    //Success
                                                    follow_button.setImageDrawable(ContextCompat.getDrawable(ViewQuestionAnswer.this, R.drawable.ic_follow_answer));
                                                    tvFollow.setText("Follow");
                                                    tvFollow.setTextColor(ContextCompat.getColor(ViewQuestionAnswer.this, R.color.black));
                                                    getQuestionAllDataList.get(finalI1).setFollowing(false);
                                                    --quesFollowCount;
                                                    ques_follow_count.setVisibility(View.GONE);


                                                } else {


                                                }

                                            } else {

                                                Toast.makeText(ViewQuestionAnswer.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                            }


                                        }

                                        @Override
                                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }


                            }

                        }

                    }


                    break;

                case R.id.request_button:


                    for (int i = 0; i < getQuestionAllDataList.size(); i++) {


                        if (quesId == Integer.parseInt(getQuestionAllDataList.get(i).getQuestId())) {


                            final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ViewQuestionAnswer.this);
                            LayoutInflater inflater11 = LayoutInflater.from(ViewQuestionAnswer.this);
                            @SuppressLint("InflateParams") final View dialogView1 = inflater11.inflate(R.layout.activity_select_followers, null);
                            dialogBuilder1.setView(dialogView1);
                            dialogBuilder1.setTitle("");

                            follwersRecylerview = dialogView1.findViewById(R.id.user_followers_recylerview);
                            empty_list = dialogView1.findViewById(R.id.empty_list);
                            selectActionBar = dialogView1.findViewById(R.id.action_bar_select_follower);
                            selectClose = selectActionBar.findViewById(R.id.close);
                            selectDone = selectActionBar.findViewById(R.id.tv_done);

                            if (Integer.parseInt(getQuestionAllDataList.get(i).getTotalQuestRequest()) != 0) {


                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(ViewQuestionAnswer.this, "userId"));

                                    Call<GetFollowerRequestOutput> call = apiService.getRequestedFollower("get_question_request",
                                            quesId, userid);

                                    call.enqueue(new Callback<GetFollowerRequestOutput>() {
                                        @Override
                                        public void onResponse(Call<GetFollowerRequestOutput> call, Response<GetFollowerRequestOutput> response) {

                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                Log.e("friendsList", "responseStatus->" + responseStatus);

                                                if (responseStatus.equals("success")) {

                                                    //Success


                                                    for (int i = 0; i < response.body().getData().size(); i++) {

                                                        int userid = response.body().getData().get(i).getUserId();
                                                        friendsList.add(userid);

                                                    }


                                                    requestAnswerAdapter = new RequestAnswerAdapter(ViewQuestionAnswer.this, getFollowersDataList, friendsList);
                                                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(ViewQuestionAnswer.this);
                                                    follwersRecylerview.setLayoutManager(mLayoutManager1);
                                                    follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                                                    follwersRecylerview.setAdapter(requestAnswerAdapter);
                                                    follwersRecylerview.hasFixedSize();
                                                    requestAnswerAdapter.notifyDataSetChanged();


                                                } else {


                                                }

                                            } else {

                                                Toast.makeText(ViewQuestionAnswer.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                            }


                                        }

                                        @Override
                                        public void onFailure(Call<GetFollowerRequestOutput> call, Throwable t) {
                                            //Error
                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                        }
                                    });

                                } catch (Exception e) {

                                    e.printStackTrace();

                                }


                            } else {

                                requestAnswerAdapter = new RequestAnswerAdapter(ViewQuestionAnswer.this, getFollowersDataList, friendsList);
                                RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(ViewQuestionAnswer.this);
                                follwersRecylerview.setLayoutManager(mLayoutManager1);
                                follwersRecylerview.setItemAnimator(new DefaultItemAnimator());
                                follwersRecylerview.setAdapter(requestAnswerAdapter);
                                follwersRecylerview.hasFixedSize();
                                requestAnswerAdapter.notifyDataSetChanged();


                            }


                            if (getFollowersDataList.isEmpty()) {

                                empty_list.setVisibility(View.VISIBLE);
                                follwersRecylerview.setVisibility(View.GONE);

                            } else {

                                empty_list.setVisibility(View.GONE);
                                follwersRecylerview.setVisibility(View.VISIBLE);
                            }

                            final AlertDialog dialog1 = dialogBuilder1.create();
                            dialog1.show();

                            selectClose.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    dialog1.dismiss();

                                }
                            });

                            final int finalI = i;

                            selectDone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (friendsList.isEmpty()) {


                                        dialog1.dismiss();


                                    } else {

                                        requestFollower(quesId, Integer.parseInt(getQuestionAllDataList.get(finalI).getCreatedBy()));
                                        dialog1.dismiss();

                                    }


                                }
                            });

                        }

                    }


                    break;

                case R.id.share_button:


                    break;

                case R.id.back:

                    Intent intent = new Intent(ViewQuestionAnswer.this, Home.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("launchType", 3);
                    intent.putExtras(bundle);
                    finish();

                    break;


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getAllQuesData() {


        if (Utility.isConnected(ViewQuestionAnswer.this)) {

            progressBar.setVisibility(View.VISIBLE);
            progressBar.setIndeterminate(true);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(ViewQuestionAnswer.this, "userId"));

                Call<QuestionOutputPojo> call = apiService.getQuestionSingle("get_question_single", userid, quesId);

                call.enqueue(new Callback<QuestionOutputPojo>() {

                    @Override
                    public void onResponse(Call<QuestionOutputPojo> call, Response<QuestionOutputPojo> response) {


                        if (response.body() != null) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("Balaji", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                progressBar.setVisibility(View.GONE);
                                progressBar.setIndeterminate(false);

                                //Success
                                viewQuestionlayout.setVisibility(View.VISIBLE);

                                for (int i = 0; i < response.body().getData().size(); i++) {

                                /*Log.e("karthick", "responseSize->" + response.body().getData().get(i).getQuesType());
                                Log.e("karthick", "responseSize->" + new Gson().toJson(response.body()));*/

                                    String questId = response.body().getData().get(i).getQuestId();
                                    String askQuestion = response.body().getData().get(i).getAskQuestion();
                                    String totalAnswers = response.body().getData().get(i).getTotalAnswers();
                                    String totalComments = response.body().getData().get(i).getTotalComments();
                                    int quesCreatedBy = Integer.parseInt(response.body().getData().get(i).getCreatedBy());
                                    String quesCreatedOn = response.body().getData().get(i).getCreatedOn();
                                    String country = response.body().getData().get(i).getCountry();
                                    String city = response.body().getData().get(i).getCity();
                                    String latitude = response.body().getData().get(i).getLatitude();
                                    String longitude = response.body().getData().get(i).getLongitude();
                                    String createdBy = response.body().getData().get(i).getCreatedBy();
                                    String createdOn = response.body().getData().get(i).getCreatedOn();
                                    String active = response.body().getData().get(i).getActive();
                                    String deleted = response.body().getData().get(i).getDeleted();
                                    String oauthProvider = response.body().getData().get(i).getOauthProvider();
                                    String oauthUid = response.body().getData().get(i).getOauthUid();
                                    String email = response.body().getData().get(i).getEmail();
                                    String imei = response.body().getData().get(i).getImei();
                                    String contNo = response.body().getData().get(i).getContNo();
                                    String gender = response.body().getData().get(i).getGender();
                                    String dob = response.body().getData().get(i).getDob();
                                    String picture = response.body().getData().get(i).getPicture();
                                    String pollRegId = response.body().getData().get(i).getPollRegId();


                                    String firstName = response.body().getData().get(i).getFirstName();
                                    String lastName = response.body().getData().get(i).getLastName();
                                    String userPicture = response.body().getData().get(i).getPicture();
                                    String quesType = response.body().getData().get(i).getQuesType();
                                    int ques_follow = response.body().getData().get(i).getQuesFollow();
                                    int totalQuestFollow = response.body().getData().get(i).getTotalQuestFollow();
                                    String totalQuestRequest = response.body().getData().get(i).getTotalQuestRequest();
                                    String pollId = response.body().getData().get(i).getPollId();
                                    int total_followers = response.body().getData().get(i).getTotalFollowers();

                                    answerDataList = response.body().getData().get(i).getAnswer();
                                    pollDataArrayList = response.body().getData().get(i).getPoll();
                                    QuestLastCmtList = response.body().getData().get(i).getQuestLastCmt();

                                    String userId = response.body().getData().get(i).getUserId();
                                    int pollsize = response.body().getData().get(i).getPoll().size();

                                    questionAnswerPojo = new QuestionAnswerPojo(
                                            pollsize,
                                            questId,
                                            askQuestion,
                                            quesType,
                                            country,
                                            city,
                                            totalAnswers,
                                            totalComments,
                                            latitude,
                                            longitude,
                                            createdBy,
                                            createdOn,
                                            active,
                                            deleted,
                                            userId,
                                            oauthProvider,
                                            oauthUid,
                                            firstName,
                                            lastName,
                                            email,
                                            imei,
                                            contNo,
                                            gender,
                                            dob,
                                            picture,
                                            total_followers,
                                            totalQuestFollow,
                                            totalQuestRequest,
                                            pollRegId,
                                            pollId,
                                            ques_follow,
                                            QuestLastCmtList,
                                            pollDataArrayList,
                                            answerDataList);


                                    getQuestionAllDataList.add(questionAnswerPojo);


                                }


                                getAnswerData();
                                loadUserProfile();
                                loadQuestionDetails();
                                loadAnswerDetails();

                                Utility.picassoImageLoader(Pref_storage.getDetail(ViewQuestionAnswer.this, "picture_user_login"),
                                        0, userphotoComment, getApplicationContext());

//                                GlideApp.with(ViewQuestionAnswer.this).load(Pref_storage.getDetail(ViewQuestionAnswer.this, "picture_user_login")).into(userphotoComment);

                                //progressBar.setVisibility(View.GONE);


                            } else {

                                progressBar.setVisibility(View.GONE);
                                progressBar.setIndeterminate(false);
                            }

                        } else {

                            progressBar.setVisibility(View.GONE);
                            progressBar.setIndeterminate(false);

                            Toast.makeText(ViewQuestionAnswer.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<QuestionOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("Balaji", "" + t.getMessage());

                        progressBar.setVisibility(View.GONE);
                        progressBar.setIndeterminate(false);
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        }


    }

    /*Loading user profile*/
    private void loadUserProfile() {


        questionAnswerPojo = getQuestionAllDataList.get(questionPosition);
        /*User name setting*/

        String userName = questionAnswerPojo.getFirstName() + " " + questionAnswerPojo.getLastName();
        username.setText(userName);
        /*User image displaying*/

        Utility.picassoImageLoader(questionAnswerPojo.getPicture(),
                0, userimage, getApplicationContext());


        int totalFollowers = getQuestionAllDataList.get(questionPosition).getTotalFollowers();

        String followers = "";

        switch (totalFollowers) {

            case 0:

                noOfFollowers.setVisibility(View.GONE);

                break;

            case 1:

                followers = totalFollowers + " Follower";

                break;

            default:

                followers = totalFollowers + " Followers";

                break;

        }

        noOfFollowers.setText(followers);

        /*Time stamp displaying*/
        String createdon = questionAnswerPojo.getCreatedOn();

        Utility.setTimeStamp(createdon, answeredOn);


    }


    /*Load question details*/
    private void loadQuestionDetails() {

        if (!getQuestionAllDataList.isEmpty()) {


            for (int i = 0; i < getQuestionAllDataList.size(); i++) {


                if (quesId == Integer.parseInt(getQuestionAllDataList.get(i).getQuestId())) {


                    questionAnswerPojo = getQuestionAllDataList.get(i);

                    if (Integer.parseInt(questionAnswerPojo.getTotalAnswers()) == 0) {

                        totalAnswers.setText("No answers yet");

                    } else {

                        totalAnswers.setText(questionAnswerPojo.getTotalAnswers() + " Answers");

                    }


                    /*Log.e("getTotal_quest_request","getTotal_quest_request->"+getQuestionAllDataList.get(i).getQues_follow());
                    Log.e("getTotal_quest_request","quesId->"+quesId);
*/
                    if (Integer.parseInt(questionAnswerPojo.getTotalQuestRequest()) != 0) {

                        request_button.setImageDrawable(ContextCompat.getDrawable(ViewQuestionAnswer.this, R.drawable.ic_requested_answer));
                        tvRequest.setTextColor(ContextCompat.getColor(ViewQuestionAnswer.this, R.color.blue));

                    } else {


                        request_button.setImageDrawable(ContextCompat.getDrawable(ViewQuestionAnswer.this, R.drawable.ic_request_answer));
                        tvRequest.setTextColor(ContextCompat.getColor(ViewQuestionAnswer.this, R.color.black));


                    }


                    if (getQuestionAllDataList.get(i).getQuesFollow() != 0) {

                        follow_button.setImageDrawable(ContextCompat.getDrawable(ViewQuestionAnswer.this, R.drawable.ic_followed_answer));
                        tvFollow.setText("Follow");
                        tvFollow.setTextColor(ContextCompat.getColor(ViewQuestionAnswer.this, R.color.blue));
                        getQuestionAllDataList.get(i).setFollowing(true);
                        ques_follow_count.setText(String.valueOf(getQuestionAllDataList.get(i).getTotalQuestFollow()));
                        ques_follow_count.setVisibility(View.VISIBLE);


                    } else {

                        follow_button.setImageDrawable(ContextCompat.getDrawable(ViewQuestionAnswer.this, R.drawable.ic_follow_answer));
                        tvFollow.setText("Follow");
                        tvFollow.setTextColor(ContextCompat.getColor(ViewQuestionAnswer.this, R.color.black));
                        getQuestionAllDataList.get(i).setFollowing(false);
                        ques_follow_count.setVisibility(View.GONE);

                    }

                    writeAnswer.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {


                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                            /*Post answer text visibility*/
                            if (writeAnswer.getText().toString().trim().length() != 0) {
                                postAnswer.setVisibility(View.VISIBLE);
                            } else {
                                postAnswer.setVisibility(View.GONE);
                            }


                        }
                    });


                    question.setText(questionAnswerPojo.getAskQuestion());

                }

            }

        }

    }

    /*Post answer api calling*/
    private void postAnswer() {


        qaprogress.setVisibility(View.VISIBLE);
        qaprogress.setIndeterminate(true);

        if (writeAnswer.getText().toString().trim().length() != 0) {

            if (Utility.isConnected(this)) {

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(ViewQuestionAnswer.this, "userId"));

                    Call<AnswerPojoClass> call = apiService.createAnswer("create_answer", 0,
                            Integer.parseInt(getQuestionAllDataList.get(questionPosition).getQuestId()),
                            0, 0,
                            org.apache.commons.text.StringEscapeUtils.escapeJava(writeAnswer.getText().toString().trim()),
                            latitude, longitude, userid);


                    call.enqueue(new Callback<AnswerPojoClass>() {
                        @Override
                        public void onResponse(Call<AnswerPojoClass> call, Response<AnswerPojoClass> response) {

                            qaprogress.setVisibility(View.GONE);
                            qaprogress.setIndeterminate(false);
                            if (response.body() != null) {


                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {

                                    Toast.makeText(ViewQuestionAnswer.this, "Answer created successfully", Toast.LENGTH_SHORT).show();
                                    //Success
                                    writeAnswer.setText("");

                                    /*New answer data api calling*/

                                    getNewAnswerData();



                                /*Intent intent = getIntent();
                                finish();
                                startActivity(intent);*/
                                }


                            } else {

                                Toast.makeText(ViewQuestionAnswer.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                            }


                        }

                        @Override
                        public void onFailure(Call<AnswerPojoClass> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "FailureError" + t.getMessage());
                            qaprogress.setVisibility(View.GONE);
                            qaprogress.setIndeterminate(false);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

            }

        }

    }


    public void getAnswerData1(){


        if (Utility.isConnected(this)) {
            getQuestionAllDataList.clear();


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(ViewQuestionAnswer.this, "userId"));

                Call<QuestionOutputPojo> call = apiService.getQuestionAll1("get_question_answer_all", userid);

                call.enqueue(new Callback<QuestionOutputPojo>() {

                    @Override
                    public void onResponse(Call<QuestionOutputPojo> call, Response<QuestionOutputPojo> response) {


                        if (response.body() != null) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("Balaji", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                //Success

                                for (int i = 0; i < response.body().getData().size(); i++) {

                                /*Log.e("karthick", "responseSize->" + response.body().getData().get(i).getQuesType());
                                Log.e("karthick", "responseSize->" + new Gson().toJson(response.body()));*/

                                    String questId = response.body().getData().get(i).getQuestId();
                                    String askQuestion = response.body().getData().get(i).getAskQuestion();
                                    String totalAnswers = response.body().getData().get(i).getTotalAnswers();
                                    String totalComments = response.body().getData().get(i).getTotalComments();
                                    int quesCreatedBy = Integer.parseInt(response.body().getData().get(i).getCreatedBy());
                                    String quesCreatedOn = response.body().getData().get(i).getCreatedOn();
                                    String country = response.body().getData().get(i).getCountry();
                                    String city = response.body().getData().get(i).getCity();
                                    String latitude = response.body().getData().get(i).getLatitude();
                                    String longitude = response.body().getData().get(i).getLongitude();
                                    String createdBy = response.body().getData().get(i).getCreatedBy();
                                    String createdOn = response.body().getData().get(i).getCreatedOn();
                                    String active = response.body().getData().get(i).getActive();
                                    String deleted = response.body().getData().get(i).getDeleted();
                                    String oauthProvider = response.body().getData().get(i).getOauthProvider();
                                    String oauthUid = response.body().getData().get(i).getOauthUid();
                                    String email = response.body().getData().get(i).getEmail();
                                    String imei = response.body().getData().get(i).getImei();
                                    String contNo = response.body().getData().get(i).getContNo();
                                    String gender = response.body().getData().get(i).getGender();
                                    String dob = response.body().getData().get(i).getDob();
                                    String picture = response.body().getData().get(i).getPicture();
                                    String pollRegId = response.body().getData().get(i).getPollRegId();


                                    String firstName = response.body().getData().get(i).getFirstName();
                                    String lastName = response.body().getData().get(i).getLastName();
                                    String userPicture = response.body().getData().get(i).getPicture();
                                    String quesType = response.body().getData().get(i).getQuesType();
                                    int ques_follow = response.body().getData().get(i).getQuesFollow();
                                    int totalQuestFollow = response.body().getData().get(i).getTotalQuestFollow();
                                    String totalQuestRequest = response.body().getData().get(i).getTotalQuestRequest();
                                    String pollId = response.body().getData().get(i).getPollId();
                                    int total_followers = response.body().getData().get(i).getTotalFollowers();

                                    answerDataList = response.body().getData().get(i).getAnswer();
                                    pollDataArrayList = response.body().getData().get(i).getPoll();
                                    QuestLastCmtList = response.body().getData().get(i).getQuestLastCmt();

                                    String userId = response.body().getData().get(i).getUserId();
                                    int pollsize = response.body().getData().get(i).getPoll().size();

                                    questionAnswerPojo = new QuestionAnswerPojo(
                                            pollsize,
                                            questId,
                                            askQuestion,
                                            quesType,
                                            country,
                                            city,
                                            totalAnswers,
                                            totalComments,
                                            latitude,
                                            longitude,
                                            createdBy,
                                            createdOn,
                                            active,
                                            deleted,
                                            userId,
                                            oauthProvider,
                                            oauthUid,
                                            firstName,
                                            lastName,
                                            email,
                                            imei,
                                            contNo,
                                            gender,
                                            dob,
                                            picture,
                                            total_followers,
                                            totalQuestFollow,
                                            totalQuestRequest,
                                            pollRegId,
                                            pollId,
                                            ques_follow,
                                            QuestLastCmtList,
                                            pollDataArrayList,
                                            answerDataList);
                                    getQuestionAllDataList.add(questionAnswerPojo);


                                }


                                progressBar.setVisibility(View.GONE);
                                progressBar.setIndeterminate(false);

                              /*  getAnswerData();
                                loadUserProfile();
                                loadQuestionDetails();
                                loadAnswerDetails();*/

                                getAnswerData();
                                loadAnswerDetails();
                                loadQuestionDetails();


                            }

                        } else {

                            Toast.makeText(ViewQuestionAnswer.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<QuestionOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("Balaji", "" + t.getMessage());
                        progressBar.setVisibility(View.GONE);
                        progressBar.setIndeterminate(false);
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }
        } else {
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }

    }
    /*New answer data api calling*/
    public void getNewAnswerData() {
/*
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);*/

        if (Utility.isConnected(this)) {
           // getQuestionAllDataList.clear();


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(ViewQuestionAnswer.this, "userId"));

                Call<QuestionOutputPojo> call = apiService.getQuestionAll1("get_question_answer_all", userid);

                call.enqueue(new Callback<QuestionOutputPojo>() {

                    @Override
                    public void onResponse(Call<QuestionOutputPojo> call, Response<QuestionOutputPojo> response) {


                        if (response.body() != null) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("Balaji", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                //Success

                                for (int i = 0; i < response.body().getData().size(); i++) {

                                /*Log.e("karthick", "responseSize->" + response.body().getData().get(i).getQuesType());
                                Log.e("karthick", "responseSize->" + new Gson().toJson(response.body()));*/

                                    String questId = response.body().getData().get(i).getQuestId();
                                    String askQuestion = response.body().getData().get(i).getAskQuestion();
                                    String totalAnswers = response.body().getData().get(i).getTotalAnswers();
                                    String totalComments = response.body().getData().get(i).getTotalComments();
                                    int quesCreatedBy = Integer.parseInt(response.body().getData().get(i).getCreatedBy());
                                    String quesCreatedOn = response.body().getData().get(i).getCreatedOn();
                                    String country = response.body().getData().get(i).getCountry();
                                    String city = response.body().getData().get(i).getCity();
                                    String latitude = response.body().getData().get(i).getLatitude();
                                    String longitude = response.body().getData().get(i).getLongitude();
                                    String createdBy = response.body().getData().get(i).getCreatedBy();
                                    String createdOn = response.body().getData().get(i).getCreatedOn();
                                    String active = response.body().getData().get(i).getActive();
                                    String deleted = response.body().getData().get(i).getDeleted();
                                    String oauthProvider = response.body().getData().get(i).getOauthProvider();
                                    String oauthUid = response.body().getData().get(i).getOauthUid();
                                    String email = response.body().getData().get(i).getEmail();
                                    String imei = response.body().getData().get(i).getImei();
                                    String contNo = response.body().getData().get(i).getContNo();
                                    String gender = response.body().getData().get(i).getGender();
                                    String dob = response.body().getData().get(i).getDob();
                                    String picture = response.body().getData().get(i).getPicture();
                                    String pollRegId = response.body().getData().get(i).getPollRegId();


                                    String firstName = response.body().getData().get(i).getFirstName();
                                    String lastName = response.body().getData().get(i).getLastName();
                                    String userPicture = response.body().getData().get(i).getPicture();
                                    String quesType = response.body().getData().get(i).getQuesType();
                                    int ques_follow = response.body().getData().get(i).getQuesFollow();
                                    int totalQuestFollow = response.body().getData().get(i).getTotalQuestFollow();
                                    String totalQuestRequest = response.body().getData().get(i).getTotalQuestRequest();
                                    String pollId = response.body().getData().get(i).getPollId();
                                    int total_followers = response.body().getData().get(i).getTotalFollowers();

                                    answerDataList = response.body().getData().get(i).getAnswer();
                                    pollDataArrayList = response.body().getData().get(i).getPoll();
                                    QuestLastCmtList = response.body().getData().get(i).getQuestLastCmt();

                                    String userId = response.body().getData().get(i).getUserId();
                                    int pollsize = response.body().getData().get(i).getPoll().size();

                                    questionAnswerPojo = new QuestionAnswerPojo(
                                            pollsize,
                                            questId,
                                            askQuestion,
                                            quesType,
                                            country,
                                            city,
                                            totalAnswers,
                                            totalComments,
                                            latitude,
                                            longitude,
                                            createdBy,
                                            createdOn,
                                            active,
                                            deleted,
                                            userId,
                                            oauthProvider,
                                            oauthUid,
                                            firstName,
                                            lastName,
                                            email,
                                            imei,
                                            contNo,
                                            gender,
                                            dob,
                                            picture,
                                            total_followers,
                                            totalQuestFollow,
                                            totalQuestRequest,
                                            pollRegId,
                                            pollId,
                                            ques_follow,
                                            QuestLastCmtList,
                                            pollDataArrayList,
                                            answerDataList);
                                    getQuestionAllDataList.add(questionAnswerPojo);


                                }


                                LinkedHashSet<QuestionAnswerPojo> s = new LinkedHashSet<>(getQuestionAllDataList);
                                s.addAll(getQuestionAllDataList);
                                getQuestionAllDataList.clear();
                                getQuestionAllDataList.addAll(s);

                              /*  progressBar.setVisibility(View.GONE);
                                progressBar.setIndeterminate(false);*/

                              /*  getAnswerData();
                                loadUserProfile();
                                loadQuestionDetails();
                                loadAnswerDetails();*/

                            getAnswerData();
                            loadAnswerDetails();
                            loadQuestionDetails();


                            }

                        } else {

                            Toast.makeText(ViewQuestionAnswer.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<QuestionOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("Balaji", "" + t.getMessage());
                      /*  progressBar.setVisibility(View.GONE);
                        progressBar.setIndeterminate(false);*/
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }
        } else {
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }


    }
    /*Getting answer data api calling*/

    private void getAnswerData() {

        if (getQuestionAllDataList.size() > 0) {

            for (int i = 0; i < getQuestionAllDataList.size(); i++) {


                if (quesId == Integer.parseInt(getQuestionAllDataList.get(i).getQuestId()) && Integer.parseInt(getQuestionAllDataList.get(i).getTotalAnswers()) != 0) {

                    answerDataList = getQuestionAllDataList.get(i).getAnswer();


                    if (Integer.parseInt(answerDataList.get(0).getAnsId()) != 0) {

                        Collections.reverse(answerDataList);

                        /*Setting adapter*/
                        getAllAnswersAdapter = new GetAllAnswersAdapter(ViewQuestionAnswer.this, answerDataList, ViewQuestionAnswer.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ViewQuestionAnswer.this);
                        answer_recyclerview.setLayoutManager(mLayoutManager);
                        answer_recyclerview.setItemAnimator(new DefaultItemAnimator());
                        answer_recyclerview.setAdapter(getAllAnswersAdapter);
                        answer_recyclerview.hasFixedSize();
                        getAllAnswersAdapter.notifyDataSetChanged();

                    }


                }

            }

        }


    }

    /*Loading answer details*/
    private void loadAnswerDetails() {

        answer_recyclerview.setVisibility(View.VISIBLE);


        questionAnswerPojo = getQuestionAllDataList.get(questionPosition);

        if (Integer.parseInt(questionAnswerPojo.getTotalAnswers()) == 0) {

            totalAnswers.setText("No answers yet");
            llAnsweredBy.setVisibility(View.GONE);
//            votingLayout.setVisibility(View.GONE);


          /*  answerChecked.setVisibility(View.GONE);
            answerUnchecked.setVisibility(View.VISIBLE);*/

        } else if (!answerDataList.isEmpty()) {

            answerData = answerDataList.get(0);
//            totalAnswers.setText(answerData.getTotalAnswers());
//            votingLayout.setVisibility(View.VISIBLE);

          /*  if (answerDataList.get(0).getTotalUpvote() == 1) {

                upVoteBtn.setImageDrawable(ContextCompat.getDrawable(ViewQuestionAnswer.this, R.drawable.ic_upvoted));
                answerDataList.get(0).setUpVoted(true);


            } else {

                upVoteBtn.setImageDrawable(ContextCompat.getDrawable(ViewQuestionAnswer.this, R.drawable.ic_upvote));
                answerDataList.get(0).setUpVoted(false);

            }


            if (answerDataList.get(0).getTotalDownvote() == 1) {

                downVoteBtn.setImageDrawable(ContextCompat.getDrawable(ViewQuestionAnswer.this, R.drawable.ic_upvoted));
                answerDataList.get(0).setDownVoted(true);


            } else {

                downVoteBtn.setImageDrawable(ContextCompat.getDrawable(ViewQuestionAnswer.this, R.drawable.ic_upvote));
                answerDataList.get(0).setDownVoted(false);

            }*/


//            llAnsweredBy.setVisibility(View.VISIBLE);
            /*answerChecked.setVisibility(View.VISIBLE);
            answerUnchecked.setVisibility(View.GONE);*/

        }


        /* Answered By */

        if (answerData != null) {

            String userName = answerData.getFirstName() + " " + answerData.getLastName();
            answerUsername.setText(userName);

            Utility.picassoImageLoader(answerData.getPicture(),
                    0, answerUserimage, getApplicationContext());


            String createdon = answerData.getCreatedOn();

            try {

                if (answerAnsweredOn != null || !answerAnsweredOn.equals("")) {
                    Utility.setTimeStamp(createdon, answerAnsweredOn);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    /*Request follower*/

    private void requestFollower(int quesid, int createdby) {
        if (Utility.isConnected(this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(ViewQuestionAnswer.this, "userId"));

                Call<CommonOutputPojo> call = apiService.createQuestionRequest("create_question_request", quesid, createdby, friendsList);

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body() != null) {


                            String responseStatus = response.body().getResponseMessage();

                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                //Success
                                Toast.makeText(ViewQuestionAnswer.this, "Requested Successully", Toast.LENGTH_SHORT).show();
                                request_button.setImageDrawable(ContextCompat.getDrawable(ViewQuestionAnswer.this, R.drawable.ic_requested_answer));
                                tvRequest.setTextColor(ContextCompat.getColor(ViewQuestionAnswer.this, R.color.blue));

                            }


                        } else {

                            Toast.makeText(ViewQuestionAnswer.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "FailureError" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }
        } else {

            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }

    }


    /*Get request followers interface*/
    public void getRequestFollowers(View view, int adapterPosition) {

        if (((CheckBox) view).isChecked()) {

            if (friendsList.contains(Integer.valueOf(getFollowersDataList.get(adapterPosition).getFollowerId()))) {

                return;

            } else {

                friendsList.add(Integer.valueOf(getFollowersDataList.get(adapterPosition).getFollowerId()));

            }


        } else {

            friendsList.remove(Integer.valueOf(getFollowersDataList.get(adapterPosition).getFollowerId()));

        }
    }

    /*Loading new data*/
    public void loadNewData() {
        /*New answer data api calling*/

        getNewAnswerData();

    }


    @Override
    public void onUpvoteDownvote(View view, int adapterposition, View viewitem) {

        switch (view.getId()) {

            case R.id.tv_upVote:

                tv_upVote = (TextView) viewitem.findViewById(R.id.tv_upVote);
                tv_downVote = (TextView) viewitem.findViewById(R.id.tv_downVote);
                progress_answer = (ProgressBar) viewitem.findViewById(R.id.progress_answer);
                upVoteBtn = (ImageButton) viewitem.findViewById(R.id.upVoteBtn);
                downVoteBtn = (ImageButton) viewitem.findViewById(R.id.downVoteBtn);

                create_answer_votes(answerDataList, adapterposition);
                break;

            case R.id.tv_downVote:
                tv_upVote = (TextView) viewitem.findViewById(R.id.tv_upVote);
                tv_downVote = (TextView) viewitem.findViewById(R.id.tv_downVote);
                progress_answer = (ProgressBar) viewitem.findViewById(R.id.progress_answer);
                upVoteBtn = (ImageButton) viewitem.findViewById(R.id.upVoteBtn);
                downVoteBtn = (ImageButton) viewitem.findViewById(R.id.downVoteBtn);

                create_answer_downvote(answerDataList, adapterposition);
                break;


        }


    }

    private void create_answer_downvote(final List<AnswerData> getQuestionAllDataList, final int adapterposition) {

      /*  if (getQuestionAllDataList.get(adapterposition).isUpVoted()) {

            if (Utility.isConnected(this)) {

                progress_answer.setVisibility(View.VISIBLE);
                progress_answer.setIndeterminate(true);
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                    Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                            Integer.parseInt(getQuestionAllDataList.get(adapterposition).getAnsId()), 0, userid);

                    call.enqueue(new Callback<create_answer_votesResponse>() {
                        @Override
                        public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {


                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);
                            if (response.body() != null) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {


                                    //Success
                                    upVoteBtn.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_upvote));
                                    getQuestionAllDataList.get(adapterposition).setUpVoted(false);
                                    upVotedFlag = false;


                                    //  tv_upVote.setText("Upvote ("+response.body().getData().get(0).getTotalUpvote()+" )");



                                    int upVote = Integer.parseInt(response.body().getData().get(0).getTotalUpvote());
                                    int downVote = Integer.parseInt(response.body().getData().get(0).getTotalDownvote());

                                    Toast.makeText(ViewQuestionAnswer.this, ""+upVote, Toast.LENGTH_SHORT).show();
                                    Toast.makeText(ViewQuestionAnswer.this, ""+downVote, Toast.LENGTH_SHORT).show();

                                    //for upvote
                                    if (upVote == 0) {

                                        String upVoteCount = "Upvote";
                                        tv_upVote.setText(upVoteCount);

                                    } else {

                                        String upVoteCount = "Upvote (" + upVote + ")";
                                        tv_upVote.setText(upVoteCount);
                                    }

                                    //for downvote
                                    if (downVote == 0) {

                                        String downVoteCount = "Downvote";
                                        tv_downVote.setText(downVoteCount);

                                    } else {

                                        String downVoteCount = "Downvote (" + downVote + ")";
                                        tv_downVote.setText(downVoteCount);
                                    }
                                } else if (responseStatus.equals("alreadyvoted")) {

                                    Toast.makeText(getApplicationContext(), "Already voted", Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                Toast.makeText(getApplicationContext(), "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                            }


                        }

                        @Override
                        public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "FailureError" + t.getMessage());
                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);
                        }
                    });

                } catch (Exception e) {

                    e.printStackTrace();

                }
            } else {

                Toast.makeText(this, this.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

            }

        }

*/
        if (getQuestionAllDataList.get(adapterposition).isDownVoted()) {
            if (Utility.isConnected(this)) {

                progress_answer.setVisibility(View.VISIBLE);
                progress_answer.setIndeterminate(true);
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                    Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                            Integer.parseInt(getQuestionAllDataList.get(adapterposition).getAnsId()), 0, userid);

                    call.enqueue(new Callback<create_answer_votesResponse>() {
                        @Override
                        public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {
                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);
                            if (response.body() != null) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {

                                    //Success
                                    downVoteBtn.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_upvote));
                                    getQuestionAllDataList.get(adapterposition).setDownVoted(false);
                                    downVotedFlag = false;

                                    //  tv_downVote.setText("Downvote ("+response.body().getData().get(0).getTotalDownvote()+" )");

                                    int upVote = Integer.parseInt(response.body().getData().get(0).getTotalUpvote());
                                    int downVote = Integer.parseInt(response.body().getData().get(0).getTotalDownvote());


                                    //for upvote
                                    if (upVote == 0) {

                                        String upVoteCount = "Upvote";
                                        tv_upVote.setText(upVoteCount);

                                    } else {

                                        String upVoteCount = "Upvote (" + upVote + ")";
                                        tv_upVote.setText(upVoteCount);
                                    }

                                    //for downvote
                                    if (downVote == 0) {

                                        String downVoteCount = "Downvote";
                                        tv_downVote.setText(downVoteCount);

                                    } else {

                                        String downVoteCount = "Downvote (" + downVote + ")";
                                        tv_downVote.setText(downVoteCount);
                                    }

                                } else if (responseStatus.equals("alreadyvoted")) {

                                    Toast.makeText(getApplicationContext(), "Already voted", Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                Toast.makeText(getApplicationContext(), "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                            }


                        }

                        @Override
                        public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                            //Error
                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);
                            Log.e("FailureError", "FailureError" + t.getMessage());
                        }
                    });

                } catch (Exception e) {

                    e.printStackTrace();

                }
            } else {

                Toast.makeText(this, this.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

            }


        }

        else if (!getQuestionAllDataList.get(adapterposition).isDownVoted()) {

            if (Utility.isConnected(this)) {
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                progress_answer.setVisibility(View.VISIBLE);
                progress_answer.setIndeterminate(true);
                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                    Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                            Integer.parseInt(getQuestionAllDataList.get(adapterposition).getAnsId()), 2, userid);

                    call.enqueue(new Callback<create_answer_votesResponse>() {
                        @Override
                        public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {

                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);
                            if (response.body() != null) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {

                                    //Success


                                    downVoteBtn.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_upvoted));
                                    getQuestionAllDataList.get(adapterposition).setDownVoted(true);
                                    downVotedFlag = true;

                                    upVoteBtn.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_upvote));
                                    getQuestionAllDataList.get(adapterposition).setUpVoted(false);
                                    upVotedFlag = false;

                                    //tv_downVote.setText("Downvote (" + response.body().getData().get(0).getTotalDownvote()+" )");

                                    int upVote = Integer.parseInt(response.body().getData().get(0).getTotalUpvote());
                                    int downVote = Integer.parseInt(response.body().getData().get(0).getTotalDownvote());

                                    //for upvote
                                    if (upVote == 0) {

                                        String upVoteCount = "Upvote";
                                        tv_upVote.setText(upVoteCount);

                                    } else {

                                        String upVoteCount = "Upvote (" + upVote + ")";
                                        tv_upVote.setText(upVoteCount);
                                    }

                                    //for downvote
                                    if (downVote == 0) {

                                        String downVoteCount = "Downvote";
                                        tv_downVote.setText(downVoteCount);

                                    } else {

                                        String downVoteCount = "Downvote (" + downVote + ")";
                                        tv_downVote.setText(downVoteCount);
                                    }

                                } else if (responseStatus.equals("alreadyvoted")) {

                                    Toast.makeText(getApplicationContext(), "Already voted", Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                Toast.makeText(getApplicationContext(), "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                            }


                        }

                        @Override
                        public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "FailureError" + t.getMessage());
                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);

                        }
                    });

                } catch (Exception e) {

                    e.printStackTrace();

                }
            }

            else {

                Toast.makeText(this, this.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

            }


        }
    }

    //create answer vites api
    private void create_answer_votes(final List<AnswerData> getQuestionAllDataList, final int adapterposition) {


      /*  if (getQuestionAllDataList.get(adapterposition).isDownVoted()) {

            if (Utility.isConnected(this)) {
                progress_answer.setVisibility(View.VISIBLE);
                progress_answer.setIndeterminate(true);
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(this, "userId"));

                    Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                            Integer.parseInt(getQuestionAllDataList.get(adapterposition).getAnsId()), 0, userid);

                    call.enqueue(new Callback<create_answer_votesResponse>() {
                        @Override
                        public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {

                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);
                            if (response.body() != null) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {

                                    //Success

                                    downVoteBtn.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_upvote));
                                    getQuestionAllDataList.get(adapterposition).setDownVoted(false);
                                    downVotedFlag = false;


                                    // tv_downVote.setText("Downvote ("+response.body().getData().get(0).getTotalDownvote() +" )");
                                    //  tv_upVote.setText("Upvote ("+response.body().getData().get(0).getTotalUpvote() +" )");

                                    int upVote = Integer.parseInt(response.body().getData().get(0).getTotalUpvote());
                                    int downVote = Integer.parseInt(response.body().getData().get(0).getTotalDownvote());

                                    Toast.makeText(ViewQuestionAnswer.this, ""+upVote, Toast.LENGTH_SHORT).show();
                                    Toast.makeText(ViewQuestionAnswer.this, ""+downVote, Toast.LENGTH_SHORT).show();

                                    //for upvote
                                    if (upVote == 0) {

                                        String upVoteCount = "Upvote";
                                        tv_upVote.setText(upVoteCount);

                                    } else {

                                        String upVoteCount = "Upvote (" + upVote + ")";
                                        tv_upVote.setText(upVoteCount);
                                    }

                                    //for downvote
                                    if (downVote == 0) {

                                        String downVoteCount = "Downvote";
                                        tv_downVote.setText(downVoteCount);

                                    } else {

                                        String downVoteCount = "Downvote (" + downVote + ")";
                                        tv_downVote.setText(downVoteCount);
                                    }


                                } else if (responseStatus.equals("alreadyvoted")) {

                                    Toast.makeText(getApplicationContext(), "Already voted", Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                Toast.makeText(getApplicationContext(), "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                            }


                        }

                        @Override
                        public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "FailureError" + t.getMessage());
                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);
                        }
                    });

                } catch (Exception e) {

                    e.printStackTrace();

                }
            } else {

                Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

            }

        }
*/
        if (getQuestionAllDataList.get(adapterposition).isUpVoted()) {

            if (Utility.isConnected(this)) {
                progress_answer.setVisibility(View.VISIBLE);
                progress_answer.setIndeterminate(true);
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(this, "userId"));

                    Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                            Integer.parseInt(getQuestionAllDataList.get(adapterposition).getAnsId()), 0, userid);

                    call.enqueue(new Callback<create_answer_votesResponse>() {
                        @Override
                        public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {

                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);
                            if (response.body() != null) {


                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {

                                    //Success
                                    upVoteBtn.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_upvote));
                                    getQuestionAllDataList.get(adapterposition).setUpVoted(false);
                                    upVotedFlag = false;

                                    int upVote = Integer.parseInt(response.body().getData().get(0).getTotalUpvote());
                                    int downVote = Integer.parseInt(response.body().getData().get(0).getTotalDownvote());



                                    Log.e("upVote1", "onResponse: " + upVote);

                                    //for upvote
                                    if (upVote == 0) {

                                        String upVoteCount = "Upvote";
                                        tv_upVote.setText(upVoteCount);

                                    } else {

                                        String upVoteCount = "Upvote (" + upVote + ")";
                                        tv_upVote.setText(upVoteCount);
                                    }

                                    //for downvote
                                    if (downVote == 0) {

                                        String downVoteCount = "Downvote";
                                        tv_downVote.setText(downVoteCount);

                                    } else {

                                        String downVoteCount = "Downvote (" + downVote + ")";
                                        tv_downVote.setText(downVoteCount);
                                    }


                                } else if (responseStatus.equals("alreadyvoted")) {

                                    Toast.makeText(getApplicationContext(), "Already voted", Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                Toast.makeText(getApplicationContext(), "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                            }


                        }

                        @Override
                        public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                            //Error
                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);
                            Log.e("FailureError", "FailureError" + t.getMessage());
                        }
                    });

                } catch (Exception e) {

                    e.printStackTrace();

                }
            } else {

                Toast.makeText(this, this.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

            }


        }

        else if (!getQuestionAllDataList.get(adapterposition).isUpVoted()) {

            if (Utility.isConnected(this)) {
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                progress_answer.setVisibility(View.VISIBLE);
                progress_answer.setIndeterminate(true);
                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                    Call<create_answer_votesResponse> call = apiService.createAnswerVotes("create_answer_votes",
                            Integer.parseInt(getQuestionAllDataList.get(adapterposition).getAnsId()), 1, userid);

                    call.enqueue(new Callback<create_answer_votesResponse>() {
                        @Override
                        public void onResponse(Call<create_answer_votesResponse> call, Response<create_answer_votesResponse> response) {

                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);
                            if (response.body() != null) {

                                String responseStatus = response.body().getResponseMessage();

                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                if (responseStatus.equals("success")) {

                                    //Success

                                    upVoteBtn.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_upvoted));
                                    getQuestionAllDataList.get(adapterposition).setUpVoted(true);
                                    upVotedFlag = true;

                                    downVoteBtn.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_upvote));
                                    getQuestionAllDataList.get(adapterposition).setDownVoted(false);
                                    downVotedFlag = false;


//                                        int upVote = getAnswerDataList.get(position).getTotalUpvote() - 1;
                                    int upVote = Integer.parseInt(response.body().getData().get(0).getTotalUpvote());
                                    int downVote = Integer.parseInt(response.body().getData().get(0).getTotalDownvote());



                                    Log.e("upVote1", "onResponse: " + upVote);

                                    if (upVote == 0) {

                                        String upVoteCount = "Upvote";
                                        tv_upVote.setText(upVoteCount);

                                    } else {

                                        String upVoteCount = "Upvote (" + upVote + ")";
                                        tv_upVote.setText(upVoteCount);
                                    }
                                    //downvote
                                    if (downVote == 0) {

                                        String downVoteCount = "Downvote";
                                        tv_downVote.setText(downVoteCount);

                                    } else {

                                        String downVoteCount = "Downvote (" + downVote + ")";
                                        tv_downVote.setText(downVoteCount);
                                    }


                                } else if (responseStatus.equals("alreadyvoted")) {

                                    Toast.makeText(getApplicationContext(), "Already voted", Toast.LENGTH_SHORT).show();

                                }
                            } else {

                                Toast.makeText(getApplicationContext(), "Something went wrong please try again", Toast.LENGTH_SHORT).show();

                            }


                        }

                        @Override
                        public void onFailure(Call<create_answer_votesResponse> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "FailureError" + t.getMessage());
                            progress_answer.setVisibility(View.GONE);
                            progress_answer.setIndeterminate(false);
                        }
                    });

                } catch (Exception e) {

                    e.printStackTrace();

                }
            } else {

                Toast.makeText(this, this.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

            }


        }

    }
}
