package com.kaspontech.foodwall.questionspackage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RequestAnswerAdapter extends RecyclerView.Adapter<RequestAnswerAdapter.MyViewHolder> {

    /**
     * Application TAG
     **/

    private static final String TAG = "RequestAnswerAdapter";

    /**
     * Context
     **/

    private Context context;

    /**
     * View Question Answer pojo
     **/

    private ViewQuestionAnswer viewQuestionAnswer;

    /**
     * Followers data list
     **/

    private List<GetFollowersData> getFollowersDataList;

    /**
     * Invited people integer arraylist
     **/

    private List<Integer> invitedPeopleList = new ArrayList<>();

    /**
     * Followers pojo
     **/

    private GetFollowersData getFollowersData;


    public RequestAnswerAdapter(Context context, List<GetFollowersData> getFollowersDataList, List<Integer> invitedPeopleList) {
        this.context = context;
        this.getFollowersDataList = getFollowersDataList;
        this.invitedPeopleList = invitedPeopleList;
        viewQuestionAnswer = (ViewQuestionAnswer) context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_select_followers, parent, false);
        return new MyViewHolder(itemView, viewQuestionAnswer);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        getFollowersData = getFollowersDataList.get(position);

        /*User Image loading*/

        Utility.picassoImageLoader(getFollowersData.getPicture(),0,holder.followerPhoto,context);

        /*User name text displaying*/

        holder.userName.setText(getFollowersData.getFirstName().concat(" ").concat(getFollowersData.getLastName()));


        /*Followers list*/
        for (Integer value : invitedPeopleList) {

            Log.e(TAG, "onBindViewHolder: "+value );
            Log.e(TAG, "onBindViewHolder: "+getFollowersData.getFollowerId() );

            if (getFollowersData.getFollowerId().equals(String.valueOf(value))) {

                holder.selectFollowerBox.setChecked(true);

            }

        }

    }

    @Override
    public int getItemCount() {
        return getFollowersDataList.size();
    }

    /*Widget initialization*/
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        CircleImageView followerPhoto;
        TextView userName;
        CheckBox selectFollowerBox;
        ViewQuestionAnswer viewQuestionAnswer;

        public MyViewHolder(View view, ViewQuestionAnswer viewQuestionAnswer) {
            super(view);

            this.viewQuestionAnswer = viewQuestionAnswer;
            followerPhoto = view.findViewById(R.id.user_image);
            userName = view.findViewById(R.id.user_name);
            selectFollowerBox = view.findViewById(R.id.followers_checked);
            selectFollowerBox.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            viewQuestionAnswer.getRequestFollowers(view, getAdapterPosition());

        }
    }
}
