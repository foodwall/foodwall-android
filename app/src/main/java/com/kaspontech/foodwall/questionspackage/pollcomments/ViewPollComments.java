package com.kaspontech.foodwall.questionspackage.pollcomments;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter.TimelineAdapter;
import com.kaspontech.foodwall.adapters.TimeLine.ViewPagerAdapter;
import com.kaspontech.foodwall.adapters.questionansweradapter.pollcomments.GetPollCommentsAdapter;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.PollComments.GetPollCommentsData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.PollComments.GetPollCommentsOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewPollComments extends _SwipeActivityClass implements View.OnClickListener {
    /**
     * Application TAG
     */

    private static final String TAG = "ViewPollComments";
    /**
     * Action Bar
     */
    Toolbar actionBar;
    /**
     * Back button press
     */
    ImageButton back;
    /**
     * Close button press
     */
    ImageButton imgBtnClose;
    /**
     * Comments recycler view
     */

    RecyclerView rvComments;
    /**
     * Loader
     */
    ProgressBar progressDialog;
    /**
     * Emoji edit text
     */
    EmojiconEditText edittxtCommentnew;
    /**
     * Emoji action icon
     */
    EmojIconActions emojIcon;
    /**
     * User image photo
     */
    CircleImageView userphotoCommentnew;
    /**
     * Post text press
     */
    TextView txtPostnew;
    /**
     * Snack bar ty reply to user name
     */
    TextView txtReplyingTo;
    /**
     * Comment post relative layout
     */
    LinearLayout rlComment;
    RelativeLayout rlPostlayoutnew;
    RelativeLayout rlReplying;
    RelativeLayout rlCommentactivity;
    /**
     * No comments available layout
     */
    LinearLayout llCommentNodata;

    /**
     * *User tagged image
     */
    ImageView mviewpager;

    /**
     * * User firstname
     */
    TextView username;
    /**
     * Poll comments adapter
     */
    GetPollCommentsAdapter getPollCommentsAdapter;
    /**
     * Poll comments arraylist
     */
    List<GetPollCommentsData> getPollCommentsDataList = new ArrayList<>();
    /**
     * comment timeline id for api calling reference
     */
    String cmtTimelineid, comment_image, comment_firstname, comment_quesId, comment_question, comment_picture, comment_firstnamee, comment_pollsize,
            comment_poll1, comment_poll2, comment_poll3, comment_poll4, comment_votecount, comment_createdon;
    /**
     * From Adapter interface
     */
    int frmAdaptertlCmmntid;
    int frmAdapUserId;
    int frmAdapTimelineId;
    int clickedforreply;
    int clickedforedit;
    int clickedforeditreply;
    int clickededitreplyid;

    //qa comments
    TextView qa_user_name, answered_on, tv_poll_answer_one, tv_poll_answer_two, tv_poll_answer_third, tv_poll_answer_four, total_poll_votes;
    TextView question;
    LinearLayout ll_progress_section, ll_answer_section, ll_poll_answer_two, ll_poll_answer_one, ll_poll_answer_third, ll_poll_answer_four;
    ImageView user_image;

    String comment_pollid;
    Double userVotes, totalVotesOne, totalVotesTwo, totalVotesThree, totalVotesFour;

    ProgressBar qaprogress,poll_answer_one_progress, poll_answer_two_progress, poll_answer_third_progress, poll_answer_four_progress;
    TextView poll_answer_one_percent, poll_answer_two_percent, poll_answer_third_percent, poll_answer_four_percent;
    TextView polling_answer_one, polling_answer_two, polling_answer_three, polling_answer_four;

    LinearLayout ll_poll_progress_one, ll_poll_progress_two, ll_poll_progress_third, ll_poll_progress_four;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_poll_comments);

        /*Widget initialization*/
        initComponents();


        /*Getting comment user id from various adapters and activities*/
        Intent intent = getIntent();
        cmtTimelineid = intent.getStringExtra("comment_quesId");
        comment_image = intent.getStringExtra("comment_image");
        comment_firstname = intent.getStringExtra("comment_firstname");


        comment_question = intent.getStringExtra("comment_question");
        comment_picture = intent.getStringExtra("comment_picture");
        comment_firstnamee = intent.getStringExtra("comment_firstname");
        comment_pollsize = intent.getStringExtra("pollsize");
        comment_poll1 = intent.getStringExtra("comment_poll1");
        comment_poll2 = intent.getStringExtra("comment_poll2");
        comment_poll3 = intent.getStringExtra("comment_poll3");
        comment_poll4 = intent.getStringExtra("comment_poll4");
        comment_votecount = intent.getStringExtra("comment_votecount");
        comment_createdon = intent.getStringExtra("comment_createdon");
        comment_pollid = intent.getStringExtra("comment_pollid");

        try {
            userVotes = Double.valueOf(intent.getStringExtra("userVotes"));
            totalVotesOne = Double.valueOf(intent.getStringExtra("totalVotesOne"));
            totalVotesTwo = Double.valueOf(intent.getStringExtra("totalVotesTwo"));
            totalVotesThree = Double.valueOf(intent.getStringExtra("totalVotesThree"));
            totalVotesFour = Double.valueOf(intent.getStringExtra("totalVotesFour"));


            Log.e(TAG, "cmtTimelineid: " + cmtTimelineid);
            Log.e(TAG, "onCreate: " + comment_createdon);
            Log.e(TAG, "comment_pollsize: " + comment_pollsize);
            Log.e(TAG, "comment_picture: " + comment_picture);
            Log.e(TAG, "comment_votecount: " + comment_votecount);
            Log.e(TAG, "comment_pollid: " + comment_pollid);

            Log.e(TAG, "userVotes: " + userVotes);
            Log.e(TAG, "totalVotesOne: " + totalVotesOne);
            Log.e(TAG, "totalVotesTwo: " + totalVotesTwo);
            Log.e(TAG, "totalVotesThree: " + totalVotesThree);
            Log.e(TAG, "totalVotesFour: " + totalVotesFour);


        } catch (Exception e) {
            e.printStackTrace();
        }


        /* Calling comments all API */
        progressDialog.setVisibility(View.VISIBLE);

        /* set username */
        username.setText(comment_firstname);

        question.setText(comment_question);
        qa_user_name.setText(comment_firstnamee);
        tv_poll_answer_one.setText(comment_poll1);
        tv_poll_answer_two.setText(comment_poll2);
        tv_poll_answer_third.setText(comment_poll3);
        tv_poll_answer_four.setText(comment_poll4);
        total_poll_votes.setText(comment_votecount);


        try {
            Utility.setTimeStamp(comment_createdon, answered_on);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* load QA's user image*/
        GlideApp.with(this)
                .load(comment_picture)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)
                .into(user_image);


        /**
         * check poll question is answered or not
         */
        try {

            /**
             * check pollsize and enable layout
             */

            if (comment_pollsize.equalsIgnoreCase("0")) {

                ll_poll_answer_one.setVisibility(View.GONE);
                ll_poll_answer_two.setVisibility(View.GONE);
                ll_poll_answer_third.setVisibility(View.GONE);
                ll_poll_answer_four.setVisibility(View.GONE);
            } else if (comment_pollsize.equalsIgnoreCase("2")) {
                ll_poll_answer_one.setVisibility(View.VISIBLE);
                ll_poll_answer_two.setVisibility(View.VISIBLE);
                ll_poll_answer_third.setVisibility(View.GONE);
                ll_poll_answer_four.setVisibility(View.GONE);


            } else if (comment_pollsize.equalsIgnoreCase("3")) {
                ll_poll_answer_one.setVisibility(View.VISIBLE);
                ll_poll_answer_two.setVisibility(View.VISIBLE);
                ll_poll_answer_third.setVisibility(View.VISIBLE);
                ll_poll_answer_four.setVisibility(View.GONE);
            } else if (comment_pollsize.equalsIgnoreCase("4")) {
                ll_poll_answer_one.setVisibility(View.VISIBLE);
                ll_poll_answer_two.setVisibility(View.VISIBLE);
                ll_poll_answer_third.setVisibility(View.VISIBLE);
                ll_poll_answer_four.setVisibility(View.VISIBLE);
            }

            if (Integer.parseInt(comment_pollid) != 0) {

                ll_progress_section.setVisibility(View.VISIBLE);
                ll_answer_section.setVisibility(View.GONE);
                // tv_poll_undo.setVisibility(View.VISIBLE);
                // ll_poll_progress_one.setVisibility(View.VISIBLE);


                if (Integer.parseInt(comment_pollsize) != 0) {


                    polling_answer_one.setText(comment_poll1);


                    if (Integer.parseInt(comment_pollsize) == 2) {

                        /* Poll option one */

                        double percentOne;

                        if (userVotes == 0 || totalVotesOne == 0) {

                            percentOne = 0;

                        } else {

                            percentOne = (totalVotesOne / userVotes) * 100;


                        }

                        poll_answer_one_progress.setProgress((int) percentOne);
                        poll_answer_one_percent.setText((int) percentOne + " %");



                        /* Poll option two */
                        polling_answer_two.setText(comment_poll2);
                        double percentTwo;

                        if (userVotes == 0 || totalVotesTwo == 0) {
                            percentTwo = 0;
                        } else {
                            percentTwo = (totalVotesTwo / userVotes) * 100;
                        }

                   /* if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(1).getPollId()) {

                        pollAdapter.poll_two_checked.setVisibility(View.VISIBLE);

                    } else {

                        pollAdapter.poll_two_checked.setVisibility(View.GONE);
                    }
*/
                        poll_answer_two_progress.setProgress((int) percentTwo);
                        poll_answer_two_percent.setText((int) percentTwo + " %");


                    }
                    /* Poll option three */
                    else if (Integer.parseInt(comment_pollsize) == 3) {

                        /* Poll option one */

                        double percentOne;

                        if (userVotes == 0 || totalVotesOne == 0) {

                            percentOne = 0;

                        } else {

                            percentOne = (totalVotesOne / userVotes) * 100;


                        }

                        poll_answer_one_progress.setProgress((int) percentOne);
                        poll_answer_one_percent.setText((int) percentOne + " %");



                        /* Poll option two */
                        polling_answer_two.setText(comment_poll2);
                        double percentTwo;

                        if (userVotes == 0 || totalVotesTwo == 0) {
                            percentTwo = 0;
                        } else {
                            percentTwo = (totalVotesTwo / userVotes) * 100;
                        }

                   /* if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(1).getPollId()) {

                        pollAdapter.poll_two_checked.setVisibility(View.VISIBLE);

                    } else {

                        pollAdapter.poll_two_checked.setVisibility(View.GONE);
                    }
*/
                        poll_answer_two_progress.setProgress((int) percentTwo);
                        poll_answer_two_percent.setText((int) percentTwo + " %");


                        /* Poll option three */
                        ll_poll_progress_third.setVisibility(View.VISIBLE);
                        polling_answer_three.setText(comment_poll3);


                        double percentThree;

                        if (userVotes == 0 || totalVotesThree == 0) {

                            percentThree = 0;

                        } else {

                            percentThree = (totalVotesThree / userVotes) * 100;

                        }

                       /* if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(2).getPollId()) {

                            poll_three_checked.setVisibility(View.VISIBLE);

                        } else {

                            poll_three_checked.setVisibility(View.GONE);


                        }*/

                        poll_answer_third_progress.setProgress((int) percentThree);
                        poll_answer_third_percent.setText((int) percentThree + " %");


                    }
                    /* Poll option four */
                    else if (Integer.parseInt(comment_pollsize) == 4) {
                        /* Poll option one */

                        double percentOne;

                        if (userVotes == 0 || totalVotesOne == 0) {

                            percentOne = 0;

                        } else {

                            percentOne = (totalVotesOne / userVotes) * 100;


                        }

                        poll_answer_one_progress.setProgress((int) percentOne);
                        poll_answer_one_percent.setText((int) percentOne + " %");



                        /* Poll option two */
                        polling_answer_two.setText(comment_poll2);
                        double percentTwo;

                        if (userVotes == 0 || totalVotesTwo == 0) {
                            percentTwo = 0;
                        } else {
                            percentTwo = (totalVotesTwo / userVotes) * 100;
                        }

                   /* if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(1).getPollId()) {

                        pollAdapter.poll_two_checked.setVisibility(View.VISIBLE);

                    } else {

                        pollAdapter.poll_two_checked.setVisibility(View.GONE);
                    }
*/
                        poll_answer_two_progress.setProgress((int) percentTwo);
                        poll_answer_two_percent.setText((int) percentTwo + " %");

                        // poll option 3

                        ll_poll_progress_third.setVisibility(View.VISIBLE);
                        polling_answer_three.setText(comment_poll3);


                        double percentThree;

                        if (userVotes == 0 || totalVotesThree == 0) {

                            percentThree = 0;

                        } else {

                            percentThree = (totalVotesThree / userVotes) * 100;

                        }

                       /* if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(2).getPollId()) {

                            poll_three_checked.setVisibility(View.VISIBLE);

                        } else {

                            poll_three_checked.setVisibility(View.GONE);


                        }*/

                        poll_answer_third_progress.setProgress((int) percentThree);
                        poll_answer_third_percent.setText((int) percentThree + " %");


                        /* poll option 4 */

                        ll_poll_progress_four.setVisibility(View.VISIBLE);
                        polling_answer_four.setText(comment_poll4);


                        double percentFour;

                        if (userVotes == 0 || totalVotesFour == 0) {

                            percentFour = 0;

                        } else {

                            percentFour = (totalVotesFour / userVotes) * 100;

                        }

                      /*  if (getQuestionAllDataList.get(position).getPollId() == getQuestionAllDataList.get(position).getPoll().get(3).getPollId()) {

                            poll_four_checked.setVisibility(View.VISIBLE);

                        } else {

                            poll_four_checked.setVisibility(View.GONE);


                        }*/

                        poll_answer_four_progress.setProgress((int) percentFour);
                        poll_answer_four_percent.setText((int) percentFour + " %");

                    }

                }


            }



            /*if (Integer.parseInt(comment_pollid)!=0) {
                ll_progress_section.setVisibility(View.VISIBLE);
                ll_answer_section.setVisibility(View.GONE);
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }


        getPollCommentsAll();

        /*User id from shared preference*/
        String userdp = (Pref_storage.getDetail(ViewPollComments.this, "picture_user_login"));


        /* User picture loading */

        Utility.picassoImageLoader(userdp, 1, userphotoCommentnew, getApplicationContext());

        /* load timeline image*/
        GlideApp.with(this)
                .load(comment_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_add_photo)
                .into(mviewpager);

        /* Edit text change listener for posting comments */

        edittxtCommentnew.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                /*Post text visibility condition*/
                if (edittxtCommentnew.getText().toString().trim().length() != 0) {
                    txtPostnew.setVisibility(View.VISIBLE);
                } else {
                    txtPostnew.setVisibility(View.GONE);
                }


            }
        });


    }

    @Override
    public void onSwipeRight() {
            finish();
    }

    @Override
    protected void onSwipeLeft() {

    }

    private void initComponents() {

        actionBar = findViewById(R.id.actionbar_comment);
        /*Back button*/
        back = actionBar.findViewById(R.id.button_back);
        /*Close button*/
        imgBtnClose = findViewById(R.id.img_btn_close);

        /*On click listener*/
        back.setOnClickListener(this);
        imgBtnClose.setOnClickListener(this);

        /*Comment widgets initialization*/
        rlComment = findViewById(R.id.rl_commentnew);
        rlCommentactivity = findViewById(R.id.rl_commentactivity);
        rlPostlayoutnew = findViewById(R.id.rl_postlayoutnew);
        rlReplying = findViewById(R.id.rl_replying);

        /*No comments avaliable*/
        llCommentNodata = findViewById(R.id.ll_comment_nodata);
        /*Loader*/
        progressDialog = findViewById(R.id.progress_dialog_comment);
        /*User profile photo*/
        userphotoCommentnew = findViewById(R.id.userphoto_commentnew);
        /*Edit text for user comments*/
        edittxtCommentnew = findViewById(R.id.edittxt_commentnew);
        /*Emoji display*/
        emojIcon = new EmojIconActions(ViewPollComments.this, rlCommentactivity, edittxtCommentnew, userphotoCommentnew);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        /*Replying to name*/
        txtReplyingTo = findViewById(R.id.txt_replying_to);
        /*Post the comment text*/
        txtPostnew = findViewById(R.id.txt_adapter_postnew);
        txtPostnew.setOnClickListener(this);
        /*Comments recyclerview*/
        rvComments = findViewById(R.id.rv_commentsnew);

        /* to load user tagged image */
        mviewpager = findViewById(R.id.mviewpager);

        /* to load user first name */
        username = findViewById(R.id.username);

        question = findViewById(R.id.question);
        qa_user_name = findViewById(R.id.qa_user_name);
        answered_on = findViewById(R.id.answered_on);
        tv_poll_answer_one = findViewById(R.id.tv_poll_answer_one);
        tv_poll_answer_two = findViewById(R.id.tv_poll_answer_two);
        tv_poll_answer_third = findViewById(R.id.tv_poll_answer_third);
        tv_poll_answer_four = findViewById(R.id.tv_poll_answer_four);
        total_poll_votes = findViewById(R.id.total_poll_votes);
        ll_progress_section = findViewById(R.id.ll_progress_section);
        ll_answer_section = findViewById(R.id.ll_answer_section);
        user_image = findViewById(R.id.user_image);
        ll_poll_answer_two = findViewById(R.id.ll_poll_answer_two);
        ll_poll_answer_one = findViewById(R.id.ll_poll_answer_one);
        ll_poll_answer_third = findViewById(R.id.ll_poll_answer_third);
        ll_poll_answer_four = findViewById(R.id.ll_poll_answer_four);

        poll_answer_one_progress = findViewById(R.id.poll_answer_one_progress);
        poll_answer_two_progress = findViewById(R.id.poll_answer_two_progress);
        poll_answer_third_progress = findViewById(R.id.poll_answer_third_progress);
        poll_answer_four_progress = findViewById(R.id.poll_answer_four_progress);

        poll_answer_one_percent = findViewById(R.id.poll_answer_one_percent);
        poll_answer_two_percent = findViewById(R.id.poll_answer_two_percent);
        poll_answer_third_percent = findViewById(R.id.poll_answer_third_percent);
        poll_answer_four_percent = findViewById(R.id.poll_answer_four_percent);

        ll_poll_progress_one = findViewById(R.id.ll_poll_progress_one);
        ll_poll_progress_two = findViewById(R.id.ll_poll_progress_two);
        ll_poll_progress_third = findViewById(R.id.ll_poll_progress_third);
        ll_poll_progress_four = findViewById(R.id.ll_poll_progress_four);

        polling_answer_one = findViewById(R.id.polling_answer_one);
        polling_answer_two = findViewById(R.id.polling_answer_two);
        polling_answer_three = findViewById(R.id.polling_answer_three);
        polling_answer_four = findViewById(R.id.polling_answer_four);


        qaprogress = findViewById(R.id.qaprogress);


    }


    /*Comment adapter interface to check the list size*/
    public void check_timeline_reply_size(int size) {
        if (size == 0) {
            getPollCommentsAll();
        }
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            /*Back button press*/
            case R.id.button_back:
                finish();
                break;

            /*Close button press*/

            case R.id.img_btn_close:
                rlReplying.setVisibility(View.GONE);
                break;

            /*Post comment text click*/

            case R.id.txt_adapter_postnew:



                /* Hiding keyboard */
                Utility.hideKeyboard(v);
                txtPostnew.setVisibility(View.GONE);

                /* Posting reply */

                if (clickedforreply == 11) {
                    /* Calling comments reply API */
                    createEditTimelineCommentsReply(frmAdaptertlCmmntid);

                } else if (clickedforedit == 22) {
                    /* Calling edit comments API */

                    editTimelineComments(frmAdaptertlCmmntid, frmAdapTimelineId);

                } else if (clickedforeditreply == 44) {
                    /* Calling reply API */

                    editTimelineCommentsReply(frmAdapTimelineId, frmAdaptertlCmmntid);

                } else {
                    /* Calling comments post API */

                    createTimelineComments(cmtTimelineid);
                }



                break;

        }

    }


    // Interface

    /*
     *
     * Comments_adapter
     *
     * */

    /*To show no comments layout*/

    public void adaptersize(int size) {
        if (size == 0) {
            llCommentNodata.setVisibility(View.VISIBLE);
        }

    }

    /*Username finding from comments adapter*/
    public void clicked_username(View view, String username, int position) {
        frmAdaptertlCmmntid = position;
        rlReplying.setVisibility(View.VISIBLE);
        txtReplyingTo.setText("Replying to ".concat(username));
        edittxtCommentnew.requestFocus();

    }

    /*Clicked postion comment id*/
    public void clickedcommentposition(View view, int position) {
        frmAdaptertlCmmntid = position;
    }

    public void clickedforreply(View view, int position) {
        clickedforreply = position;


    }
    /*Clicked postion comment edit id*/

    public void clickedfor_edit(View view, int position) {
        clickedforedit = position;

    }


    /*
     * Comments_reply_adapter
     *
     * */

    /* edit commment */
    public void clickedcommentposition_timelineid(View view, String comments, int position, int timelineid) {
        frmAdaptertlCmmntid = position;
        frmAdapTimelineId = timelineid;

        edittxtCommentnew.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(comments));
        int pos = edittxtCommentnew.getText().length();
        edittxtCommentnew.setSelection(pos);
        edittxtCommentnew.requestFocus();
    }

    /* Reply edit  commment */

    public void clickedcommentposition_reply_edit(View view, String comments, int replyid, int comment_tl_id) {
        frmAdaptertlCmmntid = comment_tl_id;
        frmAdapTimelineId = replyid;
        edittxtCommentnew.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(comments));
        int pos = edittxtCommentnew.getText().length();
        edittxtCommentnew.setSelection(pos);
        edittxtCommentnew.requestFocus();
    }

    /*Reply edit position*/
    public void clickedfor_replyedit(View view, int position) {
        clickedforeditreply = position;

    }
    /*Clicked postion reply id*/

    public void clickedfor_replyid(View view, int position) {
        clickededitreplyid = position;


    }
    /*Clicked commments user id*/

    public void clickedcomment_userid_position(View view, int position) {
        frmAdapUserId = position;

    }


    /* Calling comments all API */

    private void getPollCommentsAll() {


        if (Utility.isConnected(getApplicationContext())) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                String createuserid = Pref_storage.getDetail(ViewPollComments.this, "userId");

                Call<GetPollCommentsOutput> call = apiService.getPollCommentsAll("get_question_comments_all", Integer.parseInt(cmtTimelineid), Integer.parseInt(createuserid));
                call.enqueue(new Callback<GetPollCommentsOutput>() {
                    @Override
                    public void onResponse(Call<GetPollCommentsOutput> call, Response<GetPollCommentsOutput> response) {


                        if (response.body() != null) {

                            String responseStatus = response.body().getResponseMessage();

                            if (responseStatus.equals("success")) {

                                progressDialog.setVisibility(View.GONE);
                                progressDialog.setIndeterminate(false);


                                /*Setting adapter*/
                                getPollCommentsDataList = response.body().getData();
                                getPollCommentsAdapter = new GetPollCommentsAdapter(ViewPollComments.this, getPollCommentsDataList);
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ViewPollComments.this);
                                rvComments.setLayoutManager(linearLayoutManager);
                                /*linearLayoutManager.setReverseLayout(true);*/
                                rvComments.setAdapter(getPollCommentsAdapter);
                                rvComments.setNestedScrollingEnabled(false);
                                getPollCommentsAdapter.notifyDataSetChanged();

                            }

                        }


                    }

                    @Override
                    public void onFailure(Call<GetPollCommentsOutput> call, Throwable t) {
                                               //Error
                        Log.e(TAG, getString(R.string.failure_error) + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }

    }


    /* Create Comments API */

    private void createTimelineComments(String cmtTimelineid) {

        if (Utility.isConnected(getApplicationContext())) {
            qaprogress.setVisibility(View.VISIBLE);
            qaprogress.setIndeterminate(true);

            if (edittxtCommentnew.getText().toString().trim().length() != 0) {

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    String createdby = Pref_storage.getDetail(ViewPollComments.this, "userId");

                    Call<GetPollCommentsOutput> call = apiService.createEditPollComments("create_edit_question_comments",
                            0, Integer.parseInt(cmtTimelineid),
                            org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtCommentnew.getText().toString().trim()),
                            Integer.parseInt(createdby));
                    call.enqueue(new Callback<GetPollCommentsOutput>() {
                        @Override
                        public void onResponse(Call<GetPollCommentsOutput> call, Response<GetPollCommentsOutput> response) {

                            String responseStatus = response.body().getResponseMessage();

                            if (responseStatus.equals("success")) {

                                qaprogress.setVisibility(View.GONE);
                                qaprogress.setIndeterminate(false);

                                edittxtCommentnew.setText("");
                                txtPostnew.setVisibility(View.GONE);

                                /*Calling poll comments all api*/
                                getPollCommentsAll();


                            }

                        }

                        @Override
                        public void onFailure(Call<GetPollCommentsOutput> call, Throwable t) {
                            //Error
                            qaprogress.setVisibility(View.GONE);
                            qaprogress.setIndeterminate(false);
                            Log.e(TAG, getString(R.string.failure_error) + t.getMessage());
                              }
                    });
                } catch (
                        Exception e) {
                    e.printStackTrace();
                }


            } else {

                edittxtCommentnew.setError("Enter your comment");
            }

        } else {
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        }


    }

    //Comments edit API

    private void editTimelineComments(int commentid, int timelineid) {

        if (Utility.isConnected(getApplicationContext())) {
            qaprogress.setVisibility(View.VISIBLE);
            qaprogress.setIndeterminate(true);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createuserid = Pref_storage.getDetail(ViewPollComments.this, "userId");

                Call<GetPollCommentsOutput> call = apiService.createEditPollComments("create_edit_question_comments", commentid, timelineid, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtCommentnew.getText().toString().trim()), Integer.parseInt(createuserid));
                call.enqueue(new Callback<GetPollCommentsOutput>() {
                    @Override
                    public void onResponse(Call<GetPollCommentsOutput> call, Response<GetPollCommentsOutput> response) {

                        if (response.body().getResponseCode() == 1) {

                            qaprogress.setVisibility(View.GONE);
                            qaprogress.setIndeterminate(false);

                            edittxtCommentnew.setText("");
                            frmAdaptertlCmmntid = 0;
                            frmAdapTimelineId = 0;
                            clickedforedit = 0;
                            /*Calling poll comments all api*/

                            getPollCommentsAll();
                        }

                    }

                    @Override
                    public void onFailure(Call<GetPollCommentsOutput> call, Throwable t) {
                        //Error
                           Log.e(TAG, getString(R.string.failure_error) + t.getMessage());
                        qaprogress.setVisibility(View.GONE);
                        qaprogress.setIndeterminate(false); }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        }


    }

    /* Comments reply API */

    private void createEditTimelineCommentsReply(int commentId) {

        if (Utility.isConnected(getApplicationContext())) {

            qaprogress.setVisibility(View.VISIBLE);
            qaprogress.setIndeterminate(true);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createdby = Pref_storage.getDetail(ViewPollComments.this, "userId");
                Call<CommonOutputPojo> call = apiService.createEditPollCommentsReply("create_edit_question_comments_reply", 0, commentId, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtCommentnew.getText().toString().trim()), Integer.parseInt(createdby));
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            qaprogress.setVisibility(View.GONE);
                            qaprogress.setIndeterminate(false);

                            edittxtCommentnew.setText("");
                            rlReplying.setVisibility(View.GONE);
                            frmAdaptertlCmmntid = 0;
                            clickedforreply = 0;

                            /*Calling poll comments all api*/



                            getPollCommentsAll();


                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        qaprogress.setVisibility(View.GONE);
                        qaprogress.setIndeterminate(false); Log.e(TAG, getString(R.string.failure_error) + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        }


    }

    /* Comments reply edit API */

    private void editTimelineCommentsReply(int replyid, int timelineid) {

        if (Utility.isConnected(getApplicationContext())) {

            qaprogress.setVisibility(View.VISIBLE);
            qaprogress.setIndeterminate(true);

            String userid = Pref_storage.getDetail(getApplicationContext(), "userId");
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                Call<CommonOutputPojo> call = apiService.createEditPollCommentsReply("create_edit_question_comments_reply", replyid, timelineid, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxtCommentnew.getText().toString().trim()), Integer.parseInt(userid));
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1) {

                            qaprogress.setVisibility(View.GONE);
                            qaprogress.setIndeterminate(false);

                            edittxtCommentnew.setText("");
                            clickedforeditreply = 0;
                            frmAdapTimelineId = 0;
                            frmAdaptertlCmmntid = 0;
                            /*Calling poll comments all api*/

                            getPollCommentsAll();
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                              Log.e(TAG, getString(R.string.failure_error) + t.getMessage());
                        qaprogress.setVisibility(View.GONE);
                        qaprogress.setIndeterminate(false); }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        }


    }


}
