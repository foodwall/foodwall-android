package com.kaspontech.foodwall.questionspackage;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestionAnswerPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestionOutputPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Question.CreateQuestionOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditQuestion extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    /**
     * Application TAG
     */
    private static final String TAG = "EditQuestion";

    /**
     * Action Bar
     */
    Toolbar actionBar;
    /**
     * Close button press
     */
    ImageButton close;

    /**
     * Create question textview
     */
    TextView createQuestion;


    /* EditText's */

    EditText getNormalQuestionsInput;
    EditText getPollQuestionsInput;
    EditText pollAnswerOne;
    EditText pollAnswerTwo;
    EditText pollAnswerThird;
    EditText pollAnswerFour;

    /**
     * Segment group
     */
    SegmentedGroup segmentedGroup;
    /**
     * Radio button for question type
     */
    RadioButton questionType;
    /**
     * Radio button for poll type
     */
    RadioButton pollType;
    /**
     * Question layout
     */
    LinearLayout questionLayout;

    /**
     * Poll layout
     */
    LinearLayout pollLayout;

    /* Latitude and longitude values */
    private double latitude;
    private double longitude;
    /**
     * GPS tracker
     */
    GPSTracker gpsTracker;
    /**
     * Question id
     */
    private int quesId;
    /**
     * Question answer arraylist
     */
    List<QuestionAnswerPojo> getQuestionAllDataList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_question);

        /*Gps tracker initialization*/

        gpsTracker = new GPSTracker(EditQuestion.this);

        /*Toolbar tracker initialization*/
        actionBar = findViewById(R.id.action_bar_create_ques);


        /*Close button initialization*/

        close = actionBar.findViewById(R.id.close);

        /*Create Question initialization*/

        createQuestion = actionBar.findViewById(R.id.tv_create_question);

        /* Edit text input initialization*/

        getNormalQuestionsInput = findViewById(R.id.get_normal_question);
        getPollQuestionsInput = findViewById(R.id.get_poll_question);
        pollAnswerOne = findViewById(R.id.poll_answer_one);
        pollAnswerTwo = findViewById(R.id.poll_answer_two);
        pollAnswerThird = findViewById(R.id.poll_answer_third);
        pollAnswerFour = findViewById(R.id.poll_answer_four);

        /*Segment group initialization*/

        segmentedGroup = findViewById(R.id.segmented_question);

        /*QuestionLayout initialization*/

        questionLayout = findViewById(R.id.ll_question_section);

        /*Poll Layout initialization*/

        pollLayout = findViewById(R.id.ll_poll_section);

        /*Question Type radio button initialization*/

        questionType = findViewById(R.id.type_question);

        /*Ans Type radio button initialization*/

        pollType = findViewById(R.id.type_poll);

        /*On click listener*/

        close.setOnClickListener(this);
        createQuestion.setOnClickListener(this);
        segmentedGroup.setOnCheckedChangeListener(this);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            quesId = bundle.getInt("quesId");

        }


        getQuestionAllDataList.clear();
        /*Calling question all api*/

        getAllQuesData();

        /*Poll and question layout vibility*/

        if (segmentedGroup.getCheckedRadioButtonId() == R.id.type_question) {

            pollLayout.setVisibility(View.GONE);
            questionLayout.setVisibility(View.VISIBLE);

        } else if (segmentedGroup.getCheckedRadioButtonId() == R.id.type_poll) {

            pollLayout.setVisibility(View.VISIBLE);
            questionLayout.setVisibility(View.GONE);

        }


        /*Latitude and longitude values*/

        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();


        /*  Edit text normal question listener */

        getNormalQuestionsInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String str = s.toString();

//                if(str.length() > 0 && str.contains(" "))

                /*Space not allowing condition*/

                if(str.equals(" "))
                {

                    Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                    getNormalQuestionsInput.setText(getNormalQuestionsInput.getText().toString().replaceAll(" ",""));
                    getNormalQuestionsInput.setSelection(getNormalQuestionsInput.getText().length());

                }

            }
        });


        /* Edit text question listener */

        getPollQuestionsInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String str = s.toString();
                /*Space not allowing condition*/

                if(str.equals(" "))
                {

                    Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                    getPollQuestionsInput.setText(getPollQuestionsInput.getText().toString().replaceAll(" ",""));
                    getPollQuestionsInput.setSelection(getPollQuestionsInput.getText().length());

                }

            }
        });

    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            case R.id.close:
                /*Close button click listener*/

                float deg = close.getRotation() + 180F;
                close.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
              /*  Intent intent = new Intent(EditQuestion.this, Home.class);
                Bundle bundle = new Bundle();
                bundle.putInt("launchType", 3);
                intent.putExtras(bundle);
                startActivity(intent);*/
                finish();
                break;


            case R.id.tv_create_question:

                /*Create question API calling*/

                int selectedId = segmentedGroup.getCheckedRadioButtonId();

                /*Hiding keyboarad*/

                Utility.hideKeyboard(v);


                switch (selectedId) {

                    case R.id.type_question:


                        if (getNormalQuestionsInput.getText().toString().trim().length() != 0) {
                            /*Loader*/

                            final AlertDialog dialog = new SpotsDialog.Builder().setContext(EditQuestion.this).setMessage("").build();

                            dialog.show();
                            if(Utility.isConnected(getApplicationContext())){
                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(EditQuestion.this, "userId"));


                                Call<CreateQuestionOutput> call = apiService.createNormalQuestion("create_question", quesId, getNormalQuestionsInput.getText().toString(), latitude, longitude, userid, 0, 0);


                                call.enqueue(new Callback<CreateQuestionOutput>() {
                                    @Override
                                    public void onResponse(Call<CreateQuestionOutput> call, Response<CreateQuestionOutput> response) {


                                        if (response.body() != null) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("success")) {

                                                Toast.makeText(EditQuestion.this, "Question edited sucessfully", Toast.LENGTH_SHORT).show();
                                                //Success
                                                /*Intent intent1 = new Intent(EditQuestion.this, Home.class);
                                                Bundle bundle1 = new Bundle();
                                                bundle1.putInt("launchType", 3);
                                                intent1.putExtras(bundle1);
                                                startActivity(intent1);*/
                                                finish();


                                            }

                                        } else {

                                            Toast.makeText(EditQuestion.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                        }


                                    }

                                    @Override
                                    public void onFailure(Call<CreateQuestionOutput> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                        dialog.dismiss();

                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                                dialog.dismiss();
                            }}else{
                                Toast.makeText(getApplicationContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                            }

                        } else {

                            getNormalQuestionsInput.setError("Ask Question");

                        }

                        break;

                    case R.id.type_poll:


                        if (validate()) {
                            /*Loader*/


                            final AlertDialog dialog = new SpotsDialog.Builder().setContext(EditQuestion.this).setMessage("").build();

                            dialog.show();
                            if(Utility.isConnected(getApplicationContext())){

                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


                            try {

                                int userid = Integer.parseInt(Pref_storage.getDetail(EditQuestion.this, "userId"));

                                final List<String> pollList = new ArrayList<>();


                                pollList.add(pollAnswerOne.getText().toString());
                                pollList.add(pollAnswerTwo.getText().toString());

                                if (pollAnswerThird.getText().toString().length() != 0) {
                                    pollList.add(pollAnswerThird.getText().toString());
                                }

                                if (pollAnswerFour.getText().toString().length() != 0) {
                                    pollList.add(pollAnswerFour.getText().toString());
                                }

                                String json = new Gson().toJson(pollList);
                                Call<CreateQuestionOutput> call = apiService.createPollQuestion("create_question",
                                        quesId, getPollQuestionsInput.getText().toString(),
                                        latitude, longitude, userid, 1,
                                        json);


                                call.enqueue(new Callback<CreateQuestionOutput>() {
                                    @Override
                                    public void onResponse(Call<CreateQuestionOutput> call, Response<CreateQuestionOutput> response) {


                                        if (response.body() != null) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            if (responseStatus.equals("success")) {


                                                Toast.makeText(EditQuestion.this, "Question edited sucessfully", Toast.LENGTH_SHORT).show();
                                                //Success
                                             /*   Intent intent1 = new Intent(EditQuestion.this, Home.class);
                                                Bundle bundle1 = new Bundle();
                                                bundle1.putInt("launchType", 3);
                                                intent1.putExtras(bundle1);
                                                startActivity(intent1);*/
                                                finish();


                                            }

                                        } else {

                                            Toast.makeText(EditQuestion.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                        }


                                    }

                                    @Override
                                    public void onFailure(Call<CreateQuestionOutput> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "FailureError" + t.getMessage());
                                        dialog.dismiss();

                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                                dialog.dismiss();

                            }}else{
                                    Toast.makeText(getApplicationContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                            }

                        }


                        break;


                }

                break;

        }

    }

    private void getAllQuesData() {

        if (Utility.isConnected(EditQuestion.this)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(EditQuestion.this, "userId"));

                Call<QuestionOutputPojo> call = apiService.getQuestionAll1("get_question_answer_all", userid);

                call.enqueue(new Callback<QuestionOutputPojo>() {

                    @Override
                    public void onResponse(Call<QuestionOutputPojo> call, Response<QuestionOutputPojo> response) {


                        if (response.body() != null) {



                            String responseStatus = response.body().getResponseMessage();

                            Log.e("Balaji", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                //Success
                                getQuestionAllDataList.clear();


                                for (int i = 0; i < response.body().getData().size(); i++) {

                                    Log.e("karthick", "responseSize->" + response.body().getData().get(i).getQuesType());
                                    Log.e("karthick", "responseSize->" + new Gson().toJson(response.body()));

                                   /* int questId = response.body().getData().get(i).getQuestId();
                                    String askQuestion = response.body().getData().get(i).getAskQuestion();
                                    int totalAnswers = response.body().getData().get(i).getTotalAnswers();
                                    int totalComments = response.body().getData().get(i).getTotalComments();
                                    int quesCreatedBy = response.body().getData().get(i).getCreatedBy();
                                    String quesCreatedOn = response.body().getData().get(i).getCreatedOn();
                                    String firstName = response.body().getData().get(i).getFirstName();
                                    String lastName = response.body().getData().get(i).getLastName();
                                    String userPicture = response.body().getData().get(i).getPicture();
                                    int quesType = response.body().getData().get(i).getQuesType();
                                    int ques_follow = response.body().getData().get(i).getQues_follow();
                                    int totalQuestFollow = response.body().getData().get(i).getTotal_quest_follow();
                                    int totalQuestRequest = response.body().getData().get(i).getTotal_quest_request();
                                    int pollId = response.body().getData().get(i).getPollId();
                                    int total_followers = response.body().getData().get(i).getTotal_followers();
                                    List<AnswerData> answerDataList = response.body().getData().get(i).getAnswer();
                                    List<PollData> pollDataArrayList = response.body().getData().get(i).getPoll();

                                    String userId = response.body().getData().get(i).getUserId();
                                    questionAnswerPojo = new QuestionAnswerPojo(userId,questId, askQuestion, quesType, totalAnswers,totalComments, quesCreatedBy, quesCreatedOn,
                                            firstName, lastName, userPicture, pollDataArrayList,
                                            answerDataList, ques_follow, totalQuestFollow, totalQuestRequest, pollId,total_followers);

                                    getQuestionAllDataList.add(questionAnswerPojo);

                                    */


                                    getQuestionAllDataList.addAll(response.body().getData());




                                }


                                setQuestionDetails();

                                Log.e("duration", "duration->" + getQuestionAllDataList.size());


                            }

                        } else {

                            Toast.makeText(EditQuestion.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<QuestionOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("Balaji", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        }else{
        Toast.makeText(getApplicationContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

    }

    }

    /*Setting up question details*/
    private void setQuestionDetails() {


        for (int i = 0; i < getQuestionAllDataList.size(); i++) {


            if (quesId == Integer.parseInt(getQuestionAllDataList.get(i).getQuestId())) {


                if (Integer.parseInt(getQuestionAllDataList.get(i).getQuesType()) == 1) {

                    pollType.setChecked(true);
                    getPollQuestionsInput.setText(getQuestionAllDataList.get(i).getAskQuestion());

                    int pollSize = getQuestionAllDataList.get(i).getPoll().size();

                    pollAnswerOne.setText(getQuestionAllDataList.get(i).getPoll().get(0).getPollList());
                    pollAnswerTwo.setText(getQuestionAllDataList.get(i).getPoll().get(1).getPollList());


                    if (pollSize == 3) {

                        pollAnswerThird.setText(getQuestionAllDataList.get(i).getPoll().get(2).getPollList());

                    } else if (pollSize == 4) {


                        pollAnswerThird.setText(getQuestionAllDataList.get(i).getPoll().get(2).getPollList());
                        pollAnswerFour.setText(getQuestionAllDataList.get(i).getPoll().get(3).getPollList());

                    }


                } else {

                    questionType.setChecked(true);
                    getNormalQuestionsInput.setText(getQuestionAllDataList.get(i).getAskQuestion());

                }


            }

        }


    }

    /*Validation*/

    private boolean validate() {

        boolean valid = true;


        String pollQuestion = getPollQuestionsInput.getText().toString().trim();
        String answerOne = pollAnswerOne.getText().toString().trim();
        String answerTwo = pollAnswerTwo.getText().toString().trim();

        if (pollQuestion.length() == 0) {
            getPollQuestionsInput.requestFocus();
            getPollQuestionsInput.setError("Ask question");
            valid = false;
        } else {
            getPollQuestionsInput.setError(null);
        }


        if (answerOne.length() == 0) {
            pollAnswerOne.requestFocus();
            pollAnswerOne.setError("Enter answer one");
            valid = false;
        } else {
            pollAnswerOne.setError(null);
        }


        if (answerTwo.length() == 0) {
            pollAnswerTwo.requestFocus();
            pollAnswerTwo.setError("Enter answer two");
            valid = false;
        } else {
            pollAnswerTwo.setError(null);
        }


        String answerThree = pollAnswerThird.getText().toString();

        if (answerThree.length() > 0) {

            if (pollAnswerThird.getText().toString().trim().length() == 0) {
                valid = false;
                pollAnswerThird.setError("Enter valid option");
            } else {
                pollAnswerThird.setError(null);
            }

        }


        String answerFour = pollAnswerFour.getText().toString();

        if (answerFour.length() > 0) {

            if (pollAnswerFour.getText().toString().trim().length() == 0) {
                valid = false;
                pollAnswerFour.setError("Enter valid option");
            } else {
                pollAnswerFour.setError(null);

            }

        }


        return valid;
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId) {

            case R.id.type_question:

                if (questionLayout.getVisibility() == View.VISIBLE) {

                    pollLayout.setVisibility(View.GONE);

                } else {

                    pollLayout.setVisibility(View.GONE);
                    questionLayout.setVisibility(View.VISIBLE);

                }

                break;

            case R.id.type_poll:


                if (pollLayout.getVisibility() == View.VISIBLE) {

                    questionLayout.setVisibility(View.GONE);

                } else {

                    questionLayout.setVisibility(View.GONE);
                    pollLayout.setVisibility(View.VISIBLE);

                }

                break;

            default:
                // Nothing to do
        }
    }





}
