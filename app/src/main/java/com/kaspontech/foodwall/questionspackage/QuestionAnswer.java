package com.kaspontech.foodwall.questionspackage;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.adapters.questionansweradapter.GetQuestionAllAdapter;
import com.kaspontech.foodwall.adapters.questionansweradapter.PollAdapter;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.fcm.Constants;
import com.kaspontech.foodwall.foodFeedsPackage.TotalCountModule_Response;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.modelclasses.AnswerPojoClass;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Answer.GetAnswerData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Answer.GetAnswerOutput;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.PollData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestLastCmt;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestionAnswerPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.QuestionOutputPojo;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Poll.GetPollData;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Poll.GetPollDataPojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Poll.create_delete_answer_poll_undoPOJO;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.PollComments.GetPollCommentsOutput;
import com.kaspontech.foodwall.modelclasses.Review_pojo.ReviewCommentsPojo;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.questionspackage.pollcomments.ViewPollComments;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.VISIBLE;

/**
 * A simple {@link Fragment} subclass.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class QuestionAnswer extends Fragment implements View.OnClickListener, ScrollView.OnScrollChangeListener, GetQuestionAllAdapter.onRefershListener,
        onCommentsPostListener {

    View view;
    Context context;

    private static final String TAG = "QuestionAnswer";

    // Action Bar
    Toolbar actionBar;
    ImageButton ask_question;


    // Widgets
    ProgressBar question_progress_bar;

    // Lottie animation

    // LottieAnimationView animationView;

    // Vars
    public RecyclerView questionRecylerView;

    //adapter
    GetQuestionAllAdapter getQuestionAllAdapter;

    List<QuestionAnswerPojo> getQuestionAllDataList = new ArrayList<>();


    List<QuestionAnswerPojo> paginationDataList = new ArrayList<>();

    //Pagination arraylist

    List<QuestionAnswerPojo> paginagtionDataList = new ArrayList<>();

    // Pagination

    private boolean loading = true;

    QuestionAnswerPojo questionAnswerPojo;

    List<QuestLastCmt> QuestLastCmtList = new ArrayList<>();
    List<AnswerData> answerDataList = new ArrayList<>();
    AnswerData answerData;

    List<PollData> pollDataArrayList = new ArrayList<>();
    PollData pollData;


    List<String> quesidList = new ArrayList<>();

    HashMap<String, List<GetPollData>> pollDataList = new HashMap<>();


    //Linear Layout (No data available)


    LinearLayoutManager linearLayoutManager;

    public TextView txt_no_dish;


    //Scroll view

    public static ScrollView question_scrollview;

    SwipeRefreshLayout qaswiperefresh;

    int pagecount = 1;

    Button page_loader;
    public static Button btn_size_check;

    private boolean undoPoll = false;

    ProgressBar progressbar_review;

    TextView tv_view_all_comments;

    private ProgressBar comments_progressbar, progress_dialog_follower;

    private EmojiconEditText edittxt_comment;

    private View viewAdapterfShow;

    TextView txt_adapter_post, answer;

    private AlertDialog dialog1;

    GPSTracker gpsTracker;

    Double latitude;

    Double longitude;

    public QuestionAnswer() {
        // Required empty public constructor
//        getAllQuesData();

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        onSaveInstanceState(savedInstanceState);

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_question_answer, container, false);
        context = view.getContext();

        Pref_storage.setDetail(context, "shareView", null);

        initComponents();

        Pref_storage.setDetail(context, "Page_quesans", String.valueOf(1));


        get_total_count_modules();


        // Layout manager initialization

        linearLayoutManager = new LinearLayoutManager(context);

        if (getQuestionAllDataList.isEmpty()) {


            getQuestionAllAdapter = new GetQuestionAllAdapter(context, getQuestionAllDataList, this, this);
            questionRecylerView.setLayoutManager(linearLayoutManager);
            questionRecylerView.setItemAnimator(new DefaultItemAnimator());
            questionRecylerView.setAdapter(getQuestionAllAdapter);
            questionRecylerView.setNestedScrollingEnabled(false);
            //     animationView.setVisibility(View.VISIBLE);

            question_progress_bar.setVisibility(View.VISIBLE);
            question_progress_bar.setIndeterminate(true);
            //API calling
            getAllQuesData(context, pagecount);

        } else {

            getQuestionAllAdapter = new GetQuestionAllAdapter(context, getQuestionAllDataList, this, this);
            questionRecylerView.setLayoutManager(linearLayoutManager);
            questionRecylerView.setItemAnimator(new DefaultItemAnimator());
            questionRecylerView.setAdapter(getQuestionAllAdapter);
            questionRecylerView.setNestedScrollingEnabled(false);

            question_progress_bar.setVisibility(View.GONE);
            question_progress_bar.setIndeterminate(false);

            //  animationView.setVisibility(View.GONE);


        }


        //implement swipe refresh layout
        qaswiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //call get_question_answer_all api
                swipegetAllQuesData();

                question_scrollview.smoothScrollTo(0, 0);

                qaswiperefresh.setRefreshing(false);
            }
        });

        return view;
    }

    private void initComponents() {
        actionBar = (Toolbar) view.findViewById(R.id.action_bar_ques_answer);
        ask_question = (ImageButton) actionBar.findViewById(R.id.ask_question);

        txt_no_dish = (TextView) view.findViewById(R.id.txt_no_dish);

        question_progress_bar = (ProgressBar) view.findViewById(R.id.question_progress_bar);
        // animationView = (LottieAnimationView) view.findViewById(R.id.animation_view);


        questionRecylerView = (RecyclerView) view.findViewById(R.id.que_ans_recyclerview);

        question_scrollview = (ScrollView) view.findViewById(R.id.question_recylerview);

        question_scrollview.setOnScrollChangeListener(this);

        page_loader = (Button) view.findViewById(R.id.page_loader);

        btn_size_check = (Button) view.findViewById(R.id.btn_size_check);
        qaswiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.qaswiperefresh);

//        btn_size_check.setOnClickListener(this);


        ask_question.setOnClickListener(this);
    }



    private void get_total_count_modules() {
        try {
            if (Utility.isConnected(getActivity())) {


                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {

                    String userid = Pref_storage.getDetail(getActivity(), "userId");

                    Call<TotalCountModule_Response> call = apiService.get_total_count_modules("get_total_count_modules",
                            Integer.parseInt(userid));
                    call.enqueue(new Callback<TotalCountModule_Response>() {
                        @Override
                        public void onResponse(Call<TotalCountModule_Response> call, Response<TotalCountModule_Response> response) {

                            if (response.body().getResponseMessage().equalsIgnoreCase("success")) {

                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    //save total timeline count
                                    Pref_storage.setDetail(getActivity(), "TotalPostCount", String.valueOf(response.body().getData().get(i).getTotalPosts()));

                                    Log.e(TAG, "count total: post" + response.body().getData().get(i).getTotalPosts());

                                    Pref_storage.setDetail(getActivity(), "TotalReviewCount", String.valueOf(response.body().getData().get(i).getTotalReview()));

                                    Log.e(TAG, "counttotal:review " + response.body().getData().get(i).getTotalReview());
                                    Pref_storage.setDetail(getActivity(), "TotalEventCount", String.valueOf(response.body().getData().get(i).getTotalEvents()));

                                    Log.e(TAG, "counttotal:event " + response.body().getData().get(i).getTotalReview());

                                    Pref_storage.setDetail(getActivity(), "TotalQuestionCount", String.valueOf(response.body().getData().get(i).getTotalQuestion()));

                                    Log.e(TAG, "counttotal:question " + response.body().getData().get(i).getTotalQuestion());

                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<TotalCountModule_Response> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        get_total_count_modules();
        Log.e(TAG, "onResume: Called");
    }

    //Calling all q&a API


    public void getAllQuesData(final Context context, int pageCount) {


        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                 Call<QuestionOutputPojo> call = apiService.getQuestionAll1("get_question_answer_all", userid, pageCount);

                call.enqueue(new Callback<QuestionOutputPojo>() {

                    @Override
                    public void onResponse(Call<QuestionOutputPojo> call, Response<QuestionOutputPojo> response) {

                        question_progress_bar.setVisibility(View.GONE);
                        question_progress_bar.setIndeterminate(false);

                        if (response.body() != null) {


                            String responseStatus = response.body().getResponseMessage();

                            Log.e("Balaji", "responseStatus->" + responseStatus);


                            if (responseStatus.equalsIgnoreCase("success")) {


                                if (response.body().getData().size() > 0) {

                                    pagecount += 1;


                                    Pref_storage.setDetail(context, "Page_quesans", String.valueOf(pagecount));

                                    Log.e(TAG, "onResponse:shared_page-->" + Pref_storage.getDetail(context, "Page_quesans"));
                                } else {

                                }


                                quesidList.clear();
                                pollDataList.clear();

                                getQuestionAllDataList.clear();

                                //Success

                                for (int i = 0; i < response.body().getData().size(); i++) {

                                    Log.e("karthick", "responseSize->" + response.body().getData().get(i).getQuesType());
                                    Log.e("karthick", "responseSize->" + new Gson().toJson(response.body()));

                                    String questId = response.body().getData().get(i).getQuestId();
                                    String askQuestion = response.body().getData().get(i).getAskQuestion();
                                    String totalAnswers = response.body().getData().get(i).getTotalAnswers();
                                    String totalComments = response.body().getData().get(i).getTotalComments();
                                    int quesCreatedBy = Integer.parseInt(response.body().getData().get(i).getCreatedBy());
                                    String redirect_url = response.body().getData().get(i).getRedirect_url();
                                    String quesCreatedOn = response.body().getData().get(i).getCreatedOn();
                                    String country = response.body().getData().get(i).getCountry();
                                    String city = response.body().getData().get(i).getCity();
                                    String latitude = response.body().getData().get(i).getLatitude();
                                    String longitude = response.body().getData().get(i).getLongitude();
                                    String createdBy = response.body().getData().get(i).getCreatedBy();
                                    String createdOn = response.body().getData().get(i).getCreatedOn();
                                    String active = response.body().getData().get(i).getActive();
                                    String deleted = response.body().getData().get(i).getDeleted();
                                    String oauthProvider = response.body().getData().get(i).getOauthProvider();
                                    String oauthUid = response.body().getData().get(i).getOauthUid();
                                    String email = response.body().getData().get(i).getEmail();
                                    String imei = response.body().getData().get(i).getImei();
                                    String contNo = response.body().getData().get(i).getContNo();
                                    String gender = response.body().getData().get(i).getGender();
                                    String dob = response.body().getData().get(i).getDob();
                                    String picture = response.body().getData().get(i).getPicture();
                                    String pollRegId = response.body().getData().get(i).getPollRegId();


                                    String firstName = response.body().getData().get(i).getFirstName();
                                    String lastName = response.body().getData().get(i).getLastName();
                                    String userPicture = response.body().getData().get(i).getPicture();
                                    String quesType = response.body().getData().get(i).getQuesType();
                                    int ques_follow = response.body().getData().get(i).getQuesFollow();
                                    int totalQuestFollow = response.body().getData().get(i).getTotalQuestFollow();
                                    String totalQuestRequest = response.body().getData().get(i).getTotalQuestRequest();
                                    String pollId = response.body().getData().get(i).getPollId();
                                    int total_followers = response.body().getData().get(i).getTotalFollowers();

                                    answerDataList = response.body().getData().get(i).getAnswer();
                                    pollDataArrayList = response.body().getData().get(i).getPoll();
                                    QuestLastCmtList = response.body().getData().get(i).getQuestLastCmt();

                                    String userId = response.body().getData().get(i).getUserId();
                                    int pollsize = response.body().getData().get(i).getPoll().size();

                                    questionAnswerPojo = new QuestionAnswerPojo(
                                            pollsize,
                                            questId,
                                            askQuestion,
                                            quesType,
                                            country,
                                            city,
                                            totalAnswers,
                                            totalComments,
                                            latitude,
                                            longitude,
                                            createdBy,
                                            createdOn,
                                            active,
                                            deleted,
                                            userId,
                                            oauthProvider,
                                            oauthUid,
                                            firstName,
                                            lastName,
                                            email,
                                            imei,
                                            contNo,
                                            gender,
                                            dob,
                                            picture,
                                            total_followers,
                                            totalQuestFollow,
                                            totalQuestRequest,
                                            pollRegId,
                                            pollId,
                                            ques_follow,
                                            QuestLastCmtList,
                                            pollDataArrayList,
                                            answerDataList,
                                            redirect_url);


                                    getQuestionAllDataList.add(questionAnswerPojo);



                                }

                                getQuestionAllAdapter = new GetQuestionAllAdapter(context, getQuestionAllDataList, QuestionAnswer.this, QuestionAnswer.this);
                                questionRecylerView.setLayoutManager(linearLayoutManager);
                                questionRecylerView.setItemAnimator(new DefaultItemAnimator());
                                questionRecylerView.setAdapter(getQuestionAllAdapter);
                                questionRecylerView.setNestedScrollingEnabled(false);
                                //     animationView.setVisibility(View.VISIBLE);

                                Log.e("duration", "duration->" + getQuestionAllDataList.size());


                            } else {

                                question_progress_bar.setIndeterminate(false);
                                question_progress_bar.setVisibility(View.GONE);


                             }


                        } else {

                            question_progress_bar.setIndeterminate(false);
                            question_progress_bar.setVisibility(View.GONE);

                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<QuestionOutputPojo> call, Throwable t) {
                        question_progress_bar.setIndeterminate(false);
                        question_progress_bar.setVisibility(View.GONE);


                        if (getQuestionAllDataList.isEmpty()) {

                            txt_no_dish.setVisibility(View.VISIBLE);

                        } else {
                            txt_no_dish.setVisibility(View.GONE);
                        }
                        //Error
                        Log.e("Balaji", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {
            Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }


    }

    //calling get_question_answer_all api
    private void swipegetAllQuesData() {


        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

//                Call<QuestionOutputPojo> call = apiService.getQuestionAll1("get_question_answer_all", userid);
                Call<QuestionOutputPojo> call = apiService.getQuestionAll1("get_question_answer_all", userid,
                        1);

                call.enqueue(new Callback<QuestionOutputPojo>() {

                    @Override
                    public void onResponse(Call<QuestionOutputPojo> call, Response<QuestionOutputPojo> response) {

                        if (response.body() != null) {

                            if (progressbar_review != null) {
                                progressbar_review.setVisibility(View.GONE);
                                progressbar_review.setIndeterminate(false);
                            }

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("Balaji", "responseStatus->" + responseStatus);

                            getQuestionAllDataList.clear();
                            if (responseStatus.equalsIgnoreCase("success")) {


                                if (response.body().getData().size() > 0) {
                                    pagecount += 1;

//                            Log.e(TAG, "onResponse:page-->"+pagecount++ );

                                    Pref_storage.setDetail(context, "Page_quesans", String.valueOf(pagecount));

                                    Log.e(TAG, "onResponse:shared_page-->" + Pref_storage.getDetail(context, "Page_quesans"));
                                } else {

                                }


                                quesidList.clear();
                                pollDataList.clear();

                                getQuestionAllDataList.clear();

                                //Success

                                for (int i = 0; i < response.body().getData().size(); i++) {

                                    Log.e("karthick", "responseSize->" + response.body().getData().get(i).getQuesType());
                                    Log.e("karthick", "responseSize->" + new Gson().toJson(response.body()));

                                    String questId = response.body().getData().get(i).getQuestId();
                                    String askQuestion = response.body().getData().get(i).getAskQuestion();
                                    String totalAnswers = response.body().getData().get(i).getTotalAnswers();
                                    String totalComments = response.body().getData().get(i).getTotalComments();
                                    int quesCreatedBy = Integer.parseInt(response.body().getData().get(i).getCreatedBy());
                                    String quesCreatedOn = response.body().getData().get(i).getCreatedOn();
                                    String country = response.body().getData().get(i).getCountry();
                                    String city = response.body().getData().get(i).getCity();
                                    String latitude = response.body().getData().get(i).getLatitude();
                                    String longitude = response.body().getData().get(i).getLongitude();
                                    String createdBy = response.body().getData().get(i).getCreatedBy();
                                    String createdOn = response.body().getData().get(i).getCreatedOn();
                                    String active = response.body().getData().get(i).getActive();
                                    String deleted = response.body().getData().get(i).getDeleted();
                                    String oauthProvider = response.body().getData().get(i).getOauthProvider();
                                    String oauthUid = response.body().getData().get(i).getOauthUid();
                                    String email = response.body().getData().get(i).getEmail();
                                    String imei = response.body().getData().get(i).getImei();
                                    String contNo = response.body().getData().get(i).getContNo();
                                    String gender = response.body().getData().get(i).getGender();
                                    String dob = response.body().getData().get(i).getDob();
                                    String picture = response.body().getData().get(i).getPicture();
                                    String pollRegId = response.body().getData().get(i).getPollRegId();


                                    String firstName = response.body().getData().get(i).getFirstName();
                                    String lastName = response.body().getData().get(i).getLastName();
                                    String userPicture = response.body().getData().get(i).getPicture();
                                    String quesType = response.body().getData().get(i).getQuesType();
                                    String redirect_url = response.body().getData().get(i).getRedirect_url();
                                    int ques_follow = response.body().getData().get(i).getQuesFollow();
                                    int totalQuestFollow = response.body().getData().get(i).getTotalQuestFollow();
                                    String totalQuestRequest = response.body().getData().get(i).getTotalQuestRequest();
                                    String pollId = response.body().getData().get(i).getPollId();
                                    int total_followers = response.body().getData().get(i).getTotalFollowers();

                                    answerDataList = response.body().getData().get(i).getAnswer();
                                    pollDataArrayList = response.body().getData().get(i).getPoll();
                                    QuestLastCmtList = response.body().getData().get(i).getQuestLastCmt();

                                    String userId = response.body().getData().get(i).getUserId();
                                    int pollsize = response.body().getData().get(i).getPoll().size();

                                    questionAnswerPojo = new QuestionAnswerPojo(
                                            pollsize,
                                            questId,
                                            askQuestion,
                                            quesType,
                                            country,
                                            city,
                                            totalAnswers,
                                            totalComments,
                                            latitude,
                                            longitude,
                                            createdBy,
                                            createdOn,
                                            active,
                                            deleted,
                                            userId,
                                            oauthProvider,
                                            oauthUid,
                                            firstName,
                                            lastName,
                                            email,
                                            imei,
                                            contNo,
                                            gender,
                                            dob,
                                            picture,
                                            total_followers,
                                            totalQuestFollow,
                                            totalQuestRequest,
                                            pollRegId,
                                            pollId,
                                            ques_follow,
                                            QuestLastCmtList,
                                            pollDataArrayList,
                                            answerDataList,
                                            redirect_url);


                                    getQuestionAllDataList.add(questionAnswerPojo);


                                    getQuestionAllAdapter.notifyDataSetChanged();




                                }
                                question_progress_bar.setIndeterminate(false);
                                question_progress_bar.setVisibility(View.GONE);


                                Log.e("duration", "duration->" + getQuestionAllDataList.size());


                            } else {
                                question_progress_bar.setIndeterminate(false);
                                question_progress_bar.setVisibility(View.GONE);


                             }


                        } else {

                            if (progressbar_review != null) {
                                progressbar_review.setVisibility(View.GONE);
                                progressbar_review.setIndeterminate(false);
                            }



                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<QuestionOutputPojo> call, Throwable t) {
                        if (progressbar_review != null) {
                            progressbar_review.setVisibility(View.GONE);
                            progressbar_review.setIndeterminate(false);
                        }

                        if (getQuestionAllDataList.isEmpty()) {

                            txt_no_dish.setVisibility(View.VISIBLE);

                        } else {
                            txt_no_dish.setVisibility(View.GONE);
                        }
                        //Error
                        Log.e("Balaji", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {
                if (progressbar_review != null) {
                    progressbar_review.setVisibility(View.GONE);
                    progressbar_review.setIndeterminate(false);
                }
                e.printStackTrace();

            }

        } else {

            if (progressbar_review != null) {
                progressbar_review.setVisibility(View.GONE);
                progressbar_review.setIndeterminate(false);
            }

            Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }


    }

    //Pagination

    private void getAllQuesData(int pagecountt) {


        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                 Call<QuestionOutputPojo> call = apiService.getQuestionAll1("get_question_answer_all", userid, pagecountt);

                call.enqueue(new Callback<QuestionOutputPojo>() {

                    @Override
                    public void onResponse(Call<QuestionOutputPojo> call, Response<QuestionOutputPojo> response) {

                        if (response.body() != null) {


                            String responseStatus = response.body().getResponseMessage();

                            Log.e("Balaji", "responseStatus->" + responseStatus);


                            if (responseStatus.equals("success")) {


                                if (Pref_storage.getDetail(context, "Page_quesans").isEmpty() || Pref_storage.getDetail(context, "Page_quesans") == null) {
//                            Pref_storage.getDetail(context,"Page_count")

                                } else {
                                    int page_incre = Integer.parseInt(Pref_storage.getDetail(context, "Page_quesans"));

                                    Log.e(TAG, "onResponsePagination: page_incre-->" + page_incre);


                                    int page_incre_shared = page_incre + 1;

                                    Pref_storage.setDetail(context, "Page_quesans", String.valueOf(page_incre_shared));

                                    Log.e(TAG, "onResponsePagination: page_count_incre-->" + Integer.parseInt(Pref_storage.getDetail(context, "Page_quesans")));
                                    pagecount = Integer.parseInt(Pref_storage.getDetail(context, "Page_quesans"));

                                }



                                paginagtionDataList.clear();

                                //Success

                                for (int i = 0; i < response.body().getData().size(); i++) {

                                    Log.e("qanda", "responseSize->" + response.body().getData().get(i).getQuesType());
                                    Log.e("qanda", "responseSize->" + new Gson().toJson(response.body()));

                                    String questId = response.body().getData().get(i).getQuestId();
                                    String askQuestion = response.body().getData().get(i).getAskQuestion();
                                    String totalAnswers = response.body().getData().get(i).getTotalAnswers();
                                    String totalComments = response.body().getData().get(i).getTotalComments();
                                    int quesCreatedBy = Integer.parseInt(response.body().getData().get(i).getCreatedBy());
                                    String quesCreatedOn = response.body().getData().get(i).getCreatedOn();
                                    String country = response.body().getData().get(i).getCountry();
                                    String city = response.body().getData().get(i).getCity();
                                    String latitude = response.body().getData().get(i).getLatitude();
                                    String longitude = response.body().getData().get(i).getLongitude();
                                    String createdBy = response.body().getData().get(i).getCreatedBy();
                                    String createdOn = response.body().getData().get(i).getCreatedOn();
                                    String active = response.body().getData().get(i).getActive();
                                    String deleted = response.body().getData().get(i).getDeleted();
                                    String oauthProvider = response.body().getData().get(i).getOauthProvider();
                                    String oauthUid = response.body().getData().get(i).getOauthUid();
                                    String email = response.body().getData().get(i).getEmail();
                                    String imei = response.body().getData().get(i).getImei();
                                    String contNo = response.body().getData().get(i).getContNo();
                                    String redirect_url = response.body().getData().get(i).getRedirect_url();
                                    String gender = response.body().getData().get(i).getGender();
                                    String dob = response.body().getData().get(i).getDob();
                                    String picture = response.body().getData().get(i).getPicture();
                                    String pollRegId = response.body().getData().get(i).getPollRegId();


                                    String firstName = response.body().getData().get(i).getFirstName();
                                    String lastName = response.body().getData().get(i).getLastName();
                                    String userPicture = response.body().getData().get(i).getPicture();
                                    String quesType = response.body().getData().get(i).getQuesType();
                                    int ques_follow = response.body().getData().get(i).getQuesFollow();
                                    int totalQuestFollow = response.body().getData().get(i).getTotalQuestFollow();
                                    String totalQuestRequest = response.body().getData().get(i).getTotalQuestRequest();
                                    String pollId = response.body().getData().get(i).getPollId();
                                    int total_followers = response.body().getData().get(i).getTotalFollowers();

                                    answerDataList = response.body().getData().get(i).getAnswer();
                                    pollDataArrayList = response.body().getData().get(i).getPoll();
                                    QuestLastCmtList = response.body().getData().get(i).getQuestLastCmt();

                                    String userId = response.body().getData().get(i).getUserId();
                                    int pollsize = response.body().getData().get(i).getPoll().size();

                                    questionAnswerPojo = new QuestionAnswerPojo(
                                            pollsize,
                                            questId,
                                            askQuestion,
                                            quesType,
                                            country,
                                            city,
                                            totalAnswers,
                                            totalComments,
                                            latitude,
                                            longitude,
                                            createdBy,
                                            createdOn,
                                            active,
                                            deleted,
                                            userId,
                                            oauthProvider,
                                            oauthUid,
                                            firstName,
                                            lastName,
                                            email,
                                            imei,
                                            contNo,
                                            gender,
                                            dob,
                                            picture,
                                            total_followers,
                                            totalQuestFollow,
                                            totalQuestRequest,
                                            pollRegId,
                                            pollId,
                                            ques_follow,
                                            QuestLastCmtList,
                                            pollDataArrayList,
                                            answerDataList,
                                            redirect_url);


                                    getQuestionAllDataList.add(questionAnswerPojo);



                                }

                                page_loader.setVisibility(View.GONE);

                                Log.e("duration", "duration->" + getQuestionAllDataList.size());

                                LinkedHashSet<QuestionAnswerPojo> mainSet = new LinkedHashSet<>(getQuestionAllDataList);
                                getQuestionAllDataList.clear();
                                getQuestionAllDataList.addAll(mainSet);


                                question_progress_bar.setIndeterminate(false);
                                question_progress_bar.setVisibility(View.GONE);

                                questionRecylerView.setNestedScrollingEnabled(false);
                                getQuestionAllAdapter.notifyDataSetChanged();

                            } else {


                                question_progress_bar.setIndeterminate(false);
                                question_progress_bar.setVisibility(View.GONE);
                             }


                        } else {

                            question_progress_bar.setIndeterminate(false);
                            question_progress_bar.setVisibility(View.GONE);

                            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<QuestionOutputPojo> call, Throwable t) {
                        question_progress_bar.setIndeterminate(false);
                        question_progress_bar.setVisibility(View.GONE);

                        page_loader.setVisibility(View.GONE);

                        Toast.makeText(context, "You've seen all data.. No new data found.", Toast.LENGTH_SHORT).show();

                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }


    }




    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            case R.id.ask_question:
                /*Intent to create new question page*/
                startActivity(new Intent(getActivity(), CreateQuestion.class));

                break;
        }

    }


    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        View view = (View) question_scrollview.getChildAt(question_scrollview.getChildCount() - 1);
        int diff = (view.getBottom() - (question_scrollview.getHeight() + question_scrollview.getScrollY()));

        // if diff is zero, then the bottom has been reached


        Log.e(TAG, "onScrollChange: PageCount Shared--> " + Pref_storage.getDetail(context, "Page_quesans"));

        if (diff == 0) {




            if (pagecount == 1) {

            } else {


                if (Pref_storage.getDetail(context, "Page_quesans").isEmpty() || Pref_storage.getDetail(context, "Page_quesans") == null) {

                } else {

                    pagecount = Integer.parseInt(Pref_storage.getDetail(context, "Page_quesans"));

                    Log.e(TAG, "onScrollChange: pagecount " + pagecount);

                    page_loader.setVisibility(View.VISIBLE);

                    getAllQuesData(pagecount);

                }


            }


            // do stuff
        }

        Utility.hideKeyboard(v);
    }


    @Override
    public void onRefereshedFragment(View view, final int adapterPosition, View pollAdapterView) {

        switch (view.getId()) {

            case R.id.tv_poll_undo:

                if (Utility.isConnected(Objects.requireNonNull(getActivity()))) {

                    progressbar_review = (ProgressBar) pollAdapterView.findViewById(R.id.progressbar_review);

                    progressbar_review.setVisibility(View.VISIBLE);
                    progressbar_review.setIndeterminate(true);

                    int totalVotes = getQuestionAllDataList.get(adapterPosition).getPoll().get(0).getTotalPollUser();
                    totalVotes = totalVotes - 1;

                    if (totalVotes <= 0) {

                        getQuestionAllDataList.get(adapterPosition).getPoll().get(0).setTotalPollUser(totalVotes);
                        TextView total_poll_votes = (TextView) pollAdapterView.findViewById(R.id.total_poll_votes);
                        total_poll_votes.setText("No Votes");

                    } else if (totalVotes == 1) {

                        getQuestionAllDataList.get(adapterPosition).getPoll().get(0).setTotalPollUser(totalVotes);
                        TextView total_poll_votes = (TextView) pollAdapterView.findViewById(R.id.total_poll_votes);
                        total_poll_votes.setText(totalVotes + " Vote");


                    } else {

                        getQuestionAllDataList.get(adapterPosition).getPoll().get(0).setTotalPollUser(totalVotes);
                        TextView total_poll_votes = (TextView) pollAdapterView.findViewById(R.id.total_poll_votes);
                        total_poll_votes.setText(totalVotes + " Vote");

                    }


                    for (int i = 0; i < getQuestionAllDataList.get(adapterPosition).getPoll().size(); i++) {

                        if (getQuestionAllDataList.get(adapterPosition).getPollId() == getQuestionAllDataList.get(adapterPosition).getPoll().get(i).getPollId()) {

                            if (!undoPoll) {

                                int totalPoll = getQuestionAllDataList.get(adapterPosition).getPoll().get(i).getTotalPoll();
                                totalPoll = totalPoll - 1;
                                getQuestionAllDataList.get(adapterPosition).getPoll().get(i).setTotalPoll(totalPoll);
                                undoPoll = true;

                            }

                        }

                    }

                    // Display progress
                    final LinearLayout ll_progress_section = pollAdapterView.findViewById(R.id.ll_progress_section);
                    final LinearLayout ll_answer_section = pollAdapterView.findViewById(R.id.ll_answer_section);
                    final TextView tv_poll_undo = pollAdapterView.findViewById(R.id.tv_poll_undo);
                    final ImageView poll_one_checked = pollAdapterView.findViewById(R.id.poll_one_checked);
                    final ImageView poll_two_checked = pollAdapterView.findViewById(R.id.poll_two_checked);
                    final ImageView poll_three_checked = pollAdapterView.findViewById(R.id.poll_three_checked);
                    final ImageView poll_four_checked = pollAdapterView.findViewById(R.id.poll_four_checked);


                    ll_progress_section.setVisibility(View.GONE);
                    ll_answer_section.setVisibility(View.VISIBLE);
                    tv_poll_undo.setVisibility(View.GONE);
                    poll_one_checked.setVisibility(View.GONE);
                    poll_two_checked.setVisibility(View.GONE);
                    poll_three_checked.setVisibility(View.GONE);
                    poll_four_checked.setVisibility(View.GONE);

                    if (Utility.isConnected(context)) {

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Log.e("Balaji", "userid->" + userid);
                            Log.e("Balaji", "questionid->" + getQuestionAllDataList.get(adapterPosition).getQuestId());

                            Call<create_delete_answer_poll_undoPOJO> call = apiService.undoPollVote("create_delete_answer_poll_undo",
                                    Integer.parseInt(getQuestionAllDataList.get(adapterPosition).getQuestId()),
                                    userid);

                            call.enqueue(new Callback<create_delete_answer_poll_undoPOJO>() {

                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onResponse(Call<create_delete_answer_poll_undoPOJO> call, Response<create_delete_answer_poll_undoPOJO> response) {

                                    if (response.body() != null) {

                                        String responseStatus = response.body().getResponseMessage();

                                        Log.e("Balaji", "responseStatus->" + responseStatus);

                                        if (responseStatus.equals("success")) {


                                            //Success


                                            pagecount = Integer.parseInt(Pref_storage.getDetail(context, "Page_quesans"));

                                            //API calling
                                            swipegetAllQuesData();


                                        }


                                    } else {

                                        progressbar_review.setVisibility(View.GONE);
                                        progressbar_review.setIndeterminate(false);

                                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                                    }


                                }

                                @Override
                                public void onFailure(Call<create_delete_answer_poll_undoPOJO> call, Throwable t) {
                                    //Error

                                    progressbar_review.setVisibility(View.GONE);
                                    progressbar_review.setIndeterminate(false);


                                    Log.e("Balaji", "" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    } else {

                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                    }
                } else {

                    Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                }
                break;


            case R.id.txt_adapter_post:

                if (Utility.isConnected(Objects.requireNonNull(getActivity()))) {

                    progressbar_review = (ProgressBar) pollAdapterView.findViewById(R.id.progressbar_review);

                    progressbar_review.setVisibility(View.VISIBLE);
                    progressbar_review.setIndeterminate(true);

                    edittxt_comment = (EmojiconEditText) pollAdapterView.findViewById(R.id.edittxt_comment);
                    tv_view_all_comments = (TextView) pollAdapterView.findViewById(R.id.tv_view_all_comments);

                    if (edittxt_comment.getText().toString().isEmpty()) {

                        Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();

                    } else {

                        Utility.hideKeyboard(view);

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<GetPollCommentsOutput> call = apiService.get_create_edit_question_comments("create_edit_question_comments",

                                    Integer.parseInt(getQuestionAllDataList.get(adapterPosition).getQuestId()),
                                    0,
                                    userid,
                                    edittxt_comment.getText().toString());

                            call.enqueue(new Callback<GetPollCommentsOutput>() {

                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onResponse(Call<GetPollCommentsOutput> call, Response<GetPollCommentsOutput> response) {

                                    edittxt_comment.setText("");

                                    if (response.code() == 500) {

                                        progressbar_review.setVisibility(View.GONE);
                                        progressbar_review.setIndeterminate(false);

                                        Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();

                                    } else {

                                        progressbar_review.setVisibility(View.GONE);
                                        progressbar_review.setIndeterminate(false);


                                        if (response.body() != null) {

                                            if (Objects.requireNonNull(response.body()).getResponseMessage().equals("success")) {

                                                if (response.body().getData().size() > 0) {

                                                    if (tv_view_all_comments.getVisibility() == View.GONE) {

                                                        tv_view_all_comments.setVisibility(View.VISIBLE);
                                                        //String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                                        String comments = getString(R.string.view_one_comment);
                                                        tv_view_all_comments.setText(comments);

                                                    } else {

                                                        String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                                        tv_view_all_comments.setText(comments);
                                                    }

                                                    Intent intent = new Intent(context, ViewPollComments.class);

                                                    intent.putExtra("comment_quesId", String.valueOf(getQuestionAllDataList.get(adapterPosition).getQuestId()));
                                                    intent.putExtra("comment_question", getQuestionAllDataList.get(adapterPosition).getAskQuestion());
                                                    intent.putExtra("comment_picture", getQuestionAllDataList.get(adapterPosition).getPicture());
                                                    intent.putExtra("comment_firstname", getQuestionAllDataList.get(adapterPosition).getFirstName() + " " + getQuestionAllDataList.get(adapterPosition).getLastName());
                                                    intent.putExtra("comment_createdon", getQuestionAllDataList.get(adapterPosition).getCreatedOn());
                                                    intent.putExtra("comment_pollid", getQuestionAllDataList.get(adapterPosition).getPollId());

                                                    intent.putExtra("pollsize", String.valueOf(getQuestionAllDataList.get(adapterPosition).getPoll().size()));

                                                    try {

                                                        double userVotesTwo = getQuestionAllDataList.get(adapterPosition).getPoll().get(0).getTotalPollUser();
                                                        intent.putExtra("userVotes", String.valueOf(userVotesTwo));

                                                        if (getQuestionAllDataList.get(adapterPosition).getPoll().size() == 2) {
                                                            double totalVotesOne = getQuestionAllDataList.get(adapterPosition).getPoll().get(0).getTotalPoll();
                                                            double totalVotesTwo = getQuestionAllDataList.get(adapterPosition).getPoll().get(1).getTotalPoll();

                                                            intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                                            intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                                            intent.putExtra("comment_poll1", getQuestionAllDataList.get(adapterPosition).getPoll().get(0).getPollList());
                                                            intent.putExtra("comment_poll2", getQuestionAllDataList.get(adapterPosition).getPoll().get(1).getPollList());
                                                        } else if (getQuestionAllDataList.get(adapterPosition).getPoll().size() == 3) {
                                                            double totalVotesOne = getQuestionAllDataList.get(adapterPosition).getPoll().get(0).getTotalPoll();
                                                            double totalVotesTwo = getQuestionAllDataList.get(adapterPosition).getPoll().get(1).getTotalPoll();
                                                            double totalVotesThree = getQuestionAllDataList.get(adapterPosition).getPoll().get(2).getTotalPoll();

                                                            intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                                            intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                                            intent.putExtra("totalVotesThree", String.valueOf(totalVotesThree));

                                                            intent.putExtra("comment_poll1", getQuestionAllDataList.get(adapterPosition).getPoll().get(0).getPollList());
                                                            intent.putExtra("comment_poll2", getQuestionAllDataList.get(adapterPosition).getPoll().get(1).getPollList());
                                                            intent.putExtra("comment_poll3", getQuestionAllDataList.get(adapterPosition).getPoll().get(2).getPollList());
                                                        } else if (getQuestionAllDataList.get(adapterPosition).getPoll().size() == 4) {


                                                            double totalVotesOne = getQuestionAllDataList.get(adapterPosition).getPoll().get(0).getTotalPoll();
                                                            double totalVotesTwo = getQuestionAllDataList.get(adapterPosition).getPoll().get(1).getTotalPoll();
                                                            double totalVotesThree = getQuestionAllDataList.get(adapterPosition).getPoll().get(2).getTotalPoll();
                                                            double totalVotesFour = getQuestionAllDataList.get(adapterPosition).getPoll().get(3).getTotalPoll();

                                                            intent.putExtra("totalVotesOne", String.valueOf(totalVotesOne));
                                                            intent.putExtra("totalVotesTwo", String.valueOf(totalVotesTwo));
                                                            intent.putExtra("totalVotesThree", String.valueOf(totalVotesThree));
                                                            intent.putExtra("totalVotesFour", String.valueOf(totalVotesFour));

                                                            intent.putExtra("comment_poll1", getQuestionAllDataList.get(adapterPosition).getPoll().get(0).getPollList());
                                                            intent.putExtra("comment_poll2", getQuestionAllDataList.get(adapterPosition).getPoll().get(1).getPollList());
                                                            intent.putExtra("comment_poll3", getQuestionAllDataList.get(adapterPosition).getPoll().get(2).getPollList());
                                                            intent.putExtra("comment_poll4", getQuestionAllDataList.get(adapterPosition).getPoll().get(3).getPollList());
                                                        }


                                                        intent.putExtra("comment_votecount", getQuestionAllDataList.get(adapterPosition).getPoll().get(0).getTotalPollUser() + " votes ");

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    startActivity(intent);

                                                } else {

                                                }

                                            } else {

                                            }
                                        }

                                    }
                                }

                                @Override
                                public void onFailure(Call<GetPollCommentsOutput> call, Throwable t) {
                                    //Error

                                    progressbar_review.setVisibility(View.GONE);
                                    progressbar_review.setIndeterminate(false);

                                    Log.e("Balaji", "" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            Log.e("Balaji", "" + e.getMessage());
                            e.printStackTrace();

                        }
                    }

                } else {

                    Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.edittxt_comment:

                edittxt_comment = (EmojiconEditText) pollAdapterView.findViewById(R.id.edittxt_comment);
                edittxt_comment.requestFocus();
                break;

            case R.id.img_sharechat:

                Intent intent1 = new Intent(context, ChatActivity.class);
                if (getQuestionAllDataList.size() > 0) {
                    if (getQuestionAllDataList.get(adapterPosition).getRedirect_url() != null) {
                        intent1.putExtra("redirect_url", getQuestionAllDataList.get(adapterPosition).getRedirect_url());
                    }
                }
                Pref_storage.setDetail(getActivity(), "shareView", Constants.questionAnswer);
                startActivity(intent1);
                break;

        }
    }

    @Override
    public void onPostCommitedView(View view, final int adapterPosition, View hotelDetailsView) {

        switch (view.getId()) {

            case R.id.ll_answer:

                final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(context);
                LayoutInflater inflater11 = LayoutInflater.from(context);
                @SuppressLint("InflateParams") final View dialogView1 = inflater11.inflate(R.layout.alert_answer_layout, null);
                dialogBuilder1.setView(dialogView1);
                dialogBuilder1.setTitle("");

                answer = (TextView) hotelDetailsView.findViewById(R.id.answer);


                final EditText edittxt_comment;
                ImageView userphoto_comment;
                final TextView user_answer;

                edittxt_comment = (EditText) dialogView1.findViewById(R.id.edittxt_comment);
                userphoto_comment = (ImageView) dialogView1.findViewById(R.id.userphoto_comment);
                user_answer = (TextView) dialogView1.findViewById(R.id.user_answer);
                txt_adapter_post = (TextView) dialogView1.findViewById(R.id.txt_adapter_post);
                progress_dialog_follower = (ProgressBar) dialogView1.findViewById(R.id.progress_dialog_follower);

                if (progress_dialog_follower.getVisibility() == VISIBLE) {
                    dialogBuilder1.setCancelable(false);
                } else {
                    dialogBuilder1.setCancelable(true);
                }

                dialog1 = dialogBuilder1.create();
                dialog1.show();

                // User Image loading
                GlideApp.with(context)
                        .load(Pref_storage.getDetail(context, "picture_user_login"))
                        .placeholder(R.drawable.ic_add_photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .into(userphoto_comment);

                user_answer.setText(getQuestionAllDataList.get(adapterPosition).getAskQuestion());
                edittxt_comment.setHint("Add your answer");

                // Edittext comment listener
                edittxt_comment.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {


                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (edittxt_comment.getText().toString().trim().length() != 0) {
                            txt_adapter_post.setVisibility(View.VISIBLE);
                        } else {
                            txt_adapter_post.setVisibility(View.GONE);
                        }
                    }
                });

                if (txt_adapter_post != null) {
                    txt_adapter_post.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (edittxt_comment.getText().toString().isEmpty()) {
                                edittxt_comment.setError("Enter your answer");
                            } else {
                                progress_dialog_follower.setVisibility(VISIBLE);
                                progress_dialog_follower.setIndeterminate(true);

                                //create answer api
                                createAnswer(adapterPosition, edittxt_comment);

                                Utility.hideKeyboard(view);
                            }
                        }
                    });
                }
                break;

            case R.id.img_sharechat:

                Intent intent1 = new Intent(context, ChatActivity.class);
                if (getQuestionAllDataList.size() > 0) {
                    if (getQuestionAllDataList.get(adapterPosition).getRedirect_url() != null) {
                        intent1.putExtra("redirect_url", getQuestionAllDataList.get(adapterPosition).getRedirect_url());
                    }
                }
                Pref_storage.setDetail(getActivity(), "shareView", Constants.questionAnswer);
                startActivity(intent1);
                break;

            default:
                break;


        }
    }


    private void createAnswer(final int adapterPosition, EditText edittxt_comment) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {

            gpsTracker = new GPSTracker(context);
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

            Call<AnswerPojoClass> call = apiService.createAnswerView("create_answer", 0,
                    Integer.parseInt(getQuestionAllDataList.get(adapterPosition).getQuestId()),
                    0, 0, edittxt_comment.getText().toString(), latitude, longitude, userid);

            call.enqueue(new Callback<AnswerPojoClass>() {
                @Override
                public void onResponse(Call<AnswerPojoClass> call, Response<AnswerPojoClass> response) {

                    if (response.body() != null) {

                        int responseStatus = response.body().getResponseCode();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus == 1) {

                            //Toast.makeText(context, "Answer created successfully", Toast.LENGTH_SHORT).show();
                            //Success
                            dialog1.dismiss();

                            progress_dialog_follower.setVisibility(View.GONE);
                            progress_dialog_follower.setIndeterminate(false);

                            answer.setText(response.body().getData().get(0).getAskAnswer());


                            //Success
                            Intent intent = new Intent(context, ViewQuestionAnswer.class);
                            intent.putExtra("position", String.valueOf(adapterPosition));
                            intent.putExtra("quesId", getQuestionAllDataList.get(adapterPosition).getQuestId());
                            intent.putExtra("firstname", getQuestionAllDataList.get(adapterPosition).getFirstName() + " "
                                    + getQuestionAllDataList.get(adapterPosition).getLastName());
                            intent.putExtra("createdon", getQuestionAllDataList.get(adapterPosition).getCreatedOn());
                            intent.putExtra("profile", getQuestionAllDataList.get(adapterPosition).getPicture());
                            startActivity(intent);

                        } else if (responseStatus == 0) {

                            dialog1.dismiss();

                            progress_dialog_follower.setVisibility(View.GONE);
                            progress_dialog_follower.setIndeterminate(false);

                        }

                    } else {


                        progress_dialog_follower.setVisibility(View.GONE);
                        progress_dialog_follower.setIndeterminate(false);
                        dialog1.dismiss();


                        Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                    }


                }

                @Override
                public void onFailure(Call<AnswerPojoClass> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "FailureError" + t.getMessage());
                    dialog1.dismiss();

                    progress_dialog_follower.setVisibility(View.GONE);
                    progress_dialog_follower.setIndeterminate(false);

                }
            });
        } catch (Exception e) {
            Log.e("FailureError", "FailureError" + e.getMessage());
            e.printStackTrace();
        }
    }
}