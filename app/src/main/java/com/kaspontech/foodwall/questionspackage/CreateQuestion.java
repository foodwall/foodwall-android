package com.kaspontech.foodwall.questionspackage;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.eventspackage.CreateEvent;
import com.kaspontech.foodwall.eventspackage.Events;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.historicalMapPackage.CreateHistoricalMap;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Question.CreateQuestionOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateQuestion extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    /**
     * Application TAG
     */
    private static final String TAG = "CreateQuestion";

    /**
     * Action Bar
     */
    Toolbar actionBar;

    /**
     * Close button press
     */
    ImageButton close;
    /**
     * Create question textview
     */
    TextView createQuestion;

    /**
     * Edittexts
     */
    EditText getNormalQuestionsInput;
    EditText getPollQuestionsInput;
    EditText pollAnswerOne;
    EditText pollAnswerTwo;
    EditText pollAnswerThird;
    EditText pollAnswerFour;
    /**
     * Segment groups
     */
    SegmentedGroup segmentedGroup;
    /**
     * Radio button for question type
     */
    RadioButton questionType;
    /**
     * Radio button for poll type
     */
    RadioButton pollType;
    /**
     * Question layout
     */
    LinearLayout questionLayout;

    /**
     * Poll layout
     */
    LinearLayout pollLayout;

    /* Latitude and longitude values */
    private double latitude;
    private double longitude;
    /**
     * GPS tracker
     */
    GPSTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_question);


        /*Gps tracker initialization*/
        gpsTracker = new GPSTracker(CreateQuestion.this);

        /*Toolbar tracker initialization*/

        actionBar = findViewById(R.id.action_bar_create_ques);

        /*Close button initialization*/

        close = actionBar.findViewById(R.id.close);

        /*Create Question initialization*/
        createQuestion = actionBar.findViewById(R.id.tv_create_question);

        /* Edit text input initialization*/

        getNormalQuestionsInput = findViewById(R.id.get_normal_question);
        getPollQuestionsInput = findViewById(R.id.get_poll_question);
        pollAnswerOne = findViewById(R.id.poll_answer_one);
        pollAnswerTwo = findViewById(R.id.poll_answer_two);
        pollAnswerThird = findViewById(R.id.poll_answer_third);
        pollAnswerFour = findViewById(R.id.poll_answer_four);

        /*Segment group initialization*/
        segmentedGroup = findViewById(R.id.segmented_question);

        /*QuestionLayout initialization*/

        questionLayout = findViewById(R.id.ll_question_section);
        /*Poll Layout initialization*/
        pollLayout = findViewById(R.id.ll_poll_section);
        /*Question Type radio button initialization*/
        questionType = findViewById(R.id.type_question);
        /*Ans Type radio button initialization*/
        pollType = findViewById(R.id.type_poll);

        /*On click listener*/
        close.setOnClickListener(this);
        createQuestion.setOnClickListener(this);
        segmentedGroup.setOnCheckedChangeListener(this);

        /*Visibility of question & poll layout*/
        if (questionLayout.getVisibility() == View.VISIBLE) {

            pollLayout.setVisibility(View.GONE);

        } else {

            pollLayout.setVisibility(View.GONE);
            questionLayout.setVisibility(View.VISIBLE);

        }

        /*Latitude and longitude values*/
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();


        /*  Edit text normal question listener */

        getNormalQuestionsInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String str = s.toString();
                /*Space not allowing condition*/
                if (str.equals(" ")) {

                    Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();

                    getNormalQuestionsInput.setText(getNormalQuestionsInput.getText().toString().replaceAll(" ", ""));
                    getNormalQuestionsInput.setSelection(getNormalQuestionsInput.getText().length());

                }

            }
        });


        /* Edit text question listener */

        getPollQuestionsInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String str = s.toString();
                /*Space not allowing condition*/

                if (str.equals(" ")) {

                    Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                    getPollQuestionsInput.setText(getPollQuestionsInput.getText().toString().replaceAll(" ", ""));
                    getPollQuestionsInput.setSelection(getPollQuestionsInput.getText().length());

                }

            }
        });


    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
            /*Close button click listener*/
            case R.id.close:

              /*  float deg = close.getRotation() + 180F;
                close.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
                *//*Intent intent = new Intent(CreateQuestion.this, Home.class);
                Bundle bundle = new Bundle();
                bundle.putInt("launchType", 3);
                intent.putExtras(bundle);
                startActivity(intent);*//*
                finish();*/

                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Confirm")
                        .setContentText("Are you sure you want to skip")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                float deg = close.getRotation() + 180F;
                                close.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
                /*Intent intent = new Intent(CreateQuestion.this, Home.class);
                Bundle bundle = new Bundle();
                bundle.putInt("launchType", 3);
                intent.putExtras(bundle);
                startActivity(intent);*/
                                finish();
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


                break;


            case R.id.tv_create_question:

                /*Create question API calling*/
                int selectedId = segmentedGroup.getCheckedRadioButtonId();

                /*Hiding keyboarad*/

                Utility.hideKeyboard(v);


                switch (selectedId) {

                    case R.id.type_question:


                        if (getNormalQuestionsInput.getText().toString().trim().length() != 0) {
                            /*Loader*/
                            final AlertDialog dialog = new SpotsDialog.Builder().setContext(CreateQuestion.this).setMessage("").build();

                            dialog.show();
                            if (Utility.isConnected(getApplicationContext())) {
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(CreateQuestion.this, "userId"));


                                    Call<CreateQuestionOutput> call = apiService.createNormalQuestion("create_question", 0, getNormalQuestionsInput.getText().toString(), latitude, longitude, userid, 0, 0);


                                    call.enqueue(new Callback<CreateQuestionOutput>() {
                                        @Override
                                        public void onResponse(Call<CreateQuestionOutput> call, Response<CreateQuestionOutput> response) {


                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                if (responseStatus.equals("success")) {

                                                    Toast.makeText(CreateQuestion.this, "Question created sucessfully", Toast.LENGTH_SHORT).show();
                                                    //Success
                                               /* Intent intent1 = new Intent(CreateQuestion.this, Home.class);
                                                Bundle bundle1 = new Bundle();
                                                bundle1.putInt("launchType", 3);
                                                intent1.putExtras(bundle1);
                                                startActivity(intent1);*/
                                                    Intent intent = new Intent(CreateQuestion.this, Home.class);
                                                    intent.putExtra("redirect", "qa");
                                                    startActivity(intent);
                                                    finish();


                                                }


                                            } else {

                                                Toast.makeText(getApplicationContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();


                                            }


                                        }

                                        @Override
                                        public void onFailure(Call<CreateQuestionOutput> call, Throwable t) {
                                            //Error
                                            Log.e(TAG, getString(R.string.failure_error) + t.getMessage());
                                            dialog.dismiss();

                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                }

                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                            }

                        } else {

                            getNormalQuestionsInput.setError("Ask Question");

                        }


                        break;

                    case R.id.type_poll:


                        if (validate()) {

                            /*Loader*/
                            final AlertDialog dialog = new SpotsDialog.Builder().setContext(CreateQuestion.this).setMessage("").build();

                            dialog.show();
                            if (Utility.isConnected(getApplicationContext())) {

                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                try {

                                    int userid = Integer.parseInt(Pref_storage.getDetail(CreateQuestion.this, "userId"));

                                    List<String> pollList = new ArrayList<>();


                                    pollList.add(pollAnswerOne.getText().toString());
                                    pollList.add(pollAnswerTwo.getText().toString());

                                    if (pollAnswerThird.getText().toString().length() != 0) {
                                        pollList.add(pollAnswerThird.getText().toString());
                                    }

                                    if (pollAnswerFour.getText().toString().length() != 0) {
                                        pollList.add(pollAnswerFour.getText().toString());
                                    }

                                    String json = new Gson().toJson(pollList);

                                    Call<CreateQuestionOutput> call = apiService.createPollQuestion("create_question",
                                            0, getPollQuestionsInput.getText().toString(),
                                            latitude, longitude,
                                            userid,
                                            1,
                                            json);


                                    call.enqueue(new Callback<CreateQuestionOutput>() {
                                        @Override
                                        public void onResponse(Call<CreateQuestionOutput> call, Response<CreateQuestionOutput> response) {


                                            if (response.body() != null) {

                                                String responseStatus = response.body().getResponseMessage();

                                                if (responseStatus.equals("success")) {

                                                    Toast.makeText(CreateQuestion.this, "Poll created sucessfully", Toast.LENGTH_SHORT).show();
                                                    //Success
                                                   /*Intent intent1 = new Intent(CreateQuestion.this, Home.class);
                                                   Bundle bundle1 = new Bundle();
                                                   bundle1.putInt("launchType", 3);
                                                   intent1.putExtras(bundle1);
                                                   startActivity(intent1);*/
                                                    Intent intent = new Intent(CreateQuestion.this, Home.class);
                                                    intent.putExtra("redirect", "qa");
                                                    startActivity(intent);
                                                    finish();


                                                }

                                            } else {

                                                Toast.makeText(getApplicationContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();


                                            }


                                        }

                                        @Override
                                        public void onFailure(Call<CreateQuestionOutput> call, Throwable t) {
                                            //Error
                                            Log.e(TAG, getString(R.string.failure_error) + t.getMessage());
                                            dialog.dismiss();

                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();

                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                            }


                        }


                        break;


                }

                break;

        }

    }

    /*Validation*/

    private boolean validate() {

        boolean valid = true;

        String pollQuestion = getPollQuestionsInput.getText().toString().trim();

        String answerOne = pollAnswerOne.getText().toString().trim();

        String answerTwo = pollAnswerTwo.getText().toString().trim();

        String answerThree = pollAnswerThird.getText().toString();

        String answerFour = pollAnswerFour.getText().toString();


        if (pollQuestion.length() == 0 && answerOne.length() == 0 && answerTwo.length() == 0 && answerThree.length() == 0 && answerFour.length() == 0) {

            getPollQuestionsInput.requestFocus();
            getPollQuestionsInput.setError("Ask question");
            valid = false;

        } else {

            if (pollQuestion.length() == 0) {
                getPollQuestionsInput.requestFocus();
                getPollQuestionsInput.setError("Ask question");
                valid = false;
            } else {
                getPollQuestionsInput.setError(null);
            }


            if (pollQuestion.length() != 0 && answerOne.length() == 0) {
                pollAnswerOne.requestFocus();
                pollAnswerOne.setError("Enter option one");
                valid = false;
            } else {
                pollAnswerOne.setError(null);
            }


            if (answerOne.length() != 0 && answerTwo.length() == 0) {
                pollAnswerTwo.requestFocus();
                pollAnswerTwo.setError("Enter option two");
                valid = false;
            } else {
                pollAnswerTwo.setError(null);
            }


            if (answerThree.length() > 0) {

                if (pollAnswerThird.getText().toString().trim().length() == 0) {
                    valid = false;
                    pollAnswerThird.setError("Enter valid option");
                } else {
                    pollAnswerThird.setError(null);
                }

            }


            if (answerFour.length() > 0) {

                if (pollAnswerFour.getText().toString().trim().length() == 0) {
                    valid = false;
                    pollAnswerFour.setError("Enter valid option");
                } else {
                    pollAnswerFour.setError(null);

                }

            }
        }


        return valid;
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId) {

            case R.id.type_question:

                if (questionLayout.getVisibility() == View.VISIBLE) {

                    pollLayout.setVisibility(View.GONE);

                } else {

                    pollLayout.setVisibility(View.GONE);
                    questionLayout.setVisibility(View.VISIBLE);

                }

                break;

            case R.id.type_poll:


                if (pollLayout.getVisibility() == View.VISIBLE) {

                    questionLayout.setVisibility(View.GONE);

                } else {

                    questionLayout.setVisibility(View.GONE);
                    pollLayout.setVisibility(View.VISIBLE);

                }

                break;

            default:
                // Nothing to do
        }
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Confirm")
                .setContentText("Are you sure you want to skip")
                .setConfirmText("Yes")
                .setCancelText("No")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        float deg = close.getRotation() + 180F;
                        close.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
                /*Intent intent = new Intent(CreateQuestion.this, Home.class);
                Bundle bundle = new Bundle();
                bundle.putInt("launchType", 3);
                intent.putExtras(bundle);
                startActivity(intent);*/
                        finish();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();

    }

}
