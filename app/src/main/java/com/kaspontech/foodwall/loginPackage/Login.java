package com.kaspontech.foodwall.loginPackage;

import android.Manifest;
import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.LoginPojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.fcm.Constants;
import com.kaspontech.foodwall.modelclasses.LoginPojo.UpdateNewOtp_Response;
import com.kaspontech.foodwall.profilePackage.forgotPasswordActivity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;
import com.shobhitpuri.custombuttons.GoogleSignInButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.kaspontech.foodwall.utills.Utility.isValidEmail;

public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    // Log Tag
    private static final String TAG = "Login";

    // Widgets
    private Button button_login;
    private LinearLayout loginParent;
    private TextView sign_up;
    private TextView txt_frgt_pwd;
    private LoginButton facebook_login_btn;
    private EditText emailInput, passwordInput;
    private String fbemail;

    PopupWindow popupWindow;
    View popup;
    private AlertDialog alt;

    // ArrayList


    // Google plus integration
    private static final int Sign_In = 007;
    private GoogleApiClient googleApiClient;
    private GoogleSignInButton googleSignInButton;


    // Facebook Read Permission
    private static final String EMAIL = "email";
    private static final String DOB = "user_birthday";
    private static final String USER_FRIENDS = "user_friends";
    private static final String PUBLIC_PROFILE = "public_profile";


    private CallbackManager mCallbackManager;


    // Broadcast
    BroadcastReceiver broadcastReceiver;


    // Variables
    private double latitude, longitude;
    private String latitudeResult, longitudeResult;
    private String imei, fcmkey, country;
    private boolean isValidEmail, isValidPassword;


    // GPS
    GPSTracker gpsTracker;
    private GoogleApiClient mGoogleApiClient;


    // Permission codes
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private static final int REQUEST_PERMISSION_PHONE_STATE = 1;
    private int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;


    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";
    public NotificationManager mNotificationManager;

    private AlertDialog dialog;

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /**
         * FCM key for push notification
         * */

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(Login.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmkey = instanceIdResult.getToken();
                Pref_storage.setDetail(Login.this, "GcmKey", fcmkey);
                Log.e("newToken", fcmkey);
            }
        });

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        setFacebookData(loginResult);
                        // App code
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
        /*Firebase Initialiazation*/
        FirebaseApp.initializeApp(Login.this);

        /*GPS tracker Initialiazation*/
        gpsTracker = new GPSTracker(Login.this);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        // Getting latitude and longitude values

        latitudeResult = String.valueOf(gpsTracker.getLatitude());
        longitudeResult = String.valueOf(gpsTracker.getLongitude());


        /*Finding ID's of the fields*/
        emailInput = (EditText) findViewById(R.id.editText_username);
        passwordInput = (EditText) findViewById(R.id.editText_password);
//        passwordInput.setTransformationMethod(new AestericPasswordTransmission());
        button_login = (Button) findViewById(R.id.button_login);
        googleSignInButton = (GoogleSignInButton) findViewById(R.id.btn_google_signin);
        facebook_login_btn = (LoginButton) findViewById(R.id.facebook_login_btn);
        sign_up = (TextView) findViewById(R.id.signuuptext);
        txt_frgt_pwd = (TextView) findViewById(R.id.txt_frgt_pwd);
        loginParent = (LinearLayout) findViewById(R.id.loginParent);


        /*On click listeners*/
        button_login.setOnClickListener(this);
        googleSignInButton.setOnClickListener(this);
        facebook_login_btn.setOnClickListener(this);
        sign_up.setOnClickListener(this);
        txt_frgt_pwd.setOnClickListener(this);

        // Disabling login button for various conditon checking
        button_login.setEnabled(false);


        imei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        //imei = IMEIUtil.getDeviceId(Login.this);
        Log.e(TAG, "getDeviceIMEI: " + imei);
        Pref_storage.setDetail(getApplicationContext(), "IMEI", imei);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel mChannel = new NotificationChannel(Constants.CHANNEL_ID, Constants.CHANNEL_NAME, importance);
            mChannel.setDescription(Constants.CHANNEL_DESCRIPTION);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);

            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mNotificationManager.createNotificationChannel(mChannel);


            NotificationCompat.Builder builder = new NotificationCompat.Builder(Login.this);
            builder.build();
        }


        /**
         *
         * Intialize google signin button
         *
         * */
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        /**
         *
         *  Set the initial permissions to request from the user while logging in
         *
         * */
//        facebook_login_btn.setPermissions(Arrays.asList(EMAIL, USER_FRIENDS, PUBLIC_PROFILE, DOB));
        facebook_login_btn.setPermissions(Arrays.asList(USER_FRIENDS, PUBLIC_PROFILE, DOB));

        /**
         * Register a callback to respond to the user
         */
        facebook_login_btn.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {

                Log.e(TAG, "onSuccess: Facebook login Success");
                Log.e("View", "onSuccess: Facebook login Success------->" + new Gson().toJson(loginResult));
                setResult(RESULT_OK);

                /// Setting face book data
                setFacebookData(loginResult);

            }

            @Override
            public void onCancel() {
                Log.e(TAG, "onCancel: User Cancelled login");
                setResult(RESULT_CANCELED);

            }

            @Override
            public void onError(FacebookException e) {


                Log.e(TAG, "onError: " + e.toString());
                // Handle exception

                Snackbar snackbar = Snackbar.make(loginParent, R.string.something_went_wrong, Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(Color.RED);
                View view1 = snackbar.getView();
                TextView textview = (TextView) view1.findViewById(android.support.design.R.id.snackbar_text);
                textview.setTextColor(Color.WHITE);
                snackbar.show();

            }
        });

        /**
         * Email validation while starts typing
         * */

        emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                Log.e(TAG, "beforeTextChanged: " + s.toString());

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.e(TAG, "onTextChanged: " + s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {


                Log.e(TAG, "afterTextChanged: " + s.toString());

                if (passwordInput.getText().toString().length() == 0) {

                    isValidPassword = false;

                } else {

                    isValidPassword = true;
                }


                /**Email validation and calling API for checking the email type exists**/
                if (isValidEmail(s.toString())) {

                    Log.d(TAG, "afterTextChanged: Valid");

                    if (Utility.isConnected(getApplicationContext())) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        Call<CommonOutputPojo> call = apiService.checkValidEmail("get_user_check", s.toString());
                        call.enqueue(new Callback<CommonOutputPojo>() {

                            @Override
                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                                if (response.body() != null) {

                                    if (response.body().getResponseMessage().equals("nodata")) {

                                        isValidEmail = false;
                                        emailInput.setError("The email address that you've entered doesn't match any account. Sign up for an account.");
//                                    enableLoginButton();

                                    } else {

                                        isValidEmail = true;
                                        emailInput.setError(null);
//                                    enableLoginButton();

                                    }

                                } else {

                                    Snackbar snackbar = Snackbar.make(loginParent, R.string.something_went_wrong, Snackbar.LENGTH_LONG);
                                    snackbar.setActionTextColor(Color.RED);
                                    View view1 = snackbar.getView();
                                    TextView textview = (TextView) view1.findViewById(android.support.design.R.id.snackbar_text);
                                    textview.setTextColor(Color.WHITE);
                                    snackbar.show();
                                }


                            }

                            @Override
                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                                Snackbar snackbar = Snackbar.make(loginParent, R.string.something_went_wrong, Snackbar.LENGTH_LONG);
                                snackbar.setActionTextColor(Color.RED);
                                View view1 = snackbar.getView();
                                TextView textview = (TextView) view1.findViewById(android.support.design.R.id.snackbar_text);
                                textview.setTextColor(Color.WHITE);
                                snackbar.show();

                            }
                        });

                    } else {

                    }


                } else {

                    isValidEmail = false;
//                    enableLoginButton();

                }


                /// Login button enable / disable

                if (!emailInput.getText().toString().isEmpty() && !passwordInput.getText().toString().isEmpty()) {
                    button_login.setEnabled(true);
                } else {
                    button_login.setEnabled(false);

                }

            }
        });

        /**
         * Password validation (should not be empty)
         * */
        passwordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {


                /**
                 *  Email Validation
                 */


                String str = s.toString();

                if (str.length() > 0 && str.contains(" ")) {

                    Toast.makeText(Login.this, "Space is not allowed", Toast.LENGTH_SHORT).show();
//                    passwordInput.setError("Space is not allowed");

                    passwordInput.setText(passwordInput.getText().toString().replaceAll(" ", ""));
                    passwordInput.setSelection(passwordInput.getText().length());
                }


                if (isValidEmail(emailInput.getText().toString())) {

                    isValidEmail = true;
//                    enableLoginButton();

                } else {

                    isValidEmail = false;
//                    enableLoginButton();

                }

                /**
                 *  Password should not be empty */

                if (s.toString().length() != 0) {

                    isValidPassword = true;
//                    enableLoginButton();

                } else {

                    isValidPassword = false;
//                    enableLoginButton();

                }
/// Login button enable / disable

                if (!emailInput.getText().toString().isEmpty() && !passwordInput.getText().toString().isEmpty()) {
                    button_login.setEnabled(true);
                } else {
                    button_login.setEnabled(false);

                }
            }
        });


    }// OnCreate Ends Here

    @Override
    protected void onStart() {
        super.onStart();

    }


    // On Resume google client is connected again

    protected void onResume() {
        super.onResume();
        if (googleApiClient != null) {
            googleApiClient.connect();
            Log.e(TAG, "OnResume, Connected back!");
        }

    }

    @Override
    public void onDestroy() {
// TODO Auto-generated method stub

        try {
            if (broadcastReceiver != null)
                /*Unregistering the broadcast receiver after the application gets destroyed*/

                unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }
        super.onDestroy();

    }

    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {
            // Login button click

            case R.id.button_login:

                Log.i(TAG, "onClick: login button");

                // Hide Keyboard
                Utility.hideKeyboard(view);

                if (validate()) {


                    /*Checking internet connectivty*/
                    if (Utility.isConnected(this)) {


                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Login.this);
                        LayoutInflater inflater1 = LayoutInflater.from(Login.this);
                        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.universal_loader, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);
                        dialogBuilder.setTitle("");

                        dialog = dialogBuilder.create();

                        dialog.show();

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        Call<CreateUserPojoOutput> call = apiService.userLogin("get_validate_user", emailInput.getText().toString(),
                                passwordInput.getText().toString(), latitude, longitude, "000000000000000", fcmkey);


                        call.enqueue(new Callback<CreateUserPojoOutput>() {
                            @Override
                            public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                                Log.e("Logintext", "" + response);


                                dialog.dismiss();



                                String status = response.body().getResponseMessage();


                                if (response.body().getResponseCode() == 1) {


                                    dialog.dismiss();

                                    if (response.body().getResponseMessage().equals("success")) {

                                        //Success

                                        Pref_storage.setDetail(Login.this, "IsLoggedIn", "1");

                                        String userId = response.body().getData().get(0).getUserId();
                                        String oauthProvider = response.body().getData().get(0).getOauthProvider();
                                        String oauthUid = response.body().getData().get(0).getOauthUid();
                                        String firstName = response.body().getData().get(0).getFirstName();
                                        String lastName = response.body().getData().get(0).getLastName();
                                        String totalFollowers = response.body().getData().get(0).getTotalFollowers();
                                        String totalFollowering = response.body().getData().get(0).getTotalFollowings();
                                        String email = response.body().getData().get(0).getEmail();
                                        String imei = response.body().getData().get(0).getImei();
                                        String gcmkey = response.body().getData().get(0).getGcmkey();
                                        String ipAddress = response.body().getData().get(0).getIpAddress();
                                        String latitude = response.body().getData().get(0).getLatitude();
                                        String longitude = response.body().getData().get(0).getLongitude();
                                        String password = response.body().getData().get(0).getPassword();
                                        String gender = response.body().getData().get(0).getGender();
                                        //  String dob = response.body().getData().get(0).getDob();
                                        String locale = response.body().getData().get(0).getLocale();
                                        String picture = response.body().getData().get(0).getPicture();
                                        String link = response.body().getData().get(0).getLink();
                                        String loginType = response.body().getData().get(0).getLoginType();
                                        String createdTime = response.body().getData().get(0).getCreatedTime();
                                        String modifiedTime = response.body().getData().get(0).getModifiedTime();
                                        String deleted = response.body().getData().get(0).getDeleted();

                                        Pref_storage.setDetail(Login.this, "userId", userId);
                                        Pref_storage.setDetail(Login.this, "oauthProvider", oauthProvider);
                                        Pref_storage.setDetail(Login.this, "oauthUid", oauthUid);
                                        Pref_storage.setDetail(Login.this, "firstName", firstName);
                                        Pref_storage.setDetail(Login.this, "lastName", lastName);
                                        Pref_storage.setDetail(Login.this, "totalFollowers", totalFollowers);
                                        Pref_storage.setDetail(Login.this, "totalFollowering", totalFollowering);
                                        Pref_storage.setDetail(Login.this, "email", email);
                                        Pref_storage.setDetail(Login.this, "imei", imei);
                                        Pref_storage.setDetail(Login.this, "fcmkey", gcmkey);
                                        Pref_storage.setDetail(Login.this, "ipAddress", ipAddress);
                                        Pref_storage.setDetail(Login.this, "latitude", latitude);
                                        Pref_storage.setDetail(Login.this, "longitude", longitude);
                                        Pref_storage.setDetail(Login.this, "password", password);
                                        Pref_storage.setDetail(Login.this, "gender", gender);
                                        Pref_storage.setDetail(Login.this, "dob", "");
                                        Pref_storage.setDetail(Login.this, "locale", locale);
                                        Pref_storage.setDetail(Login.this, "picture_user_login", picture);
                                        Pref_storage.setDetail(Login.this, "link", link);
                                        Pref_storage.setDetail(Login.this, "loginType", loginType);
                                        Pref_storage.setDetail(Login.this, "createdTime", createdTime);
                                        Pref_storage.setDetail(Login.this, "modifiedTime", modifiedTime);
                                        Pref_storage.setDetail(Login.this, "deleted", deleted);

                                        dialog.dismiss();
                                        startActivity(new Intent(Login.this, Home.class));
                                        finish();

                                    } else if (status.equals("nodata")) {

                                        dialog.dismiss();
                                        Snackbar snackbar = Snackbar.make(loginParent, "Login Failed. Invalid Credentials.", Snackbar.LENGTH_LONG);
                                        snackbar.setActionTextColor(Color.RED);
                                        View view1 = snackbar.getView();
                                        TextView textview = (TextView) view1.findViewById(android.support.design.R.id.snackbar_text);
                                        textview.setTextColor(Color.WHITE);
                                        snackbar.show();

                                    }
                                } else if (response.body().getResponseCode() == 2) {
                                    //Utility.universalLoaderDismiss();

                                    dialog.dismiss();

                                    Toast.makeText(Login.this, "EMAIL NOT VERIFIED", Toast.LENGTH_SHORT).show();
                                    Toast.makeText(Login.this, "EMAIL NOT VERIFIED", Toast.LENGTH_SHORT).show();


                                    // report issue
                                    popup = LayoutInflater.from(Login.this).inflate(R.layout.otpverified_layout, null);
                                    popupWindow = new PopupWindow(popup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
                                    final AlertDialog.Builder alertbox = new AlertDialog.Builder(Login.this);

                                     final EditText enter_otp = (EditText) popup.findViewById(R.id.enter_otp);

                                    Button cancel = (Button) popup.findViewById(R.id.cancel);
                                    Button submit = (Button) popup.findViewById(R.id.submit);

                                    final ProgressBar progress_timeline = (ProgressBar) popup.findViewById(R.id.progress_timeline);


                                    cancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            popupWindow.dismiss();
                                            alt.dismiss();

                                        }
                                    });

                                    submit.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            //call create_report_email api
                                            if (Utility.isConnected(Login.this)) {

                                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


                                                if (enter_otp.getText().toString().length() != 0) {
                                                    try {
                                                        progress_timeline.setVisibility(VISIBLE);


                                                        Call<UpdateNewOtp_Response> call = apiService.update_new_register_email_verifycode("update_new_register_email_verifycode",
                                                                emailInput.getText().toString(),
                                                                enter_otp.getText().toString()
                                                        );


                                                        call.enqueue(new Callback<UpdateNewOtp_Response>() {
                                                            @Override
                                                            public void onResponse(Call<UpdateNewOtp_Response> call, Response<UpdateNewOtp_Response> response) {

                                                                if (response.body() != null) {

                                                                    String responseStatus = response.body().getResponseMessage();

                                                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                                    progress_timeline.setVisibility(GONE);

                                                                    if (response.body().getResponseCode() == 1 || response.body().getResponseMessage().equalsIgnoreCase("success")) {

                                                                        //Success

                                                                        Intent intent = new Intent(Login.this, Home.class);
                                                                        startActivity(intent);
                                                                        popupWindow.dismiss();
                                                                        alt.dismiss();

                                                                    } else {
                                                                        Toast.makeText(Login.this, "" + response.body().getResponseMessage(), Toast.LENGTH_SHORT).show();

                                                                    }

                                                                } else {

                                                                    Toast.makeText(Login.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<UpdateNewOtp_Response> call, Throwable t) {
                                                                //Error
                                                                Log.e("FailureError", "FailureError" + t.getMessage());
                                                                progress_timeline.setVisibility(GONE);
                                                            }
                                                        });

                                                    } catch (Exception e) {

                                                        e.printStackTrace();

                                                    }
                                                } else {
                                                    enter_otp.setError("OTP should not be empty");
                                                }

                                            } else {
                                                Toast.makeText(Login.this, Login.this.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                    });


                                    alertbox.setView(popup);
                                    alt = alertbox.show();
                                    popupWindow.dismiss();
                                } else if (response.body().getResponseCode() == 0) {


                                    dialog.dismiss();

                                    Toast.makeText(Login.this, "Invalid Email Address", Toast.LENGTH_SHORT).show();

                                }


                            }

                            @Override
                            public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {

                                dialog.dismiss();

                                Log.e("FailureError", "" + t.getMessage());
                                Snackbar snackbar = Snackbar.make(loginParent, "Login Failed. Invalid Credentials.", Snackbar.LENGTH_LONG);
                                snackbar.setActionTextColor(Color.RED);
                                View view1 = snackbar.getView();
                                TextView textview = (TextView) view1.findViewById(android.support.design.R.id.snackbar_text);
                                textview.setTextColor(Color.WHITE);
                                snackbar.show();
                            }
                        });


                    } else {

                        Snackbar snackbar = Snackbar.make(loginParent, "Login Failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        View view1 = snackbar.getView();
                        TextView textview = (TextView) view1.findViewById(android.support.design.R.id.snackbar_text);
                        textview.setTextColor(Color.WHITE);
                        snackbar.show();
                    }

                }

                break;
            // Google sign in button click

            case R.id.btn_google_signin:

                if (Utility.isConnected(this)) {

                    Intent intentGoogleapi = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                    startActivityForResult(intentGoogleapi, Sign_In);


                } else {

                    Snackbar snackbar = Snackbar.make(loginParent, "Login Failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                    snackbar.setActionTextColor(Color.RED);
                    View view1 = snackbar.getView();
                    TextView textview = (TextView) view1.findViewById(android.support.design.R.id.snackbar_text);
                    textview.setTextColor(Color.WHITE);
                    snackbar.show();

                }

                break;


            case R.id.signuuptext:

                startActivity(new Intent(Login.this, EmailSignUp.class));
                break;

            case R.id.txt_frgt_pwd:

                startActivity(new Intent(Login.this, forgotPasswordActivity.class));
                emailInput.setError(null);
                passwordInput.setError(null);

                break;
        }

    }


    /**
     * Enables the login button if the credentials are valid
     */
    private void enableLoginButton() {

        if (isValidEmail && isValidPassword) {

            button_login.setEnabled(true);

        } else {

            button_login.setEnabled(false);

        }

    }


    /**
     * Retrieving the Google plus login data of the users
     */

    private void setGooglePlusData(GoogleSignInResult googleSignInResult) {

        Log.e("view", "handleSignInResult" + googleSignInResult.isSuccess());
        Log.e("view", "handleSignInResult" + googleSignInResult.getStatus());

        if (googleSignInResult.isSuccess()) {

            GoogleSignInAccount googleSignInAccount = googleSignInResult.getSignInAccount();

            Locale locale = getResources().getConfiguration().locale;
            String password = "";
            String gender = "";
            String bio = "";

            String link2 = "";
            String dob = "";
            String ipaddress = "";
            String LOGIN_TYPE = "2";
            String REGISTER_TYPE = "2";
            String MOBILE_OS = "1";
            String oauthProvider = "GooglePlus";
            String oauth_id = googleSignInAccount.getId();
            String firstName = googleSignInAccount.getGivenName();
            String lastName = googleSignInAccount.getFamilyName();

            if (firstName == null || lastName == null) {
                firstName = googleSignInAccount.getDisplayName();
                lastName = googleSignInAccount.getDisplayName();
            }
            Uri personPhotourl = googleSignInAccount.getPhotoUrl();
            String email = googleSignInAccount.getEmail();
            Account link = googleSignInAccount.getAccount();

            fcmkey = Pref_storage.getDetail(Login.this, "GcmKey");

            sendUserInformation("create_user", imei, fcmkey, oauthProvider,
                    oauth_id, firstName, lastName, email, password, gender, dob,
                    locale.toString(), Objects.requireNonNull(link).toString(), ipaddress, bio, String.valueOf(personPhotourl)
                    , latitudeResult, longitudeResult, REGISTER_TYPE, MOBILE_OS, LOGIN_TYPE);

            Log.d(TAG, "imei" + imei);
            Log.d(TAG, "fcmkey" + fcmkey);
            Log.d(TAG, "fcmkey" + fcmkey);
            Log.d(TAG, "oauthProvider" + oauthProvider);
            Log.d(TAG, "oauth_id" + oauth_id);
            Log.d(TAG, "firstName" + firstName);
            Log.d(TAG, "lastName" + lastName);
            Log.d(TAG, "email" + email);
            Log.d(TAG, "password" + password);
            Log.d(TAG, "gender" + gender);
            Log.d(TAG, "dob" + dob);
            Log.d(TAG, "dob" + locale.toString());
            Log.d(TAG, "dob" + link.toString());
            Log.d(TAG, "ipaddress" + ipaddress);
            Log.d(TAG, "bio" + bio);
            Log.d(TAG, "String.valueOf(personPhotourl)" + String.valueOf(personPhotourl));
            Log.d(TAG, "latitudeResult" + latitudeResult);
            Log.d(TAG, "longitudeResult" + longitudeResult);
            Log.d(TAG, "REGISTER_TYPE" + REGISTER_TYPE);
            Log.d(TAG, "MOBILE_OS" + MOBILE_OS);
            Log.d(TAG, "LOGIN_TYPE" + LOGIN_TYPE);

        } else {

            Log.d(TAG, "handleSignInResult" + googleSignInResult.getStatus());

        }

    }


    /**
     * Retrieving the Facebook login data of the users
     */
    private void setFacebookData(final LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        // Application code
                        try {

                            Log.i(TAG, "onCompleted: " + response.toString());

                            Profile profile = Profile.getCurrentProfile();

                            Locale locale = getResources().getConfiguration().locale;

                            String profilePicUrl = "";
                            String password = "";
                            String gender = "null";
                            String bio = "";

                            String link = "";
                            String ipaddress = "";
                            String LOGIN_TYPE = "1";
                            String REGISTER_TYPE = "2";
                            String MOBILE_OS = "1";
                            String oauthProvider = "facebook";

                            String oauth_id = response.getJSONObject().getString("id");
                            Log.i("View", "gender:viji" + oauth_id);

                            String firstName = response.getJSONObject().getString("first_name");
                            String lastName = response.getJSONObject().getString("last_name");

                            JSONObject data = response.getJSONObject();

                            if (data.has("picture")) {
                                profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");

                            }

                            fcmkey = Pref_storage.getDetail(Login.this, "GcmKey");


                       /* if (response.getJSONObject().getString("email") != null) {
                            fbemail = response.getJSONObject().getString("email");
                        } else {

                        }*/

                            // gender = response.getJSONObject().getString("gender");
                            //   gender = object.getString("gender");
                            //   gender = "null";
                            //  String dob = response.getJSONObject().getString("birthday").replace("/", "-");
                            //  Log.i(TAG, "dob: " + dob);
                            Log.i(TAG, "gender:viji" + gender);
                            sendUserInformation("create_user", imei, fcmkey, oauthProvider,
                                    oauth_id, firstName, lastName, "0", password, "0", "0",
                                    locale.toString(), link, ipaddress, bio, profilePicUrl,
                                    latitudeResult, longitudeResult, REGISTER_TYPE, MOBILE_OS, LOGIN_TYPE
                            );

                        } catch (JSONException e) {
                            Log.e("View", "gender:viji-------> " + e.getMessage());
                            Toast.makeText(Login.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                });

        Bundle parameters = new Bundle();
        //parameters.putString("fields", "id,email,first_name,last_name,gender,birthday,picture");
        parameters.putString("fields", "id,first_name,last_name,gender,birthday,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }


    /**
     * Sending the User data to the server
     */
    private void sendUserInformation(String methodname, String imei, String gcmkey, String oauthprovider,
                                     String oauth_uid, final String first_name, String last_name, String email, String password, String gender,
                                     String dob, String locale, String link, String ip, String bio, String picture,
                                     String latitude, String longitude, String register_type, String mobile_os, String logintype) {


        final AlertDialog dialog = new SpotsDialog.Builder().setContext(Login.this).setMessage("").build();
        dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {


            Call<CreateUserPojoOutput> call = apiService.createUser(methodname, imei, gcmkey, oauthprovider, oauth_uid,
                    first_name, last_name, email, password, gender, dob, locale, link, ip, bio, picture,
                    latitude, longitude, register_type, mobile_os, logintype, "", "", "");


            call.enqueue(new Callback<CreateUserPojoOutput>() {

                @Override
                public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {


                    if (response.body() != null) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            Pref_storage.setDetail(Login.this, "IsLoggedIn", "1");


                            String userId = response.body().getData().get(0).getUserId();
                            String oauthProvider = response.body().getData().get(0).getOauthProvider();
                            String oauthUid = response.body().getData().get(0).getOauthUid();

                            String firstName = response.body().getData().get(0).getFirstName();
                            String lastName = response.body().getData().get(0).getLastName();

                            // Toast.makeText(Login.this, "" + firstName, Toast.LENGTH_SHORT).show();
                            //  Toast.makeText(Login.this, "" + lastName, Toast.LENGTH_SHORT).show();

                            String totalFollowers = response.body().getData().get(0).getTotalFollowers();
                            String totalFollowering = response.body().getData().get(0).getTotalFollowings();
                            String email = response.body().getData().get(0).getEmail();
                            String imei = response.body().getData().get(0).getImei();
                            String gcmkey = response.body().getData().get(0).getGcmkey();
                            String ipAddress = response.body().getData().get(0).getIpAddress();
                            String latitude = response.body().getData().get(0).getLatitude();
                            String longitude = response.body().getData().get(0).getLongitude();
                            String password = response.body().getData().get(0).getPassword();
                            String gender = response.body().getData().get(0).getGender();
                            //  String dob = response.body().getData().get(0).get();
                            String locale = response.body().getData().get(0).getLocale();
                            String picture = response.body().getData().get(0).getPicture();
                            String link = response.body().getData().get(0).getLink();
                            String loginType = response.body().getData().get(0).getLoginType();
                            String createdTime = response.body().getData().get(0).getCreatedTime();
                            String modifiedTime = response.body().getData().get(0).getModifiedTime();
                            String deleted = response.body().getData().get(0).getDeleted();

                            Pref_storage.setDetail(Login.this, "userId", userId);
                            Pref_storage.setDetail(Login.this, "oauthProvider", oauthProvider);
                            Pref_storage.setDetail(Login.this, "oauthUid", oauthUid);
                            Pref_storage.setDetail(Login.this, "firstName", firstName);
                            Pref_storage.setDetail(Login.this, "lastName", lastName);
                            Pref_storage.setDetail(Login.this, "totalFollowers", totalFollowers);
                            Pref_storage.setDetail(Login.this, "totalFollowering", totalFollowering);
                            Pref_storage.setDetail(Login.this, "email", email);
                            Pref_storage.setDetail(Login.this, "imei", imei);
                            Pref_storage.setDetail(Login.this, "fcmkey", gcmkey);
                            Pref_storage.setDetail(Login.this, "ipAddress", ipAddress);
                            Pref_storage.setDetail(Login.this, "latitude", latitude);
                            Pref_storage.setDetail(Login.this, "longitude", longitude);
                            Pref_storage.setDetail(Login.this, "password", password);
                            Pref_storage.setDetail(Login.this, "gender", gender);
                            //  Pref_storage.setDetail(Login.this, "dob", dob);
                            Pref_storage.setDetail(Login.this, "locale", locale);
                            Pref_storage.setDetail(Login.this, "picture_user_login", picture);
                            Pref_storage.setDetail(Login.this, "link", link);
                            Pref_storage.setDetail(Login.this, "loginType", loginType);
                            Pref_storage.setDetail(Login.this, "createdTime", createdTime);
                            Pref_storage.setDetail(Login.this, "modifiedTime", modifiedTime);
                            Pref_storage.setDetail(Login.this, "deleted", deleted);


                            Pref_storage.setDetail(Login.this, "firstName", firstName);
                            Pref_storage.setDetail(Login.this, "lastName", lastName);

                            dialog.dismiss();

                            Toast.makeText(Login.this, "Logged In Successfully", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(Login.this, Home.class));
                            Pref_storage.setDetail(Login.this, "GcmKey", null);
                            LoginManager.getInstance().logOut();

                            //  finish();

                          /*  Intent intent=new Intent(Login.this,Home.class);
                            startActivity(intent);*/


                        } else if (responseStatus.equals("nodata")) {

                            dialog.dismiss();

                            Toast.makeText(Login.this, "User already exits. Login with email", Toast.LENGTH_SHORT).show();
                        }

                    }

                }

                @Override
                public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                    //Error
                    dialog.dismiss();
                    Toast.makeText(Login.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();
                    Log.e("responseStatus", "responseStatus->" + t.getMessage());
                }
            });

        } catch (Exception e) {
            dialog.dismiss();
            e.printStackTrace();
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        Log.d("onActivityResult()", "resultCode" + Integer.toString(resultCode));
        Log.d("onActivityResult()", "requestCode" + Integer.toString(requestCode));

        if (requestCode == Sign_In) {

            GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            setGooglePlusData(googleSignInResult);


        }

        if (requestCode == REQUEST_CHECK_SETTINGS) {

            Log.d("onActivityResult()", "LOCATION_OK" + Integer.toString(requestCode));


            switch (resultCode) {

                case RESULT_OK:

                    latitude = gpsTracker.getLatitude();
                    longitude = gpsTracker.getLongitude();

                    Log.d("onActivityResult()", "latitude" + latitude);
                    Log.d("onActivityResult()", "latitude" + longitude);

                    break;


                case RESULT_CANCELED:


                    break;

            }


        }


        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.e("onRequestPermissions", "requestCode->" + requestCode);

        int permissionSize = permissions.length;


        switch (requestCode) {

            case ACCESS_FINE_LOCATION_INTENT_ID:

                for (int i = 0; i < permissionSize; i++) {

                    if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "ACCESS_FINE_LOCATION permission granted");
//                        showSettingDialog();

                        // Permission to get IMEI
//                       showPhoneStatePermission();

                    } else if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "ACCESS_FINE_LOCATION permission denied");

                    }


                }


                break;

            case REQUEST_PERMISSION_PHONE_STATE:


                for (int i = 0; i < permissionSize; i++) {

                    if (permissions[i].equals(Manifest.permission.READ_PHONE_STATE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_PHONE_STATE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_PHONE_STATE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_PHONE_STATE permission denied");

                        showPhoneStatePermission();
                    }
                }
                break;
        }
    }


    /**
     * email and password field validation
     */
    private boolean validate() {

        boolean valid = true;

        String email = emailInput.getText().toString().trim();
        String password = passwordInput.getText().toString().trim();


        if (email.length() == 0) {
            emailInput.setError("Invalid email");
            valid = false;
        } else {
            emailInput.setError(null);
        }

        if (password.length() == 0) {
            passwordInput.setError("Invalid password");
            valid = false;
        } else {
            passwordInput.setError(null);
        }

        return valid;
    }





    //Check permission to IMEI for the device
    private void showPhoneStatePermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_PHONE_STATE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            requestPermission(MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            getDeviceIMEI();
        }
    }

    // Method to get IMEI for the device
    @SuppressLint("HardwareIds")
    private void getDeviceIMEI() {

        imei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);


        //imei = IMEIUtil.getDeviceId(Login.this);

        Log.e(TAG, "getDeviceIMEI: " + imei);

        Pref_storage.setDetail(getApplicationContext(), "IMEI", imei);

    }

    // Permission request


    private void requestPermission(int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_PHONE_STATE}, permissionRequestCode);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


}
