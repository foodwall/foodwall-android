package com.kaspontech.foodwall.loginPackage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Image;
import com.kaspontech.foodwall.utills.CustomGIFview;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.File;

import io.fabric.sdk.android.Fabric;

public class SplashScreen extends AppCompatActivity {


    // Splash screen timer
    // private static int SPLASH_TIME_OUT = 1000;
    private static int SPLASH_TIME_OUT = 3000;

    // Variables

    private static String isLoggedIn, gcmkey, imei;
    private static double latitude, longitude;

    //Splash screen image view

    //  ImageView imgSplash;


    // Broadcast
    BroadcastReceiver broadcastReceiver;


    // Data Store

    Pref_storage pref_storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*Crashlytics initialization*/

       Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_splash_screen);


        //  imgSplash = findViewById(R.id.imgSplash);


        //  Glide.with(this).load(R.raw.splashgif).into(imgSplash);

        /*Cache deletion*/
        // deleteCache(SplashScreen.this);

        /*File fdelete = new File("/data/local/tmp/com.kaspontech.foodwall-build-id.txt");

        if (fdelete.exists()) {

            if (fdelete.delete()) {

                System.out.println("file Deleted :" + "/data/local/tmp/com.kaspontech.foodwall-build-id.txt");

            } else {

                System.out.println("file not Deleted :" + "/data/local/tmp/com.kaspontech.foodwall-build-id.txt");

            }
        }*/


        pref_storage = new Pref_storage();

        /**
         * FCM key for push notification
         * */
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(SplashScreen.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String fcmkey = instanceIdResult.getToken();
                Pref_storage.setDetail(SplashScreen.this, "GcmKey", fcmkey);
                Log.e("new","newToken---------> "+fcmkey);
            }
        });
        /* *//*Firebase Initialiazation*//*

        FirebaseApp.initializeApp(SplashScreen.this);

        *//*Broadcast initialization*//*
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                gcmkey = Pref_storage.getDetail(SplashScreen.this, "LoginFCM-key");
            }
        };

        *//*Getting the GCM key from the shared prefernce*//*
        gcmkey = Pref_storage.getDetail(SplashScreen.this, "LoginFCM-key");*/



        /*Getting the app is logged in or not whether to redirect to home page ot login page */
        isLoggedIn = Pref_storage.getDetail(this, "IsLoggedIn");


        Log.e("SplashScreen", "isLoggedIn->" + isLoggedIn);

        /*Delay to show splash screen and redirecting to home page*/

        if (isLoggedIn == null) {

            new Handler().postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity

                    if (isLoggedIn == null) {

                        /*Redirecting to Login page*/
                        Intent i = new Intent(SplashScreen.this, Login.class);
                        startActivity(i);
                        finish();

                    } else {


                    }
                    // close this activity
                    finish();
                }
            }, SPLASH_TIME_OUT);

        } else {

            new Handler().postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity

                    /*Redirecting to Home page*/
                    Intent i = new Intent(SplashScreen.this, Home.class);
                    startActivity(i);
                    finish();


                    // close this activity
                    finish();
                }
            }, SPLASH_TIME_OUT);


        }


    }



    /*Cache deletion*/

    public static void deleteCache(Context context) {

        try {

            File dir = context.getCacheDir();
            deleteDir(dir);

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    /*Deleting directory*/
    public static boolean deleteDir(File dir) {

        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    public void onDestroy() {
// TODO Auto-generated method stub
        try {
            if (broadcastReceiver != null)

                /*Unregistering the broadcast receiver after the application gets destroyed*/
                unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }


}
