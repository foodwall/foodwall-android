package com.kaspontech.foodwall.loginPackage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.LoginPojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.REST_API.ProgressRequestBody;

//
import com.kaspontech.foodwall.modelclasses.LoginPojo.UpdateNewOtp_Response;
import com.kaspontech.foodwall.modelclasses.LoginPojo.VerifyOTP_response;
import com.kaspontech.foodwall.modelclasses.ReportResponse;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import org.joda.time.DateTime;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.kaspontech.foodwall.utills.Utility.dismissDottedDialog;
import static com.kaspontech.foodwall.utills.Utility.hideKeyboard;
import static com.kaspontech.foodwall.utills.Utility.isValidEmail;
import static com.kaspontech.foodwall.utills.Utility.isValidPassword;

public class EmailSignUp extends AppCompatActivity implements
        View.OnClickListener,
        ProgressRequestBody.UploadCallbacks {


    // Action Bar
    Toolbar actionBar;
    ImageButton back;


    // Context
    Context mContext = EmailSignUp.this;


    //Widgets
    Button button_generateotp;
    Button btn_dob_done;
    RadioGroup genderGroup;
    RadioGroup radioGrpOthers;
    RelativeLayout emailParent;
    RadioButton male_radio_btn;
    RadioButton female_radio_btn;
    RadioButton transgender_radio_btn;
    RadioButton rathernotsay_radio_btn;
    ImageView userPhoto;
    ImageView img_sts_update;

    EditText email_input;
    EditText first_name_input;
    EditText last_name_input;
    EditText dateofbirth_input;
    EditText password_input;
    EditText
            confirm_password_input;
    EditText bio_descripition;

    DatePicker dob_picker;

    //Private account switch

    Switch switchPvtAcc;

    TextView txtPrivateTxt;

    int account_status;


    // Login type
    private final int MOBILE_OS = 1;
    private final int LOGIN_TYPE = 3;
    private final int REGISTER_TYPE = 2;

    // Variables
    boolean pwd_valid = true;
    private int img_status = 0;
    private double latitude, longitude;
    private static final int PICK_FROM_GALLERY = 1;
    int PICK_IMAGE_MULTIPLE = 2;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_GALLERY_PERMISSION = 2;
    private static final int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private boolean isValidEmail, isValidFirstName, isValidLastName, isValidDate, isValidPwd, isValidCnPwd;
    private String imei, fcmkey, oauthprovider = "self", oauth_uid = "", gender, country, city, countryCode, result;


    // Data Storage
    Uri imageUri;
    String profileImage;


    // GPS
    GPSTracker gpsTracker;

    private static GoogleApiClient mGoogleApiClient;


    // Permissions
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int REQUEST_PERMISSION_SETTING = 5;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";

    private static final String TAG = "LoggerEmailSignUp";

    PopupWindow popupWindow;
    View popup;
    private AlertDialog alt;

    private AlertDialog alertDialog;

    private EditText enter_otp;

    private Button cancel, submit;


    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_sign_up);

        /**
         * Initiate google api client for turn on location dialog
         * */
        initGoogleAPIClient();


        /**
         * Ensure that users gave permission to get their locations
         * */

        checkLocationPermissions();
        gpsTracker = new GPSTracker(EmailSignUp.this);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();


        actionBar = findViewById(R.id.action_bar_signup);
        back = actionBar.findViewById(R.id.back);
        email_input = findViewById(R.id.editText_useremailID);
        first_name_input = findViewById(R.id.editText_userfirstname);
        last_name_input = findViewById(R.id.editText_userlastname);
        dateofbirth_input = findViewById(R.id.date_of_birth);
        password_input = findViewById(R.id.editText_password);
        confirm_password_input = findViewById(R.id.editText_cnfpassword);
        male_radio_btn = findViewById(R.id.radioM);
        female_radio_btn = findViewById(R.id.radioF);
        transgender_radio_btn = findViewById(R.id.radioTransgender);
        rathernotsay_radio_btn = findViewById(R.id.radioRathernotSay);
        genderGroup = findViewById(R.id.radioGrp);
        genderGroup.clearCheck();
//        radioGrpOthers = findViewById(R.id.radioGrpOthers);
        // signup_btn = findViewById(R.id.button_signup);
        button_generateotp = findViewById(R.id.button_generateotp);
        userPhoto = findViewById(R.id.img_circular);
        img_sts_update = findViewById(R.id.img_sts_update);
        emailParent = findViewById(R.id.emailParent);
        bio_descripition = findViewById(R.id.bio_descripition);

        // Private switch
        switchPvtAcc = findViewById(R.id.switch_pvt_acc);
        txtPrivateTxt = findViewById(R.id.txt_private_txt);


        //Dob Picker

        dob_picker = findViewById(R.id.dob_picker);
        btn_dob_done = findViewById(R.id.btn_dob_done);


        button_generateotp.setOnClickListener(this);
        userPhoto.setOnClickListener(this);
        back.setOnClickListener(this);
        dateofbirth_input.setOnClickListener(this);
        // signup_btn.setOnClickListener(this);
        first_name_input.setOnClickListener(this);
        btn_dob_done.setOnClickListener(this);

        // Setting hint

        String email = "<font color='#d6d6d6'>Enter your E-mail ID </font>" + "<font color='#FF0000'>*</font>";
        String first = "<font color='#d6d6d6'>First name </font>" + "<font color='#FF0000'>*</font>";
        String last = "<font color='#d6d6d6'>Last name </font>" + "<font color='#FF0000'>*</font>";
        String dob = "<font color='#d6d6d6'>Date of Birth </font>" + "<font color='#FF0000'>*</font>";
//        String bio = "<font color='#d6d6d6'>Bio description </font>" + "<font color='#FF0000'>*</font>";
        String bio = "<font color='#d6d6d6'>Bio description </font>";
        String pwd = "<font color='#d6d6d6'>Password </font>" + "<font color='#FF0000'>*</font>";
        String cnfpwd = "<font color='#d6d6d6'>Confirm password </font>" + "<font color='#FF0000'>*</font>";

        email_input.setHint(Html.fromHtml(email));
        first_name_input.setHint(Html.fromHtml(first));
        last_name_input.setHint(Html.fromHtml(last));
        dateofbirth_input.setHint(Html.fromHtml(dob));
        bio_descripition.setHint(Html.fromHtml(bio));
        password_input.setHint(Html.fromHtml(pwd));
        confirm_password_input.setHint(Html.fromHtml(cnfpwd));


        //Private account switch listener

        switchPvtAcc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    txtPrivateTxt.setVisibility(View.VISIBLE);
                    account_status = 1;
                } else {
                    account_status = 0;
                    txtPrivateTxt.setVisibility(View.GONE);
                }
            }
        });


        imei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        //imei = IMEIUtil.getDeviceId(Login.this);
        Log.e(TAG, "getDeviceIMEI: " + imei);

        // Email input validation

        first_name_input.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                if (Utility.isValidEmail(email_input.getText().toString())) {
                    email_input.setError(null);
                } else {
                    email_input.setError("Enter valid email");
                }

                return false;
            }
        });


        /**
         * FCM key for push notification
         * */

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(EmailSignUp.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmkey = instanceIdResult.getToken();
                Log.e("newToken", fcmkey);


            }
        });


        male_radio_btn.setChecked(true);
        gender = "male";


        // Gender checked change listener
        male_radio_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                male_radio_btn.setChecked(true);
                gender = "male";
                female_radio_btn.setChecked(false);
                transgender_radio_btn.setChecked(false);
                rathernotsay_radio_btn.setChecked(false);
             }
        });

        female_radio_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                female_radio_btn.setChecked(true);
                gender = "female";
                male_radio_btn.setChecked(false);
                transgender_radio_btn.setChecked(false);
                rathernotsay_radio_btn.setChecked(false);
             }
        });


        transgender_radio_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transgender_radio_btn.setChecked(true);
                gender = "transgender";
                male_radio_btn.setChecked(false);
                female_radio_btn.setChecked(false);
                rathernotsay_radio_btn.setChecked(false);
             }
        });
        rathernotsay_radio_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rathernotsay_radio_btn.setChecked(true);
                gender = "rathernotsay";
                male_radio_btn.setChecked(false);
                female_radio_btn.setChecked(false);
                transgender_radio_btn.setChecked(false);
             }
        });





        /* *//**
         *
         * To get the device imei number - No need to implement in our project
         *
         *
         * *//*

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        imei = telephonyManager.getDeviceId();*/


        /**
         * Here there is a validation of the existing user email address
         *
         * */
        email_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                Log.d(TAG, "beforeTextChanged: " + s.toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d(TAG, "onTextChanged: " + s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

                Log.d(TAG, "afterTextChanged: " + s.toString());

                if (isValidEmail(s.toString())) {

                    Log.d(TAG, "afterTextChanged: Valid");

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


                    Call<CommonOutputPojo> call = apiService.checkValidEmail("get_user_check", s.toString());

                    call.enqueue(new Callback<CommonOutputPojo>() {

                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                            if (response.body() != null) {

                                if (response.body().getResponseMessage().equals("success")) {

                                    isValidEmail = false;
                                    email_input.setError("Someone's already using that email. If that’s you, login with your Email and password.");

                                    button_generateotp.setEnabled(false);
                                } else {

                                    button_generateotp.setEnabled(true);

                                    isValidEmail = true;
                                    email_input.setError(null);
                                    enableSignUpButton();


                                }

                            } else {


                            }


                        }

                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());

                        }
                    });


                }

            }
        });


        /**
         *  Here there is a validation of firstname of the user's name not to be empty
         *
         * */

        first_name_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().length() > 0) {

                    isValidFirstName = true;
                    enableSignUpButton();

                } else {

                    isValidFirstName = false;
                }
            }
        });


        // Email input focus validation

        first_name_input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                if (hasFocus) {

                    if (!isValidEmail) {

                        if (email_input.getText().toString().trim().length() == 0) {

                            email_input.setError("Please enter the email id.");

                        } else if (!Utility.isValidEmail(email_input.getText().toString())) {

                            email_input.setError("Please enter the valid email id.");

                        } else if (Utility.isValidEmail(email_input.getText().toString())) {

                            email_input.setError("Someone's already using that email. If that’s you, login with your Email and password.");

                        }

                    }
                }
            }
        });

        /**
         *  Here there is a validation of lastname of the user's name not to be empty
         *
         * */

        last_name_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().length() > 0) {

                    isValidLastName = true;
                    enableSignUpButton();

                } else {

                    isValidLastName = false;
                }
            }
        });


        /**
         * Email input focus validation
         *
         **/

        last_name_input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                if (hasFocus) {


                    // Email validation

                    if (!isValidEmail) {

                        if (email_input.getText().toString().trim().length() == 0) {

                            email_input.setError("Please enter the email id.");

                        } else if (!Utility.isValidEmail(email_input.getText().toString())) {

                            email_input.setError("Please enter the valid email id.");

                        } else if (Utility.isValidEmail(email_input.getText().toString())) {

                            email_input.setError("Someone's already using that email. If that’s you, login with your Email and password.");

                        }

                    }

                    // First name validation

                    /*if(!isValidFirstName){

                        first_name_input.setError("Please enter the first name");
                    }*/


                }
            }
        });

        /**
         *  Here there is a validation of date of birth of the user not to be empty
         *
         * */

        dateofbirth_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().length() > 0) {

                    isValidDate = true;
                    enableSignUpButton();


                } else {

                    isValidDate = false;
                }
            }


        });

        /**
         *Date of birth focus listerner and input focus validation
         *
         **/

        dateofbirth_input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                if (hasFocus) {

                    if (!isValidEmail) {

                        if (email_input.getText().toString().trim().length() == 0) {

                            email_input.setError("Please enter the email id.");

                        } else if (!Utility.isValidEmail(email_input.getText().toString())) {

                            email_input.setError("Please enter the valid email id.");

                        } else if (Utility.isValidEmail(email_input.getText().toString())) {

                            email_input.setError("Someone's already using that email. If that’s you, login with your Email and password.");

                        }

                    }
                }
            }
        });

        /**
         * Here is a password validation when the users starts typing
         *
         * */
        password_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub


            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub


                if (isValidPassword(password_input.getText().toString())) {

                    password_input.setTextColor(getResources().getColor(R.color.Green));
                    password_input.setError(null);

                    if (password_input.getText().toString().length() < 9) {

                        password_input.setError("Your password should be as per the instructions");
                        password_input.setTextColor(getResources().getColor(R.color.red));

                    } else {
                        enableSignUpButton();
                        isValidPwd = true;

                    }


                } else {

                    password_input.setTextColor(getResources().getColor(R.color.colorAccent));

                    isValidPwd = false;

                    if (password_input.getText().toString().length() < 9) {

                        password_input.setError("Your password should be as per the instructions");
                        password_input.setTextColor(getResources().getColor(R.color.red));
                        isValidPwd = false;

                    }

                }


                if (!confirm_password_input.getText().toString().equals(password_input.getText().toString())) {

                    isValidCnPwd = false;
                    confirm_password_input.setTextColor(getResources().getColor(R.color.red));

                } else {

                    isValidCnPwd = true;
                    confirm_password_input.setTextColor(getResources().getColor(R.color.Green));

                }


            }
        });


        /**
         * Password input focus validation
         *
         **/

        password_input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                if (hasFocus) {

                    if (!isValidEmail) {

                        if (email_input.getText().toString().trim().length() == 0) {

                            email_input.setError("Please enter the email id.");

                        } else if (!Utility.isValidEmail(email_input.getText().toString())) {

                            email_input.setError("Please enter the valid email id.");

                        } else if (Utility.isValidEmail(email_input.getText().toString())) {

                            email_input.setError("Someone's already using that email. If that’s you, login with your Email and password.");

                        }

                    }
                }
            }
        });

        /**
         * Here is a confirm password validation
         *
         * */
        confirm_password_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub


            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                if (password_input.getText().toString().length() > confirm_password_input.getText().toString().length()) {

                    confirm_password_input.setTextColor(getResources().getColor(R.color.colorAccent));
                    isValidCnPwd = false;
                    confirm_password_input.setError("Your password does not matched");


                }


                if (password_input.getText().toString().length() <= confirm_password_input.getText().toString().length()) {

                    if (!confirm_password_input.getText().toString().equals(password_input.getText().toString())) {

                        isValidCnPwd = false;
                        confirm_password_input.requestFocus();
                        confirm_password_input.setError("Your password does not matched");
                        confirm_password_input.setTextColor(getResources().getColor(R.color.red));
                        enableSignUpButton();

                    } else {

                        if (confirm_password_input.getText().length() != 0) {

                            isValidCnPwd = true;

                            enableSignUpButton();

                        }


                        confirm_password_input.setError(null);
                        confirm_password_input.setTextColor(getResources().getColor(R.color.Green));

                    }

                } else {

                    enableSignUpButton();

                }


            }
        });


        /**
         * Confirm password input focus validation
         *
         **/

        confirm_password_input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                if (hasFocus) {

                    if (!isValidEmail) {

                        if (email_input.getText().toString().trim().length() == 0) {

                            email_input.setError("Please enter the email id.");

                        } else if (!Utility.isValidEmail(email_input.getText().toString())) {

                            email_input.setError("Please enter the valid email id.");

                        } else if (Utility.isValidEmail(email_input.getText().toString())) {

                            email_input.setError("Someone's already using that email. If that’s you, login with your Email and password.");

                        }

                    }
                }
            }
        });

    }


    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub

        super.onDestroy();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {


            /**
             * Profile image on click
             *
             **/

            case R.id.img_circular:

                checkProfilePicStatus();

                List<String> listItems = new ArrayList<String>();

                listItems.add("Take Photo");
                listItems.add("Choose from library");
                listItems.add("Cancel");

                if (img_status == 1) {

                    listItems.remove(2);
                    listItems.add("Remove photo");
                    listItems.add("Cancel");

                }


                final CharSequence[] items = listItems.toArray(new CharSequence[listItems.size()]);


                /**
                 * Alert dialog options
                 *
                 **/

                AlertDialog.Builder builder = new AlertDialog.Builder(EmailSignUp.this);
                builder.setTitle("Set profile photo");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {


                        if (items[item].equals("Take Photo")) {


                            try {

                                if (ActivityCompat.checkSelfPermission(EmailSignUp.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                        && ActivityCompat.checkSelfPermission(EmailSignUp.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                                    ActivityCompat.requestPermissions(EmailSignUp.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);

                                } else {

                                    Log.e(TAG, "onClick: else part");

                                    if (ActivityCompat.checkSelfPermission(EmailSignUp.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                            && ActivityCompat.checkSelfPermission(EmailSignUp.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                                        Log.e(TAG, "onClick: camera");

                                        if (img_status == 1) {

                                            final AlertDialog.Builder builder = new AlertDialog.Builder(EmailSignUp.this);
                                            builder.setMessage("Are you sure want to edit the photo");
                                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    userPhoto.setImageDrawable(null);
                                                    img_sts_update.setImageDrawable(getResources().getDrawable(R.drawable.ic_new_add));
                                                    /**
                                                     * Camera intent redirect
                                                     *
                                                     **/
                                                    cameraIntent();
                                                    dialog.dismiss();


                                                }
                                            });

                                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    dialog.dismiss();

                                                }
                                            });

                                            builder.show();

                                        } else if (img_status == 0) {

                                            /**
                                             * Camera intent redirect
                                             *
                                             **/


                                            cameraIntent();

                                        }


                                    } else {

                                        Log.e(TAG, "onClick: cameraStorageGrantRequest");

                                        cameraStorageGrantRequest();

                                    }


                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        else if (items[item].equals("Choose from library")) {

                            try {
                                if (ActivityCompat.checkSelfPermission(EmailSignUp.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(EmailSignUp.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);

                                } else {
                                    /**
                                     * Gallery intent redirect
                                     *
                                     **/
                                    galleryIntent();

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else if (items[item].equals("Cancel")) {

                            dialog.dismiss();

                        } else if (items[item].equals("Remove photo")) {

                            userPhoto.setImageDrawable(null);

                           /* Picasso.get()
                                    .load(R.drawable.ic_new_add)
                                    .into(img_sts_update);*/

                            img_sts_update.setImageDrawable(getResources().getDrawable(R.drawable.ic_new_add));

                        }
                    }
                });
                builder.show();


                break;


            /**
             * Back press on click
             *
             **/
            case R.id.back:
                finish();
                break;

            /**
             * DOB Done button click listener
             *
             **/
            case R.id.btn_dob_done:
                dob_picker.setVisibility(View.GONE);
                btn_dob_done.setVisibility(View.GONE);
                break;

            /**
             * Date of birht click listener
             *
             **/
            case R.id.date_of_birth:
                /**
                 * Hide Keyboard
                 *
                 **/
                hideKeyboard(v);

                dob_picker.setVisibility(View.VISIBLE);
                btn_dob_done.setVisibility(View.VISIBLE);


                final Calendar c = Calendar.getInstance();
                c.add(Calendar.YEAR, -3);
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);


                Log.e(TAG, "Calender Year: " + Calendar.YEAR);

                dob_picker.init(mYear, mMonth, mDay, new DatePicker.OnDateChangedListener() {

                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {

                        String frmdate = dayOfMonth + "-" + (month + 1) + "-" + year;


                        dateofbirth_input.setText(frmdate);

                        dateofbirth_input.setError(null);


                    }
                });

                // Joda Time @https://www.joda.org
                DateTime dateTime = new DateTime().minusYears(3);
                dob_picker.setMaxDate(dateTime.getMillis());
                break;


            case R.id.editText_userfirstname:


                break;

            case R.id.button_generateotp:

                if (validate()) {
                    create_email_verify_otp();
                }
                break;

        }

    }

    private void create_email_verify_otp() {


        if (Utility.isConnected(EmailSignUp.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            final ACProgressFlower dialog = new ACProgressFlower.Builder(this)
                    .direction(ACProgressConstant.PIE_AUTO_UPDATE)
                    .themeColor(Color.WHITE)
                    .text("Loading...")
                    .fadeColor(Color.YELLOW).build();
            dialog.show();

            try {

                Call<VerifyOTP_response> call = apiService.generateOTP("create_email_verify_otp", email_input.getText().toString());
                call.enqueue(new Callback<VerifyOTP_response>() {
                    @Override
                    public void onResponse(Call<VerifyOTP_response> call, Response<VerifyOTP_response> response) {

                        if (response.body() != null) {

                            dialog.hide();
                            String responseStatus = response.body().getResponseMessage();
                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                            // prog.setVisibility(GONE);
                            if (response.body().getResponseMessage().equalsIgnoreCase("success")) {

                                // report issue
                                popup_createOTPGenerate();

                            } else {

                                Toast.makeText(EmailSignUp.this, "" + response.body().getResponseMessage(), Toast.LENGTH_SHORT).show();

                            }

                        } else {

                            Toast.makeText(EmailSignUp.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                        }
                    }


                    @Override
                    public void onFailure(Call<VerifyOTP_response> call, Throwable t) {
                        //Error
                        dialog.hide();

                        Log.e("FailureError", "FailureError" + t.getMessage());
                        //progress_timeline.setVisibility(GONE);
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }


        } else {
            Toast.makeText(EmailSignUp.this, EmailSignUp.this.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }

    }


    private void popup_createOTPGenerate() {

        popup = LayoutInflater.from(EmailSignUp.this).inflate(R.layout.otpverified_layout, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(EmailSignUp.this);
        builder.setView(popup);
        builder.setCancelable(false);
        //popupWindow = new PopupWindow(popup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
        //popupWindow.setOutsideTouchable(false);

        enter_otp = popup.findViewById(R.id.enter_otp);
        cancel = popup.findViewById(R.id.cancel);
        submit = popup.findViewById(R.id.submit);

        final ProgressBar progress_timeline = popup.findViewById(R.id.progress_timeline);

        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        //popupWindow.showAtLocation(popup, Gravity.CENTER, 0, 0);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //popupWindow.dismiss();
                alertDialog.dismiss();

                // signup_btn.setVisibility(VISIBLE);
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (enter_otp.getText().toString().isEmpty()) {

                    enter_otp.setError("OTP should not be empty");

                } else {
                    //call create_report_email api

                    if (Utility.isConnected(EmailSignUp.this)) {

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            final ACProgressFlower dialog = new ACProgressFlower.Builder(EmailSignUp.this)
                                    .direction(ACProgressConstant.PIE_AUTO_UPDATE)
                                    .themeColor(Color.WHITE)
                                    .text("Loading...")
                                    .fadeColor(Color.YELLOW).build();
                            dialog.show();
                            //  progress_timeline.setVisibility(VISIBLE);


                            Call<UpdateNewOtp_Response> call = apiService.update_new_register_email_verifycode("update_new_register_email_verifycode",
                                    email_input.getText().toString(),
                                    enter_otp.getText().toString());
                            call.enqueue(new Callback<UpdateNewOtp_Response>() {
                                @Override
                                public void onResponse(Call<UpdateNewOtp_Response> call, Response<UpdateNewOtp_Response> response) {

                                    if (response.body() != null) {

                                        if (response.body().getResponseCode() == 1) {

                                            String responseStatus = response.body().getResponseMessage();

                                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                                            dialog.hide();

                                            alertDialog.dismiss();

                                            if (validate()) {

                                                country = "";
                                                countryCode = "";
                                                city = "";

                                                String first_name = first_name_input.getText().toString().trim();
                                                String last_name = last_name_input.getText().toString().trim();
                                                String email = email_input.getText().toString().trim();
                                                String dob = dateofbirth_input.getText().toString().trim();
                                                String password = confirm_password_input.getText().toString().trim();

                                                Locale locale = getResources().getConfiguration().locale;

                                                sendUserInformation("create_user", imei, fcmkey, city, oauthprovider, oauth_uid,
                                                        first_name, last_name, email, password, gender, dob, locale.toString(), "", "", "", country, countryCode, latitude, longitude, REGISTER_TYPE, MOBILE_OS, LOGIN_TYPE, account_status);

                                                Log.d(TAG, "imei" + imei);
                                                Log.d(TAG, "fcmkey" + fcmkey);
                                                Log.d(TAG, "fcmkey" + fcmkey);
                                                Log.d(TAG, "oauthProvider" + oauthprovider);
                                                Log.d(TAG, "oauth_id" + oauth_uid);
                                                Log.d(TAG, "firstName" + first_name);
                                                Log.d(TAG, "lastName" + last_name);
                                                Log.d(TAG, "email" + email);
                                                Log.d(TAG, "password" + password);
                                                Log.d(TAG, "gender" + gender);
                                                Log.d(TAG, "dob" + dob);
                                                Log.d(TAG, "dob" + locale.toString());
                                                Log.d(TAG, "latitudeResult" + latitude);
                                                Log.d(TAG, "longitudeResult" + longitude);
                                                Log.d(TAG, "REGISTER_TYPE" + REGISTER_TYPE);
                                                Log.d(TAG, "MOBILE_OS" + MOBILE_OS);
                                                Log.d(TAG, "LOGIN_TYPE" + LOGIN_TYPE);

                                            } else {

                                                Toast.makeText(mContext, "Sign-Up Failed. Please check your internet connection", Toast.LENGTH_SHORT).show();
                                            }

                                        } else if (response.body().getResponseCode() == 0) {

                                            dialog.hide();

                                            Toast.makeText(mContext, response.body().getResponseMessage(), Toast.LENGTH_SHORT).show();
                                            enter_otp.setText("");
                                            enter_otp.clearComposingText();
                                        }

                                    } else {

                                        Toast.makeText(EmailSignUp.this, "Internal Server error", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<UpdateNewOtp_Response> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                    //  progress_timeline.setVisibility(GONE);
                                    // button_generateotp.setVisibility(VISIBLE);
                                    dialog.hide();
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                        Toast.makeText(EmailSignUp.this, EmailSignUp.this.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }

    /**
     * Hitting API for Sign UP
     **/

    private void sendUserInformation(String methodname, String imei, String gcmkey, String city, String oauthprovider,
                                     String oauth_uid, String first_name, String last_name, final String email, String password, String Resultgender,
                                     String dob, String locale, String link, String ip, String picture, String country, String countrycode,
                                     double latitude, double longitude, int register_type, int mobile_os, int logintype, int account_status) {

        String userProfilePhoto = "";
        Log.e("UserValues", "userProfilePhoto->" + gcmkey);




        /**
         * Loader
         *
         **/
        final ACProgressFlower dialog = new ACProgressFlower.Builder(this)
                .direction(ACProgressConstant.PIE_AUTO_UPDATE)
                .themeColor(Color.WHITE)
                .text("Signing up..")
                .fadeColor(Color.YELLOW).build();
        dialog.show();


        if (img_status == 1) {

            userProfilePhoto = Pref_storage.getDetail(EmailSignUp.this, "UserImage");

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Log.e(TAG, "sendUserInformation: gender" + gender);

            try {

                File sourceFile = new File(userProfilePhoto);

                File compressedImageFile = new Compressor(this).setQuality(75).compressToFile(sourceFile);
                ProgressRequestBody fileBody = new ProgressRequestBody(compressedImageFile, this);
                MultipartBody.Part image = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), fileBody);

                RequestBody requestBodyImei = RequestBody.create(MediaType.parse("text/plain"), imei);
                RequestBody fcmkeyrequest = RequestBody.create(MediaType.parse("text/plain"), gcmkey);
                RequestBody requestBodyoauthprovider = RequestBody.create(MediaType.parse("text/plain"), oauthprovider);
                RequestBody requestBodyoauth_uid = RequestBody.create(MediaType.parse("text/plain"), oauth_uid);
                RequestBody requestBodyfirst_name = RequestBody.create(MediaType.parse("text/plain"), first_name);
                RequestBody requestBodylast_name = RequestBody.create(MediaType.parse("text/plain"), last_name);
                RequestBody requestBodyemail = RequestBody.create(MediaType.parse("text/plain"), email);
                RequestBody requestBodypassword = RequestBody.create(MediaType.parse("text/plain"), password);
                RequestBody requestBodygender = RequestBody.create(MediaType.parse("text/plain"), Resultgender);
                RequestBody requestBodydob = RequestBody.create(MediaType.parse("text/plain"), dob);
                RequestBody requestBodylocale = RequestBody.create(MediaType.parse("text/plain"), locale);
//                RequestBody requestBodyAccountStatus = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(account_status));


                Call<CreateUserPojoOutput> call = apiService.signUpWithProfilePhoto(methodname,
                        requestBodyImei,
                        fcmkeyrequest,
                        requestBodyoauthprovider,
                        requestBodyoauth_uid,
                        requestBodyfirst_name,
                        requestBodylast_name,
                        requestBodyemail,
                        requestBodypassword,
                        requestBodygender,
                        requestBodydob,
                        requestBodylocale,
                        link,
                        ip,
                        bio_descripition.getText().toString().trim(),
                        picture,
                        latitude,
                        longitude,
                        register_type,
                        mobile_os,
                        logintype,
                        img_status,
                        account_status,
                        image);


                call.enqueue(new Callback<CreateUserPojoOutput>() {

                    @Override
                    public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                        dialog.hide();
                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + response);

                        if (response.body().getResponseCode() == 1) {

                            if (response.body().getResponseMessage().equalsIgnoreCase("success")) {

                                //Success
                                Pref_storage.setDetail(EmailSignUp.this, "IsLoggedIn", "1");

                                String userId = response.body().getData().get(0).getUserId();
                                String oauthProvider = response.body().getData().get(0).getOauthProvider();
                                String oauthUid = response.body().getData().get(0).getOauthUid();
                                String firstName = response.body().getData().get(0).getFirstName();
                                String lastName = response.body().getData().get(0).getLastName();
                                String totalFollowers = response.body().getData().get(0).getTotalFollowers();
                                String totalFollowering = response.body().getData().get(0).getTotalFollowings();
                                String email = response.body().getData().get(0).getEmail();
                                String imei = response.body().getData().get(0).getImei();
                                String gcmkey = response.body().getData().get(0).getGcmkey();
                                String ipAddress = response.body().getData().get(0).getIpAddress();
                                String latitude = response.body().getData().get(0).getLatitude();
                                String longitude = response.body().getData().get(0).getLongitude();
                                String password = response.body().getData().get(0).getPassword();
                                String gender = response.body().getData().get(0).getGender();
                                //String dob = response.body().getData().get(0).getDob();
                                String locale = response.body().getData().get(0).getLocale();
                                String picture = response.body().getData().get(0).getPicture();
                                String link = response.body().getData().get(0).getLink();
                                String loginType = response.body().getData().get(0).getLoginType();
                                String createdTime = response.body().getData().get(0).getCreatedTime();
                                String modifiedTime = response.body().getData().get(0).getModifiedTime();
                                String deleted = response.body().getData().get(0).getDeleted();

                                /**
                                 * Saving personal details in shared preference
                                 *
                                 **/

                                Pref_storage.setDetail(EmailSignUp.this, "userId", userId);
                                Pref_storage.setDetail(EmailSignUp.this, "oauthProvider", oauthProvider);
                                Pref_storage.setDetail(EmailSignUp.this, "oauthUid", oauthUid);
                                Pref_storage.setDetail(EmailSignUp.this, "firstName", firstName);
                                Pref_storage.setDetail(EmailSignUp.this, "lastName", lastName);
                                Pref_storage.setDetail(EmailSignUp.this, "totalFollowers", totalFollowers);
                                Pref_storage.setDetail(EmailSignUp.this, "totalFollowering", totalFollowering);
                                Pref_storage.setDetail(EmailSignUp.this, "email", email);
                                Pref_storage.setDetail(EmailSignUp.this, "imei", imei);
                                Pref_storage.setDetail(EmailSignUp.this, "gcmkey", gcmkey);
                                Pref_storage.setDetail(EmailSignUp.this, "ipAddress", ipAddress);
                                Pref_storage.setDetail(EmailSignUp.this, "latitude", latitude);
                                Pref_storage.setDetail(EmailSignUp.this, "longitude", longitude);
                                Pref_storage.setDetail(EmailSignUp.this, "password", password);
                                Pref_storage.setDetail(EmailSignUp.this, "gender", gender);
                                Pref_storage.setDetail(EmailSignUp.this, "dob", "");
                                Pref_storage.setDetail(EmailSignUp.this, "locale", locale);
                                Pref_storage.setDetail(EmailSignUp.this, "picture_user_login", picture);
                                Pref_storage.setDetail(EmailSignUp.this, "link", link);
                                Pref_storage.setDetail(EmailSignUp.this, "loginType", loginType);
                                Pref_storage.setDetail(EmailSignUp.this, "createdTime", createdTime);
                                Pref_storage.setDetail(EmailSignUp.this, "modifiedTime", modifiedTime);
                                Pref_storage.setDetail(EmailSignUp.this, "deleted", deleted);

//                            dialog1.dismiss();
                                dialog.dismiss();

                                /**
                                 *Redirecting to home after successfull signup
                                 *
                                 **/
                                Toast.makeText(EmailSignUp.this, "Account Created Successfully", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), Home.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();


                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                        //Error
                        Log.e("responseStatus", "responseStatus->" + t.getMessage());
//                        dialog1.dismiss();
                        dialog.hide();
                        Toast.makeText(EmailSignUp.this, "Something went wrong please try again", Toast.LENGTH_SHORT).show();

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();

            }

        } else {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                RequestBody requestBodyImei = RequestBody.create(MediaType.parse("text/plain"), imei);
                RequestBody requestBodyoauthprovider = RequestBody.create(MediaType.parse("text/plain"), oauthprovider);
                RequestBody requestBodyoauth_uid = RequestBody.create(MediaType.parse("text/plain"), oauth_uid);
                RequestBody requestBodyfirst_name = RequestBody.create(MediaType.parse("text/plain"), first_name);
                RequestBody requestBodylast_name = RequestBody.create(MediaType.parse("text/plain"), last_name);
                RequestBody requestBodyemail = RequestBody.create(MediaType.parse("text/plain"), email);
                RequestBody requestBodypassword = RequestBody.create(MediaType.parse("text/plain"), password);
                RequestBody requestBodygender = RequestBody.create(MediaType.parse("text/plain"), gender);
                RequestBody requestBodydob = RequestBody.create(MediaType.parse("text/plain"), dob);
                RequestBody requestBodylocale = RequestBody.create(MediaType.parse("text/plain"), locale);


                Call<CreateUserPojoOutput> call = apiService.signUpWithOutProfilePhoto(methodname, requestBodyImei, gcmkey, requestBodyoauthprovider,
                        requestBodyoauth_uid, requestBodyfirst_name, requestBodylast_name, requestBodyemail, requestBodypassword, requestBodygender, requestBodydob, requestBodylocale, link,
                        ip, bio_descripition.getText().toString(), picture, latitude, longitude, register_type, mobile_os, logintype, img_status, account_status);


                call.enqueue(new Callback<CreateUserPojoOutput>() {
                    @Override
                    public void onResponse(Call<CreateUserPojoOutput> call, Response<CreateUserPojoOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.contains("success")) {

                            //Success
                            Pref_storage.setDetail(EmailSignUp.this, "IsLoggedIn", "1");

                            String userId = response.body().getData().get(0).getUserId();
                            String oauthProvider = response.body().getData().get(0).getOauthProvider();
                            String oauthUid = response.body().getData().get(0).getOauthUid();
                            String firstName = response.body().getData().get(0).getFirstName();
                            String lastName = response.body().getData().get(0).getLastName();
                            String totalFollowers = response.body().getData().get(0).getTotalFollowers();
                            String totalFollowering = response.body().getData().get(0).getTotalFollowings();
                            String email = response.body().getData().get(0).getEmail();
                            String imei = response.body().getData().get(0).getImei();
                            String gcmkey = response.body().getData().get(0).getGcmkey();
                            String ipAddress = response.body().getData().get(0).getIpAddress();
                            String latitude = response.body().getData().get(0).getLatitude();
                            String longitude = response.body().getData().get(0).getLongitude();
                            String password = response.body().getData().get(0).getPassword();
                            String gender = response.body().getData().get(0).getGender();
                            // String dob = response.body().getData().get(0).getDob();
                            String locale = response.body().getData().get(0).getLocale();
                            String picture = response.body().getData().get(0).getPicture();
                            String link = response.body().getData().get(0).getLink();
                            String loginType = response.body().getData().get(0).getLoginType();
                            String createdTime = response.body().getData().get(0).getCreatedTime();
                            String modifiedTime = response.body().getData().get(0).getModifiedTime();
                            String deleted = response.body().getData().get(0).getDeleted();
                            /**
                             * Saving personal details in shared preference
                             *
                             **/
                            Pref_storage.setDetail(EmailSignUp.this, "userId", userId);
                            Pref_storage.setDetail(EmailSignUp.this, "oauthProvider", oauthProvider);
                            Pref_storage.setDetail(EmailSignUp.this, "oauthUid", oauthUid);
                            Pref_storage.setDetail(EmailSignUp.this, "firstName", firstName);
                            Pref_storage.setDetail(EmailSignUp.this, "lastName", lastName);
                            Pref_storage.setDetail(EmailSignUp.this, "totalFollowers", totalFollowers);
                            Pref_storage.setDetail(EmailSignUp.this, "totalFollowering", totalFollowering);
                            Pref_storage.setDetail(EmailSignUp.this, "email", email);
                            Pref_storage.setDetail(EmailSignUp.this, "imei", imei);
                            Pref_storage.setDetail(EmailSignUp.this, "gcmkey", gcmkey);
                            Pref_storage.setDetail(EmailSignUp.this, "ipAddress", ipAddress);
                            Pref_storage.setDetail(EmailSignUp.this, "latitude", latitude);
                            Pref_storage.setDetail(EmailSignUp.this, "longitude", longitude);
                            Pref_storage.setDetail(EmailSignUp.this, "password", password);
                            Pref_storage.setDetail(EmailSignUp.this, "gender", gender);
                            Pref_storage.setDetail(EmailSignUp.this, "dob", "");
                            Pref_storage.setDetail(EmailSignUp.this, "locale", locale);
                            Pref_storage.setDetail(EmailSignUp.this, "picture_user_login", picture);
                            Pref_storage.setDetail(EmailSignUp.this, "link", link);
                            Pref_storage.setDetail(EmailSignUp.this, "loginType", loginType);
                            Pref_storage.setDetail(EmailSignUp.this, "createdTime", createdTime);
                            Pref_storage.setDetail(EmailSignUp.this, "modifiedTime", modifiedTime);
                            Pref_storage.setDetail(EmailSignUp.this, "deleted", deleted);
//                            dialog1.dismiss();

                            dialog.dismiss();
                            /**
                             *Redirecting to home after successfull signup
                             *
                             **/
                            Toast.makeText(EmailSignUp.this, "Account Created Successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), Home.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();

                        }
                    }

                    @Override
                    public void onFailure(Call<CreateUserPojoOutput> call, Throwable t) {
                        //Error
                        Log.e("responseStatus", "responseStatus->" + t.getMessage());
//                        dialog1.dismiss();
                        dialog.dismiss();
                        Toast.makeText(EmailSignUp.this, "Something went wrong please try again", Toast.LENGTH_SHORT).show();

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


    /**
     * Image processing for profile picture uploading
     **/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                result = data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == REQUEST_CAMERA) {


                onCaptureImageResult(data);


            } else if (requestCode == PICK_IMAGE_MULTIPLE) {

                try {

                    if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && null != data && !(data.toString().equals("Intent { (has extras) }"))) {

                        if (data.getData() != null) {


                            Uri mImageUri = data.getData();
                            Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                            cursor.moveToFirst();
                            String document_id = cursor.getString(0);
                            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                            cursor.close();

                            cursor = getContentResolver().query(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                            cursor.moveToFirst();
                            profileImage = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                            Pref_storage.setDetail(getApplicationContext(), "UserImage", profileImage);

                            Utility.picassoImageLoader(profileImage,
                                    1, userPhoto, getApplicationContext());

//                            GlideApp.with(EmailSignUp.this)
//                                    .load(profileImage)
//                                    .centerCrop()
//                                    .into(userPhoto);


                            img_sts_update.setImageDrawable(getResources().getDrawable(R.drawable.ic_checked));

                            cursor.close();


                            Log.v("LOG_TAG", "Selected Images" + profileImage);

                        } else {

                            if (data.getClipData() != null) {
                                ClipData mClipData = data.getClipData();
                                ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                                for (int i = 0; i < mClipData.getItemCount(); i++) {

                                    ClipData.Item item = mClipData.getItemAt(i);
                                    Uri uri = item.getUri();
                                    mArrayUri.add(uri);

                                    // Get the cursor

                                    Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                                    cursor.moveToFirst();
                                    String document_id = cursor.getString(0);
                                    document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                                    cursor.close();

                                    cursor = getContentResolver().query(
                                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                            null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                                    cursor.moveToFirst();
                                    profileImage = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                                    Pref_storage.setDetail(getApplicationContext(), "UserImage", profileImage);

                                    Utility.picassoImageLoader(profileImage,
                                            1, userPhoto, getApplicationContext());

//                                    GlideApp.with(EmailSignUp.this)
//                                            .load(profileImage)
//                                            .centerCrop()
//                                            .into(userPhoto);

                                    img_sts_update.setImageDrawable(getResources().getDrawable(R.drawable.ic_checked));


                                    cursor.close();


                                    Log.v("LOG_TAG", "Selected Images" + profileImage);
                                }
                                Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                            }
                        }
                    } else {
                        /* Toast.makeText(this, "You haven't picked Image",Toast.LENGTH_LONG).show();*/
                    }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Permission request for the profile picture uploading
     **/
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        int permissionSize = permissions.length;

        Log.e(TAG, "onRequestPermissionsResult:" + "requestCode->" + requestCode);

        switch (requestCode) {

            case REQUEST_CAMERA:

                for (int i = 0; i < permissionSize; i++) {

                    if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission granted");

                    } else if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission denied");
                        cameraStorageGrantRequest();
                    }

                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");

                        if (ActivityCompat.checkSelfPermission(EmailSignUp.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraIntent();

                        }


                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");

                        if (ActivityCompat.checkSelfPermission(EmailSignUp.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraStorageGrantRequest();

                        }

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

            case PICK_FROM_GALLERY:

                for (int i = 0; i < permissionSize; i++) {


                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");
                        galleryIntent();

                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");
                        cameraStorageGrantRequest();

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

        }


    }

    /**
     * Camera intent method
     **/
    private void cameraIntent() {


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, REQUEST_CAMERA);

        } else {


            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);

        }

    }

    /**
     * Storage permission for signup
     **/

    private void cameraStorageGrantRequest() {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                EmailSignUp.this);

        builder.setTitle("Food Wall would like to access your camera and storage")
                .setMessage("You previously chose not to use Android's camera or storage with Food Wall. To set your profile pic, allow camera and storage permission in App Settings. Click Ok to allow.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // permission dialog
                        dialog.dismiss();
                        try {

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();


    }

    /**
     * Capturing thumbnail for profile picture
     **/
    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = null;
        try {
            thumbnail = getThumbnail(imageUri);
            saveBitmap(thumbnail);
        } catch (IOException e) {
            e.printStackTrace();
//            Toast.makeText(this, "CameraCaptureError", Toast.LENGTH_SHORT).show();
            Log.e("CameraCaptureError", "CameraCaptureError->" + e.getMessage());
        }


    }

    /**
     * Gallery intent for the profile picture
     **/
    private void galleryIntent() {

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_GALLERY_PERMISSION);
        } else {

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);

        }
    }


    /**
     * Saving image bitmap
     **/
    private void saveBitmap(Bitmap bm) {


        String folder_main = "FoodWall";

        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }

        File newFile = new File(f, System.currentTimeMillis() + ".jpg");
        Log.e("ImageFile", "ImageFile->" + newFile);

        try {

            FileOutputStream fileOutputStream = new FileOutputStream(newFile);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();

            profileImage = String.valueOf(newFile);
            Pref_storage.setDetail(getApplicationContext(), "UserImage", profileImage);

            Utility.picassoImageLoader(profileImage,
                    1, userPhoto, getApplicationContext());

//            GlideApp.with(EmailSignUp.this)
//                    .load(profileImage)
//                    .centerCrop()
//                    .into(userPhoto);

            img_sts_update.setImageDrawable(getResources().getDrawable(R.drawable.ic_checked));


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getting thumbnail for the profile image
     **/
    public Bitmap getThumbnail(Uri uri) throws IOException {
        InputStream input = EmailSignUp.this.getContentResolver().openInputStream(uri);
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 800) ? (originalSize / 800) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    /**
     * Getting Sample image ratio
     **/

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }


    /**
     * Writing the image URI path
     */
    public Uri writeToTempImageAndGetPathUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    /**
     * Getting Profile image URI
     */
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    private void enableSignUpButton() {

        boolean isImageUpload = false;

        if (profileImage != null) {
            isImageUpload = true;
        }

        if (isValidEmail && isValidFirstName && isValidLastName && isValidDate && isValidPwd && isValidCnPwd /*&& isImageUpload*/) {

            // signup_btn.setEnabled(true);

        } else {

            // signup_btn.setEnabled(false);

        }

    }

    /**
     * Checking profile picture status
     **/

    private void checkProfilePicStatus() {


        if (userPhoto.getDrawable() != null) {

            img_status = 1;

        } else {

            img_status = 0;

        }

    }


    /**
     * Validation for form fields
     **/

    private boolean validate() {

        boolean valid = true;

        String pass = password_input.getText().toString().trim();
        String cpass = confirm_password_input.getText().toString().trim();
        String email = email_input.getText().toString().trim();
        String fname = first_name_input.getText().toString().trim();
        String lname = last_name_input.getText().toString().trim();
        String dob = dateofbirth_input.getText().toString().trim();
//        String bio = bio_descripition.getText().toString();

        if (userPhoto.getDrawable() != null) {

            img_status = 1;
            valid = true;

        } else {

            Toast.makeText(mContext, "Profile image is mandatory!!", Toast.LENGTH_SHORT).show();
            valid = false;

            img_status = 0;

        }

        if (!isValidEmail(email)) {
            email_input.requestFocus();
            email_input.setError("Email address not valid");
            valid = false;
        } else {
            email_input.setError(null);
        }

        if (fname.length() == 0) {
            first_name_input.setError("Enter your first name");
            valid = false;
        } else {
            first_name_input.setError(null);
        }


        if (lname.length() == 0) {
            last_name_input.requestFocus();
            last_name_input.setError("Enter your last name");
            valid = false;
        } else {
            last_name_input.setError(null);
        }

        if (dob.length() == 0) {
            dateofbirth_input.requestFocus();
            dateofbirth_input.setError("Select your date of birth");
            valid = false;
        } else {
            dateofbirth_input.setError(null);
        }


        if (pwd_valid) {

            if (pass.length() == 0) {

                password_input.requestFocus();
                password_input.setError("Password field should not be empty");
                valid = false;

            } else {

                password_input.setError(null);

            }

            if (!pass.equals(cpass)) {

                confirm_password_input.requestFocus();
                confirm_password_input.setError("Your password does not matched");
                valid = false;

            } else {

                confirm_password_input.setError(null);

            }

        }

        Log.e(TAG, "onClick: -> Image Status" + img_status);

        return valid;
    }


    /* Initiate Google API Client  */
    private void initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(EmailSignUp.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    /**
     * Check Location Permission for Marshmallow Devices
     */
    private void checkLocationPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(EmailSignUp.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else
                showSettingDialog();
        } else
            showSettingDialog();

    }

    /**
     * Show Popup to access User Permission
     */
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(EmailSignUp.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(EmailSignUp.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(EmailSignUp.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    /**
     * Showing Popup dialog for high accuracy GPS
     */
    private void showSettingDialog() {


        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(EmailSignUp.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    /**
     * Showing Progress update percentage for the profile image upload
     */
    @Override
    public void onProgressUpdate(int percentage) {

        Log.e(TAG, "onProgressUpdate: EmailSignup profile photo" + percentage);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

















    /*--------------------------------------Unused methods--------------------------------------------------*/


    /*
        @SuppressWarnings("deprecation")
    */
    private void onSelectFromGalleryResult(Intent data) {

        Uri selectedImage = data.getData();


        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor c = getApplicationContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);

        c.moveToFirst();


        int columnIndex = c.getColumnIndex(filePathColumn[0]);
        String picturePath = c.getString(columnIndex);
//        Toast.makeText(getApplicationContext(), ""+picturePath, Toast.LENGTH_SHORT).show();
        c.close();


        Bitmap photo1 = (BitmapFactory.decodeFile(picturePath));
        userPhoto.setImageBitmap(photo1);


        img_sts_update.setImageDrawable(getResources().getDrawable(R.drawable.ic_checked));


        Pref_storage.setDetail(getApplicationContext(), "UserImage", picturePath);


    }

    public String saveBitmapToFile(Bitmap bm) {

        String folder_main = "FoodWall";
        String imagepath = "";

        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }

        File newFile = new File(f, System.currentTimeMillis() + ".png");
        Log.e("ImageFile", "ImageFile->" + newFile);

        try {

            FileOutputStream fileOutputStream = new FileOutputStream(newFile);
            bm.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();

            imagepath = String.valueOf(newFile);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return imagepath;

    }


    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
//            Toast.makeText(mContext, "vis"+result, Toast.LENGTH_SHORT).show();
            cursor.close();
        }
        return result;

    }


    public String getImageUrlWithAuthority1(Context context, Uri uri) {
        InputStream is = null;
        if (uri.getAuthority() != null) {
            try {
                is = context.getContentResolver().openInputStream(uri);

                Bitmap bmp = BitmapFactory.decodeStream(is);

                return writeToTempImageAndGetPathUri(context, bmp).toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }


    public String getImageUrlWithAuthority(Context context, Uri uri) {
        InputStream is = null;
        if (uri.getAuthority() != null) {
            try {
                is = context.getContentResolver().openInputStream(uri);
                Bitmap bmp = BitmapFactory.decodeStream(is);
                return writeToTempImageAndGetPathUri(context, bmp).toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }


    private void AddressFinder(double latitude, double longitude) {


        Geocoder geocoder;
        List<Address> addresses = new ArrayList<>();
        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        try {


            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            String locality = addresses.get(0).getLocality();
            String knownName = addresses.get(0).getFeatureName();
            String sublocality = addresses.get(0).getSubLocality();


        } catch (IOException e) {

            e.printStackTrace();

        }
    }

    /**
     * Broadcast receiver to check status of GPS
     */
    private BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //If Action is Location
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                //Check if GPS is turned ON or OFF
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Log.e("About GPS", "GPS is Enabled in your device");
                } else {
                    //If GPS turned OFF show Location Dialog
                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    // showSettingDialog();
                    Log.e("About GPS", "GPS is Disabled in your device");
                }

            }
        }
    };

    //Run on UI
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            showSettingDialog();
        }
    };

}