package com.kaspontech.foodwall.menuPackage;

import android.app.SearchManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.Follow_following.FindFriendsAdapter;
import com.kaspontech.foodwall.modelclasses.FindFriends.GetFindFriends;
import com.kaspontech.foodwall.modelclasses.FindFriends.GetFindFriendsOutput;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.MyDividerItemDecoration;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FindFriends extends AppCompatActivity implements View.OnClickListener,FindFriendsAdapter.FriendsAdapterListener{


    private static final String TAG = "FindFriends";

    private RecyclerView recyclerView;
    private SearchView searchView;
    private FindFriendsAdapter findFriendsAdapter;
    private List<GetFindFriends> followerlist = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // toolbar fancy stuff
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.discover_people);


        recyclerView = findViewById(R.id.recycler_view);

        findFriendsAdapter = new FindFriendsAdapter(this, followerlist,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 20));
        recyclerView.setAdapter(findFriendsAdapter);


        /*Calling get friend suggestion api call*/

        getFriendsAll();

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            finish();
        }

    }


    /*Calling get friend suggestion api call*/

    private void getFriendsAll(){

        if (Utility.isConnected(FindFriends.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                final String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<GetFindFriendsOutput> call = apiService.getFindFriends("get_friend_all", Integer.parseInt(createdby));
                call.enqueue(new Callback<GetFindFriendsOutput>() {
                    @Override
                    public void onResponse(Call<GetFindFriendsOutput> call, Response<GetFindFriendsOutput> response) {

                        if (response.body().getResponseMessage().equals("success")) {

                            followerlist.clear();


                            followerlist.addAll(response.body().getData());

                            int pos = 0;

                            /*for(int i = 0 ; i < followerlist.size();i++){

                                if(followerlist.get(i).getUserId().equalsIgnoreCase(createdby)){

                                    pos = i;
                                }
                            }

                            followerlist.remove(pos);*/

                            // refreshing recycler view
                            findFriendsAdapter.notifyDataSetChanged();

                            Log.e(TAG, "onResponse: "+findFriendsAdapter.toString() );


                        }else {


                            Log.e(TAG, "onResponse: " );

                        }

                    }

                    @Override
                    public void onFailure(Call<GetFindFriendsOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());


                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        /*Calling get friend suggestion api call*/

        getFriendsAll();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint("Type name..");

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                findFriendsAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                findFriendsAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        if (id == android.R.id.home) {

            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }



    @Override
    public void onFollowed() {
//        getFriendsAll();
    }
}
