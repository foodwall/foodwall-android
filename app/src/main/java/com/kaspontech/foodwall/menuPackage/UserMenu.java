package com.kaspontech.foodwall.menuPackage;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.bucketlistpackage.MyBucketlistActivity;
import com.kaspontech.foodwall.eventspackage.Events;
import com.kaspontech.foodwall.foodFeedsPackage.TimeLineFeeds;
import com.kaspontech.foodwall.loginPackage.Login;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Profile_output_pojo;
import com.kaspontech.foodwall.modelclasses.logout.Logout_Response;
import com.kaspontech.foodwall.notificationsPackage.AppNotification;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.profilePackage.editProfileActivity;
import com.kaspontech.foodwall.profilePackage.Password_change_Activity;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.Version_info_activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.questionspackage.QuestionAnswer;
import com.kaspontech.foodwall.reviewpackage.Reviews;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.io.File;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.kaspontech.foodwall.questionspackage.QuestionAnswer.question_scrollview;
import static com.kaspontech.foodwall.reviewpackage.allTab.AllFragment.scrollAllfragment;

public class UserMenu extends _SwipeActivityClass implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener {


    private static final String TAG = "UserMenu";


    CircleImageView user_image;
    RelativeLayout rl_profile;
    TextView txt_firstname, txt_viewprofile;
    LinearLayout viewProfile, viewEvents, view_bucket_list,
            ll_edt_profile, ll_update_password, ll_about_version, ll_find_friends,ll_logout;


    String firstName1, lastName1,username, password, email1, gender, dob, picture,pro_status, total_post, follwing, followers, fullname, logintype, bio_description;


    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions googleSignInOptions;

    android.support.v7.widget.Toolbar toolbar;
    ImageView back;

    public UserMenu() {
        // Required empty public constructor
    }

    /**
     * Bottom navigateion menu
     * @param savedInstanceState
     */
    // Widgets
    BottomNavigationView bottom_navigation_home;

    Menu menu;

    private AlertDialog dialog;

    // Fragments
    Fragment timeLineFeeds;
    Fragment reviews;
    Fragment events;
    Fragment questionAnswer;
    Fragment profile;
    Fragment notifications;
    Fragment userMenu;


    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_usermenu);



        FacebookSdk.sdkInitialize(getApplicationContext());
        getWindow().setEnterTransition(null);


        toolbar =  findViewById(R.id.actionbar_comment);
        back = (ImageView)toolbar.findViewById(R.id.back);

        viewProfile = (LinearLayout) findViewById(R.id.view_profile);
        viewEvents = (LinearLayout) findViewById(R.id.view_events_page);
        ll_edt_profile = (LinearLayout) findViewById(R.id.ll_edt_profile);
        ll_update_password = (LinearLayout) findViewById(R.id.ll_update_password);
        ll_about_version = (LinearLayout) findViewById(R.id.ll_about_version);
        ll_logout = (LinearLayout) findViewById(R.id.ll_logout);
        view_bucket_list = (LinearLayout) findViewById(R.id.view_bucket_list);
        ll_find_friends = (LinearLayout) findViewById(R.id.ll_find_friends);
        rl_profile = (RelativeLayout) findViewById(R.id.rl_profile);
        user_image = (CircleImageView) findViewById(R.id.user_image_menu);
        txt_firstname = (TextView) findViewById(R.id.txt_firstnamemenu);
        txt_viewprofile = (TextView) findViewById(R.id.txt_viewprofile);

        /**
         * Bottom navigationmenu initialization
         */
        bottom_navigation_home = (BottomNavigationView) findViewById(R.id.home_bottomNavigation);
        menu = bottom_navigation_home.getMenu();

        /*Fragment Initialization*/
        timeLineFeeds = new TimeLineFeeds();
        reviews = new Reviews();
        notifications = new AppNotification();
        questionAnswer = new QuestionAnswer();
        userMenu = new MyBucketlistActivity();


        /**
         * Googleapiclient for google signin logout
         * */
        initGoogleAPIClient();

        viewProfile.setOnClickListener(this);
        viewEvents.setOnClickListener(this);
        rl_profile.setOnClickListener(this);
        ll_edt_profile.setOnClickListener(this);
        ll_update_password.setOnClickListener(this);
        ll_about_version.setOnClickListener(this);
        ll_logout.setOnClickListener(this);
        ll_find_friends.setOnClickListener(this);
        view_bucket_list.setOnClickListener(this);
        back.setOnClickListener(this);


        /**
         * User profile photo
         * */

        String userProfilePic = Pref_storage.getDetail(getApplicationContext(),"picture_user_login");


        try {
            if (userProfilePic.equals("0")) {


                GlideApp.with(this)
                        .load(R.drawable.ic_add_photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .into(user_image);

            } else {

                GlideApp.with(this)
                        .load(userProfilePic)
                        .placeholder(R.drawable.ic_add_photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .into(user_image);
            }


        }catch (Exception e)
        {
            e.printStackTrace();
        }



            firstName1 = Pref_storage.getDetail(this, "firstName");
            lastName1 = Pref_storage.getDetail(this, "lastName");

            fullname = firstName1.concat(" ").concat(lastName1);
            txt_firstname.setText(fullname);


        get_profile();


        /**
         *BottomNavigationView
         */
        bottom_navigation_home = (BottomNavigationView) findViewById(R.id.home_bottomNavigation);
     //   menu = bottom_navigation_home.getMenu();


        bottom_navigation_home.setSelectedItemId(0);


         bottom_navigation_home.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
             @Override
             public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                 switch (menuItem.getItemId()) {
                     /*TimelineFeeds Module*/
                     case R.id.action_feeds:
                         Intent intent=new Intent(UserMenu.this,Home.class);
                         intent.putExtra("redirect","timeline");
                         startActivity(intent);
                         finish();
                         break;
                     /*Review module*/
                     case R.id.action_review:
                         Intent revintent=new Intent(UserMenu.this,Home.class);
                         revintent.putExtra("redirect","from_review");
                         startActivity(revintent);
                         finish();
                         break;


                     case R.id.action_questions:
                         Intent qaintent=new Intent(UserMenu.this,Home.class);
                         qaintent.putExtra("redirect","qa");
                         startActivity(qaintent);
                         finish();
                         break;
                     /*Notification module*/
                     case R.id.action_notifications:
                         Intent noteintent=new Intent(UserMenu.this,Home.class);
                         noteintent.putExtra("redirect","notification");
                         startActivity(noteintent);
                         finish();
                         break;

                     /*Question Answer Module*/
                     case R.id.action_menu:

                         Intent bucketintent=new Intent(UserMenu.this,Home.class);
                         bucketintent.putExtra("redirect","profile");
                         startActivity(bucketintent);
                         finish();
                         break;


                 }

                 return false;
             }
         });

     }

    @Override
    public void onSwipeRight() {

    }

    @Override
    protected void onSwipeLeft() {

        finish();
        overridePendingTransition(R.anim.swipe_righttoleft, R.anim.swipe_lefttoright);

    }


    @Override
    public void onStart() {


        initGoogleAPIClient();
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();



        super.onStart();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.d(TAG, "onConnectionFailed: "+connectionResult.getErrorMessage());
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();


        switch (id) {



            case R.id.view_profile:
                startActivity(new Intent(this, Profile.class));
                break;

            case R.id.rl_profile:
                startActivity(new Intent(this, Profile.class));
                break;
            case R.id.ll_edt_profile:

                if (isConnected(this)) {

                    Intent i = new Intent(this, editProfileActivity.class);
                    i.putExtra("firstname", firstName1);
                    i.putExtra("lastname", lastName1);
                    i.putExtra("password", password);
                    i.putExtra("email", email1);
                    i.putExtra("gender", gender);
                    i.putExtra("dob", dob);
                    i.putExtra("picture", picture);
                    i.putExtra("pro_status", pro_status);
                    i.putExtra("username", username);
                    i.putExtra("logintype", logintype);
                    i.putExtra("bio_description", bio_description);

                    startActivity(i);
                }else{

                    Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
                }



                break;

            case R.id.ll_update_password:

                startActivity(new Intent(this, Password_change_Activity.class));
                break;

            case R.id.ll_about_version:
                startActivity(new Intent(this, Version_info_activity.class));
                break;

            case R.id.ll_find_friends:

                startActivity(new Intent(this, FindFriends.class));

                break;

            case R.id.ll_logout:
                logout();
                break;

            case R.id.view_events_page:

                Intent intent = new Intent(this, Events.class);
                intent.putExtra("eventType", "0");
                startActivity(intent);

                break;

            case R.id.view_bucket_list:

                 intent = new Intent(this, Home.class);
                intent.putExtra("redirect", "bucketlist");
                break;

            case R.id.back:

                finish();
                Pref_storage.setDetail(UserMenu.this,"scroll","false");
                overridePendingTransition(R.anim.swipe_righttoleft, R.anim.swipe_lefttoright);
                break;


        }

    }


    /** Initiate Google API Client  */
    private void initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();
        mGoogleApiClient.connect();
    }



    private void logout() {

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure want to logout?")
                .setConfirmText("Yes")
                .setCancelText("No")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        int loginType = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "loginType"));

                        switch (loginType) {

                            case 1:

                                dialog = new SpotsDialog.Builder().setContext(UserMenu.this).setMessage("").build();
                                dialog.setCancelable(false);
                                dialog.show();

                                // facebook
                                LoginManager.getInstance().logOut();
                                goToLogin();

                                break;

                            case 2:

                                // Google+

                                dialog = new SpotsDialog.Builder().setContext(UserMenu.this).setMessage("").build();
                                dialog.setCancelable(false);
                                dialog.show();

                                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                        new ResultCallback<Status>() {
                                            @Override
                                            public void onResult(Status status) {
                                                // ...

                                                Log.d(TAG, "onResult: "+mGoogleApiClient.isConnected());
                                                Log.d(TAG, "onResult: "+status.getStatus() );
                                                goToLogin();

                                            }
                                        });

                                break;

                            case 3:

                                dialog = new SpotsDialog.Builder().setContext(UserMenu.this).setMessage("").build();
                                dialog.setCancelable(false);
                                dialog.show();

                                // sign up
                                goToLogin();


                                break;
                        }


                    }
                })
                .show();

    }

    public static void deleteCache(Context context) {

        try {

            File dir = context.getCacheDir();
            deleteDir(dir);

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    public static boolean deleteDir(File dir) {

        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }

    }

    private void goToLogin() {

        //call logout api
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        String user_id = Pref_storage.getDetail(this, "userId");

        Call<Logout_Response> call = apiService.update_logout("update_logout", user_id);

        call.enqueue(new Callback<Logout_Response>() {
            @Override
            public void onResponse(Call<Logout_Response> call, Response<Logout_Response> response) {


                if(response.body().getResponseMessage().equalsIgnoreCase("success")) {

                    dialog.dismiss();

                    deleteCache(getApplicationContext());
                    Pref_storage.clearData();

                    Intent intent = new Intent(getApplicationContext(), Login.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }else {

                    dialog.dismiss();

                    Toast.makeText(UserMenu.this, "Unable to logout", Toast.LENGTH_SHORT).show();
                }




            }

            @Override
            public void onFailure(Call<Logout_Response> call, Throwable t) {
                dialog.dismiss();
                //Error
                Log.e("FailureError", "" + t.getMessage());
            }
        });


//        ((ActivityManager) context.getSystemService(ACTIVITY_SERVICE)).clearApplicationUserData();


    }



    public static void clearSharedPreferences(Context ctx){
        File dir = new File(ctx.getFilesDir().getParent() + "/shared_prefs/");
        String[] children = dir.list();
        for (int i = 0; i < children.length; i++) {
            // clear each of the prefrances
            Log.d(TAG, "clearSharedPreferences: "+children[i]);
            ctx.getSharedPreferences(children[i].replace(".xml", ""), Context.MODE_PRIVATE).edit().clear().commit();
        }
        // Make sure it has enough time to save all the commited changes
        try { Thread.sleep(10000); } catch (InterruptedException e) {}
        for (int i = 0; i < children.length; i++) {
            // delete the files
            new File(dir, children[i]).delete();
        }
    }

    private void get_profile() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        String user_id = Pref_storage.getDetail(this, "userId");

        Call<Profile_output_pojo> call = apiService.get_profile("get_profile", Integer.parseInt(user_id),Integer.parseInt(user_id));

        call.enqueue(new Callback<Profile_output_pojo>() {
            @Override
            public void onResponse(Call<Profile_output_pojo> call, Response<Profile_output_pojo> response) {


                if(response.body() != null){


                    if (response.body().getResponseCode() == 1) {

                        for (int j = 0; j < response.body().getData().size(); j++) {

                            String userId = response.body().getData().get(j).getUserId();

                            firstName1 = response.body().getData().get(j).getFirstName();
                            lastName1 = response.body().getData().get(j).getLastName();
                            username = response.body().getData().get(j).getUsername();
                            password = response.body().getData().get(j).getPassword();
                            email1 = response.body().getData().get(j).getEmail();
                            gender = response.body().getData().get(j).getGender();
                            dob = response.body().getData().get(j).getDob();
                            picture = response.body().getData().get(j).getPicture();

                            Pref_storage.setDetail(getApplicationContext(),"picture_user_login",picture);

                            pro_status = response.body().getData().get(j).getProfileStatus();
                            total_post = response.body().getData().get(j).getTotalPosts();
                            follwing = response.body().getData().get(j).getTotalFollowings();
                            followers = response.body().getData().get(j).getTotalFollowers();
                            logintype = response.body().getData().get(j).getLoginType();
                            bio_description = response.body().getData().get(j).getBioDescription();
                            Log.e("Bio", "onResponse:"+bio_description );
                            fullname = firstName1.concat(" ").concat(lastName1);
                            txt_firstname.setText(fullname);




                        }
                    }


                }


            }

            @Override
            public void onFailure(Call<Profile_output_pojo> call, Throwable t) {
                //Error
                Log.e("FailureError", "" + t.getMessage());
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();


        GlideApp.with(this)
                .load(Pref_storage.getDetail(this,"picture_user_login"))
                .placeholder(R.drawable.ic_add_photo)
                .into(user_image);

        get_profile();

    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Pref_storage.setDetail(UserMenu.this,"scroll","false");
        finish();
        overridePendingTransition(R.anim.swipe_righttoleft, R.anim.swipe_lefttoright);
    }
}


