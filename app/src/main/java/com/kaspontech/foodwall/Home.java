package com.kaspontech.foodwall;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.bucketlistpackage.MyBucketlistActivity;
import com.kaspontech.foodwall.chatpackage.chatUserProfileActivity;
import com.kaspontech.foodwall.foodFeedsPackage.TotalCountModule_Response;
import com.kaspontech.foodwall.menuPackage.UserMenu;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.ClearChatResponse_Pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Newtimeline_output_response;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_notification_count_output;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.commonSearchView.CommonSearch;
import com.kaspontech.foodwall.foodFeedsPackage.TimeLineFeeds;
import com.kaspontech.foodwall.historicalMapPackage.CreateHistoricalMap;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_online_status_pojo;
import com.kaspontech.foodwall.notificationsPackage.AppNotification;
import com.kaspontech.foodwall.questionspackage.QuestionAnswer;
import com.kaspontech.foodwall.reviewpackage.Reviews;
import com.kaspontech.foodwall.utills.BottomNavigationViewHelper;
import com.kaspontech.foodwall.utills.Utility;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.foodFeedsPackage.TimeLineFeeds.btnSizeCheck;
import static com.kaspontech.foodwall.foodFeedsPackage.TimeLineFeeds.rv_newsfeed;
import static com.kaspontech.foodwall.notificationsPackage.AppNotification.scroll_notify;
import static com.kaspontech.foodwall.questionspackage.QuestionAnswer.question_scrollview;
import static com.kaspontech.foodwall.reviewpackage.allTab.AllFragment.scrollAllfragment;


public class Home extends _SwipeActivityClass implements View.OnClickListener, LocationListener {

    /**
     * Application TAG
     **/

    private static final String TAG = "Home";

    /**
     * Action bar
     **/
    Toolbar actionBar;
    /**
     * Main layout
     **/
    RelativeLayout rl_main;
    /**
     * Common edit search
     **/
    private EditText edit_common_search;
    /**
     * Button camera and chat
     **/
    ImageButton button_camera, chat, add_post;

    // Widgets
    BottomNavigationView bottom_navigation_home;

    Menu menu;

    // Fragments
    Fragment timeLineFeeds;
    Fragment reviews;
    Fragment events;
    Fragment questionAnswer;
    Fragment profile;
    Fragment notifications;
    Fragment userMenu;


    boolean doubleBackToExitPressedOnce = false;

    Handler mHandler;
    TimerTask timerTask;
    Timer timer;

    /**
     * Online checking
     **/
    LocationManager locationManager;

    Location location;

    /**
     * latitude & longitude
     **/

    double latitude_online, longitude_online;

    /**
     * String results
     **/

    String landing, imei, mprovider;

    TextView textViewApplicationCout;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Pref_storage.setDetail(Home.this, "scroll", "test");

        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        /*Widgets initialization*/
        actionBar = findViewById(R.id.action_bar_home);

        edit_common_search = actionBar.findViewById(R.id.edit_common_search);
        button_camera = actionBar.findViewById(R.id.button_camera);
        chat = actionBar.findViewById(R.id.chat);
        rl_main = actionBar.findViewById(R.id.rl_main);
        bottom_navigation_home = (BottomNavigationView) findViewById(R.id.home_bottomNavigation);
        menu = bottom_navigation_home.getMenu();

        /*Onclick listeners*/
        button_camera.setOnClickListener(this);
        chat.setOnClickListener(this);
        edit_common_search.setOnClickListener(this);


        //Back ground service for app offline API calling
        createChatLoginSessiontime();

        backgroundServiceForOnlineChecking();

        getNotificationsCount();


        //to get notification count for all modeules
        get_total_count_modules();

        /*Fragment Initialization*/
        timeLineFeeds = new TimeLineFeeds();
        reviews = new Reviews();
        notifications = new AppNotification();
        questionAnswer = new QuestionAnswer();
        userMenu = new MyBucketlistActivity();


        //Redirecting to relavant landing page
        Intent intent = getIntent();
        landing = intent.getStringExtra("redirect");

        if (landing != null) {

            if (landing.equalsIgnoreCase("from_review")) {

                changeFragment(reviews, "review");
                //replaceFragment(reviews);
                bottom_navigation_home.setSelectedItemId(R.id.action_review);

            } else if (landing.equalsIgnoreCase("profile")) {

                changeFragment(userMenu, "profile");
                //replaceFragment(reviews);
                bottom_navigation_home.setSelectedItemId(R.id.action_menu);

            } else if (landing.equalsIgnoreCase("timeline")) {

                changeFragment(timeLineFeeds, "timeline");
                bottom_navigation_home.setSelectedItemId(R.id.action_feeds);

            } else if (landing.equalsIgnoreCase("qa")) {

                changeFragment(questionAnswer, "qa");
                bottom_navigation_home.setSelectedItemId(R.id.action_questions);

            } else if (landing.equalsIgnoreCase("notification")) {
                changeFragment(notifications, "notification");
                bottom_navigation_home.setSelectedItemId(R.id.action_notifications);

            }


        } else {

            changeFragment(timeLineFeeds, "timeline");
            //  replaceFragment(timeLineFeeds);
        }

        BottomNavigationViewHelper.removeShiftMode(bottom_navigation_home);


        // Menu navigation listener
        bottom_navigation_home.setOnNavigationItemSelectedListener(

                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                        switch (item.getItemId()) {
                            /*TimelineFeeds Module*/
                            case R.id.action_feeds:

                                textViewApplicationCout = inApplicationCount();

                                if (textViewApplicationCout != null) {
                                    if (textViewApplicationCout.getVisibility() == View.VISIBLE) {
                                        textViewApplicationCout.setVisibility(View.GONE);
                                    }
                                }

                                //replaceFragment(timeLineFeeds);
                                changeFragment(timeLineFeeds, "timeline");
                                break;

                            /*Review module*/
                            case R.id.action_review:

                                textViewApplicationCout = inApplicationCount();

                                if (textViewApplicationCout != null) {
                                    if (textViewApplicationCout.getVisibility() == View.VISIBLE) {
                                        textViewApplicationCout.setVisibility(View.GONE);
                                    }
                                }

                                changeFragment(reviews, "review");

                                // replaceFragment(reviews);
                                try {

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;

                            /*Notification module*/
                            case R.id.action_notifications:

                                textViewApplicationCout = inApplicationCount();

                                if (textViewApplicationCout != null) {
                                    if (textViewApplicationCout.getVisibility() == View.VISIBLE) {
                                        textViewApplicationCout.setVisibility(View.GONE);
                                    }
                                }

                                changeFragment(notifications, "notification");
                                break;

                            /*Question Answer Module*/
                            case R.id.action_questions:

                                textViewApplicationCout = inApplicationCount();

                                if (textViewApplicationCout != null) {
                                    if (textViewApplicationCout.getVisibility() == View.VISIBLE) {
                                        textViewApplicationCout.setVisibility(View.GONE);
                                    }
                                }

                                changeFragment(questionAnswer, "qa");
                                // replaceFragment(questionAnswer);
                                break;

                            /*Menu module*/
                            case R.id.action_menu:

                                textViewApplicationCout = inApplicationCount();

                                if (textViewApplicationCout != null) {
                                    if (textViewApplicationCout.getVisibility() == View.VISIBLE) {
                                        textViewApplicationCout.setVisibility(View.GONE);
                                    }
                                }

                                changeFragment(userMenu, "profile");
                                // replaceFragment(userMenu);
                                break;
                        }
                        return true;
                    }
                });

        bottom_navigation_home.setOnNavigationItemReselectedListener(
                new BottomNavigationView.OnNavigationItemReselectedListener() {
                    @Override
                    public void onNavigationItemReselected(@NonNull MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            /*TimelineFeeds Module*/
                            case R.id.action_feeds:

                                textViewApplicationCout = inApplicationCount();

                                if (textViewApplicationCout != null) {
                                    if (textViewApplicationCout.getVisibility() == View.VISIBLE) {
                                        textViewApplicationCout.setVisibility(View.GONE);
                                    }
                                }

                                changeFragment(timeLineFeeds, "timeline");

                                try {
                                    rv_newsfeed.scrollToPosition(0);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                break;
                            /*Review module*/
                            case R.id.action_review:
                                changeFragment(reviews, "review");

                                try {
                                    // by double tab recyclerview move to 0 position
                                    scrollAllfragment.scrollTo(0, 0);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;


                            case R.id.action_notifications:

                                textViewApplicationCout = inApplicationCount();

                                if (textViewApplicationCout != null) {
                                    if (textViewApplicationCout.getVisibility() == View.VISIBLE) {
                                        textViewApplicationCout.setVisibility(View.GONE);
                                    }
                                }

                                changeFragment(notifications, "notification");


                                try {
                                    scroll_notify.scrollTo(0, 0);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                break;

                            /*Question Answer Module*/
                            case R.id.action_questions:

                                textViewApplicationCout = inApplicationCount();

                                if (textViewApplicationCout != null) {
                                    if (textViewApplicationCout.getVisibility() == View.VISIBLE) {
                                        textViewApplicationCout.setVisibility(View.GONE);
                                    }
                                }

                                changeFragment(questionAnswer, "qa");


                                try {
                                    // by double tab recyclerview move to 0 position
                                    question_scrollview.scrollTo(0, 0);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                break;

                        }
                    }
                });

        //Getting location details
        getLatLong();


        //Chat history continuous loop

        mHandler = new Handler();
        timer = new Timer();

        timerTask = new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    public void run() {

                        try {
                            getNotificationsCountLoop();


                            get_total_count_modulesLoop();


                        } catch (Exception e) {
                            // error, do something
                        }
                    }
                });
            }
        };

        timer.schedule(timerTask, 0, 5000);
    }


    private void get_total_count_modulesLoop() {
        try {
            if (Utility.isConnected(getApplicationContext())) {


                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {

                    String userid = Pref_storage.getDetail(getApplicationContext(), "userId");

                    Call<TotalCountModule_Response> call = apiService.get_total_count_modules("get_total_count_modules",
                            Integer.parseInt(userid));


                    call.enqueue(new Callback<TotalCountModule_Response>() {
                        @Override
                        public void onResponse(@NonNull Call<TotalCountModule_Response> call, @NonNull Response<TotalCountModule_Response> response) {


                            if (response.body().getResponseMessage().equalsIgnoreCase("success")) {


                                try {

                                    for (int i = 0; i < response.body().getData().size(); i++) {

                                        int count = response.body().getData().get(i).getTotalPosts();
                                        int oldcount = Integer.parseInt(Pref_storage.getDetail(Home.this, "TotalPostCount"));

                                        int revcount = response.body().getData().get(i).getTotalReview();
                                        int revoldcount = Integer.parseInt(Pref_storage.getDetail(Home.this, "TotalReviewCount"));

                                        int evecount = response.body().getData().get(i).getTotalEvents();
                                        int eveoldcount = Integer.parseInt(Pref_storage.getDetail(Home.this, "TotalEventCount"));

                                        int qacount = response.body().getData().get(i).getTotalQuestion();
                                        int qaoldcount = Integer.parseInt(Pref_storage.getDetail(Home.this, "TotalQuestionCount"));

                                        Log.e(TAG, "counttotal:count " + count);
                                        Log.e(TAG, "counttotal:oldcount " + oldcount);
                                        Log.e(TAG, "counttotal:revcount " + revcount);
                                        Log.e(TAG, "counttotal:revoldcount " + revoldcount);
                                        Log.e(TAG, "counttotal:question " + qacount);
                                        Log.e(TAG, "counttotal:question " + qaoldcount);


                                        //Timeline notification count
                                        BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) bottom_navigation_home.getChildAt(0);
                                        View v = bottomNavigationMenuView.getChildAt(0);
                                        BottomNavigationItemView itemView = (BottomNavigationItemView) v;

                                        View badge = LayoutInflater.from(Home.this)
                                                .inflate(R.layout.notification_badge, itemView, true);

                                        TextView notifications = badge.findViewById(R.id.notifications);

                                        //check previous and present count
                                        if (count > oldcount) {
                                            //timeline
                                            int countt = count - oldcount;

                                            notifications.setVisibility(View.VISIBLE);
                                            notifications.setText(String.valueOf(countt));

                                            Pref_storage.setDetail(Home.this, "timeline", "1");

                                        } else {
                                            Pref_storage.setDetail(Home.this, "timeline", "0");

                                            notifications.setText("");
                                            notifications.setVisibility(View.GONE);

                                            menu.findItem(R.id.action_feeds).setIcon(R.drawable.ic_home_new);

                                        }


                                        //Review notification count
                                        BottomNavigationMenuView bottomNavigationMenuView_review = (BottomNavigationMenuView) bottom_navigation_home.getChildAt(0);
                                        View v_review = bottomNavigationMenuView_review.getChildAt(1);
                                        BottomNavigationItemView itemView_review = (BottomNavigationItemView) v_review;

                                        View badge_review = LayoutInflater.from(Home.this)
                                                .inflate(R.layout.review_count, itemView_review, true);

                                        TextView notifications_review = badge_review.findViewById(R.id.notifications);


                                        //check previous and present count
                                        if (revcount > revoldcount) {
                                            //timeline
                                            int countt = revcount - revoldcount;

                                            notifications_review.setVisibility(View.VISIBLE);
                                            notifications_review.setText(String.valueOf(countt));


                                        } else {

                                            notifications_review.setText("");
                                            notifications_review.setVisibility(View.GONE);


                                            menu.findItem(R.id.action_review).setIcon(R.drawable.ic_review_photos);

                                        }

                                        //QA notification count

                                        BottomNavigationMenuView bottomNavigationMenuView_qa = (BottomNavigationMenuView) bottom_navigation_home.getChildAt(0);
                                        View v_qa = bottomNavigationMenuView_qa.getChildAt(2);
                                        BottomNavigationItemView itemView_qa = (BottomNavigationItemView) v_qa;

                                        View badge_qa = LayoutInflater.from(Home.this)
                                                .inflate(R.layout.qa_count, itemView_qa, true);

                                        TextView notifications_qa = badge_qa.findViewById(R.id.notifications);

                                        //check previous and present count
                                        if (qacount > qaoldcount) {
                                            //timeline
                                            int countt = qacount - qaoldcount;

                                            notifications_qa.setVisibility(View.VISIBLE);
                                            notifications_qa.setText(String.valueOf(countt));


                                        } else {

                                            notifications_qa.setText("");
                                            notifications_qa.setVisibility(View.GONE);


                                            menu.findItem(R.id.action_review).setIcon(R.drawable.ic_review_photos);

                                        }


                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e("errorresponce1", "get_total_count_modulesLoop: " + e);

                                }

                            }

                        }

                        @Override
                        public void onFailure(@NonNull Call<TotalCountModule_Response> call, @NonNull Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());

                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("errorresponce2", "get_total_count_modulesLoop: " + e);
                }

            } else {
                Toast.makeText(getApplicationContext(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void get_total_count_modules() {
        try {
            if (Utility.isConnected(getApplicationContext())) {


                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {

                    String userid = Pref_storage.getDetail(getApplicationContext(), "userId");

                    Call<TotalCountModule_Response> call = apiService.get_total_count_modules("get_total_count_modules",
                            Integer.parseInt(userid));
                    call.enqueue(new Callback<TotalCountModule_Response>() {
                        @Override
                        public void onResponse(Call<TotalCountModule_Response> call, Response<TotalCountModule_Response> response) {

                            if (response.body().getResponseMessage().equalsIgnoreCase("success")) {

                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    //save total timeline count
                                    Pref_storage.setDetail(Home.this, "TotalPostCount", String.valueOf(response.body().getData().get(i).getTotalPosts()));

                                    Log.e(TAG, "counttotal: post" + response.body().getData().get(i).getTotalPosts());

                                    Pref_storage.setDetail(Home.this, "TotalReviewCount", String.valueOf(response.body().getData().get(i).getTotalReview()));

                                    Log.e(TAG, "counttotal:review " + response.body().getData().get(i).getTotalEvents());

                                    Pref_storage.setDetail(Home.this, "TotalEventCount", String.valueOf(response.body().getData().get(i).getTotalEvents()));

                                    Log.e(TAG, "counttotal:event " + response.body().getData().get(i).getTotalEvents());

                                    Pref_storage.setDetail(Home.this, "TotalQuestionCount", String.valueOf(response.body().getData().get(i).getTotalQuestion()));

                                    Log.e(TAG, "counttotal:question " + response.body().getData().get(i).getTotalQuestion());

                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<TotalCountModule_Response> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getApplicationContext(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onSwipeRight() {
        Intent intent = new Intent(Home.this, UserMenu.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_right);
        finish();
    }

    @Override
    protected void onSwipeLeft() {
        Intent intent1 = new Intent(Home.this, ChatActivity.class);
        startActivity(intent1);
        overridePendingTransition(R.anim.swipe_righttoleft, R.anim.swipe_lefttoright);
        finish();
    }


    //Back ground service for app offline API calling
    private void backgroundServiceForOnlineChecking() {
        Intent intent1 = new Intent(Home.this, AppStatusService.class);
        startService(intent1);
    }


    //Calling get notification count API
    private void getNotificationsCount() {

        if (Utility.isConnected(getApplicationContext())) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String userId = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_notification_count_output> call = apiService.get_notifications_count("get_notifications_count", Integer.parseInt(userId));
                call.enqueue(new Callback<Get_notification_count_output>() {
                    @Override
                    public void onResponse(Call<Get_notification_count_output> call, Response<Get_notification_count_output> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                final String count = response.body().getData().get(j).getTotalCount();

                                /*Saving the notification count in shared preference*/

                                Pref_storage.setDetail(getApplicationContext(), "NotificationCount", count);


                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<Get_notification_count_output> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Toast.makeText(getApplicationContext(), "No Internet connection.", Toast.LENGTH_SHORT).show();
        }


    }    //Calling get notification count API

    private void getNotificationsCountLoop() {

        if (Utility.isConnected(getApplicationContext())) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String userId = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_notification_count_output> call = apiService.get_notifications_count("get_notifications_count", Integer.parseInt(userId));
                call.enqueue(new Callback<Get_notification_count_output>() {
                    @Override
                    public void onResponse(Call<Get_notification_count_output> call, Response<Get_notification_count_output> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                final String count = response.body().getData().get(j).getTotalCount();


                                try {
                                    /*Comparing the notification count from shared preference*/
                                    int oldCount = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "NotificationCount"));

                                    BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) bottom_navigation_home.getChildAt(0);
                                    View v = bottomNavigationMenuView.getChildAt(3);
                                    BottomNavigationItemView itemView = (BottomNavigationItemView) v;

                                    View badge = LayoutInflater.from(Home.this).inflate(R.layout.appnotification_count, itemView, true);
                                    TextView notifications = badge.findViewById(R.id.notifications);

                                    if (Integer.parseInt(count) > oldCount) {

                                        //  menu.findItem(R.id.action_notifications).setIcon(R.drawable.ic_notificationdot);
                                        // Inapp notification count
                                        notifications.setVisibility(View.VISIBLE);
                                        notifications.setText(String.valueOf(Integer.parseInt(count) - oldCount));

                                    } else {

                                        menu.findItem(R.id.action_notifications).setIcon(R.drawable.ic_notifications);
                                        notifications.setVisibility(View.GONE);

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<Get_notification_count_output> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Toast.makeText(getApplicationContext(), "No Internet connection.", Toast.LENGTH_SHORT).show();
        }


    }

    //Getting Latitude and longitude

    private void getLatLong() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();

        mprovider = locationManager.getBestProvider(criteria, false);

        if (mprovider != null && !mprovider.equals("")) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = locationManager.getLastKnownLocation(mprovider);
            locationManager.requestLocationUpdates(mprovider, 15000, 1, this);

            if (location != null)
                onLocationChanged(location);
            // else
            // Toast.makeText(getBaseContext(), "No Location Provider Found.", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

//                    imei = getImeiNumber();
                    imei = "0000000000000";


                } else {

                    Toast.makeText(Home.this, "You have Denied the Permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


        // Checks whether a hardware keyboard is available
        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {

            Toast.makeText(this, "keyboard visible", Toast.LENGTH_SHORT).show();

        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {

            Toast.makeText(this, "keyboard hidden", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {


            /*Common search click listener*/

            case R.id.edit_common_search:

                int tabId = bottom_navigation_home.getSelectedItemId();
                Log.e(TAG, "onClick: " + tabId);
                Log.e(TAG, "onClick: " + R.id.action_review);


                switch (tabId) {

                    case R.id.action_review:

                        Intent intent1 = new Intent(Home.this, CommonSearch.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("position", 1);
                        intent1.putExtras(bundle);
                        startActivity(intent1);
//                        Home.this.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);


                        break;

                    case R.id.action_questions:

                        Intent intent2 = new Intent(Home.this, CommonSearch.class);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("position", 2);
                        intent2.putExtras(bundle1);
                        startActivity(intent2);
//                        Home.this.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                        break;

                    default:

                        Intent intent3 = new Intent(Home.this, CommonSearch.class);
                        Bundle bundle2 = new Bundle();
                        bundle2.putInt("position", 0);
                        intent3.putExtras(bundle2);
                        startActivity(intent3);
//                        Home.this.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                        break;
                }


                break;

            /* Intent to create new post */

            case R.id.button_camera:
                Intent intent = new Intent(Home.this, UserMenu.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_right);


                break;

            /* Intent to chat page*/
            case R.id.chat:


                Intent intent1 = new Intent(Home.this, ChatActivity.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.swipe_righttoleft, R.anim.swipe_lefttoright);
                // finish();
                break;

        }


    }


    /**
     * Replace fragments using fragment manager
     **/
    public void replaceFragment(Fragment fragment) {

        try {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.home_container, fragment);
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // change fragment
    public void changeFragment(Fragment fragment, String tagFragmentName) {

        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        Fragment currentFragment = mFragmentManager.getPrimaryNavigationFragment();
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment);
        }

        Fragment fragmentTemp = mFragmentManager.findFragmentByTag(tagFragmentName);
        if (fragmentTemp == null) {
            fragmentTemp = fragment;
            fragmentTransaction.add(R.id.home_container, fragmentTemp, tagFragmentName);
        } else {
            fragmentTransaction.show(fragmentTemp);
        }

        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp);
        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.commitNowAllowingStateLoss();
    }

    /**
     * GPS process - to get latitude and longitude
     **/


    public void getLocation() {
        try {
            locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, (LocationListener) getApplicationContext());

            latitude_online = location.getLatitude();
            longitude_online = location.getLongitude();

            Log.e(TAG, "getLocation: lati" + latitude_online);
            Log.e(TAG, "getLocation: longi" + longitude_online);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Forground checking

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        isInBackground = false;

                        Toast.makeText(this, "Hii", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "no hi", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

        return isInBackground;
    }

    // Chat session Online API calling (For user Online/Offline status checking)

    private void createChatLoginSessiontime() {


        if (Utility.isConnected(getApplicationContext())) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");
                String imei = Pref_storage.getDetail(getApplicationContext(), "IMEI");
                if (imei == null) {
                    imei = "";
                }

//                latitude_online= 12.97289526;
//                longitude_online=80.274808;

                Call<Get_chat_online_status_pojo> call = apiService.create_chat_login_sessiontime(
                        "create_chat_login_sessiontime",
                        1,
                        Integer.parseInt(createdby),
                        imei,
                        latitude_online,
                        longitude_online);

                call.enqueue(new Callback<Get_chat_online_status_pojo>() {
                    @Override
                    public void onResponse(Call<Get_chat_online_status_pojo> call, Response<Get_chat_online_status_pojo> response) {

                        try {
                            if (response.body().getResponseCode() == 1) {


                                String loggedintime = response.body().getData();


                                Log.e("loggedintime", "onResponse:" + loggedintime);


                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<Get_chat_online_status_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Toast.makeText(getApplicationContext(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }


    }

    // Chat session offline API calling (For user Online/Offline status checking)

    private void createChatLoginSessiontimeOffline() {

        if (Utility.isConnected(getApplicationContext())) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                String imei = Pref_storage.getDetail(getApplicationContext(), "IMEI");

                if (imei == null) {

                    imei = "";

                }

                Call<Get_chat_online_status_pojo> call = apiService.create_chat_login_sessiontime("create_chat_login_sessiontime", 0, Integer.parseInt(createdby), imei, latitude_online, longitude_online);

                call.enqueue(new Callback<Get_chat_online_status_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Get_chat_online_status_pojo> call, @NonNull Response<Get_chat_online_status_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            String loggedOut = response.body().getData();
                            Log.e("loggedOut", "loggedOut:" + loggedOut);

                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Get_chat_online_status_pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e(TAG, "FailureError" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(getApplicationContext(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();

        }


    }


    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

            createChatLoginSessiontimeOffline();

//            mHandler.removeCallbacksAndMessages(null);
//            timerTask.cancel();
//            timer.cancel();
//            timer.purge();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Snackbar snackbar = Snackbar
                .make(findViewById(android.R.id.content), "Please click back again to exit.", Snackbar.LENGTH_LONG);
        snackbar.show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

        createChatLoginSessiontimeOffline();


        mHandler.removeCallbacksAndMessages(null);
        timerTask.cancel();
        timer.cancel();
        timer.purge();
    }

    @Override
    public void onLocationChanged(Location location) {

        latitude_online = location.getLatitude();
        longitude_online = location.getLongitude();

        Log.e(TAG, "getLocation: lati" + latitude_online);
        Log.e(TAG, "getLocation: longi" + longitude_online);

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private TextView inApplicationCount() {

        BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) bottom_navigation_home.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(3);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;

        View badge = LayoutInflater.from(Home.this).inflate(R.layout.appnotification_count, itemView, true);
        TextView notifications = badge.findViewById(R.id.notifications);
        return notifications;
    }
}
