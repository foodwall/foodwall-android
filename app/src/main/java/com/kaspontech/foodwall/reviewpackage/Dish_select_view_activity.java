package com.kaspontech.foodwall.reviewpackage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;

/**
 * Created by Vishnu on 21-06-2018.
 */

public class Dish_select_view_activity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "Dish_select_view";
    ImageView img_dish;
    EditText edt_food_name;
    Toolbar actionbar_dish_fullimage;
    TextView toolbarnext;
    String image_dish, dish_name,for_top,for_worst;
    String selectedImagePath,
            Hotel_name, Hotel_location, Hotel_icon, Hotel_rating,place_id,google_id, image,dish_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dish_view_select_layout);
        actionbar_dish_fullimage= (Toolbar)findViewById(R.id.actionbar_dish_fullimage);
        toolbarnext= (TextView)actionbar_dish_fullimage.findViewById(R.id.story_share);
         toolbarnext.setText(R.string.done);
         toolbarnext.setOnClickListener(this);
        img_dish=(ImageView) findViewById(R.id.img_dish);
        edt_food_name=(EditText) findViewById(R.id.edt_food_name);

        Intent intent= getIntent();
        image_dish= intent.getStringExtra("dish_image");
        dish_name= intent.getStringExtra("dish_name");
        Hotel_name = intent.getStringExtra("Hotel_name");
        Hotel_icon = intent.getStringExtra("Hotel_icon");
        Hotel_location = intent.getStringExtra("Hotel_location");
        Hotel_rating = intent.getStringExtra("Hotel_rating");
        place_id = intent.getStringExtra("placeid");
        google_id = intent.getStringExtra("googleid");
        for_top = intent.getStringExtra("for_top");
        for_worst = intent.getStringExtra("for_worst");

        img_dish.setImageURI(Uri.parse(image_dish));

    }
    @Override
    public void onClick(View v) {

        int clicked= v.getId();
        switch (clicked){
            case R.id.story_share:

                if(edt_food_name.getText().toString().equalsIgnoreCase("")){
                    Toast.makeText(this, "Dish name is empty!", Toast.LENGTH_SHORT).show();
                }else {

                    Intent intent = new Intent(Dish_select_view_activity.this, New_review_activity.class);

                    intent.putExtra("dish_image", image_dish);
                    intent.putExtra("dish_name", edt_food_name.getText().toString());
                    intent.putExtra("Hotel_name",Hotel_name);
                    intent.putExtra("Hotel_icon",Hotel_icon);
                    intent.putExtra("Hotel_rating",Hotel_rating);
                    intent.putExtra("Hotel_location",Hotel_location);
                    intent.putExtra("placeid",place_id);
                    intent.putExtra("googleid",google_id);
                    intent.putExtra("for_top",for_top);
                    intent.putExtra("for_worst",for_worst);
                    startActivity(intent);
                    finish();
                }

                break;
        }

    }
}
