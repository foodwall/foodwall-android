package com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReviewTopDishPojo {
    @SerializedName("dishname")
    @Expose
    private String dishname;

    @SerializedName("img")
    @Expose
    private String img;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param img
     */
    public ReviewTopDishPojo(String img,String dishname) {
        super();
        this.img = img;
        this.dishname = dishname;
    }

    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
