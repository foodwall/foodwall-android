package com.kaspontech.foodwall.reviewpackage.Review_adapter_folder;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.R;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

//

public class Review_image_top_dish_adapter extends RecyclerView.Adapter<Review_image_top_dish_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;


    private ArrayList<Review_image_pojo> getttingimagelist = new ArrayList<>();

    private ArrayList<String> aListLanguage = new ArrayList<String>();


    private Review_image_pojo reviewImagePojo;

    private TopDishRemovedListener topDishRemovedListener;

    private EmojiconEditText edt_food_caption;

    public interface TopDishRemovedListener {

        void removedTopImageLisenter(ArrayList<Review_image_pojo> imagesList);

    }

    public Review_image_top_dish_adapter(Context c, ArrayList<Review_image_pojo> gettinglist, TopDishRemovedListener topDishRemovedListener, EmojiconEditText edt_food_caption) {
        this.context = c;
        this.getttingimagelist = gettinglist;
        this.topDishRemovedListener = topDishRemovedListener;
        this.edt_food_caption = edt_food_caption;
    }

    public Review_image_top_dish_adapter(Context c, ArrayList<Review_image_pojo> gettinglist, TopDishRemovedListener topDishRemovedListener) {
        this.context = c;
        this.getttingimagelist = gettinglist;
        this.topDishRemovedListener = topDishRemovedListener;
    }


    @NonNull
    @Override
    public Review_image_top_dish_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_review_image, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Review_image_top_dish_adapter.MyViewHolder vh = new Review_image_top_dish_adapter.MyViewHolder(v);

        Pref_storage pref_storage = new Pref_storage();
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Review_image_top_dish_adapter.MyViewHolder holder, final int position) {


        reviewImagePojo = getttingimagelist.get(position);

        edt_food_caption.setText("");
        Log.e("text", "ViewText--------->  " + edt_food_caption.toString());
        edt_food_caption.removeTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                edt_food_caption.setText("");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                edt_food_caption.setText("");
            }

            @Override
            public void afterTextChanged(Editable editable) {
                edt_food_caption.setText("");
            }
        });

        holder.txt_image.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(reviewImagePojo.getImage_text()));
        holder.txt_image.setTextColor(Color.parseColor("#000000"));


        // Log.e("Image-->", "image" + reviewImagePojo.getImage());
        // Log.e("Image-->", "text-->" + reviewImagePojo.getImage_text());

//        Utility.picassoImageLoader(reviewImagePojo.getImage(),
//                 0,holder.image_review);

        GlideApp.with(context)
                .load(reviewImagePojo.getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(holder.image_review);


        holder.remove_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getttingimagelist.remove(position);
                notifyDataSetChanged();
                topDishRemovedListener.removedTopImageLisenter(getttingimagelist);

            }
        });

    }


    @Override
    public int getItemCount() {
        Log.e("view", "image--------> " + getttingimagelist.size());
        return getttingimagelist.size();

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //TextView

        TextView txt_image;

        //Imageview

        ImageView image_review;
        ImageButton remove_image;

        RelativeLayout rl_hotel_datas;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_image = (TextView) itemView.findViewById(R.id.txt_image);
            image_review = (ImageView) itemView.findViewById(R.id.image_review);
            remove_image = (ImageButton) itemView.findViewById(R.id.remove_image);
            rl_hotel_datas = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_datas);

        }


    }


}
