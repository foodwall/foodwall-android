package com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailedTopdishImagePojo {
    @SerializedName("dishname")
    @Expose
    private String dishname;

    @SerializedName("img")
    @Expose
    private String img;

    /**
     * No args constructor for use in serialization
     *
     */


    public DetailedTopdishImagePojo(String dishname, String img) {
        this.dishname = dishname;
        this.img = img;
    }




    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
