package com.kaspontech.foodwall.reviewpackage.alldishtab;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_ambience_image_all;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_ambience_image_pojo_output;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.reviewpackage.alldishtab.alldishadapters.AmbienceAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AmbienceFragment extends Fragment {
    /**
     * View
     */
    View view;
    /**
     * Context
     */
    Context context;
    /**
     * Recycler view for Ambience dish
     */
    RecyclerView rvAmbienceFragment;

    /**
     * Loader
     */
    ProgressBar progressbarAmbience;

    /**
     * No dish - Text view
     */
    TextView txtNoDish;
    /**
     * Getting ambience type all input pojo
     */
    Get_ambience_image_all getAmbienceImageAll;

    /**
     * Getting ambience type all image pojo
     */
    ArrayList<Get_ambience_image_all> getAmbienceImageAllArrayList = new ArrayList<>();

    /**
     * Ambience Adapter
     */

    AmbienceAdapter ambianceImagesAdapter;


    /**
     * Integer - Review id
     */

    int reviewid;


    public AmbienceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.ambience_frag_layout, container, false);

        context = view.getContext();

        /* Recycler view for  ambience */

        rvAmbienceFragment = view.findViewById(R.id.rv_ambience_fragment);

        /*Loader*/
        progressbarAmbience = view.findViewById(R.id.progressbar_ambience);

        /* No dish available - Text view */

        txtNoDish = view.findViewById(R.id.txt_no_dish);


        /* Interface activity */
        AllDishDisplayActivity activity = (AllDishDisplayActivity) getActivity();
        if (activity != null) {
            reviewid = Integer.parseInt(activity.getReviewId());
        }

        /*Setting adapter and calling Getting ambience - API call*/

        if(!getAmbienceImageAllArrayList.isEmpty()){
            ambianceImagesAdapter= new AmbienceAdapter(context,getAmbienceImageAllArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            rvAmbienceFragment.setLayoutManager(mLayoutManager);
            rvAmbienceFragment.setHasFixedSize(true);
            rvAmbienceFragment.setItemAnimator(new DefaultItemAnimator());
            rvAmbienceFragment.setAdapter(ambianceImagesAdapter);
            rvAmbienceFragment.setNestedScrollingEnabled(false);
            ambianceImagesAdapter.notifyDataSetChanged();
            progressbarAmbience.setVisibility(View.GONE);
        }else {

            getAmbience();
        }


        return view;
    }


   /*Getting ambience - API call*/

    private void getAmbience() {


        ApiInterface apiService2 = ApiClient.getClient().create(ApiInterface.class);
        try {


            Call<Get_ambience_image_pojo_output> call2  = apiService2.get_hotel_review_ambiance_image_all_new(
                    "get_hotel_review_ambiance_image_all",
                    reviewid);

            call2.enqueue(new Callback<Get_ambience_image_pojo_output>() {
                @Override
                public void onResponse(@NonNull Call<Get_ambience_image_pojo_output> call2, @NonNull Response<Get_ambience_image_pojo_output> response) {

                    if (response.body() != null && response.body().getResponseCode() == 1) {

                        /*Clearing image arraylist*/
                        getAmbienceImageAllArrayList.clear();

                        for (int i = 0; i < response.body().getData().size(); i++) {

                            String photoId = response.body().getData().get(i).getPhotoId();
                            String reviewId = response.body().getData().get(i).getReviewid();
                            String userid = response.body().getData().get(i).getUserid();
                            String dishimage = response.body().getData().get(i).getDishimage();

                            getAmbienceImageAll = new Get_ambience_image_all(photoId, reviewId, userid, dishimage);
                            getAmbienceImageAllArrayList.add(getAmbienceImageAll);

                        }


                        ambianceImagesAdapter = new AmbienceAdapter(context, getAmbienceImageAllArrayList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                        rvAmbienceFragment.setLayoutManager(mLayoutManager);
                        rvAmbienceFragment.setHasFixedSize(true);
                        rvAmbienceFragment.setItemAnimator(new DefaultItemAnimator());
                        rvAmbienceFragment.setAdapter(ambianceImagesAdapter);
                        rvAmbienceFragment.setNestedScrollingEnabled(false);
                        ambianceImagesAdapter.notifyDataSetChanged();
                        progressbarAmbience.setVisibility(View.GONE);
                    }

                }



                @Override
                public void onFailure(@NonNull Call<Get_ambience_image_pojo_output> call, @NonNull Throwable t) {
                    if (getAmbienceImageAllArrayList.isEmpty()) {
                        txtNoDish.setVisibility(View.VISIBLE);
                    }
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }











    }



    @Override
    public void onResume() {
        super.onResume();
        if(!getAmbienceImageAllArrayList.isEmpty()){
            /*Getting ambience - API call*/

            getAmbience();

        }else {
            ambianceImagesAdapter= new AmbienceAdapter(context,getAmbienceImageAllArrayList);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            rvAmbienceFragment.setLayoutManager(mLayoutManager);
            rvAmbienceFragment.setHasFixedSize(true);
            rvAmbienceFragment.setItemAnimator(new DefaultItemAnimator());
            rvAmbienceFragment.setAdapter(ambianceImagesAdapter);
            rvAmbienceFragment.setNestedScrollingEnabled(false);
            ambianceImagesAdapter.notifyDataSetChanged();
            progressbarAmbience.setVisibility(View.GONE);
        }

    }


}
