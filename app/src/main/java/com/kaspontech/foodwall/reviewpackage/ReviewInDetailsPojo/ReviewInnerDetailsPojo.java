package com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;

import java.util.List;

public class ReviewInnerDetailsPojo {

    @SerializedName("hotel_id")
    @Expose
    private String hotelId;
    @SerializedName("google_id")
    @Expose
    private String googleId;
    @SerializedName("hotel_name")
    @Expose
    private String hotelName;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("open_times")
    @Expose
    private String openTimes;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("bucket_list")
    @Expose
    private String bucket_list;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("photo_reference")
    @Expose
    private String photoReference;
    @SerializedName("total_review")
    @Expose
    private String totalReview;
    @SerializedName("total_food_exprience")
    @Expose
    private String totalFoodExprience;
    @SerializedName("total_ambiance")
    @Expose
    private String totalAmbiance;
    @SerializedName("total_taste")
    @Expose
    private String totalTaste;
    @SerializedName("total_service")
    @Expose
    private String totalService;
    @SerializedName("total_package")
    @Expose
    private String totalPackage;
    @SerializedName("total_timedelivery")
    @Expose
    private String totalTimedelivery;
    @SerializedName("total_value_money")
    @Expose
    private String totalValueMoney;
    @SerializedName("total_good")
    @Expose
    private String totalGood;
    @SerializedName("total_bad")
    @Expose
    private String totalBad;
    @SerializedName("total_good_bad_user")
    @Expose
    private String totalGoodBadUser;
    @SerializedName("ambi_image")
    @Expose
    private List<DetailedAmbienceImagePojo> ambiImage = null;
    @SerializedName("ambi_image_count")
    @Expose
    private Integer ambiImageCount;
    @SerializedName("topdishimage")
    @Expose
    private List<DetailedTopdishImagePojo> topdishimage = null;
    @SerializedName("topdishimage_count")
    @Expose
    private Integer topdishimageCount;
    @SerializedName("topdish")
    @Expose
    private String topdish;
    @SerializedName("top_count")
    @Expose
    private Integer topCount;
    @SerializedName("avoiddishimage")
    @Expose
    private List<DetailedAvoiddishimagePojo> avoiddishimage = null;
    @SerializedName("avoiddishimage_count")
    @Expose
    private Integer avoiddishimageCount;
    @SerializedName("avoiddish")
    @Expose
    private String avoiddish;
    @SerializedName("avoid_count")
    @Expose
    private Integer avoidCount;
    @SerializedName("review")
    @Expose
    private List<DetailedReviewPojo> review=null;
    @SerializedName("review_count")
    @Expose
    private Integer reviewCount;


    @SerializedName("pack_image")
    @Expose
    private List<PackImage> packImage = null;
    @SerializedName("pack_image_count")
    @Expose
    private Integer packImageCount;


    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param avoiddishimage
     * @param phone
     * @param totalFoodExprience
     * @param totalAmbiance
     * @param hotelId
     * @param topdish
     * @param topCount
     * @param photoReference
     * @param totalTaste
     * @param totalReview
     * @param openTimes
     * @param placeId
     * @param hotelName
     * @param longitude
     * @param avoiddish
     * @param avoidCount
     * @param ambiImage
     * @param totalGoodBadUser
     * @param totalBad
     * @param ambiImageCount
     * @param googleId
     * @param avoiddishimageCount
     * @param topdishimage
     * @param totalService
     * @param address
     * @param totalTimedelivery
     * @param reviewCount
     * @param latitude
     * @param totalGood
     * @param review
     * @param totalValueMoney
     * @param topdishimageCount
     * @param totalPackage
     */
    public ReviewInnerDetailsPojo(String hotelId, String googleId, String hotelName, String placeId, String openTimes, String address, String latitude, String longitude, String phone, String photoReference, String totalReview, String totalFoodExprience, String totalAmbiance, String totalTaste, String totalService, String totalPackage, String totalTimedelivery, String totalValueMoney, String totalGood, String totalBad, String totalGoodBadUser, List<DetailedAmbienceImagePojo> ambiImage, Integer ambiImageCount, List<DetailedTopdishImagePojo> topdishimage, Integer topdishimageCount, String topdish, Integer topCount, List<DetailedAvoiddishimagePojo> avoiddishimage, Integer avoiddishimageCount, String avoiddish, Integer avoidCount, List<DetailedReviewPojo> review, Integer reviewCount) {
        super();
        this.hotelId = hotelId;
        this.googleId = googleId;
        this.hotelName = hotelName;
        this.placeId = placeId;
        this.openTimes = openTimes;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
        this.photoReference = photoReference;
        this.totalReview = totalReview;
        this.totalFoodExprience = totalFoodExprience;
        this.totalAmbiance = totalAmbiance;
        this.totalTaste = totalTaste;
        this.totalService = totalService;
        this.totalPackage = totalPackage;
        this.totalTimedelivery = totalTimedelivery;
        this.totalValueMoney = totalValueMoney;
        this.totalGood = totalGood;
        this.totalBad = totalBad;
        this.totalGoodBadUser = totalGoodBadUser;
        this.ambiImage = ambiImage;
        this.ambiImageCount = ambiImageCount;
        this.topdishimage = topdishimage;
        this.topdishimageCount = topdishimageCount;
        this.topdish = topdish;
        this.topCount = topCount;
        this.avoiddishimage = avoiddishimage;
        this.avoiddishimageCount = avoiddishimageCount;
        this.avoiddish = avoiddish;
        this.avoidCount = avoidCount;
        this.review = review;
        this.reviewCount = reviewCount;
    }

    public List<PackImage> getPackImage() {
        return packImage;
    }

    public void setPackImage(List<PackImage> packImage) {
        this.packImage = packImage;
    }

    public Integer getPackImageCount() {
        return packImageCount;
    }

    public void setPackImageCount(Integer packImageCount) {
        this.packImageCount = packImageCount;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(String openTimes) {
        this.openTimes = openTimes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public String getTotalReview() {
        return totalReview;
    }

    public void setTotalReview(String totalReview) {
        this.totalReview = totalReview;
    }

    public String getTotalFoodExprience() {
        return totalFoodExprience;
    }

    public void setTotalFoodExprience(String totalFoodExprience) {
        this.totalFoodExprience = totalFoodExprience;
    }

    public String getTotalAmbiance() {
        return totalAmbiance;
    }

    public void setTotalAmbiance(String totalAmbiance) {
        this.totalAmbiance = totalAmbiance;
    }

    public String getTotalTaste() {
        return totalTaste;
    }

    public void setTotalTaste(String totalTaste) {
        this.totalTaste = totalTaste;
    }

    public String getTotalService() {
        return totalService;
    }

    public void setTotalService(String totalService) {
        this.totalService = totalService;
    }

    public String getTotalPackage() {
        return totalPackage;
    }

    public void setTotalPackage(String totalPackage) {
        this.totalPackage = totalPackage;
    }

    public String getTotalTimedelivery() {
        return totalTimedelivery;
    }

    public void setTotalTimedelivery(String totalTimedelivery) {
        this.totalTimedelivery = totalTimedelivery;
    }

    public String getTotalValueMoney() {
        return totalValueMoney;
    }

    public void setTotalValueMoney(String totalValueMoney) {
        this.totalValueMoney = totalValueMoney;
    }

    public String getTotalGood() {
        return totalGood;
    }

    public void setTotalGood(String totalGood) {
        this.totalGood = totalGood;
    }

    public String getTotalBad() {
        return totalBad;
    }

    public void setTotalBad(String totalBad) {
        this.totalBad = totalBad;
    }

    public String getTotalGoodBadUser() {
        return totalGoodBadUser;
    }

    public void setTotalGoodBadUser(String totalGoodBadUser) {
        this.totalGoodBadUser = totalGoodBadUser;
    }

    public List<DetailedAmbienceImagePojo> getAmbiImage() {
        return ambiImage;
    }

    public void setAmbiImage(List<DetailedAmbienceImagePojo> ambiImage) {
        this.ambiImage = ambiImage;
    }

    public Integer getAmbiImageCount() {
        return ambiImageCount;
    }

    public void setAmbiImageCount(Integer ambiImageCount) {
        this.ambiImageCount = ambiImageCount;
    }

    public List<DetailedTopdishImagePojo> getTopdishimage() {
        return topdishimage;
    }

    public void setTopdishimage(List<DetailedTopdishImagePojo> topdishimage) {
        this.topdishimage = topdishimage;
    }

    public Integer getTopdishimageCount() {
        return topdishimageCount;
    }

    public void setTopdishimageCount(Integer topdishimageCount) {
        this.topdishimageCount = topdishimageCount;
    }

    public String getTopdish() {
        return topdish;
    }

    public void setTopdish(String topdish) {
        this.topdish = topdish;
    }

    public Integer getTopCount() {
        return topCount;
    }

    public void setTopCount(Integer topCount) {
        this.topCount = topCount;
    }

    public List<DetailedAvoiddishimagePojo> getAvoiddishimage() {
        return avoiddishimage;
    }

    public void setAvoiddishimage(List<DetailedAvoiddishimagePojo> avoiddishimage) {
        this.avoiddishimage = avoiddishimage;
    }

    public Integer getAvoiddishimageCount() {
        return avoiddishimageCount;
    }

    public void setAvoiddishimageCount(Integer avoiddishimageCount) {
        this.avoiddishimageCount = avoiddishimageCount;
    }

    public String getAvoiddish() {
        return avoiddish;
    }

    public void setAvoiddish(String avoiddish) {
        this.avoiddish = avoiddish;
    }

    public Integer getAvoidCount() {
        return avoidCount;
    }

    public void setAvoidCount(Integer avoidCount) {
        this.avoidCount = avoidCount;
    }

    public List<DetailedReviewPojo> getReview() {
        return review;
    }

    public void setReview( List<DetailedReviewPojo> review) {
        this.review = review;
    }

    public Integer getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getBucket_list() {
        return bucket_list;
    }

    public void setBucket_list(String bucket_list) {
        this.bucket_list = bucket_list;
    }
}
