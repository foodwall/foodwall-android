package com.kaspontech.foodwall.reviewpackage.Review_adapter_folder;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_adapter.SelectIndividualAdapter;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.Slide_ambience_adapter;
import com.kaspontech.foodwall.bucketlistpackage.CreateBucketlistActivty;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.CreateEditEventsComments;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersOutput;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_ambience_image_all;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_ambience_image_pojo_output;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_all_input;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_all_output;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_image_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_hotel_all_inner_review_pojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.reviewpackage.ReviewsLikesAllActivity;
import com.kaspontech.foodwall.reviewpackage.alldishtab.AllDishDisplayActivity;
import com.kaspontech.foodwall.reviewpackage.Review_in_detail_activity;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
//
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static java.lang.Integer.parseInt;

//

public class Get_all_hotel_review_adapter extends RecyclerView.Adapter<Get_all_hotel_review_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;

//    private ArrayList<Getall_hotel_review_pojo> getallHotelReviewPojoArrayList = new ArrayList<>();
    private ArrayList<Get_hotel_all_inner_review_pojo> getallHotelReviewPojoArrayList = new ArrayList<>();

    //Top dish list
    private ArrayList<Get_dish_type_all_input> getDishTypeAllInputslist = new ArrayList<>();



    private ArrayList<Get_ambience_image_all> getAmbienceImageAllArrayList = new ArrayList<>();



    private ArrayList<Get_dish_type_image_pojo> getDishTypeImagePojoArrayList = new ArrayList<>();
    private ArrayList<Get_dish_type_image_pojo> getDishTypeImagePojoArrayListworst = new ArrayList<>();

    private Get_dish_type_all_input getDishTypeAllInput;


    private Get_dish_type_image_pojo getDishTypeImagePojo;

    //Dish Image
    String images,photoId;


    private Getall_hotel_review_pojo getallHotelReviewPojo;
    private Get_hotel_all_inner_review_pojo getHotelAllInnerReviewPojo;

    //Top dish Adapter

    Review_top_dish_adapter reviewTopDishAdapter;
    Review_worst_dish_adapter reviewWorstDishAdapter;
    Ambiance_images_adapter ambianceImagesAdapter;

    Slide_ambience_adapter slideAmbienceAdapter;



    private final String[] itemsothers = {/*"Edit review", */"Bucket list options","Delete review"};
    private Pref_storage pref_storage;

    ApiInterface apiInterface;
    int post_type;


    RecyclerView follwersRecylerview;
    Toolbar selectActionBar;
    List<GetFollowersData> getFollowersDataList = new ArrayList<>();
        List<Integer> friendsList = new ArrayList<>();
    TextView selectDone, selectClose;

    SelectIndividualAdapter selectIndividualAdapter;

    Call<CommonOutputPojo> call;

    private static int revcurrentPage = 0;
    private static int revNUM_PAGES = 0;

    boolean liked = false;

    public Get_all_hotel_review_adapter(Context c, ArrayList<Get_hotel_all_inner_review_pojo> gettinglist) {
        this.context = c;
        this.getallHotelReviewPojoArrayList = gettinglist;
    }


    @NonNull
    @Override
    public Get_all_hotel_review_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_hotel_review_all, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Get_all_hotel_review_adapter.MyViewHolder vh = new Get_all_hotel_review_adapter.MyViewHolder(v);

        pref_storage = new Pref_storage();

        String userid= Pref_storage.getDetail(context,"userId");

        getFollowers(Integer.parseInt(userid));


        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Get_all_hotel_review_adapter.MyViewHolder holder, final int position) {


        getHotelAllInnerReviewPojo = getallHotelReviewPojoArrayList.get(position);

        //reviewer_name
        String reviewer_fname = getallHotelReviewPojoArrayList.get(position).getFirstName();
        String reviewer_lname=     getallHotelReviewPojoArrayList.get(position).getLastName();
        holder.username.setText(reviewer_fname);
        holder.userlastname.setText(reviewer_lname);
        holder.username.setTextColor(Color.parseColor("#000000"));
        holder.userlastname.setTextColor(Color.parseColor("#000000"));

        //txt_hotelname
        holder.tv_restaurant_name.setText(getallHotelReviewPojoArrayList.get(position).getHotelName());
        //hotel_address
      /*  String hotel_address = getallHotelReviewPojoArrayList.get(position).getAddress();
        holder.txt_hotel_address.setText(hotel_address);
        holder.txt_hotel_address.setTextColor(Color.parseColor("#000000"));*/




        //Reviewer Image

        Utility.picassoImageLoader(getallHotelReviewPojoArrayList.get(position).getPicture(),
                0,holder.userphoto_profile,context );
//        GlideApp.with(context)
//                .load(getallHotelReviewPojoArrayList.get(position).getPicture())
//                .into(holder.userphoto_profile);

          //Reviewer Image

        Utility.picassoImageLoader(Pref_storage.getDetail(context,"picture_user_login"),
                0,holder.review_user_photo, context);

//        GlideApp.with(context)
//                .load(Pref_storage.getDetail(context,"picture_user_login"))
//                .into(holder.review_user_photo);



        // Timestamp for review
        String createdon = getHotelAllInnerReviewPojo.getCreatedOn();


        try {
            Log.e("created_on", "-->"+createdon );

            long now = System.currentTimeMillis();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date convertedDate = dateFormat.parse(createdon);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now,

                    DateUtils.FORMAT_ABBREV_RELATIVE);
            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                holder.txt_created_on.setText(R.string.just_now);
            } else {
                holder.txt_created_on.append(relavetime1 + "\n\n");

            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Display top dishes

        if(getallHotelReviewPojoArrayList.get(position).getTopdish()==null){
            holder.tv_top_dishes.setVisibility(GONE);
        }else {

            holder.tv_top_dishes.setText(getallHotelReviewPojoArrayList.get(position).getTopdish().replace(",",", "));

        }
         //Display avoid dishes

        if(getallHotelReviewPojoArrayList.get(position).getAvoiddish()==null){
            holder.tv_dishes_to_avoid.setVisibility(GONE);
        }else {

            holder.tv_dishes_to_avoid.setText(getallHotelReviewPojoArrayList.get(position).getAvoiddish().replace(",",", "));

        }


        //Ambiance display

/*

          //Hotel Image
        Image_load_activity.loadGooglePhoto(context,holder.img_foodimage,getallHotelReviewPojoArrayList.get(position).getPhotoReference());

*/

        //Edit text comment

        holder.review_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (holder.review_comment.getText().toString().length() != 0) {
                    holder.txt_adapter_post.setVisibility(VISIBLE);
                } else {
                    holder.txt_adapter_post.setVisibility(GONE);
                }


            }
        });

        holder.txt_adapter_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {
                    String createuserid = Pref_storage.getDetail(context, "userId");

                    Call<CreateEditEventsComments> call = apiService.create_edit_hotel_review_comments("create_edit_hotel_review_comments", 0, Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getHotelId()), Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getHotelId()), holder.review_comment.getText().toString(),Integer.parseInt(createuserid));
                    call.enqueue(new Callback<CreateEditEventsComments>() {
                        @Override
                        public void onResponse(Call<CreateEditEventsComments> call, Response<CreateEditEventsComments> response) {

                            if (response.body().getResponseCode() == 1) {


                                int tot_comments= Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getTotalComments());
                                if(tot_comments==0||getallHotelReviewPojoArrayList.get(position).getTotalComments()==null){
                                    holder.tv_cmt_count.setText("1"+" comment");
//                                    holder.txt_review_comment.setText(R.string.commenttt);
                                    holder.tv_cmt_count.setVisibility(View.VISIBLE);
//                                    holder.txt_review_comment_count.setVisibility(View.VISIBLE);
                                } else if (tot_comments>=1) {

                                    int inc_cmnt= tot_comments+1;
                                    holder.tv_cmt_count.setText(String.valueOf(inc_cmnt).concat(" "+" comments"));
//                                    holder.txt_review_comment.setText(R.string.comments);
                                    holder.tv_cmt_count.setVisibility(View.VISIBLE);
//                                    holder.txt_review_comment_count.setVisibility(View.VISIBLE);

                                }
                                holder.review_comment.setText("");
                                holder.txt_adapter_post.setVisibility(GONE);

                            }

                        }

                        @Override
                        public void onFailure(Call<CreateEditEventsComments> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        //Dishes to avoid text count


        int dish_avoid= getallHotelReviewPojoArrayList.get(position).getAvoidCount();

        if (dish_avoid==0){
            holder.ll_dish_to_avoid.setVisibility(View.GONE);
        }




        //Overallrating
//        String rating = getallHotelReviewPojoArrayList.get(position).getTotalFoodExprience();
        String rating = getallHotelReviewPojoArrayList.get(position).getFoodExprience();

        holder.tv_res_overall_rating.setText(rating);
        if(Integer.parseInt(rating)==1){
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_red);
//            holder.img_overall_rating_star.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_review_rating_red));
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background) );
//            holder.img_overall_rating_star.setBackgroundResource(R.drawable.ic_review_rating_red);

        }else if(Integer.parseInt(rating)==2){
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.ll_res_overall_rating.setBackgroundResource(R.drawable.review_rating_orange_bg);
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_orange_star);
        }else if(Integer.parseInt(rating)==3){
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg) );
        }else if(Integer.parseInt(rating)==4){
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg) );
        }else if(Integer.parseInt(rating)==5){
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg) );
        }


        try {
//Time delivery
            if (Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getCategoryType()) == 2) {

                String time_deleivery = getallHotelReviewPojoArrayList.get(position).getTotalTimedelivery();
                Log.e("time_deleivery", "time_deleivery: "+time_deleivery );
                holder.ambi_or_timedelivery.setText(R.string.Delivery_new);
                holder.tv_ambience_rating.setText(time_deleivery);

                if (Integer.parseInt(time_deleivery) == 1) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_red);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

                } else if (Integer.parseInt(time_deleivery) == 2) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                } else if (Integer.parseInt(time_deleivery) == 3) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                } else if (Integer.parseInt(time_deleivery) == 4) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_star);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                } else if (Integer.parseInt(time_deleivery) == 5) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                }

            } else {

                //Ambiance
                String ambiance = getallHotelReviewPojoArrayList.get(position).getAmbiance();

                holder.tv_ambience_rating.setText(ambiance);

                if (Integer.parseInt(ambiance) == 1) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_red);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

                } else if (Integer.parseInt(ambiance) == 2) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                } else if (Integer.parseInt(ambiance) == 3) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                } else if (Integer.parseInt(ambiance) == 4) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_star);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                } else if (Integer.parseInt(ambiance) == 5) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                }

            }

        }catch (Exception e){
            e.printStackTrace();
        }


  //Taste

        String taste = getallHotelReviewPojoArrayList.get(position).getTaste();

        holder.txt_taste_count.setText(taste);

        if(Integer.parseInt(taste)==1){
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_red);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

        }else if(Integer.parseInt(taste)==2){
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_orange_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
        }else if(Integer.parseInt(taste)==3){
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
        }else if(Integer.parseInt(taste)==4){
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
        }else if(Integer.parseInt(taste)==5){
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
        }



  //Value for money

        String vfm = getallHotelReviewPojoArrayList.get(position).getValueMoney();

        holder.tv_vfm_rating.setText(vfm);

        if(Integer.parseInt(vfm)==1){
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_red);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
        }else if(Integer.parseInt(vfm)==2){
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_orange_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
        }else if(Integer.parseInt(vfm)==3){
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
        }else if(Integer.parseInt(vfm)==4){
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
        }else if(Integer.parseInt(vfm)==5){
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
        }



        try{
//Packaging

        if(Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getCategoryType())==2){

            String package_rating = getallHotelReviewPojoArrayList.get(position).get_package();
            holder.tv_service_rating.setText(package_rating);
            holder.serv_or_package.setText(R.string.Package);

            if(Integer.parseInt(package_rating)==1){
                holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_service_star.setImageResource(R.drawable.ic_review_rating_red);
                holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
            }else if(Integer.parseInt(package_rating)==2){
                holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_service_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
            }else if(Integer.parseInt(package_rating)==3){
                holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_service_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
            }else if(Integer.parseInt(package_rating)==4){
                holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_service_star.setImageResource(R.drawable.ic_review_rating_star);

                holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
            }else if(Integer.parseInt(package_rating)==5){
                holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_service_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
            }



        }else {
            //Service

            String service = getallHotelReviewPojoArrayList.get(position).getService();

            holder.tv_service_rating.setText(service);
            if(Integer.parseInt(service)==1){
                holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_service_star.setImageResource(R.drawable.ic_review_rating_red);
                holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
            }else if(Integer.parseInt(service)==2){
                holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_service_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
            }else if(Integer.parseInt(service)==3){
                holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_service_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
            }else if(Integer.parseInt(service)==4){
                holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_service_star.setImageResource(R.drawable.ic_review_rating_star);

                holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
            }else if(Integer.parseInt(service)==5){
                holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                holder.img_service_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
            }


        }
    }catch (Exception e){
        e.printStackTrace();
    }





//        holder.rtng_hotel.setRating(Float.parseFloat(rating));


 /*       //rtng_user_review
        String rtng_user_review = getallHotelReviewPojoArrayList.get(position).getFoodExprience();
        Log.e("food_rating", "rtng_user_review" + rtng_user_review);
        holder.rtng_user_review.setRating(Float.parseFloat(rtng_user_review));*/

        String total_reviews = getallHotelReviewPojoArrayList.get(position).getTotalReview();
        String total_followers = getallHotelReviewPojoArrayList.get(position).getTotalFollowers();

      /*  //Followers display
        if(Integer.parseInt(total_followers)!=0&& Integer.parseInt(total_followers)==1){
            holder.comma.setVisibility(VISIBLE);
            holder.txt_followers_count.setText(total_followers);
            holder.txt_followers_count.setVisibility(VISIBLE);
            holder.txt_followers.setVisibility(VISIBLE);
        }else if(Integer.parseInt(total_followers)!=0&& Integer.parseInt(total_followers)>1){

            holder.comma.setVisibility(VISIBLE);
            holder.txt_followers_count.setText(total_followers);
            holder.txt_followers.setText(R.string.followers);
            holder.txt_followers_count.setVisibility(VISIBLE);
            holder.txt_followers.setVisibility(VISIBLE);
        }

        if (parseInt(total_reviews) == 1) {
                holder.count.setText(total_reviews.concat(" Review"));
        } else if (parseInt(total_reviews) > 1) {
            holder.count.setText(total_reviews.concat(" Reviews"));
        }*/



        //Review comment
        if(getallHotelReviewPojoArrayList.get(position).getHotelReview().equalsIgnoreCase("0")){
            holder.reviewcmmnt.setVisibility(GONE);
        }else {
            holder.reviewcmmnt.setText(getallHotelReviewPojoArrayList.get(position).getHotelReview());

        }


        //Total likes

        final String total_likes = getallHotelReviewPojoArrayList.get(position).getTotalLikes();

         int userliked= Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getHtlRevLikes());

        if (userliked==0){
            getallHotelReviewPojoArrayList.get(position).setUserliked(false);
            holder.img_btn_review_like.setVisibility(View.VISIBLE);
            holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_gray));
        }else if(userliked==1){
            getallHotelReviewPojoArrayList.get(position).setUserliked(true);
            holder.img_btn_review_like.setVisibility(View.VISIBLE);
            holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
        }



        if (parseInt(total_likes) == 0 || total_likes.equalsIgnoreCase(null) || total_likes.equalsIgnoreCase("")) {

            holder.tv_like_count.setVisibility(View.GONE);
       }
        if (parseInt(total_likes) == 1) {
            holder.tv_like_count.setText(getallHotelReviewPojoArrayList.get(position).getTotalLikes().concat(" ").concat("like"));
            holder.tv_like_count.setVisibility(View.VISIBLE);


        } else if (parseInt(total_likes) > 1) {
            holder.tv_like_count.setText(getallHotelReviewPojoArrayList.get(position).getTotalLikes().concat(" ").concat("likes"));
            holder.tv_like_count.setVisibility(View.VISIBLE);
        }

        //Total comments

        String total_comments = getallHotelReviewPojoArrayList.get(position).getTotalComments();

        if (parseInt(total_comments) == 0 || total_likes.equalsIgnoreCase(null) || total_likes.equalsIgnoreCase("")) {
            holder.tv_cmt_count.setVisibility(View.GONE);
            holder.tv_view_all_comments.setVisibility(View.GONE);
//            holder.ll_likecomment.setVisibility(View.GONE);
//            holder.txt_review_comment_count.setVisibility(View.GONE);
        }
        if (parseInt(total_comments) == 1) {
            holder.tv_cmt_count.setText(getallHotelReviewPojoArrayList.get(position).getTotalComments().concat(" ").concat("comment"));
            holder.tv_view_all_comments.setText(R.string.view_one_comment);
            holder.tv_view_all_comments.setVisibility(View.VISIBLE);
            holder.tv_cmt_count.setVisibility(View.VISIBLE);
//            holder.ll_likecomment.setVisibility(View.VISIBLE);

//            holder.txt_review_comment_count.setVisibility(View.VISIBLE);

        } else if (parseInt(total_comments) > 1) {
            holder.tv_cmt_count.setText(getallHotelReviewPojoArrayList.get(position).getTotalComments().concat(" ").concat("comments"));
//            holder.txt_review_comment.setText(R.string.comments);
            holder.tv_view_all_comments.setText("View all "+total_comments+" comments");
            holder.tv_view_all_comments.setVisibility(View.VISIBLE);
            holder.tv_cmt_count.setVisibility(View.VISIBLE);
//            holder.ll_likecomment.setVisibility(View.VISIBLE);
//            holder.txt_review_comment_count.setVisibility(View.VISIBLE);
        }


        holder.tv_view_all_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent= new Intent(context, Reviews_comments_all_activity.class);
                intent.putExtra("hotelid",getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("reviewid",getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("userid",getallHotelReviewPojoArrayList.get(position).getUserId());
                context.startActivity(intent);
            }
        });

 holder.tv_cmt_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent= new Intent(context, Reviews_comments_all_activity.class);
                intent.putExtra("hotelid",getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("reviewid",getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("userid",getallHotelReviewPojoArrayList.get(position).getUserId());
                context.startActivity(intent);
            }
        });


    holder.tv_like_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent= new Intent(context, ReviewsLikesAllActivity.class);

                intent.putExtra("hotelid",getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("reviewid",getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("userid",getallHotelReviewPojoArrayList.get(position).getUserId());
                context.startActivity(intent);
            }
        });





holder.img_btn_review_comment.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


        Intent intent= new Intent(context, Reviews_comments_all_activity.class);

        intent.putExtra("hotelid",getallHotelReviewPojoArrayList.get(position).getHotelId());
        intent.putExtra("reviewid",getallHotelReviewPojoArrayList.get(position).getRevratId());
        intent.putExtra("userid",getallHotelReviewPojoArrayList.get(position).getUserId());
        context.startActivity(intent);

    }
});


holder.username.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


        String user_id = getallHotelReviewPojoArrayList.get(position).getUserId();

        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
            context.startActivity(new Intent(context, Profile.class));
        } else {

            Intent intent = new Intent(context, User_profile_Activity.class);
            intent.putExtra("created_by", user_id);
            context.startActivity(intent);

        }

    }
});
holder.userlastname.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


        String user_id = getallHotelReviewPojoArrayList.get(position).getUserId();

        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
            context.startActivity(new Intent(context, Profile.class));
        } else {

            Intent intent = new Intent(context, User_profile_Activity.class);
            intent.putExtra("created_by", user_id);
            context.startActivity(intent);

        }

    }
});

holder.userphoto_profile.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        String user_id = getallHotelReviewPojoArrayList.get(position).getUserId();

        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
            context.startActivity(new Intent(context, Profile.class));
        } else {

            Intent intent = new Intent(context, User_profile_Activity.class);
            intent.putExtra("created_by", user_id);
            context.startActivity(intent);

        }

    }
});
holder.review_user_photo.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        String user_id = getallHotelReviewPojoArrayList.get(position).getUserId();

        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
            context.startActivity(new Intent(context, Profile.class));
        } else {

            Intent intent = new Intent(context, User_profile_Activity.class);
            intent.putExtra("created_by", user_id);
            context.startActivity(intent);

        }
    }
});


holder.rl_overall_review.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


        Intent intent= new Intent(context, Review_in_detail_activity.class);

        intent.putExtra("hotelid",getallHotelReviewPojoArrayList.get(position).getHotelId());
        intent.putExtra("reviewid",getallHotelReviewPojoArrayList.get(position).getRevratId());
/*        intent.putExtra("googleid",getallHotelReviewPojoArrayList.get(position).getGoogleId());
        intent.putExtra("placeid",getallHotelReviewPojoArrayList.get(position).getPlaceId());
        intent.putExtra("userid",getallHotelReviewPojoArrayList.get(position).getUserId());
        intent.putExtra("Hotel_name",getallHotelReviewPojoArrayList.get(position).getHotelName());
        intent.putExtra("Hotel_Review",getallHotelReviewPojoArrayList.get(position).getHotelReview());
        intent.putExtra("Value_money",getallHotelReviewPojoArrayList.get(position).getValueMoney());
        intent.putExtra("Taste",getallHotelReviewPojoArrayList.get(position).getTaste());
        intent.putExtra("Service",getallHotelReviewPojoArrayList.get(position).getService());
        intent.putExtra("Ambiance",getallHotelReviewPojoArrayList.get(position).getAmbiance());
        intent.putExtra("Address",getallHotelReviewPojoArrayList.get(position).getAddress());
        intent.putExtra("Picture",getallHotelReviewPojoArrayList.get(position).getPicture());
        intent.putExtra("photo_reference",getallHotelReviewPojoArrayList.get(position).getPhotoReference());
        intent.putExtra("cate_type",getallHotelReviewPojoArrayList.get(position).getCategoryType());
        intent.putExtra("food_exp",getallHotelReviewPojoArrayList.get(position).getFoodExprience());
        intent.putExtra("total_reviews",getallHotelReviewPojoArrayList.get(position).getTotalReview());
        intent.putExtra("total_likes",getallHotelReviewPojoArrayList.get(position).getTotalLikes());
        intent.putExtra("total_comments",getallHotelReviewPojoArrayList.get(position).getTotalComments());
        intent.putExtra("f_name",getallHotelReviewPojoArrayList.get(position).getFirstName());
        intent.putExtra("l_name",getallHotelReviewPojoArrayList.get(position).getLastName());
        intent.putExtra("veg_nonveg",getallHotelReviewPojoArrayList.get(position).getVegNonveg());
        intent.putExtra("totalAmbiance",getallHotelReviewPojoArrayList.get(position).getTotalAmbiance());
        intent.putExtra("totalTaste",getallHotelReviewPojoArrayList.get(position).getTotalTaste());
        intent.putExtra("totalService",getallHotelReviewPojoArrayList.get(position).getTotalService());
        intent.putExtra("totalValueMoney",getallHotelReviewPojoArrayList.get(position).getTotalValueMoney());
        intent.putExtra("totalGood",getallHotelReviewPojoArrayList.get(position).getTotalGood());
        intent.putExtra("totalBad",getallHotelReviewPojoArrayList.get(position).getTotalBad());
        intent.putExtra("totalGoodBadUser",getallHotelReviewPojoArrayList.get(position).getTotalGoodBadUser());*/
        context.startActivity(intent);

    }
});






//Like functionality

        holder.img_btn_review_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            if (getallHotelReviewPojoArrayList.get(position).isUserliked()==true){


                try {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);
                     apiInterface= ApiClient.getClient().create(ApiInterface.class);
                    int userid = parseInt(Pref_storage.getDetail(context, "userId"));

                    call= apiInterface.create_hotel_review_likes("create_hotel_review_likes", parseInt(getallHotelReviewPojoArrayList.get(position).getHotelId()), parseInt(getallHotelReviewPojoArrayList.get(position).getRevratId()),0, userid);

                    call.enqueue(new Callback<CommonOutputPojo>() {
                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                            String responseStatus = response.body().getResponseMessage();
                            int resposecode = response.body().getResponseCode();

                            Log.e("responseStatus", "responseStatus->" + responseStatus);
                            Log.e("resposecode", "resposecode->" + resposecode);

                            if (resposecode==1) {

                                //Success
                                holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_gray));
                                getallHotelReviewPojoArrayList.get(position).setUserliked(false);
                                liked = false;


                                int likedecrement= parseInt(getallHotelReviewPojoArrayList.get(position).getTotalLikes());
                                Log.e("dis_likes", "dis_likes->" + likedecrement);
                                if (likedecrement == 0) {
                                    holder.tv_like_count.setVisibility(View.GONE);
                                }  if (likedecrement == 1) {
                                    holder.tv_like_count.setVisibility(View.GONE);
                                } else if (likedecrement > 1) {
                                    int likedecre = (likedecrement-1);
                                    holder.tv_like_count.setText(String.valueOf(likedecre).concat(" ").concat("likes"));
                                    holder.tv_like_count.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "FailureError" + t.getMessage());
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {

                try {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);
                    apiInterface= ApiClient.getClient().create(ApiInterface.class);

                    int userid = parseInt(Pref_storage.getDetail(context, "userId"));

                    Call<CommonOutputPojo> call = apiInterface.create_hotel_review_likes("create_hotel_review_likes", parseInt(getallHotelReviewPojoArrayList.get(position).getHotelId()), parseInt(getallHotelReviewPojoArrayList.get(position).getRevratId()),1, userid);

                    call.enqueue(new Callback<CommonOutputPojo>() {
                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                            String responseStatus = response.body().getResponseMessage();

                            Log.e("responseStatus", "responseStatus->" + responseStatus);

                            if (responseStatus.equals("success")) {

                                //Success
                                holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                                getallHotelReviewPojoArrayList.get(position).setUserliked(true);
                                liked = true;

//                                new LongOperation_like().execute("");

                                int likedecrement= parseInt(getallHotelReviewPojoArrayList.get(position).getTotalLikes());
                                Log.e("likedincrement", "likedincrement->" + likedecrement);



                                if (likedecrement == 0) {
                                    int incre =likedecrement + 1;
                                    holder.tv_like_count.setText(String.valueOf(incre).concat(" ").concat("like"));
                                    holder.tv_like_count.setVisibility(View.VISIBLE);
                                } else if (likedecrement == 1) {
                                    int incre1 =likedecrement;
                                    holder.tv_like_count.setText(String.valueOf(likedecrement).concat(" ").concat("like"));
                                    holder.tv_like_count.setVisibility(View.VISIBLE);
                                } else if (likedecrement > 1) {
                                    int incre2 = likedecrement + 1;
                                    holder.tv_like_count.setText(String.valueOf(incre2).concat(" ").concat("likes"));
                                    holder.tv_like_count.setVisibility(View.VISIBLE);

                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "FailureError" + t.getMessage());
                        }
                    });

                } catch (Exception e) {

                    e.printStackTrace();

                }

            }




            }
        });


    //More settings visibility


        if (getallHotelReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context,"userId"))){
            holder.img_view_more.setVisibility(VISIBLE);
        }else{
            holder.img_view_more.setVisibility(GONE);
        }

//More settings
        holder.img_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder front_builder = new AlertDialog.Builder(context);
                front_builder.setItems(itemsothers, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (itemsothers[which].equals("Bucket list options")) {

                            dialog.dismiss();

                            Intent intent= new Intent(context, CreateBucketlistActivty.class);
                            intent.putExtra("f_name",getallHotelReviewPojoArrayList.get(position).getFirstName());
                            intent.putExtra("l_name",getallHotelReviewPojoArrayList.get(position).getLastName());
                            intent.putExtra("hotelid",getallHotelReviewPojoArrayList.get(position).getHotelId());
                            intent.putExtra("reviewid",getallHotelReviewPojoArrayList.get(position).getRevratId());
                            context.startActivity(intent);

                        } else if (itemsothers[which].equals("Delete review")) {
                            new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Are you sure want to delete this reviewed hotel?")
                                    .setContentText("Won't be able to recover this hotel anymore!")
                                    .setConfirmText("Delete")
                                    .setCancelText("Cancel")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                            try {
                                                String createuserid = Pref_storage.getDetail(context,"userId");

                                                String review_id= getallHotelReviewPojoArrayList.get(position).getRevratId();

                                                Call<CommonOutputPojo> call = apiService.update_delete_hotel_review("update_delete_hotel_review", Integer.parseInt(review_id), Integer.parseInt(createuserid));
                                                call.enqueue(new Callback<CommonOutputPojo>() {
                                                    @Override
                                                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                        if (response.body().getResponseCode() == 1) {

                                                            getallHotelReviewPojoArrayList.remove(position);
                                                            notifyItemRemoved(position);
                                                            notifyItemRangeChanged(position, getallHotelReviewPojoArrayList.size());
                                                            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                                    .setTitleText("Deleted!!")

                                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                        @Override
                                                                        public void onClick(SweetAlertDialog sDialog) {
                                                                            sDialog.dismissWithAnimation();


                                                                        }
                                                                    })
                                                                    .show();


                                                        }

                                                    }

                                                    @Override
                                                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                        //Error
                                                        Log.e("FailureError", "" + t.getMessage());
                                                    }
                                                });
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    })
                                    .show();

                            dialog.dismiss();
                        }
                    }
                });
                front_builder.show();
            }
        });



//Adding bucketlist API
holder.img_bucket_list.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
        v.startAnimation(anim);

        Intent intent= new Intent(context, CreateBucketlistActivty.class);
        intent.putExtra("f_name",getallHotelReviewPojoArrayList.get(position).getFirstName());
        intent.putExtra("l_name",getallHotelReviewPojoArrayList.get(position).getLastName());
        intent.putExtra("reviewid",getallHotelReviewPojoArrayList.get(position).getRevratId());
        intent.putExtra("hotelid",getallHotelReviewPojoArrayList.get(position).getHotelId());

        context.startActivity(intent);


    }
});



//Load all image Activity
holder.rl_view_more_top.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
        v.startAnimation(anim);

        Intent intent= new Intent(context, AllDishDisplayActivity.class);
        intent.putExtra("reviewid",getallHotelReviewPojoArrayList.get(position).getRevratId());
        intent.putExtra("hotelname",getallHotelReviewPojoArrayList.get(position).getHotelName());
        context.startActivity(intent);


    }
});
//Load all image Activity
holder.top_more.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
        v.startAnimation(anim);

        Intent intent= new Intent(context, AllDishDisplayActivity.class);
        intent.putExtra("reviewid",getallHotelReviewPojoArrayList.get(position).getRevratId());
        intent.putExtra("hotelname",getallHotelReviewPojoArrayList.get(position).getHotelName());
        context.startActivity(intent);


    }
});

//Load all image Activity
holder.rl_view_more_avoid.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
        v.startAnimation(anim);

        Intent intent= new Intent(context, AllDishDisplayActivity.class);
        intent.putExtra("reviewid",getallHotelReviewPojoArrayList.get(position).getRevratId());
        intent.putExtra("hotelname",getallHotelReviewPojoArrayList.get(position).getHotelName());
        context.startActivity(intent);


    }
});

//Load all image Activity
        holder.avoid_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);

                Intent intent= new Intent(context, AllDishDisplayActivity.class);
                intent.putExtra("reviewid",getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("hotelname",getallHotelReviewPojoArrayList.get(position).getHotelName());
                context.startActivity(intent);


            }
        });




//API for top dish images

        if(getallHotelReviewPojoArrayList.get(position).getTopCount()!=0){


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");

            Call<Get_dish_type_all_output> call = apiService.get_hotel_dish_type_all("get_hotel_dish_type_all", Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getRevratId()), 1,Integer.parseInt(userId));
            call.enqueue(new Callback<Get_dish_type_all_output>() {
                @Override
                public void onResponse(Call<Get_dish_type_all_output> call, Response<Get_dish_type_all_output> response) {

                    if (response.body().getResponseCode() == 1&&response.body().getResponseMessage().equalsIgnoreCase("success")) {
                        getDishTypeAllInputslist.clear();


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());

                            try {

                                int imageCount = response.body().getData().get(j).getImageCount();

                                String dishname= response.body().getData().get(j).getDishName();



                                images= response.body().getData().get(j).getImage().get(0).getImg();

                                Log.e("images-->", "" +images);

                                photoId= response.body().getData().get(j).getImage().get(0).getPhotoId();

                                Log.e("images-->", "" +photoId);
                                getDishTypeImagePojo = new Get_dish_type_image_pojo(photoId,images);
                                getDishTypeImagePojoArrayList.add(getDishTypeImagePojo);


                                /*for (int i=0;i<response.body().getData().size();i++){
                                         images= response.body().getData().get(j).getImage().get(0).getImg();

                                        Log.e("images-->", "" +images);

                                        photoId= response.body().getData().get(j).getImage().get(0).getPhotoId();

                                        Log.e("images-->", "" +photoId);

                                        getDishTypeImagePojo = new Get_dish_type_image_pojo(photoId,images);
                                        getDishTypeImagePojoArrayList.add(getDishTypeImagePojo);

                                    }
*/




                                Get_dish_type_all_input getDishTypeAllInput =  new Get_dish_type_all_input("", "", "", dishname,"",
                                        "","","","","","","",
                                        "","","","","","","","",
                                        "","","","","","","","","","","",
                                        getDishTypeImagePojoArrayList,imageCount);

                                getDishTypeAllInputslist.add(getDishTypeAllInput);


                                reviewTopDishAdapter = new Review_top_dish_adapter(context, getDishTypeAllInputslist,getDishTypeImagePojoArrayList);

                                RecyclerView.LayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);
                                holder.rv_review_top_dish.setLayoutManager(llm);
//                                Collections.reverse(getDishTypeAllInputslist);
                                holder. rv_review_top_dish.setItemAnimator(new DefaultItemAnimator());
                                holder.rv_review_top_dish.setAdapter(reviewTopDishAdapter);
                                holder. rv_review_top_dish.setNestedScrollingEnabled(false);
                                reviewTopDishAdapter.notifyDataSetChanged();
                                holder.rl_top_dish.setVisibility(VISIBLE);

//                                holder.tv_top_dishes.setText(getDishTypeAllInputslist.get(position).getDishName());

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }  if (response.body().getResponseMessage().equalsIgnoreCase("nodata")){
/*  txt_no_top_dish.setVisibility(View.VISIBLE);
                    img_no_top_dish.setVisibility(View.VISIBLE);*/

                    }

                }

                @Override
                public void onFailure(Call<Get_dish_type_all_output> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
/*txt_no_top_dish.setVisibility(View.VISIBLE);
                img_no_top_dish.setVisibility(View.VISIBLE);*/

                }
            });

        }

//Dish to avoid names API

        if (getallHotelReviewPojoArrayList.get(position).getAvoidCount()!=0){
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");

            Call<Get_dish_type_all_output> call = apiService.get_hotel_dish_type_all("get_hotel_dish_type_all", Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getRevratId()), 2,Integer.parseInt(userId));
            call.enqueue(new Callback<Get_dish_type_all_output>() {
                @Override
                public void onResponse(Call<Get_dish_type_all_output> call, Response<Get_dish_type_all_output> response) {

                    if (response.body().getResponseCode() == 1&&response.body().getResponseMessage().equalsIgnoreCase("success")) {
                        getDishTypeAllInputslist.clear();


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());

                            try {

                                int imageCount = response.body().getData().get(j).getImageCount();

                                String dishname= response.body().getData().get(j).getDishName();

//                            String images= response.body().getData().get(j).getImage().get(j).getImg();
                                images= response.body().getData().get(j).getImage().get(0).getImg();

                                Log.e("images-->", "" +images);


                                photoId= response.body().getData().get(j).getImage().get(0).getPhotoId();
                                Log.e("images-->", "" +photoId);


                                getDishTypeImagePojo = new Get_dish_type_image_pojo(photoId,images);
                                getDishTypeImagePojoArrayListworst.add(getDishTypeImagePojo);

                        /*        for (int i=0;i<response.body().getData().size();i++){
                                    images= response.body().getData().get(j).getImage().get(0).getImg();

                                    Log.e("images-->", "" +images);

                                    photoId= response.body().getData().get(j).getImage().get(0).getPhotoId();

                                    Log.e("images-->", "" +photoId);


                                    getDishTypeImagePojo = new Get_dish_type_image_pojo(photoId,images);
                                    getDishTypeImagePojoArrayListworst.add(getDishTypeImagePojo);

                                }
*/

                                Get_dish_type_all_input getDishTypeAllInput =  new Get_dish_type_all_input("", "", "", dishname,"",
                                        "","","","","","","",
                                        "","","","","","","","",
                                        "","","","","","","","","","","",
                                        getDishTypeImagePojoArrayListworst,imageCount);

                                getDishTypeAllInputslist.add(getDishTypeAllInput);



                                reviewWorstDishAdapter = new Review_worst_dish_adapter(context, getDishTypeAllInputslist,getDishTypeImagePojoArrayListworst);

                                RecyclerView.LayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);

                                holder.rv_review_avoid_dish.setLayoutManager(llm);
//                                Collections.reverse(getDishTypeAllInputslist);
                                holder. rv_review_avoid_dish.setItemAnimator(new DefaultItemAnimator());
                                holder.rv_review_avoid_dish.setAdapter(reviewWorstDishAdapter);
                                holder. rv_review_avoid_dish.setNestedScrollingEnabled(false);
                                reviewWorstDishAdapter.notifyDataSetChanged();
                                holder.rl_avoid_dish.setVisibility(VISIBLE);



//                                holder.tv_top_dishes.setText(getDishTypeAllInputslist.get(position).getDishName());


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }  if (response.body().getResponseMessage().equalsIgnoreCase("nodata")){
/*  txt_no_top_dish.setVisibility(View.VISIBLE);
                    img_no_top_dish.setVisibility(View.VISIBLE);*/

                    }

                }

                @Override
                public void onFailure(Call<Get_dish_type_all_output> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
/*txt_no_top_dish.setVisibility(View.VISIBLE);
                img_no_top_dish.setVisibility(View.VISIBLE);*/

                }
            });

        }


     //API for ambience images

                    ApiInterface apiService2 = ApiClient.getClient().create(ApiInterface.class);
                            try {
                                String review_id= getallHotelReviewPojoArrayList.get(position).getRevratId();

                                Call<Get_ambience_image_pojo_output> call2  = apiService2.get_hotel_review_ambiance_image_all_new(
                                        "get_hotel_review_ambiance_image_all",
                                        Integer.parseInt(review_id)
                                        );

                                call2.enqueue(new Callback<Get_ambience_image_pojo_output>() {
                                    @Override
                                    public void onResponse(Call<Get_ambience_image_pojo_output> call2, Response<Get_ambience_image_pojo_output> response) {

                                        if (response.body().getResponseCode() == 1) {


                                            for(int i=0;i <response.body().getData().size();i++){
                                                String photo_id= response.body().getData().get(i).getPhotoId();
                                                String reviewid= response.body().getData().get(i).getReviewid();
                                                String userid= response.body().getData().get(i).getUserid();
                                                String dishimage= response.body().getData().get(i).getDishimage();

                                                Get_ambience_image_all getAmbienceImageAll= new Get_ambience_image_all(photo_id,reviewid,userid,dishimage);

                                                getAmbienceImageAllArrayList.add(getAmbienceImageAll);

                                                if(getAmbienceImageAllArrayList.size()!=0){
                                                    ambianceImagesAdapter = new Ambiance_images_adapter(context,getAmbienceImageAllArrayList );

                                                    RecyclerView.LayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);

                                                    holder.review_photos_recylerview.setLayoutManager(llm);
//                                Collections.reverse(getDishTypeAllInputslist);
                                                    holder. review_photos_recylerview.setItemAnimator(new DefaultItemAnimator());
                                                    holder.review_photos_recylerview.setAdapter(ambianceImagesAdapter);
                                                    holder. review_photos_recylerview.setNestedScrollingEnabled(false);
                                                    ambianceImagesAdapter.notifyDataSetChanged();
                                                    holder.ll_view_more.setVisibility(VISIBLE);
                                                    holder.ll_view_more.setVisibility(VISIBLE);

                                                }


/*


                                                if(getAmbienceImageAllArrayList.size()==0){
                                                     holder.ambi_viewpager.setVisibility(View.GONE);
                                                     holder.rl_ambiance_dot.setVisibility(View.GONE);
                                                     holder.title_ambience.setVisibility(GONE);
                                                     holder.ll_ambi_more.setVisibility(GONE);
                                                     holder.ll_view_more.setVisibility(GONE);
                                                     holder. review_photos_recylerview.setVisibility(GONE);
                                                 }
                                                 else {


                                                 }
*/




                                            }

                                        } else if (response.body().getData().toString().equalsIgnoreCase("novalue")){
                                            holder.ambi_viewpager.setVisibility(View.GONE);
                                            holder.rl_ambiance_dot.setVisibility(View.GONE);
                                            holder.title_ambience.setVisibility(GONE);
                                            holder.ll_ambi_more.setVisibility(GONE);
                                            holder.ll_view_more.setVisibility(GONE);
                                            holder. review_photos_recylerview.setVisibility(GONE);
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<Get_ambience_image_pojo_output> call, Throwable t) {
                                        //Error
                                        Log.e("FailureError", "" + t.getMessage());
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }





// Hotel Images

        holder.ll_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.ll_ambi_more.setVisibility(VISIBLE);
                holder.ll_view_more.setVisibility(GONE);

            }
        });

        holder.layout_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.ll_ambi_more.setVisibility(VISIBLE);
                holder.ll_view_more.setVisibility(GONE);

            }
        });


    }






//Custom alert dialog for more options

    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity c;
        public Dialog d;

        TextView txt_edit_review, txt_delete_review;

        CustomDialogClass(Context a) {
            super(a);
            // TODO Auto-generated constructor stub
            context = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.edit_delete_review_layout);


            txt_edit_review = (TextView) findViewById(R.id.txt_edit_review);
            txt_delete_review = (TextView) findViewById(R.id.txt_delete_review);

            txt_edit_review.setOnClickListener(this);
            txt_delete_review.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_edit_review:
                    dismiss();
                    break;
                case R.id.txt_delete_review:

                /*    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to delete this reviewed hotel?")
                            .setContentText("Won't be able to recover this hotel anymore!")
                            .setConfirmText("Delete")
                            .setCancelText("Cancel")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                    try {
                                        String createuserid =Pref_storage.getDetail(context,"userId");

                                        String review_id= getallHotelReviewPojoArrayList.get(position).getRevratId();

                                        Call<CommonOutputPojo> call = apiService.update_delete_hotel_review("update_delete_hotel_review", Integer.parseInt(review_id), Integer.parseInt(createuserid));
                                        call.enqueue(new Callback<CommonOutputPojo>() {
                                            @Override
                                            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                if (response.body().getResponseCode() == 1) {
                                                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                            .setTitleText("Deleted!!")
                                                            .setContentText("Your comment has been deleted.")

                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                @Override
                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                    sDialog.dismissWithAnimation();

                                                                    getallHotelReviewPojoArrayList.remove(position);
                                                                    notifyItemRemoved(position);
                                                                    notifyItemRangeChanged(position, getallHotelReviewPojoArrayList.size());
                                                                }
                                                            })
                                                            .show();


                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                //Error
                                                Log.e("FailureError", "" + t.getMessage());
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                }
                            })
                            .show();

                 */
                    dismiss();
                    break;


                default:
                    break;
            }
        }
    }


//hideKeyboard

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }
    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }



    //getFollowers

    private void getFollowers(final int userId) {

        if (isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<GetFollowersOutput> call = apiService.getFollower("get_follower", userId);

                call.enqueue(new Callback<GetFollowersOutput>() {
                    @Override
                    public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {
                            getFollowersDataList.clear();

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                if(response.body().getData().get(i).getFollowerId()==null){

                                }else {
                                    int  userid = response.body().getData().get(i).getUserid();
                                    String  followerId = response.body().getData().get(i).getFollowerId();
                                    String firstName = response.body().getData().get(i).getFirstName();
                                    String lastName = response.body().getData().get(i).getLastName();
                                    String userPhoto = response.body().getData().get(i).getPicture();

                                    if (!followerId.contains(String.valueOf(userid))) {

                                        GetFollowersData getFollowersData = new GetFollowersData(userid, followerId, firstName, lastName, userPhoto);
                                        getFollowersDataList.add(getFollowersData);
                                    }
                                }




                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

    }






    //Interface friends list


    public void getfriendslist(View view, int position) {

        if (((CheckBox) view).isChecked()) {

            if (friendsList.contains(Integer.valueOf(getFollowersDataList.get(position).getFollowerId()))) {

                return;

            } else {

                friendsList.add(Integer.valueOf(getFollowersDataList.get(position).getFollowerId()));

            }


        } else {

            friendsList.remove(Integer.valueOf(getFollowersDataList.get(position).getFollowerId()));

        }

    }





    @Override
    public int getItemCount() {

        return getallHotelReviewPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        //New design view holder

        TextView username,userlastname,count,count_post,comma,txt_followers_count,txt_followers,serv_or_package,
                txt_created_on,tv_restaurant_name,txt_hotel_address,tv_res_overall_rating,tv_ambience_rating,ambi_or_timedelivery,
                txt_taste_count,tv_vfm_rating,tv_service_rating,title_ambience,tv_view_more,tv_top_dishes,
                tv_dishes_to_avoid,title_dish_avoid,title_top_dish,tv_review_caption,tv_show_less,
                tv_like_count,tv_cmt_count,tv_view_all_comments,txt_adapter_post;




        /*TextView txt_hotelname, txt_hotel_address,
                txt_review_username, txt_review_follow,txt_review_like_count, txt_review_like, txt_review_comment_count,txt_review_comment,
                txt_review_shares,txt_adapter_post;*/




        RelativeLayout rl_follow, rl_likesnew_layout,rl_overall_review;
        LinearLayout ll_rv_share,ll_rv_cmt,ll_rv_like,ll_ambi_more,ll_view_more,ll_dish_to_avoid,ll_likecomment,ll_res_overall_rating,ll_ambience_rating,ll_taste_rating,
                ll_value_of_money_rating,ll_service_rating;
        TextView txt_review_caption;
        EditText review_comment;
        ImageView userphoto_profile, img_foodimage,review_user_photo,img_overall_rating_star,
                img_ambience_star,img_taste_star,img_vfm_star,img_service_star;

        ExpandableTextView reviewcmmnt;

        ImageButton img_btn_review_like, layout_more,img_btn_review_comment, img_review_share,img_bucket_list,img_view_more,avoid_more,top_more;

//        RatingBar rtng_hotel, rtng_user_review;


       ViewPager ambi_viewpager;
       RelativeLayout rl_ambiance_dot,rl_avoid_dish,rl_top_dish,rl_view_more_top,rl_view_more_avoid;

       CircleIndicator ambi_indicator;

        RecyclerView review_photos_recylerview,rv_review_top_dish,rv_review_avoid_dish;


        public MyViewHolder(View itemView) {
            super(itemView);



            username=(TextView) itemView.findViewById(R.id.username);
            userlastname=(TextView) itemView.findViewById(R.id.userlastname);
            count=(TextView) itemView.findViewById(R.id.count);
            count_post=(TextView) itemView.findViewById(R.id.count_post);
            comma=(TextView) itemView.findViewById(R.id.comma);
            txt_followers_count=(TextView) itemView.findViewById(R.id.txt_followers_count);
            txt_followers=(TextView) itemView.findViewById(R.id.txt_followers);
            txt_created_on=(TextView) itemView.findViewById(R.id.txt_created_on);
            tv_restaurant_name=(TextView) itemView.findViewById(R.id.tv_restaurant_name);
            txt_hotel_address=(TextView) itemView.findViewById(R.id.txt_hotel_address);
            tv_res_overall_rating=(TextView) itemView.findViewById(R.id.tv_res_overall_rating);
            tv_ambience_rating=(TextView) itemView.findViewById(R.id.tv_ambience_rating);
            txt_taste_count=(TextView) itemView.findViewById(R.id.txt_taste_count);
            tv_ambience_rating=(TextView) itemView.findViewById(R.id.tv_ambience_rating);
            ambi_or_timedelivery=(TextView) itemView.findViewById(R.id.ambi_or_timedelivery);
            serv_or_package=(TextView) itemView.findViewById(R.id.serv_or_package);
            tv_vfm_rating=(TextView) itemView.findViewById(R.id.tv_vfm_rating);
            tv_service_rating=(TextView) itemView.findViewById(R.id.tv_service_rating);
            tv_view_more=(TextView) itemView.findViewById(R.id.tv_view_more);
            title_ambience=(TextView) itemView.findViewById(R.id.title_ambience);
            tv_top_dishes=(TextView) itemView.findViewById(R.id.tv_top_dishes);
            tv_dishes_to_avoid=(TextView) itemView.findViewById(R.id.tv_dishes_to_avoid);
            title_dish_avoid=(TextView) itemView.findViewById(R.id.title_dish_avoid);
            title_top_dish=(TextView) itemView.findViewById(R.id.title_top_dish);
//            tv_review_caption=(TextView) itemView.findViewById(R.id.tv_review_caption);
            tv_show_less=(TextView) itemView.findViewById(R.id.tv_show_less);
            tv_like_count=(TextView) itemView.findViewById(R.id.tv_like_count);
            tv_cmt_count=(TextView) itemView.findViewById(R.id.tv_cmt_count);
            tv_view_all_comments=(TextView) itemView.findViewById(R.id.tv_view_all_comments);
            txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);




       /*     txt_hotelname = (TextView) itemView.findViewById(R.id.txt_hotelname);
            txt_hotel_address = (TextView) itemView.findViewById(R.id.txt_hotel_address);
            txt_review_username = (TextView) itemView.findViewById(R.id.txt_review_username);
            txt_review_follow = (TextView) itemView.findViewById(R.id.txt_review_follow);
            txt_review_like = (TextView) itemView.findViewById(R.id.txt_review_like);
            txt_review_like_count = (TextView) itemView.findViewById(R.id.txt_review_like_count);
            txt_review_comment = (TextView) itemView.findViewById(R.id.txt_review_comment);
            txt_review_comment_count = (TextView) itemView.findViewById(R.id.txt_review_comment_count);
            txt_review_shares = (TextView) itemView.findViewById(R.id.txt_review_shares);*/



            ambi_viewpager=(ViewPager) itemView.findViewById(R.id.ambi_viewpager);

            ambi_indicator = (CircleIndicator) itemView.findViewById(R.id.ambi_indicator);
            review_comment = (EditText) itemView.findViewById(R.id.review_comment);
            reviewcmmnt = (ExpandableTextView) itemView.findViewById(R.id.hotel_revieww_description);


            txt_review_caption = (TextView) itemView.findViewById(R.id.tv_review_caption);

            //Imageview

            userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);

            img_overall_rating_star = (ImageView) itemView.findViewById(R.id.img_overall_rating_star);
            img_ambience_star = (ImageView) itemView.findViewById(R.id.img_ambience_star);
            img_taste_star = (ImageView) itemView.findViewById(R.id.img_taste_star);
            img_vfm_star = (ImageView) itemView.findViewById(R.id.img_vfm_star);
            img_service_star = (ImageView) itemView.findViewById(R.id.img_service_star);
            userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);
//            img_foodimage = (ImageView) itemView.findViewById(R.id.img_foodimage);
            review_user_photo = (ImageView) itemView.findViewById(R.id.review_user_photo);

            //ImageButton
            img_btn_review_like = (ImageButton) itemView.findViewById(R.id.img_btn_review_like);
            img_btn_review_comment = (ImageButton) itemView.findViewById(R.id.img_btn_review_comment);
//            img_review_share = (ImageButton) itemView.findViewById(R.id.img_review_share);
            img_view_more = (ImageButton) itemView.findViewById(R.id.img_view_more);
            img_bucket_list = (ImageButton) itemView.findViewById(R.id.img_bucket_list);
            top_more = (ImageButton) itemView.findViewById(R.id.top_more);
            avoid_more = (ImageButton) itemView.findViewById(R.id.avoid_more);
            layout_more = (ImageButton) itemView.findViewById(R.id.layout_more);


      /*      //Rating Bar
            rtng_hotel = (RatingBar) itemView.findViewById(R.id.rtng_hotel);
            rtng_user_review = (RatingBar) itemView.findViewById(R.id.rtng_user_review);*/

            rl_overall_review= (RelativeLayout)itemView.findViewById(R.id.rl_overall_review);
            ll_likecomment= (LinearLayout)itemView.findViewById(R.id.ll_likecomment);
            ll_res_overall_rating= (LinearLayout)itemView.findViewById(R.id.ll_res_overall_rating);
            ll_dish_to_avoid= (LinearLayout)itemView.findViewById(R.id.ll_dish_to_avoid);
            ll_ambience_rating= (LinearLayout)itemView.findViewById(R.id.ll_ambience_rating);
            ll_taste_rating= (LinearLayout)itemView.findViewById(R.id.ll_taste_rating);
            ll_value_of_money_rating= (LinearLayout)itemView.findViewById(R.id.ll_value_of_money_rating);
            ll_service_rating= (LinearLayout)itemView.findViewById(R.id.ll_service_rating);
            ll_view_more=(LinearLayout) itemView.findViewById(R.id.ll_view_more);
            ll_ambi_more=(LinearLayout) itemView.findViewById(R.id.ll_ambi_more);
          /*  ll_rv_share= (LinearLayout) itemView.findViewById(R.id.ll_rv_share);
            ll_rv_cmt= (LinearLayout)itemView.findViewById(R.id.ll_rv_cmt);
            ll_rv_like= (LinearLayout)itemView.findViewById(R.id.ll_rv_like);*/


          review_photos_recylerview =(RecyclerView) itemView.findViewById(R.id.review_photos_recylerview);
            rv_review_top_dish =(RecyclerView) itemView.findViewById(R.id.rv_review_top_dish);
            rv_review_avoid_dish =(RecyclerView) itemView.findViewById(R.id.rv_review_avoid_dish);

            rl_ambiance_dot =(RelativeLayout) itemView.findViewById(R.id.rl_ambiance_dot);
            rl_avoid_dish =(RelativeLayout) itemView.findViewById(R.id.rl_avoid_dish);
            rl_top_dish =(RelativeLayout) itemView.findViewById(R.id.rl_top_dish);
            rl_view_more_top=(RelativeLayout) itemView.findViewById(R.id.rl_view_more_top);

            rl_view_more_avoid=(RelativeLayout) itemView.findViewById(R.id.rl_view_more_avoid);
        }


    }


}
