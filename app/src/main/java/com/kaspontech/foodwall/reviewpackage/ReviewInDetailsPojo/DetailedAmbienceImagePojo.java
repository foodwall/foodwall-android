package com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailedAmbienceImagePojo {

    @SerializedName("img")
    @Expose
    private String img;

    /**
     * No args constructor for use in serialization
     *
     */

    /**
     *
     * @param img
     */
    public DetailedAmbienceImagePojo(String img) {
        super();
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

}
