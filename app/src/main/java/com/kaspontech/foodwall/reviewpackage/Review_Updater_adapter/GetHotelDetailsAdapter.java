package com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kaspontech.foodwall.bucketlistpackage.CreateBucketlistActivty;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersData;
import com.kaspontech.foodwall.modelclasses.Followers.GetFollowersOutput;
import com.kaspontech.foodwall.modelclasses.ReportResponse;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.New_Getting_timeline_all_pojo;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;
import com.kaspontech.foodwall.reviewpackage.alldishtab.AllDishDisplayActivity;
import com.kaspontech.foodwall.reviewpackage.ReviewDeliveryDetails;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewInputAllPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_in_detail_activity;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
import com.kaspontech.foodwall.reviewpackage.ReviewsLikesAllActivity;
//
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.kaspontech.foodwall.reviewpackage.Reviews.review_filter;
import static java.lang.Integer.parseInt;

@RequiresApi(api = Build.VERSION_CODES.M)
public class GetHotelDetailsAdapter extends RecyclerView.Adapter<GetHotelDetailsAdapter.MyViewHolder>
        implements RecyclerView.OnItemTouchListener, RecyclerView.OnScrollChangeListener {

    /**
     * Application TAG
     */
    private static final String TAG = "GetHotelDetailsAdapter";


    PopupWindow popupWindow;
    View popup;
    private android.app.AlertDialog alt;
    /**
     * tv_top_dishes
     * Context
     */
    public Context context;
    /**
     * Shared Preference
     */
    public Pref_storage pref_storage;

    /**
     * Api call interface
     */

    public Call<CommonOutputPojo> call;
    public ApiInterface apiInterface;

    /**
     * Adapters Top , avoid ,ambience
     */
    public ReviewTopDishAdapter reviewTopDishAdapter;
    public ReviewAmbienceAdapter ambianceImagesAdapter;
    public ReviewAvoidDishAdapter reviewWorstDishAdapter;
    /**
     * From which adapter String result
     */
    String fromwhich;

     /**
     * More options
     */
    public final String[] itemsothers = {"Bucket list options", "Delete review", "Share Via"};
    public final String[] itemsothers1 = {"Bucket list options", "Share Via", "Report"};

    /**
     * Top dish list
     */
    public ReviewTopDishPojo reviewTopDishPojo;
    /**
     * Avoid dish list
     */
    public ReviewAvoidDishPojo reviewAvoidDishPojo;
    /**
     * Ambience list
     */
    public ReviewAmbiImagePojo reviewAmbiImagePojo;

    public PackImage packImage;
    /**
     * Review all pojo & its arraylist
     */
    public ReviewInputAllPojo getHotelAllInnerReviewPojo;

    public List<ReviewInputAllPojo> getallHotelReviewPojoArrayList;

    /**
     * Array lists
     */

    public List<Integer> friendsList = new ArrayList<>();
    public List<GetFollowersData> getFollowersDataList = new ArrayList<>();

    public ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();
    public ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

    private onCommentsPostListener onCommentsPostListener;

    public GetHotelDetailsAdapter(Context c, List<ReviewInputAllPojo> gettinglist, String fromwhich, onCommentsPostListener onCommentsPostListener) {
        this.context = c;
        this.getallHotelReviewPojoArrayList = gettinglist;
        this.fromwhich = fromwhich;
        this.onCommentsPostListener = onCommentsPostListener;
    }

    public GetHotelDetailsAdapter(Context c, List<ReviewInputAllPojo> gettinglist, String fromwhich) {
        this.context = c;
        this.getallHotelReviewPojoArrayList = gettinglist;
        this.fromwhich = fromwhich;
//        setHasStableIds(true);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public GetHotelDetailsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_delivery_all, parent, false);

        GetHotelDetailsAdapter.MyViewHolder vh = new GetHotelDetailsAdapter.MyViewHolder(v, onCommentsPostListener);
         return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final GetHotelDetailsAdapter.MyViewHolder holder, final int position) {



        getHotelAllInnerReviewPojo = getallHotelReviewPojoArrayList.get(position);

        /* Reviewer first name */
        if (getHotelAllInnerReviewPojo.getFirstName() == null) {

            holder.username.setText(context.getString(R.string.Unknown));
        } else {

            holder.username.setText(getHotelAllInnerReviewPojo.getFirstName());
        }

        /*Reviewer Last name*/
        if (getHotelAllInnerReviewPojo.getLastName() == null) {

            holder.userlastname.setText(context.getString(R.string.Unknown));

        } else {

            holder.userlastname.setText(getHotelAllInnerReviewPojo.getLastName());

        }

        holder.username.setTextColor(Color.parseColor("#000000"));
        holder.userlastname.setTextColor(Color.parseColor("#000000"));

        /*Category type condition setting*/
        if (getallHotelReviewPojoArrayList.get(position).getCategoryType().equalsIgnoreCase("2")) {

            holder.title_ambience.setText("Packaging");

        } else {

            holder.title_ambience.setText("Hotel Ambience");

        }

        if (getallHotelReviewPojoArrayList.get(position).getBucket_list() != null) {

            /*Bucket list image visibilty condition*/
            if (getallHotelReviewPojoArrayList.get(position).getBucket_list() != 0) {

                holder.img_bucket_list.setImageResource(R.drawable.ic_bucket_neww);

            } else {

                holder.img_bucket_list.setImageResource(R.drawable.ic_bucket);
            }
        }

        /* Text hotel name */
        holder.tv_restaurant_name.setText(getallHotelReviewPojoArrayList.get(position).getHotelName());

        /* Mode of delivery text */
        if (getallHotelReviewPojoArrayList.get(position).getMode_id().equalsIgnoreCase("0")) {

            holder.txt_mode_delivery.setVisibility(GONE);

        } else {

            Log.e("fromwhich", "onBindViewHolder: " + fromwhich);

            if (fromwhich == null || fromwhich.equalsIgnoreCase("")) {

                holder.ll_delivery_mode.setVisibility(VISIBLE);

                if (getallHotelReviewPojoArrayList.get(position).getMode_id().equalsIgnoreCase("1")) {
                    holder.txt_mode_delivery.setText(getallHotelReviewPojoArrayList.get(position).getComp_name());
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#f25353"));

                } else if (getallHotelReviewPojoArrayList.get(position).getMode_id().equalsIgnoreCase("2")) {
                    holder.txt_mode_delivery.setText(getallHotelReviewPojoArrayList.get(position).getComp_name().replace("swiggy", "Swiggy"));
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#ff8008"));

                } else if (getallHotelReviewPojoArrayList.get(position).getMode_id().equalsIgnoreCase("3")) {
                    holder.txt_mode_delivery.setText(getallHotelReviewPojoArrayList.get(position).getComp_name().replace("Food panda", "Foodpanda"));
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#ffff8800"));

                } else if (getallHotelReviewPojoArrayList.get(position).getMode_id().equalsIgnoreCase("4")) {
                    holder.txt_mode_delivery.setText(getallHotelReviewPojoArrayList.get(position).getComp_name());
                    holder.txt_mode_delivery.setTextColor(Color.parseColor("#2ecc71"));

                }
            } else {
                holder.ll_delivery_mode.setVisibility(GONE);
            }


        }

        /* Mode of delivery view visibility conditiom */

        if (getallHotelReviewPojoArrayList.get(position).getCategoryType().equalsIgnoreCase("2")) {

            if (fromwhich == null || fromwhich.equalsIgnoreCase("")) {
                holder.ll_delivery_mode.setVisibility(VISIBLE);
            } else {
                holder.ll_delivery_mode.setVisibility(GONE);
            }


        } else {

            holder.ll_delivery_mode.setVisibility(GONE);

        }

        /* Reviewer Image */

        Utility.picassoImageLoader(getallHotelReviewPojoArrayList.get(position).getPicture(), 0, holder.userphoto_profile, context);


        /* User Image */

        Utility.picassoImageLoader(Pref_storage.getDetail(context, "picture_user_login"), 0, holder.review_user_photo, context);




        /* Timestamp for review created time*/

        String createdon = getHotelAllInnerReviewPojo.getCreatedOn();

        Utility.setTimeStamp(createdon, holder.txt_created_on);




        /* Display top dishes */

        if (getallHotelReviewPojoArrayList.get(position).getTopdish().trim().equalsIgnoreCase("") || getallHotelReviewPojoArrayList.get(position).getTopdish().trim().isEmpty() || getallHotelReviewPojoArrayList.get(position).getTopdish() == null) {

            holder.tv_top_dishes.setVisibility(GONE);
            holder.rl_top_dish.setVisibility(GONE);
            holder.ll_top_dish.setVisibility(GONE);

        } else {

             holder.tv_top_dishes.setVisibility(GONE);
            holder.rl_top_dish.setVisibility(VISIBLE);
            holder.ll_top_dish.setVisibility(VISIBLE);
        }

        try {

             if (getallHotelReviewPojoArrayList.get(position).getTopdishimage().get(0).getImg().trim().equalsIgnoreCase("") || getallHotelReviewPojoArrayList.get(position).getTopdishimage().get(0).getImg().isEmpty()) {

                holder.rl_top_dish.setVisibility(GONE);
                holder.tv_top_dishes.setVisibility(GONE);

            } else {
                holder.tv_top_dishes.setVisibility(GONE);
                holder.rl_top_dish.setVisibility(VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* Display avoid dishes */

        if (getallHotelReviewPojoArrayList.get(position).getAvoiddish().trim().equalsIgnoreCase("") || getallHotelReviewPojoArrayList.get(position).getAvoiddish().trim().isEmpty() || getallHotelReviewPojoArrayList.get(position).getAvoiddish() == null) {

            holder.tv_dishes_to_avoid.setVisibility(GONE);
            holder.rl_avoid_dish.setVisibility(GONE);
            holder.ll_dish_to_avoid.setVisibility(GONE);

        } else {

             holder.tv_dishes_to_avoid.setVisibility(GONE);
            holder.rl_avoid_dish.setVisibility(VISIBLE);
            holder.ll_dish_to_avoid.setVisibility(VISIBLE);

        }

        try {

            if (getallHotelReviewPojoArrayList.get(position).getAvoidCount() == 0) {

                holder.rl_avoid_dish.setVisibility(GONE);
                holder.tv_dishes_to_avoid.setVisibility(VISIBLE);

            } else {
                holder.tv_dishes_to_avoid.setVisibility(GONE);
                holder.rl_avoid_dish.setVisibility(VISIBLE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        /* Edit text comment text change listener */

        holder.review_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void afterTextChanged(Editable s) {
                /*Post button visibilty*/
                if (holder.review_comment.getText().toString().trim().length() != 0) {
                    holder.txt_adapter_post.setVisibility(VISIBLE);
                    if (review_filter != null) {
                        review_filter.setVisibility(GONE);
                    }
                } else {
                    holder.txt_adapter_post.setVisibility(GONE);
                    if (review_filter != null) {
                        review_filter.setVisibility(VISIBLE);
                    }
                }


            }
        });


        /* Dishes to avoid text count */
        int dishAvoid = getallHotelReviewPojoArrayList.get(position).getAvoidCount();

        if (dishAvoid == 0) {
            holder.ll_dish_to_avoid.setVisibility(View.GONE);
        }


        /* Overallrating */
        String rating = getallHotelReviewPojoArrayList.get(position).getFoodExprience();

        holder.tv_res_overall_rating.setText(rating);

        if (Integer.parseInt(rating) == 1) {
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_red);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

        } else if (Integer.parseInt(rating) == 2) {
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_orange_star);
            holder.ll_res_overall_rating.setBackgroundResource(R.drawable.review_rating_orange_bg);

        } else if (Integer.parseInt(rating) == 3) {
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
        } else if (Integer.parseInt(rating) == 4) {
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
        } else if (Integer.parseInt(rating) == 5) {
            holder.tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
            holder.ll_res_overall_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
        }


        try {

            /* Time delivery */
            if (Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getCategoryType()) == 2) {

                String time_deleivery = getallHotelReviewPojoArrayList.get(position).getTimedelivery();
                holder.ambi_or_timedelivery.setText(R.string.Delivery_new);
                holder.tv_ambience_rating.setText(getallHotelReviewPojoArrayList.get(position).getTimedelivery());

                if (Integer.parseInt(time_deleivery) == 1) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_red);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

                } else if (Integer.parseInt(time_deleivery) == 2) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                } else if (Integer.parseInt(time_deleivery) == 3) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                } else if (Integer.parseInt(time_deleivery) == 4) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_star);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                } else if (Integer.parseInt(time_deleivery) == 5) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                }

            } else {

                /* Ambiance */
                String ambiance = getallHotelReviewPojoArrayList.get(position).getAmbiance();

                holder.tv_ambience_rating.setText(ambiance);

                if (Integer.parseInt(ambiance) == 1) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_red);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

                } else if (Integer.parseInt(ambiance) == 2) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                } else if (Integer.parseInt(ambiance) == 3) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                } else if (Integer.parseInt(ambiance) == 4) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_star);

                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                } else if (Integer.parseInt(ambiance) == 5) {
                    holder.tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_ambience_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                    holder.ll_ambience_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        /* Taste */
        String taste = getallHotelReviewPojoArrayList.get(position).getTaste();

        holder.txt_taste_count.setText(taste);

        if (Integer.parseInt(taste) == 1) {
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_red);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));

        } else if (Integer.parseInt(taste) == 2) {
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_orange_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
        } else if (Integer.parseInt(taste) == 3) {
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
        } else if (Integer.parseInt(taste) == 4) {
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
        } else if (Integer.parseInt(taste) == 5) {
            holder.txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_taste_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
            holder.ll_taste_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
        }


        /* Value for money */
        String vfm = getallHotelReviewPojoArrayList.get(position).getValueMoney();

        holder.tv_vfm_rating.setText(vfm);

        if (Integer.parseInt(vfm) == 1) {
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_red);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
        } else if (Integer.parseInt(vfm) == 2) {
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_orange_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
        } else if (Integer.parseInt(vfm) == 3) {
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
        } else if (Integer.parseInt(vfm) == 4) {
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
        } else if (Integer.parseInt(vfm) == 5) {
            holder.tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
            holder.img_vfm_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
            holder.ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
        }


        try {


            /* Packaging */
            if (Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getCategoryType()) == 2) {

                String packageRating = getallHotelReviewPojoArrayList.get(position).get_package();
                holder.tv_service_rating.setText(packageRating);
                holder.serv_or_package.setText(R.string.Package);

                if (Integer.parseInt(packageRating) == 1) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_red);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
                } else if (Integer.parseInt(packageRating) == 2) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                } else if (Integer.parseInt(packageRating) == 3) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                } else if (Integer.parseInt(packageRating) == 4) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_star);

                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                } else if (Integer.parseInt(packageRating) == 5) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                }


            } else {

                /* Service */
                String service = getallHotelReviewPojoArrayList.get(position).getService();

                holder.tv_service_rating.setText(service);
                if (Integer.parseInt(service) == 1) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_red);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_red_background));
                } else if (Integer.parseInt(service) == 2) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_orange_bg));
                } else if (Integer.parseInt(service) == 3) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_orange_bg));
                } else if (Integer.parseInt(service) == 4) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_star);

                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_light_green_bg));
                } else if (Integer.parseInt(service) == 5) {
                    holder.tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.img_service_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                    holder.ll_service_rating.setBackground(ContextCompat.getDrawable(context, R.drawable.review_rating_dark_green_bg));
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        String total_reviews = getallHotelReviewPojoArrayList.get(position).getTotalReview();
        String total_followers = getallHotelReviewPojoArrayList.get(position).getTotalFollowers();


        /* Review comment visibility */
        if (getallHotelReviewPojoArrayList.get(position).getHotelReview().equalsIgnoreCase("0")) {
            holder.reviewcmmnt.setVisibility(GONE);
        } else {
            holder.reviewcmmnt.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getallHotelReviewPojoArrayList.get(position).getHotelReview()));

        }


        /* Total likes displaying */
        final String total_likes = getallHotelReviewPojoArrayList.get(position).getTotalLikes();

        if (Integer.parseInt(total_likes) == 0) {

            holder.ll_likecomment.setVisibility(GONE);

        } else {

            holder.ll_likecomment.setVisibility(VISIBLE);

        }

        int userliked = Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getHtlRevLikes());

        if (userliked == 0) {

            getallHotelReviewPojoArrayList.get(position).setUserliked(false);
            holder.img_btn_review_like.setVisibility(View.VISIBLE);
            holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));

        } else if (userliked == 1) {

            getallHotelReviewPojoArrayList.get(position).setUserliked(true);
            holder.img_btn_review_like.setVisibility(View.VISIBLE);
            holder.ll_likecomment.setVisibility(VISIBLE);
            holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));

        }


        if (parseInt(total_likes) == 0 || total_likes.equalsIgnoreCase(null) || total_likes.equalsIgnoreCase("")) {

            holder.tv_like_count.setVisibility(View.GONE);
        }
        if (parseInt(total_likes) == 1) {
            holder.tv_like_count.setText(getallHotelReviewPojoArrayList.get(position).getTotalLikes().concat(" ").concat("like"));
            holder.tv_like_count.setVisibility(View.VISIBLE);


        } else if (parseInt(total_likes) > 1) {
            holder.tv_like_count.setText(getallHotelReviewPojoArrayList.get(position).getTotalLikes().concat(" ").concat("likes"));
            holder.tv_like_count.setVisibility(View.VISIBLE);
        }

        /* Total comments */
        String total_comments = getallHotelReviewPojoArrayList.get(position).getTotalComments();

        if (parseInt(total_comments) == 0 || total_comments.equalsIgnoreCase(null) || total_comments.equalsIgnoreCase("")) {

            holder.tv_view_all_comments.setVisibility(View.GONE);

        }

        if (parseInt(total_comments) == 1) {

            holder.tv_view_all_comments.setText(R.string.view_one_comment);
            holder.tv_view_all_comments.setVisibility(View.VISIBLE);

        } else if (parseInt(total_comments) > 1) {

            holder.tv_view_all_comments.setText("View all " + total_comments + " comments");
            holder.tv_view_all_comments.setVisibility(View.VISIBLE);
        }

        /*Redirect to comments page*/
        holder.tv_view_all_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, Reviews_comments_all_activity.class);

                intent.putExtra("posttype", getallHotelReviewPojoArrayList.get(position).getCategoryType());
                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("userid", getallHotelReviewPojoArrayList.get(position).getUserId());

                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                intent.putExtra("overallrating", getallHotelReviewPojoArrayList.get(position).getFoodExprience());

                if (getallHotelReviewPojoArrayList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                    intent.putExtra("totaltimedelivery", getallHotelReviewPojoArrayList.get(position).getTotalTimedelivery());
                } else {
                    intent.putExtra("totaltimedelivery", getallHotelReviewPojoArrayList.get(position).getAmbiance());
                }

                intent.putExtra("taste_count", getallHotelReviewPojoArrayList.get(position).getTaste());
                intent.putExtra("vfm_rating", getallHotelReviewPojoArrayList.get(position).getValueMoney());
                if (getallHotelReviewPojoArrayList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                    intent.putExtra("package_rating", getallHotelReviewPojoArrayList.get(position).get_package());
                } else {
                    intent.putExtra("package_rating", getallHotelReviewPojoArrayList.get(position).getService());
                }

                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                intent.putExtra("reviewprofile", getallHotelReviewPojoArrayList.get(position).getPicture());
                intent.putExtra("createdon", getallHotelReviewPojoArrayList.get(position).getCreatedOn());
                intent.putExtra("firstname", getallHotelReviewPojoArrayList.get(position).getFirstName());
                intent.putExtra("lastname", getallHotelReviewPojoArrayList.get(position).getLastName());

                if (getallHotelReviewPojoArrayList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                    intent.putExtra("title_ambience", "Packaging");
                } else {
                    intent.putExtra("title_ambience", "Hotel Ambience");
                }

                if (getallHotelReviewPojoArrayList.get(position).getBucket_list() != 0) {
                    intent.putExtra("addbucketlist", "yes");
                } else {
                    intent.putExtra("addbucketlist", "no");
                }

                context.startActivity(intent);
            }
        });

        /*Redirect to Likes page*/
        holder.tv_like_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ReviewsLikesAllActivity.class);

                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("userid", getallHotelReviewPojoArrayList.get(position).getUserId());
                context.startActivity(intent);
            }
        });

        /*Redirect to Comments page*/
        holder.img_btn_review_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(context, Reviews_comments_all_activity.class);

                intent.putExtra("posttype", getallHotelReviewPojoArrayList.get(position).getCategoryType());
                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("userid", getallHotelReviewPojoArrayList.get(position).getUserId());

                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                intent.putExtra("overallrating", getallHotelReviewPojoArrayList.get(position).getFoodExprience());

                if (getallHotelReviewPojoArrayList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                    intent.putExtra("totaltimedelivery", getallHotelReviewPojoArrayList.get(position).getTotalTimedelivery());
                } else {
                    intent.putExtra("totaltimedelivery", getallHotelReviewPojoArrayList.get(position).getAmbiance());
                }

                intent.putExtra("taste_count", getallHotelReviewPojoArrayList.get(position).getTaste());
                intent.putExtra("vfm_rating", getallHotelReviewPojoArrayList.get(position).getValueMoney());
                if (getallHotelReviewPojoArrayList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                    intent.putExtra("package_rating", getallHotelReviewPojoArrayList.get(position).get_package());
                } else {
                    intent.putExtra("package_rating", getallHotelReviewPojoArrayList.get(position).getService());
                }

                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                intent.putExtra("reviewprofile", getallHotelReviewPojoArrayList.get(position).getPicture());
                intent.putExtra("createdon", getallHotelReviewPojoArrayList.get(position).getCreatedOn());
                intent.putExtra("firstname", getallHotelReviewPojoArrayList.get(position).getFirstName());
                intent.putExtra("lastname", getallHotelReviewPojoArrayList.get(position).getLastName());

                if (getallHotelReviewPojoArrayList.get(position).getCategoryType().equalsIgnoreCase("2")) {
                    intent.putExtra("title_ambience", "Packaging");
                } else {
                    intent.putExtra("title_ambience", "Hotel Ambience");
                }

                if (getallHotelReviewPojoArrayList.get(position).getBucket_list() != 0) {
                    intent.putExtra("addbucketlist", "yes");
                } else {
                    intent.putExtra("addbucketlist", "no");
                }

                context.startActivity(intent);

            }
        });

        /*Redirect to user profile page*/
        holder.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getallHotelReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    Intent intent = new Intent(context, Profile.class);
                    intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);

                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);
                }

            }
        });

        /*Redirect to user profile page*/
        holder.userlastname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getallHotelReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    Intent intent = new Intent(context, Profile.class);
                    intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);

                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);
                }

            }
        });
        /*Redirect to user profile page*/
        holder.userphoto_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getallHotelReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    Intent intent = new Intent(context, Profile.class);
                    intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);

                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);
                }

            }
        });

        /*Redirect to user profile page*/
        holder.review_user_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getallHotelReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    Intent intent = new Intent(context, Profile.class);
                    intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);

                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("created_by", getallHotelReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);
                }

            }
        });

        /*Redirect to detailed hotel data  page*/

        holder.rl_hotel_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, Review_in_detail_activity.class);
                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("f_name", getallHotelReviewPojoArrayList.get(position).getFirstName());
                intent.putExtra("l_name", getallHotelReviewPojoArrayList.get(position).getLastName());
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("timelineId", "0");
                context.startActivity(intent);

            }
        });

        holder.ll_delivery_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ReviewDeliveryDetails.class);
                intent.putExtra("catType", getallHotelReviewPojoArrayList.get(position).getCategoryType());
                intent.putExtra("delMode", getallHotelReviewPojoArrayList.get(position).getDelivery_mode());
                context.startActivity(intent);

            }
        });


        /* Like functionality with api calling */
        holder.img_btn_review_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                holder.ll_likecomment.setVisibility(VISIBLE);


                if (getallHotelReviewPojoArrayList.get(position).isUserliked()) {


                    try {


                        holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_border_redlike_24dp));
                        getallHotelReviewPojoArrayList.get(position).setUserliked(false);
                        getallHotelReviewPojoArrayList.get(position).setHtlRevLikes(String.valueOf(0));


                        int total_likes = Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getTotalLikes());

                        if (total_likes == 1) {

                            total_likes = total_likes - 1;
                            getallHotelReviewPojoArrayList.get(position).setTotalLikes(String.valueOf(total_likes));
                            holder.tv_like_count.setVisibility(View.GONE);


                        } else if (total_likes == 2) {

                            total_likes = total_likes - 1;
                            getallHotelReviewPojoArrayList.get(position).setTotalLikes(String.valueOf(total_likes));
                            holder.tv_like_count.setText(String.valueOf(total_likes) + " Like");
                            holder.tv_like_count.setVisibility(View.VISIBLE);

                        } else {

                            total_likes = total_likes - 1;
                            getallHotelReviewPojoArrayList.get(position).setTotalLikes(String.valueOf(total_likes));
                            holder.tv_like_count.setText(String.valueOf(total_likes) + " Likes");
                            holder.tv_like_count.setVisibility(View.VISIBLE);

                        }

                        if (Utility.isConnected(context)) {

                            apiInterface = ApiClient.getClient().create(ApiInterface.class);
                            int userid = parseInt(Pref_storage.getDetail(context, "userId"));

                            call = apiInterface.create_hotel_review_likes("create_hotel_review_likes", parseInt(getallHotelReviewPojoArrayList.get(position).getHotelId()), parseInt(getallHotelReviewPojoArrayList.get(position).getRevratId()), 0, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();
                                    int resposecode = response.body().getResponseCode();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);
                                    Log.e("resposecode", "resposecode->" + resposecode);

                                    if (resposecode == 1) {

                                        //Success

                                        /*Starting animating*/
                                        final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                                        v.startAnimation(anim);

                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } else {

                            Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    try {


                        holder.ll_likecomment.setVisibility(VISIBLE);
                        holder.img_btn_review_like.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red_24dp));
                        getallHotelReviewPojoArrayList.get(position).setUserliked(true);
                        getallHotelReviewPojoArrayList.get(position).setHtlRevLikes(String.valueOf(1));


                        int total_likes = Integer.parseInt(getallHotelReviewPojoArrayList.get(position).getTotalLikes());

                        if (total_likes == 0) {

                            total_likes = total_likes + 1;
                            getallHotelReviewPojoArrayList.get(position).setTotalLikes(String.valueOf(total_likes));
                            holder.tv_like_count.setText(String.valueOf(total_likes) + " Like");
                            holder.tv_like_count.setVisibility(View.VISIBLE);

                        } else {

                            total_likes = total_likes + 1;
                            getallHotelReviewPojoArrayList.get(position).setTotalLikes(String.valueOf(total_likes));
                            holder.tv_like_count.setText(String.valueOf(total_likes) + " Likes");

                        }
                        if (Utility.isConnected(context)) {

                            apiInterface = ApiClient.getClient().create(ApiInterface.class);

                            int userid = parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<CommonOutputPojo> call = apiInterface.create_hotel_review_likes("create_hotel_review_likes", parseInt(getallHotelReviewPojoArrayList.get(position).getHotelId()), parseInt(getallHotelReviewPojoArrayList.get(position).getRevratId()), 1, userid);

                            call.enqueue(new Callback<CommonOutputPojo>() {
                                @Override
                                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                    String responseStatus = response.body().getResponseMessage();

                                    Log.e("responseStatus", "responseStatus->" + responseStatus);

                                    if (responseStatus.equals("success")) {

                                        //Success

                                        /*Starting animating*/
                                        final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                                        v.startAnimation(anim);

                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "FailureError" + t.getMessage());
                                }
                            });

                        } else {

                            Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                        }


                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }


            }
        });



        /* More settings on click listener redirecting to bucket list page*/
        holder.img_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getallHotelReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {

                    AlertDialog.Builder front_builder = new AlertDialog.Builder(context);

                    front_builder.setItems(itemsothers, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            /*Redirecting to bucket list page*/
                            if (itemsothers[which].equals("Bucket list options")) {

                                dialog.dismiss();

                                Intent intent = new Intent(context, CreateBucketlistActivty.class);
                                intent.putExtra("f_name", getallHotelReviewPojoArrayList.get(position).getFirstName());
                                intent.putExtra("l_name", getallHotelReviewPojoArrayList.get(position).getLastName());
                                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                                intent.putExtra("timelineId", "0");
                                context.startActivity(intent);

                            } else if (itemsothers[which].equals("Delete review")) {

                                /*Deleting the review*/
                                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Are you sure want to delete?")
                                        .setContentText("Won't be able to recover it anymore!")
                                        .setConfirmText("Delete")
                                        .setCancelText("Cancel")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {

                                                sDialog.dismissWithAnimation();

                                                // Check internet connection


                                                if (Utility.isConnected(context)) {

                                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                                    try {

                                                        String createuserid = Pref_storage.getDetail(context, "userId");

                                                        String review_id = getallHotelReviewPojoArrayList.get(position).getRevratId();

                                                        Call<CommonOutputPojo> call = apiService.update_delete_hotel_review("update_delete_hotel_review", parseInt(review_id), parseInt(createuserid));

                                                        call.enqueue(new Callback<CommonOutputPojo>() {
                                                            @Override
                                                            public void onResponse(@NonNull Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                                if (response.body() != null && response.body().getResponseCode() == 1) {


                                                                    getallHotelReviewPojoArrayList.remove(position);
                                                                    notifyItemRemoved(position);
                                                                    notifyItemRangeChanged(position, getallHotelReviewPojoArrayList.size());


                                                                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                                            .setTitleText("Deleted!!")
                                                                            .setContentText("Your hotel review has been deleted.")
                                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                                @Override
                                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                                    sDialog.dismissWithAnimation();

                                                                                }
                                                                            })

                                                                            .show();


                                                                }

                                                            }

                                                            @Override
                                                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                                //Error
                                                                Log.e("FailureError", "" + t.getMessage());
                                                            }
                                                        });
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }

                                                } else {

                                                    Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        })
                                        .show();

                                dialog.dismiss();
                            } else if (itemsothers[which].equals("Share Via")) {
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("text/plain");
                                i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                                i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                                context.startActivity(Intent.createChooser(i, "Share via"));

                            } else if (itemsothers[which].equals("Report")) {
                                popup = LayoutInflater.from(context).inflate(R.layout.reportoption_popup, null);
                                popupWindow = new PopupWindow(popup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
                                final android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(context);

                                final EditText edt_reportcomments = (EditText) popup.findViewById(R.id.edt_reportcomments);
                                Button cancel = (Button) popup.findViewById(R.id.cancel);
                                Button submit = (Button) popup.findViewById(R.id.submit);
                                final ProgressBar progress_timeline = (ProgressBar) popup.findViewById(R.id.progress_timeline);


                                cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        popupWindow.dismiss();
                                        alt.dismiss();


                                    }
                                });


                                submit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //call create_report_email api
                                        if (Utility.isConnected(context)) {

                                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                            if (edt_reportcomments.getText().toString().length() != 0) {
                                                try {
                                                    progress_timeline.setVisibility(VISIBLE);

                                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));


                                                    Call<ReportResponse> call = apiService.create_report_email("create_report_email",
                                                            3,
                                                            edt_reportcomments.getText().toString(),
                                                            Integer.valueOf(getallHotelReviewPojoArrayList.get(position).getCreatedBy()),
                                                            getallHotelReviewPojoArrayList.get(position).getRevratId(),
                                                            userid);


                                                    call.enqueue(new Callback<ReportResponse>() {
                                                        @Override
                                                        public void onResponse(Call<ReportResponse> call, Response<ReportResponse> response) {

                                                            if (response.body() != null) {

                                                                String responseStatus = response.body().getResponseMessage();

                                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                                if (responseStatus.equals("success")) {

                                                                    progress_timeline.setVisibility(GONE);

                                                                    //Success
                                                                    popupWindow.dismiss();
                                                                    alt.dismiss();


                                                                }

                                                            } else {

                                                                Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<ReportResponse> call, Throwable t) {
                                                            //Error
                                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                                            progress_timeline.setVisibility(GONE);
                                                        }
                                                    });

                                                } catch (Exception e) {

                                                    e.printStackTrace();

                                                }
                                            } else {
                                                edt_reportcomments.setError("Type your report");
                                            }
                                        } else {
                                            Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });


                                alertbox.setView(popup);
                                alt = alertbox.show();
                                popupWindow.dismiss();

                            }
                        }
                    });
                    front_builder.show();
                } else {
                    AlertDialog.Builder front_builder = new AlertDialog.Builder(context);

                    front_builder.setItems(itemsothers1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            /*Redirecting to bucket list page*/
                            if (itemsothers1[which].equals("Bucket list options")) {

                                dialog.dismiss();

                                Intent intent = new Intent(context, CreateBucketlistActivty.class);
                                intent.putExtra("f_name", getallHotelReviewPojoArrayList.get(position).getFirstName());
                                intent.putExtra("l_name", getallHotelReviewPojoArrayList.get(position).getLastName());
                                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                                intent.putExtra("timelineId", "0");
                                context.startActivity(intent);

                            } else if (itemsothers1[which].equals("Delete review")) {

                                /*Deleting the review*/
                                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Are you sure want to delete?")
                                        .setContentText("Won't be able to recover it anymore!")
                                        .setConfirmText("Delete")
                                        .setCancelText("Cancel")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {

                                                sDialog.dismissWithAnimation();

                                                // Check internet connection


                                                if (Utility.isConnected(context)) {

                                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                                    try {

                                                        String createuserid = Pref_storage.getDetail(context, "userId");

                                                        String review_id = getallHotelReviewPojoArrayList.get(position).getRevratId();

                                                        Call<CommonOutputPojo> call = apiService.update_delete_hotel_review("update_delete_hotel_review", parseInt(review_id), parseInt(createuserid));

                                                        call.enqueue(new Callback<CommonOutputPojo>() {
                                                            @Override
                                                            public void onResponse(@NonNull Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                                                                if (response.body() != null && response.body().getResponseCode() == 1) {


                                                                    getallHotelReviewPojoArrayList.remove(position);
                                                                    notifyItemRemoved(position);
                                                                    notifyItemRangeChanged(position, getallHotelReviewPojoArrayList.size());


                                                                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                                            .setTitleText("Deleted!!")
                                                                            .setContentText("Your hotel review has been deleted.")
                                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                                @Override
                                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                                    sDialog.dismissWithAnimation();

                                                                                }
                                                                            })

                                                                            .show();


                                                                }

                                                            }

                                                            @Override
                                                            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                                                                //Error
                                                                Log.e("FailureError", "" + t.getMessage());
                                                            }
                                                        });
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }

                                                } else {

                                                    Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        })
                                        .show();

                                dialog.dismiss();
                            } else if (itemsothers1[which].equals("Share Via")) {
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("text/plain");
                                i.putExtra(Intent.EXTRA_SUBJECT, "See this FoodWall photo by @xxxx ");
                                i.putExtra(Intent.EXTRA_TEXT, "https://www.foodwall.com/p/Rgdf78hfhud876");
                                context.startActivity(Intent.createChooser(i, "Share via"));

                            } else if (itemsothers1[which].equals("Report")) {
                                popup = LayoutInflater.from(context).inflate(R.layout.reportoption_popup, null);
                                popupWindow = new PopupWindow(popup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
                                final android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(context);

                                final EditText edt_reportcomments = (EditText) popup.findViewById(R.id.edt_reportcomments);
                                Button cancel = (Button) popup.findViewById(R.id.cancel);
                                Button submit = (Button) popup.findViewById(R.id.submit);
                                final ProgressBar progress_timeline = (ProgressBar) popup.findViewById(R.id.progress_timeline);


                                cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        popupWindow.dismiss();
                                        alt.dismiss();


                                    }
                                });


                                submit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //call create_report_email api
                                        if (Utility.isConnected(context)) {

                                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                            if (edt_reportcomments.getText().toString().length() != 0) {
                                                try {
                                                    progress_timeline.setVisibility(VISIBLE);

                                                    int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));


                                                    Call<ReportResponse> call = apiService.create_report_email("create_report_email",
                                                            3,
                                                            edt_reportcomments.getText().toString(),
                                                            Integer.valueOf(getallHotelReviewPojoArrayList.get(position).getCreatedBy()),
                                                            getallHotelReviewPojoArrayList.get(position).getRevratId(),
                                                            userid);


                                                    call.enqueue(new Callback<ReportResponse>() {
                                                        @Override
                                                        public void onResponse(Call<ReportResponse> call, Response<ReportResponse> response) {

                                                            if (response.body() != null) {

                                                                String responseStatus = response.body().getResponseMessage();

                                                                Log.e("responseStatus", "responseStatus->" + responseStatus);

                                                                if (responseStatus.equals("success")) {

                                                                    progress_timeline.setVisibility(GONE);

                                                                    //Success
                                                                    popupWindow.dismiss();
                                                                    alt.dismiss();


                                                                }

                                                            } else {

                                                                Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();

                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<ReportResponse> call, Throwable t) {
                                                            //Error
                                                            Log.e("FailureError", "FailureError" + t.getMessage());
                                                            progress_timeline.setVisibility(GONE);
                                                        }
                                                    });

                                                } catch (Exception e) {

                                                    e.printStackTrace();

                                                }
                                            } else {
                                                edt_reportcomments.setError("Type your report");
                                            }
                                        } else {
                                            Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });


                                alertbox.setView(popup);
                                alt = alertbox.show();
                                popupWindow.dismiss();

                            }
                        }
                    });
                    front_builder.show();
                }

            }
        });


        /* Adding bucketlist API to create bucket list page */
        holder.img_bucket_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Loader animation*/

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);

                /* Bucket list intent */

                Intent intent = new Intent(context, CreateBucketlistActivty.class);
                intent.putExtra("f_name", getallHotelReviewPojoArrayList.get(position).getFirstName());
                intent.putExtra("l_name", getallHotelReviewPojoArrayList.get(position).getLastName());
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(position).getHotelId());
                intent.putExtra("timelineId", "0");
                context.startActivity(intent);


            }
        });


        /* Load all image Activity */
        holder.rl_view_more_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Loader animation*/

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);

                /*Top dish activity intent*/
                Intent intent = new Intent(context, AllDishDisplayActivity.class);
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                intent.putExtra("cat_type", getallHotelReviewPojoArrayList.get(position).getCategoryType());
                context.startActivity(intent);


            }
        });


        /* Load all image Activity */
        holder.top_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Loader animation*/

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);

                /*Top dish activity intent*/

                Intent intent = new Intent(context, AllDishDisplayActivity.class);
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                intent.putExtra("cat_type", getallHotelReviewPojoArrayList.get(position).getCategoryType());

                context.startActivity(intent);


            }
        });

        /* Load all image Activity */
        holder.rl_view_more_avoid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Loader animation*/

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);

                /*Dish to avoid activity intent*/

                Intent intent = new Intent(context, AllDishDisplayActivity.class);
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                context.startActivity(intent);

            }
        });

        /* Load all image Activity */
        holder.avoid_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Loader animation*/
                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);

                /*Dish to avoid activity intent*/
                Intent intent = new Intent(context, AllDishDisplayActivity.class);
                intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(position).getRevratId());
                intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(position).getHotelName());
                context.startActivity(intent);


            }
        });


        /* API for top dish images */
        if (!getallHotelReviewPojoArrayList.get(position).getTopdish().trim().equalsIgnoreCase("")) {

            reviewTopDishPojoArrayList.clear();

            ArrayList<ReviewTopDishPojo> reviewTopDishPojoList = new ArrayList<>();

            try {


                if (getallHotelReviewPojoArrayList.get(position).getTopCount() == 0) {


                } else {
                    holder.rl_top_dish.setVisibility(VISIBLE);


                    reviewTopDishPojoList.clear();

                    List<ReviewTopDishPojo> reviewTopDishPojoList1 = getallHotelReviewPojoArrayList.get(position).getTopdishimage();
                    for (int m = 0; m < getallHotelReviewPojoArrayList.get(position).getTopdishimage().size(); m++) {

                        String avoiddishimage = reviewTopDishPojoList1.get(m).getImg();
                        String avoiddishimagename = reviewTopDishPojoList1.get(m).getDishname();

                        reviewTopDishPojo = new ReviewTopDishPojo(avoiddishimage, avoiddishimagename);
                        reviewTopDishPojoList.add(reviewTopDishPojo);
                    }
                    Log.e("image", "topdishesView-------> " + new Gson().toJson(reviewTopDishPojoList));

                    // reviewTopDishAdapter.notifyDataSetChanged();

                }
                String reviewId = getallHotelReviewPojoArrayList.get(position).getRevratId();
                String hotelName = getallHotelReviewPojoArrayList.get(position).getHotelName();
                String catType = getallHotelReviewPojoArrayList.get(position).getCategoryType();

                LinkedHashSet<ReviewTopDishPojo> s = new LinkedHashSet<>(reviewTopDishPojoList);
                s.addAll(reviewTopDishPojoList);
                reviewTopDishPojoList.clear();
                reviewTopDishPojoList.addAll(s);

                /*Setting top dish adapter*/
                ReviewTopDishAdapter reviewTopDishAdapter = new ReviewTopDishAdapter(context, reviewTopDishPojoList, reviewId, hotelName, catType);
                LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_review_top_dish.setLayoutManager(llm);


                holder.rv_review_top_dish.setItemAnimator(new DefaultItemAnimator());
                holder.rv_review_top_dish.setAdapter(reviewTopDishAdapter);
                holder.rv_review_top_dish.setNestedScrollingEnabled(false);

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        /* Dish to avoid names API */

        if (!getallHotelReviewPojoArrayList.get(position).getAvoiddish().trim().equalsIgnoreCase("")) {


            try {
                ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();

                if (getallHotelReviewPojoArrayList.get(position).getAvoidCount() == 0) {


                } else {
                    holder.rl_avoid_dish.setVisibility(VISIBLE);


                    reviewAvoidDishPojoArrayList.clear();

                    List<ReviewAvoidDishPojo> reviewAmbiImagePojos = getallHotelReviewPojoArrayList.get(position).getAvoiddishimage();
                    for (int m = 0; m < reviewAmbiImagePojos.size(); m++) {

                        String avoiddishimage = reviewAmbiImagePojos.get(m).getImg();
                        String avoiddishimagename = reviewAmbiImagePojos.get(m).getDishname();

                        reviewAvoidDishPojo = new ReviewAvoidDishPojo(avoiddishimage, avoiddishimagename);
                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);
                        Log.e("msg", "abmiViewsize-------> " + reviewAmbiImagePojoArrayList.size());
                        Log.e("msg", "abmiViewimg-------> " + reviewAmbiImagePojos.get(m).getImg());
                    }
                    Log.e("image", "dishestoavoidView-------> " + new Gson().toJson(reviewAvoidDishPojoArrayList));

                }
                String reviewId = getallHotelReviewPojoArrayList.get(position).getRevratId();
                String hotelName = getallHotelReviewPojoArrayList.get(position).getHotelName();
                String catType = getallHotelReviewPojoArrayList.get(position).getCategoryType();

                LinkedHashSet<ReviewAvoidDishPojo> s = new LinkedHashSet<>(reviewAvoidDishPojoArrayList);
                s.addAll(reviewAvoidDishPojoArrayList);
                reviewAvoidDishPojoArrayList.clear();
                reviewAvoidDishPojoArrayList.addAll(s);

                /*Setting avoid dish adapter*/
                reviewWorstDishAdapter = new ReviewAvoidDishAdapter(context, reviewAvoidDishPojoArrayList, reviewId, hotelName, catType);
                LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_review_avoid_dish.setLayoutManager(llm);
                holder.rv_review_avoid_dish.setItemAnimator(new DefaultItemAnimator());
                holder.rv_review_avoid_dish.setAdapter(reviewWorstDishAdapter);
                holder.rv_review_avoid_dish.setNestedScrollingEnabled(false);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        //ambience image
        int ambiImageCount = getallHotelReviewPojoArrayList.get(position).getAmbiImageCount();
        ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

        if (ambiImageCount == 0) {

        } else {

            holder.review_photos_recylerview.setVisibility(VISIBLE);

            reviewAmbiImagePojoArrayList.clear();
            List<ReviewAmbiImagePojo> reviewAmbiImagePojos = getallHotelReviewPojoArrayList.get(position).getAmbiImage();

            for (int m = 0; m < reviewAmbiImagePojos.size(); m++) {

                String ambimg = reviewAmbiImagePojos.get(m).getImg();
                reviewAmbiImagePojo = new ReviewAmbiImagePojo(ambimg);
                reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);

                Log.e("msg", "abmiViewsize-------> " + reviewAmbiImagePojoArrayList.size());
                Log.e("msg", "abmiViewimg-------> " + reviewAmbiImagePojos.get(m).getImg());
            }
            Log.e("image", "ambiencedishesView-------> " + new Gson().toJson(reviewAmbiImagePojoArrayList));

            String reviewId = getallHotelReviewPojoArrayList.get(position).getRevratId();
            String hotelName = getallHotelReviewPojoArrayList.get(position).getHotelName();
            String catType = getallHotelReviewPojoArrayList.get(position).getCategoryType();

            LinkedHashSet<ReviewAmbiImagePojo> s = new LinkedHashSet<>(reviewAmbiImagePojoArrayList);
            s.addAll(reviewAmbiImagePojoArrayList);
            reviewAmbiImagePojoArrayList.clear();
            reviewAmbiImagePojoArrayList.addAll(s);

            /*Setting ambience dish adapter*/
            ambianceImagesAdapter = new ReviewAmbienceAdapter(context, reviewAmbiImagePojoArrayList, reviewId, hotelName, catType);

            LinearLayoutManager llm3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

            holder.review_photos_recylerview.setLayoutManager(llm3);
            holder.review_photos_recylerview.setItemAnimator(new DefaultItemAnimator());
            holder.review_photos_recylerview.setAdapter(ambianceImagesAdapter);
            holder.review_photos_recylerview.setNestedScrollingEnabled(false);
            holder.ll_view_more.setVisibility(VISIBLE);

        }



        //packaging img
        int packImageCount = getallHotelReviewPojoArrayList.get(position).getPackImageCount();
        ArrayList<PackImage> packImagePojoArrayList = new ArrayList<>();

        if (packImageCount == 0) {

        } else {

             holder.review_photos_recylerview.setVisibility(GONE);

            packImagePojoArrayList.clear();


            List<PackImage> packImagePojos = getallHotelReviewPojoArrayList.get(position).getPackImage();
            for (int n = 0; n < getallHotelReviewPojoArrayList.get(position).getPackImage().size(); n++) {

                String ambimg = getallHotelReviewPojoArrayList.get(position).getPackImage().get(n).getImg();
                packImage = new PackImage(ambimg);
                packImagePojoArrayList.add(packImage);

                Log.e("packaging", "packViewsize-------> " + packImagePojos.size());
                Log.e("packaging", "abmiViewimg-------> " + packImagePojos.get(n).getImg());
            }
            Log.e("packaging", "ambiencedishesView-------> " + new Gson().toJson(packImagePojoArrayList));


            String reviewId = getallHotelReviewPojoArrayList.get(position).getRevratId();
            String hotelName = getallHotelReviewPojoArrayList.get(position).getHotelName();
            String catType = getallHotelReviewPojoArrayList.get(position).getCategoryType();


            LinkedHashSet<PackImage> s = new LinkedHashSet<>(packImagePojoArrayList);
            s.addAll(packImagePojoArrayList);
            packImagePojoArrayList.clear();
            packImagePojoArrayList.addAll(s);

            /*Setting ambience dish adapter*/
            PackagingDetailsAdapter packagingDetailsAdapter = new PackagingDetailsAdapter(context, packImagePojoArrayList, reviewId, hotelName, catType);
            LinearLayoutManager llm3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.package_recylerview.setLayoutManager(llm3);
            holder.package_recylerview.setItemAnimator(new DefaultItemAnimator());
            holder.package_recylerview.setAdapter(packagingDetailsAdapter);
            holder.package_recylerview.setNestedScrollingEnabled(false);
            packagingDetailsAdapter.notifyDataSetChanged();
            holder.ll_ambi_more.setVisibility(VISIBLE);


            if (packImagePojoArrayList.isEmpty()) {
                holder.ambi_viewpager.setVisibility(View.GONE);
                holder.rl_ambiance_dot.setVisibility(View.GONE);
                holder.title_ambience.setVisibility(GONE);
                holder.ll_ambi_more.setVisibility(GONE);
                holder.ll_view_more.setVisibility(GONE);
                holder.review_photos_recylerview.setVisibility(GONE);
            } else {

            }
        }

        /* Hotel view more ambience images */
        holder.ll_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.ll_ambi_more.setVisibility(VISIBLE);
                holder.ll_view_more.setVisibility(GONE);

            }
        });


        /* Hotel view more ambience images */
        holder.layout_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.ll_ambi_more.setVisibility(VISIBLE);
                holder.ll_view_more.setVisibility(GONE);

            }
        });


    }


    @Override
    public long getItemId(int position) {
        return position;
    }




    /*Create follower api*/
    private void getFollowers(final int userId) {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<GetFollowersOutput> call = apiService.getFollower("get_follower", userId);

                call.enqueue(new Callback<GetFollowersOutput>() {
                    @Override
                    public void onResponse(Call<GetFollowersOutput> call, Response<GetFollowersOutput> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {
                            getFollowersDataList.clear();

                            //Success

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            for (int i = 0; i < response.body().getData().size(); i++) {

                                if (response.body().getData().get(i).getFollowerId() == null) {

                                } else {
                                    int userid = response.body().getData().get(i).getUserid();
                                    String followerId = response.body().getData().get(i).getFollowerId();
                                    String firstName = response.body().getData().get(i).getFirstName();
                                    String lastName = response.body().getData().get(i).getLastName();
                                    String userPhoto = response.body().getData().get(i).getPicture();

                                    if (!followerId.contains(String.valueOf(userid))) {

                                        GetFollowersData getFollowersData = new GetFollowersData(userid, followerId, firstName, lastName, userPhoto);
                                        getFollowersDataList.add(getFollowersData);
                                    }
                                }


                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<GetFollowersOutput> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

    }


    /* GetFollowers user if interdface */

    public void getfriendslist(View view, int position) {

        if (((CheckBox) view).isChecked()) {

            if (friendsList.contains(Integer.valueOf(getFollowersDataList.get(position).getFollowerId()))) {

                return;

            } else {

                friendsList.add(Integer.valueOf(getFollowersDataList.get(position).getFollowerId()));

            }


        } else {

            friendsList.remove(Integer.valueOf(getFollowersDataList.get(position).getFollowerId()));

        }

    }


    // Interface friends list

    @Override
    public int getItemCount() {

        return getallHotelReviewPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

        Log.e(TAG, "onScrollChange: " + v.getVerticalScrollbarPosition());

    }

    /*Custom alert dialog*/
    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity c;
        public Dialog d;

        TextView txt_edit_review, txt_delete_review;

        CustomDialogClass(Context a) {
            super(a);
            // TODO Auto-generated constructor stub
            context = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.edit_delete_review_layout);


            txt_edit_review = (TextView) findViewById(R.id.txt_edit_review);
            txt_delete_review = (TextView) findViewById(R.id.txt_delete_review);

            txt_edit_review.setOnClickListener(this);
            txt_delete_review.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_edit_review:
                    dismiss();
                    break;
                case R.id.txt_delete_review:



                    dismiss();
                    break;


                default:
                    break;
            }
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // New design view holder


        EmojiconEditText review_comment;

        ViewPager ambi_viewpager;

        ExpandableTextView reviewcmmnt;

        CircleIndicator ambi_indicator;

        RecyclerView review_photos_recylerview, rv_review_top_dish, rv_review_avoid_dish, package_recylerview;

        ImageView userphoto_profile, review_user_photo, img_overall_rating_star,
                img_ambience_star, img_taste_star, img_vfm_star, img_service_star;

        RelativeLayout rl_overall_review, rl_ambiance_dot, rl_hotel_details, rl_avoid_dish, rl_top_dish, rl_view_more_top, rl_view_more_avoid;

        LinearLayout ll_ambi_more, ll_view_more, ll_dish_to_avoid, ll_likecomment, ll_res_overall_rating, ll_ambience_rating, ll_taste_rating,
                ll_value_of_money_rating, ll_service_rating, ll_delivery_mode, ll_top_dish;

        ImageButton img_btn_review_like, layout_more, img_btn_review_comment, img_bucket_list, img_view_more, avoid_more, top_more,img_shareadapter;

        // TextView

        TextView username, txt_mode_delivery, userlastname, count, count_post, comma, txt_followers_count, txt_followers, serv_or_package,
                txt_created_on, tv_restaurant_name, txt_hotel_address, tv_res_overall_rating, tv_ambience_rating, ambi_or_timedelivery,
                txt_taste_count, tv_vfm_rating, tv_service_rating, title_ambience, tv_view_more, tv_top_dishes,
                tv_dishes_to_avoid, title_dish_avoid, title_top_dish, tv_show_less, txt_review_caption,
                tv_like_count, tv_view_all_comments, txt_adapter_post;

        RelativeLayout rl_share_adapter;

        ProgressBar comments_progressbar;

        onCommentsPostListener onCommentsPostListener;

        View hotelDetailsView;

        public MyViewHolder(View itemView, GetHotelDetailsAdapter.onCommentsPostListener onCommentsPostListener) {
            super(itemView);

            this.onCommentsPostListener = onCommentsPostListener;
            this.hotelDetailsView = itemView;

            username = (TextView) itemView.findViewById(R.id.username);
            txt_mode_delivery = (TextView) itemView.findViewById(R.id.txt_mode_delivery);
            userlastname = (TextView) itemView.findViewById(R.id.userlastname);
            count = (TextView) itemView.findViewById(R.id.count);
            count_post = (TextView) itemView.findViewById(R.id.count_post);
            comma = (TextView) itemView.findViewById(R.id.comma);
            txt_followers_count = (TextView) itemView.findViewById(R.id.txt_followers_count);
            txt_followers = (TextView) itemView.findViewById(R.id.txt_followers);
            txt_created_on = (TextView) itemView.findViewById(R.id.txt_created_on);
            tv_restaurant_name = (TextView) itemView.findViewById(R.id.tv_restaurant_name);
            txt_hotel_address = (TextView) itemView.findViewById(R.id.txt_hotel_address);
            tv_res_overall_rating = (TextView) itemView.findViewById(R.id.tv_res_overall_rating);
            tv_ambience_rating = (TextView) itemView.findViewById(R.id.tv_ambience_rating);
            txt_taste_count = (TextView) itemView.findViewById(R.id.txt_taste_count);
            //  tv_ambience_rating = (TextView) itemView.findViewById(R.id.tv_ambience_rating);
            ambi_or_timedelivery = (TextView) itemView.findViewById(R.id.ambi_or_timedelivery);
            serv_or_package = (TextView) itemView.findViewById(R.id.serv_or_package);
            tv_vfm_rating = (TextView) itemView.findViewById(R.id.tv_vfm_rating);
            tv_service_rating = (TextView) itemView.findViewById(R.id.tv_service_rating);
            tv_view_more = (TextView) itemView.findViewById(R.id.tv_view_more);
            title_ambience = (TextView) itemView.findViewById(R.id.title_ambience);
            tv_top_dishes = (TextView) itemView.findViewById(R.id.tv_top_dishes);
            tv_dishes_to_avoid = (TextView) itemView.findViewById(R.id.tv_dishes_to_avoid);
            title_dish_avoid = (TextView) itemView.findViewById(R.id.title_dish_avoid);
            title_top_dish = (TextView) itemView.findViewById(R.id.title_top_dish);
            tv_show_less = (TextView) itemView.findViewById(R.id.tv_show_less);
            tv_like_count = (TextView) itemView.findViewById(R.id.tv_like_count);
            tv_view_all_comments = (TextView) itemView.findViewById(R.id.tv_view_all_comments);
            txt_adapter_post = (TextView) itemView.findViewById(R.id.txt_adapter_post);
            rl_share_adapter = (RelativeLayout) itemView.findViewById(R.id.rl_share_adapter);
            rl_share_adapter.setClickable(true);


            txt_hotel_address = (TextView) itemView.findViewById(R.id.txt_hotel_address);


            ambi_viewpager = (ViewPager) itemView.findViewById(R.id.ambi_viewpager);

            ambi_indicator = (CircleIndicator) itemView.findViewById(R.id.ambi_indicator);
            review_comment = (EmojiconEditText) itemView.findViewById(R.id.review_comment);
            reviewcmmnt = (ExpandableTextView) itemView.findViewById(R.id.hotel_revieww_description);


            txt_review_caption = (TextView) itemView.findViewById(R.id.tv_review_caption);

            //Imageview

            userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);

            img_overall_rating_star = (ImageView) itemView.findViewById(R.id.img_overall_rating_star);
            img_ambience_star = (ImageView) itemView.findViewById(R.id.img_ambience_star);
            img_taste_star = (ImageView) itemView.findViewById(R.id.img_taste_star);
            img_vfm_star = (ImageView) itemView.findViewById(R.id.img_vfm_star);
            img_service_star = (ImageView) itemView.findViewById(R.id.img_service_star);
            userphoto_profile = (ImageView) itemView.findViewById(R.id.userphoto_profile);
            review_user_photo = (ImageView) itemView.findViewById(R.id.review_user_photo);

            //ImageButton
            img_btn_review_like = (ImageButton) itemView.findViewById(R.id.img_btn_review_like);
            img_btn_review_comment = (ImageButton) itemView.findViewById(R.id.img_btn_review_comment);
            img_view_more = (ImageButton) itemView.findViewById(R.id.img_view_more);
            img_bucket_list = (ImageButton) itemView.findViewById(R.id.img_bucket_list);
            top_more = (ImageButton) itemView.findViewById(R.id.top_more);
            avoid_more = (ImageButton) itemView.findViewById(R.id.avoid_more);
            layout_more = (ImageButton) itemView.findViewById(R.id.layout_more);
            img_shareadapter = (ImageButton) itemView.findViewById(R.id.img_shareadapter);


            rl_overall_review = (RelativeLayout) itemView.findViewById(R.id.rl_overall_review);
            ll_likecomment = (LinearLayout) itemView.findViewById(R.id.ll_likecomment);
            ll_res_overall_rating = (LinearLayout) itemView.findViewById(R.id.ll_res_overall_rating);
            ll_dish_to_avoid = (LinearLayout) itemView.findViewById(R.id.ll_dish_to_avoid);
            ll_ambience_rating = (LinearLayout) itemView.findViewById(R.id.ll_ambience_rating);
            ll_taste_rating = (LinearLayout) itemView.findViewById(R.id.ll_taste_rating);
            ll_value_of_money_rating = (LinearLayout) itemView.findViewById(R.id.ll_value_of_money_rating);
            ll_service_rating = (LinearLayout) itemView.findViewById(R.id.ll_service_rating);
            ll_view_more = (LinearLayout) itemView.findViewById(R.id.ll_view_more);
            ll_ambi_more = (LinearLayout) itemView.findViewById(R.id.ll_ambi_more);
            ll_delivery_mode = (LinearLayout) itemView.findViewById(R.id.ll_delivery_mode);
            ll_top_dish = (LinearLayout) itemView.findViewById(R.id.ll_top_dish);


            review_photos_recylerview = (RecyclerView) itemView.findViewById(R.id.review_photos_recylerview);
            package_recylerview = (RecyclerView) itemView.findViewById(R.id.package_recylerview);
            rv_review_top_dish = (RecyclerView) itemView.findViewById(R.id.rv_review_top_dish);
            rv_review_avoid_dish = (RecyclerView) itemView.findViewById(R.id.rv_review_avoid_dish);

            rl_ambiance_dot = (RelativeLayout) itemView.findViewById(R.id.rl_ambiance_dot);
            rl_hotel_details = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_details);
            rl_avoid_dish = (RelativeLayout) itemView.findViewById(R.id.rl_avoid_dish);
            rl_top_dish = (RelativeLayout) itemView.findViewById(R.id.rl_top_dish);
            rl_view_more_top = (RelativeLayout) itemView.findViewById(R.id.rl_view_more_top);

            rl_view_more_avoid = (RelativeLayout) itemView.findViewById(R.id.rl_view_more_avoid);

            comments_progressbar = (ProgressBar) itemView.findViewById(R.id.comments_progressbar);


            txt_adapter_post.setOnClickListener(this);
            img_shareadapter.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            switch (view.getId()) {

                case R.id.txt_adapter_post:

                    onCommentsPostListener.onPostCommitedView(view, getAdapterPosition(), hotelDetailsView);
                    break;

                case R.id.img_shareadapter:

                    onCommentsPostListener.onPostCommitedView(view,getAdapterPosition(),hotelDetailsView);
                    break;
            }
        }
    }

    public interface onCommentsPostListener {

        void onPostCommitedView(View view, int adapterPosition, View hotelDetailsView);
    }


}
