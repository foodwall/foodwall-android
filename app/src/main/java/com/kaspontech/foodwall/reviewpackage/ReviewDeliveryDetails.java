package com.kaspontech.foodwall.reviewpackage;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.reviewpackage.ReviewDeliveryDetailsPojo.ReviewDeliveyDetailsOutput;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewInputAllPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewOutputResponsePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.GetHotelDetailsAdapter;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewDeliveryDetails extends _SwipeActivityClass implements View.OnClickListener{


    private static final String TAG = "ReviewDeliveryDetails";

    Toolbar toolbar;
    RatingBar delivery_rating;
    CoordinatorLayout co_layout;
    AppBarLayout app_bar_layout;
    ImageView img_hotel_background;
    CollapsingToolbarLayout hotel_collapsingtoolbar;

    String fromwhich="ReviewDeliveryDetails";


    int top_count, avoid_count, ambi_image_count, topdishimage_count, avoiddishimage_count,packCount;

    String deliveryName = "",delMode,catType;
    String txt_hotelname, user_id, hotel_id, txt_hotel_address,
            txt_review_username, txt_review_userlastname, userimage, txt_review_follow, txt_review_like, txt_review_comment,
            txt_review_shares, txt_review_caption, hotel_review_rating, hotel_user_rating, total_followers, total_followings,
            hotelId, cat_type, veg_nonveg, revratID, ambiance, taste, total_package, total_timedelivery, service, valuemoney,
            createdby, createdon, googleID, placeID, opentimes, delivery_mode,dishname, topdishimage, topdishimage_name, avoiddishimage_name, avoiddishomage, ambiimage, dishtoavoid,
            category_type, latitude, longitude, phone, package_value, time_delivery, foodexp, photo_reference, emailID, likes_hotelID, revrathotel_likes,
            followingID, totalreview,redirect_url, total_ambiance, total_taste, total_service, modeid, company_name, total_value, total_good, total_bad, total_good_bad_user;
    int top_dish_img_count  ,
            avoid_dish_img_count,bucketlist;

    RecyclerView delivery_details_recylerview;
    LinearLayout ll_nodata;
    GetHotelDetailsAdapter getHotelDetailsAdapter;
    public List<ReviewInputAllPojo> getallHotelReviewPojoArrayList = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_delivery_details);

        delivery_details_recylerview = (RecyclerView)findViewById(R.id.delivery_details_recylerview);

        co_layout = (CoordinatorLayout) findViewById(R.id.co_layout);
        ll_nodata = (LinearLayout) findViewById(R.id.ll_nodata);
        img_hotel_background = (ImageView) findViewById(R.id.img_hotel_background);

        //Toolar

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Collapsing tool bar layout

        hotel_collapsingtoolbar = (CollapsingToolbarLayout) findViewById(R.id.hotel_collapsingtoolbar);
        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        delivery_rating = (RatingBar)findViewById(R.id.delivery_rating);

        Intent intent = getIntent();
        catType = intent.getStringExtra("catType");
        delMode = intent.getStringExtra("delMode");

        Log.e(TAG, "onCreate: "+catType );
        Log.e(TAG, "onCreate: "+delMode );

      /*  if(delMode != null){

            switch (delMode){

                case "1":

                    deliveryName = "Zomato";

                    break;

                case "2":

                    deliveryName = "Swiggy";

                    break;

                case "3":

                    deliveryName = "Food panda";

                    break;

                case "4":

                    deliveryName = "Uber eats";

                    break;


            }

        }
*/



        if(catType != null && delMode != null){

            get_hotel_review_delivery_mode();

            getDeliveryLogo();

        }

        app_bar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {

                    hotel_collapsingtoolbar.setTitle(deliveryName);

                    isShow = true;
                } else if (isShow) {
                    hotel_collapsingtoolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });


    }

    @Override
    public void onSwipeRight() {
       finish();
    }

    @Override
    protected void onSwipeLeft() {

    }

    private void getDeliveryLogo(){

        if (isConnected(ReviewDeliveryDetails.this)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(ReviewDeliveryDetails.this, "userId");

            Call<ReviewDeliveyDetailsOutput> call = apiService.get_hotel_review_delivery_mode_image("get_hotel_review_delivery_mode_image",
                    Integer.parseInt(delMode),
                    Integer.parseInt(userId));
            call.enqueue(new Callback<ReviewDeliveyDetailsOutput>() {
              //  @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewDeliveyDetailsOutput> call, Response<ReviewDeliveyDetailsOutput> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {


                        String logo = response.body().getData().get(0).getCompImg();

//                        Utility.picassoImageLoader(logo,
//                                 0,img_hotel_background);

                        GlideApp.with(getApplicationContext())
                                .load(logo).diskCacheStrategy(DiskCacheStrategy.ALL).thumbnail(0.1f)
                                .into(img_hotel_background);

                        deliveryName=response.body().getData().get(0).getCompName();

                    } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {

                    }

                }

                @Override
                public void onFailure(Call<ReviewDeliveyDetailsOutput> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                   /* img_no_dish.setVisibility(View.VISIBLE);
                    txt_no_dish.setVisibility(View.VISIBLE);*/
                }
            });
        } else {

            Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void get_hotel_review_delivery_mode(){

        if (isConnected(ReviewDeliveryDetails.this)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(ReviewDeliveryDetails.this, "userId");

            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_delivery_mode("get_hotel_review_delivery_mode",
                    Integer.parseInt(catType),
                    Integer.parseInt(delMode),
                    Integer.parseInt(userId));

            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {

                        getallHotelReviewPojoArrayList.clear();


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());


                            try {

                                hotelId = response.body().getData().get(j).getHotelId();
                                cat_type = response.body().getData().get(j).getCategoryType();
                                time_delivery = response.body().getData().get(j).getTimedelivery();
                                veg_nonveg = response.body().getData().get(j).getVegNonveg();
                                revratID = response.body().getData().get(j).getRevratId();
                                modeid = response.body().getData().get(j).getMode_id();
                                company_name = response.body().getData().get(j).getComp_name();
                                ambiance = response.body().getData().get(j).getAmbiance();
                                taste = response.body().getData().get(j).getTaste();
                                total_package = response.body().getData().get(j).getTotalPackage();
                                total_timedelivery = response.body().getData().get(j).getTotalTimedelivery();
                                service = response.body().getData().get(j).getService();
                                valuemoney = response.body().getData().get(j).getValueMoney();
                                createdby = response.body().getData().get(j).getCreatedBy();
                                createdon = response.body().getData().get(j).getCreatedOn();
                                googleID = response.body().getData().get(j).getGoogleId();
                                placeID = response.body().getData().get(j).getPlaceId();
                                opentimes = response.body().getData().get(j).getOpenTimes();
                                category_type = response.body().getData().get(j).getCategoryType();
                                latitude = response.body().getData().get(j).getLatitude();
                                longitude = response.body().getData().get(j).getLongitude();
                                phone = response.body().getData().get(j).getPhone();
                                photo_reference = response.body().getData().get(j).getPhotoReference();
                                emailID = response.body().getData().get(j).getEmail();
                                likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                                revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                                followingID = response.body().getData().get(j).getFollowingId();
                                totalreview = response.body().getData().get(j).getTotalReview();
                                total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                                total_taste = response.body().getData().get(j).getTotalTaste();
                                package_value = response.body().getData().get(j).get_package();
                                total_service = response.body().getData().get(j).getTotalService();
                                total_value = response.body().getData().get(j).getTotalValueMoney();
                                total_good = response.body().getData().get(j).getTotalGood();
                                total_bad = response.body().getData().get(j).getTotalBad();
                                total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();
                                txt_hotelname = response.body().getData().get(j).getHotelName();
                                user_id = response.body().getData().get(j).getUserId();
                                hotel_id = response.body().getData().get(j).getHotelId();
                                txt_hotel_address = response.body().getData().get(j).getAddress();
                                txt_review_username = response.body().getData().get(j).getFirstName();
                                txt_review_userlastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                txt_review_caption = response.body().getData().get(j).getHotelReview();
                                txt_review_like = response.body().getData().get(j).getTotalLikes();
                                txt_review_comment = response.body().getData().get(j).getTotalComments();
                                txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                                hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                                hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                                total_followers = response.body().getData().get(j).getTotalFollowers();
                                total_followings = response.body().getData().get(j).getTotalFollowings();
                                foodexp = response.body().getData().get(j).getFoodExprience();
                                top_count = response.body().getData().get(j).getTopCount();
                                dishtoavoid = response.body().getData().get(j).getAvoiddish();
//                                topdishimage_count= response.body().getData().get(j).getTopdishimageCount();
//                                avoiddishimage_count= response.body().getData().get(j).getAvoiddishimageCount();
                                avoid_count = response.body().getData().get(j).getAvoidCount();
                                ambi_image_count = response.body().getData().get(j).getAmbiImageCount();
                                dishname = response.body().getData().get(j).getTopdish();
                                delivery_mode = response.body().getData().get(j).getDelivery_mode();
                                top_dish_img_count = response.body().getData().get(j).getTop_dish_img_count();
                                avoid_dish_img_count = response.body().getData().get(j).getAvoid_dish_img_count();
                                redirect_url = response.body().getData().get(j).getRedirect_url();
                                packCount = response.body().getData().get(j).getPackImageCount();
//                                bucketlist = response.body().getData().get(j).getBucket_list();


                                String totalDelivery = response.body().getData().get(j).getTotalTimedelivery();

                                if(totalDelivery != null){

                                    int tot_delivery = Integer.parseInt(totalDelivery);

                                    if (tot_delivery == 1) {

                                        delivery_rating.setRating(1f);
                                        delivery_rating.setIsIndicator(true);
                                        delivery_rating.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(ReviewDeliveryDetails.this, R.color.red)));

                                    } else if (tot_delivery == 2) {
                                        delivery_rating.setRating(2f);
                                        delivery_rating.setIsIndicator(true);
                                        delivery_rating.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(ReviewDeliveryDetails.this, R.color.mild_orange)));

                                    } else if (tot_delivery == 3) {
                                        delivery_rating.setRating(3f);
                                        delivery_rating.setIsIndicator(true);
                                        delivery_rating.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(ReviewDeliveryDetails.this, R.color.orange)));

                                    } else if (tot_delivery == 4) {
                                        delivery_rating.setRating(4f);
                                        delivery_rating.setIsIndicator(true);
                                        delivery_rating.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(ReviewDeliveryDetails.this, R.color.light_green)));


                                    } else if (tot_delivery == 5) {

                                        delivery_rating.setRating(5f);
                                        delivery_rating.setIsIndicator(true);
                                        delivery_rating.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(ReviewDeliveryDetails.this, R.color.dark_green)));

                                    }                                }


                                ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();

                                if (top_count == 0) {

                                } else {

                                    for (int b = 0; b < response.body().getData().get(j).getTopCount(); b++) {

                                        topdishimage = response.body().getData().get(j).getTopdishimage().get(b).getImg();
                                        topdishimage_name = response.body().getData().get(j).getTopdishimage().get(b).getDishname();
                                        ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimage_name);
                                        reviewTopDishPojoArrayList.add(reviewTopDishPojo);
                                    }


                                }

                                ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();

                                if (avoid_count == 0) {

                                } else {
                                    for (int a = 0; a < response.body().getData().get(j).getAvoidCount(); a++) {

                                        String dishavoidimage = response.body().getData().get(j).getAvoiddishimage().get(a).getImg();
                                        avoiddishimage_name = response.body().getData().get(j).getAvoiddishimage().get(a).getDishname();
                                        ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(dishavoidimage, avoiddishimage_name);
                                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);
                                    }

                                }


                                ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

                                //Ambiance
                                if (ambi_image_count == 0) {


                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getAmbiImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getAmbiImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(img);
                                        reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);
                                    }


                                }
                                //packaging

                                ArrayList<PackImage> reviewpackImagePojoArrayList = new ArrayList<>();


                                if (packCount == 0) {

                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getPackImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getPackImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        PackImage packImage = new PackImage(img);

                                        reviewpackImagePojoArrayList.add(packImage);
                                    }

                                }

                                ReviewInputAllPojo reviewInputAllPojo = new ReviewInputAllPojo(hotelId, revratID, txt_review_caption, txt_review_like, txt_review_comment,
                                        category_type, veg_nonveg, foodexp, ambiance, modeid, company_name, taste, service, package_value, time_delivery,
                                        valuemoney, createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude, longitude, phone,
                                        photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname, emailID, "", "", "", "",
                                        total_followers, total_followings, userimage, likes_hotelID, revrathotel_likes, followingID, totalreview,
                                        txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service, total_package, total_timedelivery,
                                        total_value, total_good, total_bad, total_good_bad_user, reviewTopDishPojoArrayList, topdishimage_count,
                                        dishname, top_count, reviewAvoidDishPojoArrayList, avoiddishimage_count, dishtoavoid, avoid_count,
                                        reviewAmbiImagePojoArrayList, ambi_image_count, false,delivery_mode , top_dish_img_count  ,
                                        avoid_dish_img_count,bucketlist,redirect_url,reviewpackImagePojoArrayList,packCount);

                                getallHotelReviewPojoArrayList.add(reviewInputAllPojo);



                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        getallHotelReviewPojoArrayList=response.body().getData();

                        getHotelDetailsAdapter = new GetHotelDetailsAdapter(ReviewDeliveryDetails.this, getallHotelReviewPojoArrayList,fromwhich);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ReviewDeliveryDetails.this);
                        delivery_details_recylerview.setLayoutManager(mLayoutManager);
                        delivery_details_recylerview.setItemAnimator(new DefaultItemAnimator());
                        delivery_details_recylerview.setAdapter(getHotelDetailsAdapter);
                        delivery_details_recylerview.setNestedScrollingEnabled(false);
                        getHotelDetailsAdapter.notifyDataSetChanged();


                    } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {
                        /*img_no_dish.setVisibility(View.VISIBLE);
                        txt_no_dish.setVisibility(View.VISIBLE);*/
                    }

                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                    if(getallHotelReviewPojoArrayList.isEmpty()){
                        ll_nodata.setVisibility(View.VISIBLE);
                    }

                }
            });
        } else {

            Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }


    }


    private void get_hotel_review_delivery_mode_resume(){

        if (isConnected(ReviewDeliveryDetails.this)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(ReviewDeliveryDetails.this, "userId");

            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_delivery_mode("get_hotel_review_delivery_mode", Integer.parseInt(catType),Integer.parseInt(delMode), Integer.parseInt(userId));
            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {
                        getallHotelReviewPojoArrayList.clear();


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());


                            try {


                                hotelId = response.body().getData().get(j).getHotelId();
                                cat_type = response.body().getData().get(j).getCategoryType();
                                veg_nonveg = response.body().getData().get(j).getVegNonveg();
                                revratID = response.body().getData().get(j).getRevratId();
                                modeid = response.body().getData().get(j).getMode_id();
                                company_name = response.body().getData().get(j).getComp_name();
                                ambiance = response.body().getData().get(j).getAmbiance();
                                taste = response.body().getData().get(j).getTaste();
                                total_package = response.body().getData().get(j).getTotalPackage();
                                total_timedelivery = response.body().getData().get(j).getTotalTimedelivery();
                                service = response.body().getData().get(j).getService();
                                valuemoney = response.body().getData().get(j).getValueMoney();
                                createdby = response.body().getData().get(j).getCreatedBy();
                                createdon = response.body().getData().get(j).getCreatedOn();
                                googleID = response.body().getData().get(j).getGoogleId();
                                placeID = response.body().getData().get(j).getPlaceId();
                                opentimes = response.body().getData().get(j).getOpenTimes();
                                category_type = response.body().getData().get(j).getCategoryType();
                                latitude = response.body().getData().get(j).getLatitude();
                                longitude = response.body().getData().get(j).getLongitude();
                                phone = response.body().getData().get(j).getPhone();
                                photo_reference = response.body().getData().get(j).getPhotoReference();
                                emailID = response.body().getData().get(j).getEmail();
                                likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                                revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                                followingID = response.body().getData().get(j).getFollowingId();
                                totalreview = response.body().getData().get(j).getTotalReview();
                                total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                                total_taste = response.body().getData().get(j).getTotalTaste();
                                package_value = response.body().getData().get(j).get_package();
                                total_service = response.body().getData().get(j).getTotalService();
                                total_value = response.body().getData().get(j).getTotalValueMoney();
                                total_good = response.body().getData().get(j).getTotalGood();
                                total_bad = response.body().getData().get(j).getTotalBad();
                                total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();
                                txt_hotelname = response.body().getData().get(j).getHotelName();
                                user_id = response.body().getData().get(j).getUserId();
                                hotel_id = response.body().getData().get(j).getHotelId();
                                txt_hotel_address = response.body().getData().get(j).getAddress();
                                txt_review_username = response.body().getData().get(j).getFirstName();
                                txt_review_userlastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                txt_review_caption = response.body().getData().get(j).getHotelReview();
                                txt_review_like = response.body().getData().get(j).getTotalLikes();
                                txt_review_comment = response.body().getData().get(j).getTotalComments();
                                txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                                hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                                hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                                total_followers = response.body().getData().get(j).getTotalFollowers();
                                total_followings = response.body().getData().get(j).getTotalFollowings();
                                foodexp = response.body().getData().get(j).getFoodExprience();
                                top_count = response.body().getData().get(j).getTopCount();
                                dishtoavoid = response.body().getData().get(j).getAvoiddish();
//                                topdishimage_count= response.body().getData().get(j).getTopdishimageCount();
//                                avoiddishimage_count= response.body().getData().get(j).getAvoiddishimageCount();
                                avoid_count = response.body().getData().get(j).getAvoidCount();
                                ambi_image_count = response.body().getData().get(j).getAmbiImageCount();
                                dishname = response.body().getData().get(j).getTopdish();
                                delivery_mode = response.body().getData().get(j).getDelivery_mode();
                                top_dish_img_count = response.body().getData().get(j).getTop_dish_img_count();
                                avoid_dish_img_count = response.body().getData().get(j).getAvoid_dish_img_count();
                                redirect_url = response.body().getData().get(j).getRedirect_url();
                                packCount = response.body().getData().get(j).getPackImageCount();
//                                bucketlist = response.body().getData().get(j).getBucket_list();


                                String totalDelivery = response.body().getData().get(j).getTotalTimedelivery();

                                if(totalDelivery != null){

                                    int tot_delivery = Integer.parseInt(totalDelivery);

                                    if (tot_delivery == 1) {

                                        delivery_rating.setRating(1f);
                                        delivery_rating.setIsIndicator(true);
                                        delivery_rating.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(ReviewDeliveryDetails.this, R.color.red)));

                                    } else if (tot_delivery == 2) {
                                        delivery_rating.setRating(2f);
                                        delivery_rating.setIsIndicator(true);
                                        delivery_rating.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(ReviewDeliveryDetails.this, R.color.mild_orange)));

                                    } else if (tot_delivery == 3) {
                                        delivery_rating.setRating(3f);
                                        delivery_rating.setIsIndicator(true);
                                        delivery_rating.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(ReviewDeliveryDetails.this, R.color.orange)));

                                    } else if (tot_delivery == 4) {
                                        delivery_rating.setRating(4f);
                                        delivery_rating.setIsIndicator(true);
                                        delivery_rating.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(ReviewDeliveryDetails.this, R.color.light_green)));


                                    } else if (tot_delivery == 5) {

                                        delivery_rating.setRating(5f);
                                        delivery_rating.setIsIndicator(true);
                                        delivery_rating.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(ReviewDeliveryDetails.this, R.color.dark_green)));

                                    }                                }


                                ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();

                                if (top_count == 0) {

                                } else {

                                    for (int b = 0; b < response.body().getData().get(j).getTopCount(); b++) {

                                        topdishimage = response.body().getData().get(j).getTopdishimage().get(b).getImg();
                                        topdishimage_name = response.body().getData().get(j).getTopdishimage().get(b).getDishname();
                                        ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimage_name);
                                        reviewTopDishPojoArrayList.add(reviewTopDishPojo);
                                    }


                                }

                                ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();

                                if (avoid_count == 0) {

                                } else {
                                    for (int a = 0; a < response.body().getData().get(j).getAvoidCount(); a++) {

                                        String dishavoidimage = response.body().getData().get(j).getAvoiddishimage().get(a).getImg();
                                        avoiddishimage_name = response.body().getData().get(j).getAvoiddishimage().get(a).getDishname();
                                        ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(dishavoidimage, avoiddishimage_name);
                                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);
                                    }

                                }


                                ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

                                //Ambiance
                                if (ambi_image_count == 0) {


                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getAmbiImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getAmbiImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(img);
                                        reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);
                                    }


                                }

                                //packaging

                                ArrayList<PackImage> reviewpackImagePojoArrayList = new ArrayList<>();


                                if (packCount == 0) {

                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getPackImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getPackImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        PackImage packImage = new PackImage(img);

                                        reviewpackImagePojoArrayList.add(packImage);
                                    }

                                }
                                ReviewInputAllPojo reviewInputAllPojo = new ReviewInputAllPojo(hotelId, revratID, txt_review_caption, txt_review_like, txt_review_comment,
                                        category_type, veg_nonveg, foodexp, ambiance, modeid, company_name, taste, service, package_value, time_delivery,
                                        valuemoney, createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude, longitude, phone,
                                        photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname, emailID, "", "", "", "",
                                        total_followers, total_followings, userimage, likes_hotelID, revrathotel_likes, followingID, totalreview,
                                        txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service, total_package, total_timedelivery,
                                        total_value, total_good, total_bad, total_good_bad_user, reviewTopDishPojoArrayList, topdishimage_count,
                                        dishname, top_count, reviewAvoidDishPojoArrayList, avoiddishimage_count, dishtoavoid, avoid_count,
                                        reviewAmbiImagePojoArrayList, ambi_image_count, false,delivery_mode , top_dish_img_count  ,
                                        avoid_dish_img_count, bucketlist,redirect_url,reviewpackImagePojoArrayList,packCount);

                                getallHotelReviewPojoArrayList.add(reviewInputAllPojo);



                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        getHotelDetailsAdapter = new GetHotelDetailsAdapter(ReviewDeliveryDetails.this, getallHotelReviewPojoArrayList,fromwhich);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ReviewDeliveryDetails.this);
                        delivery_details_recylerview.setLayoutManager(mLayoutManager);
                        delivery_details_recylerview.setItemAnimator(new DefaultItemAnimator());
                        delivery_details_recylerview.setAdapter(getHotelDetailsAdapter);
                        delivery_details_recylerview.setNestedScrollingEnabled(false);
                        getHotelDetailsAdapter.notifyDataSetChanged();


                    } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {
                        /*img_no_dish.setVisibility(View.VISIBLE);
                        txt_no_dish.setVisibility(View.VISIBLE);*/
                    }

                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                   /* img_no_dish.setVisibility(View.VISIBLE);
                    txt_no_dish.setVisibility(View.VISIBLE);*/
                }
            });
        } else {

            Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }


    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!getallHotelReviewPojoArrayList.isEmpty()){

//            getHotelDetailsAdapter.notifyDataSetChanged();

            get_hotel_review_delivery_mode_resume();
        }else{
            get_hotel_review_delivery_mode();
        }

    }

    @Override
    public void onClick(View v) {

    }


}
