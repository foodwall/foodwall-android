package com.kaspontech.foodwall.reviewpackage;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.kaspontech.foodwall.R;

import java.util.ArrayList;
import java.util.List;

public class PlacesRecyclerViewAdapter extends
        RecyclerView.Adapter<PlacesRecyclerViewAdapter.ViewHolder> {

    private ArrayList<Place> placelist;
    private List<Integer> restaurantList;
    private Context context;
    Place place;

    public PlacesRecyclerViewAdapter(Context ctx, ArrayList<Place> list) {
        this.context = ctx;
        this.placelist = list;
    }


    @Override
    public int getItemCount() {
        return placelist.size();
    }

    @NonNull
    @Override
    public PlacesRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.places_item, parent, false);

        PlacesRecyclerViewAdapter.ViewHolder viewHolder = new PlacesRecyclerViewAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PlacesRecyclerViewAdapter.ViewHolder holder, final int position) {
        final int itemPos = position;


        place = placelist.get(position);


      /*  holder.name.setText(placelist.get(position).getName());
        holder.address.setText(placelist.get(position).getAddress());
        holder.phone.setText(placelist.get(position).getPhoneNumber());
        if (placelist.get(position).getWebsiteUri() != null) {
            holder.website.setText(placelist.get(position).getWebsiteUri().toString());
        }

        if (placelist.get(position).getRating() > -1) {
            holder.ratingBar.setNumStars((int) placelist.get(position).getRating());
        } else {
            holder.ratingBar.setVisibility(View.GONE);
        }*/

        restaurantList = placelist.get(position).getPlaceTypes();


//        Log.e("placetype-->",""+placelist.get(position).getPlaceTypes().get(position).intValue());
/*
try {
    if (placelist.get(position).getPlaceTypes().get(position) == 79) {*/
        holder.name.setText(placelist.get(position).getName());
        holder.address.setText(placelist.get(position).getAddress());
        holder.phone.setText(placelist.get(position).getPhoneNumber());
        if (placelist.get(position).getWebsiteUri() != null) {
            holder.website.setText(placelist.get(position).getWebsiteUri().toString());
        }
        if (placelist.get(position).getRating() > -1) {
            holder.ratingBar.setNumStars((int) placelist.get(position).getRating());
        } else {
            holder.ratingBar.setVisibility(View.GONE);
        }
/*    }
}catch (Exception e){
    e.printStackTrace();
}*/




        holder.viewOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showOnMap(placelist.get(position));
            }
        });

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView address;
        TextView phone;
        TextView website;
        RatingBar ratingBar;
        Button viewOnMap;

        public ViewHolder(View view) {

            super(view);

            name = view.findViewById(R.id.name);
            address = view.findViewById(R.id.address);
            phone = view.findViewById(R.id.phone);
            website = view.findViewById(R.id.website);
            ratingBar = view.findViewById(R.id.rating);
            viewOnMap = view.findViewById(R.id.view_map_b);
        }
    }

    private void showOnMap(Place place) {
        android.support.v4.app.FragmentManager fm = ((New_restaurant) context)
                .getSupportFragmentManager();

        Bundle bundle=new Bundle();
        bundle.putString("name", (String)place.getName());
        bundle.putString("address", (String)place.getAddress());
        bundle.putDouble("lat", place.getLatLng().latitude);
        bundle.putDouble("lng", place.getLatLng().longitude);

        Map_fragment placeFragment = new Map_fragment();
        placeFragment.setArguments(bundle);
        fm.beginTransaction().replace(R.id.map_frame, placeFragment).commit();
    }
}
