package com.kaspontech.foodwall.reviewpackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.kaspontech.foodwall.R;

public class View_restaurant_details extends AppCompatActivity implements View.OnClickListener {
    String TAG = "View_restaurant_details";

    CoordinatorLayout co_layout;
    AppBarLayout app_bar_layout;
    CollapsingToolbarLayout hotel_collapsingtoolbar;
    ImageView img_hotel_background;
    Toolbar toolbar;
    ProgressBar view_hotel_progress;
    NestedScrollView hotel_scroll;
    TextView hotel_name, hotel_location, htl_open,hotel_rating,hotel_opened;
    String Hotel_name, Hotel_location, Hotel_icon, Hotel_rating,googleid,placeid;
    RatingBar rtng_hotel;
    SeekBar seekbar_hotel;
//    FloatingActionMenu fab_menu;
    android.support.design.widget.FloatingActionButton fab_menu;
    FloatingActionButton fab_write_review;
    boolean open;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_restaurant);
        co_layout = (CoordinatorLayout) findViewById(R.id.co_layout);


        img_hotel_background = (ImageView) findViewById(R.id.img_hotel_background);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        view_hotel_progress = (ProgressBar) findViewById(R.id.view_hotel_progress);
        hotel_scroll = (NestedScrollView) findViewById(R.id.hotel_scroll);
        hotel_name = (TextView) findViewById(R.id.hotel_name);
        htl_open = (TextView) findViewById(R.id.htl_open);
        hotel_opened = (TextView) findViewById(R.id.hotel_opened);
        hotel_rating = (TextView) findViewById(R.id.hotel_rating);
        hotel_location = (TextView) findViewById(R.id.hotel_location);
        rtng_hotel = (RatingBar) findViewById(R.id.rtng_hotel);
        seekbar_hotel = (SeekBar) findViewById(R.id.seekbar_hotel);

        fab_menu = (android.support.design.widget.FloatingActionButton) findViewById(R.id.fab_menu);
        fab_menu.setOnClickListener(this);
/*
        fab_write_review = (FloatingActionButton) findViewById(R.id.fab_write_review);
        fab_write_review.setOnClickListener(this);
*/

        //From adapter
        Intent intent = getIntent();
        Hotel_name = intent.getStringExtra("Hotel_name");
        Hotel_location = intent.getStringExtra("Hotel_location");
        Hotel_icon = intent.getStringExtra("Hotel_icon");
        Hotel_rating = intent.getStringExtra("Hotel_rating");
        placeid = intent.getStringExtra("placeid");
        googleid = intent.getStringExtra("googleid");
try {
    hotel_name.setText(Hotel_name);
    hotel_location.setText(Hotel_location);

/*
      String open= Pref_storage.getDetail(this,"Hotel_open");

        if(open.equalsIgnoreCase("true")){
            htl_open.setText(R.string.open);
            hotel_opened.setVisibility(View.VISIBLE);
            htl_open.setVisibility(View.VISIBLE);
        }else {
            htl_open.setText(R.string.closed);
            hotel_opened.setVisibility(View.VISIBLE);
            htl_open.setVisibility(View.VISIBLE);
        }*/




  /*  if (Hotel_rating == null) {
        rtng_hotel.setVisibility(View.GONE);
        hotel_rating.setVisibility(View.GONE);

    } else {
        rtng_hotel.setRating(Float.parseFloat(Hotel_rating));
    }*/


    rtng_hotel.setVisibility(View.GONE);
    hotel_rating.setVisibility(View.GONE);


    Image_load_activity.loadGooglePhoto(this, img_hotel_background, Hotel_icon);


}catch (Exception e){
    e.printStackTrace();
}



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        hotel_collapsingtoolbar = (CollapsingToolbarLayout) findViewById(R.id.hotel_collapsingtoolbar);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    hotel_collapsingtoolbar.setTitle(Hotel_name);
                    isShow = true;
                } else if (isShow) {
                    hotel_collapsingtoolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
    /*        case R.id.fab_write_review:
                Intent intent1 = new Intent(View_restaurant_details.this, ReviewTestActivity.class);
                intent1.putExtra("Hotel_name", Hotel_name);
                intent1.putExtra("Hotel_location", Hotel_location);
                intent1.putExtra("Hotel_icon", Hotel_icon);
                intent1.putExtra("Hotel_rating", Hotel_rating);
                intent1.putExtra("placeid", placeid);
                intent1.putExtra("googleid", googleid);
                startActivity(intent1);
                finish();
//                fab_menu.close(true);
                fab_menu.hide();

                break;*/

                case R.id.fab_menu:
                Intent intent2 = new Intent(View_restaurant_details.this, ReviewTestActivity.class);
                    intent2.putExtra("Hotel_name", Hotel_name);
                    intent2.putExtra("Hotel_location", Hotel_location);
                    intent2.putExtra("Hotel_icon", Hotel_icon);
                    intent2.putExtra("Hotel_rating", Hotel_rating);
                    intent2.putExtra("placeid", placeid);
                    intent2.putExtra("googleid", googleid);
                startActivity(intent2);
                finish();
//                fab_menu.close(true);

                break;

        }

    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    public void redirect_newReview(View view) {
        Intent intent2 = new Intent(View_restaurant_details.this, ReviewTestActivity.class);
        intent2.putExtra("Hotel_name", Hotel_name);
        intent2.putExtra("Hotel_location", Hotel_location);
        intent2.putExtra("Hotel_icon", Hotel_icon);
        intent2.putExtra("Hotel_rating", Hotel_rating);
        intent2.putExtra("placeid", placeid);
        intent2.putExtra("googleid", googleid);
        startActivity(intent2);
        finish();
    }
}
