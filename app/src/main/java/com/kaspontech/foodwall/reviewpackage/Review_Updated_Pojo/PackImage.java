package com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PackImage {

    @SerializedName("img")
    @Expose
    private String img;

    /**
     * No args constructor for use in serialization
     *
     */
    public PackImage() {
    }

    /**
     *
     * @param img
     */
    public PackImage(String img) {
        super();
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

}