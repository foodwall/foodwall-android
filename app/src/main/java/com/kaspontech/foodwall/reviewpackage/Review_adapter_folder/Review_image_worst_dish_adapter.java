package com.kaspontech.foodwall.reviewpackage.Review_adapter_folder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.R;
//
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

//

public class Review_image_worst_dish_adapter extends RecyclerView.Adapter<Review_image_worst_dish_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    Context context;

    ArrayList<Review_image_pojo> getttingimagelist = new ArrayList<>();

    Review_image_pojo reviewImagePojo;

    Pref_storage pref_storage;

    private EmojiconEditText emojiconEditText;

    private AvoidDishRemovedListener avoidDishRemovedListener;

    public interface AvoidDishRemovedListener {

        void removedAvoidImageLisenter(ArrayList<Review_image_pojo> imagesList);

    }


    public Review_image_worst_dish_adapter(Context c, ArrayList<Review_image_pojo> gettinglist, AvoidDishRemovedListener avoidDishRemovedListener, EmojiconEditText emojiconEditText) {
        this.context = c;
        this.getttingimagelist = gettinglist;
        this.avoidDishRemovedListener = avoidDishRemovedListener;
        this.emojiconEditText = emojiconEditText;
    }


    @NonNull
    @Override
    public Review_image_worst_dish_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_review_image, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Review_image_worst_dish_adapter.MyViewHolder vh = new Review_image_worst_dish_adapter.MyViewHolder(v);

        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Review_image_worst_dish_adapter.MyViewHolder holder, final int position) {


        reviewImagePojo = getttingimagelist.get(position);

//
//        Utility.picassoImageLoader(getttingimagelist.get(position).getImage(),
//                0,holder.image_review);

        emojiconEditText.setText("");

        GlideApp.with(context)
                .load(getttingimagelist.get(position).getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(holder.image_review);

        holder.txt_image.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(reviewImagePojo.getImage_text()));


        holder.remove_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getttingimagelist.remove(position);
                notifyDataSetChanged();
                avoidDishRemovedListener.removedAvoidImageLisenter(getttingimagelist);

            }
        });


    }


    @Override
    public int getItemCount() {
        return getttingimagelist.size();

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //TextView

        TextView txt_image;

        //Imageview

        ImageView image_review;
        ImageButton remove_image;

        RelativeLayout rl_hotel_datas;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt_image = (TextView) itemView.findViewById(R.id.txt_image);
            image_review = (ImageView) itemView.findViewById(R.id.image_review);
            remove_image = (ImageButton) itemView.findViewById(R.id.remove_image);
            rl_hotel_datas = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_datas);
        }


    }


}
