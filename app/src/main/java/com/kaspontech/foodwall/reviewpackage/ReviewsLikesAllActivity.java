package com.kaspontech.foodwall.reviewpackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_adapter.Reviews_like_adapter;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_all_hotel_likes_all_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_hotel_likes_output_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Vishnu on 18-06-2018.
 */

public class ReviewsLikesAllActivity extends AppCompatActivity implements View.OnClickListener {
    /**
     * Application TAG
     */
    private static final String TAG = "Revs_likes_all";

    /**
     * Action Bar
     */
    Toolbar actionBar;
    /**
     * Back button
     */
    ImageButton back;
    /**
     * Likes relative layout
     */
    RelativeLayout rlLikes;
    /**
     * Recycler view for likes
     */
    RecyclerView rvLikes;
    /**
     * Linear layout manager
     */
    LinearLayoutManager llm;
    /**
     * Toolbar title and next text
     */
    TextView toolbarNext;
    TextView toolbarTitle;

    /**
     * Likes adapter - Review
     */
    Reviews_like_adapter likesAdapter;

    /**
     * Review likes pojo
     */
    Get_all_hotel_likes_all_pojo getAllHotelLikesAllPojo;

    /**
     * Loader for likes
     */
    ProgressBar progressDialogReply;
    /**
     * Arraylist
     */
    ArrayList<Get_all_hotel_likes_all_pojo> likelist = new ArrayList<>();

    /**
     * String results of user details
     */
    String txtUsernamelike;

    String txtLastusernamelike;

    String likeuserimage;
    /**
     * Integer results of hotel id,review id & userid
     */
    int hotelid;

    int reviewid;

    int userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes_layout);

        /*Widgets initialization*/

        actionBar = findViewById(R.id.actionbar_like);

        /*Back button initialization*/
        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        /*Relative layout for likes*/

        rlLikes = findViewById(R.id.rl_likes);

        /*Recycler view for reviwe likes*/

        rvLikes = findViewById(R.id.rv_likes);

        /*Loader*/
        progressDialogReply = findViewById(R.id.progress_dialog_reply);

        /*Title text view*/

        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setVisibility(View.VISIBLE);

        /*Title next view visibilty*/

        toolbarNext = actionBar.findViewById(R.id.toolbar_next);
        toolbarNext.setVisibility(View.GONE);


        //Setting Adapter

        likesAdapter = new Reviews_like_adapter(ReviewsLikesAllActivity.this, likelist);
        llm = new LinearLayoutManager(this);
        rvLikes.setLayoutManager(llm);
        rvLikes.setAdapter(likesAdapter);
        rvLikes.setItemAnimator(new DefaultItemAnimator());
        rvLikes.setNestedScrollingEnabled(false);

        /*Getting Input values of for hotel id,review id, user id various adapters*/

        Intent intent = getIntent();
        hotelid = Integer.parseInt(intent.getStringExtra("hotelid"));
        reviewid = Integer.parseInt(intent.getStringExtra("reviewid"));
        userid = Integer.parseInt(intent.getStringExtra("userid"));




        /* Calling Review likes All API */

        progressDialogReply.setIndeterminate(true);
        progressDialogReply.setVisibility(View.VISIBLE);

        getHotelReviewLikesAll();


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        /*Back button on click listener*/
        if (id == R.id.back) {
            finish();
        }


    }




    /* Calling Review Likes API */

    private void getHotelReviewLikesAll() {
        /*Checking whether mobile connectivity is there or not*/

        if (Utility.isConnected(this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(ReviewsLikesAllActivity.this, "userId");

                Call<Get_hotel_likes_output_pojo> call = apiService.get_hotel_review_likes_all("get_hotel_review_likes_all", hotelid, reviewid, Integer.parseInt(createdby));
                call.enqueue(new Callback<Get_hotel_likes_output_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Get_hotel_likes_output_pojo> call, @NonNull Response<Get_hotel_likes_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            progressDialogReply.setVisibility(View.GONE);

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                txtUsernamelike = response.body().getData().get(j).getFirstName();
                                txtLastusernamelike = response.body().getData().get(j).getLastName();
                                String userId = response.body().getData().get(j).getUserId();
                                String hotelId = response.body().getData().get(j).getHotelId();
                                String googleId = response.body().getData().get(j).getGoogleId();
                                String hotelName = response.body().getData().get(j).getHotelName();
                                String placeId = response.body().getData().get(j).getPlaceId();
                                String categoryType = response.body().getData().get(j).getCategoryType();
                                String openTimes = response.body().getData().get(j).getOpenTimes();
                                String address = response.body().getData().get(j).getAddress();
                                String latitude = response.body().getData().get(j).getLatitude();
                                String longitude = response.body().getData().get(j).getLongitude();
                                String phone = response.body().getData().get(j).getPhone();
                                String photoReference = response.body().getData().get(j).getPhotoReference();
                                String revratId = response.body().getData().get(j).getRevratId();
                                String likesHtlId = response.body().getData().get(j).getLikesHtlId();
                                String htlRevLikes = response.body().getData().get(j).getHtlRevLikes();
                                String createdBy = response.body().getData().get(j).getCreatedBy();
                                String createdOn = response.body().getData().get(j).getCreatedOn();
                                String hotelReview = response.body().getData().get(j).getHotelReview();
                                String totalLikes = response.body().getData().get(j).getTotalLikes();
                                String totalComments = response.body().getData().get(j).getTotalComments();
                                String email = response.body().getData().get(j).getEmail();
                                String picture = response.body().getData().get(j).getPicture();


                                likeuserimage = response.body().getData().get(j).getPicture();


                                getAllHotelLikesAllPojo = new Get_all_hotel_likes_all_pojo(hotelId, googleId, hotelName,
                                        placeId, categoryType, openTimes, address, latitude, longitude, phone, photoReference,
                                        revratId, likesHtlId, htlRevLikes, createdBy, createdOn,
                                        hotelReview, totalLikes, totalComments, userId, txtUsernamelike, txtLastusernamelike, email, "", "",
                                        "", picture);

                                likelist.add(getAllHotelLikesAllPojo);
                            }

                            likesAdapter.notifyDataSetChanged();
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Get_hotel_likes_output_pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e(TAG, "FailureError-->" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {


            Toast.makeText(getApplicationContext(), "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();

        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
