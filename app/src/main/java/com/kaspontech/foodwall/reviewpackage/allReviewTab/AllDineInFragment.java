/*
package com.kaspontech.foodwall.ReviewPackage.AllReviewTab;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.ReviewPackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.ReviewPackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.ReviewPackage.Review_Updated_Pojo.ReviewInputAllPojo;
import com.kaspontech.foodwall.ReviewPackage.Review_Updated_Pojo.ReviewOutputResponsePojo;
import com.kaspontech.foodwall.ReviewPackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.ReviewPackage.Review_Updater_adapter.GetHotelDetailsAdapter;
import com.kaspontech.foodwall.Utills.Pref_storage;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllDineInFragment extends Fragment {

    View view;
    Context context;

    TextView view_more, cat_rating, caption, showless;
    LinearLayout images;
    RecyclerView rv_review;

    String fromwhich;



    // Hotel details pojo
    ReviewInputAllPojo reviewInputAllPojo;
    // Top dish pojo
    ReviewTopDishPojo reviewTopDishPojo;

    //Avoid dish pojo

    ReviewAvoidDishPojo reviewAvoidDishPojo;

    //Ambience dish pojo
    ReviewAmbiImagePojo reviewAmbiImagePojo;


    // getallHotelReviewPojoArrayList
    ArrayList<ReviewInputAllPojo> getallHotelReviewPojoArrayList = new ArrayList<>();

    // topDishImagePojoArrayList
    ArrayList<ReviewTopDishPojo> topDishImagePojoArrayList = new ArrayList<>();

    //avoidDishImagePojoArrayList
    ArrayList<ReviewAvoidDishPojo> avoidDishImagePojoArrayList = new ArrayList<>();

    //ambiImagePojoArrayList
    ArrayList<ReviewAmbiImagePojo> ambiImagePojoArrayList = new ArrayList<>();

    ImageView img_no_dish;
    TextView txt_no_dish;


    RelativeLayout rl_hotel;


    int top_count,avoid_count,ambi_image_count,topdishimage_count,avoiddishimage_count,newhotelId;

    //    Get_all_hotel_review_adapter get_all_hotel_review_adapter;
    GetHotelDetailsAdapter getHotelDetailsAdapter;


    ProgressBar progressbar_review;


    String txt_hotelname, user_id, hotel_id, txt_hotel_address,avoiddishimage_name,topdishimage_name,topdishimage,delivery_mode,dishname,dishtoavoid,
            txt_review_username, txt_review_userlastname, userimage, txt_review_follow, txt_review_like, txt_review_comment,
            txt_review_shares, txt_review_caption, hotel_review_rating, hotel_user_rating, total_followers,total_followings,
            hotelId, cat_type, veg_nonveg, revratID, ambiance,modeid,comp_name, taste,total_package,total_timedelivery, service, valuemoney,
            createdby, createdon, googleID, placeID, opentimes,
            category_type, latitude, longitude, phone, package_value,time_delivery,foodexp,photo_reference, emailID, likes_hotelID, revrathotel_likes,
            followingID, totalreview, total_ambiance, total_taste, total_service, total_value, total_good, total_bad, total_good_bad_user;

    public AllDineInFragment() {

        // Required empty public constructor


    }


    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState){

        // Inflate the layout for this fragment
//        view = inflater.inflate(R.layout.fragment_dine_in, container, false);
        view = inflater.inflate(R.layout.activity_hotel_review_all, container, false);
        context = view.getContext();

        rv_review = (RecyclerView) view.findViewById(R.id.rv_review);
        progressbar_review = (ProgressBar) view.findViewById(R.id.progressbar_review);


        img_no_dish = (ImageView) view.findViewById(R.id.img_no_dish);

        txt_no_dish = (TextView) view.findViewById(R.id.txt_no_dish);



    */
/*    ReviewInsideHotelDetails activity = (ReviewInsideHotelDetails) getActivity();
        newhotelId = Integer.parseInt(activity.getHotel_id());*//*

//Category based hotel review


//            Toast.makeText(context, "OncreateDINE", Toast.LENGTH_SHORT).show();

        if(getallHotelReviewPojoArrayList.size()==0){

            get_hotel_review();
        }
        else{
            getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList,fromwhich);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            rv_review.setLayoutManager(mLayoutManager);
            rv_review.setItemAnimator(new DefaultItemAnimator());
            rv_review.setAdapter(getHotelDetailsAdapter);
            rv_review.setNestedScrollingEnabled(false);
            getHotelDetailsAdapter.notifyDataSetChanged();
            progressbar_review.setVisibility(View.GONE);



        }


        return view;

    }



//get_hotel_review API CALL

    private void get_hotel_review() {
        if(isConnected(context)){
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");

            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review("get_hotel_review", newhotelId, Integer.parseInt(userId));
            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    if (response.body().getResponseCode() == 1&&response.body().getResponseMessage().equalsIgnoreCase("success")) {
                        getallHotelReviewPojoArrayList.clear();



                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());


                            try {


                                hotelId = response.body().getData().get(j).getHotelId();
                                cat_type = response.body().getData().get(j).getCategoryType();
                                veg_nonveg = response.body().getData().get(j).getVegNonveg();
                                revratID = response.body().getData().get(j).getRevratId();
                                ambiance = response.body().getData().get(j).getAmbiance();
                                modeid = response.body().getData().get(j).getMode_id();
                                comp_name = response.body().getData().get(j).getComp_name();
                                taste = response.body().getData().get(j).getTaste();
                                total_package=response.body().getData().get(j).getTotalPackage();
                                total_timedelivery=response.body().getData().get(j).getTotalTimedelivery();
                                service = response.body().getData().get(j).getService();
                                valuemoney = response.body().getData().get(j).getValueMoney();
                                createdby = response.body().getData().get(j).getCreatedBy();
                                createdon = response.body().getData().get(j).getCreatedOn();
                                googleID = response.body().getData().get(j).getGoogleId();
                                placeID = response.body().getData().get(j).getPlaceId();
                                opentimes = response.body().getData().get(j).getOpenTimes();
                                category_type = response.body().getData().get(j).getCategoryType();
                                latitude = response.body().getData().get(j).getLatitude();
                                longitude = response.body().getData().get(j).getLongitude();
                                phone = response.body().getData().get(j).getPhone();
                                photo_reference = response.body().getData().get(j).getPhotoReference();
                                emailID = response.body().getData().get(j).getEmail();
                                likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                                revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                                followingID = response.body().getData().get(j).getFollowingId();
                                totalreview = response.body().getData().get(j).getTotalReview();
                                total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                                total_taste = response.body().getData().get(j).getTotalTaste();
                                package_value = response.body().getData().get(j).get_package();
                                total_service = response.body().getData().get(j).getTotalService();
                                total_value = response.body().getData().get(j).getTotalValueMoney();
                                total_good = response.body().getData().get(j).getTotalGood();
                                total_bad = response.body().getData().get(j).getTotalBad();
                                total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();

                                txt_hotelname = response.body().getData().get(j).getHotelName();
                                user_id = response.body().getData().get(j).getUserId();
                                hotel_id = response.body().getData().get(j).getHotelId();
                                txt_hotel_address = response.body().getData().get(j).getAddress();

                                txt_review_username = response.body().getData().get(j).getFirstName();
                                txt_review_userlastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                txt_review_caption = response.body().getData().get(j).getHotelReview();
                                txt_review_like = response.body().getData().get(j).getTotalLikes();
                                txt_review_comment = response.body().getData().get(j).getTotalComments();
                                txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                                hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                                hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                                total_followers = response.body().getData().get(j).getTotalFollowers();
                                total_followings = response.body().getData().get(j).getTotalFollowings();
                                foodexp = response.body().getData().get(j).getFoodExprience();
                                top_count= response.body().getData().get(j).getTopCount();
                                dishtoavoid= response.body().getData().get(j).getAvoiddish();
//                                topdishimage_count= response.body().getData().get(j).getTopdishimageCount();
//                                avoiddishimage_count= response.body().getData().get(j).getAvoiddishimageCount();
                                avoid_count= response.body().getData().get(j).getAvoidCount();
                                ambi_image_count= response.body().getData().get(j).getAmbiImageCount();
                                dishname = response.body().getData().get(j).getTopdish();
                                delivery_mode = response.body().getData().get(j).getDelivery_mode();
                                //Top dish

                                if(top_count==0){

                                }else {
                                    for (int b=0;b<response.body().getData().get(j).getTopdishimage().size();b++){
                                        topdishimage= response.body().getData().get(j).getTopdishimage().get(b).getImg();
                                        topdishimage_name= response.body().getData().get(j).getTopdishimage().get(b).getDishname();
                                        reviewTopDishPojo= new ReviewTopDishPojo(topdishimage,topdishimage_name);
                                        topDishImagePojoArrayList.add(reviewTopDishPojo);
                                    }

                                }
                                //Avoid count

                                if(avoid_count==0){

                                }else{

                                    for (int a=0;a<response.body().getData().get(j).getAvoiddishimage().size();a++){

                                        String dishavoidimage= response.body().getData().get(j).getAvoiddishimage().get(a).getImg();
                                        avoiddishimage_name= response.body().getData().get(j).getAvoiddishimage().get(a).getDishname();
                                        reviewAvoidDishPojo= new ReviewAvoidDishPojo(dishavoidimage,avoiddishimage_name);
                                        avoidDishImagePojoArrayList.add(reviewAvoidDishPojo);
                                    }

                                }





                                //Ambiance
                                if (ambi_image_count==0){


                                }else {

                                    for (int l=0;l<response.body().getData().get(j).getAmbiImage().size();l++){
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getAmbiImage().get(l).getImg();
                                        Log.e("ambi_all", "-->"+img );
                                        reviewAmbiImagePojo= new ReviewAmbiImagePojo(img);

                                        ambiImagePojoArrayList.add(reviewAmbiImagePojo);
                                    }


                                }






                                reviewInputAllPojo = new ReviewInputAllPojo(hotelId,revratID,txt_review_caption,txt_review_like,txt_review_comment,
                                        category_type,veg_nonveg,foodexp,ambiance,modeid,comp_name,taste,service,package_value,time_delivery,
                                        valuemoney,createdby,createdon,googleID,txt_hotelname,placeID,opentimes,txt_hotel_address,latitude,longitude,phone,
                                        photo_reference,user_id,"","",txt_review_username,txt_review_userlastname,emailID,"","","","",
                                        total_followers,total_followings,userimage,likes_hotelID,revrathotel_likes,followingID,totalreview,
                                        txt_review_follow,hotel_review_rating,total_ambiance,total_taste,total_service,total_package,total_timedelivery,
                                        total_value,total_good,total_bad,total_good_bad_user,topDishImagePojoArrayList,topdishimage_count,
                                        dishname,top_count,avoidDishImagePojoArrayList,avoiddishimage_count,dishtoavoid,avoid_count,
                                        ambiImagePojoArrayList,ambi_image_count,false,delivery_mode);




                                getallHotelReviewPojoArrayList.add(reviewInputAllPojo);
                                getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList,fromwhich);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                                rv_review.setLayoutManager(mLayoutManager);
                                rv_review.setItemAnimator(new DefaultItemAnimator());
                                rv_review.setAdapter(getHotelDetailsAdapter);
                                rv_review.setNestedScrollingEnabled(true);
                                getHotelDetailsAdapter.notifyDataSetChanged();
                                progressbar_review.setVisibility(View.GONE);



                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }else if (response.body().getResponseCode()==1&&response.body().getResponseMessage().equalsIgnoreCase("nodata")){
                        img_no_dish.setVisibility(View.VISIBLE);
                        txt_no_dish.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    img_no_dish.setVisibility(View.VISIBLE);
                    txt_no_dish.setVisibility(View.VISIBLE);
                }
            });
        }else {

            Snackbar snackbar = Snackbar.make(rl_hotel, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }



    }




    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }







    @Override
    public void onResume () {
        super.onResume();

        if(getallHotelReviewPojoArrayList.size()==0){

            get_hotel_review();
        }
        else{
            getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList,fromwhich);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            rv_review.setLayoutManager(mLayoutManager);
            rv_review.setItemAnimator(new DefaultItemAnimator());
            rv_review.setAdapter(getHotelDetailsAdapter);
            rv_review.setNestedScrollingEnabled(false);
            getHotelDetailsAdapter.notifyDataSetChanged();
            progressbar_review.setVisibility(View.GONE);



        }




    }
}


*/
