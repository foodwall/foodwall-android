package com.kaspontech.foodwall.reviewpackage.dineInTab;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.reviewpackage.Review_adapter_folder.Get_all_hotel_review_adapter;
import com.kaspontech.foodwall.reviewpackage.Review_adapter_folder.Getall_hotel_review_pojo;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DineInFragment extends Fragment {

    View view;
    Context context;

    TextView view_more, cat_rating, caption, showless;
    LinearLayout images;
    RecyclerView rv_review;

    Getall_hotel_review_pojo getallHotelReviewPojo;
    ArrayList<Getall_hotel_review_pojo> getallHotelReviewPojoArrayList = new ArrayList<>();

    ProgressBar progressbar_review;

    Get_all_hotel_review_adapter get_all_hotel_review_adapter;

    String txt_hotelname, user_id, hotel_id, txt_hotel_address,
            txt_review_username, txt_review_userlastname, userimage, txt_review_follow, txt_review_like, txt_review_comment,
            txt_review_shares, txt_review_caption, hotel_review_rating, hotel_user_rating, total_followers,
            hotelId, cat_type, veg_nonveg, revratID, ambiance, taste, service, valuemoney,
            createdby, createdon, googleID, placeID, opentimes,
            category_type, latitude, longitude, phone, photo_reference, emailID, likes_hotelID, revrathotel_likes,
            followingID, totalreview, total_ambiance, total_taste, total_service, total_value, total_good, total_bad, total_good_bad_user;

    public DineInFragment() {

        // Required empty public constructor


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
//        view = inflater.inflate(R.layout.fragment_dine_in, container, false);
        view = inflater.inflate(R.layout.activity_hotel_review_all, container, false);
        context = view.getContext();

        rv_review = (RecyclerView) view.findViewById(R.id.rv_review);
        progressbar_review = (ProgressBar) view.findViewById(R.id.progressbar_review);

//Category based hotel review

        get_hotel_review_category_type();
      /*  get_hotel_review_veg();
        get_hotel_review_nonveg();*/

        return view;

    }


    private void get_hotel_review_category_type() {

/*
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final String userId = Pref_storage.getDetail(context, "userId");

        Call<Getall_hotel_review_output_pojo> call = apiService.get_hotel_review_category_type("get_hotel_review_category_type", 1, Integer.parseInt(userId));
        call.enqueue(new Callback<Getall_hotel_review_output_pojo>() {
            @Override
            public void onResponse(Call<Getall_hotel_review_output_pojo> call, Response<Getall_hotel_review_output_pojo> response) {

                if (response.body().getResponseCode() == 1) {
                    getallHotelReviewPojoArrayList.clear();



                    for (int j = 0; j < response.body().getData().size(); j++) {

                        Log.e("size-->", "" + response.body().getData().size());


                        try {
*/
/*

                            getallHotelReviewPojo = new Getall_hotel_review_pojo("", "", "", "",
                                    "", "", "", "", "", "", "", "",
                                    "", "", "", "", "", "", "", "",
                                    "", "", "", "", "", "", "","" ,"",
                                    "", "", "", "", "", "", "", "", "",
                                    "", "", "", "", "", "",
                                    "", "", "", "",false);
*//*



                            hotelId = response.body().getData().get(j).getHotelId();
                            cat_type = response.body().getData().get(j).getCategoryType();
                            veg_nonveg = response.body().getData().get(j).getVegNonveg();
                            revratID = response.body().getData().get(j).getRevratId();
                            ambiance = response.body().getData().get(j).getAmbiance();
                            taste = response.body().getData().get(j).getTaste();
                            service = response.body().getData().get(j).getService();
                            valuemoney = response.body().getData().get(j).getValueMoney();
                            createdby = response.body().getData().get(j).getCreatedBy();
                            createdon = response.body().getData().get(j).getCreatedOn();
                            googleID = response.body().getData().get(j).getGoogleId();
                            placeID = response.body().getData().get(j).getPlaceId();
                            opentimes = response.body().getData().get(j).getOpenTimes();
                            category_type = response.body().getData().get(j).getCategoryType();
                            latitude = response.body().getData().get(j).getLatitude();
                            longitude = response.body().getData().get(j).getLongitude();
                            phone = response.body().getData().get(j).getPhone();
                            photo_reference = response.body().getData().get(j).getPhotoReference();
                            emailID = response.body().getData().get(j).getEmail();
                            likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                            revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                            followingID = response.body().getData().get(j).getFollowingId();
                            totalreview = response.body().getData().get(j).getTotalReview();
                            total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                            total_taste = response.body().getData().get(j).getTotalTaste();
                            total_service = response.body().getData().get(j).getTotalService();
                            total_value = response.body().getData().get(j).getTotalValueMoney();
                            total_good = response.body().getData().get(j).getTotalGood();
                            total_bad = response.body().getData().get(j).getTotalBad();
                            total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();

                            txt_hotelname = response.body().getData().get(j).getHotelName();
                            user_id = response.body().getData().get(j).getUserId();
                            hotel_id = response.body().getData().get(j).getHotelId();
                            txt_hotel_address = response.body().getData().get(j).getAddress();

                            txt_review_username = response.body().getData().get(j).getFirstName();
                            txt_review_userlastname = response.body().getData().get(j).getLastName();
                            userimage = response.body().getData().get(j).getPicture();
                            txt_review_caption = response.body().getData().get(j).getHotelReview();
                            txt_review_like = response.body().getData().get(j).getTotalLikes();
                            txt_review_comment = response.body().getData().get(j).getTotalComments();
                            txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                            hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                            hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                            total_followers = response.body().getData().get(j).getTotal_followers();


                             getallHotelReviewPojo = new Getall_hotel_review_pojo(hotelId, revratID, txt_review_caption, txt_review_like,
                                    txt_review_comment, cat_type, veg_nonveg, hotel_user_rating, ambiance, taste, service, valuemoney,
                                    createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude,
                                    longitude, phone, photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname,
                                    emailID, "", "", "", "", userimage, likes_hotelID, revrathotel_likes, followingID,
                                    totalreview, total_followers,txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service,
                                    total_value, total_good, total_bad, total_good_bad_user,false);


                            getallHotelReviewPojoArrayList.add(getallHotelReviewPojo);
                            get_all_hotel_review_adapter = new Get_all_hotel_review_adapter(context, getallHotelReviewPojoArrayList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
                            rvReview.setLayoutManager(mLayoutManager);
                            rvReview.setItemAnimator(new DefaultItemAnimator());
                            rvReview.setAdapter(get_all_hotel_review_adapter);
                            rvReview.setNestedScrollingEnabled(false);
                            get_all_hotel_review_adapter.notifyDataSetChanged();
                            progressbarReview.setVisibility(View.GONE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<Getall_hotel_review_output_pojo> call, Throwable t) {
                //Error
                Log.e("FailureError", "" + t.getMessage());
            }
        });
*/


    }




/* private void get_hotel_review_veg() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final String userId = Pref_storage.getDetail(context, "userId");

        Call<Getall_hotel_review_output_pojo> call = apiService.get_hotel_review_veg("get_hotel_review_vegnonveg", 1, 1,Integer.parseInt(userId));
        call.enqueue(new Callback<Getall_hotel_review_output_pojo>() {
            @Override
            public void onResponse(Call<Getall_hotel_review_output_pojo> call, Response<Getall_hotel_review_output_pojo> response) {

                if (response.body().getResponseCode() == 1) {
                    getallHotelReviewPojoArrayList.clear();



                    for (int j = 0; j < response.body().getData().size(); j++) {

                        Log.e("size-->", "" + response.body().getData().size());


                        try {

                            getallHotelReviewPojo = new Getall_hotel_review_pojo("", "", "", "",
                                    "", "", "", "", "", "", "", "",
                                    "", "", "", "", "", "", "", "",
                                    "", "", "", "", "", "", "","" ,"",
                                    "", "", "", "", "", "", "", "", "",
                                    "", "", "", "", "", "",
                                    "", "", "", "",false);


                            hotelId = response.body().getData().get(j).getHotelId();
                            cat_type = response.body().getData().get(j).getCategoryType();
                            veg_nonveg = response.body().getData().get(j).getVegNonveg();
                            revratID = response.body().getData().get(j).getRevratId();
                            ambiance = response.body().getData().get(j).getAmbiance();
                            taste = response.body().getData().get(j).getTaste();
                            service = response.body().getData().get(j).getService();
                            valuemoney = response.body().getData().get(j).getValueMoney();
                            createdby = response.body().getData().get(j).getCreatedBy();
                            createdon = response.body().getData().get(j).getCreatedOn();
                            googleID = response.body().getData().get(j).getGoogleId();
                            placeID = response.body().getData().get(j).getPlaceId();
                            opentimes = response.body().getData().get(j).getOpenTimes();
                            category_type = response.body().getData().get(j).getCategoryType();
                            latitude = response.body().getData().get(j).getLatitude();
                            longitude = response.body().getData().get(j).getLongitude();
                            phone = response.body().getData().get(j).getPhone();
                            photo_reference = response.body().getData().get(j).getPhotoReference();
                            emailID = response.body().getData().get(j).getEmail();
                            likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                            revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                            followingID = response.body().getData().get(j).getFollowingId();
                            totalreview = response.body().getData().get(j).getTotalReview();
                            total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                            total_taste = response.body().getData().get(j).getTotalTaste();
                            total_service = response.body().getData().get(j).getTotalService();
                            total_value = response.body().getData().get(j).getTotalValueMoney();
                            total_good = response.body().getData().get(j).getTotalGood();
                            total_bad = response.body().getData().get(j).getTotalBad();
                            total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();

                            txt_hotelname = response.body().getData().get(j).getHotelName();
                            user_id = response.body().getData().get(j).getUserId();
                            hotel_id = response.body().getData().get(j).getHotelId();
                            txt_hotel_address = response.body().getData().get(j).getAddress();

                            txt_review_username = response.body().getData().get(j).getFirstName();
                            txt_review_userlastname = response.body().getData().get(j).getLastName();
                            userimage = response.body().getData().get(j).getPicture();
                            txt_review_caption = response.body().getData().get(j).getHotelReview();
                            txt_review_like = response.body().getData().get(j).getTotalLikes();
                            txt_review_comment = response.body().getData().get(j).getTotalComments();
                            txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                            hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                            hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                            total_followers = response.body().getData().get(j).getTotal_followers();


                            Getall_hotel_review_pojo getall_hotel_review_pojo = new Getall_hotel_review_pojo(hotelId, revratID, txt_review_caption, txt_review_like,
                                    txt_review_comment, cat_type, veg_nonveg, hotel_user_rating, ambiance, taste, service, valuemoney,
                                    createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude,
                                    longitude, phone, photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname,
                                    emailID, "", "", "", "", userimage, likes_hotelID, revrathotel_likes, followingID,
                                    totalreview,total_followers, txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service,
                                    total_value, total_good, total_bad, total_good_bad_user,false);


//                            Collections.reverse(getallHotelReviewPojoArrayList);
                            getallHotelReviewPojoArrayList.add(getall_hotel_review_pojo);
                            get_all_hotel_review_adapter = new Get_all_hotel_review_adapter(context, getallHotelReviewPojoArrayList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
                            rvReview.setLayoutManager(mLayoutManager);
                            rvReview.setItemAnimator(new DefaultItemAnimator());
                            rvReview.setAdapter(get_all_hotel_review_adapter);
                            rvReview.setNestedScrollingEnabled(false);
                            get_all_hotel_review_adapter.notifyDataSetChanged();
                            progressbarReview.setVisibility(View.GONE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<Getall_hotel_review_output_pojo> call, Throwable t) {
                //Error
                Log.e("FailureError", "" + t.getMessage());
            }
        });


    }



 private void get_hotel_review_nonveg() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final String userId = Pref_storage.getDetail(context, "userId");

        Call<Getall_hotel_review_output_pojo> call = apiService.get_hotel_review_nonveg("get_hotel_review_vegnonveg", 1, 2,Integer.parseInt(userId));
        call.enqueue(new Callback<Getall_hotel_review_output_pojo>() {
            @Override
            public void onResponse(Call<Getall_hotel_review_output_pojo> call, Response<Getall_hotel_review_output_pojo> response) {

                if (response.body().getResponseCode() == 1) {
                    getallHotelReviewPojoArrayList.clear();



                    for (int j = 0; j < response.body().getData().size(); j++) {

                        Log.e("size-->", "" + response.body().getData().size());


                        try {

                            getallHotelReviewPojo = new Getall_hotel_review_pojo("", "", "", "",
                                    "", "", "", "", "", "", "", "",
                                    "", "", "", "", "", "", "", "",
                                    "", "", "", "", "", "", "","" ,"",
                                    "", "", "", "", "", "", "", "", "",
                                    "", "", "", "", "", "",
                                    "", "", "", "",false);


                            hotelId = response.body().getData().get(j).getHotelId();
                            cat_type = response.body().getData().get(j).getCategoryType();
                            veg_nonveg = response.body().getData().get(j).getVegNonveg();
                            revratID = response.body().getData().get(j).getRevratId();
                            ambiance = response.body().getData().get(j).getAmbiance();
                            taste = response.body().getData().get(j).getTaste();
                            service = response.body().getData().get(j).getService();
                            valuemoney = response.body().getData().get(j).getValueMoney();
                            createdby = response.body().getData().get(j).getCreatedBy();
                            createdon = response.body().getData().get(j).getCreatedOn();
                            googleID = response.body().getData().get(j).getGoogleId();
                            placeID = response.body().getData().get(j).getPlaceId();
                            opentimes = response.body().getData().get(j).getOpenTimes();
                            category_type = response.body().getData().get(j).getCategoryType();
                            latitude = response.body().getData().get(j).getLatitude();
                            longitude = response.body().getData().get(j).getLongitude();
                            phone = response.body().getData().get(j).getPhone();
                            photo_reference = response.body().getData().get(j).getPhotoReference();
                            emailID = response.body().getData().get(j).getEmail();
                            likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                            revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                            followingID = response.body().getData().get(j).getFollowingId();
                            totalreview = response.body().getData().get(j).getTotalReview();
                            total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                            total_taste = response.body().getData().get(j).getTotalTaste();
                            total_service = response.body().getData().get(j).getTotalService();
                            total_value = response.body().getData().get(j).getTotalValueMoney();
                            total_good = response.body().getData().get(j).getTotalGood();
                            total_bad = response.body().getData().get(j).getTotalBad();
                            total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();

                            txt_hotelname = response.body().getData().get(j).getHotelName();
                            user_id = response.body().getData().get(j).getUserId();
                            hotel_id = response.body().getData().get(j).getHotelId();
                            txt_hotel_address = response.body().getData().get(j).getAddress();

                            txt_review_username = response.body().getData().get(j).getFirstName();
                            txt_review_userlastname = response.body().getData().get(j).getLastName();
                            userimage = response.body().getData().get(j).getPicture();
                            txt_review_caption = response.body().getData().get(j).getHotelReview();
                            txt_review_like = response.body().getData().get(j).getTotalLikes();
                            txt_review_comment = response.body().getData().get(j).getTotalComments();
                            txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                            hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                            hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                            total_followers = response.body().getData().get(j).getTotal_followers();

                            getallHotelReviewPojo = new Getall_hotel_review_pojo(hotelId, revratID, txt_review_caption, txt_review_like,
                                    txt_review_comment, cat_type, veg_nonveg, hotel_user_rating, ambiance, taste, service, valuemoney,
                                    createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude,
                                    longitude, phone, photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname,
                                    emailID, "", "", "", "", userimage, likes_hotelID, revrathotel_likes, followingID,
                                    totalreview, total_followers,txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service,
                                    total_value, total_good, total_bad, total_good_bad_user,false);


//                            Collections.reverse(getallHotelReviewPojoArrayList);
                            getallHotelReviewPojoArrayList.add(getallHotelReviewPojo);
                            get_all_hotel_review_adapter = new Get_all_hotel_review_adapter(context, getallHotelReviewPojoArrayList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
                            rvReview.setLayoutManager(mLayoutManager);
                            rvReview.setItemAnimator(new DefaultItemAnimator());
                            rvReview.setAdapter(get_all_hotel_review_adapter);
                            rvReview.setNestedScrollingEnabled(false);
                            get_all_hotel_review_adapter.notifyDataSetChanged();
                            progressbarReview.setVisibility(View.GONE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<Getall_hotel_review_output_pojo> call, Throwable t) {
                //Error
                Log.e("FailureError", "" + t.getMessage());
            }
        });


    }*/

    @Override
    public void onResume() {
        super.onResume();
        get_hotel_review_category_type();

    }
}
