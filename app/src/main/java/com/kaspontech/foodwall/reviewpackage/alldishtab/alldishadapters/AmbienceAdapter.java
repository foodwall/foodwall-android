package com.kaspontech.foodwall.reviewpackage.alldishtab.alldishadapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_ambience_image_all;
import com.kaspontech.foodwall.R;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;


public class AmbienceAdapter extends RecyclerView.Adapter<AmbienceAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     */

    private Context context;

    /**
     * Ambience image arraylist
     */
    private ArrayList<Get_ambience_image_all> getAmbienceImageAllArrayList = new ArrayList<>();

    /**
     * Ambience image all pojo
     */

    private Get_ambience_image_all getAmbienceImageAll;

    /*Constructor*/

    public AmbienceAdapter(Context c, ArrayList<Get_ambience_image_all> gettinglist) {
        this.context = c;
        this.getAmbienceImageAllArrayList = gettinglist;

    }


    @NonNull
    @Override
    public AmbienceAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dishes_image_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        AmbienceAdapter.MyViewHolder vh = new AmbienceAdapter.MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final AmbienceAdapter.MyViewHolder holder, final int position) {

        /*Ambience pojo initialization*/

        getAmbienceImageAll = getAmbienceImageAllArrayList.get(position);

        /*Image loading*/

        if(!getAmbienceImageAll.getDishimage().isEmpty()||getAmbienceImageAll.getDishimage()!=null){

            GlideApp.with(context)
                    .load(getAmbienceImageAll.getDishimage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.01f)
                    .centerCrop()
                    .into(holder.imageReview);

        }else {
            /*Remove postion*/
            removeat(position);
        }


        /*Layout visibility*/
        holder.rlTextDish.setVisibility(View.GONE);



    }

    /*Remove postion*/

    private void removeat(int position){

        getAmbienceImageAllArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getAmbienceImageAllArrayList.size());

    }
    @Override
    public int getItemCount() {
        return getAmbienceImageAllArrayList.size();

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        /* TextView */

        TextView txtImage;

        /* Imageview */

        PhotoView imageReview;
        /*Text dish relative layout*/
        RelativeLayout rlTextDish;


        public MyViewHolder(View itemView) {
            super(itemView);

            txtImage = itemView.findViewById(R.id.txt_image);
            imageReview = itemView.findViewById(R.id.image_review);
            rlTextDish = itemView.findViewById(R.id.rl_text_dish);

        }


    }


}
