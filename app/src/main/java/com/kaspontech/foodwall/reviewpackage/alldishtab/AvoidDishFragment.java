package com.kaspontech.foodwall.reviewpackage.alldishtab;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_all_input;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_all_output;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_image_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.reviewpackage.alldishtab.alldishadapters.AvoidDishAdapter;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AvoidDishFragment extends Fragment {
    /**
     * View
     */

    View view;
    /**
     * Context
     */
    Context context;
    /**
     * Recycler view for Avoid dish
     */
    RecyclerView rvAvoidFragment;

    /**
     * Loader
     */
    ProgressBar progressbarAvoid;

    /**
     * No dish - Text view
     */

    TextView txtNoDish;
    /**
     * Getting dish type all input pojo
     */
    Get_dish_type_all_input getDishTypeAllInput;

    /**
     * Getting dish type image pojo
     */

    Get_dish_type_image_pojo getDishTypeImagePojo;

    /**
     * Arraylist for dish Type input
     */
    ArrayList<Get_dish_type_all_input> getDishTypeAllInputslist = new ArrayList<>();

    /**
     * Arraylist for image dish Type input
     */
    ArrayList<Get_dish_type_image_pojo> getDishTypeImagePojoArrayList = new ArrayList<>();

    /**
     * Avoid dish Adapter
     */

    AvoidDishAdapter reviewWorstDishAdapter;
    /**
     * String - Images, Photo id
     */
    String images;

    String photoId;
    /**
     * Integer - Review id
     */
   int reviewid;


    public AvoidDishFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.avoid_dish_frag_layout, container, false);

        context = view.getContext();

        /* Recycler view for avoid dish */

        rvAvoidFragment = view.findViewById(R.id.rv_avoid_fragment);
        /*Loader*/

        progressbarAvoid = view.findViewById(R.id.progressbar_avoid);

        /* No dish available - Text view */

        txtNoDish = view.findViewById(R.id.txt_no_dish);

        /* Interface activity */
        AllDishDisplayActivity activity = (AllDishDisplayActivity) getActivity();
        if (activity != null) {
            reviewid = Integer.parseInt(activity.getReviewId());
        }

        /*Setting adapter*/
        if(getDishTypeAllInputslist.isEmpty()){
            /*Getting avoid dish - API call*/

            avoiddishApi();
        }
        else{
            reviewWorstDishAdapter = new AvoidDishAdapter(context, getDishTypeAllInputslist,getDishTypeImagePojoArrayList);
            RecyclerView.LayoutManager llm = new LinearLayoutManager(context);
            rvAvoidFragment.setLayoutManager(llm);
            rvAvoidFragment.setItemAnimator(new DefaultItemAnimator());
            rvAvoidFragment.setAdapter(reviewWorstDishAdapter);
            rvAvoidFragment.setNestedScrollingEnabled(false);
            reviewWorstDishAdapter.notifyDataSetChanged();
            progressbarAvoid.setVisibility(View.GONE);
        }

        return view;
    }


    /*Getting avoid dish - API call*/

    private void avoiddishApi() {

        if (Utility.isConnected(context)){
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");

            Call<Get_dish_type_all_output> call = apiService.get_hotel_dish_type_all("get_hotel_dish_type_all", reviewid, 2,Integer.parseInt(userId));
            call.enqueue(new Callback<Get_dish_type_all_output>() {
                @Override
                public void onResponse(@NonNull Call<Get_dish_type_all_output> call, @NonNull Response<Get_dish_type_all_output> response) {

                    if (response.body() != null && response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {
                        getDishTypeAllInputslist.clear();


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());

                            try {

                                int imageCount = response.body().getData().get(j).getImageCount();

                                String dishname = response.body().getData().get(j).getDishName();


                                images = response.body().getData().get(j).getImage().get(0).getImg();

                                Log.e("images-->", "" + images);

                                photoId = response.body().getData().get(j).getImage().get(0).getPhotoId();

                                Log.e("images-->", "" + photoId);
                                getDishTypeImagePojo = new Get_dish_type_image_pojo(photoId, images);


                                getDishTypeImagePojoArrayList.add(getDishTypeImagePojo);


                                getDishTypeAllInput = new Get_dish_type_all_input("", "", "", dishname, "",
                                        "", "", "", "", "", "", "",
                                        "", "", "", "", "", "", "", "",
                                        "", "", "", "", "", "", "", "", "", "", "",
                                        getDishTypeImagePojoArrayList, imageCount);

                                getDishTypeAllInputslist.add(getDishTypeAllInput);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                        /*Setting Adapter*/
                        reviewWorstDishAdapter = new AvoidDishAdapter(context, getDishTypeAllInputslist, getDishTypeImagePojoArrayList);
                        RecyclerView.LayoutManager llm = new LinearLayoutManager(context);
                        rvAvoidFragment.setLayoutManager(llm);
                        rvAvoidFragment.setItemAnimator(new DefaultItemAnimator());
                        rvAvoidFragment.setAdapter(reviewWorstDishAdapter);
                        rvAvoidFragment.setNestedScrollingEnabled(false);
                        reviewWorstDishAdapter.notifyDataSetChanged();
                        progressbarAvoid.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Get_dish_type_all_output> call, @NonNull Throwable t) {

                    if (getDishTypeAllInputslist.isEmpty()) {
                        txtNoDish.setVisibility(View.VISIBLE);
                    }
                    //Error
                    Log.e("FailureError", "" + t.getMessage());


                }
            });
        }else {

            Toast.makeText(context, "Loading failed. Please check your internet connenction!!", Toast.LENGTH_SHORT).show();

        }

        }



    @Override
    public void onResume() {
        super.onResume();
        if(!getDishTypeAllInputslist.isEmpty()){

            avoiddishApi();
        }
        else{
            reviewWorstDishAdapter = new AvoidDishAdapter(context, getDishTypeAllInputslist,getDishTypeImagePojoArrayList);
            RecyclerView.LayoutManager llm = new LinearLayoutManager(context);
            rvAvoidFragment.setLayoutManager(llm);
            rvAvoidFragment.setItemAnimator(new DefaultItemAnimator());
            rvAvoidFragment.setAdapter(reviewWorstDishAdapter);
            rvAvoidFragment.setNestedScrollingEnabled(false);
            reviewWorstDishAdapter.notifyDataSetChanged();
            progressbarAvoid.setVisibility(View.GONE);
        }
    }


}
