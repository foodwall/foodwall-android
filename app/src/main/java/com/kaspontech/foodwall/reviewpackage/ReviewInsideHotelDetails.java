package com.kaspontech.foodwall.reviewpackage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.modelclasses.Review_pojo.ReviewCommentsPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewInputAllPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewOutputResponsePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.GetHotelDetailsAdapter;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewInsideHotelDetails extends AppCompatActivity implements View.OnClickListener, GetHotelDetailsAdapter.onCommentsPostListener {

    // Action Bar
    Toolbar actionBar;
    TextView toolbar_title, txt_no_dish;

    ImageButton back;

    RelativeLayout rl_hotel;

    String hotelid, hotel_name;
    RecyclerView rv_review;
    ProgressBar progressbar_review;
    ImageView img_no_dish;


    // getallHotelReviewPojoArrayList
    List<ReviewInputAllPojo> getallHotelReviewPojoArrayList = new ArrayList<>();

    //Get_all_hotel_review_adapter get_all_hotel_review_adapter;
    GetHotelDetailsAdapter getHotelDetailsAdapter;


    int newhotelId;

    TextView tv_view_all_comments;

    private ProgressBar comments_progressbar;

    private EmojiconEditText review_comment;

    public String fromwhich, createdby, hotel_id, txt_hotel_address, service, taste, dishname, veg_nonveg, ambiance, category_type;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_hotel_all_details_layout);

        actionBar = (Toolbar) findViewById(R.id.action_bar_hotels);

        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);
        txt_no_dish = (TextView) findViewById(R.id.txt_no_dish);


        rv_review = (RecyclerView) findViewById(R.id.rv_review);
        rl_hotel = (RelativeLayout) findViewById(R.id.rl_hotel);
        progressbar_review = (ProgressBar) findViewById(R.id.progressbar_review);
        img_no_dish = (ImageView) findViewById(R.id.img_no_dish);

        toolbar_title.setGravity(View.TEXT_ALIGNMENT_CENTER);


        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);


        Intent intent = getIntent();

        hotelid = intent.getStringExtra("hotelid");
        hotel_name = intent.getStringExtra("hotelname");

        newhotelId = Integer.parseInt(hotelid);
        toolbar_title.setText(hotel_name);

        /// Setting adapter
        get_hotel_review();


    }

    //get_hotel_review API CALL
    private void get_hotel_review() {

        if (isConnected(ReviewInsideHotelDetails.this)) {

            progressbar_review.setVisibility(View.VISIBLE);
            progressbar_review.setIndeterminate(true);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(ReviewInsideHotelDetails.this, "userId");
            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review("get_hotel_review", newhotelId, Integer.parseInt(userId));
            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    assert response.body() != null;
                    if (response.body().getResponseCode() == 1
                            && response.body().getResponseMessage().equalsIgnoreCase("success")) {


                        getallHotelReviewPojoArrayList.clear();

                        progressbar_review.setVisibility(View.GONE);
                        progressbar_review.setIndeterminate(false);

                        if (response.body().getData().size() > 0) {

                            getallHotelReviewPojoArrayList = response.body().getData();

                            getHotelDetailsAdapter = new GetHotelDetailsAdapter(ReviewInsideHotelDetails.this, getallHotelReviewPojoArrayList, fromwhich, ReviewInsideHotelDetails.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ReviewInsideHotelDetails.this);
                            rv_review.setLayoutManager(mLayoutManager);
                            rv_review.setItemAnimator(new DefaultItemAnimator());
                            rv_review.setAdapter(getHotelDetailsAdapter);
                            rv_review.setNestedScrollingEnabled(false);
                            getHotelDetailsAdapter.notifyDataSetChanged();

                        } else {

                            img_no_dish.setVisibility(View.VISIBLE);
                            txt_no_dish.setVisibility(View.VISIBLE);
                        }


                    } else if (response.body().getResponseCode() == 0
                            && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {

                        progressbar_review.setVisibility(View.GONE);
                        progressbar_review.setIndeterminate(false);

                        img_no_dish.setVisibility(View.VISIBLE);
                        txt_no_dish.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    progressbar_review.setVisibility(View.GONE);
                    progressbar_review.setIndeterminate(false);

                    img_no_dish.setVisibility(View.VISIBLE);
                    txt_no_dish.setVisibility(View.VISIBLE);
                }
            });
        } else {

            Snackbar snackbar = Snackbar.make(rl_hotel, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    public void onClick(View v) {
        int click = v.getId();
        switch (click) {
            case R.id.back:
                finish();
                break;
        }

    }

    @Override
    public void onPostCommitedView(View view, final int adapterPosition, View hotelDetailsView) {

        if (Utility.isConnected(ReviewInsideHotelDetails.this)) {

            review_comment = (EmojiconEditText) hotelDetailsView.findViewById(R.id.review_comment);

            comments_progressbar = (ProgressBar) hotelDetailsView.findViewById(R.id.comments_progressbar);
            comments_progressbar.setVisibility(View.VISIBLE);
            comments_progressbar.setIndeterminate(true);

            tv_view_all_comments = (TextView) hotelDetailsView.findViewById(R.id.tv_view_all_comments);

            if (review_comment.getText().toString().isEmpty()) {

                Toast.makeText(ReviewInsideHotelDetails.this, "Enter your comments to continue", Toast.LENGTH_SHORT).show();

            } else {

                Utility.hideKeyboard(view);

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(ReviewInsideHotelDetails.this, "userId"));

                    Call<ReviewCommentsPojo> call = apiService.get_create_edit_hotel_review_comments("create_edit_hotel_review_comments",
                            Integer.parseInt(getallHotelReviewPojoArrayList.get(adapterPosition).getHotelId()), 0, userid, review_comment.getText().toString(), Integer.parseInt(getallHotelReviewPojoArrayList.get(adapterPosition).getRevratId()));

                    call.enqueue(new Callback<ReviewCommentsPojo>() {

                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onResponse(Call<ReviewCommentsPojo> call, Response<ReviewCommentsPojo> response) {

                            review_comment.setText("");

                            if (response.code() == 500) {

                                comments_progressbar.setVisibility(View.GONE);
                                comments_progressbar.setIndeterminate(false);

                                Toast.makeText(ReviewInsideHotelDetails.this, "Internal server error", Toast.LENGTH_SHORT).show();

                            } else {

                                comments_progressbar.setVisibility(View.GONE);
                                comments_progressbar.setIndeterminate(false);


                                if (response.body() != null) {

                                    if (Objects.requireNonNull(response.body()).getResponseMessage().equals("success")) {

                                        if (response.body().getData().size() > 0) {

                                            if (tv_view_all_comments.getVisibility() == View.GONE) {

                                                tv_view_all_comments.setVisibility(View.VISIBLE);
                                                //String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                                String comments = getString(R.string.view_one_comment);
                                                tv_view_all_comments.setText(comments);

                                            } else {

                                                String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                                tv_view_all_comments.setText(comments);
                                            }

                                            Intent intent = new Intent(ReviewInsideHotelDetails.this, Reviews_comments_all_activity.class);
                                            intent.putExtra("posttype", getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType());
                                            intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(adapterPosition).getHotelId());
                                            intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(adapterPosition).getRevratId());
                                            intent.putExtra("userid", getallHotelReviewPojoArrayList.get(adapterPosition).getUserId());
                                            intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(adapterPosition).getHotelName());
                                            intent.putExtra("overallrating", getallHotelReviewPojoArrayList.get(adapterPosition).getFoodExprience());

                                            if (getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                                intent.putExtra("totaltimedelivery", getallHotelReviewPojoArrayList.get(adapterPosition).getTotalTimedelivery());

                                            } else {
                                                intent.putExtra("totaltimedelivery", getallHotelReviewPojoArrayList.get(adapterPosition).getAmbiance());

                                            }
                                            intent.putExtra("taste_count", getallHotelReviewPojoArrayList.get(adapterPosition).getTaste());
                                            intent.putExtra("vfm_rating", getallHotelReviewPojoArrayList.get(adapterPosition).getValueMoney());
                                            if (getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                                intent.putExtra("package_rating", getallHotelReviewPojoArrayList.get(adapterPosition).get_package());

                                            } else {
                                                intent.putExtra("package_rating", getallHotelReviewPojoArrayList.get(adapterPosition).getService());

                                            }
                                            intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(adapterPosition).getHotelName());
                                            intent.putExtra("reviewprofile", getallHotelReviewPojoArrayList.get(adapterPosition).getPicture());
                                            intent.putExtra("createdon", getallHotelReviewPojoArrayList.get(adapterPosition).getCreatedOn());
                                            intent.putExtra("firstname", getallHotelReviewPojoArrayList.get(adapterPosition).getFirstName());
                                            intent.putExtra("lastname", getallHotelReviewPojoArrayList.get(adapterPosition).getLastName());
                                            if (getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                                intent.putExtra("title_ambience", "Packaging");
                                            } else {
                                                intent.putExtra("title_ambience", "Hotel Ambience");
                                            }
                                            if (getallHotelReviewPojoArrayList.get(adapterPosition).getBucket_list() != null) {
                                                if (getallHotelReviewPojoArrayList.get(adapterPosition).getBucket_list() != 0) {
                                                    intent.putExtra("addbucketlist", "yes");
                                                } else {
                                                    intent.putExtra("addbucketlist", "no");
                                                }
                                            }
                                            startActivity(intent);


                                        } else {

                                        }

                                    } else {

                                    }
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<ReviewCommentsPojo> call, Throwable t) {
                            //Error

                            comments_progressbar.setVisibility(View.GONE);
                            comments_progressbar.setIndeterminate(false);

                            Log.e("Balaji", "" + t.getMessage());
                        }
                    });

                } catch (Exception e) {

                    Log.e("Balaji", "" + e.getMessage());
                    e.printStackTrace();

                }
            }

        } else {

            Toast.makeText(ReviewInsideHotelDetails.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }
    }
}
