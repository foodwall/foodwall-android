package com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailedReviewPojo {
    @SerializedName("hotel_id")
    @Expose
    private String hotelId;
    @SerializedName("revrat_id")
    @Expose
    private String revratId;
    @SerializedName("hotel_review")
    @Expose
    private String hotelReview;
    @SerializedName("total_likes")
    @Expose
    private String totalLikes;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("category_type")
    @Expose
    private String categoryType;
    @SerializedName("veg_nonveg")
    @Expose
    private String vegNonveg;
    @SerializedName("food_exprience")
    @Expose
    private String foodExprience;
    @SerializedName("ambiance")
    @Expose
    private String ambiance;
    @SerializedName("taste")
    @Expose
    private String taste;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("package")
    @Expose
    private String _package;
    @SerializedName("timedelivery")
    @Expose
    private String timedelivery;
    @SerializedName("value_money")
    @Expose
    private String valueMoney;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("google_id")
    @Expose
    private String googleId;
    @SerializedName("hotel_name")
    @Expose
    private String hotelName;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("open_times")
    @Expose
    private String openTimes;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("photo_reference")
    @Expose
    private String photoReference;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("total_followers")
    @Expose
    private String totalFollowers;
    @SerializedName("total_followings")
    @Expose
    private String totalFollowings;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("likes_htl_id")
    @Expose
    private String likesHtlId;
    @SerializedName("htl_rev_likes")
    @Expose
    private String htlRevLikes;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param oauthProvider
     * @param phone
     * @param vegNonveg
     * @param totalComments
     * @param totalFollowers
     * @param hotelReview
     * @param ambiance
     * @param taste
     * @param hotelId
     * @param photoReference
     * @param htlRevLikes
     * @param hotelName
     * @param placeId
     * @param openTimes
     * @param userId
     * @param gender
     * @param contNo
     * @param timedelivery
     * @param longitude
     * @param firstName
     * @param likesHtlId
     * @param lastName
     * @param revratId
     * @param _package
     * @param imei
     * @param categoryType
     * @param valueMoney
     * @param googleId
     * @param oauthUid
     * @param picture
     * @param createdOn
     * @param totalLikes
     * @param createdBy
     * @param email
     * @param address
     * @param dob
     * @param service
     * @param totalFollowings
     * @param latitude
     * @param foodExprience
     */
    public DetailedReviewPojo(String hotelId, String revratId, String hotelReview, String totalLikes, String totalComments, String categoryType, String vegNonveg, String foodExprience, String ambiance, String taste, String service, String _package, String timedelivery, String valueMoney, String createdBy, String createdOn, String googleId, String hotelName, String placeId, String openTimes, String address, String latitude, String longitude, String phone, String photoReference, String userId, String oauthProvider, String oauthUid, String firstName, String lastName, String email, String imei, String contNo, String gender, String dob, String totalFollowers, String totalFollowings, String picture, String likesHtlId, String htlRevLikes) {
        super();
        this.hotelId = hotelId;
        this.revratId = revratId;
        this.hotelReview = hotelReview;
        this.totalLikes = totalLikes;
        this.totalComments = totalComments;
        this.categoryType = categoryType;
        this.vegNonveg = vegNonveg;
        this.foodExprience = foodExprience;
        this.ambiance = ambiance;
        this.taste = taste;
        this.service = service;
        this._package = _package;
        this.timedelivery = timedelivery;
        this.valueMoney = valueMoney;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.googleId = googleId;
        this.hotelName = hotelName;
        this.placeId = placeId;
        this.openTimes = openTimes;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
        this.photoReference = photoReference;
        this.userId = userId;
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imei = imei;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.totalFollowers = totalFollowers;
        this.totalFollowings = totalFollowings;
        this.picture = picture;
        this.likesHtlId = likesHtlId;
        this.htlRevLikes = htlRevLikes;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getRevratId() {
        return revratId;
    }

    public void setRevratId(String revratId) {
        this.revratId = revratId;
    }

    public String getHotelReview() {
        return hotelReview;
    }

    public void setHotelReview(String hotelReview) {
        this.hotelReview = hotelReview;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getVegNonveg() {
        return vegNonveg;
    }

    public void setVegNonveg(String vegNonveg) {
        this.vegNonveg = vegNonveg;
    }

    public String getFoodExprience() {
        return foodExprience;
    }

    public void setFoodExprience(String foodExprience) {
        this.foodExprience = foodExprience;
    }

    public String getAmbiance() {
        return ambiance;
    }

    public void setAmbiance(String ambiance) {
        this.ambiance = ambiance;
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getPackage() {
        return _package;
    }

    public void setPackage(String _package) {
        this._package = _package;
    }

    public String getTimedelivery() {
        return timedelivery;
    }

    public void setTimedelivery(String timedelivery) {
        this.timedelivery = timedelivery;
    }

    public String getValueMoney() {
        return valueMoney;
    }

    public void setValueMoney(String valueMoney) {
        this.valueMoney = valueMoney;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(String openTimes) {
        this.openTimes = openTimes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getTotalFollowers() {
        return totalFollowers;
    }

    public void setTotalFollowers(String totalFollowers) {
        this.totalFollowers = totalFollowers;
    }

    public String getTotalFollowings() {
        return totalFollowings;
    }

    public void setTotalFollowings(String totalFollowings) {
        this.totalFollowings = totalFollowings;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLikesHtlId() {
        return likesHtlId;
    }

    public void setLikesHtlId(String likesHtlId) {
        this.likesHtlId = likesHtlId;
    }

    public String getHtlRevLikes() {
        return htlRevLikes;
    }

    public void setHtlRevLikes(String htlRevLikes) {
        this.htlRevLikes = htlRevLikes;
    }
}
