package com.kaspontech.foodwall.reviewpackage;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.adapters.TimeLine.Timeline_notification_adapter.Comments_like_adapter;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Comment_like_output_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Likes_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Reviewscommentslikeall extends AppCompatActivity implements View.OnClickListener {
    /**
     * Application TAG
     */
    private static final String TAG = "Rev_cmts_like_all";
    /**
     * Action Bar
     */

    Toolbar actionBar;
    /**
     * Back button
     */
    ImageButton back;
    /**
     * Likes relative layout
     */
    RelativeLayout rlLikes;

    RelativeLayout rlOverLikes;
    /**
     * Recycler view for likes
     */
    RecyclerView rvLikes;
    /**
     * Toolbar title
     */
    TextView toolbarTitle;
    /**
     * Linear layout manager
     */
    LinearLayoutManager llm;

    /**
     * Likes adapter - Comments review
     */
    Comments_like_adapter commentsLikeAdapter;


    /**
     * Loader for likes
     */
    ProgressBar progressDialogReply;
    /**
     * String results of user details
     */
    String commentId;
    /**
     * Arraylist
     */
    ArrayList<Likes_pojo> likelist = new ArrayList<>();

    /*Likes all Pojo
    * */

    Likes_pojo likesPojo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes_layout);

        /*Widgets initialization*/

        actionBar = findViewById(R.id.actionbar_like);

        /*Title text view*/

        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setVisibility(View.VISIBLE);

        /*Back button initialization*/

        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        /*Relative layout for likes*/

        rlLikes = findViewById(R.id.rl_likes);

        rlOverLikes = findViewById(R.id.rl_over_likes);

        /*Recycler view for reviwe likes*/

        rvLikes = findViewById(R.id.rv_likes);
        /*Loader*/
        progressDialogReply = findViewById(R.id.progress_dialog_reply);

        /*Setting layout manager*/
        llm = new LinearLayoutManager(this);
        rvLikes.setLayoutManager(llm);


        /*Getting Input values of for comment id various adapters*/

        Intent intent = getIntent();

        commentId = intent.getStringExtra("Comment_id");

        /*Loader visibilty*/
        progressDialogReply.setVisibility(View.VISIBLE);

        /*Review comments likes all api call*/

        getHotelReviewCommentLikesAll();


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        /*Back button on click listener*/

        if(id ==R.id.back ){
            finish();
        }



    }



    //Get total likes API

    private void getHotelReviewCommentLikesAll() {

        if (Utility.isConnected(this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                Call<Comment_like_output_pojo> call = apiService.get_hotel_review_comment_likes_all("get_hotel_review_comment_likes_all", Integer.parseInt(commentId));
                call.enqueue(new Callback<Comment_like_output_pojo>() {
                    @Override
                    public void onResponse(@NonNull Call<Comment_like_output_pojo> call, @NonNull Response<Comment_like_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            likelist.clear();

                            progressDialogReply.setVisibility(View.GONE);

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                String fname = response.body().getData().get(j).getFirstName();
                                String userId = response.body().getData().get(j).getUserId();
                                String commenttimelineid = response.body().getData().get(j).getCmmtTlId();
                                String lname = response.body().getData().get(j).getLastName();
                                String picture = response.body().getData().get(j).getPicture();
                                String timeline = response.body().getData().get(j).getTimelineId();
                                String tlcomments = response.body().getData().get(j).getTlComments();
                                String totalcmmntlikes = response.body().getData().get(j).getTotalCmmtLikes();
                                String cmmtlikesID = response.body().getData().get(j).getCmmtLikesId();
                                String totcmtlikes = response.body().getData().get(j).getTlCmmtLikes();
                                String createdon = response.body().getData().get(j).getCreatedOn();
                                String createdby = response.body().getData().get(j).getCreatedBy();


                                likesPojo = new Likes_pojo(commenttimelineid, timeline, tlcomments,
                                        totalcmmntlikes, cmmtlikesID, totcmtlikes, createdby, createdon, userId, fname, lname,
                                        "", "", "", "", picture);

                                likelist.add(likesPojo);


                            }
                            /*Setting adapter*/

                            commentsLikeAdapter = new Comments_like_adapter(Reviewscommentslikeall.this, likelist);
                            llm = new LinearLayoutManager(Reviewscommentslikeall.this);
                            rvLikes.setLayoutManager(llm);
                            rvLikes.setHasFixedSize(true);
                            rvLikes.setItemAnimator(new DefaultItemAnimator());
                            rvLikes.setAdapter(commentsLikeAdapter);
                            rvLikes.setNestedScrollingEnabled(false);
                            commentsLikeAdapter.notifyDataSetChanged();
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Comment_like_output_pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e(TAG, "FailureError-->" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Snackbar snackbar = Snackbar.make(rlOverLikes, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
