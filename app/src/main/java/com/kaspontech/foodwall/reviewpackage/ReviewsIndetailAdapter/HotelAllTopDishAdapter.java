package com.kaspontech.foodwall.reviewpackage.ReviewsIndetailAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.reviewpackage.alldishtab.AllDishDisplayActivity;
import com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo.DetailedReviewPojo;
import com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo.DetailedTopdishImagePojo;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;

//import com.kaspontech.foodwall.ReviewPackage.Review_adapter_folder.HotelAllTopDishAdapter;

public class HotelAllTopDishAdapter extends RecyclerView.Adapter<HotelAllTopDishAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;


    private ArrayList<DetailedTopdishImagePojo> detailedTopdishImagePojoArrayList = new ArrayList<>();
    private ArrayList<DetailedReviewPojo> detailedReviewPojoArrayList = new ArrayList<>();

    private DetailedTopdishImagePojo detailedTopdishImagePojo;



    private Pref_storage pref_storage;


    public HotelAllTopDishAdapter(Context c, ArrayList<DetailedTopdishImagePojo> gettingimglist, ArrayList<DetailedReviewPojo>gettingdetailedlist) {
        this.context = c;
        this.detailedTopdishImagePojoArrayList = gettingimglist;
        this.detailedReviewPojoArrayList = gettingdetailedlist;


    }


    @NonNull
    @Override
    public HotelAllTopDishAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_image_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        HotelAllTopDishAdapter.MyViewHolder vh = new HotelAllTopDishAdapter.MyViewHolder(v);
        vh.setIsRecyclable(false);
        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final HotelAllTopDishAdapter.MyViewHolder holder, final int position) {


        detailedTopdishImagePojo = detailedTopdishImagePojoArrayList.get(position);


       if(detailedTopdishImagePojoArrayList.get(position).getImg()==null||detailedTopdishImagePojoArrayList.get(position).getImg().trim().equalsIgnoreCase("")){

        }else {




//
//           Picasso.get()
//                   .load(detailedTopdishImagePojoArrayList.get(position).getImg())
//                   .into(holder.image_review);


                   GlideApp.with(context)
                .load(detailedTopdishImagePojoArrayList.get(position).getImg())
                           .diskCacheStrategy(DiskCacheStrategy.ALL)
                           .thumbnail(0.1f)
                .into(holder.image_review);



           holder.txt_image.setText(detailedTopdishImagePojoArrayList.get(position).getDishname());

        }


//        GlideApp.with(context)
//                .load(getDishTypeAllInput.get(position).getImg())
//                .into(holder.image_review);


        if(detailedTopdishImagePojoArrayList.get(position).getImg()==null||detailedTopdishImagePojoArrayList.get(position).getImg().trim().equalsIgnoreCase("")){
            holder.rl_hotel_datas.setVisibility(View.GONE);
        }else {
            holder.rl_hotel_datas.setVisibility(View.VISIBLE);
        }




        holder.rl_hotel_datas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                v.startAnimation(anim);
                Intent intent= new Intent(context, AllDishDisplayActivity.class);
                intent.putExtra("reviewid",detailedReviewPojoArrayList.get(0).getRevratId());
                intent.putExtra("hotelname",detailedReviewPojoArrayList.get(0).getHotelName());
                context.startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
        });
    }


    @Override
    public int getItemCount() {
        return detailedTopdishImagePojoArrayList.size();

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //TextView

        TextView txt_image;

        //Imageview

        ImageView image_review;
        RelativeLayout rl_hotel_datas;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_image = (TextView) itemView.findViewById(R.id.txt_image);
            image_review = (ImageView) itemView.findViewById(R.id.image_review);
            rl_hotel_datas = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_datas);

        }


    }


}
