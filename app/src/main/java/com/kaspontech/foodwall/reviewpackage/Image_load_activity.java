package com.kaspontech.foodwall.reviewpackage;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Utility;
import com.squareup.picasso.Picasso;


//

public class Image_load_activity {

    Context context;

    public static final String PHOTO_URL = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=500&photoreference=%s&key=%s";


    public static void loadGooglePhoto(Context context, ImageView imageView, String photoreference) {
        String url = String.format(PHOTO_URL, photoreference, APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        loadIcon(context, url, imageView);
    }

    public static void loadIcon(Context context, String url, ImageView imageView) {
        if (TextUtils.isEmpty(url)) return;


        GlideApp.with(context).load(url).
                diskCacheStrategy(DiskCacheStrategy.ALL).
                thumbnail(0.1f).centerCrop().into(imageView);
    }

    public static void loadRoundCornerIcon(Context context, String photoreference, ImageView imageView) {

        String url = String.format(PHOTO_URL, photoreference, APIClient_restaurant.GOOGLE_PLACE_API_KEY);

        if (TextUtils.isEmpty(url)) return;

        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(20));




        Glide.with(context)
                .load(url)
                .apply(requestOptions)
                .into(imageView);

    }


    public static void loadRestaurantImages(Context context, String photoreference, ImageView imageView) {

        String url = String.format(PHOTO_URL, photoreference, APIClient_restaurant.GOOGLE_PLACE_API_KEY);

        if (TextUtils.isEmpty(url)) return;


        Utility.picassoImageLoader(url,3,imageView,context);


//        Picasso.get()
//                .load(url)
//                .placeholder(R.drawable.ic_restaurant)
//                .into(imageView);

    }



}
