package com.kaspontech.foodwall.reviewpackage.Review_adapter_folder;

public class Review_image_pojo {

    private String image;
    private String image_text;


    public Review_image_pojo(String image, String image_text) {
        this.image = image;
        this.image_text = image_text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_text() {
        return image_text;
    }

    public void setImage_text(String image_text) {
        this.image_text = image_text;
    }
}
