package com.kaspontech.foodwall.reviewpackage.Review_adapter_folder;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.modelclasses.Review_pojo.GetHotelAllPojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.reviewpackage.Image_load_activity;
import com.kaspontech.foodwall.reviewpackage.View_restaurant_details;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;

public class GetAllHotelAdapter extends RecyclerView.Adapter<GetAllHotelAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;

    private ArrayList<GetHotelAllPojo> getHotelAllPojoArrayList = new ArrayList<>();
    private GetHotelAllPojo getHotelAllPojo;


    private Pref_storage pref_storage;

    ApiInterface apiInterface;

    boolean liked = false;

    public GetAllHotelAdapter(Context c, ArrayList<GetHotelAllPojo> gettinglist) {
        this.context = c;
        this.getHotelAllPojoArrayList = gettinglist;
    }


    @NonNull
    @Override
    public GetAllHotelAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_hotel_all, parent, false);
        // set the view's size, margins, paddings and layout parameters
        GetAllHotelAdapter.MyViewHolder vh = new GetAllHotelAdapter.MyViewHolder(v);

        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final GetAllHotelAdapter.MyViewHolder holder, final int position) {


        getHotelAllPojo = getHotelAllPojoArrayList.get(position);


        //HotelName
        String hotel_name= getHotelAllPojoArrayList.get(position).getHotelName();

        holder.txtStoreName.setText(hotel_name);

        //address
        String address= getHotelAllPojoArrayList.get(position).getAddress();

        holder.txtStoreAddr.setText(address);

        //total_rating
        String total_rating= getHotelAllPojoArrayList.get(position).getTotalFoodExprience();
        holder.rtng_restaurant.setRating(Float.parseFloat(total_rating));


        Image_load_activity.loadGooglePhoto(context, holder.img_location_icon, getHotelAllPojoArrayList.get(position).getPhotoReference());



        holder.ll_overall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, View_restaurant_details.class);
                intent.putExtra("Hotel_name", getHotelAllPojoArrayList.get(position).getHotelName());
                intent.putExtra("Hotel_rating", getHotelAllPojoArrayList.get(position).getTotalFoodExprience());
                intent.putExtra("Hotel_location", getHotelAllPojoArrayList.get(position).getAddress());
                intent.putExtra("Hotel_icon", getHotelAllPojoArrayList.get(position).getPhotoReference());

                context.startActivity(intent);
            }
        });






        /*Call<GetHotelReviewImage_output> call = apiInterface.get_hotel_review_image_all("get_hotel_review_image_all", Integer.parseInt( getHotelAllPojoArrayList.get(position)));

        call.enqueue(new Callback<GetHotelReviewImage_output>() {
            @Override
            public void onResponse(Call<GetHotelReviewImage_output> call, Response<GetHotelReviewImage_output> response) {

                String responseStatus = response.body().getResponseMessage();

                Log.e("responseStatus", "responseStatus->" + responseStatus);

                if (responseStatus.equals("success")) {

                    //Success






                }
            }

            @Override
            public void onFailure(Call<GetHotelReviewImage_output> call, Throwable t) {
                //Error
                Log.e("FailureError", "FailureError" + t.getMessage());
            }
        });


*/





    }






    private  class LongOperation_like extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {


            for (int i = 0; i < 5; i++) {

            }
            return "Executed";
        }


        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {



        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    private class LongOperation_unlike extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {


            for (int i = 0; i < 5; i++) {
                try {
                    Thread.sleep(2000);

                    try {

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } catch (InterruptedException e) {
                    Thread.interrupted();
                }
            }
            return "Executed";
        }


        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {



        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }


    @Override
    public int getItemCount() {

        return getHotelAllPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtStoreName, txtStoreAddr;
        RelativeLayout rl_follow, rl_likesnew_layout;

        LinearLayout ll_hotel,ll_overall;

        ImageView img_location_icon;

        RatingBar rtng_restaurant;


        public MyViewHolder(View itemView) {
            super(itemView);
            //TextView
            txtStoreName = (TextView) itemView.findViewById(R.id.txtStoreName);
            txtStoreAddr = (TextView) itemView.findViewById(R.id.txtStoreAddr);

            //Imageview
            img_location_icon = (ImageView) itemView.findViewById(R.id.img_location_icon);

            //Rating Bar
            rtng_restaurant = (RatingBar) itemView.findViewById(R.id.rtng_restaurant);

           //LinearLayout
            ll_hotel = (LinearLayout) itemView.findViewById(R.id.ll_hotel);
            ll_overall = (LinearLayout) itemView.findViewById(R.id.ll_overall);
        }


    }


}
