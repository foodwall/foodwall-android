package com.kaspontech.foodwall.reviewpackage;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.hsalf.smilerating.SmileRating;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_image_adapter;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Utility;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mabbas007.tagsedittext.TagsEditText;

//

public class WriteReviewActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {


    private static final String TAG = "WriteReviewActivity";

    // Action Bar
    Toolbar actionBar;
    ImageButton back;
    Button btn_addImages, btn_create_review;
    ViewPager reviewpager;
    int CAPTURE_IMAGE_CALLBACK = 1;
    int SELECT_PICTURE_CALLBACK = 2;
    int PICK_IMAGE_MULTIPLE = 3;
    String imageEncoded;
    List<String> imagesEncodedList;
    String[] filePathColumn;

    Bitmap bitmap;
    EditText worst_dishes;
    String selectedImagePath;
    //ImageView overall
    ImageView image_add_dish, image_add, rtg_one,rtg_two,rtg_three,rtg_four,rtg_five;

    //Imageview ambiance
    ImageView rtg_one_ambiance,rtg_two_ambiance,rtg_three_ambiance,rtg_four_ambiance,rtg_five_ambiance;

    //Imageview taste
      ImageView rtg_one_taste, rtg_two_taste,rtg_three_taste,rtg_four_taste,rtg_five_taste;

      //Imageview service
    ImageView rtg_one_service,rtg_two_service,rtg_three_service,rtg_four_service,rtg_five_service;

    //Imageview value
     ImageView rtg_one_value,rtg_two_value,rtg_three_value,rtg_four_value,rtg_five_value;

    public static PhotoView img_addimage;

    //Image reset

    ImageView img_reset,img_reset_ambiance,img_reset_taste,img_reset_value,img_reset_serive;
    Spinner spinner,dish_spinner_worst;
    TextView txt_add_image;
    //Textview ratingoverall
    TextView txt_rating;

    //Textview ambiance

    TextView txt_rating_ambiance;
    //Textview taste

    TextView txt_rating_taste;

    //Textview service

    TextView txt_rating_service;


    //Textview value
    TextView  txt_rating_value,txt_rating_low,txt_rating_low_ambiance,txt_rating_low_taste,txt_rating_low_service,txt_rating_low_value;
    public static LinearLayout ll_image_review, ll_edt_add_dish,ll_edt_tag_dish,ll_edt_tag_worst_dish;


    Review_image_adapter reviewImageAdapter;

    //Rating bar

//    RatingBar ratingBar;
    SmileRating ratingBar, rtng_ambiance,rtng_taste,rtng_service,rtng_value;


    //Popup details
    Button done_btn;
    ImageView img_food;
    TextView txt_food_caption;


    String[] Dishtype_top = {"Select dish type", "Top dishes"};
    String[] Dishtype_worst = {"Select dish type", "Worst dishes"};


    String[] Top_dish = new String[]{
            "Chapati/Roti",
            "Naan",
            "Butter Naan",
            "Paratha",
            "Aloo Paratha",
            "Chapathi",
            "Puri",
            "Papadam",
            "Aloo tikki",
            "Butter chicken",
            "Coconut Rice",
            "Lemon Rice",
            "Jeera Rice",
            "Coconut Rice",
            "Veg Biryani",
            "Chicken Hariyali",
            "Tandoori Chicken",
            "Chicken MalaiKabab",
            "SheekhKabab",
            "Chili Chicken",
            "Grilled Chicken",
            "Keema Samosa",
            "Chicken 65",
            "Jumbo Bagari Spiced Shrimp",
            "Vegetable FormaishiKabab",
            "Tandoori Mix Grill",
            "Tandoori Lobster",
            "Lamb BotiKabab",
            "Tandoori Fish Tikka",
            "Tandoori Jumbo Shrimp",
            "Chicken Biryani",
            "Mutton Biryani",
            "Fish Biryani",
            "Gobi Biryani",
            "Beef Biryani",
            "Mushroom Biryani",
            "Shrimp Biryani",
            "Goose Biryani",
            "Chana masala ",
            "Chicken Tikka masala",
            "Dosa",
            "Masala dosa",
            "Momos",
            "Mutton korma",
            "Pakora",
            "Paneer",
            "Pulao",
            "Chicken Roll",
            "Mutton Roll",
            "Samosa",
            "Shahi paneer",
            "Thali",
            "Vadas",
            "Halwa",
            "Rasgulla/Roshogulla",
            "Kulfi",
            "Kheer",


    };

    String[] worst_dish = new String[]{
            "Chapati/Roti",
            "Naan",
            "Butter Naan",
            "Paratha",
            "Aloo Paratha",
            "Chapathi",
            "Puri",
            "Papadam",
            "Aloo tikki",
            "Butter chicken",
            "Coconut Rice",
            "Lemon Rice",
            "Jeera Rice",
            "Coconut Rice",
            "Veg Biryani",
            "Chicken Hariyali",
            "Tandoori Chicken",
            "Chicken Malai Kabab",
            "SheekhKabab",
            "Chilli Chicken",
            "Grilled Chicken",
            "Keema Samosa",
            "Chicken 65",
            "Jumbo Bagari Spiced Shrimp",
            "Vegetable Formaishi Kabab",
            "Tandoori Mix Grill",
            "Tandoori Lobster",
            "Lamb BotiKabab",
            "Tandoori Fish Tikka",
            "Tandoori Jumbo Shrimp",
            "Chicken Biryani",
            "Mutton Biryani",
            "Fish Biryani",
            "Gobi Biryani",
            "Beef Biryani",
            "Mushroom Biryani",
            "Shrimp Biryani",
            "Goose Biryani",
            "Chana masala ",
            "Chicken Tikka masala",
            "Dosa",
            "Masala dosa",
            "Momos",
            "Mutton korma",
            "Pakora",
            "Paneer",
            "Pulao",
            "Chicken Roll",
            "Mutton Roll",
            "Samosa",
            "Shahi paneer",
            "Thali",
            "Vadas",
            "Halwa",
            "Rasgulla",
            "Kulfi",
            "Kheer",


    };

    List<String> auto_top_dish = new ArrayList<>(Arrays.asList(Top_dish));
    List<String> auto_worst_dish = new ArrayList<>(Arrays.asList(worst_dish));
    AutoCompleteTextView autoCompleteTextView;
    String type;
    String autocompleteresult,tageditresult,tagedit_worst_result;
    TagsEditText tagsedittext,tagsedittext_worst;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_review);
//        setContentView(R.layout.create_review_unused);
        setContentView(R.layout.create_review_ratingbar);

        actionBar = (Toolbar) findViewById(R.id.action_bar_write_post);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        btn_addImages = (Button) findViewById(R.id.btn_addImages);
        btn_addImages.setOnClickListener(this);


        reviewpager = (ViewPager) findViewById(R.id.reviewpager);
        tagsedittext = (TagsEditText) findViewById(R.id.tagsedittext);
        tagsedittext_worst = (TagsEditText) findViewById(R.id.tagsedittext_worst);
        tagsedittext_worst.setImeOptions(EditorInfo.IME_ACTION_DONE);


        tagsedittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {


                    View popupView = LayoutInflater.from(WriteReviewActivity.this).inflate(R.layout.review_popup_layout, null);
                    final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    popupWindow.showAsDropDown(popupView, 0, 0);
                    done_btn = popupView.findViewById(R.id.done_btn);
                    img_food = popupView.findViewById(R.id.img_food);


                    done_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            popupWindow.dismiss();
                        }
                    });
                    // Do whatever you want here
                    return true;
                }
                return false;
            }
        });

        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        /*autoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    View popupView = LayoutInflater.from(WriteReviewActivity.this).inflate(R.layout.review_popup_layout, null);
                    final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    popupWindow.showAsDropDown(popupView, 0, 0);
                    done_btn = popupView.findViewById(R.id.done_btn);
                    img_food = popupView.findViewById(R.id.img_food);
                    txt_food_caption = popupView.findViewById(R.id.txt_food_caption);

                    done_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            popupWindow.dismiss();
                        }
                    });
                    // Do whatever you want here
                    return true;
                }
                return false;
            }
        });*/

        image_add_dish = (ImageView) findViewById(R.id.image_add_dish);
        image_add_dish.setOnClickListener(this);

    /*    image_add = (ImageView) findViewById(R.id.image_add);
        image_add.setOnClickListener(this);*/

        img_addimage = (PhotoView) findViewById(R.id.img_addimage);
        img_addimage.setOnClickListener(this);

        //Smile Rating
        ratingBar = (SmileRating) findViewById(R.id.rtng_overall);
        rtng_ambiance= (SmileRating) findViewById(R.id.rtng_ambiance);
        rtng_service= (SmileRating) findViewById(R.id.rtng_service);
        rtng_taste= (SmileRating) findViewById(R.id.rtng_taste);
        rtng_value= (SmileRating) findViewById(R.id.rtng_value);

        //TextView
        txt_rating_low = (TextView) findViewById(R.id.txt_rating_low);
        txt_rating_low_ambiance = (TextView) findViewById(R.id.txt_rating_low_ambiance);
        txt_rating_low_taste = (TextView) findViewById(R.id.txt_rating_low_taste);
        txt_rating_low_service = (TextView) findViewById(R.id.txt_rating_low_service);
        txt_rating_low_value = (TextView) findViewById(R.id.txt_rating_low_value);


        img_reset=(ImageView)findViewById(R.id.img_reset);
        img_reset.setOnClickListener(this);

        img_reset_ambiance=(ImageView)findViewById(R.id.img_reset_ambiance);
        img_reset_ambiance.setOnClickListener(this);

        img_reset_serive=(ImageView)findViewById(R.id.img_reset_serive);
        img_reset_serive.setOnClickListener(this);

        img_reset_taste=(ImageView)findViewById(R.id.img_reset_taste);
        img_reset_taste.setOnClickListener(this);

        img_reset_value=(ImageView)findViewById(R.id.img_reset_value);
        img_reset_value.setOnClickListener(this);




//        worst_dishes = (EditText) findViewById(R.id.worst_dishes);


        btn_create_review = (Button) findViewById(R.id.btn_create_review);
        btn_create_review.setOnClickListener(this);


        ll_image_review = (LinearLayout) findViewById(R.id.ll_image_review);
        ll_edt_add_dish = (LinearLayout) findViewById(R.id.ll_edt_add_dish);
        ll_edt_tag_dish = (LinearLayout) findViewById(R.id.ll_edt_tag_dish);
        ll_edt_tag_worst_dish = (LinearLayout) findViewById(R.id.ll_edt_tag_worst_dish);


        spinner = (Spinner) findViewById(R.id.dish_spinner);
        dish_spinner_worst = (Spinner) findViewById(R.id.dish_spinner_worst);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                if (position == 0) {
                    type = spinner.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.GONE);
//                    ll_edt_add_dish.setVisibility(View.GONE);
                    ll_edt_tag_dish.setVisibility(View.GONE);
                }

                if (position == 1) {
                    type = spinner.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.VISIBLE);
//                    ll_edt_add_dish.setVisibility(View.VISIBLE);
                    ll_edt_tag_dish.setVisibility(View.VISIBLE);
                } /*else if (position == 2) {
                    type = spinner.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.VISIBLE);
//                    ll_edt_add_dish.setVisibility(View.VISIBLE);
                    ll_edt_tag_dish.setVisibility(View.VISIBLE);
                }*/

                if (type.equals("Top dishes")) {


                    try {
                        ArrayAdapter arrayAdapter = new ArrayAdapter(WriteReviewActivity.this, R.layout.autocompletetxt, Top_dish);
                        tagsedittext.setAdapter(arrayAdapter);
                        autoCompleteTextView.setAdapter(arrayAdapter);
                        autoCompleteTextView.setThreshold(1);
                        autocompleteresult = autoCompleteTextView.getText().toString();
                        tageditresult = tagsedittext.getText().toString();





                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }/* else if (type.equals("Worst dishes")){

                    try {
                        ArrayAdapter arrayAdapter = new ArrayAdapter(WriteReviewActivity.this, R.layout.autocompletetxt, worst_dish);
                        autoCompleteTextView.setAdapter(arrayAdapter);
                        tagsedittext.setAdapter(arrayAdapter);
                        autoCompleteTextView.setThreshold(1);
                        autocompleteresult = autoCompleteTextView.getText().toString();
                        tageditresult = tagsedittext.getText().toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


     //Worst spinner
        dish_spinner_worst.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                if (position == 0) {
                    type = dish_spinner_worst.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.GONE);
//                    ll_edt_add_dish.setVisibility(View.GONE);
                    ll_edt_tag_worst_dish.setVisibility(View.GONE);
                }
/*
                if (position == 1) {
                    type = dish_spinner_worst.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.VISIBLE);
//                    ll_edt_add_dish.setVisibility(View.VISIBLE);
                    ll_edt_tag_worst_dish.setVisibility(View.VISIBLE);
                } */else if (position == 1) {
                    type = dish_spinner_worst.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.VISIBLE);
//                    ll_edt_add_dish.setVisibility(View.VISIBLE);
                    ll_edt_tag_worst_dish.setVisibility(View.VISIBLE);
                }

             /*   if (type.equals("Top dishes")) {


                    try {
                        ArrayAdapter arrayAdapter = new ArrayAdapter(WriteReviewActivity.this, R.layout.autocompletetxt, Top_dish);
                        tagsedittext_worst.setAdapter(arrayAdapter);
                        autoCompleteTextView.setAdapter(arrayAdapter);
                        autoCompleteTextView.setThreshold(1);
                        autocompleteresult = autoCompleteTextView.getText().toString();
                        tagedit_worst_result = tagsedittext_worst.getText().toString();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else*/ if (type.equals("Worst dishes")){

                    try {
                        ArrayAdapter arrayAdapter = new ArrayAdapter(WriteReviewActivity.this, R.layout.autocompletetxt, worst_dish);
                        autoCompleteTextView.setAdapter(arrayAdapter);
                        tagsedittext_worst.setAdapter(arrayAdapter);
                        autoCompleteTextView.setThreshold(1);
                        autocompleteresult = autoCompleteTextView.getText().toString();
                        tagedit_worst_result = tagsedittext_worst.getText().toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });





        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Dishtype_top);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aa);



        ArrayAdapter bb = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Dishtype_worst);
        bb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        dish_spinner_worst.setAdapter(bb);
//        View popupView2 = LayoutInflater.from(WriteReviewActivity.this).inflate(R.layout.review_popup_layout, null);
//        final PopupWindow popupWindow2 = new PopupWindow(popupView2, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        popupWindow2.showAsDropDown(popupView, 0, 0);
//        done_btn = popupView.findViewById(R.id.done_btn);
//        img_food = popupView.findViewById(R.id.img_food);
//        txt_food_caption = popupView.findViewById(R.id.txt_food_caption);
//
//
//        done_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                popupWindow.dismiss();
//            }
//        });



        addListenerOnRatingBar();



    }



    //Custom alert

    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public AppCompatActivity c;
        public Dialog d;
        public AppCompatButton btn_Ok;
        TextView txt_open_camera,txt_open_gallery;

        CustomDialogClass(AppCompatActivity a) {
            super(a);
            // TODO Auto-generated constructor stub
            c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
//            setContentView(R.layout.dialog_version_info);

            setContentView(R.layout.select_picture_layout_for_review);

            txt_open_camera = (TextView) findViewById(R.id.txt_open_camera);
            txt_open_gallery = (TextView) findViewById(R.id.txt_open_gallery);


            txt_open_camera.setOnClickListener(this);
            txt_open_gallery.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_open_camera:

                    startActivity(new Intent(WriteReviewActivity.this, Review_take_photo.class));
                    dismiss();
                    break;
                case R.id.txt_open_gallery:
                    dismiss();

                    try {

                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);


                          /*  Intent pictureActionIntent = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(
                                    pictureActionIntent,
                                    SELECT_PICTURE_CALLBACK);*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                default:
                    break;
            }
        }
    }














    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:
                finish();
                break;




            case R.id.btn_addImages:
//                OpenImage();
//                OpenGallery();
//                startDialog();
                startActivity(new Intent(WriteReviewActivity.this, GalleryImage_Review.class));
                break;

            case R.id.image_add_dish:
                CustomDialogClass ccd = new CustomDialogClass(this);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccd.setCancelable(true);
                ccd.show();
                break;

           /* case R.id.image_add:
                CustomDialogClass ccd2 = new CustomDialogClass(this);
                ccd2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccd2.setCancelable(true);
                ccd2.show();
                break;*/

                case R.id.img_reset:
                    txt_rating_low.setText("0");
                    String res= tagsedittext.getText().toString();


                break;

                case R.id.img_reset_ambiance:
                    txt_rating_low_ambiance.setText("0");

                break;

                case R.id.img_reset_serive:
                    txt_rating_low_service.setText("0");
                break;

                case R.id.img_reset_taste:
                    txt_rating_low_taste.setText("0");
                break;

                case R.id.img_reset_value:
                    txt_rating_low_value.setText("0");
                break;
        }

    }




    // rating bar clicked method

    public void addListenerOnRatingBar() {

        //if rating value is changed,
        //display the current rating value in the result (textview) automatically
        ratingBar.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
              /*  if(level==2){
                    ratingBar.setNormalColor(getResources().getColor(R.color.colorPrimary));
                } else  if(level==3){
                    ratingBar.setNormalColor(getResources().getColor(R.color.colorPrimary));
                }else  if(level==4){
                    ratingBar.setNormalColor(getResources().getColor(R.color.colorPrimary));
                }else  if(level==5){
                    ratingBar.setNormalColor(getResources().getColor(R.color.dark_green));
                }*/

                txt_rating_low.setText(String.valueOf(level));


            }
        });

        rtng_ambiance.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_ambiance.setText(String.valueOf(level));
            }
        });


        rtng_service.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_service.setText(String.valueOf(level));
            }
        });
        rtng_taste.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_taste.setText(String.valueOf(level));
            }
        });
        rtng_value.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_value.setText(String.valueOf(level));

            }
        });

    }








    public void OpenImage() {
        try {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, SELECT_PICTURE_CALLBACK);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "No gallery: " + e);
        }
    }

    public void OpenGallery() {

        Intent pictureActionIntent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(
                pictureActionIntent,
                SELECT_PICTURE_CALLBACK);

    }


    private void startDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(this);
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        try {


                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);


                          /*  Intent pictureActionIntent = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(
                                    pictureActionIntent,
                                    SELECT_PICTURE_CALLBACK);*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        startActivity(new Intent(WriteReviewActivity.this, Review_take_photo.class));

                    }
                });
        myAlertDialog.show();
    }


    //old activity result
 /*   @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {




        if (resultCode == RESULT_OK && requestCode == CAPTURE_IMAGE_CALLBACK) {


            Bitmap bmp = (Bitmap) data.getExtras().get("data");
            Uri tempUri = getImageUri(this, bmp);


            bmp = Bitmap.createScaledBitmap(bmp, 400, 400, false);

            img_addimage.setImageBitmap(bmp);

            Pref_storage.setDetail(this, "Review_image", getRealPathFromURI(tempUri));


        } else if (resultCode == RESULT_OK && requestCode == PICK_IMAGE_MULTIPLE) {
            imagesEncodedList = new ArrayList<String>();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            if(data.getData()!=null){

                Uri mImageUri=data.getData();
                // Get the cursor
                Cursor cursor = getContentResolver().query(mImageUri,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imageEncoded  = cursor.getString(columnIndex);
                cursor.close();
                Toast.makeText(this, ""+imageEncoded, Toast.LENGTH_SHORT).show();
               *//* img_addimage.setImageURI(Uri.parse(imageEncoded));*//*


            } else {
                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        mArrayUri.add(uri);
                        // Get the cursor
                        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        imageEncoded  = cursor.getString(columnIndex);
                        imagesEncodedList.add(imageEncoded);
                        Toast.makeText(this, ""+imagesEncodedList.toString(), Toast.LENGTH_SHORT).show();
                        cursor.close();

                    }
                    Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                }
            }
        } else {
            Toast.makeText(this, "You haven't picked Image",
                    Toast.LENGTH_LONG).show();
        }




                *//*Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor activity = getContentResolver().query(selectedImage, filePath,
                        null, null, null);
                activity.moveToFirst();
                int columnIndex = activity.getColumnIndex(filePath[0]);
                selectedImagePath = activity.getString(columnIndex);
                activity.close();

                bitmap = BitmapFactory.decodeFile(selectedImagePath); // load
                // preview image
                bitmap = Bitmap.createScaledBitmap(bitmap, 400, 400, false);


                img_addimage.setImageBitmap(bitmap);*//*



        super.onActivityResult(requestCode, resultCode, data);
    }





    public Uri getImageUri(Context inContext, Bitmap inImage) {
      *//*  ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);*//*
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getApplicationContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
//            Toast.makeText(mContext, "vis"+result, Toast.LENGTH_SHORT).show();
            cursor.close();
        }
        return result;

    }
*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {

            // When an Image is picked
       /*
            if (requestCode == CAPTURE_IMAGE_CALLBACK && resultCode == RESULT_OK  && data!=null) {


                Bitmap bmp = (Bitmap) data.getExtras().get("data");
                Uri tempUri = getImageUri(this, bmp);
                Toast.makeText(this, ""+tempUri.toString(), Toast.LENGTH_SHORT).show();

                bmp = Bitmap.createScaledBitmap(bmp, 400, 400, false);

               *//* img_addimage.setImageBitmap(bmp);*//*

                Pref_storage.setDetail(this, "Review_image", getRealPathFromURI(tempUri));


            }
*/

            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && data != null) {
                // Get the Image from data

          /*      Uri selectedImage = data.getData();

                String urlstring = getPathFromURI(selectedImage);

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor activity = getApplicationContext().getContentResolver().query(selectedImage, new String[]{MediaStore.Images.Media.DATA}, null, null, null);
                activity.moveToFirst();

                int columnIndex = activity.getColumnIndex(MediaStore.Images.Media.DATA);
                String picturePath = activity.getString(columnIndex);
                activity.close();

               img_addimage.setImageURI(Uri.parse(picturePath));
*/


                if (data.getData() != null) {

                    imagesEncodedList = new ArrayList<String>();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Uri mImageUri = data.getData();


                    Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                    cursor.moveToFirst();
                    String document_id = cursor.getString(0);
                    document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                    cursor.close();

                    cursor = getContentResolver().query(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                    cursor.moveToFirst();
                    String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    cursor.close();


                    Toast.makeText(this, "selectedImage\n" + path, Toast.LENGTH_SHORT).show();

                    /* img_addimage.setImageURI(Uri.parse(path));*/
                    Utility.picassoImageLoader(path,
                            0,img_addimage, getApplicationContext());
//                    GlideApp.with(this)
//                            .load(path)
//                            .centerCrop()
//                            .into(img_addimage);

                    ll_image_review.setVisibility(View.VISIBLE);
                    img_addimage.setVisibility(View.VISIBLE);



      /*              Log.v(TAG, "Selected Images" +mImageUri.toString());
                    Toast.makeText(this, "selectedImage\n"+mImageUri, Toast.LENGTH_SHORT).show();

                    // Get the cursor
                    Cursor cursor = getApplicationContext().getContentResolver().query(mImageUri, filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATA);
                    String imageEncoded  = cursor.getString(columnIndex);
                    Toast.makeText(this, ""+imageEncoded, Toast.LENGTH_SHORT).show();
                    cursor.close();

*/
                    imagesEncodedList.add(path);

                    reviewImageAdapter = new Review_image_adapter(getApplicationContext(), imagesEncodedList);
                    reviewImageAdapter.notifyDataSetChanged();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            String[] filePathColumn = {MediaStore.Images.Media.DATA};
                            // Get the cursor
                            Cursor cursor = getApplicationContext().getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                            String imageEncoded = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();

                            reviewImageAdapter = new Review_image_adapter(getApplicationContext(), imagesEncodedList);
                            reviewImageAdapter.notifyDataSetChanged();


                            Log.v(TAG, "Selected Images" + imagesEncodedList.toString());
                        }
                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }
                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
          /*  Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();*/
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
      /*  ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);*/
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getApplicationContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);

            cursor.close();

        }
        return result;

    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getApplicationContext().getContentResolver().query(contentUri, proj, "", null, "");
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
            Toast.makeText(this, "" + res, Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return res;


    }

    public String getpathofuri(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            autoCompleteTextView.setVisibility(View.GONE);
            ll_edt_add_dish.setVisibility(View.GONE);
        }

        if (position == 1) {
            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);
        } else if (position == 2) {
            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        autoCompleteTextView.setVisibility(View.GONE);
        ll_edt_add_dish.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            autoCompleteTextView.setVisibility(View.GONE);
            ll_edt_add_dish.setVisibility(View.GONE);
        }

        if (position == 1) {
            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);
        } else if (position == 2) {
            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);
        }

    }
}
