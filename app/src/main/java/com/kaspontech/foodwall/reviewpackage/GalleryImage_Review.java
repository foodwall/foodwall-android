package com.kaspontech.foodwall.reviewpackage;

import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_image_adapter;
import com.kaspontech.foodwall.R;

import java.util.ArrayList;
import java.util.List;

public class GalleryImage_Review extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "GalleryImage_Review";

    // Action Bar
    Toolbar actionBar;
    ImageButton back;
    Button btn_addImages, btn_create_review;
    int CAPTURE_IMAGE_CALLBACK = 1;
    int SELECT_PICTURE_CALLBACK = 2;
    int PICK_IMAGE_MULTIPLE = 1;
    String imageEncoded;
    public static List<String> imagesEncodedList;

    String[] filePathColumn;

    Bitmap bitmap;
    String selectedImagePath;
    ImageView img_addimage;
    Review_image_adapter reviewImageAdapter;
    ViewPager reviewpager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_review);

        actionBar = (Toolbar) findViewById(R.id.action_bar_write_post);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);
        btn_addImages = (Button) findViewById(R.id.btn_addImages);
        btn_addImages.setOnClickListener(this);

        btn_create_review = (Button) findViewById(R.id.btn_create_review);
        btn_create_review.setOnClickListener(this);
        reviewpager =(ViewPager)findViewById(R.id.reviewpager);
       /* img_addimage = (ImageView) findViewById(R.id.img_addimage);
        img_addimage.setOnClickListener(this);*/

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:
                finish();
                break;

            case R.id.btn_addImages:

                startDialog();

                break;
        }

    }


    public void OpenImage() {
        try {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, SELECT_PICTURE_CALLBACK);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "No gallery: " + e);
        }
    }

    public void OpenGallery() {


        Intent pictureActionIntent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(
                pictureActionIntent,
                SELECT_PICTURE_CALLBACK);

    }


    private void startDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(this);
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        try {


                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);

                          /*  Intent pictureActionIntent = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(
                                    pictureActionIntent,
                                    SELECT_PICTURE_CALLBACK);*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {


                        try {
                            Intent intent = new Intent(
                                    MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, CAPTURE_IMAGE_CALLBACK);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
        myAlertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            // When an Image is picked
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && data!=null) {
                // Get the Image from data

          /*      Uri selectedImage = data.getData();

                String urlstring = getPathFromURI(selectedImage);

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor activity = getApplicationContext().getContentResolver().query(selectedImage, new String[]{MediaStore.Images.Media.DATA}, null, null, null);
                activity.moveToFirst();

                int columnIndex = activity.getColumnIndex(MediaStore.Images.Media.DATA);
                String picturePath = activity.getString(columnIndex);
                activity.close();

               img_addimage.setImageURI(Uri.parse(picturePath));
*/



                if(data.getData()!=null){

                    imagesEncodedList = new ArrayList<String>();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    Uri mImageUri=data.getData();


                    Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                    cursor.moveToFirst();
                    String document_id = cursor.getString(0);
                    document_id = document_id.substring(document_id.lastIndexOf(":")+1);
                    cursor.close();

                    cursor = getContentResolver().query(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                    cursor.moveToFirst();
                    String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    cursor.close();
                    /*img_addimage.setImageURI(Uri.parse(path));*/



      /*              Log.v(TAG, "Selected Images" +mImageUri.toString());
                    Toast.makeText(this, "selectedImage\n"+mImageUri, Toast.LENGTH_SHORT).show();

                    // Get the cursor
                    Cursor cursor = getApplicationContext().getContentResolver().query(mImageUri, filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATA);
                    String imageEncoded  = cursor.getString(columnIndex);
                    Toast.makeText(this, ""+imageEncoded, Toast.LENGTH_SHORT).show();
                    cursor.close();

*/
                    imagesEncodedList.add(path);

                    reviewImageAdapter= new Review_image_adapter(getApplicationContext(),imagesEncodedList);
                    reviewImageAdapter.notifyDataSetChanged();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            String[] filePathColumn = { MediaStore.Images.Media.DATA };
                            // Get the cursor
                            Cursor cursor = getApplicationContext().getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                            String imageEncoded  = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();

                            reviewImageAdapter= new Review_image_adapter(getApplicationContext(),imagesEncodedList);
                            reviewImageAdapter.notifyDataSetChanged();




                            Log.v(TAG, "Selected Images" +imagesEncodedList.toString());
                        }
                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }
                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
          /*  Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();*/
        }

        super.onActivityResult(requestCode, resultCode, data);
    }





    public Uri getImageUri(Context inContext, Bitmap inImage) {
      /*  ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);*/
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getApplicationContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);

            cursor.close();

        }
        return result;

    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getApplicationContext().getContentResolver().query(contentUri, proj, "", null, "");
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
            Toast.makeText(this, ""+res, Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return res;



    }

    public String getpathofuri(Uri uri){
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

}

