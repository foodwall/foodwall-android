package com.kaspontech.foodwall.reviewpackage;


import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.RecyclerViewAdapter;
import com.kaspontech.foodwall.foodFeedsPackage.TotalCountModule_Response;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Geometry_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Get_hotel_photo_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Getall_restaurant_details_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.OpeningHours_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Restaurant_output_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.StoreModel;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.REST_API.API_interface_restaurant;
import com.kaspontech.foodwall.reviewpackage.allTab.AllFragment;
import com.kaspontech.foodwall.reviewpackage.deliveryTab.DeliveryFragment;
import com.kaspontech.foodwall.reviewpackage.dineInTab.NewDineInFragment;
import com.kaspontech.foodwall.reviewpackage.restaurantSearch.ReviewRestaurantSearch;
import com.kaspontech.foodwall.reviewpackage.Review_adapter_folder.GetAllHotelAdapter;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * A simple {@link Fragment} subclass.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class Reviews extends Fragment implements View.OnClickListener, ScrollView.OnScrollChangeListener, ScrollView.OnTouchListener {
    /**
     * Permission integer value
     */
    private final static int ALL_PERMISSIONS_RESULT = 101;
    /**
     * Recycler view
     */
    public RecyclerView review_recyclerview;
    /**
     * View
     */
    View view;

    /**
     * Context for review
     */
    Context context;
    /**
     * Tab layout
     */
    TabLayout reviewsTablayout;
    /**
     * Tab position integer value
     */
    int tabPosition;
    /**
     * Fragments dinein, delivery and all types of veg and nonveg
     */
    Fragment dineIn, delivery, veg, nonVeg, alltypes;
    /**
     * Action bar
     */
    Toolbar actionBar;
    /**
     * Filter button
     */
    public static FloatingActionButton review_filter;
    /**
     * Add review button
     */
    ImageButton add_review;
    /**
     * Location button
     */
    ImageButton img_location;
    /**
     * More button
     */
    ImageButton img_more;

    /**
     * Add review text
     */
    TextView txt_add_review;
    /**
     * View more text
     */
    TextView txt_view_more;
    /**
     * Edittext for  location
     */
    EditText edt_location;

    /**
     * Location finding pojo list's
     */
    List<Get_hotel_photo_pojo> photos;
    List<OpeningHours_pojo> openingHoursPojoList;
    List<String> types;
    List<String> htmlAttributions;
    List<StoreModel> storeModels;

    /**
     * Location finding pojo
     */
    Geometry_pojo geometryPojo;
    OpeningHours_pojo openingHoursPojo;

    /**
     * Restaurant API interface
     */

    API_interface_restaurant apiService;
    /**
     * Lat and long results
     */

    /*
     * Scroll view
     * */

    public static ScrollView scroll_review_new;
    String latLngString;
    LatLng latLng;

    /**
     * All restaurant details arraylist
     */

    ArrayList<Getall_restaurant_details_pojo> getallRestaurantDetailsPojoArrayList = new ArrayList<>();
    List<Restaurant_output_pojo> restaurantOutputPojoList;

    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private List<Object> weekdayText;


    /**
     * String result for multiple inputs of the arraylist
     */

    String   hotel_id,   veg_nonveg,  ambiance, taste, service, createdby, category_type, next_token, location_result;



    /**
     * View more layout
     */
    RelativeLayout ll_view_more;


    /**
     * Online checking using IMEI
     */
    TelephonyManager telephonyManager;


    /**
     * Bottom sheet for sorting
     */
    BottomSheetDialog dialog;

    /**
     * String results for sorting
     */
    String vegNonVeg, ratingSort;
    /**
     * View pager
     */

    ViewPager viewpager_review;

    /**
     * String for IMEI
     */
    String imei;
    /**
     * double values for lat and long
     */
    double latitude_online;
    double longitude_online;

    public Reviews() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("HardwareIds")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        onSaveInstanceState(savedInstanceState);

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_reviews, container, false);
        context = view.getContext();


        initComponents();
        /* APi for hotel list */
        edt_location.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    location_result = edt_location.getText().toString().trim();
                    locationSearch(location_result);
                }
                return false;
            }
        });


        /* Next location search */

        ll_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nextLocation(next_token);


            }
        });
        /* Next location search */

        img_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextLocation(next_token);

            }
        });

        /* Next location search */

        txt_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextLocation(next_token);

            }
        });


        /* Veg/Non veg filter */

        review_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Bottom sheet dialog view*/

                showBottomSheetDialog();



            }
        });



        /*Add review text on click listener*/
        txt_add_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ReviewTestActivity.class));
             }
        });

        /* Tablayout for review*/

        reviewsTablayout = (TabLayout) view.findViewById(R.id.tab_reviews);


        /*Fragment initialization*/

        /*Dinein*/
        dineIn = new NewDineInFragment();
        /*Delivery*/
        delivery = new DeliveryFragment();
        /*All types*/
        alltypes = new AllFragment();

        /*Fragment title initialization*/
        reviewsTablayout.addTab(reviewsTablayout.newTab().setText("ALL"));
        reviewsTablayout.addTab(reviewsTablayout.newTab().setText("DINE-IN"));
        reviewsTablayout.addTab(reviewsTablayout.newTab().setText("DELIVERY"));


        Intent intent = getActivity().getIntent();

        String redirectView = intent.getStringExtra("redirectView");
        if (redirectView != null) {
            if (redirectView.equals("All_View")) {
                replaceFragment(alltypes);
                reviewsTablayout.getTabAt(0).select();
            } else if (redirectView.equals("NewDinein_View")) {
                replaceFragment(dineIn);
                reviewsTablayout.getTabAt(1).select();
            } else if (redirectView.equals("delivery_View")) {
                replaceFragment(delivery);
                reviewsTablayout.getTabAt(2).select();
            }
        } else {

            replaceFragment(alltypes);
        }


        /*On tab select listener*/
        bindWidgetsWithReviewTopics();

        /*API Client for restautarnt initialization*/

        apiService = APIClient_restaurant.getClient().create(API_interface_restaurant.class);

        /*Permission to add fine location and coarse location*/

        permissions.add(ACCESS_FINE_LOCATION);

        permissions.add(ACCESS_COARSE_LOCATION);

        /*To get IMEI for the device*/
        telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return view;
        }
        /*String result for the devie id or IMEI*/
        imei = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        //  get_total_count_modules();
    }


    private void initComponents() {

        /*View pager for swaping when swiping left or right*/
        viewpager_review = (ViewPager) view.findViewById(R.id.viewpager_review);

        /*Action bar initialization*/
        actionBar = (Toolbar) view.findViewById(R.id.action_bar_review);



        /*Text for add review*/

        txt_add_review = (TextView) view.findViewById(R.id.txt_add_review);

        /*Text for view more*/

        txt_view_more = (TextView) view.findViewById(R.id.txt_view_more);

        review_filter = (FloatingActionButton) view.findViewById(R.id.fab_review_filter);

        /*Image button location*/
        img_location = (ImageButton) actionBar.findViewById(R.id.img_location);

        /*Image button for image view more*/
        img_more = (ImageButton) view.findViewById(R.id.img_more);

        /*Imaage button add review*/
        add_review = (ImageButton) actionBar.findViewById(R.id.add_review);
        add_review.setOnClickListener(this);

        /*Review recycler view*/
        review_recyclerview = (RecyclerView) view.findViewById(R.id.review_recyclerview);

        /*View more restaurants linear layout*/
        ll_view_more = (RelativeLayout) view.findViewById(R.id.rl_view_more);
        scroll_review_new = (ScrollView) view.findViewById(R.id.scroll_review_new);
        scroll_review_new.setOnScrollChangeListener(this);


        /*Edit text for location*/

        edt_location = (EditText) actionBar.findViewById(R.id.edt_location);
        edt_location.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        edt_location.setOnClickListener(this);
    }


    /*Bottom sheet dialog for sorting dishes*/
    public void showBottomSheetDialog() {

        View view = getLayoutInflater().inflate(R.layout.custom_veg_nonveg_filter, null);
        dialog = new BottomSheetDialog(context);
        dialog.setContentView(view);
        dialog.show();

        /*Widget intialization*/
        ImageButton close_btn, done_btn;
        RadioGroup veg_group, sort_group;
        RadioButton veg_radio, non_veg_redio, hightolow_radio, lowtohighradio;

        close_btn = (ImageButton) dialog.findViewById(R.id.close_btn);
        done_btn = (ImageButton) dialog.findViewById(R.id.done_btn);

        veg_group = (RadioGroup) dialog.findViewById(R.id.veg_group);
        sort_group = (RadioGroup) dialog.findViewById(R.id.sort_group);

        veg_radio = (RadioButton) dialog.findViewById(R.id.veg_radio);
        non_veg_redio = (RadioButton) dialog.findViewById(R.id.non_veg_redio);
        hightolow_radio = (RadioButton) dialog.findViewById(R.id.hightolow_radio);
        lowtohighradio = (RadioButton) dialog.findViewById(R.id.lowtohighradio);

        /*Sort - On veg/ non-veg click click listener */
        veg_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.veg_radio) {

                    Log.e("ReviewSorting", "onCheckedChanged: Veg");
                    vegNonVeg = "1";

                } else if (checkedId == R.id.non_veg_redio) {

                    Log.e("ReviewSorting", "onCheckedChanged: Non Veg");
                    vegNonVeg = "2";

                }
            }
        });
        /*Sort - On high to low / low to high click click listener */

        sort_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                if (checkedId == R.id.hightolow_radio) {

                    Log.e("ReviewSorting", "onCheckedChanged: hightolow");
                    ratingSort = "1";

                } else if (checkedId == R.id.lowtohighradio) {

                    Log.e("ReviewSorting", "onCheckedChanged: lowtohigh");
                    ratingSort = "2";
                }

            }
        });

        /*Done button click listener and API calling*/
        done_btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if (tabPosition == 0) {
                    /*All fragment replacing*/

                    AllFragment allFragment = new AllFragment();
                    Bundle b = new Bundle();

                    if (vegNonVeg == null) {
                        b.putString("veg", "0");
                    } else {
                        b.putString("veg", vegNonVeg);
                    }


                    if (ratingSort == null) {
                        b.putString("sort", "0");
                    } else {
                        b.putString("sort", ratingSort);
                    }
                    allFragment.setArguments(b);
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.review_container, allFragment)
                            .commit();


                } else if (tabPosition == 1) {
                    /*Dinein fragment replacing*/

                    NewDineInFragment dineInFragment = new NewDineInFragment();
                    Bundle b = new Bundle();
                    if (vegNonVeg == null) {
                        b.putString("veg", "0");
                    } else {
                        b.putString("veg", vegNonVeg);
                    }


                    if (ratingSort == null) {
                        b.putString("sort", "0");
                    } else {
                        b.putString("sort", ratingSort);
                    }

                    dineInFragment.setArguments(b);
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.review_container, dineInFragment)
                            .commit();

                } else if (tabPosition == 2) {
                    /*Delivery fragment replacing*/

                    DeliveryFragment deliveryFragment = new DeliveryFragment();
                    Bundle b = new Bundle();
                    if (vegNonVeg == null) {
                        b.putString("veg", "0");
                    } else {
                        b.putString("veg", vegNonVeg);
                    }


                    if (ratingSort == null) {
                        b.putString("sort", "0");
                    } else {
                        b.putString("sort", ratingSort);
                    }

                    deliveryFragment.setArguments(b);
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.review_container, deliveryFragment)
                            .commit();

                }


            }
        });
        /*Close the bottom sheet*/
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


    }


    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        View view = (View) scroll_review_new.getChildAt(scroll_review_new.getChildCount() - 1);
        int diff = (view.getBottom() - (scroll_review_new.getHeight() + scroll_review_new.getScrollY()));

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_UP) {
            // Hidekeyboard
            Utility.hideKeyboard(v);
        }
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            // Hidekeyboard
            Utility.hideKeyboard(v);
        }
        return false;

    }





    /*Filtering veg and non veg*/

    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public FragmentActivity c;
        public Dialog d;
        TextView txt_veg, txt_non_veg;

        CustomDialogClass(FragmentActivity a) {
            super(a);
            // TODO Auto-generated constructor stub
            c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_veg_nonveg_filter);

            txt_veg = (TextView) findViewById(R.id.txt_veg);
            txt_non_veg = (TextView) findViewById(R.id.txt_non_veg);
            txt_veg.setOnClickListener(this);
            txt_non_veg.setOnClickListener(this);


        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_veg:

                    if (tabPosition == 0) {

                        AllFragment allFragment = new AllFragment();
                        Bundle b = new Bundle();
                        b.putString("veg", "1");
                        allFragment.setArguments(b);
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.review_container, allFragment)
                                .commit();
                        dismiss();

                    } else if (tabPosition == 1) {

                        NewDineInFragment dineInFragment = new NewDineInFragment();
                        Bundle b = new Bundle();
                        b.putString("veg", "1");
                        dineInFragment.setArguments(b);
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.review_container, dineInFragment)
                                .commit();
                        dismiss();

                    } else if (tabPosition == 2) {
                        DeliveryFragment deliveryFragment = new DeliveryFragment();
                        Bundle b = new Bundle();
                        b.putString("veg", "1");
                        deliveryFragment.setArguments(b);
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.review_container, deliveryFragment)
                                .commit();
                        dismiss();
                    }


                    break;

                case R.id.txt_non_veg:

                    if (tabPosition == 0) {

                        AllFragment allFragment = new AllFragment();
                        Bundle b = new Bundle();
                        b.putString("veg", "2");
                        allFragment.setArguments(b);
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.review_container, allFragment)
                                .commit();
                        dismiss();
                    } else if (tabPosition == 1) {
                        NewDineInFragment dineInFragment = new NewDineInFragment();
                        Bundle b = new Bundle();
                        b.putString("veg", "2");
                        dineInFragment.setArguments(b);
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.review_container, dineInFragment)
                                .commit();
                        dismiss();
                    } else if (tabPosition == 2) {
                        DeliveryFragment deliveryFragment = new DeliveryFragment();
                        Bundle b = new Bundle();
                        b.putString("veg", "2");
                        deliveryFragment.setArguments(b);
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.review_container, deliveryFragment)
                                .commit();
                        dismiss();
                    }


                    break;

                default:
                    break;
            }
        }
    }

    /*On tab select listener*/
    private void bindWidgetsWithReviewTopics() {

        reviewsTablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }



    /*Setting current tab using its position*/

    private void setCurrentTabFragment(int tab_Position) {


        switch (tab_Position) {

            case 0:
                /*All type fragment*/
                ll_view_more.setVisibility(View.GONE);
                review_recyclerview.setVisibility(View.GONE);

                replaceFragment(alltypes);
                // changeFragment(alltypes,"all");
                tabPosition = 0;

                break;
            case 1:
                /*Dinein type fragment*/

                ll_view_more.setVisibility(View.GONE);
                review_recyclerview.setVisibility(View.GONE);

                replaceFragment(dineIn);
                //changeFragment(dineIn,"dinein");
                tabPosition = 1;


                break;

            case 2:

                /*Delivery type fragment*/

                ll_view_more.setVisibility(View.GONE);
                review_recyclerview.setVisibility(View.GONE);

                replaceFragment(delivery);
                //changeFragment(delivery,"delivery");

                tabPosition = 2;
                break;

            case 3:
                break;
        }

    }

    /*Fragment replacing using fragment manager*/
    public void replaceFragment(Fragment fragment) {

        try {

            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.setCustomAnimations( R.animator.slide_up, R.animator.slide_down );
            fragmentTransaction.replace(R.id.review_container, fragment);
            // fragmentTransaction.addToBackStack(null);
            fragmentTransaction.detach(fragment);
            fragmentTransaction.attach(fragment);
            fragmentTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /* Restaurant location search */
    private void locationSearch(String businessName) {

        Call<Restaurant_output_pojo> call = apiService.location_search("restaurants" + "+" + "in" + "+" + businessName, APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<Restaurant_output_pojo>() {
            @Override
            public void onResponse(Call<Restaurant_output_pojo> call, Response<Restaurant_output_pojo> response) {
                Restaurant_output_pojo restaurantOutputPojo = response.body();


                if (response.isSuccessful()) {

                    Utility.hideKeyboard(view);

                    next_token = restaurantOutputPojo.getNextPageToken();
                    Log.e("token_location", "onResponse" + next_token);

                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {


                        storeModels = new ArrayList<>();
                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();


                        Log.e("hotel_size", "" + restaurantOutputPojo.getResults().size());

                        for (int i = 0; i < restaurantOutputPojo.getResults().size(); i++) {

//                            Getall_restaurant_details_pojo info = results.get(i).getResults().get(0);


                                  /* Getall_restaurant_details_pojo getallRestaurantDetailsPojo= new  Getall_restaurant_details_pojo(geometryPojo,"","","",
                                           openingHoursPojo,photos,"",0,"","",types,"");*/

                            String placeid = restaurantOutputPojo.getResults().get(i).getPlaceId();

                            String Icon = restaurantOutputPojo.getResults().get(i).getIcon();
                            String hotel_name = restaurantOutputPojo.getResults().get(i).getName();
                            String Id = restaurantOutputPojo.getResults().get(i).getId();

                            String reference = restaurantOutputPojo.getResults().get(i).getReference();

                            String address = restaurantOutputPojo.getResults().get(i).getFormatted_address();
                            String scope = restaurantOutputPojo.getResults().get(i).getScope();

                            String rating = restaurantOutputPojo.getResults().get(i).getRating();
                            try {
                                String photo_ref = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getPhotoReference();
                                int width = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getWidth();
                                int height = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getHeight();

                                Get_hotel_photo_pojo getHotelPhotoPojo1 = new Get_hotel_photo_pojo(height, htmlAttributions, photo_ref, width);
                                Log.e("height1", "photo_ref1: " + photo_ref);
                                Log.e("height1", "width1: " + width);
                                Log.e("height1", "height1: " + height);
                                photos.add(getHotelPhotoPojo1);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            }


                            for (int kk = 0; kk < restaurantOutputPojo.getResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantOutputPojo.getResults().get(i).getTypes().toString());
                            }
                            try {
                                double lat = restaurantOutputPojo.getResults().get(0).getGeometry().getLocation().getLatitude();
                                double longit = restaurantOutputPojo.getResults().get(0).getGeometry().getLocation().getLongitude();


                                /*
                                geometryPojo = new Geometry_pojo(lat,longit);*/
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                boolean open_status = restaurantOutputPojo.getResults().get(i).getOpeningHours().getOpenNow();
                                Log.e("open_status", "open_status: " + open_status);

                                openingHoursPojo = new OpeningHours_pojo(open_status, weekdayText);
                                openingHoursPojoList.add(openingHoursPojo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            Getall_restaurant_details_pojo getallRestaurantDetailsPojo1 = new Getall_restaurant_details_pojo(geometryPojo, Icon, Id,
                                    hotel_name, openingHoursPojo, photos, placeid, rating, reference, scope, types, address);
                            getallRestaurantDetailsPojoArrayList.add(getallRestaurantDetailsPojo1);

                            RecyclerViewAdapter adapterStores = new RecyclerViewAdapter(getActivity(), getallRestaurantDetailsPojoArrayList, storeModels, openingHoursPojoList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true);
                            review_recyclerview.setLayoutManager(mLayoutManager);
                            mLayoutManager.setStackFromEnd(true);
                            mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                            review_recyclerview.setAdapter(adapterStores);
                            review_recyclerview.setItemAnimator(new DefaultItemAnimator());
                            review_recyclerview.setNestedScrollingEnabled(false);
                            review_recyclerview.setHorizontalScrollBarEnabled(true);
                            adapterStores.notifyDataSetChanged();
                            ll_view_more.setVisibility(View.VISIBLE);
                            review_recyclerview.setVisibility(View.VISIBLE);


//                            fetchDistance(getallRestaurantDetailsPojo);


                        }

                    } else {
                        Toast.makeText(context, "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else if (!response.isSuccessful()) {
                    Toast.makeText(context, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<Restaurant_output_pojo> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });


    }


    //Restaurant nextLocation search
    private void nextLocation(String next) {

        Call<Restaurant_output_pojo> call = apiService.nextlocation("restaurants" + "+" + "in" + "+" + edt_location.getText().toString().trim(), APIClient_restaurant.GOOGLE_PLACE_API_KEY, next);
        call.enqueue(new Callback<Restaurant_output_pojo>() {
            @Override
            public void onResponse(Call<Restaurant_output_pojo> call, Response<Restaurant_output_pojo> response) {
                Restaurant_output_pojo restaurantOutputPojo = response.body();


                if (response.isSuccessful()) {

                    next_token = restaurantOutputPojo.getNextPageToken();
                    Log.e("token_location", "next_location" + next_token);

                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {


                        storeModels = new ArrayList<>();
                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();

                        getallRestaurantDetailsPojoArrayList.clear();

                        Log.e("hotel_size", "" + restaurantOutputPojo.getResults().size());

                        for (int i = 0; i < restaurantOutputPojo.getResults().size(); i++) {

                            String placeid = restaurantOutputPojo.getResults().get(i).getPlaceId();

                            String icon = restaurantOutputPojo.getResults().get(i).getIcon();
                            String hotelName = restaurantOutputPojo.getResults().get(i).getName();
                            String id = restaurantOutputPojo.getResults().get(i).getId();

                            String reference = restaurantOutputPojo.getResults().get(i).getReference();

                            String address = restaurantOutputPojo.getResults().get(i).getFormatted_address();
                            String scope = restaurantOutputPojo.getResults().get(i).getScope();

                            String rating = restaurantOutputPojo.getResults().get(i).getRating();
                            try {
                                String photoRef = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getPhotoReference();
                                int width = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getWidth();
                                int height = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getHeight();

                                Get_hotel_photo_pojo getHotelPhotoPojo1 = new Get_hotel_photo_pojo(height, htmlAttributions, photoRef, width);
                                Log.e("height1", "photo_ref1: " + photoRef);
                                Log.e("height1", "width1: " + width);
                                Log.e("height1", "height1: " + height);
                                photos.add(getHotelPhotoPojo1);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            }


                            for (int kk = 0; kk < restaurantOutputPojo.getResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantOutputPojo.getResults().get(i).getTypes().toString());
                            }


                            try {
                                boolean openStatus = restaurantOutputPojo.getResults().get(i).getOpeningHours().getOpenNow();
                                Log.e("open_status", "open_status: " + openStatus);

                                openingHoursPojo = new OpeningHours_pojo(openStatus, weekdayText);
                                openingHoursPojoList.add(openingHoursPojo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            Getall_restaurant_details_pojo getallRestaurantDetailsPojo1 = new Getall_restaurant_details_pojo(geometryPojo, icon, id,
                                    hotelName, openingHoursPojo, photos, placeid, rating, reference, scope, types, address);
                            getallRestaurantDetailsPojoArrayList.add(getallRestaurantDetailsPojo1);


                            /*Setting adapter and view the restaurant details*/

                            RecyclerViewAdapter adapterStores = new RecyclerViewAdapter(getActivity(), getallRestaurantDetailsPojoArrayList, storeModels, openingHoursPojoList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true);
                            review_recyclerview.setLayoutManager(mLayoutManager);
                            mLayoutManager.setStackFromEnd(true);
                            mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                            review_recyclerview.setAdapter(adapterStores);
                            review_recyclerview.setItemAnimator(new DefaultItemAnimator());
                            review_recyclerview.setNestedScrollingEnabled(false);
                            review_recyclerview.setHorizontalScrollBarEnabled(true);
                            adapterStores.notifyDataSetChanged();


//                            fetchDistance(getallRestaurantDetailsPojo);


                        }

                    } else {
                        Toast.makeText(context, "No matches found near you!!", Toast.LENGTH_SHORT).show();
                    }

                } else if (!response.isSuccessful()) {

                    Toast.makeText(context, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<Restaurant_output_pojo> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });


    }




    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:


                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                } else {
                    fetchLocation();
                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    /*Location fetching using smart location plugin*/
    private void fetchLocation() {

        try {
            SmartLocation.with(getContext()).location()
                    .oneFix()
                    .start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            latLngString = location.getLatitude() + "," + location.getLongitude();
                            latitude_online = location.getLatitude();
                            longitude_online = location.getLongitude();

                            Log.e("newlat", "onLocationUpdated" + latLngString);
                            latLng = new LatLng(location.getLatitude(), location.getLongitude());

                            try {
                                /*Geocoder initialization*/
                                Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());

                                List<Address> addresses = geocoder.getFromLocation(latitude_online, longitude_online, 1);


                                // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                String locality = String.valueOf(addresses.get(0).getCountryName());
                                Log.e("address", "getAddressLine\n" + String.valueOf(addresses.get(0).getAddressLine(0)));
                                Log.e("address", "getAdminArea\n" + String.valueOf(addresses.get(0).getAdminArea()));
                                Log.e("address", "getSubLocality\n" + String.valueOf(addresses.get(0).getSubLocality()));
                                Log.e("address", "getLocality\n" + String.valueOf(addresses.get(0).getLocality()));
                                Log.e("address", "getFeatureName\n" + String.valueOf(addresses.get(0).getFeatureName()));
                                Log.e("address", "getSubAdminArea\n" + String.valueOf(addresses.get(0).getSubAdminArea()));
                                Log.e("address", "getSubThoroughfare\n" + String.valueOf(addresses.get(0).getSubThoroughfare()));
                                Log.e("address", "getThoroughfare\n" + String.valueOf(addresses.get(0).getThoroughfare()));

                                edt_location.setText(String.valueOf(addresses.get(0).getSubLocality()));


                            } catch (IOException e) {

                                e.printStackTrace();

                            }


                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {

            /*View more restaurants*/
            case R.id.rl_view_more:
//                nextLocation(next_token);
                break;

            case R.id.txt_view_more:
//                nextLocation(next_token);

                break;

            case R.id.img_more:
//                nextLocation(next_token);
                break;

            /*Redirectiong to Search restaurant page */
            case R.id.add_review:

                Intent intent = new Intent(context, ReviewRestaurantSearch.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                break;

            /*Redirectiong to Search restaurant page */

            case R.id.edt_location:

                startActivity(new Intent(context, ReviewRestaurantSearch.class));
                break;

        }
    }
}
