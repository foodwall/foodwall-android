package com.kaspontech.foodwall.reviewpackage.profileReviewAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.kaspontech.foodwall.profilePackage.ReviewSinglePage;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.reviewpackage.ProfileReviewPojo.GetReviewProfileImage;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.SquareImageView;

import java.util.ArrayList;

public class ProfileReviewImageUserAdapter extends RecyclerView.Adapter<ProfileReviewImageUserAdapter.MyViewHolder> {


    public Context context;
    public ArrayList<GetReviewProfileImage> allimagelist = new ArrayList<>();


    public ProfileReviewImageUserAdapter(Context context, ArrayList<GetReviewProfileImage> allimagelist) {
        this.context = context;
        this.allimagelist = allimagelist;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        SquareImageView gridImageView;

        public MyViewHolder(View view) {
            super(view);

            gridImageView = (SquareImageView) view.findViewById(R.id.gridImageView);

        }


        @Override
        public void onClick(View view) {


        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_grid_imageview, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {


        String image = allimagelist.get(position).getAmbImage();
        final String reviewId = allimagelist.get(position).getReviewid();
        final String userID = allimagelist.get(position).getUserid();


        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(30));
//        Utility.picassoImageLoader(getAllEventData.getEventImage(),
//                1,holder.eventBackground);
        GlideApp.with(context)
                .load(image)
                .apply(requestOptions)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_no_reviews)
                .thumbnail(0.1f)
                .into(holder.gridImageView);

        holder.gridImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ReviewSinglePage.class);
                intent.putExtra("reviewId", reviewId);
                intent.putExtra("userId", userID);
                context.startActivity(intent);


            }
        });

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return allimagelist.size();
    }


}