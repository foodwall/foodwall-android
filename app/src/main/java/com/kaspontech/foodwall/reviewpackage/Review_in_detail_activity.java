package com.kaspontech.foodwall.reviewpackage;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.bucketlistpackage.CreateBucketlistActivty;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo.DetailedAmbienceImagePojo;
import com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo.DetailedAvoiddishimagePojo;
import com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo.DetailedReviewPojo;
import com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo.DetailedTopdishImagePojo;
import com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo.ReviewInDetailsOutput;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.PackagingDetailsAdapter;
import com.kaspontech.foodwall.reviewpackage.ReviewsIndetailAdapter.HotelAllAmbienceAdapter;
import com.kaspontech.foodwall.reviewpackage.ReviewsIndetailAdapter.HotelAllAvoidDishAdapter;
import com.kaspontech.foodwall.reviewpackage.ReviewsIndetailAdapter.HotelAllPackagingAdapter;
import com.kaspontech.foodwall.reviewpackage.ReviewsIndetailAdapter.HotelAllTopDishAdapter;
import com.kaspontech.foodwall.reviewpackage.ReviewsIndetailAdapter.HotelReviewDescriptionAdapter;

import com.kaspontech.foodwall.reviewpackage.allTab.AllFragment;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Vishnu on 19-06-2018.
 */


public class Review_in_detail_activity extends _SwipeActivityClass implements View.OnClickListener {

    String TAG = "Review_in_detail";


    int timelineId = 0;


    ImageButton img_bucket_list;
    CoordinatorLayout co_layout;
    AppBarLayout app_bar_layout;
    CollapsingToolbarLayout hotel_collapsingtoolbar;
    ImageView img_hotel_background, img_no_worst_dish, img_no_top_dish, img_no_ambiance;
    Toolbar toolbar;
    ProgressBar view_hotel_progress;
    NestedScrollView hotel_scroll;

    TextView txt_hotel_review, txt_rating_intime, txt_rating_ambiance, txt_rating_service, txt_rating_package, txt_no_ambiance, txt_no_worst_dish, txt_no_top_dish, hotel_name, hotel_location, htl_open, hotel_rating, hotel_opened, id_txt_amb_images, id_txt_top_images, id_txt_worst_images;
    String txt_hotelname, newhotelid, food_exp, user_id, hotel_id, review_id, hotel_review, hotel_address, total_reviews, total_likes, total_comments,
            txt_review_username, txt_review_userlastname, userimage, txt_review_follow, txt_review_like, txt_review_comment,
            txt_review_shares, txt_review_caption, hotel_review_rating, hotel_user_rating,
            hotelId, cat_type, veg_nonveg, revratID, ambiance, taste, service, valuemoney,
            createdby, createdon, googleID, placeID, opentimes, photo_reference,
            category_type, latitude, longitude, phone, picture, emailID, likes_hotelID, revrathotel_likes,
            followingID, totalfoodexperience, totalreview, total_ambiance, total_value_money, total_timedelivery, total_package,
            total_taste, total_service, total_value, total_good, total_bad, total_good_bad_user,
            next_token, location_result, next_token_next, topdishsize, avoiddishsize, ambisize, topdishimage, topdishname, avoiddishname, avoiddishimage, ambienceimage;

    RatingBar rtng_hotel, rating_ambiance, rating_value_ontime, rating_value_package, rating_service, rating_taste, rating_value;
    String fname, lname, reviewid;


    int package_count, ambi_count, topdish_count, avoidcount, reviewcount, totalfoodexp, tot_review_count, total_amb, t_taste, t_service, tot_package, tot_delivery, t_package, t_timedeilvery, t_vfm;
    float flt_totalfoodexp;

    //Ambience adapter
    HotelAllAmbienceAdapter ambianceImagesAdapter;

    //packaging adpate
    HotelAllPackagingAdapter packagingDetailsAdapter;

    //DetailedAmbienceImagePojo
    DetailedAmbienceImagePojo detailedAmbienceImagePojo;
    PackImage packImage;

    ArrayList<DetailedAmbienceImagePojo> getAmbienceImageAllArrayList = new ArrayList<>();
    //packaging
    ArrayList<PackImage> packImageArrayList = new ArrayList<>();

    //Top dish Adapter

    HotelAllTopDishAdapter reviewImageTopDishAdapter;
    //Detailed top dish pojo
    DetailedTopdishImagePojo detailedTopdishImagePojo;
    ArrayList<DetailedTopdishImagePojo> reviewTopDishAdapterArrayList = new ArrayList<>();

    //Worst dish Adapter

    HotelAllAvoidDishAdapter reviewWorstDishAdapter;

    DetailedAvoiddishimagePojo detailedAvoiddishimagePojo;

    ArrayList<DetailedAvoiddishimagePojo> reviewAvoiddishimagePojoArrayList = new ArrayList<>();

    //Detailed Review Pojo

    DetailedReviewPojo detailedReviewPojo;

    ArrayList<DetailedReviewPojo> detailedReviewPojoArrayList = new ArrayList<>();
    HotelReviewDescriptionAdapter hotelReviewDescriptionAdapter;


    ExpandableTextView hotel_review_description;
    boolean open;
    String photoid, userid, dish_img, images, photoId;

    private ViewPager ambi_viewpager, topdish_viewpager, worstdish_viewpager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    RecyclerView rv_hotelallpackaging, rv_top_dishes_all, rv_worst_dishes_all, rv_hotelallambience, rv_hotel_all_reviews;

    LinearLayout ll_no_reviews;
    RelativeLayout rl_review_layout, rl_review, rl_ambiance_view, rl_ambiance_dot, rl_topdish_view, rl_topdish_dot, rl_worstdish_view, rl_worstdish_dot;

    private CircleIndicator ambi_indicator, worstdish_indicator, topdish_indicator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review_in_detail_layout);
        co_layout = (CoordinatorLayout) findViewById(R.id.co_layout);


        img_hotel_background = (ImageView) findViewById(R.id.img_hotel_background);
        img_no_top_dish = (ImageView) findViewById(R.id.img_no_top_dish);
        img_no_worst_dish = (ImageView) findViewById(R.id.img_no_worst_dish);
        img_no_ambiance = (ImageView) findViewById(R.id.img_no_ambiance);
        img_bucket_list = (ImageButton) findViewById(R.id.img_bucket_list);


        view_hotel_progress = (ProgressBar) findViewById(R.id.view_hotel_progress);
        hotel_scroll = (NestedScrollView) findViewById(R.id.hotel_scroll);

        rl_ambiance_view = (RelativeLayout) findViewById(R.id.rl_ambiance_view);
        rl_ambiance_dot = (RelativeLayout) findViewById(R.id.rl_ambiance_dot);
        rl_topdish_view = (RelativeLayout) findViewById(R.id.rl_topdish_view);
        rl_topdish_dot = (RelativeLayout) findViewById(R.id.rl_topdish_dot);
        rl_worstdish_view = (RelativeLayout) findViewById(R.id.rl_worstdish_view);
        rl_worstdish_dot = (RelativeLayout) findViewById(R.id.rl_worstdish_dot);
        rl_review = (RelativeLayout) findViewById(R.id.rl_review);
        rl_review_layout = (RelativeLayout) findViewById(R.id.rl_review_layout);
        ll_no_reviews = (LinearLayout) findViewById(R.id.ll_no_reviews);
        rl_review.setOnClickListener(this);
        img_bucket_list.setOnClickListener(this);

        rv_hotelallpackaging = (RecyclerView) findViewById(R.id.rv_hotelallpackaging);
        rv_top_dishes_all = (RecyclerView) findViewById(R.id.rv_top_dishes_all);
        rv_worst_dishes_all = (RecyclerView) findViewById(R.id.rv_worst_dishes_all);
        rv_hotelallambience = (RecyclerView) findViewById(R.id.rv_hotelallambience);
        rv_hotel_all_reviews = (RecyclerView) findViewById(R.id.rv_hotel_all_reviews);

        hotel_name = (TextView) findViewById(R.id.hotel_name);

        txt_no_top_dish = (TextView) findViewById(R.id.txt_no_top_dish);
        txt_no_worst_dish = (TextView) findViewById(R.id.txt_no_worst_dish);
        txt_no_ambiance = (TextView) findViewById(R.id.txt_no_ambiance);
        txt_hotel_review = (TextView) findViewById(R.id.txt_hotel_review);
        txt_rating_intime = (TextView) findViewById(R.id.txt_rating_intime);
        txt_rating_package = (TextView) findViewById(R.id.txt_rating_package);
        txt_rating_ambiance = (TextView) findViewById(R.id.txt_rating_ambiance);
        txt_rating_service = (TextView) findViewById(R.id.txt_rating_service);

        htl_open = (TextView) findViewById(R.id.htl_open);
        hotel_opened = (TextView) findViewById(R.id.hotel_opened);
        hotel_rating = (TextView) findViewById(R.id.hotel_rating);
        hotel_location = (TextView) findViewById(R.id.hotel_location);
        id_txt_amb_images = (TextView) findViewById(R.id.id_txt_amb_images);
        id_txt_worst_images = (TextView) findViewById(R.id.id_txt_worst_images);
        id_txt_top_images = (TextView) findViewById(R.id.id_txt_top_images);


        hotel_review_description = (ExpandableTextView) findViewById(R.id.hotel_review_description);


        rtng_hotel = (RatingBar) findViewById(R.id.rtng_hotel);
        rating_taste = (RatingBar) findViewById(R.id.rating_taste);
        rating_value = (RatingBar) findViewById(R.id.rating_value);
        rating_ambiance = (RatingBar) findViewById(R.id.rating_ambiance);
        rating_service = (RatingBar) findViewById(R.id.rating_service);
        rating_value_package = (RatingBar) findViewById(R.id.rating_value_package);
        rating_value_ontime = (RatingBar) findViewById(R.id.rating_value_ontime);

        //circle indicator
        ambi_viewpager = (ViewPager) findViewById(R.id.ambi_viewpager);
        topdish_viewpager = (ViewPager) findViewById(R.id.topdish_viewpager);
        ambi_indicator = (CircleIndicator) findViewById(R.id.ambi_indicator);
        topdish_indicator = (CircleIndicator) findViewById(R.id.topdish_indicator);
        worstdish_indicator = (CircleIndicator) findViewById(R.id.worstdish_indicator);

        //Toolar

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        //Collapsing tool bar layout
        hotel_collapsingtoolbar = (CollapsingToolbarLayout) findViewById(R.id.hotel_collapsingtoolbar);
        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);


        Intent intent = getIntent();
        hotel_id = intent.getStringExtra("hotelid");
        fname = intent.getStringExtra("f_name");
        lname = intent.getStringExtra("l_name");

        reviewid = intent.getStringExtra("reviewid");
        try {
            timelineId = Integer.parseInt(intent.getStringExtra("timelineId"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        app_bar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    hotel_collapsingtoolbar.setTitle(txt_hotelname);

                    isShow = true;
                } else if (isShow) {
                    hotel_collapsingtoolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        //All hotel details in single API
        get_hotel_all_details();


    }

    @Override
    public void onSwipeRight() {
        finish();
    }

    @Override
    protected void onSwipeLeft() {

    }


    //All hotel details in single API

    private void get_hotel_all_details() {

        if (isConnected(Review_in_detail_activity.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(Review_in_detail_activity.this, "userId");

            Call<ReviewInDetailsOutput> call = apiService.get_hotel_all("get_hotel_all", Integer.parseInt(hotel_id), Integer.parseInt(userId));
            call.enqueue(new Callback<ReviewInDetailsOutput>() {
                @Override
                public void onResponse(Call<ReviewInDetailsOutput> call, Response<ReviewInDetailsOutput> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {

                        for (int j = 0; j < response.body().getData().size(); j++) {


                            hotel_id = response.body().getData().get(j).getHotelId();
                            newhotelid = response.body().getData().get(j).getHotelId();
                            googleID = response.body().getData().get(j).getGoogleId();
                            googleID = response.body().getData().get(j).getGoogleId();
                            txt_hotelname = response.body().getData().get(j).getHotelName();
                            placeID = response.body().getData().get(j).getPlaceId();
                            hotel_address = response.body().getData().get(j).getAddress();
                            latitude = response.body().getData().get(j).getLatitude();
                            longitude = response.body().getData().get(j).getLongitude();
                            photo_reference = response.body().getData().get(j).getPhotoReference();
                            totalreview = response.body().getData().get(j).getTotalReview();
                            totalfoodexperience = response.body().getData().get(j).getTotalFoodExprience();
                            total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                            total_taste = response.body().getData().get(j).getTotalTaste();
                            total_service = response.body().getData().get(j).getTotalService();
                            total_package = response.body().getData().get(j).getTotalPackage();
                            total_timedelivery = response.body().getData().get(j).getTotalTimedelivery();
                            total_value_money = response.body().getData().get(j).getTotalValueMoney();
                            total_good = response.body().getData().get(j).getTotalGood();
                            total_bad = response.body().getData().get(j).getTotalBad();
                            total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();
                            ambi_count = response.body().getData().get(j).getAmbiImageCount();
                            package_count = response.body().getData().get(j).getPackImageCount();

                            topdishsize = response.body().getData().get(j).getTopdish();
                            avoiddishsize = response.body().getData().get(j).getAvoiddish();
                            reviewcount = response.body().getData().get(j).getReviewCount();

                            hotel_name.setText(txt_hotelname);
                            hotel_location.setText(hotel_address);


                            if (response.body().getData().get(j).getBucket_list() != null) {

                                if (response.body().getData().get(j).getBucket_list().equalsIgnoreCase("0")) {

                                    img_bucket_list.setImageResource(R.drawable.ic_bucket);

                                } else {

                                    img_bucket_list.setImageResource(R.drawable.ic_bucket_neww);
                                }
                            }

                            if (ambi_count != 0) {


                                GlideApp.with(getApplicationContext()).load(response.body().getData().get(j).
                                        getAmbiImage().get(j).getImg()).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).thumbnail(0.3f).into(img_hotel_background);

                            } else {

                                Image_load_activity.loadGooglePhoto(getApplicationContext(), img_hotel_background, photo_reference);

                            }


                            try {


                                if (response.body().getData().get(j).getAmbiImageCount() != 0) {

                                    if (response.body().getData().get(j).getAmbiImage().size() > 0) {

                                        for (int k = 0; k < response.body().getData().get(j).getAmbiImage().size(); k++) {

                                            ambienceimage = response.body().getData().get(j).getAmbiImage().get(k).getImg();
                                            Log.e("ambienceimage-->", "" + ambienceimage);

                                            detailedAmbienceImagePojo = new DetailedAmbienceImagePojo(ambienceimage);
                                            getAmbienceImageAllArrayList.add(detailedAmbienceImagePojo);
                                        }
                                    } else {

                                        img_no_ambiance.setVisibility(View.GONE);
                                        txt_no_ambiance.setVisibility(View.GONE);
                                    }
                                }

                                if (response.body().getData().get(j).getPackImageCount() != 0) {

                                    if (response.body().getData().get(j).getPackImage().size() > 0) {

                                        for (int k = 0; k < response.body().getData().get(j).getPackImage().size(); k++) {

                                            String packimg = response.body().getData().get(j).getPackImage().get(k).getImg();
                                            Log.e("ambienceimage-->", "" + ambienceimage);

                                            packImage = new PackImage(packimg);
                                            packImageArrayList.add(packImage);
                                        }

                                    } else {

                                        img_no_ambiance.setVisibility(View.GONE);
                                        txt_no_ambiance.setVisibility(View.GONE);
                                    }
                                }


                                //top dishes
                                if (topdishsize.trim().equalsIgnoreCase("") || topdishsize == null) {

                                    img_no_top_dish.setVisibility(View.VISIBLE);
                                    txt_no_top_dish.setVisibility(View.VISIBLE);


                                } else {

                                    for (int kk = 0; kk < response.body().getData().get(j).getTopdishimage().size(); kk++) {
                                        topdishimage = response.body().getData().get(j).getTopdishimage().get(kk).getImg();
                                        topdishname = response.body().getData().get(j).getTopdishimage().get(kk).getDishname();

                                        if (topdishimage.trim().equalsIgnoreCase("") || topdishimage == null) {

                                        } else {

                                            detailedTopdishImagePojo = new DetailedTopdishImagePojo(topdishname, topdishimage);
                                            reviewTopDishAdapterArrayList.add(detailedTopdishImagePojo);
                                        }

                                    }
                                }

                                //dishes to avoid
                                if (avoiddishsize.trim().equalsIgnoreCase("") || avoiddishsize == null) {
                                    img_no_worst_dish.setVisibility(View.VISIBLE);
                                    txt_no_worst_dish.setVisibility(View.VISIBLE);

                                } else {

                                    for (int kl = 0; kl < response.body().getData().get(j).getAvoiddishimage().size(); kl++) {

                                        avoiddishimage = response.body().getData().get(j).getAvoiddishimage().get(kl).getImg();
                                        avoiddishname = response.body().getData().get(j).getAvoiddishimage().get(kl).getDishname();

                                        if (avoiddishimage.trim().equalsIgnoreCase("") || avoiddishimage == null) {

                                        } else {
                                            detailedAvoiddishimagePojo = new DetailedAvoiddishimagePojo(avoiddishname, avoiddishimage);
                                            reviewAvoiddishimagePojoArrayList.add(detailedAvoiddishimagePojo);
                                        }

                                    }

                                }

                                if (reviewcount != 0) {

                                    ll_no_reviews.setVisibility(View.GONE);
                                    rl_review_layout.setVisibility(View.VISIBLE);


                                    for (int aa = 0; aa < response.body().getData().get(j).getReview().size(); aa++) {

                                        String reviewuser_fname = response.body().getData().get(j).getReview().get(0).getFirstName();
                                        String reviewhotel_id = response.body().getData().get(j).getReview().get(0).getHotelId();
                                        String rev_id = response.body().getData().get(j).getReview().get(0).getRevratId();
                                        String rev_user_id = response.body().getData().get(j).getReview().get(0).getUserId();
                                        String reviewuser_lname = response.body().getData().get(j).getReview().get(0).getLastName();
                                        String hotel_review = response.body().getData().get(j).getReview().get(0).getHotelReview();
                                        String hotel_name = response.body().getData().get(j).getReview().get(0).getHotelName();
                                        String review_user_pic = response.body().getData().get(j).getReview().get(0).getPicture();
                                        String createdon = response.body().getData().get(j).getReview().get(0).getCreatedOn();
                                        String rev_totalfollowers = response.body().getData().get(j).getReview().get(0).getTotalFollowers();


                                        detailedReviewPojo = new DetailedReviewPojo(reviewhotel_id, rev_id, hotel_review,
                                                "", "", "", "", "", "", "",
                                                "", "", "", "", "", createdon, "",
                                                hotel_name, "", "", "", "", "", "",
                                                "", rev_user_id, "", "", reviewuser_fname, reviewuser_lname, "",
                                                "", "", "", "", rev_totalfollowers, "", review_user_pic, "",
                                                "");

                                        detailedReviewPojoArrayList.add(detailedReviewPojo);

                                    }


                                    txt_hotel_review.setText("All Reviews".concat("(").concat(String.valueOf(reviewcount)).concat(")"));

                                    //top dish adapter
                                    if (topdishsize.trim().equalsIgnoreCase("") || topdishsize == null) {
                                        img_no_top_dish.setVisibility(View.VISIBLE);
                                        txt_no_top_dish.setVisibility(View.VISIBLE);
                                    } else {
                                        reviewImageTopDishAdapter = new HotelAllTopDishAdapter(Review_in_detail_activity.this, reviewTopDishAdapterArrayList, detailedReviewPojoArrayList);
                                        LinearLayoutManager llm = new LinearLayoutManager(Review_in_detail_activity.this, LinearLayoutManager.HORIZONTAL, false);
                                        rv_top_dishes_all.setLayoutManager(llm);
                                        rv_top_dishes_all.setItemAnimator(new DefaultItemAnimator());
                                        rv_top_dishes_all.setAdapter(reviewImageTopDishAdapter);
                                        rv_top_dishes_all.setNestedScrollingEnabled(false);
                                        reviewImageTopDishAdapter.notifyDataSetChanged();
                                    }


                                    //Worst dish adapter
                                    if (avoiddishsize.trim().equalsIgnoreCase("") || avoiddishsize == null) {
                                        img_no_worst_dish.setVisibility(View.VISIBLE);
                                        txt_no_worst_dish.setVisibility(View.VISIBLE);

                                    } else {
                                        reviewWorstDishAdapter = new HotelAllAvoidDishAdapter(getApplicationContext(), reviewAvoiddishimagePojoArrayList, detailedReviewPojoArrayList);
                                        LinearLayoutManager llm2 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                        rv_worst_dishes_all.setLayoutManager(llm2);
                                        rv_worst_dishes_all.setItemAnimator(new DefaultItemAnimator());
                                        rv_worst_dishes_all.setAdapter(reviewWorstDishAdapter);
                                        reviewWorstDishAdapter.notifyDataSetChanged();
                                    }

                                    //Ambience adapter
                                    if (ambi_count == 0) {

                                      /*  if (getAmbienceImageAllArrayList.size() == 0) {

                                            img_no_ambiance.setVisibility(VISIBLE);
                                            txt_no_ambiance.setVisibility(View.VISIBLE);


                                        } else {

                                            img_no_ambiance.setVisibility(View.GONE);
                                            txt_no_ambiance.setVisibility(View.GONE);
                                        }*/


                                    } else {

                                        rv_hotelallambience.setVisibility(VISIBLE);

                                        img_no_ambiance.setVisibility(View.GONE);
                                        txt_no_ambiance.setVisibility(View.GONE);
                                        rv_hotelallpackaging.setVisibility(View.GONE);

                                        ambianceImagesAdapter = new HotelAllAmbienceAdapter(getApplicationContext(), getAmbienceImageAllArrayList, detailedReviewPojoArrayList);
                                        LinearLayoutManager llm4 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                        rv_hotelallambience.setLayoutManager(llm4);
                                        rv_hotelallambience.setItemAnimator(new DefaultItemAnimator());
                                        rv_hotelallambience.setAdapter(ambianceImagesAdapter);
                                        ambianceImagesAdapter.notifyDataSetChanged();
                                    }


                                    if (package_count == 0) {

                                        /*if (packImageArrayList.size() == 0) {

                                            img_no_ambiance.setVisibility(View.VISIBLE);
                                            txt_no_ambiance.setVisibility(View.VISIBLE);

                                        } else {

                                            img_no_ambiance.setVisibility(View.GONE);
                                            txt_no_ambiance.setVisibility(View.GONE);

                                        }*/

                                    } else {

                                        img_no_ambiance.setVisibility(View.GONE);
                                        txt_no_ambiance.setVisibility(View.GONE);

                                        rv_hotelallambience.setVisibility(GONE);
                                        id_txt_amb_images.setText("Packaging : ");
                                        packagingDetailsAdapter = new HotelAllPackagingAdapter(getApplicationContext(), packImageArrayList, detailedReviewPojoArrayList);
                                        LinearLayoutManager llm4 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                        rv_hotelallpackaging.setLayoutManager(llm4);
                                        rv_hotelallpackaging.setItemAnimator(new DefaultItemAnimator());
                                        rv_hotelallpackaging.setAdapter(packagingDetailsAdapter);
                                        packagingDetailsAdapter.notifyDataSetChanged();
                                    }


                                    totalfoodexp = Integer.parseInt(totalfoodexperience);
                                    tot_review_count = Integer.parseInt(totalreview);
                                    flt_totalfoodexp = totalfoodexp / tot_review_count;
                                    int foodexpp = totalfoodexp / tot_review_count;

                                    //Total food experience
                                    if (foodexpp == 1) {
                                        rtng_hotel.setRating(1f);
                                        rtng_hotel.setIsIndicator(true);
                                        rtng_hotel.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.red)));

                                    } else if (foodexpp == 2) {
                                        rtng_hotel.setRating(2f);
                                        rtng_hotel.setIsIndicator(true);
                                        rtng_hotel.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.mild_orange)));

                                    } else if (foodexpp == 3) {
                                        rtng_hotel.setRating(3f);
                                        rtng_hotel.setIsIndicator(true);
                                        rtng_hotel.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.orange)));

                                    } else if (foodexpp == 4) {
                                        rtng_hotel.setRating(4f);
                                        rtng_hotel.setIsIndicator(true);
                                        rtng_hotel.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.light_green)));


                                    } else if (foodexpp == 5) {
                                        rtng_hotel.setRating(5f);
                                        rtng_hotel.setIsIndicator(true);
                                        rtng_hotel.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.dark_green)));
                                    }


                                    //Total service

                                    if (total_service.equalsIgnoreCase("0")) {
                                        rating_service.setVisibility(View.INVISIBLE);
                                        txt_rating_service.setVisibility(View.INVISIBLE);
                                    } else {
                                        t_service = Integer.parseInt(total_service);
                                        int tot_serv = t_service / tot_review_count;


                                        if (tot_serv == 1) {
                                            rating_service.setRating(1f);
                                            rating_service.setIsIndicator(true);
                                            rating_service.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.red)));

                                        } else if (tot_serv == 2) {
                                            rating_service.setRating(2f);
                                            rating_service.setIsIndicator(true);
                                            rating_service.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.mild_orange)));

                                        } else if (tot_serv == 3) {
                                            rating_service.setRating(3f);
                                            rating_service.setIsIndicator(true);
                                            rating_service.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.orange)));

                                        } else if (tot_serv == 4) {
                                            rating_service.setRating(4f);
                                            rating_service.setIsIndicator(true);
                                            rating_service.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.light_green)));

                                        } else if (tot_serv == 5) {
                                            rating_service.setRating(5f);
                                            rating_service.setIsIndicator(true);
                                            rating_service.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.dark_green)));

                                        }
                                    }

                                    //Total ambience
                                    if (total_ambiance.equalsIgnoreCase("0")) {
                                        rating_ambiance.setVisibility(View.INVISIBLE);
                                        txt_rating_ambiance.setVisibility(View.INVISIBLE);

                                    } else {
                                        total_amb = Integer.parseInt(total_ambiance);
                                        int tot_ambii = total_amb / tot_review_count;


                                        if (tot_ambii == 1) {
                                            rating_ambiance.setRating(1f);
                                            rating_ambiance.setIsIndicator(true);
                                            rating_ambiance.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.red)));

                                        } else if (tot_ambii == 2) {
                                            rating_ambiance.setRating(2f);
                                            rating_ambiance.setIsIndicator(true);
                                            rating_ambiance.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.mild_orange)));

                                        } else if (tot_ambii == 3) {
                                            rating_ambiance.setRating(3f);
                                            rating_ambiance.setIsIndicator(true);
                                            rating_ambiance.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.orange)));

                                        } else if (tot_ambii == 4) {
                                            rating_ambiance.setRating(4f);
                                            rating_ambiance.setIsIndicator(true);
                                            rating_ambiance.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.light_green)));


                                        } else if (tot_ambii == 5) {
                                            rating_ambiance.setRating(5f);
                                            rating_ambiance.setIsIndicator(true);
                                            rating_ambiance.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.dark_green)));

                                        }
                                    }


                                    //Total taste
                                    t_taste = Integer.parseInt(total_taste);
                                    int tot_taste = t_taste / tot_review_count;


                                    if (tot_taste == 1) {
                                        rating_taste.setRating(1f);
                                        rating_taste.setIsIndicator(true);
                                        rating_taste.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.red)));

                                    } else if (tot_taste == 2) {
                                        rating_taste.setRating(2f);
                                        rating_taste.setIsIndicator(true);
                                        rating_taste.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.mild_orange)));

                                    } else if (tot_taste == 3) {
                                        rating_taste.setRating(3f);
                                        rating_taste.setIsIndicator(true);
                                        rating_taste.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.orange)));

                                    } else if (tot_taste == 4) {
                                        rating_taste.setRating(4f);
                                        rating_taste.setIsIndicator(true);
                                        rating_taste.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.light_green)));


                                    } else if (tot_taste == 5) {
                                        rating_taste.setRating(5f);
                                        rating_taste.setIsIndicator(true);
                                        rating_taste.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.dark_green)));

                                    }

                                    //Total value for money

                                    t_vfm = Integer.parseInt(total_value_money);
                                    int tot_vfm = t_vfm / tot_review_count;

                                    if (tot_vfm == 1) {
                                        rating_value.setRating(1f);
                                        rating_value.setIsIndicator(true);
                                        rating_value.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.red)));

                                    } else if (tot_vfm == 2) {
                                        rating_value.setRating(2f);
                                        rating_value.setIsIndicator(true);
                                        rating_value.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.mild_orange)));

                                    } else if (tot_vfm == 3) {
                                        rating_value.setRating(3f);
                                        rating_value.setIsIndicator(true);
                                        rating_value.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.orange)));

                                    } else if (tot_vfm == 4) {
                                        rating_value.setRating(4f);
                                        rating_value.setIsIndicator(true);
                                        rating_value.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.light_green)));


                                    } else if (tot_vfm == 5) {
                                        rating_value.setRating(5f);
                                        rating_value.setIsIndicator(true);
                                        rating_value.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.dark_green)));

                                    }

                                    //Total Pakage
                                    if (total_package.equalsIgnoreCase("0")) {
                                        rating_value_package.setVisibility(View.GONE);
                                        txt_rating_package.setVisibility(View.GONE);
                                    } else {
                                        t_package = Integer.parseInt(total_package);
                                        tot_package = t_package / tot_review_count;
                                    }


                                    if (tot_package == 1) {
                                        rating_value_package.setRating(1f);
                                        rating_value_package.setIsIndicator(true);
                                        rating_value_package.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.red)));

                                    } else if (tot_package == 2) {
                                        rating_value_package.setRating(2f);
                                        rating_value_package.setIsIndicator(true);
                                        rating_value_package.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.mild_orange)));

                                    } else if (tot_package == 3) {
                                        rating_value_package.setRating(3f);
                                        rating_value_package.setIsIndicator(true);
                                        rating_value_package.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.orange)));

                                    } else if (tot_package == 4) {
                                        rating_value_package.setRating(4f);
                                        rating_value_package.setIsIndicator(true);
                                        rating_value_package.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.light_green)));


                                    } else if (tot_package == 5) {
                                        rating_value_package.setRating(5f);
                                        rating_value_package.setIsIndicator(true);
                                        rating_value_package.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.dark_green)));

                                    }

                                    //Total OnTimeDelivery

                                    if (total_timedelivery.equalsIgnoreCase("0")) {
                                        rating_value_ontime.setVisibility(View.GONE);
                                        txt_rating_intime.setVisibility(View.GONE);

                                    } else {
                                        t_timedeilvery = Integer.parseInt(total_timedelivery);
                                        tot_delivery = t_timedeilvery / tot_review_count;
                                    }


                                    if (tot_delivery == 1) {
                                        rating_value_ontime.setRating(1f);
                                        rating_value_ontime.setIsIndicator(true);
                                        rating_value_ontime.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.red)));

                                    } else if (tot_delivery == 2) {
                                        rating_value_ontime.setRating(2f);
                                        rating_value_ontime.setIsIndicator(true);
                                        rating_value_ontime.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.mild_orange)));

                                    } else if (tot_delivery == 3) {
                                        rating_value_ontime.setRating(3f);
                                        rating_value_ontime.setIsIndicator(true);
                                        rating_value_ontime.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.orange)));

                                    } else if (tot_delivery == 4) {
                                        rating_value_ontime.setRating(4f);
                                        rating_value_ontime.setIsIndicator(true);
                                        rating_value_ontime.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.light_green)));


                                    } else if (tot_delivery == 5) {
                                        rating_value_ontime.setRating(5f);
                                        rating_value_ontime.setIsIndicator(true);
                                        rating_value_ontime.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(Review_in_detail_activity.this, R.color.dark_green)));

                                    }


                                } else {


                                    ll_no_reviews.setVisibility(View.VISIBLE);
                                    rl_review_layout.setVisibility(View.GONE);
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (response.body().getResponseMessage().equalsIgnoreCase("nodata")) {


                    }

                }

                @Override
                public void onFailure(Call<ReviewInDetailsOutput> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                /*txt_no_top_dish.setVisibility(View.VISIBLE);
                img_no_top_dish.setVisibility(View.VISIBLE);*/
                }
            });
        } else {

            Snackbar snackbar = Snackbar.make(co_layout, "Something went wrong. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v) {
        int click = v.getId();
        switch (click) {


            case R.id.rl_review:

                Intent intent1 = new Intent(Review_in_detail_activity.this, ReviewInsideHotelDetails.class);
                intent1.putExtra("hotelid", newhotelid);
                intent1.putExtra("hotelname", txt_hotelname);
                startActivity(intent1);
                break;

            case R.id.img_bucket_list:

                Intent intent = new Intent(Review_in_detail_activity.this, CreateBucketlistActivty.class);
                intent.putExtra("f_name", fname);
                intent.putExtra("l_name", lname);
                intent.putExtra("reviewid", reviewid);
                intent.putExtra("hotelid", hotel_id);
                intent.putExtra("timelineId", timelineId);
                startActivity(intent);
                break;

        }

    }
}
