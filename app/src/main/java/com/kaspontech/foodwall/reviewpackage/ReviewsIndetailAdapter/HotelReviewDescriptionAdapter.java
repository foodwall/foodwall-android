package com.kaspontech.foodwall.reviewpackage.ReviewsIndetailAdapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.reviewpackage.ReviewInDetailsPojo.DetailedReviewPojo;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HotelReviewDescriptionAdapter extends RecyclerView.Adapter<HotelReviewDescriptionAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;


    private ArrayList<DetailedReviewPojo> detailedReviewPojoArrayList = new ArrayList<>();

    private DetailedReviewPojo detailedReviewPojo;



    private Pref_storage pref_storage;


    public HotelReviewDescriptionAdapter(Context c, ArrayList<DetailedReviewPojo> gettingreviewlist) {
        this.context = c;

        this.detailedReviewPojoArrayList = gettingreviewlist;

    }


    @NonNull
    @Override
    public HotelReviewDescriptionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_description_adapter_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        HotelReviewDescriptionAdapter.MyViewHolder vh = new HotelReviewDescriptionAdapter.MyViewHolder(v);

        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final HotelReviewDescriptionAdapter.MyViewHolder holder, final int position) {


        detailedReviewPojo = detailedReviewPojoArrayList.get(position);


        //Reviewer image loading
//
//        Utility.picassoImageLoader(detailedReviewPojoArrayList.get(position).getPicture(),
//                0,holder.image_reviewer, context);
        GlideApp.with(context)
                .load(detailedReviewPojoArrayList.get(position).getPicture())
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(holder.image_reviewer);


        //Review user name

        String username= detailedReviewPojoArrayList.get(position).getFirstName().concat(" ").concat(detailedReviewPojoArrayList.get(position).getLastName());

        holder.txt_username.setText(username);

        String ago= detailedReviewPojoArrayList.get(position).getCreatedOn();
        try {

            long now = System.currentTimeMillis();
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convertedDate = dateFormat.parse(ago);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now,
                    DateUtils.FORMAT_ABBREV_RELATIVE);
            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                holder.txt_fullname_user.setText(R.string.just_now);
            }/* else if (relavetime1.toString().contains("hours ago")) {
                holder.txt_fullname_user.setText(relavetime1.toString().replace("hours ago", "").concat("h"));
            } else if (relavetime1.toString().contains("days ago")) {
                holder.txt_fullname_user.setText(relavetime1.toString().replace("days ago", "").concat("dialog"));
            }*/ else {
                holder.txt_fullname_user.setText(relavetime1.toString());
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Reviews, follower data loading
/*

        String total_follower= detailedReviewPojoArrayList.get(position).getTotalFollowers();
        if(Integer.parseInt(total_follower)==0){
            holder.txt_fullname_user.setText(" ");
        }else if(Integer.parseInt(total_follower)==1){
            holder.txt_fullname_user.setText(total_follower.concat(" ").concat(" Follower"));
        }else if(Integer.parseInt(total_follower)>1){
            holder.txt_fullname_user.setText(total_follower.concat(" ").concat(" Followers"));
        }
*/

        //Hotel review description loading

        String hotel_review= detailedReviewPojoArrayList.get(position).getHotelReview();
        if(hotel_review.equalsIgnoreCase("")||hotel_review.equalsIgnoreCase("0")){
            holder.hotel_review_description.setText(" ");
        }else {
            holder.hotel_review_description.setText(hotel_review);
        }

        holder.image_reviewer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detailedReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("userid", detailedReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);

                }
            }
        });

        holder.txt_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detailedReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("userid", detailedReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);

                }
            }
        });

        holder.txt_fullname_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detailedReviewPojoArrayList.get(position).getUserId().equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                    context.startActivity(new Intent(context, Profile.class));
                } else {

                    Intent intent = new Intent(context, User_profile_Activity.class);
                    intent.putExtra("userid", detailedReviewPojoArrayList.get(position).getUserId());
                    context.startActivity(intent);

                }
            }
        });



    }


    @Override
    public int getItemCount() {
        return detailedReviewPojoArrayList.size();

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //TextView

        TextView txt_username,txt_fullname_user;

        //Imageview
        ImageView image_reviewer;

        //Expandable textview
         ExpandableTextView hotel_review_description;
        RelativeLayout rl_rev_desc_layout;



        public MyViewHolder(View itemView) {
            super(itemView);

            txt_username = (TextView) itemView.findViewById(R.id.txt_username);

            txt_fullname_user = (TextView) itemView.findViewById(R.id.txt_fullname_user);

            image_reviewer = (ImageView) itemView.findViewById(R.id.image_reviewer);
            hotel_review_description= (ExpandableTextView) itemView.findViewById(R.id.hotel_review_description);

            rl_rev_desc_layout = (RelativeLayout) itemView.findViewById(R.id.rl_rev_desc_layout);

        }


    }


}
