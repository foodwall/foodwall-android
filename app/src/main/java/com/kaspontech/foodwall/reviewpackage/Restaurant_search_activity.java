package com.kaspontech.foodwall.reviewpackage;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.model.LatLng;
import com.kaspontech.foodwall.modelclasses.Review_pojo.PlacesPOJO;
import com.kaspontech.foodwall.modelclasses.Review_pojo.StoreModel;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.REST_API.API_interface_restaurant;

import java.util.ArrayList;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class Restaurant_search_activity extends AppCompatActivity {


    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private final static int ALL_PERMISSIONS_RESULT = 101;
    List<StoreModel> storeModels;
    API_interface_restaurant apiService;

    String latLngString;
    LatLng latLng;

    RecyclerView recyclerView;
    EditText editText;
    Button button;
    List<PlacesPOJO.CustomA> results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.places_search_layout);

        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            else {
                fetchLocation();
            }
        } else {
            fetchLocation();
        }


        apiService = APIClient_restaurant.getClient().create(API_interface_restaurant.class);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);


        editText = (EditText) findViewById(R.id.editText);

        button = (Button) findViewById(R.id.button);


//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String s = editText.getText().toString().trim();
//                String[] split = s.split("\\s+");
//
//
//                if (split.length != 2) {
//                    Toast.makeText(getApplicationContext(), "Please enter text in the required format", Toast.LENGTH_SHORT).show();
//                } else
//                    fetchStores(split[0], split[1]);
//            }
//        });

    }

//    private void fetchStores(String placeType, String businessName) {
//
//        /**
//         * For Locations In India McDonalds stores aren't returned accurately
//         */
//
//        //Call<PlacesPOJO.Root> call = apiService.doPlaces(placeType, latLngString,"\""+ businessName +"\"", true, "distance", APIClient.GOOGLE_PLACE_API_KEY);
//
//        Call<PlacesPOJO.Root> call = apiService.doPlaces(placeType, latLngString, businessName, true, "distance", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
//        call.enqueue(new Callback<PlacesPOJO.Root>() {
//            @Override
//            public void onResponse(Call<PlacesPOJO.Root> call, Response<PlacesPOJO.Root> response) {
//                PlacesPOJO.Root root = response.body();
//
//
//                if (response.isSuccessful()) {
//
//                    if (root.status.equals("OK")) {
//
//                        results = root.customA;
//                        storeModels = new ArrayList<>();
//                        for (int i = 0; i < results.size(); i++) {
//
//                            if (i == 10)
//                                break;
//                            PlacesPOJO.CustomA info = results.get(i);
//                            Log.e("results", "" + results.get(i));
//                            Log.e("results", "" + info);
//                            fetchDistance(info);
//
//                        }
//
//                    } else {
//                        Toast.makeText(getApplicationContext(), "No matches found near you", Toast.LENGTH_SHORT).show();
//                    }
//
//                } else if (response.code() != 200) {
//                    Toast.makeText(getApplicationContext(), "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<PlacesPOJO.Root> call, Throwable t) {
//                // Log error here since request failed
//                call.cancel();
//            }
//        });
//
//
//    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                } else {
                    fetchLocation();
                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(Restaurant_search_activity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void fetchLocation() {

        SmartLocation.with(this).location()
                .oneFix()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        latLngString = location.getLatitude() + "," + location.getLongitude();
                        latLng = new LatLng(location.getLatitude(), location.getLongitude());

                    }
                });
    }

//    private void fetchDistance(final PlacesPOJO.CustomA info) {
//
//        Call<ResultDistanceMatrix> call = apiService.getDistance(APIClient_restaurant.GOOGLE_PLACE_API_KEY, latLngString, info.geometry.locationA.lat + "," + info.geometry.locationA.lng);
//        call.enqueue(new Callback<ResultDistanceMatrix>() {
//            @Override
//            public void onResponse(Call<ResultDistanceMatrix> call, Response<ResultDistanceMatrix> response) {
//
//                ResultDistanceMatrix resultDistance = response.body();
//                if ("OK".equalsIgnoreCase(resultDistance.status)) {
//
//                    ResultDistanceMatrix.InfoDistanceMatrix infoDistanceMatrix = resultDistance.rows.get(0);
//                    ResultDistanceMatrix.InfoDistanceMatrix.DistanceElement distanceElement = infoDistanceMatrix.elements.get(0);
//                    if ("OK".equalsIgnoreCase(distanceElement.status)) {
//                        ResultDistanceMatrix.InfoDistanceMatrix.ValueItem itemDuration = distanceElement.duration;
//                        ResultDistanceMatrix.InfoDistanceMatrix.ValueItem itemDistance = distanceElement.distance;
//                        String totalDistance = String.valueOf(itemDistance.text);
//                        String totalDuration = String.valueOf(itemDuration.text);
//                        storeModels.add(new StoreModel(info.name, info.vicinity, totalDistance, totalDuration));
//                        Log.e("location-->", "" + storeModels.toString());
//                        Log.e("results-->", "" + results.toString());
//
//                        RecyclerViewAdapter adapterStores = new RecyclerViewAdapter(getApplicationContext(),results, storeModels);
//                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Restaurant_search_activity.this, LinearLayoutManager.HORIZONTAL, true);
//                        recyclerView.setLayoutManager(mLayoutManager);
//                        recyclerView.setAdapter(adapterStores);
//                        adapterStores.notifyDataSetChanged();
//
//                    }
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<ResultDistanceMatrix> call, Throwable t) {
//                call.cancel();
//            }
//        });
//
//    }
}
