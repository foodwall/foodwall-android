package com.kaspontech.foodwall.reviewpackage.restaurantSearch;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.modelclasses.HistoricalMap.GetRestaurantsResult;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.OpeningHours;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Restaurant_output_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.REST_API.API_interface_restaurant;
import com.kaspontech.foodwall.reviewpackage.Image_load_activity;
import com.kaspontech.foodwall.reviewpackage.ReviewTestActivity;
import com.kaspontech.foodwall.reviewpackage.View_restaurtant_inside_activity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewRestaurantSearchAdapter extends
        RecyclerView.Adapter<ReviewRestaurantSearchAdapter.MyViewHolder> {

    Context context;
    private List<GetRestaurantsResult> getRestaurantsResultsList;
    private List<OpeningHours> openingHoursPojoList;
    API_interface_restaurant apiService;
    ReviewRestaurantSearch reviewRestaurantSearch;

    public ReviewRestaurantSearchAdapter(Context context, List<GetRestaurantsResult> getRestaurantsResultsList, List<OpeningHours> openingHoursPojo) {
        this.context = context;
        this.getRestaurantsResultsList = getRestaurantsResultsList;
        this.openingHoursPojoList = openingHoursPojo;
        this.reviewRestaurantSearch= (ReviewRestaurantSearch) context;

    }

    @Override
    public ReviewRestaurantSearchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_restaurent_search_display, parent, false);

        return new ReviewRestaurantSearchAdapter.MyViewHolder(view,reviewRestaurantSearch);
    }


    @Override
    public void onBindViewHolder(ReviewRestaurantSearchAdapter.MyViewHolder holder, final int position) {


        Log.e("PhotosCountAdapter", "" + getRestaurantsResultsList.get(position).getRestaurantPhotos().size());

        holder.txtStoreName.setText(getRestaurantsResultsList.get(position).getName());

        String restaurant = getRestaurantsResultsList.get(position).getFormattedAddress();

        holder.txtStoreAddr.setText(restaurant);

        if (getRestaurantsResultsList.get(position).getRating() == null) {

            holder.rtng_restaurant.setVisibility(View.GONE);

        } else {

            try {

                String color_string = "#4aa3df";
                int myColor = Color.parseColor(color_string);

                String rating = getRestaurantsResultsList.get(position).getRating();

                holder.rtng_restaurant.setRating(Float.valueOf(rating));
                LayerDrawable stars = (LayerDrawable) holder.rtng_restaurant.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(myColor, PorterDuff.Mode.SRC_ATOP);

            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }



        try {


            String photo_ref = getRestaurantsResultsList.get(position).getRestaurantPhotos().get(0).getPhotoReference();
            Image_load_activity.loadRestaurantImages(context, photo_ref, holder.imgrestaurant);
//            reviewRestaurantSearch.noresults(0);

//            Image_load_activity.loadRestaurantImages(context, photo_ref, holder.imgrestaurant);
//            Log.e("PhotosCountAdapter", "Photos->: " + photo_ref.substring(0,20)+" "+position);
           /* Log.e("photo_ref", "" + photo_ref);
            Log.e("photo_ref", "" + getRestaurantsResultsList.get(position).getRestaurantPhotos().get(0).getPhotoReference());*/



        } catch (Exception e) {
//            holder.ll_hotel.setVisibility(View.GONE);

//            reviewRestaurantSearch.noresults(1);

//
            e.printStackTrace();
        }




        holder.ll_hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {

//                    Intent intent = new Intent(context, View_restaurtant_inside_activity.class);
                    Intent intent = new Intent(context, ReviewTestActivity.class);
                    intent.putExtra("Hotel_name", getRestaurantsResultsList.get(position).getName());
                    intent.putExtra("Hotel_rating", getRestaurantsResultsList.get(position).getRating());
                    intent.putExtra("placeid", getRestaurantsResultsList.get(position).getPlaceId());
                    intent.putExtra("googleid", getRestaurantsResultsList.get(position).getId());
                    if (getRestaurantsResultsList.get(position).getFormattedAddress() == null) {

                        intent.putExtra("Hotel_location", "Address is not available.");

                    } else {
                        intent.putExtra("Hotel_location", getRestaurantsResultsList.get(position).getFormattedAddress());
                    }


                    if(getRestaurantsResultsList.get(position).getRestaurantPhotos()==null){

                    }else{
                        intent.putExtra("Hotel_icon", getRestaurantsResultsList.get(position).getRestaurantPhotos().get(0).getPhotoReference());
                    }
                  /*  if (getRestaurantsResultsList.get(position).getRestaurantPhotos().get(position).getPhotoReference() == null) {
                        intent.putExtra("Hotel_icon", getRestaurantsResultsList.get(position).getIcon());

                    } else {
                        intent.putExtra("Hotel_icon", getRestaurantsResultsList.get(position).getRestaurantPhotos().get(position).getPhotoReference());

                    }*/

                    context.startActivity(intent);

                } catch (Exception e) {


                    e.printStackTrace();
                }

            }
        });


    }


    @Override
    public int getItemCount() {
        return getRestaurantsResultsList.size();
    }


    // Photo_places

    private void places_photo(int width, String html_reference) {

        Call<Restaurant_output_pojo> call = apiService.places_photo(width, html_reference, APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<Restaurant_output_pojo>() {
            @Override
            public void onResponse(Call<Restaurant_output_pojo> call, Response<Restaurant_output_pojo> response) {
                Restaurant_output_pojo restaurantOutputPojo = response.body();


                if (response.isSuccessful()) {

                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {
                        Toast.makeText(context, "Success " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                    }


                } else if (!response.isSuccessful()) {
                    Toast.makeText(context, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<Restaurant_output_pojo> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });


    }
    private void removeat(int position){

        getRestaurantsResultsList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getRestaurantsResultsList.size());



    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView txtStoreName;
        TextView txtStoreAddr, txtStoreimage,txtNoRatings;
        ImageView imgrestaurant;
        RatingBar rtng_restaurant;
        LinearLayout ll_hotel;

        ReviewRestaurantSearch reviewRestaurantSearch;


        public MyViewHolder(View itemView, ReviewRestaurantSearch reviewRestaurantSearch2) {
            super(itemView);
            this.reviewRestaurantSearch= reviewRestaurantSearch2;
            txtStoreName = (TextView) itemView.findViewById(R.id.txtStoreName);
            txtStoreAddr = (TextView) itemView.findViewById(R.id.txtStoreAddr);
            txtStoreimage = (TextView) itemView.findViewById(R.id.txtStoreimage);
            txtNoRatings = (TextView) itemView.findViewById(R.id.txtNoRatings);
            imgrestaurant = (ImageView) itemView.findViewById(R.id.img_location_icon);
            rtng_restaurant = (RatingBar) itemView.findViewById(R.id.rtng_restaurant);
            ll_hotel = (LinearLayout) itemView.findViewById(R.id.ll_hotel);
            ll_hotel.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {


        }

    }
}

