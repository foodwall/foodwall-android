package com.kaspontech.foodwall.reviewpackage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_adapter.Review_comments_adapter;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.EventsPojo.CreateEditEventsComments;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Review_comments_all_output;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Review_comments_all_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewInputAllPojo;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//

/**
 * Created by Vishnu on 17-06-2018.
 */

public class Reviews_comments_all_activity extends _SwipeActivityClass implements View.OnClickListener {


    private static final String TAG = "Reviews_comments_all";

    // Action Bar
    public Toolbar actionBar, actionbar_delete;
    View view;
    ImageButton back, img_btn_close, delete_comment;
    public EmojiconEditText edittxt_comment_review;
    EmojIconActions emojIcon;
    TextView txt_postnew, txt_replying_to;
    ImageView userphoto_commentnew;
    RelativeLayout rl_postlayoutnew, rl_replying, rl_commentactivity;
    RecyclerView rv_comments;
    LinearLayoutManager llm;

    LinearLayout ll_comment_nodata, rl_comment;
    ProgressBar progress_dialog,progress_review;
    ArrayList<Review_comments_all_pojo> review_commentlist = new ArrayList<>();
    int hotelid, reviewid, userid;
    String hotelname, overallrating, totaltimedelivery, taste_count, vfm_rating, package_rating, reviewprofile,
            createdon, firstname, lastname, title_ambience, addbucketlist;

    //Interface clicking ids
    int frmAdaptertl_cmmntid, frm_adap_userId, frm_adap_timelineId, from_adapter_hotelId, clickedforreply, clickedforedit, clickedforeditreply, clickededitreplyid;


    String hotel_id, user_fname, user_lname, user_id, ago_comment, createdby, total_comments, comments, picture, htl_cmmt_id, google_id, phone,
            placeid, hotel_name, hotel_review, cate_type, opentimes, latitude, longitude, tot_likes, tot_comments, photo_ref, cmmt_tot_likes, cmmtlikes_id, cmmt_tot_reply, review_id, review_cmmtlikes, edittextpost_review;
    Review_comments_adapter reviewCommentsAdapter;

    Review_comments_all_pojo reviewCommentsAllPojo;

    ReviewInputAllPojo reviewInputAllPojo;

    ArrayList<ReviewInputAllPojo> reviewInputAllPojoArrayList = new ArrayList<>();


    //Review details

    TextView review_username, userlastname,
            tv_restaurant_name,
            tv_res_overall_rating,
            ambi_or_timedelivery,
            tv_ambience_rating,
            txt_taste_count,
            tv_vfm_rating,
            serv_or_package,
            tv_service_rating,
            txt_reviewcreated_on;

    ImageView img_overall_rating_star, img_ambience_star, img_taste_star, img_vfm_star, img_service_star, img_bucket_list;
    LinearLayout ll_review_main, ll_res_overall_rating, ll_ambience_rating, ll_taste_rating, ll_value_of_money_rating, ll_service_rating;

    CircleImageView review_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments_layout);


        initComponents();

        ll_review_main.setVisibility(View.VISIBLE);

        edittxt_comment_review.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (edittxt_comment_review.getText().toString().trim().length() != 0) {
                    txt_postnew.setVisibility(View.VISIBLE);
                } else {
                    txt_postnew.setVisibility(View.GONE);
                }

            }
        });

        //Getting value from adapter

        Intent intent = getIntent();
        hotelid = Integer.parseInt(intent.getStringExtra("hotelid"));
        reviewid = Integer.parseInt(intent.getStringExtra("reviewid"));
        userid = Integer.parseInt(intent.getStringExtra("userid"));

        hotelname = intent.getStringExtra("hotelname");
        overallrating = intent.getStringExtra("overallrating");
        totaltimedelivery = intent.getStringExtra("totaltimedelivery");
        taste_count = intent.getStringExtra("taste_count");
        vfm_rating = intent.getStringExtra("vfm_rating");
        package_rating = intent.getStringExtra("package_rating");
        reviewprofile = intent.getStringExtra("reviewprofile");
        createdon = intent.getStringExtra("createdon");
        firstname = intent.getStringExtra("firstname");
        lastname = intent.getStringExtra("lastname");
        title_ambience = intent.getStringExtra("title_ambience");
        addbucketlist = intent.getStringExtra("addbucketlist");

        Log.e(TAG, "onCreate: addbucketlist" + addbucketlist);

        //review profile
        GlideApp.with(getApplicationContext())
                .load(reviewprofile)
                .placeholder(R.drawable.ic_add_photo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(review_profile);



        /* *
         ** set Review details
         */
        tv_restaurant_name.setText(hotelname);
        tv_res_overall_rating.setText(overallrating);
        // ambi_or_timedelivery.setText(String.valueOf(totaltimedelivery));
        tv_ambience_rating.setText(totaltimedelivery);
        txt_taste_count.setText(taste_count);
        tv_vfm_rating.setText(vfm_rating);
        // serv_or_package.setText(String.valueOf(vfm_rating));
        tv_service_rating.setText(package_rating);
        review_username.setText(firstname);
        userlastname.setText(lastname);
        ambi_or_timedelivery.setText(title_ambience);
        // Timestamp for timeline
        try {
            Utility.setTimeStamp(createdon, txt_reviewcreated_on);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            /**
             * Overall rating background
             */
            if (Integer.parseInt(overallrating) == 1) {

                tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_red);
                ll_res_overall_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_red_background));

            } else if (Integer.parseInt(overallrating) == 2) {

                tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                ll_res_overall_rating.setBackgroundResource(R.drawable.review_rating_orange_bg);
                img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_orange_star);

            } else if (Integer.parseInt(overallrating) == 3) {

                tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                ll_res_overall_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_light_orange_bg));

            } else if (Integer.parseInt(overallrating) == 4) {
                tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_star);
                ll_res_overall_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_light_green_bg));
            } else if (Integer.parseInt(overallrating) == 5) {
                tv_res_overall_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_overall_rating_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                ll_res_overall_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_dark_green_bg));
            }


            /**
             * total time delivery
             */
            if (Integer.parseInt(totaltimedelivery) == 1) {
                tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_ambience_star.setImageResource(R.drawable.ic_review_rating_red);
                ll_ambience_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_red_background));

            } else if (Integer.parseInt(totaltimedelivery) == 2) {
                tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_ambience_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                ll_ambience_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_orange_bg));
            } else if (Integer.parseInt(totaltimedelivery) == 3) {
                tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_ambience_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                ll_ambience_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_light_orange_bg));
            } else if (Integer.parseInt(totaltimedelivery) == 4) {
                tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_ambience_star.setImageResource(R.drawable.ic_review_rating_star);

                ll_ambience_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_light_green_bg));
            } else if (Integer.parseInt(totaltimedelivery) == 5) {
                tv_ambience_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_ambience_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                ll_ambience_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_dark_green_bg));
            }

            /**
             * total flavour count
             */
            if (Integer.parseInt(taste_count) == 1) {
                txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                img_taste_star.setImageResource(R.drawable.ic_review_rating_red);
                ll_taste_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_red_background));

            } else if (Integer.parseInt(taste_count) == 2) {
                txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                img_taste_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                ll_taste_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_orange_bg));
            } else if (Integer.parseInt(taste_count) == 3) {
                txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                img_taste_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                ll_taste_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_light_orange_bg));
            } else if (Integer.parseInt(taste_count) == 4) {
                txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                img_taste_star.setImageResource(R.drawable.ic_review_rating_star);
                ll_taste_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_light_green_bg));
            } else if (Integer.parseInt(taste_count) == 5) {
                txt_taste_count.setTextColor(Color.parseColor("#FFFFFF"));
                img_taste_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                ll_taste_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_dark_green_bg));
            }


            /**
             * Total value for money count
             */
            if (Integer.parseInt(vfm_rating) == 1) {
                tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_vfm_star.setImageResource(R.drawable.ic_review_rating_red);
                ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_red_background));
            } else if (Integer.parseInt(vfm_rating) == 2) {
                tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_vfm_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_orange_bg));
            } else if (Integer.parseInt(vfm_rating) == 3) {
                tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_vfm_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_light_orange_bg));
            } else if (Integer.parseInt(vfm_rating) == 4) {
                tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_vfm_star.setImageResource(R.drawable.ic_review_rating_star);
                ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_light_green_bg));
            } else if (Integer.parseInt(vfm_rating) == 5) {
                tv_vfm_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_vfm_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                ll_value_of_money_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_dark_green_bg));
            }

            /**
             * total package count
             */

            if (Integer.parseInt(package_rating) == 1) {
                tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_service_star.setImageResource(R.drawable.ic_review_rating_red);
                ll_service_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_red_background));
            } else if (Integer.parseInt(package_rating) == 2) {
                tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_service_star.setImageResource(R.drawable.ic_review_rating_orange_star);
                ll_service_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_orange_bg));
            } else if (Integer.parseInt(package_rating) == 3) {
                tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_service_star.setImageResource(R.drawable.ic_review_rating_light_orange_star);
                ll_service_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_light_orange_bg));
            } else if (Integer.parseInt(package_rating) == 4) {
                tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_service_star.setImageResource(R.drawable.ic_review_rating_star);

                ll_service_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_light_green_bg));
            } else if (Integer.parseInt(package_rating) == 5) {
                tv_service_rating.setTextColor(Color.parseColor("#FFFFFF"));
                img_service_star.setImageResource(R.drawable.ic_review_rating_darkgreen_star);
                ll_service_rating.setBackground(ContextCompat.getDrawable(this, R.drawable.review_rating_dark_green_bg));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * Review details
         */

        // Bucket list visibility

        try {
            if (addbucketlist.equalsIgnoreCase("yes")) {
                img_bucket_list.setImageResource(R.drawable.ic_bucket_neww);
            } else {
                img_bucket_list.setImageResource(R.drawable.ic_bucket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Loading profile pic

//        Utility.picassoImageLoader(Pref_storage.getDetail(getApplicationContext(), "picture_user_login"),
//                0,userphoto_commentnew, getApplicationContext());


        GlideApp.with(getApplicationContext())
                .load(Pref_storage.getDetail(getApplicationContext(), "picture_user_login"))
                .placeholder(R.drawable.ic_add_photo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(userphoto_commentnew);

        //Calling comments all API

        get_hotel_review_comments_all();


    }

    @Override
    public void onSwipeRight() {
        finish();
    }

    @Override
    protected void onSwipeLeft() {

    }

    private void initComponents() {
        actionBar = (Toolbar) findViewById(R.id.actionbar_comment);
        back = (ImageButton) actionBar.findViewById(R.id.button_back);
        back.setOnClickListener(this);

        img_btn_close = (ImageButton) findViewById(R.id.img_btn_close);
        img_btn_close.setOnClickListener(this);

        rl_comment = (LinearLayout) findViewById(R.id.rl_commentnew);
        rl_postlayoutnew = (RelativeLayout) findViewById(R.id.rl_postlayoutnew);
        rl_commentactivity = (RelativeLayout) findViewById(R.id.rl_commentactivity);
        rl_replying = (RelativeLayout) findViewById(R.id.rl_replying);
        ll_comment_nodata = (LinearLayout) findViewById(R.id.ll_comment_nodata);

        progress_dialog = (ProgressBar) findViewById(R.id.progress_dialog_comment);
        progress_review = (ProgressBar) findViewById(R.id.progress_review);
        userphoto_commentnew = (ImageView) findViewById(R.id.userphoto_commentnew);
        edittxt_comment_review = (EmojiconEditText) findViewById(R.id.edittxt_commentnew);
        emojIcon = new EmojIconActions(Reviews_comments_all_activity.this, rl_commentactivity, edittxt_comment_review, userphoto_commentnew);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

        txt_postnew = (TextView) findViewById(R.id.txt_adapter_postnew);
        txt_replying_to = (TextView) findViewById(R.id.txt_replying_to);
        txt_postnew.setOnClickListener(this);
        rv_comments = (RecyclerView) findViewById(R.id.rv_commentsnew);


        tv_restaurant_name = (TextView) findViewById(R.id.tv_restaurant_name);
        review_username = (TextView) findViewById(R.id.review_username);
        userlastname = (TextView) findViewById(R.id.userlastname);

        ambi_or_timedelivery = (TextView) findViewById(R.id.ambi_or_timedelivery);
        tv_res_overall_rating = (TextView) findViewById(R.id.tv_res_overall_rating);
        tv_ambience_rating = (TextView) findViewById(R.id.tv_ambience_rating);
        txt_taste_count = (TextView) findViewById(R.id.txt_taste_count);
        tv_vfm_rating = (TextView) findViewById(R.id.tv_vfm_rating);
        serv_or_package = (TextView) findViewById(R.id.serv_or_package);
        tv_service_rating = (TextView) findViewById(R.id.tv_service_rating);
        txt_reviewcreated_on = (TextView) findViewById(R.id.txt_reviewcreated_on);
        review_profile = (CircleImageView) findViewById(R.id.review_profile);

        img_overall_rating_star = (ImageView) findViewById(R.id.img_overall_rating_star);
        img_ambience_star = (ImageView) findViewById(R.id.img_ambience_star);
        img_taste_star = (ImageView) findViewById(R.id.img_taste_star);
        img_vfm_star = (ImageView) findViewById(R.id.img_vfm_star);
        img_service_star = (ImageView) findViewById(R.id.img_service_star);
        img_bucket_list = (ImageView) findViewById(R.id.img_bucket_list);

        ll_res_overall_rating = (LinearLayout) findViewById(R.id.ll_res_overall_rating);
        ll_review_main = (LinearLayout) findViewById(R.id.ll_review_main);
        ll_ambience_rating = (LinearLayout) findViewById(R.id.ll_ambience_rating);
        ll_taste_rating = (LinearLayout) findViewById(R.id.ll_taste_rating);
        ll_value_of_money_rating = (LinearLayout) findViewById(R.id.ll_value_of_money_rating);
        ll_service_rating = (LinearLayout) findViewById(R.id.ll_service_rating);


    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.button_back:
                finish();
                break;

            case R.id.img_btn_close:

                rl_replying.setVisibility(View.GONE);
                // Clearing all int values

                clickedforreply = 0;
                clickedforedit = 0;
                clickedforeditreply = 0;
                Utility.hideKeyboard(edittxt_comment_review);

                break;


            case R.id.txt_adapter_postnew:

                edittextpost_review = edittxt_comment_review.getText().toString();


                //Reply

                //Hiding keyboard
                hideKeyboard();

                Log.e(TAG, "clickedforreply-->" + clickedforreply);
                Log.e(TAG, "clickedforreply-->" + clickedforedit);
                Log.e(TAG, "clickedforreply-->" + clickedforeditreply);


                //Posting reply

                if (clickedforreply == 11) {
                    //Calling reply API
                    create_edit_hotel_review_comments_reply(frmAdaptertl_cmmntid);

                } else if (clickedforedit == 22) {

                    create_hotel_review_comments_edit(frmAdaptertl_cmmntid, from_adapter_hotelId, frm_adap_timelineId);

                } else if (clickedforeditreply == 44) {

                    create_edit_hotel_review_comments_reply_edit(frm_adap_timelineId, frmAdaptertl_cmmntid);

                } else {

                    create_hotel_review_comments();
                }


                break;
        }
    }


    // Interface

    /*
     *
     * Comments_adapter
     *
     * */

    public void clicked_username(View view, String username, int position) {

        frmAdaptertl_cmmntid = position;
        rl_replying.setVisibility(View.VISIBLE);
        txt_replying_to.setText("Replying to ".concat(username));
        edittxt_comment_review.requestFocus();
        Log.e("clicked_reply", "Comment_id" + frmAdaptertl_cmmntid);

    }

    public void clickedcommentposition(View view, int position) {
        frmAdaptertl_cmmntid = position;
    }

    public void clickedforreply(View view, int position) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_comment_review, InputMethodManager.SHOW_IMPLICIT);
        clickedforreply = position;
        clickedforedit = 0;
        edittxt_comment_review.setText("");

        Log.e("Vishnu", "clickedforreply" + clickedforreply);

    }

    public void clickedfor_edit(View view, int position) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_comment_review, InputMethodManager.SHOW_IMPLICIT);
        rl_replying.setVisibility(View.GONE);
        clickedforedit = position;
        clickedforreply = 0;


        Log.e("Vishnu", "clickedforedit" + clickedforedit);

    }

    public void check_review_reply_size(int size) {
        Log.e("Vishnu", "check_reply_size" + size);
        if (size == 0) {
            get_hotel_review_comments_all();
        } else {

        }
    }


    public void check_review_size(int size) {
        Log.e("Vishnu", "check_size" + size);
        if (size == 0) {

            ll_comment_nodata.setVisibility(View.VISIBLE);
            edittxt_comment_review.requestFocus();
        } else {
            ll_comment_nodata.setVisibility(View.GONE);
        }
    }







    /*
     * Comments_reply_adapter
     *
     * */

    //edit commment
    public void clickedcommentposition_timelineid(View view, String comments, int position, int hotelId, int timelineid) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_comment_review, InputMethodManager.SHOW_IMPLICIT);

        frmAdaptertl_cmmntid = position;
        frm_adap_timelineId = timelineid;
        from_adapter_hotelId = hotelId;
        edittxt_comment_review.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(comments));

        int pos = edittxt_comment_review.getText().length();
        edittxt_comment_review.setSelection(pos);
        edittxt_comment_review.requestFocus();
    }

    //Reply edit  commment

    public void clickedcommentposition_reply_edit(View view, String comments, int replyid, int comment_tl_id) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_comment_review, InputMethodManager.SHOW_IMPLICIT);
        frmAdaptertl_cmmntid = comment_tl_id;
        frm_adap_timelineId = replyid;
        edittxt_comment_review.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(comments));
        int pos = edittxt_comment_review.getText().length();
        edittxt_comment_review.setSelection(pos);
        edittxt_comment_review.requestFocus();
    }

    public void clickedfor_replyedit(View view, int position) {

        rl_replying.setVisibility(View.GONE);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_comment_review, InputMethodManager.SHOW_IMPLICIT);
        clickedforeditreply = position;

        Log.e("Vishnu", "clickedforeditreply" + clickedforeditreply);

    }

    public void clickedfor_replyid(View view, int position) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edittxt_comment_review, InputMethodManager.SHOW_IMPLICIT);
        clickededitreplyid = position;
        clickedforedit = 0;
        clickedforreply = 0;


        Log.e("Vishnu", "clickededitreplyid" + clickededitreplyid);

    }

    public void clickedcomment_userid_position(View view, int position) {
        frm_adap_userId = position;
        Log.e("Vishnu", "clickeduserId" + frm_adap_userId);

    }

    public void clickedfordelete() {

        edittxt_comment_review.setText("");
        rl_replying.setVisibility(View.GONE);

    }


    //Calling comments all API

    private void get_hotel_review_comments_all() {

        if (isConnected(getApplicationContext())) {

            String userid = Pref_storage.getDetail(getApplicationContext(), "userId");
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                Call<Review_comments_all_output> call = apiService.get_hotel_review_comments_all("get_hotel_review_comments_all", hotelid, reviewid, Integer.parseInt(userid));
                call.enqueue(new Callback<Review_comments_all_output>() {
                    @Override
                    public void onResponse(Call<Review_comments_all_output> call, Response<Review_comments_all_output> response) {

                        if (response.body().getResponseCode() == 1) {

                            review_commentlist.clear();

                            review_commentlist = response.body().getData();

                            reviewCommentsAdapter = new Review_comments_adapter(Reviews_comments_all_activity.this, review_commentlist);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(Reviews_comments_all_activity.this);
                            rv_comments.setLayoutManager(mLayoutManager);
//                            rv_comments.setHasFixedSize(true);
                            rv_comments.setAdapter(reviewCommentsAdapter);
                            rv_comments.setNestedScrollingEnabled(false);
                            progress_dialog.setVisibility(View.GONE);
                            reviewCommentsAdapter.notifyDataSetChanged();


                        }


                    }


                    @Override
                    public void onFailure(Call<Review_comments_all_output> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());

                        if (review_commentlist.isEmpty()) {
                            ll_comment_nodata.setVisibility(View.VISIBLE);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {


            Snackbar snackbar = Snackbar.make(rl_commentactivity, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        // check if no view has focus:
        View v = getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    // Calling Normal comment API
    private void create_hotel_review_comments() {

        if (isConnected(getApplicationContext())) {

            progress_review.setVisibility(View.VISIBLE);
            progress_review.setIndeterminate(true);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                String createuserid = Pref_storage.getDetail(Reviews_comments_all_activity.this, "userId");
                Call<CreateEditEventsComments> call = apiService.create_edit_hotel_review_comments("create_edit_hotel_review_comments", 0, hotelid, reviewid, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxt_comment_review.getText().toString().trim()), Integer.parseInt(createuserid));
                call.enqueue(new Callback<CreateEditEventsComments>() {
                    @Override
                    public void onResponse(Call<CreateEditEventsComments> call, Response<CreateEditEventsComments> response) {

                        if (response.body().getResponseCode() == 1) {

                            progress_review.setVisibility(View.GONE);
                            progress_review.setIndeterminate(false);
                            edittxt_comment_review.setText("");
                            //Refresh data without loading
                            get_hotel_review_comments_all();

                        }

                    }

                    @Override
                    public void onFailure(Call<CreateEditEventsComments> call, Throwable t) {
                        //Error
                        progress_review.setVisibility(View.GONE);
                        progress_review.setIndeterminate(false);  Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rl_commentactivity, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }

    // Calling Normal edit comment API

    private void create_hotel_review_comments_edit(int commentid, int hotelid, int fromadap_reviewid) {

        if (isConnected(getApplicationContext())) {
            progress_review.setVisibility(View.VISIBLE);
            progress_review.setIndeterminate(true);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createuserid = Pref_storage.getDetail(Reviews_comments_all_activity.this, "userId");
                Call<CreateEditEventsComments> call = apiService.create_edit_hotel_review_comments("create_edit_hotel_review_comments", commentid, hotelid, fromadap_reviewid, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxt_comment_review.getText().toString().trim()), Integer.parseInt(createuserid));
                call.enqueue(new Callback<CreateEditEventsComments>() {
                    @Override
                    public void onResponse(Call<CreateEditEventsComments> call, Response<CreateEditEventsComments> response) {

                        if (response.body().getResponseCode() == 1) {

                            progress_review.setVisibility(View.GONE);
                            progress_review.setIndeterminate(false);

                            edittxt_comment_review.setText("");
                            frmAdaptertl_cmmntid = 0;
                            frm_adap_timelineId = 0;
                            clickedforedit = 0;
                            //Refresh data without loading
                            get_hotel_review_comments_all();

                        }

                    }

                    @Override
                    public void onFailure(Call<CreateEditEventsComments> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        progress_review.setVisibility(View.GONE);
                        progress_review.setIndeterminate(false);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rl_commentactivity, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    // Calling reply to a comment API
    private void create_edit_hotel_review_comments_reply(int fromadap_reviewid) {

        Log.e("API reviewid", "create_edit_hotel_review_comments_reply: " + fromadap_reviewid);

        if (isConnected(getApplicationContext())) {

            progress_review.setVisibility(View.VISIBLE);
            progress_review.setIndeterminate(true);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(Reviews_comments_all_activity.this, "userId");
                Call<CommonOutputPojo> call = apiService.create_edit_hotel_review_comments_reply("create_edit_hotel_review_comments_reply", 0, fromadap_reviewid, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxt_comment_review.getText().toString().trim()), Integer.parseInt(createuserid));
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1) {

                            progress_review.setVisibility(View.GONE);
                            progress_review.setIndeterminate(false);

                            rl_replying.setVisibility(View.GONE);

                            edittxt_comment_review.setText("");

                            frmAdaptertl_cmmntid = 0;
                            clickedforreply = 0;


                            //Refresh data without loading
                            get_hotel_review_comments_all();

                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        progress_review.setVisibility(View.GONE);
                        progress_review.setIndeterminate(false);    Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rl_commentactivity, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }

    // Calling edit to a comment API
    private void create_edit_hotel_review_comments_reply_edit(int commentid, int fromadap_reviewid) {

        if (isConnected(getApplicationContext())) {
            progress_review.setVisibility(View.VISIBLE);
            progress_review.setIndeterminate(true);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(Reviews_comments_all_activity.this, "userId");
                Call<CommonOutputPojo> call = apiService.create_edit_hotel_review_comments_reply("create_edit_hotel_review_comments_reply", commentid, fromadap_reviewid, org.apache.commons.text.StringEscapeUtils.escapeJava(edittxt_comment_review.getText().toString().trim()), Integer.parseInt(createuserid));
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1) {

                            progress_review.setVisibility(View.GONE);
                            progress_review.setIndeterminate(false);

                            edittxt_comment_review.setText("");
                            clickedforeditreply = 0;
                            frm_adap_timelineId = 0;
                            frmAdaptertl_cmmntid = 0;
                            //Refresh data without loading
                            get_hotel_review_comments_all();

                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        progress_review.setVisibility(View.GONE);
                        progress_review.setIndeterminate(false);
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Snackbar snackbar = Snackbar.make(rl_commentactivity, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


}
