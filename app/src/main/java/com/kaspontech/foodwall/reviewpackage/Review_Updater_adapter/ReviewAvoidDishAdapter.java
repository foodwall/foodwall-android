package com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.reviewpackage.alldishtab.AllDishDisplayActivity;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;

public class ReviewAvoidDishAdapter extends RecyclerView.Adapter<ReviewAvoidDishAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;


    ReviewAvoidDishPojo reviewAvoidDishPojo;
    String reviewId, hotelName,catType;
    private ArrayList<ReviewAvoidDishPojo> getDishTypeImagePojoArrayList = new ArrayList<>();

    private Pref_storage pref_storage;

    public ReviewAvoidDishAdapter(Context c, ArrayList<ReviewAvoidDishPojo> gettinglist,String reviewId,String hotelName,String catType ) {
        this.context = c;
        this.reviewId = reviewId;
        this.hotelName = hotelName;
        this.catType = catType;
        this.getDishTypeImagePojoArrayList = gettinglist;
    }


    @NonNull
    @Override
    public ReviewAvoidDishAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_image_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ReviewAvoidDishAdapter.MyViewHolder vh = new ReviewAvoidDishAdapter.MyViewHolder(v);
        vh.setIsRecyclable(false);
        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ReviewAvoidDishAdapter.MyViewHolder holder, final int position) {


        try {

            reviewAvoidDishPojo = getDishTypeImagePojoArrayList.get(position);


            if(getDishTypeImagePojoArrayList.get(position).getImg().trim().equalsIgnoreCase("")||getDishTypeImagePojoArrayList.get(position).getImg().isEmpty()||getDishTypeImagePojoArrayList.get(position).getImg()==null){
                holder.rl_hotel_datas.setVisibility(View.GONE);

//                removeat(position);

            }else {
                GlideApp.with(context)
                        .load(getDishTypeImagePojoArrayList.get(position).getImg())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)

                        .into(holder.image_review);

//                Picasso.get()
//                        .load(getDishTypeImagePojoArrayList.get(position).getImg())
//                        .into(holder.image_review);

                holder.txt_image.setText(org.apache.commons.text.StringEscapeUtils.unescapeJava(getDishTypeImagePojoArrayList.get(position).getDishname()));

            }


            holder.rl_hotel_datas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent= new Intent(context, AllDishDisplayActivity.class);
                    intent.putExtra("reviewid",reviewId);
                    intent.putExtra("hotelname",hotelName);
                    intent.putExtra("catType", catType);
                    intent.putExtra("redirectdishes", "2");
                    context.startActivity(intent);
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeat(int position){

        getDishTypeImagePojoArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getDishTypeImagePojoArrayList.size());

    }
    @Override
    public int getItemCount() {
        return getDishTypeImagePojoArrayList.size();

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //TextView

        TextView txt_image;

        //Imageview

        ImageView image_review;

        RelativeLayout rl_hotel_datas;
        public MyViewHolder(View itemView) {
            super(itemView);

            txt_image = (TextView) itemView.findViewById(R.id.txt_image);
            image_review = (ImageView) itemView.findViewById(R.id.image_review);
            rl_hotel_datas = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_datas);

        }


    }


}

