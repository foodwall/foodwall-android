/*
package com.kaspontech.foodwall.reviewPackage;

*/
/*
 * Created by Vishnu on 16-07-2018.
 *//*


import android.Manifest;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.hsalf.smilerating.SmileRating;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_ambience_slide_adapter;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_image_adapter;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Create_review_output_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Geometry_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.GetAllRestaurantsPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Get_hotel_photo_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.OpeningHours_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Restaurant_output_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.StoreModel;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.REST_API.API_interface_restaurant;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.reviewPackage.Review_adapter_folder.Get_all_hotel_review_adapter;
import com.kaspontech.foodwall.reviewPackage.Review_adapter_folder.Review_image_pojo;
import com.kaspontech.foodwall.reviewPackage.Review_adapter_folder.Review_image_top_dish_adapter;
import com.kaspontech.foodwall.reviewPackage.Review_adapter_folder.Review_inside_adapter;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.TouchImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import mabbas007.tagsedittext.TagsEditText;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.reviewPackage.Ambience_take_photo.ambience_image_List;

public class Review_test_activity extends AppCompatActivity
        implements View.OnClickListener,
        AdapterView.OnItemSelectedListener,
        AdapterView.OnItemClickListener,
        Review_ambience_slide_adapter.PhotoRemovedListener,
        Review_image_top_dish_adapter.TopDishRemovedListener{


    private static final String TAG = "Review_test_activity";
    public static PhotoView img_addimage;
    public static LinearLayout ll_image_review, ll_edt_add_dish, ll_edt_tag_dish, ll_edt_tag_worst_dish;
    // Action Bar
    Toolbar actionBar;
    ImageButton back;
    Button btn_addImages, btn_create_review;
    RadioGroup radio_category, radio_vegnonveg;
    RadioButton radio_dine, radio_delivery, radio_Veg, radio_Nonveg;
    ViewPager reviewpager;
    int CAPTURE_IMAGE_CALLBACK = 1;
    int SELECT_PICTURE_CALLBACK = 2;
    int PICK_IMAGE_MULTIPLE = 3;
    String imageEncoded;


    private static ViewPager ambi_viewpager_review;
    private static int currentPage_amb = 0;
    private static int NUM_PAGES_AMB = 0;


    private CircleIndicator ambi_review_indicator;

    LinearLayout ll_review,ll_htl_layout;
    List<String> imagesEncodedList;
    ArrayList<Review_image_pojo> addimagesEncodedList = new ArrayList<Review_image_pojo>();
    String[] filePathColumn;
    ArrayList<String> ambi_image = new ArrayList<>();
    public static ArrayList<String> ambi_review_image = new ArrayList<>();
    Bitmap bitmap;

    File file;
    EditText worst_dishes, edt_hoteltxt, edt_review_description;

    int hotel_id, review_id;
    String selectedImagePath,
            Hotel_name, Hotel_location, Hotel_icon, Hotel_rating, place_id, google_id, image, dish_name, dish_image,ambience_camera,ambience_gallery,photo_upload, inactivity_camera,inactivity_gallery,for_top,for_worst;
    //ImageView overall
    ImageView image_add_top_dish, image_add_dish_worst, image_add, rtg_one, rtg_two, rtg_three, rtg_four, rtg_five;

    //Image reset
    TouchImageView img_revie;
    ImageView img_reset, img_reset_ambiance, img_reset_taste, img_reset_value, img_reset_serive, img_restaurant_image, image_ambiance;
    Spinner spinner, dish_spinner_worst;
    TextView txt_add_image;

    File sourceFile;

    //Textview ambiance
    //Textview ratingoverall
    TextView txt_rating;
    //Textview taste
    TextView txt_rating_ambiance;

    //Textview service
    TextView txt_rating_taste;
    TextView txt_rating_service;
    //Textview value
    TextView txt_rating_value, txt_rating_low, txt_rating_low_ambiance, txt_rating_low_taste, txt_rating_low_service, txt_rating_low_value,
            txt_hotel_name, txt_hotel_address, txt_search, txt_ambiance_add,txt_service,txt_ambi;
    Review_image_adapter reviewImageAdapter;

    RelativeLayout rl_ambiance_dotindicator;

    int overall, ambiance, taste, service, value_money,timedelivery;

    //Rating bar

    //    RatingBar ratingBar;
    SmileRating ratingBar, rtng_ambiance, rtng_taste, rtng_service, rtng_value;


    //Popup details
    Button done_btn;
    ImageView img_food, img_close, img_ok;
    TextView txt_caption;
    EditText edt_food_caption;

    String next_token;

    String[] Dishtype_top = {"Top dishes"};
    String[] Dishtype_worst = {"Dishes to avoid"};


    String[] Top_dish = new String[]{
            "Chapati/Roti",
            "Naan",
            "Butter Naan",
            "Paratha",
            "Aloo Paratha",
            "Chapathi",
            "Puri",
            "Papadam",
            "Aloo tikki",
            "Butter chicken",
            "Coconut Rice",
            "Lemon Rice",
            "Jeera Rice",
            "Coconut Rice",
            "Veg Biryani",
            "Chicken Hariyali",
            "Tandoori Chicken",
            "Chicken MalaiKabab",
            "SheekhKabab",
            "Chili Chicken",
            "Grilled Chicken",
            "Keema Samosa",
            "Chicken 65",
            "Jumbo Bagari Spiced Shrimp",
            "Vegetable Formaishi Kabab",
            "Tandoori Mix Grill",
            "Tandoori Lobster",
            "Lamb BotiKabab",
            "Tandoori Fish Tikka",
            "Tandoori Jumbo Shrimp",
            "Chicken Biryani",
            "Mutton Biryani",
            "Fish Biryani",
            "Gobi Biryani",
            "Beef Biryani",
            "Mushroom Biryani",
            "Shrimp Biryani",
            "Goose Biryani",
            "Chana masala ",
            "Chicken Tikka masala",
            "Dosa",
            "Masala dosa",
            "Momos",
            "Mutton korma",
            "Pakora",
            "Paneer",
            "Pulao",
            "Chicken Roll",
            "Mutton Roll",
            "Samosa",
            "Shahi paneer",
            "Thali",
            "Vadas",
            "Halwa",
            "Rasgulla/Roshogulla",
            "Kulfi",
            "Kheer",


    };

    String[] worst_dish = new String[]{
            "Chapati/Roti",
            "Naan",
            "Butter Naan",
            "Paratha",
            "Aloo Paratha",
            "Chapathi",
            "Puri",
            "Papadam",
            "Aloo tikki",
            "Butter chicken",
            "Coconut Rice",
            "Lemon Rice",
            "Jeera Rice",
            "Coconut Rice",
            "Veg Biryani",
            "Chicken Hariyali",
            "Tandoori Chicken",
            "Chicken Malai Kabab",
            "SheekhKabab",
            "Chilli Chicken",
            "Grilled Chicken",
            "Keema Samosa",
            "Chicken 65",
            "Jumbo Bagari Spiced Shrimp",
            "Vegetable Formaishi Kabab",
            "Tandoori Mix Grill",
            "Tandoori Lobster",
            "Lamb BotiKabab",
            "Tandoori Fish Tikka",
            "Tandoori Jumbo Shrimp",
            "Chicken Biryani",
            "Mutton Biryani",
            "Fish Biryani",
            "Gobi Biryani",
            "Beef Biryani",
            "Mushroom Biryani",
            "Shrimp Biryani",
            "Goose Biryani",
            "Chana masala ",
            "Chicken Tikka masala",
            "Dosa",
            "Masala dosa",
            "Momos",
            "Mutton korma",
            "Pakora",
            "Paneer",
            "Pulao",
            "Chicken Roll",
            "Mutton Roll",
            "Samosa",
            "Shahi paneer",
            "Thali",
            "Vadas",
            "Halwa",
            "Rasgulla",
            "Kulfi",
            "Kheer",


    };
    AutoCompleteTextView autoCompleteTextView;
    String type;
    String autocompleteresult, tageditresult, tagedit_worst_result;
    public static TagsEditText tagsedittext, tagsedittext_worst;

    RecyclerView rv_top_dish, rv_worst_dish, rv_img_restaurant;

    DecimalFormat df;
    String path_gallery;

    int category, veg_nonveg;

    List<Get_hotel_photo_pojo> photos;
    List<OpeningHours_pojo> openingHoursPojoList;
    List<String> types;
    List<String> htmlAttributions;
    List<StoreModel> storeModels;
    //Pojo
    Geometry_pojo geometryPojo;
    Get_hotel_photo_pojo getHotelPhotoPojo_new;
    OpeningHours_pojo openingHoursPojo;
    private List<Object> weekdayText;
//    ArrayList<Getall_restaurant_details_pojo> getallRestaurantDetailsPojoArrayList = new ArrayList<>();
    ArrayList<GetAllRestaurantsPojo> getallRestaurantDetailsPojoArrayList = new ArrayList<>();
    List<Restaurant_output_pojo> restaurantOutputPojoList;
    Get_all_hotel_review_adapter get_all_hotel_review_adapter;

    API_interface_restaurant apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_review);
//        setContentView(R.layout.create_review_unused);
//        setContentView(R.layout.create_review_ratingbar);

        actionBar = (Toolbar) findViewById(R.id.action_bar_write_post);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        btn_addImages = (Button) findViewById(R.id.btn_addImages);
        btn_addImages.setOnClickListener(this);
        //Radio group
        radio_category = (RadioGroup) findViewById(R.id.radio_category);
        radio_vegnonveg = (RadioGroup) findViewById(R.id.radio_vegnonveg);


        radio_dine = (RadioButton) findViewById(R.id.radio_dine);
        radio_delivery = (RadioButton) findViewById(R.id.radio_delivery);
        radio_Veg = (RadioButton) findViewById(R.id.radio_Veg);
        radio_Nonveg = (RadioButton) findViewById(R.id.radio_Nonveg);


        ambi_viewpager_review = (ViewPager) findViewById(R.id.ambi_viewpager_review);
        ambi_review_indicator = (CircleIndicator) findViewById(R.id.ambiindicator);
        rl_ambiance_dotindicator = (RelativeLayout) findViewById(R.id.rl_ambiance_dotindicator);


        tagsedittext = (TagsEditText) findViewById(R.id.tagsedittext);
        tagsedittext.setImeOptions(EditorInfo.IME_ACTION_DONE);

        tagsedittext_worst = (TagsEditText) findViewById(R.id.tagsedittext_worst);
        tagsedittext_worst.setImeOptions(EditorInfo.IME_ACTION_DONE);

        edt_hoteltxt = (EditText) findViewById(R.id.edt_hoteltxt);
        edt_review_description = (EditText) findViewById(R.id.edt_review_description);


        rv_top_dish = (RecyclerView) findViewById(R.id.rv_top_dish);
        rv_worst_dish = (RecyclerView) findViewById(R.id.rv_worst_dish);
        rv_img_restaurant = (RecyclerView) findViewById(R.id.rv_img_restaurant);

        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);


        image_add_top_dish = (ImageView) findViewById(R.id.image_add_top_dish);
        img_restaurant_image = (ImageView) findViewById(R.id.img_restaurant_image);

        img_restaurant_image.setOnClickListener(this);
        image_ambiance = (ImageView) findViewById(R.id.image_ambiance);

        image_ambiance.setOnClickListener(this);


        img_revie = (TouchImageView) findViewById(R.id.img_revie);

        df = new DecimalFormat("#.##");

        image_add_top_dish.setOnClickListener(this);


        image_add_dish_worst = (ImageView) findViewById(R.id.image_add_dish_worst);
        image_add_dish_worst.setOnClickListener(this);

    */
/*    image_add = (ImageView) findViewById(R.id.image_add);
        image_add.setOnClickListener(this);*//*


        img_addimage = (PhotoView) findViewById(R.id.img_addimage);
        img_addimage.setOnClickListener(this);

        //Smile Rating
        ratingBar = (SmileRating) findViewById(R.id.rtng_overall);
        rtng_ambiance = (SmileRating) findViewById(R.id.rtng_ambiance);
        rtng_service = (SmileRating) findViewById(R.id.rtng_service);
        rtng_taste = (SmileRating) findViewById(R.id.rtng_taste);
        rtng_value = (SmileRating) findViewById(R.id.rtng_value);

        //TextView
        txt_rating_low = (TextView) findViewById(R.id.txt_rating_low);
        txt_rating_low_ambiance = (TextView) findViewById(R.id.txt_rating_low_ambiance);
        txt_rating_low_taste = (TextView) findViewById(R.id.txt_rating_low_taste);
        txt_rating_low_service = (TextView) findViewById(R.id.txt_rating_low_service);
        txt_rating_low_value = (TextView) findViewById(R.id.txt_rating_low_value);
        txt_hotel_name = (TextView) findViewById(R.id.txt_hotel_name);
        txt_hotel_address = (TextView) findViewById(R.id.txt_hotel_address);
        txt_service = (TextView) findViewById(R.id.txt_service);
        txt_ambi = (TextView) findViewById(R.id.txt_ambi);
        txt_search = (TextView) findViewById(R.id.txt_search);
        txt_search.setOnClickListener(this);

        txt_ambiance_add = (TextView) findViewById(R.id.txt_ambiance_add);

        txt_ambiance_add.setOnClickListener(this);


        img_reset = (ImageView) findViewById(R.id.img_reset);
        img_reset.setOnClickListener(this);

        img_reset_ambiance = (ImageView) findViewById(R.id.img_reset_ambiance);
        img_reset_ambiance.setOnClickListener(this);

        img_reset_serive = (ImageView) findViewById(R.id.img_reset_serive);
        img_reset_serive.setOnClickListener(this);

        img_reset_taste = (ImageView) findViewById(R.id.img_reset_taste);
        img_reset_taste.setOnClickListener(this);

        img_reset_value = (ImageView) findViewById(R.id.img_reset_value);
        img_reset_value.setOnClickListener(this);


//        worst_dishes = (EditText) findViewById(R.id.worst_dishes);


        btn_create_review = (Button) findViewById(R.id.btn_create_review);



        */
/*radio_dine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                txt_ambi.setText(R.string.ambiance);
                txt_service.setText(R.string.service);
            }
        });*//*


        radio_delivery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                txt_ambi.setText(R.string.Delivery);
                txt_service.setText(R.string.Package);
            }
        });

        btn_create_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (overall != 0 && taste != 0 && ambiance != 0 && service != 0 && value_money != 0) {
                    int category_type = radio_category.getCheckedRadioButtonId();

                    if (category_type == radio_dine.getId()) {

                        category = 1;

                    } else {

                        category = 2;

                    }


                    int food_type = radio_vegnonveg.getCheckedRadioButtonId();

                    if (food_type == radio_Veg.getId()) {

                        veg_nonveg = 1;

                    } else {

                        veg_nonveg = 2;

                    }

                    if(category==1){
                        create_review();
                    }else {
                        create_review_delivery();
                    }



                } else {
                    Toast.makeText(getApplicationContext(), "Please provide all ratings to proceed.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        ll_image_review = (LinearLayout) findViewById(R.id.ll_image_review);
        ll_review = (LinearLayout) findViewById(R.id.ll_review);
        ll_htl_layout = (LinearLayout) findViewById(R.id.ll_htl_layout);
        ll_edt_add_dish = (LinearLayout) findViewById(R.id.ll_edt_add_dish);
        ll_edt_tag_dish = (LinearLayout) findViewById(R.id.ll_edt_tag_dish);
        ll_edt_tag_worst_dish = (LinearLayout) findViewById(R.id.ll_edt_tag_worst_dish);


        spinner = (Spinner) findViewById(R.id.dish_spinner);
        dish_spinner_worst = (Spinner) findViewById(R.id.dish_spinner_worst);


*/
/*
        tagsedittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    View popupView2 = LayoutInflater.from(Review_test_activity.this).inflate(R.layout.review_popup_layout, null);
                    final PopupWindow popupWindow2 = new PopupWindow(popupView2, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    popupWindow2.showAsDropDown(popupView2, 0, 0);
                    done_btn = popupView2.findViewById(R.id.done_btn);
                    img_food = popupView2.findViewById(R.id.img_food);
                    img_close = popupView2.findViewById(R.id.img_close);
                    txt_food_caption = popupView2.findViewById(R.id.txt_food_caption);


                    img_close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            popupWindow2.dismiss();
                        }
                    });

                }
                return false;
            }
        });

*//*



    */
/*    img_revie.setOnTouchImageViewListener(new TouchImageView.OnTouchImageViewListener() {


            @Override
            public void onMove() {
                PointF point = img_revie.getScrollPosition();
                RectF rect = img_revie.getZoomedRect();
                float currentZoom = img_revie.getCurrentZoom();
                boolean isZoomed = img_revie.isZoomed();

            }
        });*//*


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (position == 0) {
                    type = spinner.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.VISIBLE);
//                    ll_edt_add_dish.setVisibility(View.VISIBLE);
                    ll_edt_tag_dish.setVisibility(View.VISIBLE);
                }

*/
/*                if (position == 1) {
                    type = spinner.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.VISIBLE);
//                    ll_edt_add_dish.setVisibility(View.VISIBLE);
                    ll_edt_tag_dish.setVisibility(View.VISIBLE);
                } else if (position == 2) {
                    type = spinner.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.VISIBLE);
//                    ll_edt_add_dish.setVisibility(View.VISIBLE);
                    ll_edt_tag_dish.setVisibility(View.VISIBLE);
                }*//*


                if (type.equals("Top dishes")) {

                    try {
                        ArrayAdapter arrayAdapter = new ArrayAdapter(Review_test_activity.this, R.layout.autocompletetxt, Top_dish);
                        tagsedittext.setAdapter(arrayAdapter);
                        autoCompleteTextView.setAdapter(arrayAdapter);
                        autoCompleteTextView.setThreshold(1);
                        autocompleteresult = autoCompleteTextView.getText().toString();
                        tageditresult = tagsedittext.getText().toString();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        //Worst spinner
        dish_spinner_worst.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    type = dish_spinner_worst.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.VISIBLE);
//                    ll_edt_add_dish.setVisibility(View.VISIBLE);
                    ll_edt_tag_worst_dish.setVisibility(View.VISIBLE);
                }
*/
/*
                if (position == 1) {
                    type = dish_spinner_worst.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.VISIBLE);
//                    ll_edt_add_dish.setVisibility(View.VISIBLE);
                    ll_edt_tag_worst_dish.setVisibility(View.VISIBLE);
                } else if (position == 1) {
                    type = dish_spinner_worst.getSelectedItem().toString();
                    autoCompleteTextView.setVisibility(View.VISIBLE);
//                    ll_edt_add_dish.setVisibility(View.VISIBLE);
                    ll_edt_tag_worst_dish.setVisibility(View.VISIBLE);
                }*//*


                if (type.equals("Top dishes")) {


                    try {
                        ArrayAdapter arrayAdapter = new ArrayAdapter(Review_test_activity.this, R.layout.autocompletetxt, Top_dish);
                        tagsedittext_worst.setAdapter(arrayAdapter);
                        autoCompleteTextView.setAdapter(arrayAdapter);
                        autoCompleteTextView.setThreshold(1);
                        autocompleteresult = autoCompleteTextView.getText().toString();
                        tagedit_worst_result = tagsedittext_worst.getText().toString();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else if (type.equals("Dishes to avoid")) {

                    try {
                        ArrayAdapter arrayAdapter = new ArrayAdapter(Review_test_activity.this, R.layout.autocompletetxt, worst_dish);
                        autoCompleteTextView.setAdapter(arrayAdapter);
                        tagsedittext_worst.setAdapter(arrayAdapter);
                        autoCompleteTextView.setThreshold(1);
                        autocompleteresult = autoCompleteTextView.getText().toString();
                        tagedit_worst_result = tagsedittext_worst.getText().toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Dishtype_top);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aa);


        ArrayAdapter bb = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Dishtype_worst);
        bb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        dish_spinner_worst.setAdapter(bb);


//        View popupView2 = LayoutInflater.from(WriteReviewActivity.this).inflate(R.layout.review_popup_layout, null);
//        final PopupWindow popupWindow2 = new PopupWindow(popupView2, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        popupWindow2.showAsDropDown(popupView, 0, 0);
//        done_btn = popupView.findViewById(R.id.done_btn);
//        img_food = popupView.findViewById(R.id.img_food);
//        txt_food_caption = popupView.findViewById(R.id.txt_food_caption);
//
//
//        done_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                popupWindow.dismiss();
//            }
//        });


        addListenerOnRatingBar();

        Intent intent = getIntent();
        Hotel_name = intent.getStringExtra("Hotel_name");
        Hotel_icon = intent.getStringExtra("Hotel_icon");
        Hotel_location = intent.getStringExtra("Hotel_location");
        Hotel_rating = intent.getStringExtra("Hotel_rating");
        place_id = intent.getStringExtra("placeid");
        google_id = intent.getStringExtra("googleid");
        dish_image = intent.getStringExtra("dish_image");
        dish_name = intent.getStringExtra("dish_name");
        ambience_camera = intent.getStringExtra("userimage_camera");
        ambience_gallery = intent.getStringExtra("userimage_gallery");

        for_top = intent.getStringExtra("for_top");
        for_worst = intent.getStringExtra("for_worst");


        try {
            if (Hotel_name==null||Hotel_name.isEmpty()){

                ll_htl_layout.setVisibility(View.GONE);
            }else {
                ll_htl_layout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Image_load_activity.loadGooglePhoto(this, img_restaurant_image, Hotel_icon);

        edt_hoteltxt.setText(Hotel_name);
        txt_hotel_name.setText(Hotel_name);
        txt_hotel_address.setText(Hotel_location);
        rv_img_restaurant.setVisibility(View.GONE);
        img_restaurant_image.setVisibility(View.VISIBLE);


        try {
            if (!dish_image.equalsIgnoreCase("") && !dish_name.equalsIgnoreCase("")&&for_top!=null) {

                Image_load_activity.loadGooglePhoto(this, img_restaurant_image, Hotel_icon);
                edt_hoteltxt.setText(Hotel_name);
                txt_hotel_name.setText(Hotel_name);
                ll_htl_layout.setVisibility(View.VISIBLE);
                txt_hotel_address.setText(Hotel_location);
                rv_img_restaurant.setVisibility(View.GONE);
                img_restaurant_image.setVisibility(View.VISIBLE);

                Review_image_pojo reviewImagePojo = new Review_image_pojo(dish_image, dish_name);
                addimagesEncodedList.add(reviewImagePojo);
                Review_image_top_dish_adapter reviewImageTopDishAdapter = new Review_image_top_dish_adapter(Review_test_activity.this, addimagesEncodedList,this);
                LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, true);
                rv_top_dish.setLayoutManager(llm);
                rv_top_dish.setHasFixedSize(true);
                rv_top_dish.setAdapter(reviewImageTopDishAdapter);
                reviewImageTopDishAdapter.notifyDataSetChanged();

            }    if (!dish_image.equalsIgnoreCase("") && !dish_name.equalsIgnoreCase("")&&for_worst!=null) {

                Image_load_activity.loadGooglePhoto(this, img_restaurant_image, Hotel_icon);
                edt_hoteltxt.setText(Hotel_name);
                txt_hotel_name.setText(Hotel_name);
                txt_hotel_address.setText(Hotel_location);
                ll_htl_layout.setVisibility(View.VISIBLE);
                rv_img_restaurant.setVisibility(View.GONE);
                img_restaurant_image.setVisibility(View.VISIBLE);

                */
/*Review_image_pojo reviewImagePojo = new Review_image_pojo(dish_image, dish_name);
                addimagesEncodedList.add(reviewImagePojo);
                Review_image_worst_dish_adapter reviewImageWorstDishAdapter = new Review_image_worst_dish_adapter(Review_test_activity.this, addimagesEncodedList);
                LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, true);
                rv_worst_dish.setLayoutManager(llm);
                rv_worst_dish.setHasFixedSize(true);
                rv_worst_dish.setAdapter(reviewImageWorstDishAdapter);
                reviewImageWorstDishAdapter.notifyDataSetChanged();*//*


            }




            else if (dish_image == null && !edt_hoteltxt.getText().toString().equalsIgnoreCase("")) {

                Image_load_activity.loadGooglePhoto(this, img_restaurant_image, Hotel_icon);
                edt_hoteltxt.setText(Hotel_name);
                txt_hotel_name.setText(Hotel_name);
                txt_hotel_address.setText(Hotel_location);

                ll_htl_layout.setVisibility(View.VISIBLE);
                rv_img_restaurant.setVisibility(View.GONE);
                img_restaurant_image.setVisibility(View.VISIBLE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        txt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edt_hotel_result = edt_hoteltxt.getText().toString();

                if (!edt_hotel_result.equalsIgnoreCase("")) {
                    location_search(edt_hotel_result);
                }

            }
        });


//        ambience_images();


    }


    //Restaurant location search
    private void location_search(String businessName) {

        apiService = APIClient_restaurant.getClient().create(API_interface_restaurant.class);

        Call<Restaurant_output_pojo> call = apiService.location_search("restaurants" + "+" + "in" + "+" + businessName, APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<Restaurant_output_pojo>() {
            @Override
            public void onResponse(Call<Restaurant_output_pojo> call, Response<Restaurant_output_pojo> response) {
                Restaurant_output_pojo restaurantOutputPojo = response.body();


                if (response.isSuccessful()) {

                    next_token = restaurantOutputPojo.getNextPageToken();
                    Log.e("token_location", "onResponse" + next_token);

                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {


                        storeModels = new ArrayList<>();
                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();


                        Log.e("hotel_size", "" + restaurantOutputPojo.getResults().size());

                        for (int i = 0; i < restaurantOutputPojo.getResults().size(); i++) {

//                            Getall_restaurant_details_pojo info = results.get(i).getResults().get(0);


                                  */
/* Getall_restaurant_details_pojo getallRestaurantDetailsPojo= new  Getall_restaurant_details_pojo(geometryPojo,"","","",
                                           openingHoursPojo,photos,"",0,"","",types,"");*//*


                            String placeid = restaurantOutputPojo.getResults().get(i).getPlaceId();

                            String Icon = restaurantOutputPojo.getResults().get(i).getIcon();
                            String hotel_name = restaurantOutputPojo.getResults().get(i).getName();
                            String Id = restaurantOutputPojo.getResults().get(i).getId();

                            String reference = restaurantOutputPojo.getResults().get(i).getReference();

                            String address = restaurantOutputPojo.getResults().get(i).getFormatted_address();
                            String scope = restaurantOutputPojo.getResults().get(i).getScope();

                            String rating = restaurantOutputPojo.getResults().get(i).getRating();
                            try {
                                String photo_ref = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getPhotoReference();
                                int width = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getWidth();
                                int height = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getHeight();

                                Get_hotel_photo_pojo getHotelPhotoPojo1 = new Get_hotel_photo_pojo(height, htmlAttributions, photo_ref, width);
                                Log.e("height1", "photo_ref1: " + photo_ref);
                                Log.e("height1", "width1: " + width);
                                Log.e("height1", "height1: " + height);
                                photos.add(getHotelPhotoPojo1);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            }


                            for (int kk = 0; kk < restaurantOutputPojo.getResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantOutputPojo.getResults().get(i).getTypes().toString());
                            }
                            try {
                                double lat = restaurantOutputPojo.getResults().get(0).getGeometry().getLocation().getLatitude();
                                double longit = restaurantOutputPojo.getResults().get(0).getGeometry().getLocation().getLongitude();


                                */
/*
                                geometryPojo = new Geometry_pojo(lat,longit);*//*

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                boolean open_status = restaurantOutputPojo.getResults().get(i).getOpeningHours().getOpenNow();
                                Log.e("open_status", "open_status: " + open_status);

                                openingHoursPojo = new OpeningHours_pojo(open_status, weekdayText);
                                openingHoursPojoList.add(openingHoursPojo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            GetAllRestaurantsPojo getallRestaurantDetailsPojo1 = new GetAllRestaurantsPojo(geometryPojo, Icon, Id,
                                    hotel_name, openingHoursPojo, photos, placeid, rating, reference, scope, types, address);
                            getallRestaurantDetailsPojoArrayList.add(getallRestaurantDetailsPojo1);

                            Review_inside_adapter adapterStores = new Review_inside_adapter(Review_test_activity.this, getallRestaurantDetailsPojoArrayList, storeModels, openingHoursPojoList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(Review_test_activity.this, LinearLayoutManager.HORIZONTAL, true);
                            rv_img_restaurant.setLayoutManager(mLayoutManager);
                            mLayoutManager.setStackFromEnd(true);
                            mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                            rv_img_restaurant.setAdapter(adapterStores);
                            rv_img_restaurant.setItemAnimator(new DefaultItemAnimator());
                            rv_img_restaurant.setNestedScrollingEnabled(false);
                            rv_img_restaurant.setHorizontalScrollBarEnabled(true);
                            adapterStores.notifyDataSetChanged();

                            img_restaurant_image.setVisibility(View.GONE);
                            txt_hotel_name.setVisibility(View.GONE);
                            txt_hotel_address.setVisibility(View.GONE);

                            rv_img_restaurant.setVisibility(View.VISIBLE);
                            ll_htl_layout.setVisibility(View.VISIBLE);


//                            fetchDistance(getallRestaurantDetailsPojo);


                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<Restaurant_output_pojo> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });


    }


    //Custom alert

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:
                finish();
                break;
            case R.id.image_ambiance:

                Review_test_activity.CustomDialogClass ccdambience = new Review_test_activity.CustomDialogClass(this);
                ccdambience.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccdambience.setCancelable(true);
                ccdambience.show();

//                startActivity(new Intent(Review_test_activity.this, Ambience_take_photo.class));
                break;

            case R.id.txt_ambiance_add:

                Review_test_activity.CustomDialogClass ccdambience2 = new Review_test_activity.CustomDialogClass(this);
                ccdambience2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccdambience2.setCancelable(true);
                ccdambience2.show();

//                startActivity(new Intent(Review_test_activity.this, Ambience_take_photo.class));
                break;


            case R.id.img_restaurant_image:

                 */
/*   View popupView2 = LayoutInflater.from(Review_test_activity.this).inflate(R.layout.review_popup_layout, null);
                    final PopupWindow popupWindow2 = new PopupWindow(popupView2, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    popupWindow2.showAsDropDown(popupView2, 0, 0);
                    done_btn = popupView2.findViewById(R.id.done_btn);
                    img_food = popupView2.findViewById(R.id.img_food);
                    img_close = popupView2.findViewById(R.id.img_close);
                    img_ok = popupView2.findViewById(R.id.img_ok);
                    edt_food_caption = popupView2.findViewById(R.id.edt_food_caption);


                    img_close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            popupWindow2.dismiss();
                        }
                    });
*//*



                break;


            case R.id.btn_addImages:
//                OpenImage();
//                OpenGallery();
//                startDialog();
                startActivity(new Intent(Review_test_activity.this, GalleryImage_Review.class));
                break;

            case R.id.image_add_top_dish:

                Intent intent = new Intent(Review_test_activity.this, Review_take_photo.class);
                intent.putExtra("Hotel_name", Hotel_name);
                intent.putExtra("for_top_dish", "1");
                intent.putExtra("Hotel_icon", Hotel_icon);
                intent.putExtra("Hotel_rating", Hotel_rating);
                intent.putExtra("Hotel_location", Hotel_location);
                intent.putExtra("placeid", place_id);
                intent.putExtra("googleid", google_id);
                startActivity(intent);

        */
/*
               CustomDialogClass ccdtop = new CustomDialogClass(this);
                ccdtop.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccdtop.setCancelable(true);
                ccdtop.show();*//*

                break;

            case R.id.image_add_dish_worst:

                Intent intent3 = new Intent(Review_test_activity.this, Review_take_photo.class);
                intent3.putExtra("Hotel_name", Hotel_name);
                intent3.putExtra("for_worst_dish", "2");
                intent3.putExtra("Hotel_icon", Hotel_icon);
                intent3.putExtra("Hotel_rating", Hotel_rating);
                intent3.putExtra("Hotel_location", Hotel_location);
                intent3.putExtra("placeid", place_id);
                intent3.putExtra("googleid", google_id);


                startActivity(intent3);

             */
/* CustomDialogClass ccdworst = new CustomDialogClass(this);
                ccdworst.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccdworst.setCancelable(true);
                ccdworst.show();*//*

                break;

      */
/*      case R.id.image_add:
                Review_test_activity.CustomDialogClass ccd2 = new Review_test_activity.CustomDialogClass(this);
                ccd2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccd2.setCancelable(true);
                ccd2.show();
                break;*//*


            case R.id.img_reset:
                txt_rating_low.setText("0");
                String res = tagsedittext.getText().toString();


                break;

            case R.id.img_reset_ambiance:
                txt_rating_low_ambiance.setText("0");

                break;

            case R.id.img_reset_serive:
                txt_rating_low_service.setText("0");
                break;

            case R.id.img_reset_taste:
                txt_rating_low_taste.setText("0");
                break;

            case R.id.img_reset_value:
                txt_rating_low_value.setText("0");
                break;
        }

    }

    public void addListenerOnRatingBar() {

        //if rating value is changed,
        //display the current rating value in the result (textview) automatically
        ratingBar.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
              */
/*  if(level==2){
                    ratingBar.setNormalColor(getResources().getColor(R.color.colorPrimary));
                } else  if(level==3){
                    ratingBar.setNormalColor(getResources().getColor(R.color.colorPrimary));
                }else  if(level==4){
                    ratingBar.setNormalColor(getResources().getColor(R.color.colorPrimary));
                }else  if(level==5){
                    ratingBar.setNormalColor(getResources().getColor(R.color.dark_green));
                }*//*


                txt_rating_low.setText(String.valueOf(level));

                overall = Integer.parseInt(txt_rating_low.getText().toString());

            }
        });

        rtng_ambiance.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_ambiance.setText(String.valueOf(level));

                ambiance = Integer.parseInt(txt_rating_low_ambiance.getText().toString());
            }
        });


        rtng_service.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_service.setText(String.valueOf(level));
                service = Integer.parseInt(txt_rating_low_service.getText().toString());
            }
        });
        rtng_taste.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_taste.setText(String.valueOf(level));
                taste = Integer.parseInt(txt_rating_low_taste.getText().toString());
            }
        });
        rtng_value.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_value.setText(String.valueOf(level));
                value_money = Integer.parseInt(txt_rating_low_value.getText().toString());

            }
        });

    }


    public void create_review() {
        if (isConnected(Review_test_activity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String createuserid = Pref_storage.getDetail(Review_test_activity.this, "userId");

            if (dish_image == null) {
                image = Hotel_icon;
            } else {
                image = dish_image;
            }


            Call<Create_review_output_pojo> call = apiService.create_hotel_review("create_hotel_review", google_id, Hotel_name, edt_review_description.getText().toString(), category, veg_nonveg,
                    place_id, "", Hotel_location, 0.0, 0.0, 0, 0, image, Hotel_icon, overall, ambiance, taste, service,value_money, Integer.parseInt(createuserid));


            call.enqueue(new Callback<Create_review_output_pojo>() {
                @Override
                public void onResponse(Call<Create_review_output_pojo> call, Response<Create_review_output_pojo> response) {

                    if (response.body().getResponseCode() == 1) {

                        //  Log.e("review-->", "size" + response.body().getData().size());

                        for (int j = 0; j < response.body().getData().size(); j++) {
                            String   hotel_id = response.body().getData().get(j).getHotelid();
                            String  review_id = response.body().getData().get(j).getReviewid();

                            Log.e("hotel_id", "" + hotel_id);
                            Log.e("review_id", "" + review_id);

//                            create_hotel_review_dish_image();
                            create_hotel_review_ambiance_image();
                            startActivity(new Intent(Review_test_activity.this,Home.class));
                            finish();
                        }

                    }

                }

                @Override
                public void onFailure(Call<Create_review_output_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                }
            });


        } else {
            Snackbar snackbar = Snackbar.make(ll_review, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }
    public void create_review_delivery() {
        if (isConnected(Review_test_activity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String createuserid = Pref_storage.getDetail(Review_test_activity.this, "userId");

            if (dish_image == null) {
                image = Hotel_icon;
            } else {
                image = dish_image;
            }


            Call<Create_review_output_pojo> call = apiService.create_review_delivery("create_hotel_review", google_id, Hotel_name, edt_review_description.getText().toString(), category, veg_nonveg,
                    place_id, "", Hotel_location, 0.0, 0.0, 0, 0, image, Hotel_icon, overall, ambiance, taste, service,1 ,value_money, Integer.parseInt(createuserid));


            call.enqueue(new Callback<Create_review_output_pojo>() {
                @Override
                public void onResponse(Call<Create_review_output_pojo> call, Response<Create_review_output_pojo> response) {

                    if (response.body().getResponseCode() == 1) {

                        //  Log.e("review-->", "size" + response.body().getData().size());

                        for (int j = 0; j < response.body().getData().size(); j++) {
                            String   hotel_id = response.body().getData().get(j).getHotelid();
                            String  review_id = response.body().getData().get(j).getReviewid();

                            Log.e("hotel_id", "" + hotel_id);
                            Log.e("review_id", "" + review_id);

//                            create_hotel_review_dish_image();
                            create_hotel_review_ambiance_image();
                            startActivity(new Intent(Review_test_activity.this,Home.class));
                            finish();
                        }

                    }

                }

                @Override
                public void onFailure(Call<Create_review_output_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                }
            });


        } else {
            Snackbar snackbar = Snackbar.make(ll_review, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }

 */
/*   public void create_hotel_review_dish_image() {
        if (isConnected(Review_test_activity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String createuserid = Pref_storage.getDetail(Review_test_activity.this, "userId");

            sourceFile = new File(dish_image);

            RequestBody imageupload = RequestBody.create(MediaType.parse("image/jpeg"), sourceFile);

            RequestBody dishname = RequestBody.create(MediaType.parse("text/plain"), dish_name);
            Log.e("sourceFile", "" + sourceFile.getName());

            MultipartBody.Part image = MultipartBody.Part.createFormData("image", sourceFile.getName(), imageupload);

            Call<CommonOutputPojo> call = apiService.create_hotel_review_dish_image("create_hotel_review_dish_image", hotel_id, review_id, dishname, 1, Integer.parseInt(createuserid), image);


            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    if (response.body().getResponseCode() == 1) {
                        finish();

                    }
                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                }
            });


        } else {
            Snackbar snackbar = Snackbar.make(ll_review, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }*//*



    public void create_hotel_review_ambiance_image() {
        if (isConnected(Review_test_activity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


          */
/*  if (ambience_camera == null) {
                photo_upload = ambience_gallery;

                ambi_image = new ArrayList<String>(Arrays.asList(photo_upload));


                Log.e(TAG, "gallery" + photo_upload);
            } else if (!ambience_camera.equalsIgnoreCase("")) {
                photo_upload = ambience_camera;

                Log.e(TAG, "camera" + photo_upload);

            } else if (ambience_gallery == null) {
                photo_upload = ambience_camera;
            }
*//*


            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);


            builder.addFormDataPart("hotel_id", String.valueOf(hotel_id));
            builder.addFormDataPart("review_id", String.valueOf(hotel_id));

            //UserID
            String createdby = Pref_storage.getDetail(this, "userId");
            builder.addFormDataPart("created_by", createdby);

            //Image to upload
            for (int i = 0; i < ambience_image_List.size(); i++) {
                Log.e(TAG, "filePaths_new" + ambience_image_List.toString());


                file = new File(ambience_image_List.get(i));

                Log.e(TAG, "Multi_image_file" + file.getName());
                Log.e(TAG, "Multi_image_name" + file.toString());


                builder.addFormDataPart("image", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));

                Log.e(TAG, "filevishnu" + file.getName());

                MultipartBody requestBody = builder.build();
                Call<CommonOutputPojo> call = apiService.create_hotel_review_ambiance_image("create_hotel_review_ambiance_image", requestBody);


                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1) {
                            finish();

                        }
                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());

                    }
                });

            }
        } else {
            Snackbar snackbar = Snackbar.make(ll_review, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {


            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && data != null) {


                imagesEncodedList = new ArrayList<String>();
                ambi_review_image = new ArrayList<String>();


                String[] filePathColumn = {MediaStore.Images.Media.DATA};



                if (data.getData() != null) {

                    Uri mImageUri = data.getData();
                    Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                    cursor.moveToFirst();
                    String document_id = cursor.getString(0);
                    document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                    cursor.close();

                    cursor = getContentResolver().query(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                    cursor.moveToFirst();
                    String single_imagePath= cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    cursor.close();

                    Log.e("Gallery-->","Single"+single_imagePath);



                    ambi_review_image.add(single_imagePath);

                    ambi_viewpager_review.setAdapter(new Review_ambience_slide_adapter(getApplicationContext(), ambi_review_image,this));

                    ambi_review_indicator.setViewPager(ambi_viewpager_review);
                    ambi_viewpager_review.setVisibility(View.VISIBLE);
                    ambi_review_indicator.setVisibility(View.VISIBLE);
                    rl_ambiance_dotindicator.setVisibility(View.VISIBLE);
                    NUM_PAGES_AMB = ambi_review_image.size();

                    // Auto start of viewpager
                    final Handler handler = new Handler();
                    final Runnable Update = new Runnable() {
                        public void run() {
                            if (currentPage_amb == NUM_PAGES_AMB) {
                                currentPage_amb = 0;
                            }
                            ambi_viewpager_review.setCurrentItem(currentPage_amb++, true);
                        }
                    };


                    // Pager listener over indicator
                    ambi_review_indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                        @Override
                        public void onPageSelected(int position) {
                            currentPage_amb = position;

                        }

                        @Override
                        public void onPageScrolled(int pos, float arg1, int arg2) {

                        }

                        @Override
                        public void onPageScrollStateChanged(int pos) {

                        }
                    });


                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            // Get the cursor

                            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                            cursor.moveToFirst();
                            String document_id = cursor.getString(0);
                            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                            cursor.close();

                            cursor = getContentResolver().query(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                            cursor.moveToFirst();
                         String multi_imagepath= cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));


                            cursor.close();

                            Log.e("Gallery_ambience-->","Multi"+multi_imagepath);


                            ambi_review_image.add(multi_imagepath);
                            ambi_viewpager_review.setAdapter(new Review_ambience_slide_adapter(getApplicationContext(), ambi_review_image,this));
                            ambi_review_indicator.setViewPager(ambi_viewpager_review);
                            ambi_viewpager_review.setVisibility(View.VISIBLE);
                            ambi_review_indicator.setVisibility(View.VISIBLE);
                            rl_ambiance_dotindicator.setVisibility(View.VISIBLE);
                            NUM_PAGES_AMB = ambi_review_image.size();

                            // Auto start of viewpager

                            final Handler handler = new Handler();
                            final Runnable Update = new Runnable() {
                                public void run() {
                                    if (currentPage_amb == NUM_PAGES_AMB) {
                                        currentPage_amb = 0;
                                    }
                                    ambi_viewpager_review.setCurrentItem(currentPage_amb++, true);
                                }
                            };


                            // Pager listener over indicator
                            ambi_review_indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                                @Override
                                public void onPageSelected(int position) {
                                    currentPage_amb = position;

                                }

                                @Override
                                public void onPageScrolled(int pos, float arg1, int arg2) {

                                }

                                @Override
                                public void onPageScrollStateChanged(int pos) {

                                }
                            });



                        }
                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }
                }
            }else if (requestCode == CAPTURE_IMAGE_CALLBACK && resultCode == RESULT_OK && data != null) {

                try {
                    // TODO Auto-generated method stub

                    OutputStream output;
                    // Find the SD Card path
                 */
/*   File filepath = Environment.getExternalStorageDirectory();
                    // Create a new folder in SD Card
                    File dir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES));
                    dir.mkdirs();

                    String imge_name = "FoodWall_review" + System.currentTimeMillis()
                            + ".jpg";*//*

                    // Create a name for the saved image
                    File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "FoodWall_review_IMG" + System.currentTimeMillis()
                            + ".jpg");
                    if (file.exists()) {
                        file.delete();
                        file.createNewFile();
                    } else {
                        file.createNewFile();

                    }

                    try {

                        output = new FileOutputStream(file);

                      */
/*  // Compress into png format image from 0% - 100%
                        processedBitmap.compress(Bitmap.CompressFormat.PNG, 100, output);*//*

                        output.flush();
                        output.close();
                        int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));

                        selectedImagePath = String.valueOf(file);

                        Log.e("Gallery_ambience-->","camera"+selectedImagePath);



                        ambi_review_image.add(selectedImagePath);

                        ambi_viewpager_review.setAdapter(new Review_ambience_slide_adapter(getApplicationContext(), ambi_review_image,this));

                        ambi_review_indicator.setViewPager(ambi_viewpager_review);
                        ambi_viewpager_review.setVisibility(View.VISIBLE);
                        ambi_review_indicator.setVisibility(View.VISIBLE);
                        rl_ambiance_dotindicator.setVisibility(View.VISIBLE);

                        final float density = getApplicationContext().getResources().getDisplayMetrics().density;

//        indicator.setRadius(5 * density);
                        NUM_PAGES_AMB = ambi_review_image.size();


                        // Auto start of viewpager

                        final Handler handler = new Handler();
                        final Runnable Update = new Runnable() {
                            public void run() {
                                if (currentPage_amb == NUM_PAGES_AMB) {
                                    currentPage_amb = 0;
                                }
                                ambi_viewpager_review.setCurrentItem(currentPage_amb++, true);
                            }
                        };



                        // Pager listener over indicator
                        ambi_review_indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                            @Override
                            public void onPageSelected(int position) {
                                currentPage_amb = position;

                            }

                            @Override
                            public void onPageScrolled(int pos, float arg1, int arg2) {

                            }

                            @Override
                            public void onPageScrollStateChanged(int pos) {

                            }
                        });

                        Log.e("imagepath2", "imagepath2: " + selectedImagePath);


                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }



            */
/*else if (requestCode == CAPTURE_IMAGE_CALLBACK && resultCode == RESULT_OK && data != null) {

                try {
                    // TODO Auto-generated method stub

                    OutputStream output;

                    // Create a name for the saved image
                    File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "Foodwall_review_photo-" +System.currentTimeMillis()+".jpg");
                    if (file.exists()) {
                        file.delete();
                        file.createNewFile();
                    } else {
                        file.createNewFile();

                    }

                    try {

                        output = new FileOutputStream(file);

                      *//*
*/
/*  // Compress into png format image from 0% - 100%
                        processedBitmap.compress(Bitmap.CompressFormat.PNG, 100, output);*//*
*/
/*
                        output.flush();
                        output.close();
                        int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));

                        selectedImagePath = String.valueOf(file);

                        Log.e("Gallery_ambience-->","camera"+selectedImagePath);



                        ambi_review_image.add(selectedImagePath);

                        ambi_viewpager_review.setAdapter(new Review_ambience_slide_adapter(getApplicationContext(), ambi_review_image));

                        ambi_review_indicator.setViewPager(ambi_viewpager_review);
                        ambi_viewpager_review.setVisibility(View.VISIBLE);
                        ambi_review_indicator.setVisibility(View.VISIBLE);
                        rl_ambiance_dotindicator.setVisibility(View.VISIBLE);

                        final float density = getApplicationContext().getResources().getDisplayMetrics().density;


                        NUM_PAGES_AMB = ambi_review_image.size();


                        // Auto start of viewpager

                        final Handler handler = new Handler();
                        final Runnable Update = new Runnable() {
                            public void run() {
                                if (currentPage_amb == NUM_PAGES_AMB) {
                                    currentPage_amb = 0;
                                }
                                ambi_viewpager_review.setCurrentItem(currentPage_amb++, true);
                            }
                        };



                        // Pager listener over indicator
                        ambi_review_indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                            @Override
                            public void onPageSelected(int position) {
                                currentPage_amb = position;

                            }

                            @Override
                            public void onPageScrolled(int pos, float arg1, int arg2) {

                            }

                            @Override
                            public void onPageScrollStateChanged(int pos) {

                            }
                        });


                        Log.e("imagepath2", "imagepath2: " + selectedImagePath);



                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }*//*
 else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {

        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getApplicationContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);

            cursor.close();

        }
        return result;

    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getApplicationContext().getContentResolver().query(contentUri, proj, "", null, "");
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
            Toast.makeText(this, "" + res, Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        return res;


    }

    public String getpathofuri(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }








    private void ambience_images(){

        ambi_viewpager_review.setAdapter(new Review_ambience_slide_adapter(getApplicationContext(), ambi_review_image,this));

        ambi_review_indicator.setViewPager(ambi_viewpager_review);

        final float density = getApplicationContext().getResources().getDisplayMetrics().density;

//        indicator.setRadius(5 * density);
        NUM_PAGES_AMB = ambi_review_image.size();


        // Auto start of viewpager

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage_amb == NUM_PAGES_AMB) {
                    currentPage_amb = 0;
                }
                ambi_viewpager_review.setCurrentItem(currentPage_amb++, true);
            }
        };

       */
/* Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
*//*


        // Pager listener over indicator
        ambi_review_indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage_amb = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }








    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            autoCompleteTextView.setVisibility(View.GONE);
            ll_edt_add_dish.setVisibility(View.GONE);
        }

        if (position == 1) {
            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);
        } else if (position == 2) {
            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        autoCompleteTextView.setVisibility(View.GONE);
        ll_edt_add_dish.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            autoCompleteTextView.setVisibility(View.GONE);
            ll_edt_add_dish.setVisibility(View.GONE);
        }

        if (position == 1) {
            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);
        } else if (position == 2) {
            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void removedImageLisenter(List<String> imagesList) {


    }

    @Override
    public void removedTopImageLisenter(ArrayList<Review_image_pojo> imagesList) {

    }

    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public AppCompatActivity c;
        public Dialog d;
        public AppCompatButton btn_Ok;
        TextView txt_open_camera, txt_open_gallery;

        CustomDialogClass(AppCompatActivity a) {
            super(a);
            // TODO Auto-generated constructor stub
            c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
//            setContentView(R.layout.dialog_version_info);

            setContentView(R.layout.select_picture_layout_for_review);

            txt_open_camera = (TextView) findViewById(R.id.txt_open_camera);
            txt_open_gallery = (TextView) findViewById(R.id.txt_open_gallery);

            txt_open_camera.setOnClickListener(this);
            txt_open_gallery.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int click = v.getId();
            switch (click) {

                case R.id.txt_open_camera:

                    try {

                        dismiss();

                        if (ActivityCompat.checkSelfPermission(Review_test_activity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(Review_test_activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(Review_test_activity.this, new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAPTURE_IMAGE_CALLBACK);

                        }
                        else {

                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, CAPTURE_IMAGE_CALLBACK);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                    startActivity(new Intent(Review_test_activity.this, Review_take_photo.class));
                    break;


                case R.id.txt_open_gallery:
                    dismiss();
                    try {

                        if (ActivityCompat.checkSelfPermission(Review_test_activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(Review_test_activity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_MULTIPLE);

                        } else {

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.addFlags( Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION );
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case R.id.btn_create_review:

                */
/*    if(overall!=0&&taste!=0&&ambiance!=0&&service!=0&&value_money!=0){
                        create_review();
                    }else {
                        Toast.makeText(getApplicationContext(), "Please provide all ratings to proceed.", Toast.LENGTH_SHORT).show();
                    }*//*



                    break;


            }
        }
    }


}

*/
