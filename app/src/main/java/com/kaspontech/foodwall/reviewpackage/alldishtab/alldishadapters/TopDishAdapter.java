package com.kaspontech.foodwall.reviewpackage.alldishtab.alldishadapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_all_input;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_image_pojo;
import com.kaspontech.foodwall.R;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

public class TopDishAdapter extends RecyclerView.Adapter<TopDishAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {

    /**
     * Context
     */
    private Context context;

    /**
     * Top dish arraylist
     */
    private ArrayList<Get_dish_type_all_input> getDishTypeAllInputArrayList = new ArrayList<>();
    /**
     * Top dish image arraylist
     */
    private ArrayList<Get_dish_type_image_pojo> getDishTypeImagePojoArrayList = new ArrayList<>();

    /**
     * Top dish input arraylist
     */
    private Get_dish_type_all_input getDishTypeAllInput;

    /**
     * Top dish image arraylist
     */
    private Get_dish_type_image_pojo getDishTypeImagePojo;


    /**
     * Constructor
     */
    public TopDishAdapter(Context c, ArrayList<Get_dish_type_all_input> gettinglist, ArrayList<Get_dish_type_image_pojo> gettingimglist) {
        this.context = c;
        this.getDishTypeAllInputArrayList = gettinglist;
        this.getDishTypeImagePojoArrayList = gettingimglist;

    }


    @NonNull
    @Override
    public TopDishAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dishes_image_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        TopDishAdapter.MyViewHolder vh = new TopDishAdapter.MyViewHolder(v);


        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final TopDishAdapter.MyViewHolder holder, final int position) {

        /*Top dish input pojo initialization*/
        getDishTypeAllInput = getDishTypeAllInputArrayList.get(position);

        /*Top dish image pojo initialization*/
        getDishTypeImagePojo = getDishTypeImagePojoArrayList.get(position);

        /*Image loading*/

        if(!getDishTypeImagePojoArrayList.get(position).getImg().isEmpty()||!getDishTypeImagePojoArrayList.get(position).getImg().equalsIgnoreCase("")||getDishTypeImagePojoArrayList.get(position).getImg()!=null){
            GlideApp.with(context)
                    .load(getDishTypeImagePojoArrayList.get(position).getImg())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.01f)
                    .centerCrop()
                    .into(holder.imageReview);

        }else {

            /*Remove postion*/

            removeat(position);

        }

        /*Dish name display*/
        holder.txtImage.setText(getDishTypeAllInputArrayList.get(position).getDishName());

    }

    /*Remove postion*/

    private void removeat(int position){

        getDishTypeImagePojoArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getDishTypeImagePojoArrayList.size());

    }

    @Override
    public int getItemCount() {
        return getDishTypeAllInputArrayList.size();

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        /* TextView */

        TextView txtImage;

        /* Imageview */

        PhotoView imageReview;


        public MyViewHolder(View itemView) {
            super(itemView);

            txtImage = (TextView) itemView.findViewById(R.id.txt_image);
            imageReview = (PhotoView) itemView.findViewById(R.id.image_review);

        }


    }


}
