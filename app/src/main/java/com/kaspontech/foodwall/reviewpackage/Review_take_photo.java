package com.kaspontech.foodwall.reviewpackage;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.cameraview.CameraView;
import com.kaspontech.foodwall.foodFeedsPackage.TakePhoto;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static com.kaspontech.foodwall.reviewpackage.New_review_activity.tagsedittext;
import static com.kaspontech.foodwall.reviewpackage.WriteReviewActivity.img_addimage;
import static com.kaspontech.foodwall.reviewpackage.WriteReviewActivity.ll_image_review;

public class Review_take_photo extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = "Review_take_photo";

    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_GALLERY_PERMISSION = 2;
    private static final String FRAGMENT_DIALOG = "dialog";
    private static final int[] FLASH_OPTIONS = {
            CameraView.FLASH_AUTO,
            CameraView.FLASH_OFF,
            CameraView.FLASH_ON,
    };
    private static final int[] FLASH_ICONS = {
            R.drawable.ic_flash_auto,
            R.drawable.ic_flash_off,
            R.drawable.ic_flash_on,
    };
    private static final int[] FLASH_TITLES = {
            R.string.flash_auto,
            R.string.flash_off,
            R.string.flash_on,
    };
    public String imagepath;
    Bitmap imagebm;
    Pref_storage pref_storage;
    String selectedImagePath, for_top_dish, for_worst_dish,
            Hotel_name, Hotel_location, Hotel_icon, Hotel_rating, place_id, google_id, image, dish_name, dish_image;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private int mCurrentFlash;

    private CameraView mCameraView;
    private FloatingActionButton mfloataction;


    private Handler mBackgroundHandler;

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.take_picture:

                    if (mCameraView != null) {
                        mCameraView.takePicture();
                    }
                    break;

              /*  case R.id.switch_gallery:
                    galleryIntent();
               break;*/
            }
        }
    };
    private CameraView.Callback mCallback = new CameraView.Callback() {

        @Override
        public void onCameraOpened(CameraView cameraView) {
            Log.d(TAG, "onCameraOpened");
        }

        @Override
        public void onCameraClosed(CameraView cameraView) {
            Log.d(TAG, "onCameraClosed");
        }

        @Override
        public void onPictureTaken(CameraView cameraView, final byte[] data) {
            Log.d(TAG, "onPictureTaken " + data.length);

            getBackgroundHandler().post(new Runnable() {
                @Override
                public void run() {

                    File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "Foodwall_review_photo1.jpg");
                    imagepath = String.valueOf(file);
                    OutputStream os = null;
                    try {
                        file.createNewFile();
                        os = new FileOutputStream(file);
                        os.write(data);
                        os.flush();
                        os.close();


                        if (for_worst_dish == null) {
                            Intent intent = new Intent(Review_take_photo.this, Dish_select_view_activity.class);
                            String dish = for_top_dish;
                            intent.putExtra("dish_image", imagepath);
                            intent.putExtra("for_top", dish);
                            intent.putExtra("dish_name", tagsedittext.getText().toString());
                            intent.putExtra("Hotel_name", Hotel_name);
                            intent.putExtra("Hotel_icon", Hotel_icon);
                            intent.putExtra("Hotel_rating", Hotel_rating);
                            intent.putExtra("Hotel_location", Hotel_location);
                            intent.putExtra("placeid", place_id);
                            intent.putExtra("googleid", google_id);
                            startActivity(intent);
                            finish();

                        } else {
                            Intent intent = new Intent(Review_take_photo.this, Dish_select_view_activity.class);
                            String dish = for_worst_dish;
                            intent.putExtra("dish_image", imagepath);
                            intent.putExtra("for_worst", dish);
                            intent.putExtra("dish_name", tagsedittext.getText().toString());
                            intent.putExtra("Hotel_name", Hotel_name);
                            intent.putExtra("Hotel_icon", Hotel_icon);
                            intent.putExtra("Hotel_rating", Hotel_rating);
                            intent.putExtra("Hotel_location", Hotel_location);
                            intent.putExtra("placeid", place_id);
                            intent.putExtra("googleid", google_id);
                            startActivity(intent);
                            finish();

                        }





/*

                        Intent intent = new Intent(Review_take_photo.this, Dish_select_view_activity.class);
                        intent.putExtra("dish_image", imagepath);
                        intent.putExtra("dish_name", tagsedittext.getText().toString());
                        intent.putExtra("Hotel_name", Hotel_name);
                        intent.putExtra("Hotel_icon", Hotel_icon);
                        intent.putExtra("Hotel_rating", Hotel_rating);
                        intent.putExtra("Hotel_location", Hotel_location);
                        intent.putExtra("placeid", place_id);
                        intent.putExtra("googleid", google_id);
                        startActivity(intent);
                        finish();
*/


                        /*   try {
                         */
//                            Toast.makeText(Review_take_photo.this, "hi\n"+imagepath, Toast.LENGTH_SHORT).show();

           /*                 Review_image_pojo reviewImagePojo = new Review_image_pojo(imagepath, "Biryani");
                            addimagesEncodedList.add(reviewImagePojo);
                            Review_image_top_dish_adapter reviewImageTopDishAdapter = new Review_image_top_dish_adapter(getApplicationContext(), addimagesEncodedList);
                            LinearLayoutManager llm = new LinearLayoutManager(Review_take_photo.this, LinearLayoutManager.HORIZONTAL, true);
                            rv_top_dish.setLayoutManager(llm);
                            rv_top_dish.setAdapter(reviewImageTopDishAdapter);
                            reviewImageTopDishAdapter.notifyDataSetChanged();*/
                        /*    finish();*/


                       /* } catch (Exception e) {
                            e.printStackTrace();
                        }
*/

                    } catch (IOException e) {
                        Log.w(TAG, "Cannot write to " + file, e);
                    } finally {
                        if (os != null) {
                            try {
                                os.close();
                            } catch (IOException e) {
                                // Ignore
                            }
                        }
                    }
                }
            });
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo);
        pref_storage = new Pref_storage();

        mCameraView = findViewById(R.id.camera);


        FloatingActionButton fab = findViewById(R.id.take_picture);
        FloatingActionButton fab2 = findViewById(R.id.switch_gallery);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryIntent();
            }
        });

        if (mCameraView != null) {
            mCameraView.addCallback(mCallback);
        }

        if (fab != null) {
            fab.setOnClickListener(mOnClickListener);
        }
       /*if (fab2 != null) {
           fab2.setOnClickListener(mOnClickListener);
        }*/

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        Intent intent = getIntent();
        Hotel_name = intent.getStringExtra("Hotel_name");
        for_top_dish = intent.getStringExtra("for_top_dish");
        for_worst_dish = intent.getStringExtra("for_worst_dish");
        Hotel_name = intent.getStringExtra("Hotel_name");
        Hotel_icon = intent.getStringExtra("Hotel_icon");
        Hotel_location = intent.getStringExtra("Hotel_location");
        Hotel_rating = intent.getStringExtra("Hotel_rating");
        place_id = intent.getStringExtra("placeid");
        google_id = intent.getStringExtra("googleid");

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            mCameraView.start();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            TakePhoto.ConfirmationDialogFragment
                    .newInstance(R.string.camera_permission_confirmation,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION,
                            R.string.camera_permission_not_granted)
                    .show(getSupportFragmentManager(), FRAGMENT_DIALOG);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    @Override
    protected void onPause() {
        mCameraView.stop();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBackgroundHandler != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mBackgroundHandler.getLooper().quitSafely();
            } else {
                mBackgroundHandler.getLooper().quit();
            }
            mBackgroundHandler = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (permissions.length != 1 || grantResults.length != 1) {
                    throw new RuntimeException("Error on requesting camera permission.");
                }
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, R.string.camera_permission_not_granted,
                            Toast.LENGTH_SHORT).show();
                }
                // No need to start camera here; it is handled by onResume
                break;

          /*
                case REQUEST_GALLERY_PERMISSION:
                if (permissions.length != 1 || grantResults.length != 1) {
                    throw new RuntimeException("Error on requesting camera permission.");
                }
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                }
                // No need to start camera here; it is handled by onResume
                break;*/
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.food_feeds_camera_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.switch_flash:
                if (mCameraView != null) {
                    mCurrentFlash = (mCurrentFlash + 1) % FLASH_OPTIONS.length;
                    item.setTitle(FLASH_TITLES[mCurrentFlash]);
                    item.setIcon(FLASH_ICONS[mCurrentFlash]);
                    mCameraView.setFlash(FLASH_OPTIONS[mCurrentFlash]);
                }
                return true;

            case R.id.switch_camera:
                if (mCameraView != null) {
                    int facing = mCameraView.getFacing();
                    mCameraView.setFacing(facing == CameraView.FACING_FRONT ?
                            CameraView.FACING_BACK : CameraView.FACING_FRONT);
                }
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    private void galleryIntent() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_GALLERY_PERMISSION);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            /*   intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);*/
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FILE);

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        if (resultCode == Activity.RESULT_OK && requestCode == SELECT_FILE && data != null) {

            Uri mImageUri = data.getData();

            Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
            cursor.close();

            cursor = getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            cursor.moveToFirst();
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();

            if (for_worst_dish == null) {
               Intent intent = new Intent(Review_take_photo.this, Dish_select_view_activity.class);
                String dish = for_top_dish;
                intent.putExtra("dish_image", path);
                intent.putExtra("for_top", dish);
                intent.putExtra("dish_name", tagsedittext.getText().toString());
                intent.putExtra("Hotel_name", Hotel_name);
                intent.putExtra("Hotel_icon", Hotel_icon);
                intent.putExtra("Hotel_rating", Hotel_rating);
                intent.putExtra("Hotel_location", Hotel_location);
                intent.putExtra("placeid", place_id);
                intent.putExtra("googleid", google_id);
                startActivity(intent);
                finish();

            } else {
                Intent intent = new Intent(Review_take_photo.this, Dish_select_view_activity.class);
                String dish = for_worst_dish;
                intent.putExtra("dish_image", path);
                intent.putExtra("for_worst", dish);
                intent.putExtra("dish_name", tagsedittext.getText().toString());
                intent.putExtra("Hotel_name", Hotel_name);
                intent.putExtra("Hotel_icon", Hotel_icon);
                intent.putExtra("Hotel_rating", Hotel_rating);
                intent.putExtra("Hotel_location", Hotel_location);
                intent.putExtra("placeid", place_id);
                intent.putExtra("googleid", google_id);
                startActivity(intent);
                finish();

            }

      /*      Review_image_pojo reviewImagePojo = new Review_image_pojo(path, "Biryani");
            addimagesEncodedList.add(reviewImagePojo);
            Review_image_top_dish_adapter reviewImageTopDishAdapter = new Review_image_top_dish_adapter(getApplicationContext(), addimagesEncodedList);
            LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, true);
            rv_top_dish.setLayoutManager(llm);
            rv_top_dish.setAdapter(reviewImageTopDishAdapter);
            reviewImageTopDishAdapter.notifyDataSetChanged();
            finish();*/

        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Uri mImageUri = data.getData();

        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(mImageUri, filePathColumn, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        img_addimage.setImageURI(Uri.parse(path));
        img_addimage.setVisibility(View.VISIBLE);
        ll_image_review.setVisibility(View.VISIBLE);
//        img_status_timeline.setImageBitmap(BitmapFactory.decodeFile(picturePath));

        Pref_storage.setDetail(getApplicationContext(), "User_review_Image", path);


    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getApplicationContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
//            Toast.makeText(mContext, "vis"+result, Toast.LENGTH_SHORT).show();
            cursor.close();
        }
        return result;

    }

    private Handler getBackgroundHandler() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    public static class ConfirmationDialogFragment extends DialogFragment {

        private static final String ARG_MESSAGE = "message";
        private static final String ARG_PERMISSIONS = "permissions";
        private static final String ARG_REQUEST_CODE = "request_code";
        private static final String ARG_NOT_GRANTED_MESSAGE = "not_granted_message";

        public static TakePhoto.ConfirmationDialogFragment newInstance(@StringRes int message, String[] permissions, int requestCode, @StringRes int notGrantedMessage) {
            TakePhoto.ConfirmationDialogFragment fragment = new TakePhoto.ConfirmationDialogFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_MESSAGE, message);
            args.putStringArray(ARG_PERMISSIONS, permissions);
            args.putInt(ARG_REQUEST_CODE, requestCode);
            args.putInt(ARG_NOT_GRANTED_MESSAGE, notGrantedMessage);
            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(args.getInt(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String[] permissions = args.getStringArray(ARG_PERMISSIONS);
                                    if (permissions == null) {
                                        throw new IllegalArgumentException();
                                    }
                                    ActivityCompat.requestPermissions(getActivity(),
                                            permissions, args.getInt(ARG_REQUEST_CODE));
                                }
                            })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity(),
                                            args.getInt(ARG_NOT_GRANTED_MESSAGE),
                                            Toast.LENGTH_SHORT).show();
                                }
                            })
                    .create();
        }

    }
}