package com.kaspontech.foodwall.reviewpackage.restaurantSearch;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.historicalMapPackage.CreateHistoricalMap;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.Geometry;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.GetRestaurantsResult;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.Get_HotelStaticLocation_Response;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.OpeningHours;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.RestaurantDetails;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.RestaurantPhoto;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.REST_API.API_interface_restaurant;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewRestaurantSearch extends AppCompatActivity implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


    /**
     * Widgets
     */
    RecyclerView hotel_recylerview;
    EditText edit_search_hotel;


    /**
     * Hotel search arraylists
     */
    List<RestaurantDetails> restaurantOutputPojoList;
    ArrayList<GetRestaurantsResult> getallRestaurantDetailsPojoArrayList = new ArrayList<>();
    List<RestaurantPhoto> photos;
    List<OpeningHours> openingHoursPojoList;
    List<String> types;
    List<String> htmlAttributions;
    private List<Object> weekdayText;

    /**
     * Hotel opening hour pojo
     */
    OpeningHours openingHoursPojo;
    Geometry geometryPojo;

    /**
     * API interface for restaurant
     */
    API_interface_restaurant apiService;

    /**
     * GPS tracker
     */
    GPSTracker gpsTracker;

    private AlertDialog sweetDialog;
    /**
     * Permission integer values
     */
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private GoogleApiClient googleApiClient;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";
    final static int REQUEST_LOCATION = 199;


    // Variables

    double latitude;
    double longitude;
    /**
     * Latitude & longitude string result
     */
    String latLngString;
    String hotel_name;
    String hotel_address;
    String google_id;
    String google_photo;
    String place_id;

    /**
     * No restaurant found layout
     */
    LinearLayout ll_not_found;

    Handler mHandler;
    private ApiInterface apiInterface;

    ProgressBar progressbar;

    ReviewRestaurantSearchAdapter reviewRestaurantSearchAdapter;

    private Location mylocation;

    private int userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_restaurant_search);

        gpsTracker = new GPSTracker(ReviewRestaurantSearch.this);

        userid = Integer.parseInt(Pref_storage.getDetail(ReviewRestaurantSearch.this, "userId"));

        edit_search_hotel = (EditText) findViewById(R.id.edit_search_hotel);
        edit_search_hotel.setEnabled(false);
        hotel_recylerview = (RecyclerView) findViewById(R.id.hotel_recylerview);
        ll_not_found = (LinearLayout) findViewById(R.id.ll_not_found);
        progressbar = (ProgressBar) findViewById(R.id.progress_timeline);

        mHandler = new Handler();


        /* Initiate Google API Client  */
        setUpGClient();

        /*API interface initialization for restaurnt search API call*/
        apiService = APIClient_restaurant.getClient().create(API_interface_restaurant.class);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        /*Search hotel Edit text search click listener*/
        edit_search_hotel.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        edit_search_hotel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Your piece of code on keyboard search click

                    String getrestaurantName = edit_search_hotel.getText().toString();

                    int result = ContextCompat.checkSelfPermission(ReviewRestaurantSearch.this, Manifest.permission.ACCESS_FINE_LOCATION);

                    final String restaurantName = getrestaurantName.replace(" ", "+");


                    if (result == PackageManager.PERMISSION_GRANTED) {
                        /*Calling nearby restaurants API using edit text search results*/

                        if (gpsTracker.isGPSEnabled) {

                            // fetchStores(restaurantName);
                            nearByRestaurants(restaurantName, latitude, longitude);

                        } else {

                            latitude = 13.0723153;
                            longitude = 80.215563;

                            nearByRestaurants(restaurantName,latitude,longitude);
                        }
                    } else if (result == PackageManager.PERMISSION_DENIED){

                        latitude = 13.0723153;
                        longitude = 80.215563;

                        nearByRestaurants(restaurantName,latitude,longitude);

                    }
                    return true;
                }
                return false;
            }
        });

        /*Search hotel Edit text change listener*/
        edit_search_hotel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                getallRestaurantDetailsPojoArrayList.clear();


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String getrestaurantName = edit_search_hotel.getText().toString();

                int result = ContextCompat.checkSelfPermission(ReviewRestaurantSearch.this, Manifest.permission.ACCESS_FINE_LOCATION);

                final String restaurantName = getrestaurantName.replace(" ", "+");

                if (result == PackageManager.PERMISSION_GRANTED) {

                    if (restaurantName.length() != 0) {
                        /*Calling nearby restaurants API using edit text search results*/

                        //fetchStores(restaurantName);
                        nearByRestaurants(restaurantName, latitude, longitude);

                    } else {

                        latitude = 13.0723153;
                        longitude = 80.215563;

                        nearByRestaurants(restaurantName,latitude,longitude);
                    }

                } else if (result == PackageManager.PERMISSION_DENIED){

                    latitude = 13.0723153;
                    longitude = 80.215563;

                    nearByRestaurants(restaurantName,latitude,longitude);
                }

            }
        });
    }

    private void  permissionDeniedSearch(final String restaurantName,int userid) {

        Call<Get_HotelStaticLocation_Response> call = apiInterface.get_hotel_static_location("get_hotel_static_location", userid);
        call.enqueue(new Callback<Get_HotelStaticLocation_Response>() {
            @Override
            public void onResponse(Call<Get_HotelStaticLocation_Response> call, Response<Get_HotelStaticLocation_Response> response) {


                if (response.isSuccessful()) {


                    double latitude = response.body().getData().get(0).getLatitude();
                    double longitude = response.body().getData().get(0).getLongitude();
                    Log.e("View","Latitude ---------> "+latitude);
                    Log.e("View","Latitude ---------> "+longitude);
                    nearByRestaurants(restaurantName, latitude, longitude);

                } else {

                    Toast.makeText(ReviewRestaurantSearch.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Get_HotelStaticLocation_Response> call, Throwable t) {
                // Log error here since request failed
                call.cancel();

            }
        });
    }

    private void permissionCheckingCreateView() {

        if (Utility.isConnected(ReviewRestaurantSearch.this)) {

            getMyLocation();
            sweetDialog = new SpotsDialog.Builder().setContext(ReviewRestaurantSearch.this).setMessage("").build();
            sweetDialog.setCancelable(false);
            sweetDialog.show();
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        sleep(7000);
                        String rediusChecking = "500";

                        nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            thread.start();


        } else {

            Toast.makeText(ReviewRestaurantSearch.this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        Log.d("onActivityResult()", "resultCode" + Integer.toString(resultCode));
        Log.d("onActivityResult()", "requestCode" + Integer.toString(requestCode));
        Log.d("onActivityResult()", "data" + data);


        switch (requestCode) {

            case 1:

                switch (resultCode) {

                    case Activity.RESULT_OK: {

                        Log.v("isGPSEnabled", "latitude->" + latitude);
                        Log.v("isGPSEnabled", "longitude->" + longitude);

                        if (Utility.isConnected(ReviewRestaurantSearch.this)) {

                            sweetDialog = new SpotsDialog.Builder().setContext(ReviewRestaurantSearch.this).setMessage("").build();
                            sweetDialog.setCancelable(false);
                            sweetDialog.show();
                            Thread thread = new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        sleep(7000);
                                        String rediusChecking = "500";
                                        Log.e("isGPSEnabled", "latitude->" + latitude);
                                        Log.e("isGPSEnabled", "longitude->" + longitude);
                                        nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            };
                            thread.start();

                        } else {

                            Toast.makeText(ReviewRestaurantSearch.this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        // Toast.makeText(getApplicationContext(), "Location not enabled.", Toast.LENGTH_LONG).show();
                        //call restaurant api
                        //  get_hotel_static_location();

                        if (Utility.isConnected(ReviewRestaurantSearch.this)) {

                            sweetDialog = new SpotsDialog.Builder().setContext(ReviewRestaurantSearch.this).setMessage("").build();
                            sweetDialog.setCancelable(false);
                            sweetDialog.show();
                            Call<Get_HotelStaticLocation_Response> call = apiInterface.get_hotel_static_location("get_hotel_static_location", userid);
                            call.enqueue(new Callback<Get_HotelStaticLocation_Response>() {
                                @Override
                                public void onResponse(Call<Get_HotelStaticLocation_Response> call, Response<Get_HotelStaticLocation_Response> response) {


                                    if (response.isSuccessful()) {


                                        double latitude = response.body().getData().get(0).getLatitude();
                                        double longitude = response.body().getData().get(0).getLongitude();
                                        String rediusChecking = "500";

                                        nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);


                                    } else {
                                        Toast.makeText(ReviewRestaurantSearch.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Get_HotelStaticLocation_Response> call, Throwable t) {
                                    // Log error here since request failed
                                    call.cancel();
                                    sweetDialog.dismiss();
                                }
                            });

                            break;
                        } else {

                            Toast.makeText(this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }


    public synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }


    /* Check Location Permission for Marshmallow Devices */
    private void checkPermissions() {
        int permissionLocation = ContextCompat.checkSelfPermission(ReviewRestaurantSearch.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                getMyLocation();
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), ACCESS_FINE_LOCATION_INTENT_ID);
            }
        } else {
            if (gpsTracker.isGPSEnabled) {
                permissionCheckingCreateView();
            } else {
                getMyLocation();
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == ACCESS_FINE_LOCATION_INTENT_ID) {

            int permissionSize = permissions.length;

            for (int i = 0; i < permissionSize; i++) {

                if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                    if (gpsTracker.isGPSEnabled) {

                        getMyLocation();
                        sweetDialog = new SpotsDialog.Builder().setContext(ReviewRestaurantSearch.this).setMessage("").build();
                        sweetDialog.setCancelable(false);
                        sweetDialog.show();
                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(7000);
                                    String rediusChecking = "500";
                                    nearByRestaurantsStaticLatlong(latitude, longitude, rediusChecking);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        thread.start();

                    } else {

                        getMyLocation();
                    }

                } else if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                    if (Utility.isConnected(ReviewRestaurantSearch.this)) {

                        sweetDialog = new SpotsDialog.Builder().setContext(ReviewRestaurantSearch.this).setMessage("").build();
                        sweetDialog.setCancelable(false);
                        sweetDialog.show();
                        Call<Get_HotelStaticLocation_Response> call = apiInterface.get_hotel_static_location("get_hotel_static_location", userid);
                        call.enqueue(new Callback<Get_HotelStaticLocation_Response>() {
                            @Override
                            public void onResponse(Call<Get_HotelStaticLocation_Response> call, Response<Get_HotelStaticLocation_Response> response) {


                                if (response.isSuccessful()) {


                                    double latitude = response.body().getData().get(0).getLatitude();
                                    double longitude = response.body().getData().get(0).getLongitude();
                                    String radiusChecking = "5000";

                                    nearByRestaurantsStaticLatlong(latitude, longitude, radiusChecking);

                                } else {

                                    Toast.makeText(ReviewRestaurantSearch.this, "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Get_HotelStaticLocation_Response> call, Throwable t) {
                                // Log error here since request failed
                                call.cancel();

                                sweetDialog.dismiss();
                            }
                        });
                    } else {

                        Toast.makeText(this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
                    }
                }

            }

        }
    }


    private void getMyLocation() {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(ReviewRestaurantSearch.this, Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, ReviewRestaurantSearch.this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    mylocation = LocationServices.FusedLocationApi
                                            .getLastLocation(googleApiClient);
                                    break;

                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(ReviewRestaurantSearch.this,
                                                REQUEST_CHECK_SETTINGS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    /*  Show Popup to access User Permission  */
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(ReviewRestaurantSearch.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(ReviewRestaurantSearch.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(ReviewRestaurantSearch.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    /* Run on UI */
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            getMyLocation();
        }
    };

    /* Broadcast receiver to check status of GPS */
    private BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //If Action is Location
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                //Check if GPS is turned ON or OFF
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Log.e("About GPS", "GPS is Enabled in your device");
                } else {
                    //If GPS turned OFF show Location Dialog
                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    // showSettingDialog();
                    Log.e("About GPS", "GPS is Disabled in your device");
                }

            }
        }
    };

    /* Search hotel names */

    private void fetchStores(String businessName) {

        if (Utility.isConnected(getApplicationContext())) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

            latLngString = String.valueOf(latitude) + "," + String.valueOf(longitude);

            Log.e("FetchStores:", "businessName->" + businessName);
            Log.e("FetchStores:", "latLngString->" + latLngString);

            Call<RestaurantDetails> call = apiService.searchRestaurants(businessName, latLngString, "20000", "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
            call.enqueue(new Callback<RestaurantDetails>() {
                @Override
                public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                    RestaurantDetails restaurantDetails = response.body();

                    if (response.isSuccessful()) {

                        Log.e("restaurantOutputPojo", "" + response.toString());

                        if (restaurantDetails.getStatus().equalsIgnoreCase("OK")) {

                            restaurantOutputPojoList = new ArrayList<>();
                            getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                            photos = new ArrayList<>();
                            types = new ArrayList<>();
                            htmlAttributions = new ArrayList<>();
                            openingHoursPojoList = new ArrayList<>();

                            Log.e("hotel_size", "" + restaurantDetails.getGetRestaurantsResults().size());

                            for (int i = 0; i < restaurantDetails.getGetRestaurantsResults().size(); i++) {

                                String placeid = restaurantDetails.getGetRestaurantsResults().get(i).getPlaceId();
                                String Icon = restaurantDetails.getGetRestaurantsResults().get(i).getIcon();
                                String hotel_name = restaurantDetails.getGetRestaurantsResults().get(i).getName();
                                String Id = restaurantDetails.getGetRestaurantsResults().get(i).getId();
                                String reference = restaurantDetails.getGetRestaurantsResults().get(i).getReference();
                                String address = restaurantDetails.getGetRestaurantsResults().get(i).getFormattedAddress();
                                String rating = restaurantDetails.getGetRestaurantsResults().get(i).getRating();

                                List<RestaurantPhoto> restaurantPhotos = new ArrayList<>();
                                restaurantPhotos = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos();


                                try {

                                    String photo_ref = restaurantDetails.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();
                                    int width = restaurantDetails.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                    int height = restaurantDetails.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();


                                    if (photo_ref == null) {

                                    } else {
                                        RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
                                        Log.e("height1", "photo_ref1: " + photo_ref);
                                        Log.e("height1", "width1: " + width);
                                        Log.e("height1", "height1: " + height);
                                        photos.add(restaurantPhoto);

                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                                for (int kk = 0; kk < restaurantDetails.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                    types.add(restaurantDetails.getGetRestaurantsResults().get(i).getTypes().toString());
                                }


                                try {
                                    boolean open_status = restaurantDetails.getGetRestaurantsResults().get(i).getOpeningHours().getOpenNow();
                                    Log.e("open_status", "open_status: " + open_status);

                                    openingHoursPojo = new OpeningHours(open_status, weekdayText);
                                    openingHoursPojoList.add(openingHoursPojo);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                if (restaurantPhotos != null) {

                                    GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                            hotel_name, openingHoursPojo, restaurantPhotos, placeid, rating, reference, types, address);
                                    getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);
                                }

                                ReviewRestaurantSearchAdapter reviewRestaurantSearchAdapter = new ReviewRestaurantSearchAdapter(ReviewRestaurantSearch.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList);
                                LinearLayoutManager mLayoutManager = new LinearLayoutManager(ReviewRestaurantSearch.this, LinearLayoutManager.HORIZONTAL, true);
                                hotel_recylerview.setLayoutManager(mLayoutManager);
                                mLayoutManager.setStackFromEnd(true);
                          /*  mLayoutManager.setStackFromEnd(false);
                            mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() + 1);*/
                                hotel_recylerview.setAdapter(reviewRestaurantSearchAdapter);
                                // hotel_recylerview.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                                hotel_recylerview.setItemAnimator(new DefaultItemAnimator());
                                hotel_recylerview.setNestedScrollingEnabled(false);
                                hotel_recylerview.setHorizontalScrollBarEnabled(true);
                                reviewRestaurantSearchAdapter.notifyDataSetChanged();

                            }

                        } else {
                            Toast.makeText(ReviewRestaurantSearch.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                        }

                    } else if (!response.isSuccessful()) {
                    }

                }

                @Override
                public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                    // Log error here since request failed
                    call.cancel();
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();

        }

    }


    // Search nearby restaurants API

    private void nearByRestaurants(String restaurantName, double latitude, double longitude) {

        if (Utility.isConnected(getApplicationContext())) {

            progressbar.setVisibility(View.VISIBLE);


            latLngString = String.valueOf(latitude) + "," + String.valueOf(longitude);


            Call<RestaurantDetails> call = apiService.searchRestaurantsReviewWithnearby(restaurantName, latLngString, "500", "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
            call.enqueue(new Callback<RestaurantDetails>() {
                @Override
                public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                    RestaurantDetails restaurantOutputPojo = response.body();

                    if (response.body() != null) {

                    if (response.isSuccessful()) {

                        progressbar.setVisibility(View.GONE);

                        if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {

                            restaurantOutputPojoList = new ArrayList<>();
                            getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                            photos = new ArrayList<>();
                            types = new ArrayList<>();
                            htmlAttributions = new ArrayList<>();
                            openingHoursPojoList = new ArrayList<>();

                            Log.e("hotel_size", "" + restaurantOutputPojo.getGetRestaurantsResults().size());

                            for (int i = 0; i < restaurantOutputPojo.getGetRestaurantsResults().size(); i++) {

                                String placeid = restaurantOutputPojo.getGetRestaurantsResults().get(i).getPlaceId();
                                String Icon = restaurantOutputPojo.getGetRestaurantsResults().get(i).getIcon();
                                String hotel_name = restaurantOutputPojo.getGetRestaurantsResults().get(i).getName();
                                String Id = restaurantOutputPojo.getGetRestaurantsResults().get(i).getId();
                                String reference = restaurantOutputPojo.getGetRestaurantsResults().get(i).getReference();
                                String address = restaurantOutputPojo.getGetRestaurantsResults().get(i).getFormattedAddress();
                                String rating = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRating();

                                List<RestaurantPhoto> restaurantPhotos;

                                restaurantPhotos = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos();

                                try {

                                    String photo_ref = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();

                                    Log.e("photo_size", "onResponse: " + restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().size());
                                    int width = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                    int height = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();

                                    RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
                                    photos.add(restaurantPhoto);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                                for (int kk = 0; kk < restaurantOutputPojo.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                    types.add(restaurantOutputPojo.getGetRestaurantsResults().get(i).getTypes().toString());
                                }

                                if (restaurantPhotos != null) {

                                    GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                            hotel_name, openingHoursPojo, restaurantPhotos, placeid, rating, reference, types, address);
                                    getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);
                                }


                            }

                            reviewRestaurantSearchAdapter = new ReviewRestaurantSearchAdapter(ReviewRestaurantSearch.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(ReviewRestaurantSearch.this, LinearLayoutManager.HORIZONTAL, false);
                            hotel_recylerview.setLayoutManager(mLayoutManager);
                            hotel_recylerview.setAdapter(reviewRestaurantSearchAdapter);
                            hotel_recylerview.setItemAnimator(new DefaultItemAnimator());
                            hotel_recylerview.setNestedScrollingEnabled(false);
                            hotel_recylerview.setHorizontalScrollBarEnabled(true);
                            reviewRestaurantSearchAdapter.notifyDataSetChanged();


                        } else {

                            Toast.makeText(ReviewRestaurantSearch.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                            progressbar.setVisibility(View.GONE);

                        }

                    } else {

                    }

                    } else {

                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(ReviewRestaurantSearch.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                    // Log error here since request failed
                    call.cancel();
                    progressbar.setVisibility(View.GONE);
                }
            });

        } else {

            Toast.makeText(getApplicationContext(), "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }

    }


    private void nearByRestaurantsStaticLatlong(Double latitude, Double longitude, String radiusChecking) {

        //progressbar.setVisibility(View.VISIBLE);

        latLngString = latitude + "," + longitude;

        Call<RestaurantDetails> call = apiService.searchRestaurantsReviewWithnearby("", latLngString, radiusChecking, "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY);
        call.enqueue(new Callback<RestaurantDetails>() {
            @Override
            public void onResponse(Call<RestaurantDetails> call, Response<RestaurantDetails> response) {

                RestaurantDetails restaurantOutputPojo = response.body();


                sweetDialog.dismiss();

                if (response.isSuccessful()) {

                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {

                        edit_search_hotel.setEnabled(true);
                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();

                        Log.e("hotel_size", "" + restaurantOutputPojo.getGetRestaurantsResults().size());

                        for (int i = 0; i < restaurantOutputPojo.getGetRestaurantsResults().size(); i++) {

                            String placeid = restaurantOutputPojo.getGetRestaurantsResults().get(i).getPlaceId();
                            String Icon = restaurantOutputPojo.getGetRestaurantsResults().get(i).getIcon();
                            String hotel_name = restaurantOutputPojo.getGetRestaurantsResults().get(i).getName();
                            String Id = restaurantOutputPojo.getGetRestaurantsResults().get(i).getId();
                            String reference = restaurantOutputPojo.getGetRestaurantsResults().get(i).getReference();
                            String address = restaurantOutputPojo.getGetRestaurantsResults().get(i).getFormattedAddress();
//                            String scope = restaurantOutputPojo.getGetRestaurantsResults().get(i).getScope();
                            String rating = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRating();

                            List<RestaurantPhoto> restaurantPhotos = new ArrayList<>();

                            restaurantPhotos = response.body().getGetRestaurantsResults().get(i).getRestaurantPhotos();

                            try {

                                String photo_ref = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getPhotoReference();

                                Log.e("photo_size", "onResponse: " + restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().size());
                                int width = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getWidth();
                                int height = restaurantOutputPojo.getGetRestaurantsResults().get(i).getRestaurantPhotos().get(0).getHeight();

                                RestaurantPhoto restaurantPhoto = new RestaurantPhoto(height, htmlAttributions, photo_ref, width);
                               /* Log.e("PhotosCount", "Photos->: " + photo_ref.substring(0, 20) + " " + i);
                                Log.e("height1", "width1: " + width);
                                Log.e("height1", "height1: " + height);*/
                                photos.add(restaurantPhoto);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            for (int kk = 0; kk < restaurantOutputPojo.getGetRestaurantsResults().get(0).getTypes().size(); kk++) {
                                types.add(restaurantOutputPojo.getGetRestaurantsResults().get(i).getTypes().toString());
                            }

                            if (restaurantPhotos != null) {
                                GetRestaurantsResult getRestaurantsResult = new GetRestaurantsResult(geometryPojo, Icon, Id,
                                        hotel_name, openingHoursPojo, restaurantPhotos, placeid, rating, reference, types, address);
                                getallRestaurantDetailsPojoArrayList.add(getRestaurantsResult);
                            }
                        }


                        ReviewRestaurantSearchAdapter reviewRestaurantSearchAdapter = new ReviewRestaurantSearchAdapter(ReviewRestaurantSearch.this, getallRestaurantDetailsPojoArrayList, openingHoursPojoList);
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ReviewRestaurantSearch.this, LinearLayoutManager.HORIZONTAL, false);
                        hotel_recylerview.setLayoutManager(mLayoutManager);
                        hotel_recylerview.setAdapter(reviewRestaurantSearchAdapter);
                        hotel_recylerview.setItemAnimator(new DefaultItemAnimator());
                        hotel_recylerview.setNestedScrollingEnabled(false);
                        hotel_recylerview.setHorizontalScrollBarEnabled(true);
                        reviewRestaurantSearchAdapter.notifyDataSetChanged();

                    } else {

                        sweetDialog.dismiss();

                        Toast.makeText(ReviewRestaurantSearch.this, "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    sweetDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<RestaurantDetails> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
                //progressbar.setVisibility(View.GONE);
                sweetDialog.dismiss();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mHandler.removeCallbacksAndMessages(null);
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
        finish();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            latitude = mylocation.getLatitude();
            longitude = mylocation.getLongitude();
            Log.e("latttttt", "onCreate: " + latitude);
            Log.e("latttttt", "onCreate: " + longitude);

        } else {

            getMyLocation();
        }
    }
}
