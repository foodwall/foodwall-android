package com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;
import com.kaspontech.foodwall.reviewpackage.alldishtab.AllDishDisplayActivity;
import com.kaspontech.foodwall.utills.GlideApp;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PackagingDetailsAdapter extends RecyclerView.Adapter<PackagingDetailsAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;


    private ArrayList<PackImage> getAmbienceImageAllArrayList = new ArrayList<>();

    String reviewId, hotelName, catType;
    private PackImage reviewAmbiImagePojo;


    public PackagingDetailsAdapter(Context c, ArrayList<PackImage> gettinglist, String reviewId, String hotelName, String catType) {
        this.context = c;
        this.reviewId = reviewId;
        this.catType = catType;
        this.hotelName = hotelName;
        this.getAmbienceImageAllArrayList = gettinglist;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_image_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v);
        vh.setIsRecyclable(false);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        try {
            reviewAmbiImagePojo = getAmbienceImageAllArrayList.get(position);
//            Utility.picassoImageLoader(reviewAmbiImagePojo.getImg(),0,holder.image_review);

            if (getAmbienceImageAllArrayList.size() > 1) {

                Picasso.get().load(reviewAmbiImagePojo.getImg())
                        .into(holder.image_review);
            } else {

                GlideApp.with(context)
                        .load(reviewAmbiImagePojo.getImg())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .thumbnail(0.1f)
                        .into(holder.image_review);

            }

            holder.txt_image.setVisibility(View.GONE);


            holder.rl_hotel_datas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, AllDishDisplayActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    intent.putExtra("reviewid", reviewId);
                    intent.putExtra("hotelname", hotelName);
                    intent.putExtra("catType", catType);
                    intent.putExtra("redirectdishes", "3");
                     context.startActivity(intent);
                }
            });


        } catch (Exception e) {
            Log.e("view", "view----->" + e.getMessage());
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        Log.e("view", "view----->" + getAmbienceImageAllArrayList.size());
        return getAmbienceImageAllArrayList.size();

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //TextView
        TextView txt_image;

        //Imageview
        ImageView image_review;

        RelativeLayout rl_hotel_datas;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt_image = (TextView) itemView.findViewById(R.id.txt_image);
            image_review = (ImageView) itemView.findViewById(R.id.image_review);
            rl_hotel_datas = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_datas);
        }
    }
}
