package com.kaspontech.foodwall.reviewpackage.alldishtab;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.kaspontech.foodwall.R;

import java.util.Objects;

import info.hoang8f.android.segmented.SegmentedGroup;

public class AllDishDisplayActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    /**
     * Action Bar
     */

    Toolbar actionBar;

    /**
     * Toolbar title
     */

    TextView toolbarTitle;

    /**
     * Back button
     */
    ImageButton back;

    /**
     * Event search button
     */

    ImageButton eventSearch;


    /**
     * Frame layout for all dish container
     */
    FrameLayout alldishimageContainer;

    /**
     * Segmented Group images
     */
    SegmentedGroup segmentedImages;

    /**
     * Tab layout dishes
     */
    TabLayout tabDishes;

    /**
     * Top dish type Radio Button
     */
    RadioButton topType;
    /**
     * Avoid dish type Radio Button
     */
    RadioButton avoidType;
    /**
     * Ambience type Radio Button
     */
    RadioButton ambienceType;

    /**
     * Top dish type Fragment
     */
    Fragment topdishfragment;

    /**
     * Avoid dish type Fragment
     */
    Fragment avoiddishfragment;

    /**
     * Ambience type Fragment
     */
    Fragment ambiencefragment;

    /**
     * String results
     */
    String reviewId;
    String hotelName;
    String catType;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alldishimagelayout);

        /*Widget initialization*/

        actionBar = findViewById(R.id.action_bar_dishes);

        /*Toolbar title initialization*/

        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setGravity(View.TEXT_ALIGNMENT_CENTER);

        /*Back button initialization*/

        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        /*Event search initialization*/

        eventSearch = actionBar.findViewById(R.id.event_search);
        eventSearch.setVisibility(View.GONE);

        /*Segment images initialization*/

        segmentedImages = findViewById(R.id.segmented_images);


        /*tab dish layout initialization*/

        tabDishes = findViewById(R.id.tab_dishes);

        /*Dish containers initialization*/

        alldishimageContainer = findViewById(R.id.alldishimage_container);

        /*Top dish Radio Button initialization*/

        topType = findViewById(R.id.topType);

        /*Avoid dish Radio Button initialization*/

        avoidType = findViewById(R.id.avoidType);

        /*Ambience dish Radio Button initialization*/

        ambienceType = findViewById(R.id.ambienceType);

        /*Top , avoid & ambience fragment initialization*/

        topdishfragment = new TopdishFragment();

        avoiddishfragment = new AvoidDishFragment();

        ambiencefragment = new AmbienceFragment();

        /*Default fragment - Top dish fragment*/

        replaceFragment(topdishfragment);

        /*Getting input values from various activiies & adapters*/

        Intent intent = getIntent();
        /*Review id*/

        reviewId = intent.getStringExtra("reviewid");

        /*Hotel name*/

        hotelName = intent.getStringExtra("hotelname");

        /*Category type*/

        catType = intent.getStringExtra("catType");

        /*TAB dish name initialization*/

        tabDishes.addTab(tabDishes.newTab().setText("Top Dishes"));

        tabDishes.addTab(tabDishes.newTab().setText("Dishes To \nAvoid"));


        if (catType != null) {

            if (catType.equalsIgnoreCase("2")) {

                tabDishes.addTab(tabDishes.newTab().setText("Packaging"));

            } else {

                tabDishes.addTab(tabDishes.newTab().setText("Ambience"));

            }

        }

        /**
         * redirect activity
         */

        try {
            if (intent.getStringExtra("redirectdishes") == null) {
                replaceFragment(topdishfragment);
            } else {
                int redirect = Integer.parseInt(intent.getStringExtra("redirectdishes"));

                if (redirect == 1) {
                    Objects.requireNonNull(tabDishes.getTabAt(0)).select();
                /*
               tabDishes fragment
                */
                    replaceFragment(topdishfragment);


                } else if (redirect == 2) {
                    Objects.requireNonNull(tabDishes.getTabAt(1)).select();
            /*
            Individual bucket fragment
            */
                    replaceFragment(avoiddishfragment);
                } else if (redirect == 3) {
                    Objects.requireNonNull(tabDishes.getTabAt(2)).select();
            /*
            Group bucket fragment
            */
                    replaceFragment(ambiencefragment);
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();

        }

        /*Setting color to tab dishes*/

        tabDishes.setTabTextColors(Color.parseColor("#727272"), Color.parseColor("#ffffff"));

        /*Hotel name setting and displaying*/

        if ( hotelName == null || hotelName.equalsIgnoreCase("") ) {

            toolbarTitle.setText(hotelName);

        } else {

            toolbarTitle.setText(hotelName);

        }

        /*On tab selected lisitener*/
        tabDishes.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {

                    case 0:

                        /*Swap to TopDish Fragment*/

                        replaceFragment(topdishfragment);

                        break;
                    case 1:

                        /*Swap to AvoidDish Fragment*/

                        replaceFragment(avoiddishfragment);

                        break;

                    case 2:

                        /*Swap to Ambience Fragment*/

                        replaceFragment(ambiencefragment);
                        break;

                    case 3:
                        break;

                        default:
                            replaceFragment(topdishfragment);

                            break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }


        });


    }
    /*Getting review id*/
    public String getReviewId() {
        return reviewId;
    }
    /*Getting hotel id*/

    public String getHotelName() {
        return hotelName;
    }

    /*Fragment replacing container*/

    public void replaceFragment(Fragment fragment) {

        try {

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.alldishimage_container, fragment);
            fragmentTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {


        switch (group.getCheckedRadioButtonId()) {

            case R.id.topType:

                /*Swap to TopDish Fragment*/
                replaceFragment(topdishfragment);


                break;


            case R.id.avoidType:

                /*Swap to AvoidDish Fragment*/

                replaceFragment(avoiddishfragment);


                break;

            case R.id.ambienceType:

                /*Swap to Ambience Fragment*/
                replaceFragment(ambiencefragment);

                break;

                default:
                    /*Swap to TopDish Fragment*/
                    replaceFragment(topdishfragment);

                break;

        }

    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*Transition effect from right to left*/
//        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
        finish();

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        if(id == R.id.back){

            /*Transition effect from right to left*/

//            overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
            finish();
        }

    }


}

