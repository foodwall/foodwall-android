package com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReviewOutputResponsePojo {
    @SerializedName("methodName")
    @Expose
    private String methodName;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Data")
    @Expose
    private List<ReviewInputAllPojo> data = null;
    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("ResponseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("custom")
    @Expose
    private String custom;
    @SerializedName("noofrow")
    @Expose
    private int noofrow;

    public ReviewOutputResponsePojo(String methodName, Integer status, List<ReviewInputAllPojo> data, Integer responseCode, String responseMessage, String custom, int noofrow) {
        this.methodName = methodName;
        this.status = status;
        this.data = data;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.custom = custom;
        this.noofrow = noofrow;
    }

    public int getNoofrow() {
        return noofrow;
    }

    public void setNoofrow(int noofrow) {
        this.noofrow = noofrow;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<ReviewInputAllPojo> getData() {
        return data;
    }

    public void setData(List<ReviewInputAllPojo> data) {
        this.data = data;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }
}
