package com.kaspontech.foodwall.reviewpackage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.hsalf.smilerating.SmileRating;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_adapter.Review_ambience_image_adapter;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_packaging_dish_adapter;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_Deliverymode_Response;
import com.kaspontech.foodwall.modelclasses.Review_pojo.ReviewSpinner_Pojo;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.adapters.Review_Image_adapter.Review_ambience_slide_adapter;
import com.kaspontech.foodwall.gps.GPSTracker;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Create_review_output_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Geometry_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.GetAllRestaurantsPojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Get_hotel_photo_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.OpeningHours_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Restaurant_output_pojo;
import com.kaspontech.foodwall.modelclasses.Review_pojo.StoreModel;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.APIClient_restaurant;
import com.kaspontech.foodwall.REST_API.API_interface_restaurant;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.reviewpackage.Review_adapter_folder.Review_image_pojo;
import com.kaspontech.foodwall.reviewpackage.Review_adapter_folder.Review_image_top_dish_adapter;
import com.kaspontech.foodwall.reviewpackage.Review_adapter_folder.Review_image_worst_dish_adapter;
import com.kaspontech.foodwall.reviewpackage.Review_adapter_folder.Review_inside_adapter;
import com.kaspontech.foodwall.utills.CustomGIFview;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.TouchImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import mabbas007.tagsedittext.TagsEditText;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewTestActivity extends _SwipeActivityClass
        implements View.OnClickListener,
        AdapterView.OnItemSelectedListener,
        AdapterView.OnItemClickListener,
        Review_ambience_slide_adapter.PhotoRemovedListener,
        Review_image_top_dish_adapter.TopDishRemovedListener,
        Review_image_worst_dish_adapter.AvoidDishRemovedListener,
        Review_ambience_image_adapter.TopDishRemovedListener,
        ScrollView.OnTouchListener {


    private static final String TAG = "ReviewTestActivity";
    // Pojo
    public Geometry_pojo geometryPojo;
    public OpeningHours_pojo openingHoursPojo;
    // GPS
    public GPSTracker gpsTracker;

    // File
    public File file, file1;

    // GoogleApiClient
    public GoogleApiClient mGoogleApiClient;

    // ApiInterface
    public API_interface_restaurant apiService;


    // Alert

    AlertDialog dialog1;

    // Action Bar

    public Toolbar actionBar;
    public ImageButton back;
    public TextView tv_create_review, toolbar_title;

    // Popup details

    public Button done_btn, cancel_btn;
    public ImageView img_food, img_smiley;
    RelativeLayout rl_dialog_dish;

    EmojiconEditText edt_food_caption, edt_review_description;
    EmojIconActions emojIcon;

    public static PhotoView img_addimage;
    public Button btn_addImages, btn_create_review;
    public AutoCompleteTextView autoCompleteTextView;
    public RadioGroup radio_category, radio_vegnonveg;
    public TagsEditText tagsedittext, tagsedittext_worst;
    public EditText edt_hoteltxt;
    public LinearLayout ll_modeofdelivery, ll_review, ll_htl_layout;
    public RelativeLayout rl_ambiance_dotindicator, rl_package_dotindicator;
    public ViewPager ambi_viewpager_review, package_viewpager_review;
    public RadioButton radio_dine, radio_delivery, radio_Veg, radio_Nonveg;
    public static LinearLayout ll_image_review, ll_edt_add_dish, ll_edt_tag_dish, ll_edt_tag_worst_dish;
    public LinearLayout ll_top_dish_add_txt, ll_dish_to_avoid_add_txt, ll_amb_add_txt, ll_packaging_add_txt, ll_package_add_txt;

    // ImageView overall

    public ImageButton image_add_top_dish, image_add_dish_worst;
    public RecyclerView rv_top_dish, rv_worst_dish, rv_img_restaurant, rv_package, rv_ambience_dish, rv_package_dish;
    public CircleIndicator ambi_review_indicator, packageindicator;

    // Image reset

    public TouchImageView img_revie;
    public ImageView img_restaurant_image;
    public ImageButton image_ambiance, image_service, btn_search;
    public Spinner spinner_delivery_mode;

    List<String> deliverymodelist = new ArrayList<>();
    List<String> deliverymodeIDlist = new ArrayList<>();

    JSONObject dishnameView, withoutimage;
    ArrayList<ReviewSpinner_Pojo> reviewSpinnerPojoArrayList = new ArrayList<>();

    // Textview value

    public TextView txt_rating_low, txt_rating_low_ambiance, txt_rating_low_taste, txt_rating_low_service, txt_rating_low_value,
            txt_hotel_name, txt_hotel_address, txt_search, txt_ambiance_add, txt_service, txt_ambi;


    // RatingBar ratingBar;

    public SmileRating ratingBar, rtng_ambiance, rtng_taste, rtng_service, rtng_value;

    /*
     *  Variables
     *
     * */


    // Strings
    ArrayList<JSONObject> dishnameWithImage = new ArrayList<>();
    ArrayList<JSONObject> dishnameWithoutImage = new ArrayList<>();
    public String Hotel_name = "", Hotel_location = "", Hotel_icon, Hotel_rating, place_id = "", google_id = "",
            image, dish_name, dish_image, ambience_camera, ambience_gallery, for_top, for_worst,
            autocompleteresult, tageditresult, tagedit_worst_result, next_token, delivery_type_text = "",
            latLngString, hotel_id, review_id, type, topdishresult, worstdishresult;


    public String[] Mode_of_delivery = {"Select delivery partner", "Swiggy", "Uber Eats", "Zomato", "Food Panda"};

    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";


    //Loader
    ProgressBar progressBar;
    TextView prg_textView;
    CustomGIFview loaderimage;

    // Integer

    public int overall, ambiance, taste, service, value_money, category, veg_nonveg, delivery_type;


    private boolean handled = false;

    // Camera request code top dish
    private static final int CAPTURE_IMAGE_CALLBACK_TOP = 0;
    // Camera request code avoid dish
    private static final int CAPTURE_IMAGE_CALLBACK_AVOID = 2;
    // Camera request code ambience
    private static final int CAPTURE_IMAGE_CALLBACK_AMBIENCE = 4;
    // Camera request code service
    private static final int CAPTURE_IMAGE_CALLBACK_PACKAGE = 6;


    //Gallery request code top dish
    private static final int PICK_IMAGE_MULTIPLE_TOP = 1;
    //Gallery request code avoid dish
    private static final int PICK_IMAGE_MULTIPLE_AVOID = 3;
    //Gallery request code ambience
    private static final int PICK_IMAGE_MULTIPLE_AMBIENCE = 5;
    //Gallery request code service
    public static final int PICK_IMAGE_MULTIPLE_PACKAGE = 7;


    public static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;

    public static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int REQUEST_PERMISSION_SETTING = 5;


    // Image capture

    private Uri imageUri;


    // Double

    public double latitude, longitude;


    // Array List

    public List<String> imagesEncodedList_top = new ArrayList<>();
    public List<String> imagesEncodedList_avoid = new ArrayList<>();
    public List<String> imagesEncodedList_package = new ArrayList<>();
    public List<String> imagesEncodedList_ambience = new ArrayList<>();

    // Dishes name list

    List<String> topDishList = new ArrayList<>();
    List<String> avoidDishList = new ArrayList<>();

    public List<String> types;
    public List<Object> weekdayText;
    public List<StoreModel> storeModels;
    public List<String> htmlAttributions;
    public List<Get_hotel_photo_pojo> photos;
    public List<OpeningHours_pojo> openingHoursPojoList;
    public List<Restaurant_output_pojo> restaurantOutputPojoList;
    public ArrayList<GetAllRestaurantsPojo> getallRestaurantDetailsPojoArrayList = new ArrayList<>();

    public ArrayList<Review_image_pojo> addimagesEncodedList_top = new ArrayList<Review_image_pojo>();
    public ArrayList<Review_image_pojo> addimagesEncodedList_topNew = new ArrayList<Review_image_pojo>();
    public ArrayList<Review_image_pojo> addimagesEncodedList_avoid = new ArrayList<Review_image_pojo>();
    public ArrayList<Review_image_pojo> addimagesEncodedList_ambience = new ArrayList<Review_image_pojo>();
    public ArrayList<Review_image_pojo> addimagesEncodedList_packaging = new ArrayList<Review_image_pojo>();
    //Scroll view
    NestedScrollView scrl_rev;

    ArrayList<String> aListLanguage = new ArrayList<String>();

    // custom rating bar
    private EmojiRatingBar hurtbar, ambience_emojiratingbar, flavour_emojiratingbar, service_emojiratingbar, valueformoney_emojiratingbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_review);


        initComponents();

        //get delivery mode dropdown
        get_delivery_mode();

        edt_review_description.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.edt_review_description) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        // overall food experience rating bar
        hurtbar = (EmojiRatingBar) findViewById(R.id.emojiratingbar);
        txt_rating_low = (TextView) findViewById(R.id.txt_rating_low);

        hurtbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingBar.setRating(rating);
                String msg = "";

                switch ((int) rating) {
                    case 1:
                        txt_rating_low.setText("1");
                        break;
                    case 2:
                        txt_rating_low.setText("2");
                        break;
                    case 3:
                        txt_rating_low.setText("3");
                        break;
                    case 4:
                        txt_rating_low.setText("4");
                        break;
                    case 5:
                        txt_rating_low.setText("5");
                        break;
                    default:
                        txt_rating_low.setText("0");
                        break;
                }

            }
        });

        hurtbar.setRating(0);


        // Ambience experience rating bar
        ambience_emojiratingbar = (EmojiRatingBar) findViewById(R.id.ambience_emojiratingbar);

        ambience_emojiratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingBar.setRating(rating);
                String msg = "";

                switch ((int) rating) {
                    case 1:
                        txt_rating_low_ambiance.setText("1");
                        break;
                    case 2:
                        txt_rating_low_ambiance.setText("2");
                        break;
                    case 3:
                        txt_rating_low_ambiance.setText("3");
                        break;
                    case 4:
                        txt_rating_low_ambiance.setText("4");
                        break;
                    case 5:
                        txt_rating_low_ambiance.setText("5");
                        break;
                    default:
                        txt_rating_low_ambiance.setText("0");
                        break;
                }

            }
        });

        ambience_emojiratingbar.setRating(0);


        //Flavour rating bar
        flavour_emojiratingbar = (EmojiRatingBar) findViewById(R.id.flavour_emojiratingbar);
        flavour_emojiratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingBar.setRating(rating);
                String msg = "";

                switch ((int) rating) {
                    case 1:
                        txt_rating_low_taste.setText("1");
                        break;
                    case 2:
                        txt_rating_low_taste.setText("2");
                        break;
                    case 3:
                        txt_rating_low_taste.setText("3");
                        break;
                    case 4:
                        txt_rating_low_taste.setText("4");
                        break;
                    case 5:
                        txt_rating_low_taste.setText("5");
                        break;
                    default:
                        txt_rating_low_taste.setText("0");
                        break;
                }

            }
        });

        flavour_emojiratingbar.setRating(0);


        //service rating bar
        service_emojiratingbar = (EmojiRatingBar) findViewById(R.id.service_emojiratingbar);
        service_emojiratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingBar.setRating(rating);
                String msg = "";

                switch ((int) rating) {
                    case 1:
                        txt_rating_low_service.setText("1");
                        break;
                    case 2:
                        txt_rating_low_service.setText("2");
                        break;
                    case 3:
                        txt_rating_low_service.setText("3");
                        break;
                    case 4:
                        txt_rating_low_service.setText("4");
                        break;
                    case 5:
                        txt_rating_low_service.setText("5");
                        break;
                    default:
                        txt_rating_low_service.setText("0");
                        break;
                }

            }
        });

        service_emojiratingbar.setRating(0);

        // value for money
        valueformoney_emojiratingbar = (EmojiRatingBar) findViewById(R.id.valueformoney_emojiratingbar);
        valueformoney_emojiratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingBar.setRating(rating);
                String msg = "";

                switch ((int) rating) {
                    case 1:
                        txt_rating_low_value.setText("1");
                        break;
                    case 2:
                        txt_rating_low_value.setText("2");
                        break;
                    case 3:
                        txt_rating_low_value.setText("3");
                        break;
                    case 4:
                        txt_rating_low_value.setText("4");
                        break;
                    case 5:
                        txt_rating_low_value.setText("5");
                        break;
                    default:
                        txt_rating_low_value.setText("0");
                        break;
                }

            }
        });

        valueformoney_emojiratingbar.setRating(0);


        initGoogleAPIClient();

        // Check Location Permission

        //checkPermissions();

        gpsTracker = new GPSTracker(ReviewTestActivity.this);

        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        latLngString = String.valueOf(latitude) + "," + String.valueOf(longitude);


        tagsedittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
        tagsedittext.setTagsListener(new TagsEditText.TagsEditListener() {
            @Override
            public void onTagsChanged(Collection<String> collection) {

                Log.d(TAG, "onTagsChanged: " + collection.toString());

                topDishList.clear();

                for (String dish : collection) {

                    topDishList.add(dish);
                }

                Log.d(TAG, "onTagsChanged: " + topDishList.toString());


            }

            @Override
            public void onEditingFinished() {


            }
        });


        tagsedittext_worst.setImeOptions(EditorInfo.IME_ACTION_DONE);
        tagsedittext_worst.setTagsListener(new TagsEditText.TagsEditListener() {
            @Override
            public void onTagsChanged(Collection<String> collection) {

                Log.d(TAG, "onTagsChanged: " + collection.toString());

                avoidDishList.clear();

                for (String dish : collection) {

                    avoidDishList.add(dish);
                }

                Log.d(TAG, "onTagsChanged: " + avoidDishList.toString());


            }

            @Override
            public void onEditingFinished() {


            }
        });


        edt_hoteltxt.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        edt_hoteltxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String edt_hotel_result = edt_hoteltxt.getText().toString();

                if (!edt_hotel_result.equalsIgnoreCase("")) {
                    location_search(edt_hotel_result);
                }

                return false;

            }
        });


        // Dine in listener

        radio_dine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                txt_ambi.setText(R.string.Delivery);
                txt_service.setText(R.string.Package);
                image_service.setVisibility(View.VISIBLE);
                ll_modeofdelivery.setVisibility(View.VISIBLE);

                ambi_viewpager_review.setVisibility(View.GONE);
                rl_ambiance_dotindicator.setVisibility(View.GONE);

                ll_amb_add_txt.setVisibility(View.GONE);
                // ll_packaging_add_txt.setVisibility(View.VISIBLE);

                ll_package_add_txt.setVisibility(View.VISIBLE);

            }
        });


        // Delivery listener

        radio_delivery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                txt_ambi.setText(R.string.ambiance);
                txt_service.setText(R.string.service);
                // spinner_delivery_mode.setVisibility(View.GONE);
                image_service.setVisibility(View.GONE);
                package_viewpager_review.setVisibility(View.GONE);
                rl_package_dotindicator.setVisibility(View.GONE);

                ll_amb_add_txt.setVisibility(View.VISIBLE);
                // ll_packaging_add_txt.setVisibility(View.GONE);
                ll_modeofdelivery.setVisibility(View.GONE);

                ll_package_add_txt.setVisibility(View.GONE);

            }
        });

        spinner_delivery_mode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                delivery_type_text = spinner_delivery_mode.getSelectedItem().toString();
                // delivery_type = Integer.parseInt(deliverymodeIDlist.get(position ));


                try {
                    String name = spinner_delivery_mode.getSelectedItem().toString();

                    for (int k = 0; k < reviewSpinnerPojoArrayList.size(); k++) {
                        String value = reviewSpinnerPojoArrayList.get(k).getName();
                        if (name.equals(value)) {
                            delivery_type = Integer.parseInt(reviewSpinnerPojoArrayList.get(k).getId());

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Intent intent = getIntent();

        for_top = intent.getStringExtra("for_top");
        place_id = intent.getStringExtra("placeid");
        google_id = intent.getStringExtra("googleid");
        for_worst = intent.getStringExtra("for_worst");
        dish_name = intent.getStringExtra("dish_name");
        dish_image = intent.getStringExtra("dish_image");
        Hotel_name = intent.getStringExtra("Hotel_name");
        Hotel_icon = intent.getStringExtra("Hotel_icon");
        Hotel_rating = intent.getStringExtra("Hotel_rating");
        Hotel_location = intent.getStringExtra("Hotel_location");
        ambience_camera = intent.getStringExtra("userimage_camera");
        ambience_gallery = intent.getStringExtra("userimage_gallery");

        Log.e(TAG, "dish_image" + dish_image);
        Log.e(TAG, "dish_name" + dish_name);
        Log.e(TAG, "ambience_gallery" + ambience_gallery);


        try {

            if (Hotel_name == null || Hotel_name.isEmpty()) {

                ll_htl_layout.setVisibility(View.GONE);
            } else {
                ll_htl_layout.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        Image_load_activity.loadGooglePhoto(this, img_restaurant_image, Hotel_icon);

        edt_hoteltxt.setText(Hotel_name);
        txt_hotel_name.setText(Hotel_name);
        txt_hotel_address.setText(Hotel_location);
        rv_img_restaurant.setVisibility(View.GONE);
        img_restaurant_image.setVisibility(View.VISIBLE);

        toolbar_title.setText("Review for " + Hotel_name);

        txt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String edt_hotel_result = edt_hoteltxt.getText().toString();

                if (!edt_hotel_result.equalsIgnoreCase("")) {
                    location_search(edt_hotel_result);
                }

            }
        });


    }

    private void initComponents() {
        // Actionbar
        actionBar = (Toolbar) findViewById(R.id.action_bar_write_post);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        tv_create_review = (TextView) actionBar.findViewById(R.id.tv_create_review);
        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);

        //Scroll view
        scrl_rev = (NestedScrollView) findViewById(R.id.scrl_rev);
        scrl_rev.setOnTouchListener(this);
        btn_addImages = (Button) findViewById(R.id.btn_addImages);

        // Radio group

        radio_category = (RadioGroup) findViewById(R.id.radio_category);
        radio_vegnonveg = (RadioGroup) findViewById(R.id.radio_vegnonveg);


        radio_Veg = (RadioButton) findViewById(R.id.radio_Veg);
        radio_dine = (RadioButton) findViewById(R.id.radio_dine);
        radio_Nonveg = (RadioButton) findViewById(R.id.radio_Nonveg);
        radio_delivery = (RadioButton) findViewById(R.id.radio_delivery);


        edt_hoteltxt = (EditText) findViewById(R.id.edt_hoteltxt);
        tagsedittext = (TagsEditText) findViewById(R.id.tagsedittext);
        tagsedittext_worst = (TagsEditText) findViewById(R.id.tagsedittext_worst);


        packageindicator = (CircleIndicator) findViewById(R.id.packageindicator);
        ambi_review_indicator = (CircleIndicator) findViewById(R.id.ambiindicator);
        ambi_viewpager_review = (ViewPager) findViewById(R.id.ambi_viewpager_review);
        package_viewpager_review = (ViewPager) findViewById(R.id.package_viewpager_review);
        rl_package_dotindicator = (RelativeLayout) findViewById(R.id.rl_package_dotindicator);
        rl_ambiance_dotindicator = (RelativeLayout) findViewById(R.id.rl_ambiance_dotindicator);


        edt_review_description = (EmojiconEditText) findViewById(R.id.edt_review_description);


        image_add_top_dish = (ImageButton) findViewById(R.id.image_add_top_dish);
        img_restaurant_image = (ImageView) findViewById(R.id.img_restaurant_image);


        rv_ambience_dish = (RecyclerView) findViewById(R.id.rv_ambience_dish);
        rv_package_dish = (RecyclerView) findViewById(R.id.rv_package_dish);
        rv_package = (RecyclerView) findViewById(R.id.rv_package);
        rv_top_dish = (RecyclerView) findViewById(R.id.rv_top_dish);
        rv_worst_dish = (RecyclerView) findViewById(R.id.rv_worst_dish);
        rv_img_restaurant = (RecyclerView) findViewById(R.id.rv_img_restaurant);
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);

        btn_create_review = (Button) findViewById(R.id.btn_create_review);
        ll_image_review = (LinearLayout) findViewById(R.id.ll_image_review);
        ll_amb_add_txt = (LinearLayout) findViewById(R.id.ll_amb_add_txt);
        ll_top_dish_add_txt = (LinearLayout) findViewById(R.id.ll_top_dish_add_txt);
        ll_dish_to_avoid_add_txt = (LinearLayout) findViewById(R.id.ll_dish_to_avoid_add_txt);
        ll_packaging_add_txt = (LinearLayout) findViewById(R.id.ll_packaging_add_txt);
        ll_package_add_txt = (LinearLayout) findViewById(R.id.ll_package_add_txt);

        // Mode of delivery layout

        ll_modeofdelivery = (LinearLayout) findViewById(R.id.ll_modeofdelivery);
        ll_review = (LinearLayout) findViewById(R.id.ll_review);
        ll_htl_layout = (LinearLayout) findViewById(R.id.ll_htl_layout);
        ll_edt_add_dish = (LinearLayout) findViewById(R.id.ll_edt_add_dish);
        ll_edt_tag_dish = (LinearLayout) findViewById(R.id.ll_edt_tag_dish);
        ll_edt_tag_worst_dish = (LinearLayout) findViewById(R.id.ll_edt_tag_worst_dish);


        //Spinner mode of delivery

        spinner_delivery_mode = (Spinner) findViewById(R.id.spinner_delivery_mode);


        // Smile Rating

        ratingBar = (SmileRating) findViewById(R.id.rtng_overall);
        rtng_ambiance = (SmileRating) findViewById(R.id.rtng_ambiance);
        rtng_service = (SmileRating) findViewById(R.id.rtng_service);
        rtng_taste = (SmileRating) findViewById(R.id.rtng_taste);
        rtng_value = (SmileRating) findViewById(R.id.rtng_value);

        // TextView

        txt_rating_low = (TextView) findViewById(R.id.txt_rating_low);
        txt_rating_low_ambiance = (TextView) findViewById(R.id.txt_rating_low_ambiance);
        txt_rating_low_taste = (TextView) findViewById(R.id.txt_rating_low_taste);
        txt_rating_low_service = (TextView) findViewById(R.id.txt_rating_low_service);
        txt_rating_low_value = (TextView) findViewById(R.id.txt_rating_low_value);
        txt_hotel_name = (TextView) findViewById(R.id.txt_hotel_name);
        txt_hotel_address = (TextView) findViewById(R.id.txt_hotel_address);
        txt_service = (TextView) findViewById(R.id.txt_service);
        txt_ambi = (TextView) findViewById(R.id.txt_ambi);
        txt_search = (TextView) findViewById(R.id.txt_search);
        txt_ambiance_add = (TextView) findViewById(R.id.txt_ambiance_add);


        image_ambiance = (ImageButton) findViewById(R.id.image_ambiance);
        img_revie = (TouchImageView) findViewById(R.id.img_revie);
        image_add_dish_worst = (ImageButton) findViewById(R.id.image_add_dish_worst);
        image_service = (ImageButton) findViewById(R.id.image_service);
        btn_search = (ImageButton) findViewById(R.id.btn_search);
        img_addimage = (PhotoView) findViewById(R.id.img_addimage);


        back.setOnClickListener(this);
        txt_search.setOnClickListener(this);
        btn_search.setOnClickListener(this);
        img_addimage.setOnClickListener(this);
        btn_addImages.setOnClickListener(this);
        image_service.setOnClickListener(this);
        image_ambiance.setOnClickListener(this);
        txt_ambiance_add.setOnClickListener(this);
        tv_create_review.setOnClickListener(this);
        btn_create_review.setOnClickListener(this);
        image_add_top_dish.setOnClickListener(this);
        image_add_dish_worst.setOnClickListener(this);
        img_restaurant_image.setOnClickListener(this);
    }

    //calling get delicery mode dropdown api
    private void get_delivery_mode() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Get_Deliverymode_Response> call = apiService.get_delivery_mode("get_delivery_mode", 0);
        call.enqueue(new Callback<Get_Deliverymode_Response>() {
            @Override
            public void onResponse(Call<Get_Deliverymode_Response> call, Response<Get_Deliverymode_Response> response) {

                try {
                    deliverymodelist.clear();
                    if (response.body().getResponseMessage().equalsIgnoreCase("success")) {

                        deliverymodelist.add("Select delivery partner");
                        //        deliverymodeIDlist.add("select");
                        for (int i = 0; i < response.body().getData().size(); i++) {

                            String id = response.body().getData().get(i).getModeId();
                            String chassisno = response.body().getData().get(i).getCompName();

                            deliverymodelist.add(chassisno);
                            deliverymodeIDlist.add(id);

                            ReviewSpinner_Pojo reviewSpinner_pojo = new ReviewSpinner_Pojo(chassisno, id);
                            reviewSpinnerPojoArrayList.add(reviewSpinner_pojo);
                        }


                        // Initializing an ArrayAdapter-Shift type
                        ArrayAdapter<String> adapterchassisno = new ArrayAdapter<String>(getApplicationContext(),
                                R.layout.fab_spinner_layout,
                                deliverymodelist) {

                            @Override
                            public boolean isEnabled(int position) {
                                return position != 0;
                            }

                            @Override
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    // Set the hint text color gray
                                    tv.setTextColor(Color.GRAY);
                                } else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };
                        adapterchassisno.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_delivery_mode.setAdapter(adapterchassisno);


                    } else
                        Toast.makeText(ReviewTestActivity.this, "Unable to get response", Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Get_Deliverymode_Response> call, Throwable t) {


            }
        });
    }

    @Override
    public void onSwipeRight() {
        // finish();
    }

    @Override
    protected void onSwipeLeft() {

    }


    /* Initiate Google API Client  */

    private void initGoogleAPIClient() {

        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(ReviewTestActivity.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

    }


    private void cameraStorageGrantRequest() {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                ReviewTestActivity.this);

        builder.setTitle("Food Wall would like to access your camera and storage")
                .setMessage("You previously chose not to use Android's camera or storage with Food Wall. To upload your photos, allow camera and storage permission in App Settings. Click Ok to allow.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // permission dialog
                        dialog.dismiss();
                        try {

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();


    }

    /*  Show Popup to access User Permission  */

    private void requestLocationPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(ReviewTestActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(ReviewTestActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        } else {
            ActivityCompat.requestPermissions(ReviewTestActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }

    }

    // Run on UI
    private Runnable sendUpdatesToUI = new Runnable() {

        public void run() {
            showSettingDialog();
        }

    };


    private void showSettingDialog() {


        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.


                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(ReviewTestActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    // Restaurant location search
    private void location_search(String businessName) {

        apiService = APIClient_restaurant.getClient().create(API_interface_restaurant.class);

        Call<Restaurant_output_pojo> call = apiService.searchRestaurants_reivew(businessName, latLngString, "20000", "restaurant", APIClient_restaurant.GOOGLE_PLACE_API_KEY); /*apiService.location_search("restaurants" + "+" + "in" + "+" + businessName, APIClient_restaurant.GOOGLE_PLACE_API_KEY);*/
        call.enqueue(new Callback<Restaurant_output_pojo>() {
            @Override
            public void onResponse(Call<Restaurant_output_pojo> call, Response<Restaurant_output_pojo> response) {
                Restaurant_output_pojo restaurantOutputPojo = response.body();


                if (response.isSuccessful()) {

                    next_token = restaurantOutputPojo.getNextPageToken();
                    Log.e("token_location", "onResponse" + next_token);

                    if (restaurantOutputPojo.getStatus().equalsIgnoreCase("OK")) {


                        storeModels = new ArrayList<>();
                        restaurantOutputPojoList = new ArrayList<>();
                        getallRestaurantDetailsPojoArrayList = new ArrayList<>();
                        photos = new ArrayList<>();
                        types = new ArrayList<>();
                        htmlAttributions = new ArrayList<>();
                        openingHoursPojoList = new ArrayList<>();


                        Log.e("hotel_size", "" + restaurantOutputPojo.getResults().size());

                        for (int i = 0; i < restaurantOutputPojo.getResults().size(); i++) {


                            String placeid = restaurantOutputPojo.getResults().get(i).getPlaceId();

                            String Icon = restaurantOutputPojo.getResults().get(i).getIcon();
                            String hotel_name = restaurantOutputPojo.getResults().get(i).getName();
                            String Id = restaurantOutputPojo.getResults().get(i).getId();

                            String reference = restaurantOutputPojo.getResults().get(i).getReference();

                            String address = restaurantOutputPojo.getResults().get(i).getFormatted_address();
                            String scope = restaurantOutputPojo.getResults().get(i).getScope();

                            String rating = restaurantOutputPojo.getResults().get(i).getRating();
                            try {
                                String photo_ref = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getPhotoReference();
                                int width = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getWidth();
                                int height = restaurantOutputPojo.getResults().get(i).getPhotos().get(0).getHeight();

                                Get_hotel_photo_pojo getHotelPhotoPojo1 = new Get_hotel_photo_pojo(height, htmlAttributions, photo_ref, width);
                                Log.e("height1", "photo_ref1: " + photo_ref);
                                Log.e("height1", "width1: " + width);
                                Log.e("height1", "height1: " + height);
                                photos.add(getHotelPhotoPojo1);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            for (int kk = 0; kk < restaurantOutputPojo.getResults().get(0).getTypes().size(); kk++) {

                                types.add(restaurantOutputPojo.getResults().get(i).getTypes().toString());

                            }

                            try {

                                double lat = restaurantOutputPojo.getResults().get(0).getGeometry().getLocation().getLatitude();
                                double longit = restaurantOutputPojo.getResults().get(0).getGeometry().getLocation().getLongitude();


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {

                                boolean open_status = restaurantOutputPojo.getResults().get(i).getOpeningHours().getOpenNow();
                                Log.e("open_status", "open_status: " + open_status);

                                openingHoursPojo = new OpeningHours_pojo(open_status, weekdayText);
                                openingHoursPojoList.add(openingHoursPojo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            GetAllRestaurantsPojo getallRestaurantDetailsPojo1 = new GetAllRestaurantsPojo(geometryPojo, Icon, Id,
                                    hotel_name, openingHoursPojo, photos, placeid, rating, reference, scope, types, address);
                            getallRestaurantDetailsPojoArrayList.add(getallRestaurantDetailsPojo1);

                            Review_inside_adapter adapterStores = new Review_inside_adapter(ReviewTestActivity.this, getallRestaurantDetailsPojoArrayList, storeModels, openingHoursPojoList);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(ReviewTestActivity.this, LinearLayoutManager.HORIZONTAL, true);
                            rv_img_restaurant.setLayoutManager(mLayoutManager);
                            mLayoutManager.setStackFromEnd(true);
                            mLayoutManager.scrollToPosition(getallRestaurantDetailsPojoArrayList.size() - 1);
                            rv_img_restaurant.setAdapter(adapterStores);
                            rv_img_restaurant.setItemAnimator(new DefaultItemAnimator());
                            rv_img_restaurant.setNestedScrollingEnabled(false);
                            rv_img_restaurant.setHorizontalScrollBarEnabled(true);
                            adapterStores.notifyDataSetChanged();

                            img_restaurant_image.setVisibility(View.GONE);
                            txt_hotel_name.setVisibility(View.GONE);
                            txt_hotel_address.setVisibility(View.GONE);

                            rv_img_restaurant.setVisibility(View.VISIBLE);
                            ll_htl_layout.setVisibility(View.VISIBLE);


                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "No matches found near you", Toast.LENGTH_SHORT).show();
                    }

                } else if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Error " + response.code() + " found.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<Restaurant_output_pojo> call, Throwable t) {
                // Log error here since request failed
                call.cancel();
            }
        });


    }


    // Custom alert

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            case R.id.back:
                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Confirm")
                        .setContentText("Are you sure you want to skip")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                finish();
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();


                break;

            case R.id.image_ambiance:

                ReviewTestActivity.CustomDialogClass_ambience ccdambience = new ReviewTestActivity.CustomDialogClass_ambience(this);
                ccdambience.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                ccdambience.setCancelable(true);
                ccdambience.show();
                break;


            case R.id.image_service:


                ReviewTestActivity.CustomDialogClass_package ccdservice = new ReviewTestActivity.CustomDialogClass_package(this);
                ccdservice.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                ccdservice.setCancelable(true);
                ccdservice.show();

                break;


            case R.id.btn_search:

                String edt_hotel_result = edt_hoteltxt.getText().toString();

                if (!edt_hotel_result.equalsIgnoreCase("")) {
                    location_search(edt_hotel_result);
                }

                break;

            case R.id.image_add_top_dish:

                ReviewTestActivity.CustomDialogClass_topdish ccdtopdish = new ReviewTestActivity.CustomDialogClass_topdish(this);
                ccdtopdish.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                ccdtopdish.setCancelable(true);
                ccdtopdish.show();

                break;

            case R.id.image_add_dish_worst:


                ReviewTestActivity.CustomDialogClass_avoiddish ccdworst = new ReviewTestActivity.CustomDialogClass_avoiddish(this);
                ccdworst.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                ccdworst.setCancelable(true);
                ccdworst.show();

                break;

            case R.id.tv_create_review:

                topdishresult = tagsedittext.getTags().toString();
                worstdishresult = tagsedittext_worst.getTags().toString();

                Log.e("topdishresult", "topdishresult: " + topdishresult);
                Log.e("topdishresult", "worstdishresult: " + worstdishresult);

                if (validate()) {

                    if (isConnected(ReviewTestActivity.this)) {

                        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ReviewTestActivity.this);
                        LayoutInflater inflater1 = LayoutInflater.from(ReviewTestActivity.this);
                        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.loader_layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setTitle("");
                        progressBar = dialogView.findViewById(R.id.loader_login);
                        progressBar.setIndeterminate(true);
                        prg_textView = dialogView.findViewById(R.id.prg_textView);
                        loaderimage = findViewById(R.id.img_loader);


                        dialog1 = dialogBuilder.create();
                        dialog1.setCancelable(false);
                        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog1.show();


                        if (overall != 0 && taste != 0 && ambiance != 0 && service != 0 && value_money != 0) {

                            int category_type = radio_category.getCheckedRadioButtonId();

                            if (category_type == radio_dine.getId()) {

                                category = 1;

                            } else {

                                category = 2;

                            }

                            int food_type = radio_vegnonveg.getCheckedRadioButtonId();
                            if (food_type == radio_Veg.getId()) {

                                veg_nonveg = 1;

                            } else {

                                veg_nonveg = 2;

                            }


                            if (category == 1) {

                                create_review();

                            } else {

                                create_review_delivery();
                            }


                        } else {


                            if (txt_rating_low.getText().toString().equalsIgnoreCase("0") || txt_rating_low_taste.getText().toString().equalsIgnoreCase("0") || txt_rating_low_ambiance.getText().toString().equalsIgnoreCase("0") || txt_rating_low_service.getText().toString().equalsIgnoreCase("0") || txt_rating_low_value.getText().toString().equalsIgnoreCase("0")) {
                                Toast.makeText(getApplicationContext(), "Please provide all ratings to proceed.", Toast.LENGTH_SHORT).show();
                            }


                            if (topdishresult.equalsIgnoreCase("[]")) {

                                Toast.makeText(getApplicationContext(), "Please provide atleat 1 top dish.", Toast.LENGTH_SHORT).show();
                            }


                            if (!radio_dine.isChecked()) {

                                if (delivery_type_text.equalsIgnoreCase("[]") || delivery_type_text.equalsIgnoreCase("")) {

                                    Toast.makeText(getApplicationContext(), "Please provide the delivery partner.", Toast.LENGTH_SHORT).show();

                                }

                            }


                            if (worstdishresult.equalsIgnoreCase("[]")) {
                                Toast.makeText(getApplicationContext(), "Please provide atleat 1 dish to avoid.", Toast.LENGTH_SHORT).show();
                            }


                        }

                    } else {

                        Snackbar snackbar = Snackbar.make(ll_review, "Something went wrong. Please check your internet connection.", Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        View view1 = snackbar.getView();
                        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                        textview.setTextColor(Color.WHITE);
                        snackbar.show();
                    }

                }


                break;

        }

    }


    //Validation

    private boolean validate() {

        boolean valid = true;

        int category_type = radio_category.getCheckedRadioButtonId();

        if (category_type == radio_dine.getId()) {

            category = 1;

        } else {

            category = 2;

        }


        overall = Integer.parseInt(txt_rating_low.getText().toString());
        ambiance = Integer.parseInt(txt_rating_low_ambiance.getText().toString());
        taste = Integer.parseInt(txt_rating_low_taste.getText().toString());
        service = Integer.parseInt(txt_rating_low_service.getText().toString());
        value_money = Integer.parseInt(txt_rating_low_value.getText().toString());


        if (category == 1) {


            if (overall != 0 && ambiance != 0 && taste != 0 && service != 0 && value_money != 0) {
                valid = true;
            } else {
                if (overall == 0) {

                    valid = false;

                    Toast.makeText(this, "Please rate the overall experience", Toast.LENGTH_SHORT).show();

                } else if (ambiance == 0) {

                    valid = false;
                    Toast.makeText(this, "Please rate the ambiance", Toast.LENGTH_SHORT).show();

                } else if (taste == 0) {

                    valid = false;
                    Toast.makeText(this, "Please rate the taste", Toast.LENGTH_SHORT).show();

                } else if (service == 0) {

                    valid = false;
                    Toast.makeText(this, "Please rate the service", Toast.LENGTH_SHORT).show();

                } else if (value_money == 0) {

                    valid = false;
                    Toast.makeText(this, "Please rate the value for money", Toast.LENGTH_SHORT).show();

                }
            }
        } else {

            if (overall != 0 && ambiance != 0 && taste != 0 && service != 0 && value_money != 0) {
                valid = true;
            } else {
                if (txt_rating_low.getText().toString().equalsIgnoreCase("0")) {

                    valid = false;

                    Toast.makeText(this, "Please rate the overall experience", Toast.LENGTH_SHORT).show();

                } else if (ambiance == 0) {

                    valid = false;
                    Toast.makeText(this, "Please rate the Delivery(On-Time)", Toast.LENGTH_SHORT).show();

                } else if (taste == 0) {

                    valid = false;
                    Toast.makeText(this, "Please rate the taste", Toast.LENGTH_SHORT).show();

                } else if (service == 0) {

                    valid = false;
                    Toast.makeText(this, "Please rate the package", Toast.LENGTH_SHORT).show();

                } else if (value_money == 0) {

                    valid = false;
                    Toast.makeText(this, "Please rate the value for money", Toast.LENGTH_SHORT).show();

                }
            }
        }


        if (!radio_dine.isChecked()) {

            if (delivery_type_text.equalsIgnoreCase("[]") || delivery_type_text.equalsIgnoreCase("") || delivery_type_text.equalsIgnoreCase(Mode_of_delivery[0])) {

                valid = false;

                Toast.makeText(getApplicationContext(), "Please provide the delivery partner.", Toast.LENGTH_SHORT).show();

            }

        }


        if (google_id == null || Hotel_name == null || place_id == null || Hotel_location == null) {

            valid = false;
            Toast.makeText(this, "Please select restaurant", Toast.LENGTH_SHORT).show();

        }


        return valid;
    }

    @Override
    public void removedImageLisenter(List<String> imagesList) {

        if (radio_dine.isChecked()) {

            imagesEncodedList_ambience = imagesList;
            ambience_images();

        } else {

            imagesEncodedList_package = imagesList;
            package_images();

        }

    }

    @Override
    public void removedTopImageLisenter(ArrayList<Review_image_pojo> imagesList) {

        addimagesEncodedList_top = imagesList;
        topDishesImages();


    }

    @Override
    public void removedAvoidImageLisenter(ArrayList<Review_image_pojo> imagesList) {

        addimagesEncodedList_avoid = imagesList;
        avoidDishesImages();


    }

    @Override
    public void removedAmbienceImageLisenter(ArrayList<Review_image_pojo> imagesList) {
        addimagesEncodedList_ambience = imagesList;
        ambience_images();

    }


    private class timetouploadimage extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {


            try {
                Thread.sleep(5000);
                Log.e("tested", "tested");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Executed";
        }


        @Override
        protected void onPostExecute(String result) {
            Intent intent = new Intent(new Intent(ReviewTestActivity.this, Home.class));
            intent.putExtra("redirect", "from_review");
            startActivity(intent);
            finish();
            dialog1.dismiss();

        }

        @Override
        protected void onPreExecute() {

        }

    }
    //Rating bar listener

    public void addListenerOnRatingBar() {

        ratingBar.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {


                txt_rating_low.setText(String.valueOf(level));
                overall = Integer.parseInt(txt_rating_low.getText().toString());

            }
        });

        rtng_ambiance.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_ambiance.setText(String.valueOf(level));

                ambiance = Integer.parseInt(txt_rating_low_ambiance.getText().toString());
            }
        });


        rtng_service.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_service.setText(String.valueOf(level));
                service = Integer.parseInt(txt_rating_low_service.getText().toString());
            }
        });

        rtng_taste.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_taste.setText(String.valueOf(level));
                taste = Integer.parseInt(txt_rating_low_taste.getText().toString());
            }
        });

        rtng_value.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                txt_rating_low_value.setText(String.valueOf(level));
                value_money = Integer.parseInt(txt_rating_low_value.getText().toString());

            }
        });

    }


    // calling create_hotel_review_dish_image api for top dish and avoid dish names
    public void create_top_dishes_withoutimages(List<String> DishList, int dishtype) {
        Log.e("chargable", "" + DishList.toString());


        for (int i = 0; i < DishList.size(); i++) {
            try {
                Log.e("chargable", "" + DishList.get(i));
                withoutimage = new JSONObject();
                withoutimage.put("dishname", DishList.get(i));
                dishnameWithoutImage.add(withoutimage);
                Log.e("chargable", "" + dishnameWithoutImage.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<CommonOutputPojo> call = apiService.create_hotel_review_dish_imageWithoutImage("create_hotel_review_dish_image", 1, 0,
                hotel_id, review_id, createdby, dishtype, String.valueOf(dishnameWithoutImage));

        call.enqueue(new Callback<CommonOutputPojo>() {
            @Override
            public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                if (response.body().getResponseCode() == 1) {

                    if (addimagesEncodedList_top.size() == 0 && addimagesEncodedList_avoid.size() == 0) {

                        Toast.makeText(ReviewTestActivity.this, "tested", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(new Intent(ReviewTestActivity.this, Home.class));
                        intent.putExtra("redirect", "from_review");
                        startActivity(intent);
                        //finish();
                        dialog1.dismiss();

                    }

                } else {


                }
            }

            @Override
            public void onFailure(Call<CommonOutputPojo> call, Throwable t) {


            }
        });

    }


    // API create_review

    public void create_review() {


        if (isConnected(ReviewTestActivity.this)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String createuserid = Pref_storage.getDetail(ReviewTestActivity.this, "userId");

            if (dish_image == null) {

                image = Hotel_icon;

            } else {

                image = dish_image;

            }


            Call<Create_review_output_pojo> call = apiService.create_hotel_review("create_hotel_review", google_id, Hotel_name, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_review_description.getText().toString().trim()), category, veg_nonveg,
                    place_id, "", Hotel_location, latitude, longitude, 0, 0, image, Hotel_icon, overall, ambiance, taste, service, value_money, Integer.parseInt(createuserid));
            call.enqueue(new Callback<Create_review_output_pojo>() {
                @Override
                public void onResponse(Call<Create_review_output_pojo> call, Response<Create_review_output_pojo> response) {

                    if (response.body().getResponseCode() == 1) {


                        hotel_id = response.body().getData().get(0).getHotelid();
                        review_id = response.body().getData().get(1).getReviewid();

                        Log.e("review-->", "hotel_id" + hotel_id);
                        Log.e("review-->", "review_id" + review_id);
                        Log.e("review-->", "addimagesEncodedList_top" + addimagesEncodedList_top.toString());
                        Log.e("review-->", "addimagesEncodedList_topSize" + addimagesEncodedList_top.size());
                        Log.e("review-->", "addimagesEncodedList_avoid" + addimagesEncodedList_avoid.toString());
                        Log.e("review-->", "addimagesEncodedList_avoidsize" + addimagesEncodedList_avoid.size());

                        try {


                            if (addimagesEncodedList_top.size() == 0 &&
                                    addimagesEncodedList_avoid.size() == 0 &&
                                    addimagesEncodedList_ambience.size() == 0
                                    && imagesEncodedList_package.size() == 0
                                    && topDishList.size() == 0 && avoidDishList.size() == 0) {


                                Intent intent = new Intent(new Intent(ReviewTestActivity.this, Home.class));
                                intent.putExtra("redirect", "from_review");
                                startActivity(intent);
                                finish();
                                dialog1.dismiss();

                            }

                            //call without api
                            if (topDishList.size() != 0) {

                                // create_top_dishes_without_images();
                                //viji
                                Log.e("chargable", "" + topDishList.toString());
                                create_top_dishes_withoutimages(topDishList, 1);
                            }

                            if (avoidDishList.size() != 0) {

                                //  create_avoid_dishes_without_images();
                                //viji
                                Log.e("chargable", "" + avoidDishList.toString());
                                create_top_dishes_withoutimages(avoidDishList, 2);
                            }

                            if (addimagesEncodedList_top.size() > 0) {

                                addimagesEncodedList_top.size();
                                create_hotel_review_dish_image_new(addimagesEncodedList_top, 1);
                            }

                            if (addimagesEncodedList_avoid.size() > 0) {

                                addimagesEncodedList_avoid.size();
                                create_hotel_review_dish_image_new(addimagesEncodedList_avoid, 2);
                                //create_hotel_review_dish_to_avoid();


                            }

                            if (addimagesEncodedList_ambience.size() > 0) {

                                addimagesEncodedList_ambience.size();
                                create_hotel_review_ambiance_image(addimagesEncodedList_ambience, 2);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                @Override
                public void onFailure(Call<Create_review_output_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                }
            });


        } else {

            Snackbar snackbar = Snackbar.make(ll_review, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    // API create_review_delivery

    public void create_review_delivery() {

        if (isConnected(ReviewTestActivity.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String createuserid = Pref_storage.getDetail(ReviewTestActivity.this, "userId");

            if (dish_image == null) {

                image = Hotel_icon;

            } else {

                image = dish_image;

            }


            Call<Create_review_output_pojo> call = apiService.create_review_delivery("create_hotel_review", google_id, Hotel_name, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_review_description.getText().toString().trim()), category, veg_nonveg,
                    place_id, "", Hotel_location, latitude, longitude, 0, 0, image, Hotel_icon, overall, ambiance, taste, service, delivery_type, value_money, Integer.parseInt(createuserid));


            call.enqueue(new Callback<Create_review_output_pojo>() {
                @Override
                public void onResponse(Call<Create_review_output_pojo> call, Response<Create_review_output_pojo> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {

                        hotel_id = response.body().getData().get(0).getHotelid();
                        review_id = response.body().getData().get(1).getReviewid();


                        try {

                            if (topDishList.size() != 0) {

                                create_top_dishes_withoutimages(topDishList, 1);
                            }

                            if (avoidDishList.size() != 0) {

                                create_top_dishes_withoutimages(avoidDishList, 2);
                            }

                            if (addimagesEncodedList_top.size() == 0 &&
                                    addimagesEncodedList_avoid.size() == 0 &&
                                    addimagesEncodedList_packaging.size() == 0
                                    && topDishList.size() == 0 && avoidDishList.size() == 0) {


                                Intent intent = new Intent(new Intent(ReviewTestActivity.this, Home.class));
                                intent.putExtra("redirect", "from_review");
                                startActivity(intent);
                                finish();
                                dialog1.dismiss();

                            }

                            if (addimagesEncodedList_top.size() > 0) {

                                addimagesEncodedList_top.size();
                                create_hotel_review_dish_image_new(addimagesEncodedList_top, 1);
                            }

                            if (addimagesEncodedList_avoid.size() > 0) {

                                addimagesEncodedList_avoid.size();
                                create_hotel_review_dish_image_new(addimagesEncodedList_avoid, 2);
                            }


                            // Package Upload addimagesEncodedList_packaging

                            if (addimagesEncodedList_packaging.size() > 0) {

                                addimagesEncodedList_packaging.size();
                                create_hotel_review_package_image(addimagesEncodedList_packaging, 3);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                @Override
                public void onFailure(Call<Create_review_output_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    dialog1.dismiss();

                }
            });


        } else {

            Snackbar snackbar = Snackbar.make(ll_review, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    // Upload top dishes image API
    public void create_hotel_review_dish_image_new(List<Review_image_pojo> imagelist, final int dishtype) {

        if (isConnected(ReviewTestActivity.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String createdby = Pref_storage.getDetail(this, "userId");


            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("hotel_id", hotel_id);
            builder.addFormDataPart("review_id", review_id);
            builder.addFormDataPart("img_status", String.valueOf(1));
            builder.addFormDataPart("img_type", String.valueOf(1));
            builder.addFormDataPart("created_by", createdby);
            builder.addFormDataPart("dishtype", String.valueOf(dishtype));

            for (int i = 0; i < imagelist.size(); i++) {
                try {

                    dishnameView = new JSONObject();
                    dishnameView.put("dishname", imagelist.get(i).getImage_text());
                    dishnameWithImage.add(dishnameView);
                    Log.e("chargable", "" + dishnameWithImage.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            builder.addFormDataPart("dishname", String.valueOf(dishnameWithImage));
            Log.e("chargable", "" + dishnameWithImage.toString());

            //UserID
            RequestBody requestBody1 = null;
            for (int i = 0, size = imagelist.size(); i < size; i++) {
                File f = new File(imagelist.get(i).getImage());
                requestBody1 = RequestBody.create(MediaType.parse("image/*"), f);
                builder.addFormDataPart("image" + "[" + i + "]", f.getName(), requestBody1);
                Log.d(TAG, "requestUploadSurvey: survey image " + imagelist.get(i).getImage());
            }

            MultipartBody requestBody = builder.build();

            Call<CommonOutputPojo> call = apiService.create_hotel_review_dish_image_new("create_hotel_review_dish_image", requestBody);
            //final int finalI = i;
            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    if (response.body().getResponseCode() == 1) {

                        if (dishtype != 2) {

                            if (addimagesEncodedList_ambience.size() == 0 && addimagesEncodedList_avoid.size() == 0) {

                                Intent intent = new Intent(new Intent(ReviewTestActivity.this, Home.class));
                                intent.putExtra("redirect", "from_review");
                                startActivity(intent);
                                //finish();

                            }

                        } else {

                            if (addimagesEncodedList_ambience.size() == 0) {

                                Intent intent = new Intent(new Intent(ReviewTestActivity.this, Home.class));
                                intent.putExtra("redirect", "from_review");
                                startActivity(intent);
                                //finish();
                                //dialog1.dismiss();
                            }

                        }

                        Log.e("success", "Top dish upload");
                    }
                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                }
            });

            //}

        } else {

            Snackbar snackbar = Snackbar.make(ll_review, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    public void create_hotel_review_ambiance_image(List<Review_image_pojo> imagePojoList, int img_type) {


        if (isConnected(ReviewTestActivity.this)) {

            String createdby = Pref_storage.getDetail(this, "userId");

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("img_type", String.valueOf(img_type));
            builder.addFormDataPart("hotel_id", hotel_id);
            builder.addFormDataPart("review_id", review_id);
            builder.addFormDataPart("created_by", createdby);


            String[] stringsArrayAmbi = new String[]{};

            RequestBody requestBody1 = null;
            for (int i = 0, size = imagePojoList.size(); i < size; i++) {
                File f = new File(imagePojoList.get(i).getImage());
                requestBody1 = RequestBody.create(MediaType.parse("image/*"), f);
                builder.addFormDataPart("amb_image" + "[" + i + "]", f.getName(), requestBody1);
                Log.d(TAG, "requestUploadSurvey: survey image " + imagePojoList.get(i).getImage());

            }


            MultipartBody requestBody = builder.build();
            //Call<CommonOutputPojo> call = apiService.create_hotel_review_dish_imageWithImageambience(methodName, hotelID, reviewId, img_typeView, createdbyUser, surveyImagesParts);
            Call<CommonOutputPojo> call = apiService.create_hotel_review_dish_imageWithImageambience("create_hotel_review_dish_image", requestBody);

            // final int finalI = i;
            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    if (response.body().getResponseCode() == 1) {

                          /*  startActivity(new Intent(ReviewTestActivity.this, Home.class));
                            finish();*/
                        //new timetouploadimage().execute();

                        Intent intent = new Intent(new Intent(ReviewTestActivity.this, Home.class));
                        intent.putExtra("redirect", "from_review");
                        startActivity(intent);
                        finish();
                        dialog1.dismiss();

                           /*if (finalI == 0) {

                                if (addimagesEncodedList_avoid.size() == 0 && addimagesEncodedList_top.size() == 0) {

                                }
                            }*/
                        Log.e("success", "ambiance Upload");

                    }
                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                }
            });

            //}

        } else {

            Snackbar snackbar = Snackbar.make(ll_review, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }

    // Package image upload

    public void create_hotel_review_package_image(ArrayList<Review_image_pojo> addimagesEncodedList_packaging, int imagestatus) {


        if (isConnected(ReviewTestActivity.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("hotel_id", hotel_id);
            builder.addFormDataPart("review_id", review_id);
            builder.addFormDataPart("img_type", String.valueOf(imagestatus));

            //UserID
            String createdby = Pref_storage.getDetail(this, "userId");
            builder.addFormDataPart("created_by", createdby);

            //Image to upload
            Log.e(TAG, "packagingarrylist" + new Gson().toJson(addimagesEncodedList_packaging));

            RequestBody requestBody1 = null;
            for (int i = 0, size = addimagesEncodedList_packaging.size(); i < size; i++) {
                File f = new File(addimagesEncodedList_packaging.get(i).getImage());
                requestBody1 = RequestBody.create(MediaType.parse("image/*"), f);
                builder.addFormDataPart("pack_image" + "[" + i + "]", f.getName(), requestBody1);
                Log.d(TAG, "requestUploadSurvey: survey image " + addimagesEncodedList_packaging.get(i).getImage());

            }


            MultipartBody requestBody = builder.build();
            Call<CommonOutputPojo> call = apiService.create_hotel_review_ambiance_image("create_hotel_review_dish_image", requestBody);


            //final int finalI = i;
            call.enqueue(new Callback<CommonOutputPojo>() {
                @Override
                public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                    if (response.body().getResponseCode() == 1) {

                           /* startActivity(new Intent(ReviewTestActivity.this, Home.class));
                            finish();*/
                        Log.e("success", "package Upload");
                        Intent intent = new Intent(new Intent(ReviewTestActivity.this, Home.class));
                        intent.putExtra("redirect", "from_review");
                        startActivity(intent);
                        finish();
                        dialog1.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                }
            });


        } else {

            Snackbar snackbar = Snackbar.make(ll_review, "Uploading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }

    private void topDishesImages() {

        Review_image_top_dish_adapter reviewImageTopDishAdapter = new Review_image_top_dish_adapter(ReviewTestActivity.this, addimagesEncodedList_top, this, edt_food_caption);
        // LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rv_top_dish.setLayoutManager(mLayoutManager);
        rv_top_dish.setHasFixedSize(true);
        rv_top_dish.setAdapter(reviewImageTopDishAdapter);
        reviewImageTopDishAdapter.notifyDataSetChanged();
        edt_food_caption.setFocusable(true);
        edt_food_caption.setFocusableInTouchMode(true);
    }


    private void avoidDishesImages() {

        Review_image_worst_dish_adapter reviewImageWorstDishAdapter = new Review_image_worst_dish_adapter(ReviewTestActivity.this, addimagesEncodedList_avoid, this, edt_food_caption);
        //  LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rv_worst_dish.setLayoutManager(mLayoutManager);
        rv_worst_dish.setHasFixedSize(true);
        rv_worst_dish.setAdapter(reviewImageWorstDishAdapter);
        reviewImageWorstDishAdapter.notifyDataSetChanged();
        edt_food_caption.setFocusable(true);
        edt_food_caption.setFocusableInTouchMode(true);
    }

    // Ambience_images

    private void ambience_images() {

        Review_ambience_image_adapter ambience_image_adapter = new Review_ambience_image_adapter(ReviewTestActivity.this, addimagesEncodedList_ambience, this);
        // LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rv_ambience_dish.setLayoutManager(mLayoutManager);
        rv_ambience_dish.setHasFixedSize(true);
        rv_ambience_dish.setAdapter(ambience_image_adapter);
        ambience_image_adapter.notifyDataSetChanged();


    }


    //package_images

    private void package_images() {


        Review_packaging_dish_adapter packaging_dish_adapter = new Review_packaging_dish_adapter(ReviewTestActivity.this, addimagesEncodedList_packaging, this);
        // LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rv_package_dish.setLayoutManager(mLayoutManager);
        rv_package_dish.setHasFixedSize(true);
        rv_package_dish.setAdapter(packaging_dish_adapter);
        packaging_dish_adapter.notifyDataSetChanged();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        int permissionSize = permissions.length;

        Log.e(TAG, "onRequestPermissionsResult:" + "requestCode->" + requestCode);

        switch (requestCode) {

            case CAPTURE_IMAGE_CALLBACK_TOP:

                for (int i = 0; i < permissionSize; i++) {

                    if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission granted");

                    } else if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission denied");
                        cameraStorageGrantRequest();
                    }

                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAPTURE_IMAGE_CALLBACK_TOP);

                            } else {

                                ContentValues values = new ContentValues();
                                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                                imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                startActivityForResult(intent, CAPTURE_IMAGE_CALLBACK_TOP);
                            }
                        }


                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraStorageGrantRequest();

                        }

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;


            case CAPTURE_IMAGE_CALLBACK_AVOID:


                for (int i = 0; i < permissionSize; i++) {

                    if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission granted");

                    } else if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission denied");
                        cameraStorageGrantRequest();
                    }

                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAPTURE_IMAGE_CALLBACK_AVOID);

                            } else {

                                ContentValues values = new ContentValues();
                                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                                imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                startActivityForResult(intent, CAPTURE_IMAGE_CALLBACK_AVOID);
                            }
                        }


                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraStorageGrantRequest();

                        }

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;


            case CAPTURE_IMAGE_CALLBACK_AMBIENCE:


                for (int i = 0; i < permissionSize; i++) {

                    if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission granted");

                    } else if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission denied");
                        cameraStorageGrantRequest();
                    }

                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAPTURE_IMAGE_CALLBACK_AMBIENCE);

                            } else {

                                ContentValues values = new ContentValues();
                                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                                imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                startActivityForResult(intent, CAPTURE_IMAGE_CALLBACK_AMBIENCE);
                            }
                        }


                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraStorageGrantRequest();

                        }

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;


            case CAPTURE_IMAGE_CALLBACK_PACKAGE:


                for (int i = 0; i < permissionSize; i++) {

                    if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission granted");

                    } else if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission denied");
                        cameraStorageGrantRequest();
                    }

                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAPTURE_IMAGE_CALLBACK_PACKAGE);

                            } else {

                                ContentValues values = new ContentValues();
                                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                                imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                startActivityForResult(intent, CAPTURE_IMAGE_CALLBACK_PACKAGE);
                            }
                        }


                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraStorageGrantRequest();

                        }

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;


            case PICK_IMAGE_MULTIPLE_TOP:

                for (int i = 0; i < permissionSize; i++) {


                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");
                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_MULTIPLE_TOP);

                        } else {

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE_TOP);
                        }

                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");
                        cameraStorageGrantRequest();

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

            case PICK_IMAGE_MULTIPLE_AVOID:

                for (int i = 0; i < permissionSize; i++) {


                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");
                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_MULTIPLE_AVOID);

                        } else {

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE_AVOID);
                        }

                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");
                        cameraStorageGrantRequest();

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;


            case PICK_IMAGE_MULTIPLE_AMBIENCE:

                for (int i = 0; i < permissionSize; i++) {


                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");
                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_MULTIPLE_AMBIENCE);

                        } else {

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE_AMBIENCE);
                        }

                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");
                        cameraStorageGrantRequest();

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;


            case PICK_IMAGE_MULTIPLE_PACKAGE:

                for (int i = 0; i < permissionSize; i++) {


                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");
                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_MULTIPLE_PACKAGE);

                        } else {

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE_PACKAGE);
                        }

                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");
                        cameraStorageGrantRequest();

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {

            super.onActivityResult(requestCode, resultCode, data);

            switch (requestCode) {

                case PICK_IMAGE_MULTIPLE_TOP:

                    if (resultCode == RESULT_OK && data != null) {

                        imagesEncodedList_top = new ArrayList<String>();

                        if (data.getData() != null) {

                            imagesEncodedList_top = new ArrayList<String>();

                            String[] filePathColumn = {MediaStore.Images.Media.DATA};

                            Uri mImageUri = data.getData();
                            Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                            cursor.moveToFirst();
                            String document_id = cursor.getString(0);
                            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                            cursor.close();
                            cursor = getContentResolver().query(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                            cursor.moveToFirst();


                            final String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                            imagesEncodedList_top.add(path);
                            cursor.close();


                            // View POPUP

                            final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                            LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                            @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                            dialogBuilder1.setCancelable(false);
                            dialogBuilder1.setView(dialogView1);
                            dialogBuilder1.setTitle("");

                            done_btn = dialogView1.findViewById(R.id.done_btn);
                            rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                            cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                            img_smiley = dialogView1.findViewById(R.id.img_smiley);
                            img_food = dialogView1.findViewById(R.id.img_food);
                            edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                            edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);


                            edt_food_caption.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {

                                    String str = s.toString();

                                    if (str.equals(" ")) {

                                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                                        edt_food_caption.setText(edt_food_caption.getText().toString().replaceAll(" ", ""));
                                        edt_food_caption.setSelection(edt_food_caption.getText().length());

                                    }
                                }
                            });


                            emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                            emojIcon.ShowEmojIcon();
                            emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

                            final AlertDialog dialog1 = dialogBuilder1.create();
                            dialog1.show();


                            GlideApp.with(getApplicationContext()).load(path).centerInside().into(img_food);


                            cancel_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    dialog1.dismiss();

                                }
                            });

                            done_btn.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View view) {

                                    if (edt_food_caption.getText().toString().trim().equalsIgnoreCase("")) {

                                        Toast.makeText(ReviewTestActivity.this, "Dish name is required", Toast.LENGTH_SHORT).show();

                                    } else {

                                        dialog1.dismiss();

                                        //  ll_top_dish_add_txt.setVisibility(View.GONE);
                                        Review_image_pojo reviewImagePojo = new Review_image_pojo(path, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                        addimagesEncodedList_top.add(reviewImagePojo);
                                        topDishesImages();
                                    }
                                }
                            });


                        } else if (data.getClipData() != null) {


                            final ClipData mClipData = data.getClipData();
                            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                            for (int i = 0; i < mClipData.getItemCount(); i++) {

                                ClipData.Item item = mClipData.getItemAt(i);
                                Uri uri = item.getUri();
                                mArrayUri.add(uri);

                                // Get the cursor

                                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                                cursor.moveToFirst();
                                String document_id = cursor.getString(0);
                                document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                                cursor.close();

                                cursor = getContentResolver().query(
                                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                        null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                                cursor.moveToFirst();
                                final String imageEncoded = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));


                                imagesEncodedList_top.add(imageEncoded);
                                Log.e("text", "ViewText--------->  " + imagesEncodedList_top.size());

                                cursor.close();


                                final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                                LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                                @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                                dialogBuilder1.setCancelable(false);
                                dialogBuilder1.setView(dialogView1);
                                dialogBuilder1.setTitle("");

                                done_btn = dialogView1.findViewById(R.id.done_btn);
                                done_btn.setVisibility(View.GONE);
                                rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                                cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                                img_smiley = dialogView1.findViewById(R.id.img_smiley);
                                img_food = dialogView1.findViewById(R.id.img_food);
                                edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                                edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);


                                edt_food_caption.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {


                                    }

                                    @Override
                                    public void afterTextChanged(Editable editable) {


                                        String str = editable.toString();

                                        if (str.equals(" ")) {

                                            edt_food_caption.setText(editable.toString());
                                            edt_food_caption.setSelection(editable.toString().length());
                                            Log.e("text", "ViewText--------->  " + editable.toString());

                                        }
                                    }
                                });


                                emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                                emojIcon.ShowEmojIcon();
                                emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);


                                final AlertDialog dialog1 = dialogBuilder1.create();
                                dialog1.show();


                                GlideApp.with(getApplicationContext()).load(imageEncoded).centerInside().into(img_food);

                                cancel_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        dialog1.dismiss();

                                    }
                                });


                                edt_food_caption.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                    @Override
                                    public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                                        boolean handled = false;

                                        if (actionId == EditorInfo.IME_ACTION_GO) {

                                            edt_food_caption.setText(view.getText().toString());
                                            edt_food_caption.setSelection(view.getText().length());

                                            if (edt_food_caption.getText().toString().equalsIgnoreCase("")) {

                                                Toast.makeText(ReviewTestActivity.this, "Dish name is required", Toast.LENGTH_SHORT).show();

                                            } else {


                                                dialog1.dismiss();
                                                //   ll_top_dish_add_txt.setVisibility(View.GONE);
                                                Review_image_pojo reviewImagePojo = new Review_image_pojo(imageEncoded, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                                addimagesEncodedList_top.add(reviewImagePojo);
                                                topDishesImages();
                                            }

                                            handled = true;
                                        }
                                        return handled;
                                    }
                                });


                                done_btn.setOnClickListener(new View.OnClickListener() {
                                    @SuppressLint("ResourceType")
                                    @Override
                                    public void onClick(View view) {


                                        if (edt_food_caption.getText().toString().equalsIgnoreCase("")) {

                                            Toast.makeText(ReviewTestActivity.this, "Dish name is required", Toast.LENGTH_SHORT).show();

                                        } else {

                                            dialog1.dismiss();
                                            //   ll_top_dish_add_txt.setVisibility(View.GONE);
                                            Review_image_pojo reviewImagePojo = new Review_image_pojo(imageEncoded, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                            addimagesEncodedList_top.add(reviewImagePojo);
                                            topDishesImages();
                                        }

                                    }
                                });


                                Log.e("Gallery_ambience-->", "Multi" + imageEncoded);
                            }
                            Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                        }
                    } else {
                        Toast.makeText(this, "You haven't picked Image",
                                Toast.LENGTH_LONG).show();
                    }

                    break;


                case CAPTURE_IMAGE_CALLBACK_TOP:


                    Log.e(TAG, "onActivityResult: requestCode" + requestCode);
                    Log.e(TAG, "onActivityResult: resultCode" + resultCode);
                    Log.e(TAG, "onActivityResult: data" + data);

                    if (requestCode == CAPTURE_IMAGE_CALLBACK_TOP) {


                        try {

                            Bitmap thumbnail = null;
                            try {
                                thumbnail = getThumbnail(imageUri);
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.e("CameraCaptureError", "CameraCaptureError->" + e.getMessage());
                            }


                            String folder_main = "FoodWall";
                            String imagepath = "";


                            File f = new File(Environment.getExternalStorageDirectory(), folder_main);
                            if (!f.exists()) {
                                f.mkdirs();
                            }

                            File newFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                            Log.e("ImageFile", "ImageFile->" + newFile);

                            try {

                                FileOutputStream fileOutputStream = new FileOutputStream(newFile);
                                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                imagepath = String.valueOf(newFile);
                                Log.e("MyPath", "MyImagePath->" + imagepath);

                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            final String camerapath = imagepath;

                            // View Popup

                            final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                            LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                            @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                            dialogBuilder1.setCancelable(false);
                            dialogBuilder1.setView(dialogView1);
                            dialogBuilder1.setTitle("");

                            done_btn = dialogView1.findViewById(R.id.done_btn);
                            rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                            cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                            img_smiley = dialogView1.findViewById(R.id.img_smiley);
                            img_food = dialogView1.findViewById(R.id.img_food);
                            edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                            edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);

                            edt_food_caption.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    String str = s.toString();

                                    if (str.equals(" ")) {

                                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                                        edt_food_caption.setText(edt_food_caption.getText().toString().replaceAll(" ", ""));
                                        edt_food_caption.setSelection(edt_food_caption.getText().length());

                                    }
                                }
                            });

                            emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                            emojIcon.ShowEmojIcon();
                            emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

                            final AlertDialog dialog1 = dialogBuilder1.create();
                            dialog1.show();


                            img_food.setImageURI(Uri.parse(camerapath));


                            cancel_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    dialog1.dismiss();

                                }
                            });


                            done_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                    if (edt_food_caption.getText().toString().equalsIgnoreCase("")) {

                                        Toast.makeText(ReviewTestActivity.this, "Dish name is required", Toast.LENGTH_SHORT).show();
                                    } else {

                                        dialog1.dismiss();

                                        //  ll_top_dish_add_txt.setVisibility(View.GONE);
                                        Review_image_pojo reviewImagePojo = new Review_image_pojo(camerapath, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                        addimagesEncodedList_top.add(reviewImagePojo);
                                        topDishesImages();

                                    }


                                }
                            });


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                    } else {
                        Toast.makeText(this, "You haven't picked Image",
                                Toast.LENGTH_LONG).show();
                    }


                    break;

                // Pick gallery avoid

                case PICK_IMAGE_MULTIPLE_AVOID:

                    if (resultCode == RESULT_OK && data != null) {

                        imagesEncodedList_avoid = new ArrayList<String>();


                        if (data.getData() != null) {

                            imagesEncodedList_avoid = new ArrayList<String>();


                            String[] filePathColumn = {MediaStore.Images.Media.DATA};

                            Uri mImageUri = data.getData();


                            Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                            cursor.moveToFirst();
                            String document_id = cursor.getString(0);
                            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                            cursor.close();

                            cursor = getContentResolver().query(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                            cursor.moveToFirst();
                            final String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));


                            imagesEncodedList_avoid.add(path);

                            cursor.close();


                            //View POPUP

                            final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                            LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                            @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                            dialogBuilder1.setCancelable(false);
                            dialogBuilder1.setView(dialogView1);
                            dialogBuilder1.setTitle("");

                            done_btn = dialogView1.findViewById(R.id.done_btn);
                            rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                            cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                            img_smiley = dialogView1.findViewById(R.id.img_smiley);
                            img_food = dialogView1.findViewById(R.id.img_food);
                            edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                            edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);

                            edt_food_caption.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    String str = s.toString();

                                    if (str.equals(" ")) {

                                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                                        edt_food_caption.setText(edt_food_caption.getText().toString().replaceAll(" ", ""));
                                        edt_food_caption.setSelection(edt_food_caption.getText().length());

                                    }
                                }
                            });


                            emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                            emojIcon.ShowEmojIcon();
                            emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);


                            final AlertDialog dialog1 = dialogBuilder1.create();
                            dialog1.show();

                            GlideApp.with(getApplicationContext()).load(path).centerInside().into(img_food);


                            cancel_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    dialog1.dismiss();

                                }
                            });


                            done_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                    if (edt_food_caption.getText().toString().equalsIgnoreCase("")) {

                                        Toast.makeText(ReviewTestActivity.this, "Dish name is required", Toast.LENGTH_SHORT).show();
                                    } else {

                                        dialog1.dismiss();

                                        //  ll_dish_to_avoid_add_txt.setVisibility(View.GONE);
                                        Review_image_pojo reviewImagePojo = new Review_image_pojo(path, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                        addimagesEncodedList_avoid.add(reviewImagePojo);

                                        avoidDishesImages();
                                    }


                                }
                            });


                        } else if (data.getClipData() != null) {


                            ClipData mClipData = data.getClipData();
                            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                            for (int i = 0; i < mClipData.getItemCount(); i++) {

                                ClipData.Item item = mClipData.getItemAt(i);
                                Uri uri = item.getUri();
                                mArrayUri.add(uri);

                                // Get the cursor

                                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                                cursor.moveToFirst();
                                String document_id = cursor.getString(0);
                                document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                                cursor.close();

                                cursor = getContentResolver().query(
                                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                        null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                                cursor.moveToFirst();
                                final String imageEncoded = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));


                                imagesEncodedList_avoid.add(imageEncoded);

                                cursor.close();

                                final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                                LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                                @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                                dialogBuilder1.setCancelable(false);
                                dialogBuilder1.setView(dialogView1);
                                dialogBuilder1.setTitle("");

                                done_btn = dialogView1.findViewById(R.id.done_btn);
                                done_btn.setVisibility(View.GONE);
                                rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                                cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                                img_smiley = dialogView1.findViewById(R.id.img_smiley);
                                img_food = dialogView1.findViewById(R.id.img_food);
                                edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                                edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);

                                edt_food_caption.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        String str = s.toString();

                                        if (str.equals(" ")) {

                                            Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                                            edt_food_caption.setText(edt_food_caption.getText().toString().replaceAll(" ", ""));
                                            edt_food_caption.setSelection(edt_food_caption.getText().length());

                                        }
                                    }
                                });

                                emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                                emojIcon.ShowEmojIcon();
                                emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

                                final AlertDialog dialog1 = dialogBuilder1.create();
                                dialog1.show();

                                GlideApp.with(getApplicationContext()).load(imageEncoded).centerInside().into(img_food);

                                edt_food_caption.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                    @Override
                                    public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                                        boolean handled = false;

                                        if (actionId == EditorInfo.IME_ACTION_GO) {

                                            edt_food_caption.setText(view.getText().toString());
                                            edt_food_caption.setSelection(view.getText().length());

                                            if (edt_food_caption.getText().toString().equalsIgnoreCase("")) {

                                                Toast.makeText(ReviewTestActivity.this, "Dish name is required", Toast.LENGTH_SHORT).show();

                                            } else {

                                                dialog1.dismiss();
                                                //   ll_top_dish_add_txt.setVisibility(View.GONE);
                                                Review_image_pojo reviewImagePojo = new Review_image_pojo(imageEncoded, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                                addimagesEncodedList_avoid.add(reviewImagePojo);
                                                avoidDishesImages();

                                            }

                                            handled = true;
                                        }
                                        return handled;
                                    }
                                });

                                cancel_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        dialog1.dismiss();

                                    }
                                });


                                done_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {


                                        if (edt_food_caption.getText().toString().equalsIgnoreCase("")) {

                                            Toast.makeText(ReviewTestActivity.this, "Dish name is required", Toast.LENGTH_SHORT).show();
                                        } else {

                                            dialog1.dismiss();

                                            // ll_dish_to_avoid_add_txt.setVisibility(View.GONE);
                                            Review_image_pojo reviewImagePojo = new Review_image_pojo(imageEncoded, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                            addimagesEncodedList_avoid.add(reviewImagePojo);
                                            avoidDishesImages();


                                        }


                                    }
                                });


                                Log.e("Gallery_ambience-->", "Multi" + imageEncoded);


                            }
                            Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                        }
                    } else {
                        Toast.makeText(this, "You haven't picked Image",
                                Toast.LENGTH_LONG).show();
                    }

                    break;


                case CAPTURE_IMAGE_CALLBACK_AVOID:


                    if (requestCode == CAPTURE_IMAGE_CALLBACK_AVOID) {


                        try {

                            Bitmap thumbnail = null;
                            try {
                                thumbnail = getThumbnail(imageUri);
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.e("CameraCaptureError", "CameraCaptureError->" + e.getMessage());
                            }


                            String folder_main = "FoodWall";
                            String imagepath = "";


                            File f = new File(Environment.getExternalStorageDirectory(), folder_main);
                            if (!f.exists()) {
                                f.mkdirs();
                            }

                            File newFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                            Log.e("ImageFile", "ImageFile->" + newFile);

                            try {

                                FileOutputStream fileOutputStream = new FileOutputStream(newFile);
                                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                imagepath = String.valueOf(newFile);
                                Log.e("MyPath", "MyImagePath->" + imagepath);

                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            final String camerapath = imagepath;

                            //View Popup

                            final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                            LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                            @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                            dialogBuilder1.setCancelable(false);
                            dialogBuilder1.setView(dialogView1);
                            dialogBuilder1.setTitle("");

                            done_btn = dialogView1.findViewById(R.id.done_btn);
                            rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                            cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                            img_smiley = dialogView1.findViewById(R.id.img_smiley);
                            img_food = dialogView1.findViewById(R.id.img_food);
                            edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                            edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);

                            edt_food_caption.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    String str = s.toString();

                                    if (str.equals(" ")) {

                                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                                        edt_food_caption.setText(edt_food_caption.getText().toString().replaceAll(" ", ""));
                                        edt_food_caption.setSelection(edt_food_caption.getText().length());

                                    }
                                }
                            });

                            emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                            emojIcon.ShowEmojIcon();
                            emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

                            final AlertDialog dialog1 = dialogBuilder1.create();
                            dialog1.show();

                            img_food.setImageURI(Uri.parse(camerapath));


                            cancel_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    dialog1.dismiss();

                                }
                            });


                            done_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                    if (edt_food_caption.getText().toString().equalsIgnoreCase("")) {

                                        Toast.makeText(ReviewTestActivity.this, "Dish name is required", Toast.LENGTH_SHORT).show();
                                    } else {

                                        dialog1.dismiss();

                                        // ll_dish_to_avoid_add_txt.setVisibility(View.GONE);
                                        Review_image_pojo reviewImagePojo = new Review_image_pojo(camerapath, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                        addimagesEncodedList_avoid.add(reviewImagePojo);
                                        avoidDishesImages();

                                    }


                                }
                            });


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                    } else {
                        Toast.makeText(this, "You haven't picked Image",
                                Toast.LENGTH_LONG).show();
                    }


                    break;

                // open gallery Ambience

                case PICK_IMAGE_MULTIPLE_AMBIENCE:


                    if (resultCode == RESULT_OK && data != null) {

                        imagesEncodedList_ambience = new ArrayList<String>();


                        if (data.getData() != null) {

                            imagesEncodedList_ambience = new ArrayList<String>();


                            String[] filePathColumn = {MediaStore.Images.Media.DATA};

                            Uri mImageUri = data.getData();


                            Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                            cursor.moveToFirst();
                            String document_id = cursor.getString(0);
                            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                            cursor.close();

                            cursor = getContentResolver().query(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                            cursor.moveToFirst();
                            final String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));


                            InputStream inputStream = new FileInputStream(path);//You can get an inputStream using any IO API
                            byte[] bytes;
                            byte[] buffer = new byte[8192];
                            int bytesRead;
                            ByteArrayOutputStream output = new ByteArrayOutputStream();
                            try {
                                while ((bytesRead = inputStream.read(buffer)) != -1) {
                                    output.write(buffer, 0, bytesRead);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            bytes = output.toByteArray();
                            String encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);

                            imagesEncodedList_ambience.add(path);

                            cursor.close();


                            //View POPUP

                            final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                            LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                            @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                            dialogBuilder1.setCancelable(false);
                            dialogBuilder1.setView(dialogView1);
                            dialogBuilder1.setTitle("");

                            done_btn = dialogView1.findViewById(R.id.done_btn);
                            rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                            cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                            img_smiley = dialogView1.findViewById(R.id.img_smiley);
                            img_food = dialogView1.findViewById(R.id.img_food);
                            edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                            edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);
                            edt_food_caption.setVisibility(View.GONE);

                            edt_food_caption.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    String str = s.toString();

                                    if (str.equals(" ")) {

                                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                                        edt_food_caption.setText(edt_food_caption.getText().toString().replaceAll(" ", ""));
                                        edt_food_caption.setSelection(edt_food_caption.getText().length());

                                    }
                                }
                            });


                            emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                            emojIcon.ShowEmojIcon();
                            emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

                            final AlertDialog dialog1 = dialogBuilder1.create();
                            dialog1.show();

                            GlideApp.with(getApplicationContext()).load(path).centerInside().into(img_food);


                            cancel_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    dialog1.dismiss();

                                }
                            });


                            done_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                    dialog1.dismiss();

                                    Review_image_pojo reviewImagePojo = new Review_image_pojo(path, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                    addimagesEncodedList_ambience.add(reviewImagePojo);

                                    ambience_images();
                                    /* }*/


                                }
                            });


                        } else if (data.getClipData() != null) {


                            ClipData mClipData = data.getClipData();
                            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                            for (int i = 0; i < mClipData.getItemCount(); i++) {

                                ClipData.Item item = mClipData.getItemAt(i);
                                Uri uri = item.getUri();
                                mArrayUri.add(uri);

                                // Get the cursor

                                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                                cursor.moveToFirst();
                                String document_id = cursor.getString(0);
                                document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                                cursor.close();

                                cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                                cursor.moveToFirst();
                                final String imageEncoded = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));


                                imagesEncodedList_ambience.add(imageEncoded);

                                cursor.close();

                                final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                                LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                                @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                                dialogBuilder1.setCancelable(false);
                                dialogBuilder1.setView(dialogView1);
                                dialogBuilder1.setTitle("");

                                done_btn = dialogView1.findViewById(R.id.done_btn);
                                rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                                cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                                img_smiley = dialogView1.findViewById(R.id.img_smiley);
                                img_food = dialogView1.findViewById(R.id.img_food);
                                edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                                edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);
                                edt_food_caption.setVisibility(View.GONE);
                                edt_food_caption.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        String str = s.toString();

                                        if (str.equals(" ")) {

                                            Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                                            edt_food_caption.setText(edt_food_caption.getText().toString().replaceAll(" ", ""));
                                            edt_food_caption.setSelection(edt_food_caption.getText().length());

                                        }
                                    }
                                });

                                emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                                emojIcon.ShowEmojIcon();
                                emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

                                final AlertDialog dialog1 = dialogBuilder1.create();
                                dialog1.show();

                                GlideApp.with(getApplicationContext()).load(imageEncoded).centerInside().into(img_food);


                                cancel_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        dialog1.dismiss();

                                    }
                                });


                                done_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog1.dismiss();
                                        Review_image_pojo reviewImagePojo = new Review_image_pojo(imageEncoded, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                        addimagesEncodedList_ambience.add(reviewImagePojo);
                                        ambience_images();
                                    }
                                });


                                Log.e("Gallery_ambience-->", "Multi" + imageEncoded);


                            }
                            Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                        }
                    } else {
                        Toast.makeText(this, "You haven't picked Image",
                                Toast.LENGTH_LONG).show();
                    }

                    break;


                case CAPTURE_IMAGE_CALLBACK_AMBIENCE:


                    if (requestCode == CAPTURE_IMAGE_CALLBACK_AMBIENCE) {


                        try {

                            Bitmap thumbnail = null;
                            try {
                                thumbnail = getThumbnail(imageUri);
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.e("CameraCaptureError", "CameraCaptureError->" + e.getMessage());
                            }


                            String folder_main = "FoodWall";
                            String imagepath = "";


                            File f = new File(Environment.getExternalStorageDirectory(), folder_main);
                            if (!f.exists()) {
                                f.mkdirs();
                            }

                            File newFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                            Log.e("ImageFile", "ImageFile->" + newFile);

                            try {

                                FileOutputStream fileOutputStream = new FileOutputStream(newFile);
                                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                imagepath = String.valueOf(newFile);
                                Log.e("MyPath", "MyImagePath->" + imagepath);

                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            final String camerapath = imagepath;

                            //View Popup

                            final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                            LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                            @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                            dialogBuilder1.setCancelable(false);
                            dialogBuilder1.setView(dialogView1);
                            dialogBuilder1.setTitle("");

                            done_btn = dialogView1.findViewById(R.id.done_btn);
                            rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                            cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                            img_smiley = dialogView1.findViewById(R.id.img_smiley);
                            img_food = dialogView1.findViewById(R.id.img_food);
                            edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                            edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);
                            edt_food_caption.setVisibility(View.GONE);
                            edt_food_caption.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    String str = s.toString();

                                    if (str.equals(" ")) {

                                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                                        edt_food_caption.setText(edt_food_caption.getText().toString().replaceAll(" ", ""));
                                        edt_food_caption.setSelection(edt_food_caption.getText().length());

                                    }
                                }
                            });

                            emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                            emojIcon.ShowEmojIcon();
                            emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

                            final AlertDialog dialog1 = dialogBuilder1.create();
                            dialog1.show();

                            img_food.setImageURI(Uri.parse(camerapath));


                            cancel_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    dialog1.dismiss();

                                }
                            });


                            done_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog1.dismiss();
                                    Review_image_pojo reviewImagePojo = new Review_image_pojo(camerapath, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                    addimagesEncodedList_ambience.add(reviewImagePojo);
                                    //  avoidDishesImages();
                                    ambience_images();
                                }
                            });


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                    } else {

                        Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
                    }

                    break;

                // Open gallery package
                case PICK_IMAGE_MULTIPLE_PACKAGE:

                    if (resultCode == RESULT_OK && data != null) {

                        imagesEncodedList_package = new ArrayList<String>();


                        if (data.getData() != null) {

                            imagesEncodedList_package = new ArrayList<String>();


                            String[] filePathColumn = {MediaStore.Images.Media.DATA};

                            Uri mImageUri = data.getData();


                            Cursor cursor = getContentResolver().query(mImageUri, null, null, null, null);
                            cursor.moveToFirst();
                            String document_id = cursor.getString(0);
                            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                            cursor.close();

                            cursor = getContentResolver().query(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                            cursor.moveToFirst();
                            final String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));


                            imagesEncodedList_package.add(path);

                            cursor.close();


                            //View POPUP

                            final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                            LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                            @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                            dialogBuilder1.setCancelable(false);
                            dialogBuilder1.setView(dialogView1);
                            dialogBuilder1.setTitle("");

                            done_btn = dialogView1.findViewById(R.id.done_btn);
                            rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                            cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                            img_smiley = dialogView1.findViewById(R.id.img_smiley);
                            img_food = dialogView1.findViewById(R.id.img_food);
                            edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                            edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);
                            edt_food_caption.setVisibility(View.GONE);

                            edt_food_caption.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    String str = s.toString();

                                    if (str.equals(" ")) {

                                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                                        edt_food_caption.setText(edt_food_caption.getText().toString().replaceAll(" ", ""));
                                        edt_food_caption.setSelection(edt_food_caption.getText().length());

                                    }
                                }
                            });


                            emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                            emojIcon.ShowEmojIcon();
                            emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

                            final AlertDialog dialog1 = dialogBuilder1.create();
                            dialog1.show();

                            GlideApp.with(getApplicationContext()).load(path).centerInside().into(img_food);


                            cancel_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    dialog1.dismiss();

                                }
                            });


                            done_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                    dialog1.dismiss();

                                    //  ll_dish_to_avoid_add_txt.setVisibility(View.GONE);
                                    Review_image_pojo reviewImagePojo = new Review_image_pojo(path, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                    addimagesEncodedList_packaging.add(reviewImagePojo);

                                    package_images();


                                }
                            });


                        } else if (data.getClipData() != null) {


                            ClipData mClipData = data.getClipData();
                            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                            for (int i = 0; i < mClipData.getItemCount(); i++) {

                                ClipData.Item item = mClipData.getItemAt(i);
                                Uri uri = item.getUri();
                                mArrayUri.add(uri);

                                // Get the cursor

                                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                                cursor.moveToFirst();
                                String document_id = cursor.getString(0);
                                document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                                cursor.close();

                                cursor = getContentResolver().query(
                                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                        null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                                cursor.moveToFirst();
                                final String imageEncoded = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));


                                imagesEncodedList_package.add(imageEncoded);

                                cursor.close();

                                final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                                LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                                @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                                dialogBuilder1.setCancelable(false);
                                dialogBuilder1.setView(dialogView1);
                                dialogBuilder1.setTitle("");

                                done_btn = dialogView1.findViewById(R.id.done_btn);
                                rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                                cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                                img_smiley = dialogView1.findViewById(R.id.img_smiley);
                                img_food = dialogView1.findViewById(R.id.img_food);
                                edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                                edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);
                                edt_food_caption.setVisibility(View.GONE);
                                edt_food_caption.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        String str = s.toString();

                                        if (str.equals(" ")) {

                                            Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                                            edt_food_caption.setText(edt_food_caption.getText().toString().replaceAll(" ", ""));
                                            edt_food_caption.setSelection(edt_food_caption.getText().length());

                                        }
                                    }
                                });

                                emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                                emojIcon.ShowEmojIcon();
                                emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

                                final AlertDialog dialog1 = dialogBuilder1.create();
                                dialog1.show();

                                GlideApp.with(getApplicationContext()).load(imageEncoded).centerInside().into(img_food);


                                cancel_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        dialog1.dismiss();

                                    }
                                });


                                done_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {


                                        dialog1.dismiss();

                                        // ll_dish_to_avoid_add_txt.setVisibility(View.GONE);
                                        Review_image_pojo reviewImagePojo = new Review_image_pojo(imageEncoded, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                        addimagesEncodedList_packaging.add(reviewImagePojo);

                                        package_images();


                                    }
                                });


                                Log.e("Gallery_ambience-->", "Multi" + imageEncoded);


                            }
                            Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                        }
                    } else {
                        Toast.makeText(this, "You haven't picked Image",
                                Toast.LENGTH_LONG).show();
                    }


                    break;

                case CAPTURE_IMAGE_CALLBACK_PACKAGE:

                    if (requestCode == CAPTURE_IMAGE_CALLBACK_PACKAGE) {


                        try {

                            Bitmap thumbnail = null;
                            try {
                                thumbnail = getThumbnail(imageUri);
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.e("CameraCaptureError", "CameraCaptureError->" + e.getMessage());
                            }


                            String folder_main = "FoodWall";
                            String imagepath = "";


                            File f = new File(Environment.getExternalStorageDirectory(), folder_main);
                            if (!f.exists()) {
                                f.mkdirs();
                            }

                            File newFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                            Log.e("ImageFile", "ImageFile->" + newFile);

                            try {

                                FileOutputStream fileOutputStream = new FileOutputStream(newFile);
                                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                imagepath = String.valueOf(newFile);
                                Log.e("MyPath", "MyImagePath->" + imagepath);

                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            final String camerapath = imagepath;

                            //View Popup

                            final AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(ReviewTestActivity.this);
                            LayoutInflater inflater2 = LayoutInflater.from(ReviewTestActivity.this);
                            @SuppressLint("InflateParams") final View dialogView1 = inflater2.inflate(R.layout.review_popup_layout, null);
                            dialogBuilder1.setCancelable(false);
                            dialogBuilder1.setView(dialogView1);
                            dialogBuilder1.setTitle("");

                            done_btn = dialogView1.findViewById(R.id.done_btn);
                            rl_dialog_dish = dialogView1.findViewById(R.id.rl_dialog_dish);
                            cancel_btn = dialogView1.findViewById(R.id.cancel_btn);
                            img_smiley = dialogView1.findViewById(R.id.img_smiley);
                            img_food = dialogView1.findViewById(R.id.img_food);
                            edt_food_caption = dialogView1.findViewById(R.id.edt_food_caption);
                            edt_food_caption.setImeOptions(EditorInfo.IME_ACTION_GO);
                            edt_food_caption.setVisibility(View.GONE);
                            edt_food_caption.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    String str = s.toString();

                                    if (str.equals(" ")) {

                                        Toast.makeText(getApplicationContext(), "Space is not allowed", Toast.LENGTH_SHORT).show();


                                        edt_food_caption.setText(edt_food_caption.getText().toString().replaceAll(" ", ""));
                                        edt_food_caption.setSelection(edt_food_caption.getText().length());

                                    }
                                }
                            });

                            emojIcon = new EmojIconActions(ReviewTestActivity.this, rl_dialog_dish, edt_food_caption, img_smiley);
                            emojIcon.ShowEmojIcon();
                            emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

                            final AlertDialog dialog1 = dialogBuilder1.create();
                            dialog1.show();

                            img_food.setImageURI(Uri.parse(camerapath));


                            cancel_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    dialog1.dismiss();

                                }
                            });


                            done_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                    dialog1.dismiss();

                                    // ll_dish_to_avoid_add_txt.setVisibility(View.GONE);
                                    Review_image_pojo reviewImagePojo = new Review_image_pojo(camerapath, org.apache.commons.text.StringEscapeUtils.escapeJava(edt_food_caption.getText().toString().trim()));
                                    addimagesEncodedList_packaging.add(reviewImagePojo);

                                    //  avoidDishesImages();
                                    package_images();


                                }
                            });


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                    } else {
                        Toast.makeText(this, "You haven't picked Image",
                                Toast.LENGTH_LONG).show();
                    }

                    break;

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException {

        InputStream input = ReviewTestActivity.this.getContentResolver().openInputStream(uri);
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 800) ? (originalSize / 800) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {

        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (position == 0) {

            autoCompleteTextView.setVisibility(View.GONE);
            ll_edt_add_dish.setVisibility(View.GONE);

        }

        if (position == 1) {

            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);

        } else if (position == 2) {

            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

        autoCompleteTextView.setVisibility(View.GONE);
        ll_edt_add_dish.setVisibility(View.GONE);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (position == 0) {

            autoCompleteTextView.setVisibility(View.GONE);
            ll_edt_add_dish.setVisibility(View.GONE);

        }

        if (position == 1) {

            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);

        } else if (position == 2) {

            autoCompleteTextView.setVisibility(View.VISIBLE);
            ll_edt_add_dish.setVisibility(View.VISIBLE);

        }

    }

    // Internet Check

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


    // Open camera alert top dish

    public class CustomDialogClass_topdish extends Dialog implements View.OnClickListener {

        public AppCompatActivity c;
        public Dialog d;
        public AppCompatButton btn_Ok;
        TextView txt_open_camera, txt_open_gallery;

        CustomDialogClass_topdish(AppCompatActivity a) {
            super(a);
            // TODO Auto-generated constructor stub
            c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.select_picture_layout_for_review);

            txt_open_camera = (TextView) findViewById(R.id.txt_open_camera);
            txt_open_gallery = (TextView) findViewById(R.id.txt_open_gallery);

            txt_open_camera.setOnClickListener(this);
            txt_open_gallery.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int click = v.getId();
            switch (click) {

                case R.id.txt_open_camera:

                    dismiss();

                    if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.TITLE, "New Picture");
                        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        startActivityForResult(intent, CAPTURE_IMAGE_CALLBACK_TOP);

                    } else {

                        ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAPTURE_IMAGE_CALLBACK_TOP);

                    }

                    break;


                case R.id.txt_open_gallery:

                    dismiss();
                    try {

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE_TOP);

                        } else {

                            ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_MULTIPLE_TOP);

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;


            }
        }
    }


    // Open camera alert dish to avoid

    public class CustomDialogClass_avoiddish extends Dialog implements View.OnClickListener {

        public AppCompatActivity c;
        public Dialog d;
        public AppCompatButton btn_Ok;
        TextView txt_open_camera, txt_open_gallery;

        CustomDialogClass_avoiddish(AppCompatActivity a) {
            super(a);
            // TODO Auto-generated constructor stub
            c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.select_picture_layout_for_review);

            txt_open_camera = (TextView) findViewById(R.id.txt_open_camera);
            txt_open_gallery = (TextView) findViewById(R.id.txt_open_gallery);

            txt_open_camera.setOnClickListener(this);
            txt_open_gallery.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int click = v.getId();
            switch (click) {

                case R.id.txt_open_camera:
                    dismiss();

                    if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {


                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.TITLE, "New Picture");
                        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        startActivityForResult(intent, CAPTURE_IMAGE_CALLBACK_AVOID);


                    } else {


                        ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAPTURE_IMAGE_CALLBACK_AVOID);

                    }

                    break;


                case R.id.txt_open_gallery:

                    dismiss();

                    try {

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE_AVOID);


                        } else {

                            ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_MULTIPLE_AVOID);


                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;


            }
        }
    }


    // Open camera ambience

    public class CustomDialogClass_ambience extends Dialog implements View.OnClickListener {

        public AppCompatActivity c;
        public Dialog d;
        public AppCompatButton btn_Ok;
        TextView txt_open_camera, txt_open_gallery;

        CustomDialogClass_ambience(AppCompatActivity a) {
            super(a);
            // TODO Auto-generated constructor stub
            c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.select_picture_layout_for_review);

            txt_open_camera = (TextView) findViewById(R.id.txt_open_camera);
            txt_open_gallery = (TextView) findViewById(R.id.txt_open_gallery);

            txt_open_camera.setOnClickListener(this);
            txt_open_gallery.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int click = v.getId();
            switch (click) {

                case R.id.txt_open_camera:
                    dismiss();

                    if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {


                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.TITLE, "New Picture");
                        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        startActivityForResult(intent, CAPTURE_IMAGE_CALLBACK_AMBIENCE);


                    } else {


                        ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAPTURE_IMAGE_CALLBACK_AMBIENCE);

                    }

                    break;


                case R.id.txt_open_gallery:

                    dismiss();

                    try {

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE_AMBIENCE);

                        } else {

                            ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_MULTIPLE_AMBIENCE);


                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;


            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_UP) {

            hideKeyboard(v);
        }
        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            hideKeyboard(v);
        }

        return false;


    }


    //Hide keyboard
    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }

    // Open camera service

    public class CustomDialogClass_package extends Dialog implements View.OnClickListener {

        public AppCompatActivity c;
        public Dialog d;
        public AppCompatButton btn_Ok;
        TextView txt_open_camera, txt_open_gallery;

        CustomDialogClass_package(AppCompatActivity a) {
            super(a);
            // TODO Auto-generated constructor stub
            c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.select_picture_layout_for_review);

            txt_open_camera = (TextView) findViewById(R.id.txt_open_camera);
            txt_open_gallery = (TextView) findViewById(R.id.txt_open_gallery);

            txt_open_camera.setOnClickListener(this);
            txt_open_gallery.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            int click = v.getId();

            switch (click) {

                case R.id.txt_open_camera:

                    dismiss();

                    if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.TITLE, "New Picture");
                        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        startActivityForResult(intent, CAPTURE_IMAGE_CALLBACK_PACKAGE);

                    } else {

                        ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAPTURE_IMAGE_CALLBACK_PACKAGE);

                    }

                    break;


                case R.id.txt_open_gallery:

                    dismiss();

                    try {

                        if (ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ReviewTestActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE_PACKAGE);


                        } else {


                            ActivityCompat.requestPermissions(ReviewTestActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_MULTIPLE_PACKAGE);

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;


            }
        }
    }


    @Override
    public void onBackPressed() {

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Confirm")
                .setContentText("Are you sure you want to skip")
                .setConfirmText("Yes")
                .setCancelText("No")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        finish();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();

    }
}