package com.kaspontech.foodwall.reviewpackage.Review_adapter_folder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_all_input;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_dish_type_image_pojo;
import com.kaspontech.foodwall.R;
//
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

//

public class Review_worst_dish_adapter extends RecyclerView.Adapter<Review_worst_dish_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;
    private ArrayList<Get_dish_type_all_input> getDishTypeAllInputArrayList = new ArrayList<>();
    private ArrayList<Get_dish_type_image_pojo> getDishTypeImagePojoArrayList = new ArrayList<>();

    private Get_dish_type_all_input getDishTypeAllInput;
    private Get_dish_type_image_pojo getDishTypeImagePojo;


    private Pref_storage pref_storage;


    public Review_worst_dish_adapter(Context c, ArrayList<Get_dish_type_all_input> gettinglist, ArrayList<Get_dish_type_image_pojo> gettingimglist) {
        this.context = c;
        this.getDishTypeAllInputArrayList = gettinglist;
        this.getDishTypeImagePojoArrayList = gettingimglist;

    }


    @NonNull
    @Override
    public Review_worst_dish_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_review_image, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Review_worst_dish_adapter.MyViewHolder vh = new Review_worst_dish_adapter.MyViewHolder(v);

        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Review_worst_dish_adapter.MyViewHolder holder, final int position) {


        try {
            getDishTypeAllInput = getDishTypeAllInputArrayList.get(position);

        getDishTypeImagePojo = getDishTypeImagePojoArrayList.get(position);


            Utility.picassoImageLoader(getDishTypeImagePojoArrayList.get(position)./*getImage().get(position).*/getImg(),
                    0,holder.image_review, context);


//        GlideApp.with(context)
//                .load(getDishTypeImagePojoArrayList.get(position)./*getImage().get(position).*/getImg())
//                .into(holder.image_review);

        holder.txt_image.setText(getDishTypeAllInputArrayList.get(position).getDishName());
//        holder.txt_image.setTextColor(Color.parseColor("#000000"));

           /* holder.rl_hotel_datas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.modal_in);
                    v.startAnimation(anim);
                    Intent intent= new Intent(context, AllDishDisplayActivity.class);
                    intent.putExtra("reviewid",getDishTypeAllInputArrayList.get(position).getRevratId());
                    intent.putExtra("hotelname",getDishTypeAllInputArrayList.get(position).getHotelName());
                    context.startActivity(intent);
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return getDishTypeAllInputArrayList.size();

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //TextView

        TextView txt_image;

        //Imageview

        ImageView image_review;

        RelativeLayout rl_hotel_datas;
        public MyViewHolder(View itemView) {
            super(itemView);

            txt_image = (TextView) itemView.findViewById(R.id.txt_image);
            image_review = (ImageView) itemView.findViewById(R.id.image_review);
            rl_hotel_datas = (RelativeLayout) itemView.findViewById(R.id.rl_hotel_datas);

        }


    }


}
