package com.kaspontech.foodwall.reviewpackage.ProfileReviewPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetReviewProfileImage {

    @SerializedName("photo_id")
    @Expose
    private String photoId;
    @SerializedName("reviewid")
    @Expose
    private String reviewid;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("amb_image")
    @Expose
    private String ambImage;


    public GetReviewProfileImage(String photoId, String reviewid, String userid, String ambImage) {
        this.photoId = photoId;
        this.reviewid = reviewid;
        this.userid = userid;
        this.ambImage = ambImage;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getReviewid() {
        return reviewid;
    }

    public void setReviewid(String reviewid) {
        this.reviewid = reviewid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAmbImage() {
        return ambImage;
    }

    public void setAmbImage(String ambImage) {
        this.ambImage = ambImage;
    }
}
