package com.kaspontech.foodwall.reviewpackage.dineInTab;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.annotation.UiThread;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.fcm.Constants;
import com.kaspontech.foodwall.foodFeedsPackage.TotalCountModule_Response;
import com.kaspontech.foodwall.modelclasses.Review_pojo.ReviewCommentsPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewInputAllPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewOutputResponsePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.GetHotelDetailsAdapter;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
import com.kaspontech.foodwall.reviewpackage.allTab.AllFragment;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.M)
public class NewDineInFragment extends Fragment implements ScrollView.OnScrollChangeListener, GetHotelDetailsAdapter.onCommentsPostListener {
    /**
     * View
     */
    View view;
    /**
     * Context
     */
    Context context;
    /**
     * Application tag
     */
    private static final String TAG = "NewDineInFragment";


    /**
     * Text view for view more, category rating, caption, show less
     */
    TextView view_more;
    TextView cat_rating;
    TextView caption;
    TextView showless;

    /**
     * Linear layout for images
     */
    LinearLayout images;

    /**
     * Dine in recyclerview
     */
    RecyclerView rvReview;

    /**
     * From which adapter String result
     */
    String fromwhich;


    /*// Hotel details pojo
    ReviewInputAllPojo reviewInputAllPojo;
    // Top dish pojo
    ReviewTopDishPojo reviewTopDishPojo;

    //Avoid dish pojo

    ReviewAvoidDishPojo reviewAvoidDishPojo;

    //Ambience dish pojo
    ReviewAmbiImagePojo reviewAmbiImagePojo;*/


    /* Hotel Review Pojo ArrayList */
    List<ReviewInputAllPojo> getallHotelReviewPojoArrayList = new ArrayList<>();

    /* Pagination arraylist */
    ArrayList<ReviewInputAllPojo> PaginationArrayList = new ArrayList<>();

    /*// topDishImagePojoArrayList
    ArrayList<ReviewTopDishPojo> topDishImagePojoArrayList = new ArrayList<>();

    //avoidDishImagePojoArrayList
    ArrayList<ReviewAvoidDishPojo> avoidDishImagePojoArrayList = new ArrayList<>();

    //ambiImagePojoArrayList
    ArrayList<ReviewAmbiImagePojo> ambiImagePojoArrayList = new ArrayList<>();*/

    /**
     * No dish available Imageview
     */
    ImageView imgNoDish;

    /**
     * No dish available Textview
     */
    TextView txtNoDish;

    /**
     * Hotel review layout
     */
    RelativeLayout rlHotel;

    /**
     * Integer values for top,avoid, ambience, top dish image, avoid dish image count
     */
    int top_count;
    int avoid_count;
    int ambi_image_count;
    int topdishimage_count;
    int avoiddishimage_count;

    int top_dish_img_count;
    int avoid_dish_img_count;
    int bucketlist;
    //    Get_all_hotel_review_adapter get_all_hotel_review_adapter;


    /**
     * Hotel review adapter
     */
    GetHotelDetailsAdapter getHotelDetailsAdapter;

    /**
     * Loader
     */
    ProgressBar progressbarReview;

    /**
     * Filter veg, sort
     */
    // String veg;
    String sort;


    /**
     * Scroll view for dine in fragment
     */

    ScrollView scrollAllfragment;
    /**
     * Integer Page count
     */


    int pagecount = 1;
    int pagecountSort = 1;

    /**
     * Page loader
     */

    Button pageLoader;
    Button btnSizeCheck;

    /**
     * No dish available image and text
     */
    LinearLayout llNoData;
    /**
     * Pagination integer
     */

    int loaderInt = 0;

    /**
     * String results for hotel details
     */
    String txt_hotelname;
    String user_id;
    String hotel_id;
    String txt_hotel_address;
    String avoiddishimage_name;
    String topdishimage_name;
    String topdishimage;
    String delivery_mode;
    String dishname;
    String dishtoavoid;
    String txt_review_username;
    String txt_review_userlastname;
    String userimage;
    String txt_review_follow;
    String txt_review_like;
    String txt_review_comment;
    String txt_review_shares;
    String txt_review_caption;
    String hotel_review_rating;
    String hotel_user_rating;
    String total_followers;
    String total_followings;
    String hotelId;
    String cat_type;
    String veg_nonveg;
    String revratID;
    String ambiance;
    String modeid;
    String comp_name;
    String taste;
    String total_package;
    String total_timedelivery;
    String service;
    String valuemoney;
    String createdby;
    String createdon;
    String googleID;
    String placeID;
    String opentimes;
    String category_type;
    String latitude;
    String longitude;
    String phone;
    String package_value;
    String time_delivery;
    String foodexp;
    String photo_reference;
    String emailID;
    String likes_hotelID;
    String revrathotel_likes;
    String followingID;
    String totalreview;
    String total_ambiance;
    String total_taste;
    String total_service;
    String total_value;
    String total_good;
    String total_bad;
    String total_good_bad_user;
    String redirect_url;

    int packCount;
    SwipeRefreshLayout swiperefresh;

    TextView tv_view_all_comments;

    private ProgressBar comments_progressbar;

    private EmojiconEditText review_comment;

    public NewDineInFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.activity_hotel_review_all, container, false);
        context = view.getContext();

        Pref_storage.setDetail(context,"shareView",null);
        Pref_storage.setDetail(context,"shareViewPage",null);

        rvReview = (RecyclerView) view.findViewById(R.id.rv_review);
        swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);

        /*Loader*/
        progressbarReview = (ProgressBar) view.findViewById(R.id.progressbar_review);

        /*All fragment Scroll view initialization*/

        scrollAllfragment = (ScrollView) view.findViewById(R.id.scroll_allfragment);

        scrollAllfragment.setOnScrollChangeListener(this);

        /*No dish layout*/

        llNoData = (LinearLayout) view.findViewById(R.id.ll_no_data);

        /*No dish imageview*/
        imgNoDish = (ImageView) view.findViewById(R.id.img_no_dish);

        /*No dish textview*/

        txtNoDish = (TextView) view.findViewById(R.id.txt_no_dish);
        /*Loader buttons*/
        pageLoader = (Button) view.findViewById(R.id.page_loader);
        btnSizeCheck = (Button) view.findViewById(R.id.btn_size_check);


        /*Initial page loading shared preference value*/


        Pref_storage.setDetail(context, "Page_dinein", String.valueOf(1));
        Pref_storage.setDetail(context, "Page_dineinsort", String.valueOf(1));

        /* Category based hotel review */


        //call notification count api for in app notification
        get_total_count_modules();

        Bundle bundle = getArguments();

        if (bundle != null) {
            // veg = bundle.getString("veg");
            sort = bundle.getString("sort");
            // Log.e("Dinein", "veg: " + veg);
            Log.e("Dinein", "sort: " + sort);
        }


        if (sort == null) {

            if (getallHotelReviewPojoArrayList.size() > 0) {


                getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList, fromwhich, NewDineInFragment.this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                rvReview.setLayoutManager(mLayoutManager);
                rvReview.setItemAnimator(new DefaultItemAnimator());
                rvReview.setAdapter(getHotelDetailsAdapter);
                rvReview.setNestedScrollingEnabled(false);
                progressbarReview.setVisibility(View.GONE);

            } else {

                getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList, fromwhich, NewDineInFragment.this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                rvReview.setLayoutManager(mLayoutManager);
                rvReview.setItemAnimator(new DefaultItemAnimator());
                rvReview.setAdapter(getHotelDetailsAdapter);
                rvReview.setNestedScrollingEnabled(false);
                progressbarReview.setIndeterminate(false);
                progressbarReview.setVisibility(View.VISIBLE);

                //for in app notification
                get_total_count_modules();
                /*Calling dine in Api*/
                get_hotel_review_dineinswipe();


            }
        } else {

            getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList, fromwhich, NewDineInFragment.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            rvReview.setLayoutManager(mLayoutManager);
            rvReview.setItemAnimator(new DefaultItemAnimator());
            rvReview.setAdapter(getHotelDetailsAdapter);
            rvReview.setNestedScrollingEnabled(false);
            progressbarReview.setVisibility(View.VISIBLE);
            /*Calling dine in sort Api*/

            get_hotel_review_sort();

        }

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                get_hotel_review_dineinswipe();

                //for in app notification
                get_total_count_modules();

                scrollAllfragment.smoothScrollTo(0, 0);
                swiperefresh.setRefreshing(false);

            }
        });

        return view;

    }

    private void get_total_count_modules() {
        try {
            if (Utility.isConnected(getActivity())) {


                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {

                    String userid = Pref_storage.getDetail(getActivity(), "userId");

                    Call<TotalCountModule_Response> call = apiService.get_total_count_modules("get_total_count_modules",
                            Integer.parseInt(userid));
                    call.enqueue(new Callback<TotalCountModule_Response>() {
                        @Override
                        public void onResponse(Call<TotalCountModule_Response> call, Response<TotalCountModule_Response> response) {

                            if (response.body().getResponseMessage().equalsIgnoreCase("success")) {

                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    //save total timeline count
                                    Pref_storage.setDetail(getActivity(), "TotalPostCount", String.valueOf(response.body().getData().get(i).getTotalPosts()));

                                    Log.e("", "count total: post" + response.body().getData().get(i).getTotalPosts());

                                    Pref_storage.setDetail(getActivity(), "TotalReviewCount", String.valueOf(response.body().getData().get(i).getTotalReview()));

                                    Log.e("", "counttotal:review " + response.body().getData().get(i).getTotalReview());
                                    Pref_storage.setDetail(getActivity(), "TotalEventCount", String.valueOf(response.body().getData().get(i).getTotalEvents()));

                                    Log.e("", "counttotal:event " + response.body().getData().get(i).getTotalReview());

                                    Pref_storage.setDetail(getActivity(), "TotalQuestionCount", String.valueOf(response.body().getData().get(i).getTotalQuestion()));

                                    Log.e("", "counttotal:question " + response.body().getData().get(i).getTotalQuestion());

                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<TotalCountModule_Response> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /* Calling Hotel sort API */
    private void get_hotel_review_sort() {

        if (Utility.isConnected(context)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");

//            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_sort("get_hotel_review_sort", 1, Integer.parseInt(veg),Integer.parseInt(sort), Integer.parseInt(userId));
            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_sort("get_hotel_review_sort",
                    0,
                    0,
                    Integer.parseInt(sort),
                    Integer.parseInt(userId), pagecount);
            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {
                        getallHotelReviewPojoArrayList.clear();


                        if (response.body().getData().size() > 0) {
                            pagecountSort += 1;


                            Pref_storage.setDetail(context, "Page_dineinsort", String.valueOf(pagecountSort));

                            Log.e(TAG, "onResponse:shared_page-->" + Pref_storage.getDetail(context, "Page_dineinsort"));
                        } else {

                        }

                        pageLoader.setVisibility(View.GONE);

                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());


                            try {

                                hotelId = response.body().getData().get(j).getHotelId();
                                cat_type = response.body().getData().get(j).getCategoryType();
                                veg_nonveg = response.body().getData().get(j).getVegNonveg();
                                revratID = response.body().getData().get(j).getRevratId();
                                modeid = response.body().getData().get(j).getMode_id();
                                comp_name = response.body().getData().get(j).getComp_name();
                                ambiance = response.body().getData().get(j).getAmbiance();
                                taste = response.body().getData().get(j).getTaste();
                                total_package = response.body().getData().get(j).getTotalPackage();
                                total_timedelivery = response.body().getData().get(j).getTotalTimedelivery();
                                service = response.body().getData().get(j).getService();
                                valuemoney = response.body().getData().get(j).getValueMoney();
                                createdby = response.body().getData().get(j).getCreatedBy();
                                createdon = response.body().getData().get(j).getCreatedOn();
                                googleID = response.body().getData().get(j).getGoogleId();
                                placeID = response.body().getData().get(j).getPlaceId();
                                opentimes = response.body().getData().get(j).getOpenTimes();
                                category_type = response.body().getData().get(j).getCategoryType();
                                latitude = response.body().getData().get(j).getLatitude();
                                longitude = response.body().getData().get(j).getLongitude();
                                phone = response.body().getData().get(j).getPhone();
                                photo_reference = response.body().getData().get(j).getPhotoReference();
                                emailID = response.body().getData().get(j).getEmail();
                                likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                                revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                                followingID = response.body().getData().get(j).getFollowingId();
                                totalreview = response.body().getData().get(j).getTotalReview();
                                total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                                total_taste = response.body().getData().get(j).getTotalTaste();
                                package_value = response.body().getData().get(j).get_package();
                                total_service = response.body().getData().get(j).getTotalService();
                                total_value = response.body().getData().get(j).getTotalValueMoney();
                                total_good = response.body().getData().get(j).getTotalGood();
                                total_bad = response.body().getData().get(j).getTotalBad();
                                total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();
                                txt_hotelname = response.body().getData().get(j).getHotelName();
                                user_id = response.body().getData().get(j).getUserId();
                                hotel_id = response.body().getData().get(j).getHotelId();
                                txt_hotel_address = response.body().getData().get(j).getAddress();
                                txt_review_username = response.body().getData().get(j).getFirstName();
                                txt_review_userlastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                txt_review_caption = response.body().getData().get(j).getHotelReview();
                                txt_review_like = response.body().getData().get(j).getTotalLikes();
                                txt_review_comment = response.body().getData().get(j).getTotalComments();
                                txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                                hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                                hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                                total_followers = response.body().getData().get(j).getTotalFollowers();
                                total_followings = response.body().getData().get(j).getTotalFollowings();
                                foodexp = response.body().getData().get(j).getFoodExprience();
                                top_count = response.body().getData().get(j).getTopCount();
                                dishtoavoid = response.body().getData().get(j).getAvoiddish();
                                avoid_count = response.body().getData().get(j).getAvoidCount();
                                ambi_image_count = response.body().getData().get(j).getAmbiImageCount();
                                dishname = response.body().getData().get(j).getTopdish();
                                delivery_mode = response.body().getData().get(j).getDelivery_mode();
                                top_dish_img_count = response.body().getData().get(j).getTop_dish_img_count();
                                avoid_dish_img_count = response.body().getData().get(j).getAvoid_dish_img_count();
                                bucketlist = response.body().getData().get(j).getBucket_list();
                                redirect_url = response.body().getData().get(j).getRedirect_url();


                                ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();


                                if (top_count == 0) {

                                } else {
                                    for (int b = 0; b < response.body().getData().get(j).getTopdishimage().size(); b++) {
                                        topdishimage = response.body().getData().get(j).getTopdishimage().get(b).getImg();
                                        topdishimage_name = response.body().getData().get(j).getTopdishimage().get(b).getDishname();
                                        ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimage_name);
                                        reviewTopDishPojoArrayList.add(reviewTopDishPojo);
                                    }

                                }
                                //Avoid count

                                ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();


                                if (avoid_count == 0) {

                                } else {

                                    for (int a = 0; a < response.body().getData().get(j).getAvoiddishimage().size(); a++) {

                                        String dishavoidimage = response.body().getData().get(j).getAvoiddishimage().get(a).getImg();
                                        avoiddishimage_name = response.body().getData().get(j).getAvoiddishimage().get(a).getDishname();
                                        ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(dishavoidimage, avoiddishimage_name);
                                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);
                                    }

                                }


                                //Ambiance

                                ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

                                if (ambi_image_count == 0) {


                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getAmbiImage().size(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getAmbiImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(img);

                                        reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);
                                    }


                                }


                                //packaging

                                ArrayList<PackImage> reviewpackImagePojoArrayList = new ArrayList<>();


                                if (packCount == 0) {

                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getPackImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getPackImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        PackImage packImage = new PackImage(img);

                                        reviewpackImagePojoArrayList.add(packImage);
                                    }

                                }


                                ReviewInputAllPojo reviewInputAllPojo = new ReviewInputAllPojo(hotelId, revratID, txt_review_caption, txt_review_like, txt_review_comment,
                                        category_type, veg_nonveg, foodexp, ambiance, modeid, comp_name, taste, service, package_value, time_delivery,
                                        valuemoney, createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude, longitude, phone,
                                        photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname, emailID, "", "", "", "",
                                        total_followers, total_followings, userimage, likes_hotelID, revrathotel_likes, followingID, totalreview,
                                        txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service, total_package, total_timedelivery,
                                        total_value, total_good, total_bad, total_good_bad_user, reviewTopDishPojoArrayList, topdishimage_count,
                                        dishname, top_count, reviewAvoidDishPojoArrayList, avoiddishimage_count, dishtoavoid, avoid_count,
                                        reviewAmbiImagePojoArrayList, ambi_image_count, false, delivery_mode, top_dish_img_count,
                                        avoid_dish_img_count, bucketlist, redirect_url, reviewpackImagePojoArrayList, packCount);


                                getallHotelReviewPojoArrayList.add(reviewInputAllPojo);

                                LinkedHashSet<ReviewInputAllPojo> s = new LinkedHashSet<>(getallHotelReviewPojoArrayList);
                                getallHotelReviewPojoArrayList.clear();
                                getallHotelReviewPojoArrayList.addAll(s);


                            }  catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList, fromwhich, NewDineInFragment.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                        rvReview.setLayoutManager(mLayoutManager);
                        rvReview.setItemAnimator(new DefaultItemAnimator());
                        rvReview.setAdapter(getHotelDetailsAdapter);
                        rvReview.setNestedScrollingEnabled(false);
                        getHotelDetailsAdapter.notifyDataSetChanged();
                        progressbarReview.setVisibility(View.GONE);

                    } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {

                    }


                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                    if (getallHotelReviewPojoArrayList.size() == 0) {
                        imgNoDish.setVisibility(View.VISIBLE);
                        txtNoDish.setVisibility(View.VISIBLE);
                    } else {

                    }

                }
            });

        } else {

            Snackbar snackbar = Snackbar.make(rlHotel, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    /* Pagination sort API */
    private void get_hotel_review_sort(int pagecount_sort) {

        pageLoader.setVisibility(View.VISIBLE);

        if (Utility.isConnected(context)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");

             Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_sort("get_hotel_review_sort", 0, 0, Integer.parseInt(sort), Integer.parseInt(userId), pagecount_sort);
            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {
                        PaginationArrayList.clear();
                        loaderInt = 0;


                        if (Pref_storage.getDetail(context, "Page_dineinsort").isEmpty() || Pref_storage.getDetail(context, "Page_dineinsort") == null) {

                        } else {
                            int page_incre = Integer.parseInt(Pref_storage.getDetail(context, "Page_dineinsort"));

                            Log.e(TAG, "onResponsePagination: page_incre-->" + page_incre);


                            int page_incre_shared = page_incre + 1;

                            Pref_storage.setDetail(context, "Page_dineinsort", String.valueOf(page_incre_shared));

                            Log.e(TAG, "onResponsePagination: page_count_incre-->" + Integer.parseInt(Pref_storage.getDetail(context, "Page_dineinsort")));

                        }
                        pageLoader.setVisibility(View.GONE);


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());


                            try {

                                hotelId = response.body().getData().get(j).getHotelId();
                                cat_type = response.body().getData().get(j).getCategoryType();
                                veg_nonveg = response.body().getData().get(j).getVegNonveg();
                                revratID = response.body().getData().get(j).getRevratId();
                                modeid = response.body().getData().get(j).getMode_id();
                                comp_name = response.body().getData().get(j).getComp_name();
                                ambiance = response.body().getData().get(j).getAmbiance();
                                taste = response.body().getData().get(j).getTaste();
                                total_package = response.body().getData().get(j).getTotalPackage();
                                total_timedelivery = response.body().getData().get(j).getTotalTimedelivery();
                                service = response.body().getData().get(j).getService();
                                valuemoney = response.body().getData().get(j).getValueMoney();
                                createdby = response.body().getData().get(j).getCreatedBy();
                                createdon = response.body().getData().get(j).getCreatedOn();
                                googleID = response.body().getData().get(j).getGoogleId();
                                placeID = response.body().getData().get(j).getPlaceId();
                                opentimes = response.body().getData().get(j).getOpenTimes();
                                category_type = response.body().getData().get(j).getCategoryType();
                                latitude = response.body().getData().get(j).getLatitude();
                                longitude = response.body().getData().get(j).getLongitude();
                                phone = response.body().getData().get(j).getPhone();
                                photo_reference = response.body().getData().get(j).getPhotoReference();
                                emailID = response.body().getData().get(j).getEmail();
                                likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                                revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                                followingID = response.body().getData().get(j).getFollowingId();
                                totalreview = response.body().getData().get(j).getTotalReview();
                                total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                                total_taste = response.body().getData().get(j).getTotalTaste();
                                package_value = response.body().getData().get(j).get_package();
                                total_service = response.body().getData().get(j).getTotalService();
                                total_value = response.body().getData().get(j).getTotalValueMoney();
                                total_good = response.body().getData().get(j).getTotalGood();
                                total_bad = response.body().getData().get(j).getTotalBad();
                                total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();
                                txt_hotelname = response.body().getData().get(j).getHotelName();
                                user_id = response.body().getData().get(j).getUserId();
                                hotel_id = response.body().getData().get(j).getHotelId();
                                txt_hotel_address = response.body().getData().get(j).getAddress();
                                txt_review_username = response.body().getData().get(j).getFirstName();
                                txt_review_userlastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                txt_review_caption = response.body().getData().get(j).getHotelReview();
                                txt_review_like = response.body().getData().get(j).getTotalLikes();
                                txt_review_comment = response.body().getData().get(j).getTotalComments();
                                txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                                hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                                hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                                total_followers = response.body().getData().get(j).getTotalFollowers();
                                total_followings = response.body().getData().get(j).getTotalFollowings();
                                foodexp = response.body().getData().get(j).getFoodExprience();
                                top_count = response.body().getData().get(j).getTopCount();
                                dishtoavoid = response.body().getData().get(j).getAvoiddish();
                                avoid_count = response.body().getData().get(j).getAvoidCount();
                                ambi_image_count = response.body().getData().get(j).getAmbiImageCount();
                                dishname = response.body().getData().get(j).getTopdish();
                                delivery_mode = response.body().getData().get(j).getDelivery_mode();
                                top_dish_img_count = response.body().getData().get(j).getTop_dish_img_count();
                                avoid_dish_img_count = response.body().getData().get(j).getAvoid_dish_img_count();
                                bucketlist = response.body().getData().get(j).getBucket_list();
                                redirect_url = response.body().getData().get(j).getRedirect_url();
                                packCount = response.body().getData().get(j).getPackImageCount();


                                ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();


                                if (top_count == 0) {

                                } else {
                                    for (int b = 0; b < response.body().getData().get(j).getTopdishimage().size(); b++) {
                                        topdishimage = response.body().getData().get(j).getTopdishimage().get(b).getImg();
                                        topdishimage_name = response.body().getData().get(j).getTopdishimage().get(b).getDishname();
                                        ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimage_name);
                                        reviewTopDishPojoArrayList.add(reviewTopDishPojo);
                                    }

                                }
                                //Avoid count

                                ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();


                                if (avoid_count == 0) {

                                } else {

                                    for (int a = 0; a < response.body().getData().get(j).getAvoiddishimage().size(); a++) {

                                        String dishavoidimage = response.body().getData().get(j).getAvoiddishimage().get(a).getImg();
                                        avoiddishimage_name = response.body().getData().get(j).getAvoiddishimage().get(a).getDishname();
                                        ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(dishavoidimage, avoiddishimage_name);
                                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);
                                    }

                                }


                                //Ambiance

                                ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

                                if (ambi_image_count == 0) {


                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getAmbiImage().size(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getAmbiImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(img);

                                        reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);
                                    }


                                }

                                //packaging

                                ArrayList<PackImage> reviewpackImagePojoArrayList = new ArrayList<>();


                                if (packCount == 0) {

                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getPackImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getPackImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        PackImage packImage = new PackImage(img);

                                        reviewpackImagePojoArrayList.add(packImage);
                                    }

                                }


                                ReviewInputAllPojo reviewInputAllPojo = new ReviewInputAllPojo(hotelId, revratID, txt_review_caption, txt_review_like, txt_review_comment,
                                        category_type, veg_nonveg, foodexp, ambiance, modeid, comp_name, taste, service, package_value, time_delivery,
                                        valuemoney, createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude, longitude, phone,
                                        photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname, emailID, "", "", "", "",
                                        total_followers, total_followings, userimage, likes_hotelID, revrathotel_likes, followingID, totalreview,
                                        txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service, total_package, total_timedelivery,
                                        total_value, total_good, total_bad, total_good_bad_user, reviewTopDishPojoArrayList, topdishimage_count,
                                        dishname, top_count, reviewAvoidDishPojoArrayList, avoiddishimage_count, dishtoavoid, avoid_count,
                                        reviewAmbiImagePojoArrayList, ambi_image_count, false, delivery_mode, top_dish_img_count,
                                        avoid_dish_img_count, bucketlist, redirect_url, reviewpackImagePojoArrayList, packCount);


                                PaginationArrayList.add(reviewInputAllPojo);
                                getallHotelReviewPojoArrayList.addAll(PaginationArrayList);

                                LinkedHashSet<ReviewInputAllPojo> s = new LinkedHashSet<>(getallHotelReviewPojoArrayList);
                                getallHotelReviewPojoArrayList.clear();
                                getallHotelReviewPojoArrayList.addAll(s);


                            }  catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList, fromwhich, NewDineInFragment.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                        rvReview.setLayoutManager(mLayoutManager);
                        rvReview.setItemAnimator(new DefaultItemAnimator());
                        rvReview.setAdapter(getHotelDetailsAdapter);
                        rvReview.setNestedScrollingEnabled(false);
                        getHotelDetailsAdapter.notifyDataSetChanged();
                        progressbarReview.setVisibility(View.GONE);

                    } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {
                        imgNoDish.setVisibility(View.VISIBLE);
                        txtNoDish.setVisibility(View.VISIBLE);
                    }


                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    pageLoader.setVisibility(View.GONE);

                    Toast.makeText(context, "You've seen all data.. No new data found.", Toast.LENGTH_SHORT).show();
                }
            });

        } else {

            Snackbar snackbar = Snackbar.make(rlHotel, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    /* DINE IN API CALL */
    private void get_hotel_review_dineinswipe() {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");


            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_category_type_updated("get_hotel_review_category_type", 1, Integer.parseInt(userId), 1);
            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {

                        progressbarReview.setVisibility(View.GONE);
                        getallHotelReviewPojoArrayList.clear();


                        if (response.body().getData().size() > 0) {

                            pagecount += 1;
                            Pref_storage.setDetail(context, "Page_dinein", String.valueOf(pagecount));
                            Log.e(TAG, "onResponse:shared_page-->" + Pref_storage.getDetail(context, "Page_dinein"));

                        } else {

                        }

                        getallHotelReviewPojoArrayList = response.body().getData();
                        Set<ReviewInputAllPojo> s = new LinkedHashSet<>(getallHotelReviewPojoArrayList);
                        getallHotelReviewPojoArrayList.clear();
                        getallHotelReviewPojoArrayList.addAll(s);
                        Log.e("FailureError", "----------> " + getallHotelReviewPojoArrayList.size());

                        if (getallHotelReviewPojoArrayList.size() > 0) {

                            getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList, fromwhich, NewDineInFragment.this);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvReview.setLayoutManager(linearLayoutManager);
                            rvReview.setVisibility(View.VISIBLE);
                            rvReview.setAdapter(getHotelDetailsAdapter);
                            rvReview.setNestedScrollingEnabled(false);
                            getHotelDetailsAdapter.notifyDataSetChanged();


                        } else {

                            imgNoDish.setVisibility(View.VISIBLE);
                            txtNoDish.setVisibility(View.VISIBLE);
                        }

                    } else if (response.body().getResponseCode() == 0 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {

                        imgNoDish.setVisibility(View.VISIBLE);
                        txtNoDish.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    if (getallHotelReviewPojoArrayList.size() == 0) {
                        imgNoDish.setVisibility(View.VISIBLE);
                        txtNoDish.setVisibility(View.VISIBLE);
                    } else {

                    }
                }
            });
        } else {

            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();
           /* Snackbar snackbar = Snackbar.make(rlHotel, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();*/
        }
    }


    /* Pagination API Call */
    private void get_hotel_review_dinein(int pagecountt) {

        pageLoader.setVisibility(View.VISIBLE);


        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");

            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_category_type_updated("get_hotel_review_category_type",
                    1, Integer.parseInt(userId), pagecountt);

            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {
                        PaginationArrayList.clear();


                        loaderInt = 0;

                        pageLoader.setVisibility(View.GONE);

                        if (Pref_storage.getDetail(context, "Page_dinein").isEmpty() || Pref_storage.getDetail(context, "Page_dinein") == null) {


                        } else {

                            int page_incre = Integer.parseInt(Pref_storage.getDetail(context, "Page_dinein"));

                            Log.e(TAG, "onResponsePagination: Page_dinein-->" + page_incre);


                            int page_incre_shared = page_incre + 1;

                            Pref_storage.setDetail(context, "Page_dinein", String.valueOf(page_incre_shared));

                            Log.e(TAG, "onResponsePagination: page_count_incre-->" + Integer.parseInt(Pref_storage.getDetail(context, "Page_dinein")));
                            pagecount = Integer.parseInt(Pref_storage.getDetail(context, "Page_dinein"));

                        }


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());


                            try {


                                hotelId = response.body().getData().get(j).getHotelId();
                                cat_type = response.body().getData().get(j).getCategoryType();
                                veg_nonveg = response.body().getData().get(j).getVegNonveg();
                                revratID = response.body().getData().get(j).getRevratId();
                                ambiance = response.body().getData().get(j).getAmbiance();
                                modeid = response.body().getData().get(j).getMode_id();
                                comp_name = response.body().getData().get(j).getComp_name();
                                taste = response.body().getData().get(j).getTaste();
                                total_package = response.body().getData().get(j).getTotalPackage();
                                total_timedelivery = response.body().getData().get(j).getTotalTimedelivery();
                                service = response.body().getData().get(j).getService();
                                valuemoney = response.body().getData().get(j).getValueMoney();
                                createdby = response.body().getData().get(j).getCreatedBy();
                                createdon = response.body().getData().get(j).getCreatedOn();
                                googleID = response.body().getData().get(j).getGoogleId();
                                placeID = response.body().getData().get(j).getPlaceId();
                                opentimes = response.body().getData().get(j).getOpenTimes();
                                category_type = response.body().getData().get(j).getCategoryType();
                                latitude = response.body().getData().get(j).getLatitude();
                                longitude = response.body().getData().get(j).getLongitude();
                                phone = response.body().getData().get(j).getPhone();
                                photo_reference = response.body().getData().get(j).getPhotoReference();
                                emailID = response.body().getData().get(j).getEmail();
                                likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                                revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                                followingID = response.body().getData().get(j).getFollowingId();
                                totalreview = response.body().getData().get(j).getTotalReview();
                                total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                                total_taste = response.body().getData().get(j).getTotalTaste();
                                package_value = response.body().getData().get(j).get_package();
                                total_service = response.body().getData().get(j).getTotalService();
                                total_value = response.body().getData().get(j).getTotalValueMoney();
                                total_good = response.body().getData().get(j).getTotalGood();
                                total_bad = response.body().getData().get(j).getTotalBad();
                                total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();

                                txt_hotelname = response.body().getData().get(j).getHotelName();
                                user_id = response.body().getData().get(j).getUserId();
                                hotel_id = response.body().getData().get(j).getHotelId();
                                txt_hotel_address = response.body().getData().get(j).getAddress();

                                txt_review_username = response.body().getData().get(j).getFirstName();
                                txt_review_userlastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                txt_review_caption = response.body().getData().get(j).getHotelReview();
                                txt_review_like = response.body().getData().get(j).getTotalLikes();
                                txt_review_comment = response.body().getData().get(j).getTotalComments();
                                txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                                hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                                hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                                total_followers = response.body().getData().get(j).getTotalFollowers();
                                total_followings = response.body().getData().get(j).getTotalFollowings();
                                foodexp = response.body().getData().get(j).getFoodExprience();
                                top_count = response.body().getData().get(j).getTopCount();
                                dishtoavoid = response.body().getData().get(j).getAvoiddish();
//                                topdishimage_count= response.body().getData().get(j).getTopdishimageCount();
//                                avoiddishimage_count= response.body().getData().get(j).getAvoiddishimageCount();
                                avoid_count = response.body().getData().get(j).getAvoidCount();
                                ambi_image_count = response.body().getData().get(j).getAmbiImageCount();
                                dishname = response.body().getData().get(j).getTopdish();
                                delivery_mode = response.body().getData().get(j).getDelivery_mode();
                                top_dish_img_count = response.body().getData().get(j).getTop_dish_img_count();
                                avoid_dish_img_count = response.body().getData().get(j).getAvoid_dish_img_count();
                                bucketlist = response.body().getData().get(j).getBucket_list();
                                redirect_url = response.body().getData().get(j).getRedirect_url();
                                packCount = response.body().getData().get(j).getPackImageCount();

                                //Top dish

                                ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();


                                if (top_count == 0) {

                                } else {
                                    for (int b = 0; b < response.body().getData().get(j).getTopdishimage().size(); b++) {
                                        topdishimage = response.body().getData().get(j).getTopdishimage().get(b).getImg();
                                        topdishimage_name = response.body().getData().get(j).getTopdishimage().get(b).getDishname();
                                        ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimage_name);
                                        reviewTopDishPojoArrayList.add(reviewTopDishPojo);
                                    }

                                }
                                //Avoid count

                                ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();


                                if (avoid_count == 0) {

                                } else {

                                    for (int a = 0; a < response.body().getData().get(j).getAvoiddishimage().size(); a++) {

                                        String dishavoidimage = response.body().getData().get(j).getAvoiddishimage().get(a).getImg();
                                        avoiddishimage_name = response.body().getData().get(j).getAvoiddishimage().get(a).getDishname();
                                        ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(dishavoidimage, avoiddishimage_name);
                                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);
                                    }

                                }


                                //Ambiance

                                ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

                                if (ambi_image_count == 0) {


                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getAmbiImage().size(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getAmbiImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(img);

                                        reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);
                                    }
                                }

                                //packaging

                                ArrayList<PackImage> reviewpackImagePojoArrayList = new ArrayList<>();


                                if (packCount == 0) {

                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getPackImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getPackImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        PackImage packImage = new PackImage(img);

                                        reviewpackImagePojoArrayList.add(packImage);
                                    }

                                }
                                ReviewInputAllPojo reviewInputAllPojo = new ReviewInputAllPojo(hotelId, revratID, txt_review_caption, txt_review_like, txt_review_comment,
                                        category_type, veg_nonveg, foodexp, ambiance, modeid, comp_name, taste, service, package_value, time_delivery,
                                        valuemoney, createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude, longitude, phone,
                                        photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname, emailID, "", "", "", "",
                                        total_followers, total_followings, userimage, likes_hotelID, revrathotel_likes, followingID, totalreview,
                                        txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service, total_package, total_timedelivery,
                                        total_value, total_good, total_bad, total_good_bad_user, reviewTopDishPojoArrayList, topdishimage_count,
                                        dishname, top_count, reviewAvoidDishPojoArrayList, avoiddishimage_count, dishtoavoid, avoid_count,
                                        reviewAmbiImagePojoArrayList, ambi_image_count, false, delivery_mode, top_dish_img_count,
                                        avoid_dish_img_count, bucketlist, redirect_url, reviewpackImagePojoArrayList, packCount);


                                getallHotelReviewPojoArrayList.add(reviewInputAllPojo);


                                // getallHotelReviewPojoArrayList.addAll(PaginationArrayList);
                                LinkedHashSet<ReviewInputAllPojo> s = new LinkedHashSet<>(getallHotelReviewPojoArrayList);
                                getallHotelReviewPojoArrayList.clear();
                                getallHotelReviewPojoArrayList.addAll(s);


                           /*     HashSet hs = new HashSet();
                                hs.addAll(getallHotelReviewPojoArrayList);
                                getallHotelReviewPojoArrayList.clear();
                                getallHotelReviewPojoArrayList.addAll(hs);*/


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        rvReview.setNestedScrollingEnabled(false);
                        getHotelDetailsAdapter.notifyDataSetChanged();

//                        getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList,fromwhich);
//                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
//                        rvReview.setLayoutManager(mLayoutManager);
//                        rvReview.setItemAnimator(new DefaultItemAnimator());
//                        rvReview.setAdapter(getHotelDetailsAdapter);
//                        rvReview.setNestedScrollingEnabled(false);
//                        progressbarReview.setVisibility(View.GONE);
//                        pageLoader.setVisibility(View.GONE);


                    } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {
                        imgNoDish.setVisibility(View.VISIBLE);
                        txtNoDish.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                    pageLoader.setVisibility(View.GONE);
                    Toast.makeText(context, "You've seen all data.. No new data found.", Toast.LENGTH_SHORT).show();

//                    imgNoDish.setVisibility(View.VISIBLE);
//                    txtNoDish.setVisibility(View.VISIBLE);
                }
            });
        } else {

            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();
           /* Snackbar snackbar = Snackbar.make(rlHotel, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();*/
        }


    }

    /* Resume API */


    /* Filter veg API */
    private void get_hotel_review_veg() {
        if (Utility.isConnected(context)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");

            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_veg("get_hotel_review_vegnonveg", 1, 1, Integer.parseInt(userId));
            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {
                        getallHotelReviewPojoArrayList.clear();

                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());


                            try {


                                hotelId = response.body().getData().get(j).getHotelId();
                                cat_type = response.body().getData().get(j).getCategoryType();
                                veg_nonveg = response.body().getData().get(j).getVegNonveg();
                                revratID = response.body().getData().get(j).getRevratId();
                                modeid = response.body().getData().get(j).getMode_id();
                                comp_name = response.body().getData().get(j).getComp_name();

                                ambiance = response.body().getData().get(j).getAmbiance();
                                taste = response.body().getData().get(j).getTaste();
                                total_package = response.body().getData().get(j).getTotalPackage();
                                total_timedelivery = response.body().getData().get(j).getTotalTimedelivery();
                                service = response.body().getData().get(j).getService();
                                valuemoney = response.body().getData().get(j).getValueMoney();
                                createdby = response.body().getData().get(j).getCreatedBy();
                                createdon = response.body().getData().get(j).getCreatedOn();
                                googleID = response.body().getData().get(j).getGoogleId();
                                placeID = response.body().getData().get(j).getPlaceId();
                                opentimes = response.body().getData().get(j).getOpenTimes();
                                category_type = response.body().getData().get(j).getCategoryType();
                                latitude = response.body().getData().get(j).getLatitude();
                                longitude = response.body().getData().get(j).getLongitude();
                                phone = response.body().getData().get(j).getPhone();
                                photo_reference = response.body().getData().get(j).getPhotoReference();
                                emailID = response.body().getData().get(j).getEmail();
                                likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                                revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                                followingID = response.body().getData().get(j).getFollowingId();
                                totalreview = response.body().getData().get(j).getTotalReview();
                                total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                                total_taste = response.body().getData().get(j).getTotalTaste();
                                package_value = response.body().getData().get(j).get_package();
                                total_service = response.body().getData().get(j).getTotalService();
                                total_value = response.body().getData().get(j).getTotalValueMoney();
                                total_good = response.body().getData().get(j).getTotalGood();
                                total_bad = response.body().getData().get(j).getTotalBad();
                                total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();

                                txt_hotelname = response.body().getData().get(j).getHotelName();
                                user_id = response.body().getData().get(j).getUserId();
                                hotel_id = response.body().getData().get(j).getHotelId();
                                txt_hotel_address = response.body().getData().get(j).getAddress();

                                txt_review_username = response.body().getData().get(j).getFirstName();
                                txt_review_userlastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                txt_review_caption = response.body().getData().get(j).getHotelReview();
                                txt_review_like = response.body().getData().get(j).getTotalLikes();
                                txt_review_comment = response.body().getData().get(j).getTotalComments();
                                txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                                hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                                hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                                total_followers = response.body().getData().get(j).getTotalFollowers();
                                total_followings = response.body().getData().get(j).getTotalFollowings();
                                foodexp = response.body().getData().get(j).getFoodExprience();
                                top_count = response.body().getData().get(j).getTopCount();
                                dishtoavoid = response.body().getData().get(j).getAvoiddish();
//                                topdishimage_count= response.body().getData().get(j).getTopdishimageCount();
//                                avoiddishimage_count= response.body().getData().get(j).getAvoiddishimageCount();
                                avoid_count = response.body().getData().get(j).getAvoidCount();
                                ambi_image_count = response.body().getData().get(j).getAmbiImageCount();
                                dishname = response.body().getData().get(j).getTopdish();
                                delivery_mode = response.body().getData().get(j).getDelivery_mode();
                                top_dish_img_count = response.body().getData().get(j).getTop_dish_img_count();
                                avoid_dish_img_count = response.body().getData().get(j).getAvoid_dish_img_count();
                                bucketlist = response.body().getData().get(j).getBucket_list();
                                redirect_url = response.body().getData().get(j).getRedirect_url();
                                packCount = response.body().getData().get(j).getPackImageCount();


                                ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();


                                if (top_count == 0) {

                                } else {
                                    for (int b = 0; b < response.body().getData().get(j).getTopdishimage().size(); b++) {
                                        topdishimage = response.body().getData().get(j).getTopdishimage().get(b).getImg();
                                        topdishimage_name = response.body().getData().get(j).getTopdishimage().get(b).getDishname();
                                        ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimage_name);
                                        reviewTopDishPojoArrayList.add(reviewTopDishPojo);
                                    }

                                }
                                //Avoid count

                                ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();


                                if (avoid_count == 0) {

                                } else {

                                    for (int a = 0; a < response.body().getData().get(j).getAvoiddishimage().size(); a++) {

                                        String dishavoidimage = response.body().getData().get(j).getAvoiddishimage().get(a).getImg();
                                        avoiddishimage_name = response.body().getData().get(j).getAvoiddishimage().get(a).getDishname();
                                        ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(dishavoidimage, avoiddishimage_name);
                                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);
                                    }

                                }


                                //Ambiance

                                ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

                                if (ambi_image_count == 0) {


                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getAmbiImage().size(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getAmbiImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(img);

                                        reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);
                                    }


                                }
//packaging

                                ArrayList<PackImage> reviewpackImagePojoArrayList = new ArrayList<>();


                                if (packCount == 0) {

                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getPackImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getPackImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        PackImage packImage = new PackImage(img);

                                        reviewpackImagePojoArrayList.add(packImage);
                                    }

                                }

                                ReviewInputAllPojo reviewInputAllPojo = new ReviewInputAllPojo(hotelId, revratID, txt_review_caption, txt_review_like, txt_review_comment,
                                        category_type, veg_nonveg, foodexp, ambiance, modeid, comp_name, taste, service, package_value, time_delivery,
                                        valuemoney, createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude, longitude, phone,
                                        photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname, emailID, "", "", "", "",
                                        total_followers, total_followings, userimage, likes_hotelID, revrathotel_likes, followingID, totalreview,
                                        txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service, total_package, total_timedelivery,
                                        total_value, total_good, total_bad, total_good_bad_user, reviewTopDishPojoArrayList, topdishimage_count,
                                        dishname, top_count, reviewAvoidDishPojoArrayList, avoiddishimage_count, dishtoavoid, avoid_count,
                                        reviewAmbiImagePojoArrayList, ambi_image_count, false, delivery_mode, top_dish_img_count,
                                        avoid_dish_img_count, bucketlist, redirect_url, reviewpackImagePojoArrayList, packCount);


                                getallHotelReviewPojoArrayList.add(reviewInputAllPojo);


                            }/**/ catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList, fromwhich, NewDineInFragment.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                        rvReview.setLayoutManager(mLayoutManager);
                        rvReview.setItemAnimator(new DefaultItemAnimator());
                        rvReview.setAdapter(getHotelDetailsAdapter);
                        rvReview.setNestedScrollingEnabled(false);
                        getHotelDetailsAdapter.notifyDataSetChanged();
                        progressbarReview.setVisibility(View.GONE);

                    } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {
                        imgNoDish.setVisibility(View.VISIBLE);
                        txtNoDish.setVisibility(View.VISIBLE);
                    }


                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });

        } else {

            Snackbar snackbar = Snackbar.make(rlHotel, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }


    /* Filter non veg API */

    private void get_hotel_review_nonveg() {
        if (Utility.isConnected(context)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");

            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_nonveg("get_hotel_review_vegnonveg", 1, 2, Integer.parseInt(userId));
            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {
                        getallHotelReviewPojoArrayList.clear();


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());


                            try {


                                hotelId = response.body().getData().get(j).getHotelId();
                                cat_type = response.body().getData().get(j).getCategoryType();
                                veg_nonveg = response.body().getData().get(j).getVegNonveg();
                                revratID = response.body().getData().get(j).getRevratId();
                                modeid = response.body().getData().get(j).getMode_id();
                                comp_name = response.body().getData().get(j).getComp_name();

                                ambiance = response.body().getData().get(j).getAmbiance();
                                taste = response.body().getData().get(j).getTaste();
                                total_package = response.body().getData().get(j).getTotalPackage();
                                total_timedelivery = response.body().getData().get(j).getTotalTimedelivery();
                                service = response.body().getData().get(j).getService();
                                valuemoney = response.body().getData().get(j).getValueMoney();
                                createdby = response.body().getData().get(j).getCreatedBy();
                                createdon = response.body().getData().get(j).getCreatedOn();
                                googleID = response.body().getData().get(j).getGoogleId();
                                placeID = response.body().getData().get(j).getPlaceId();
                                opentimes = response.body().getData().get(j).getOpenTimes();
                                category_type = response.body().getData().get(j).getCategoryType();
                                latitude = response.body().getData().get(j).getLatitude();
                                longitude = response.body().getData().get(j).getLongitude();
                                phone = response.body().getData().get(j).getPhone();
                                photo_reference = response.body().getData().get(j).getPhotoReference();
                                emailID = response.body().getData().get(j).getEmail();
                                likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                                revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                                followingID = response.body().getData().get(j).getFollowingId();
                                totalreview = response.body().getData().get(j).getTotalReview();
                                total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                                total_taste = response.body().getData().get(j).getTotalTaste();
                                package_value = response.body().getData().get(j).get_package();
                                total_service = response.body().getData().get(j).getTotalService();
                                total_value = response.body().getData().get(j).getTotalValueMoney();
                                total_good = response.body().getData().get(j).getTotalGood();
                                total_bad = response.body().getData().get(j).getTotalBad();
                                total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();

                                txt_hotelname = response.body().getData().get(j).getHotelName();
                                user_id = response.body().getData().get(j).getUserId();
                                hotel_id = response.body().getData().get(j).getHotelId();
                                txt_hotel_address = response.body().getData().get(j).getAddress();

                                txt_review_username = response.body().getData().get(j).getFirstName();
                                txt_review_userlastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                txt_review_caption = response.body().getData().get(j).getHotelReview();
                                txt_review_like = response.body().getData().get(j).getTotalLikes();
                                txt_review_comment = response.body().getData().get(j).getTotalComments();
                                txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                                hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                                hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                                total_followers = response.body().getData().get(j).getTotalFollowers();
                                total_followings = response.body().getData().get(j).getTotalFollowings();
                                foodexp = response.body().getData().get(j).getFoodExprience();
                                top_count = response.body().getData().get(j).getTopCount();
                                dishtoavoid = response.body().getData().get(j).getAvoiddish();
//                                topdishimage_count= response.body().getData().get(j).getTopdishimageCount();
//                                avoiddishimage_count= response.body().getData().get(j).getAvoiddishimageCount();
                                avoid_count = response.body().getData().get(j).getAvoidCount();
                                ambi_image_count = response.body().getData().get(j).getAmbiImageCount();
                                dishname = response.body().getData().get(j).getTopdish();
                                delivery_mode = response.body().getData().get(j).getDelivery_mode();
                                top_dish_img_count = response.body().getData().get(j).getTop_dish_img_count();
                                avoid_dish_img_count = response.body().getData().get(j).getAvoid_dish_img_count();
                                bucketlist = response.body().getData().get(j).getBucket_list();
                                redirect_url = response.body().getData().get(j).getRedirect_url();
                                packCount = response.body().getData().get(j).getPackImageCount();


                                ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();


                                if (top_count == 0) {

                                } else {
                                    for (int b = 0; b < response.body().getData().get(j).getTopdishimage().size(); b++) {
                                        topdishimage = response.body().getData().get(j).getTopdishimage().get(b).getImg();
                                        topdishimage_name = response.body().getData().get(j).getTopdishimage().get(b).getDishname();
                                        ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimage_name);
                                        reviewTopDishPojoArrayList.add(reviewTopDishPojo);
                                    }

                                }
                                //Avoid count

                                ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();


                                if (avoid_count == 0) {

                                } else {

                                    for (int a = 0; a < response.body().getData().get(j).getAvoiddishimage().size(); a++) {

                                        String dishavoidimage = response.body().getData().get(j).getAvoiddishimage().get(a).getImg();
                                        avoiddishimage_name = response.body().getData().get(j).getAvoiddishimage().get(a).getDishname();
                                        ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(dishavoidimage, avoiddishimage_name);
                                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);
                                    }

                                }


                                //Ambiance

                                ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

                                if (ambi_image_count == 0) {


                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getAmbiImage().size(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getAmbiImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(img);

                                        reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);
                                    }


                                }


                                //packaging

                                ArrayList<PackImage> reviewpackImagePojoArrayList = new ArrayList<>();


                                if (packCount == 0) {

                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getPackImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getPackImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        PackImage packImage = new PackImage(img);

                                        reviewpackImagePojoArrayList.add(packImage);
                                    }

                                }
                                ReviewInputAllPojo reviewInputAllPojo = new ReviewInputAllPojo(hotelId, revratID, txt_review_caption, txt_review_like, txt_review_comment,
                                        category_type, veg_nonveg, foodexp, ambiance, modeid, comp_name, taste, service, package_value, time_delivery,
                                        valuemoney, createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude, longitude, phone,
                                        photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname, emailID, "", "", "", "",
                                        total_followers, total_followings, userimage, likes_hotelID, revrathotel_likes, followingID, totalreview,
                                        txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service, total_package, total_timedelivery,
                                        total_value, total_good, total_bad, total_good_bad_user, reviewTopDishPojoArrayList, topdishimage_count,
                                        dishname, top_count, reviewAvoidDishPojoArrayList, avoiddishimage_count, dishtoavoid, avoid_count,
                                        reviewAmbiImagePojoArrayList, ambi_image_count, false, delivery_mode, top_dish_img_count,
                                        avoid_dish_img_count, bucketlist, redirect_url, reviewpackImagePojoArrayList, packCount);


                                getallHotelReviewPojoArrayList.add(reviewInputAllPojo);

                            }/**/ catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList, fromwhich, NewDineInFragment.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                        rvReview.setLayoutManager(mLayoutManager);
                        rvReview.setItemAnimator(new DefaultItemAnimator());
                        rvReview.setAdapter(getHotelDetailsAdapter);
                        rvReview.setNestedScrollingEnabled(false);
                        getHotelDetailsAdapter.notifyDataSetChanged();
                        progressbarReview.setVisibility(View.GONE);


                    } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {
                        imgNoDish.setVisibility(View.VISIBLE);
                        txtNoDish.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } else {

            Snackbar snackbar = Snackbar.make(rlHotel, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        View view = (View) scrollAllfragment.getChildAt(scrollAllfragment.getChildCount() - 1);
        int diff = (view.getBottom() - (scrollAllfragment.getHeight() + scrollAllfragment.getScrollY()));

        // if diff is zero, then the bottom has been reached

        Log.e(TAG, "onScrollChange: PageCount Shared--> " + Pref_storage.getDetail(context, "Page_dinein"));


        if (diff == 0) {
            loaderInt++;

            if (loaderInt == 2) {


                //scroll view is at bottom

                //  Toast.makeText(context, "Bottom reached", Toast.LENGTH_SHORT).show();


                if (sort == null) {

                    if (pagecount == 1) {

                    } else {


                        if (Pref_storage.getDetail(context, "Page_dinein").isEmpty() || Pref_storage.getDetail(context, "Page_dinein") == null) {

                        } else {
                            pagecount = Integer.parseInt(Pref_storage.getDetail(context, "Page_dinein"));

                            Log.e(TAG, "onScrollChange: pagecount " + pagecount);

                            get_hotel_review_dinein(pagecount);
                            loaderInt = 0;
                        }
                    }
                } else {

                    if (pagecountSort == 1) {

                    } else {

                        if (Pref_storage.getDetail(context, "Page_dineinsort").isEmpty() || Pref_storage.getDetail(context, "Page_dineinsort") == null) {

                        } else {
                            pagecountSort = Integer.parseInt(Pref_storage.getDetail(context, "Page_dineinsort"));

                            Log.e(TAG, "onScrollChange: pagecount " + pagecountSort);

                            get_hotel_review_sort(pagecountSort);
                            loaderInt = 0;
                        }
                    }
                }
            }
        }
    }


    @Override
    public void onPostCommitedView(View view, final int adapterPosition, View hotelDetailsView) {

        switch (view.getId()) {

            case R.id.txt_adapter_post:

                if (Utility.isConnected(Objects.requireNonNull(getActivity()))) {

                    review_comment = (EmojiconEditText) hotelDetailsView.findViewById(R.id.review_comment);

                    comments_progressbar = (ProgressBar) hotelDetailsView.findViewById(R.id.comments_progressbar);
                    comments_progressbar.setVisibility(View.VISIBLE);
                    comments_progressbar.setIndeterminate(true);

                    tv_view_all_comments = (TextView) hotelDetailsView.findViewById(R.id.tv_view_all_comments);

                    if (review_comment.getText().toString().isEmpty()) {

                        Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();

                    } else {

                        Utility.hideKeyboard(view);

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        try {

                            int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                            Call<ReviewCommentsPojo> call = apiService.get_create_edit_hotel_review_comments("create_edit_hotel_review_comments",
                                    Integer.parseInt(getallHotelReviewPojoArrayList.get(adapterPosition).getHotelId()), 0, userid, review_comment.getText().toString(), Integer.parseInt(getallHotelReviewPojoArrayList.get(adapterPosition).getRevratId()));

                            call.enqueue(new Callback<ReviewCommentsPojo>() {

                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onResponse(Call<ReviewCommentsPojo> call, Response<ReviewCommentsPojo> response) {

                                    review_comment.setText("");

                                    if (response.code() == 500) {

                                        comments_progressbar.setVisibility(View.GONE);
                                        comments_progressbar.setIndeterminate(false);

                                        Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();

                                    } else {

                                        comments_progressbar.setVisibility(View.GONE);
                                        comments_progressbar.setIndeterminate(false);


                                        if (response.body() != null) {

                                            if (Objects.requireNonNull(response.body()).getResponseMessage().equals("success")) {

                                                if (response.body().getData().size() > 0) {

                                                    if (tv_view_all_comments.getVisibility() == View.GONE) {

                                                        tv_view_all_comments.setVisibility(View.VISIBLE);
                                                        //String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                                        String comments = getString(R.string.view_one_comment);
                                                        tv_view_all_comments.setText(comments);

                                                    } else {

                                                        String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                                        tv_view_all_comments.setText(comments);
                                                    }


                                                    Intent intent = new Intent(context, Reviews_comments_all_activity.class);
                                                    intent.putExtra("posttype", getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType());
                                                    intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(adapterPosition).getHotelId());
                                                    intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(adapterPosition).getRevratId());
                                                    intent.putExtra("userid", getallHotelReviewPojoArrayList.get(adapterPosition).getUserId());
                                                    intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(adapterPosition).getHotelName());
                                                    intent.putExtra("overallrating", getallHotelReviewPojoArrayList.get(adapterPosition).getFoodExprience());

                                                    if (getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                                        intent.putExtra("totaltimedelivery", getallHotelReviewPojoArrayList.get(adapterPosition).getTotalTimedelivery());

                                                    } else {
                                                        intent.putExtra("totaltimedelivery", getallHotelReviewPojoArrayList.get(adapterPosition).getAmbiance());

                                                    }
                                                    intent.putExtra("taste_count", getallHotelReviewPojoArrayList.get(adapterPosition).getTaste());
                                                    intent.putExtra("vfm_rating", getallHotelReviewPojoArrayList.get(adapterPosition).getValueMoney());
                                                    if (getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                                        intent.putExtra("package_rating", getallHotelReviewPojoArrayList.get(adapterPosition).get_package());

                                                    } else {
                                                        intent.putExtra("package_rating", getallHotelReviewPojoArrayList.get(adapterPosition).getService());

                                                    }
                                                    intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(adapterPosition).getHotelName());
                                                    intent.putExtra("reviewprofile", getallHotelReviewPojoArrayList.get(adapterPosition).getPicture());
                                                    intent.putExtra("createdon", getallHotelReviewPojoArrayList.get(adapterPosition).getCreatedOn());
                                                    intent.putExtra("firstname", getallHotelReviewPojoArrayList.get(adapterPosition).getFirstName());
                                                    intent.putExtra("lastname", getallHotelReviewPojoArrayList.get(adapterPosition).getLastName());
                                                    if (getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                                        intent.putExtra("title_ambience", "Packaging");
                                                    } else {
                                                        intent.putExtra("title_ambience", "Hotel Ambience");
                                                    }
                                                    if (getallHotelReviewPojoArrayList.get(adapterPosition).getBucket_list() != 0) {
                                                        intent.putExtra("addbucketlist", "yes");
                                                    } else {
                                                        intent.putExtra("addbucketlist", "no");
                                                    }
                                                    startActivity(intent);


                                                } else {

                                                }

                                            } else {

                                            }
                                        }

                                    }
                                }

                                @Override
                                public void onFailure(Call<ReviewCommentsPojo> call, Throwable t) {
                                    //Error

                                    comments_progressbar.setVisibility(View.GONE);
                                    comments_progressbar.setIndeterminate(false);

                                    Log.e("Balaji", "" + t.getMessage());
                                }
                            });

                        } catch (Exception e) {

                            Log.e("Balaji", "" + e.getMessage());
                            e.printStackTrace();

                        }
                    }

                } else {

                    Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.img_shareadapter:

                chagePageView(adapterPosition);
                break;

        }
    }

    private void chagePageView(int adapterPosition) {

        Intent intent1 = new Intent(context, ChatActivity.class);
        if (getallHotelReviewPojoArrayList.get(adapterPosition).getRedirect_url() != null) {
            intent1.putExtra("redirect_url", getallHotelReviewPojoArrayList.get(adapterPosition).getRedirect_url());
        }
        Pref_storage.setDetail(getActivity(), "shareView", Constants.Reviews);
        Pref_storage.setDetail(getActivity(), "shareViewPage", Constants.NewDineIN);
        startActivity(intent1);
    }
}


