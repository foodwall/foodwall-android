package com.kaspontech.foodwall.reviewpackage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.kaspontech.foodwall.R;

import java.util.ArrayList;

public class New_restaurant extends AppCompatActivity {

    public static final String TAG = "CurrentLocNearByPlaces";
    private static final int LOC_REQ_CODE = 1;

    GeoDataClient geoDataClient;
    PlaceDetectionClient placeDetectionClient;
    RecyclerView recyclerView;
    PlacesRecyclerViewAdapter recyclerViewAdapter;
    PlaceLikelihoodBufferResponse likelyPlaces;
    PlaceLikelihood placeLikelihood;
    public static ArrayList<Place> placesList;

    Toolbar actionBar;
    ImageButton back;
    TextView toolbar_title;
    Places places;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurant_new);

        Toolbar tb = findViewById(R.id.action_bar_restaurant);
    /*    setSupportActionBar(tb);
        tb.setSubtitle("Near By Places");*/

        back = (ImageButton) tb.findViewById(R.id.back);
        toolbar_title = (TextView) tb.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Find near by restaurant");


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.places_lst);

        placeDetectionClient = Places.getPlaceDetectionClient(New_restaurant.this);

        getCurrentPlaceItems();

    }

    private void getCurrentPlaceItems() {
        if (isLocationAccessPermitted()) {
            getCurrentPlaceData();
        } else {
            requestLocationAccessPermission();
        }
    }


    @SuppressLint("MissingPermission")
    private void getCurrentPlaceData() {

        try {
            Task<PlaceLikelihoodBufferResponse> placeResult = placeDetectionClient.
                    getCurrentPlace(null);
            placeResult.addOnCompleteListener(new OnCompleteListener<PlaceLikelihoodBufferResponse>() {
                @Override
                public void onComplete(@NonNull Task<PlaceLikelihoodBufferResponse> task) {
                    placesList = new ArrayList<Place>();
                    PlaceLikelihoodBufferResponse likelyPlaces = task.getResult();
                    for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                        placesList.add(placeLikelihood.getPlace().freeze());
                    }
                    likelyPlaces.release();

                    PlacesRecyclerViewAdapter recyclerViewAdapter = new PlacesRecyclerViewAdapter(New_restaurant.this, placesList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(New_restaurant.this, LinearLayoutManager.HORIZONTAL, true);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setAdapter(recyclerViewAdapter);
                    recyclerView.smoothScrollToPosition(placesList.size() - 1);
                    recyclerViewAdapter.notifyDataSetChanged();

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }







   /* @SuppressLint("MissingPermission")
    private void getCurrentPlaceData() {
        Task<PlaceLikelihoodBufferResponse> placeResult = placeDetectionClient.
                getCurrentPlace(null);
        placeResult.addOnCompleteListener(new OnCompleteListener<PlaceLikelihoodBufferResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlaceLikelihoodBufferResponse> task) {
                Log.d(TAG, "current location places info");
                List<Place> placesList = new ArrayList<Place>();
                likelyPlaces= task.getResult();
                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                    Log.i(TAG,
                            String.format("Place '%s' has likelihood: %g", placeLikelihood.getPlace().getName(),
                                    placeLikelihood.getLikelihood()));
                }
                likelyPlaces.release();


                recyclerViewAdapter = new PlacesRecyclerViewAdapter(placesList,
                        getApplicationContext());
                LinearLayoutManager recyclerLayoutManager =
                        new LinearLayoutManager(New_restaurant.this);
                recyclerView.setLayoutManager(recyclerLayoutManager);

                DividerItemDecoration dividerItemDecoration =
                        new DividerItemDecoration(recyclerView.getContext(),
                                recyclerLayoutManager.getOrientation());
                recyclerView.addItemDecoration(dividerItemDecoration);
                recyclerView.setAdapter(recyclerViewAdapter);
            }
        });
    }*/

    private boolean isLocationAccessPermitted() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    private void requestLocationAccessPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOC_REQ_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOC_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                getCurrentPlaceData();
            }
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.places_menu, menu);
        return true;
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.view_places_m:
                getCurrentPlaceItems();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/
}