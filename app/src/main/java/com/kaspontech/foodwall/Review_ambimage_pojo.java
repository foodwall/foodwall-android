package com.kaspontech.foodwall;

public class Review_ambimage_pojo {

    private String amb_image;

    public Review_ambimage_pojo(String amb_image) {
        this.amb_image = amb_image;
    }

    public String getAmb_image() {
        return amb_image;
    }

    public void setAmb_image(String amb_image) {
        this.amb_image = amb_image;
    }
}
