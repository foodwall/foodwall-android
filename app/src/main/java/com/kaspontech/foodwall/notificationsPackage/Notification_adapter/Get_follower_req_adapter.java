package com.kaspontech.foodwall.notificationsPackage.Notification_adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_follower_req_input;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_unfollow_follower_output;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;


import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Get_follower_req_adapter extends RecyclerView.Adapter<Get_follower_req_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;

    private ArrayList<Get_follower_req_input> getFollowerReqInputArrayList = new ArrayList<>();

    String positionuser_id;
    private Get_follower_req_input getFollowerReqInput;


    public Get_follower_req_adapter(Context c, ArrayList<Get_follower_req_input> gettinglist) {
        this.context = c;
        this.getFollowerReqInputArrayList = gettinglist;
    }

    @NonNull
    @Override
    public Get_follower_req_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_follower_req_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Get_follower_req_adapter.MyViewHolder vh = new Get_follower_req_adapter.MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Get_follower_req_adapter.MyViewHolder holder, final int position) {


        getFollowerReqInput = getFollowerReqInputArrayList.get(position);

        holder.txt_username.setText(getFollowerReqInputArrayList.get(position).getFirstName());
        holder.txt_username.setTextColor(Color.parseColor("#000000"));

        holder.txt_userlastname.setText(getFollowerReqInputArrayList.get(position).getLastName());
        holder.txt_userlastname.setTextColor(Color.parseColor("#000000"));

        String fullname = getFollowerReqInputArrayList.get(position).getFirstName().concat(" ").concat(getFollowerReqInputArrayList.get(position).getLastName());

        holder.txt_fullname_user.setText(fullname);

        String ownuser_id = Pref_storage.getDetail(context, "userId");

        positionuser_id = getFollowerReqInputArrayList.get(position).getFollowerId();


//Profile image loading
        Utility.picassoImageLoader(getFollowerReqInputArrayList.get(position).getPicture(),
                0, holder.img_following, context);
//        GlideApp.with(context)
//                .load(getFollowerReqInputArrayList.get(position).getPicture())
//                .into(holder.img_following);


        holder.btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.btn_accept.getText().toString().equalsIgnoreCase("Accept")) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {
                        String createdby = Pref_storage.getDetail(context, "userId");
                        Call<Get_following_output_Pojo> call = apiService.create_follower("create_follower",
                                Integer.parseInt(getFollowerReqInputArrayList.get(position).getFollowerId()),
                                Integer.parseInt(createdby),
                                1);
                        call.enqueue(new Callback<Get_following_output_Pojo>() {
                            @Override
                            public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {

                                if (response.body().getResponseCode() == 1) {

                                    holder.btn_accept.setText(R.string.txt_following);
                                    holder.btn_accept.setTextColor(Color.BLACK);
                                    holder.btn_cancel.setVisibility(View.GONE);
                                    holder.btn_accept.setBackgroundResource(R.drawable.editext_login_background);

                                }
                            }

                            @Override
                            public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                /*else if(holder.btn_accept.getText().toString().equalsIgnoreCase("Follow")){
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    try {

                        String createdby = Pref_storage.getDetail(context, "userId");


                        Call<Get_following_output_Pojo> call = apiService.create_follower("create_follower", Integer.parseInt(createdby), Integer.parseInt(getFollowerReqInputArrayList.get(position).getFollowerId()), 1);
                        call.enqueue(new Callback<Get_following_output_Pojo>() {
                            @Override
                            public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {


                                if (response.body().getResponseCode() == 1) {
                                    holder.btn_accept.setText(R.string.following);

                                }

                            }

                            @Override
                            public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


*/


            /*    Get_follower_req_adapter.CustomDialogClass2 ccd = new Get_follower_req_adapter.CustomDialogClass2(context, R.style.PauseDialog);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                ccd.setCancelable(true);
                ccd.show();
*/


            }
        });

        holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {

                    String createdby = Pref_storage.getDetail(context, "userId");


                    Call<CommonOutputPojo> call = apiService.create_follower_request("create_follower_request",
                            Integer.parseInt(createdby), Integer.parseInt(getFollowerReqInputArrayList.get(position).getFollowerId()),
                            0);
                    call.enqueue(new Callback<CommonOutputPojo>() {
                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                            if(response.body().getData() != null){

                                if (response.body().getData() == 0) {

                                    getFollowerReqInputArrayList.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, getFollowerReqInputArrayList.size());

                                }

                            }

                        }

                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }


    //custom dialog for alert


    public class CustomDialogClass2 extends Dialog implements View.OnClickListener {

        public Activity c;
        public Dialog d;
        TextView txt_unfollow_yes, txt_unfollow_cancel, txt_alert;

        ImageView img_following;
        RelativeLayout rl_unfollowing_yes, rl_cancel;

        CustomDialogClass2(Context a, int pauseDialog) {
            super(a);
            // TODO Auto-generated constructor stub
            context = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_alert_dialog_timeline);

            txt_unfollow_yes = (TextView) findViewById(R.id.txt_unfollow_yes);

            txt_unfollow_cancel = (TextView) findViewById(R.id.txt_unfollow_cancel);

            txt_alert = (TextView) findViewById(R.id.txt_alert);
            txt_alert.setText(Pref_storage.getDetail(context, "followingname"));

            img_following = (ImageView) findViewById(R.id.img_following);
            rl_unfollowing_yes = (RelativeLayout) findViewById(R.id.rl_unfollowing_yes);
            rl_unfollowing_yes.setOnClickListener(this);
            rl_cancel = (RelativeLayout) findViewById(R.id.rl_cancel);
            rl_cancel.setOnClickListener(this);

            Utility.picassoImageLoader(Pref_storage.getDetail(context, "following_image"),
                    0, img_following, context);

//            GlideApp.with(context)
//                    .load(Pref_storage.getDetail(context, "following_image"))
//                    .into(img_following);

            txt_unfollow_yes.setOnClickListener(this);
            txt_unfollow_cancel.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.txt_unfollow_yes:


                    break;

                case R.id.txt_unfollow_cancel:
                    dismiss();
                    break;
                default:
                    break;
            }
        }
    }


    public static Drawable LoadImageFromWebURL(String url) {
        try {
            InputStream iStream = (InputStream) new URL(url).getContent();
            Drawable drawable = Drawable.createFromStream(iStream, "following name");
            return drawable;
        } catch (Exception e) {
            return null;
        }
    }


    @Override
    public int getItemCount() {

        return getFollowerReqInputArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_username, txt_userlastname, txt_fullname_user, txt_following, share, caption, captiontag, txt_created_on, txt_adapter_post;
        RelativeLayout rl_following, rl_likesnew_layout, rl_follower_lay, rl_follower_ayout, rlayout_following;

        ImageView img_following;
        ImageButton btn_cancel;
        Button btn_accept;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_userlastname = (TextView) itemView.findViewById(R.id.txt_userlastname);
            txt_fullname_user = (TextView) itemView.findViewById(R.id.txt_fullname_user);
            txt_following = (TextView) itemView.findViewById(R.id.txt_following);
            img_following = (ImageView) itemView.findViewById(R.id.img_following);
            btn_accept = (Button) itemView.findViewById(R.id.btn_accept);
            btn_cancel = (ImageButton) itemView.findViewById(R.id.btn_cancel);


        }


    }


}
