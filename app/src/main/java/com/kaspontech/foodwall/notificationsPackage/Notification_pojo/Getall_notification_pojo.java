package com.kaspontech.foodwall.notificationsPackage.Notification_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Getall_notification_pojo {
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("note_description")
    @Expose
    private String noteDescription;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("related_id")
    @Expose
    private String relatedId;
    @SerializedName("multi")
    @Expose
    private List<Getall_inner_notification_pojo> multi = null;
    @SerializedName("multi_count")
    @Expose
    private Integer multiCount;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param postType
     * @param relatedId
     * @param multi
     * @param userid
     * @param noteDescription
     * @param multiCount
     */
    public Getall_notification_pojo(String postType, String noteDescription, String userid, String relatedId, List<Getall_inner_notification_pojo> multi, Integer multiCount) {
        super();
        this.postType = postType;
        this.noteDescription = noteDescription;
        this.userid = userid;
        this.relatedId = relatedId;
        this.multi = multi;
        this.multiCount = multiCount;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getNoteDescription() {
        return noteDescription;
    }

    public void setNoteDescription(String noteDescription) {
        this.noteDescription = noteDescription;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(String relatedId) {
        this.relatedId = relatedId;
    }

    public List<Getall_inner_notification_pojo> getMulti() {
        return multi;
    }

    public void setMulti(List<Getall_inner_notification_pojo> multi) {
        this.multi = multi;
    }

    public Integer getMultiCount() {
        return multiCount;
    }

    public void setMultiCount(Integer multiCount) {
        this.multiCount = multiCount;
    }

}
