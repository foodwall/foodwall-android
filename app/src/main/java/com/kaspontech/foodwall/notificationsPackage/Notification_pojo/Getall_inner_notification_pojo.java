package com.kaspontech.foodwall.notificationsPackage.Notification_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Getall_inner_notification_pojo {
    @SerializedName("note_id")
    @Expose
    private String noteId;
    @SerializedName("view_status")
    @Expose
    private String viewStatus;
    @SerializedName("related_id")
    @Expose
    private String relatedId;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("bio_description")
    @Expose
    private String bioDescription;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("event_id")
    @Expose
    private String eventId;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("cmmt_evt_id")
    @Expose
    private String cmmtEvtId;
    @SerializedName("evt_comments")
    @Expose
    private String evtComments;
    @SerializedName("timeline_id")
    @Expose
    private String timelineId;
    @SerializedName("timeline_description")
    @Expose
    private String timelineDescription;
    @SerializedName("cmmt_tl_id")
    @Expose
    private String cmmtTlId;
    @SerializedName("tl_comments")
    @Expose
    private String tlComments;
    @SerializedName("revrat_id")
    @Expose
    private String revratId;
    @SerializedName("hotel_review")
    @Expose
    private String hotelReview;
    @SerializedName("category_type")
    @Expose
    private String categoryType;
    @SerializedName("cmmt_htl_id")
    @Expose
    private String cmmtHtlId;
    @SerializedName("htl_rev_comments")
    @Expose
    private String htlRevComments;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param oauthProvider
     * @param hotelReview
     * @param cmmtEvtId
     * @param eventId
     * @param tlComments
     * @param userId
     * @param viewStatus
     * @param contNo
     * @param gender
     * @param firstName
     * @param lastName
     * @param postType
     * @param cmmtTlId
     * @param revratId
     * @param categoryType
     * @param imei
     * @param bioDescription
     * @param evtComments
     * @param noteId
     * @param oauthUid
     * @param createdOn
     * @param picture
     * @param email
     * @param relatedId
     * @param cmmtHtlId
     * @param dob
     * @param timelineId
     * @param eventName
     * @param timelineDescription
     * @param htlRevComments
     */
    public Getall_inner_notification_pojo(String noteId, String viewStatus, String relatedId, String postType, String createdOn, String userId, String oauthProvider, String oauthUid, String firstName, String lastName, String email, String imei, String contNo, String gender, String dob, String bioDescription, String picture, String eventId, String eventName, String cmmtEvtId, String evtComments, String timelineId, String timelineDescription, String cmmtTlId, String tlComments, String revratId, String hotelReview, String categoryType, String cmmtHtlId, String htlRevComments) {
        super();
        this.noteId = noteId;
        this.viewStatus = viewStatus;
        this.relatedId = relatedId;
        this.postType = postType;
        this.createdOn = createdOn;
        this.userId = userId;
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imei = imei;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.bioDescription = bioDescription;
        this.picture = picture;
        this.eventId = eventId;
        this.eventName = eventName;
        this.cmmtEvtId = cmmtEvtId;
        this.evtComments = evtComments;
        this.timelineId = timelineId;
        this.timelineDescription = timelineDescription;
        this.cmmtTlId = cmmtTlId;
        this.tlComments = tlComments;
        this.revratId = revratId;
        this.hotelReview = hotelReview;
        this.categoryType = categoryType;
        this.cmmtHtlId = cmmtHtlId;
        this.htlRevComments = htlRevComments;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getViewStatus() {
        return viewStatus;
    }

    public void setViewStatus(String viewStatus) {
        this.viewStatus = viewStatus;
    }

    public String getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(String relatedId) {
        this.relatedId = relatedId;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getBioDescription() {
        return bioDescription;
    }

    public void setBioDescription(String bioDescription) {
        this.bioDescription = bioDescription;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getCmmtEvtId() {
        return cmmtEvtId;
    }

    public void setCmmtEvtId(String cmmtEvtId) {
        this.cmmtEvtId = cmmtEvtId;
    }

    public String getEvtComments() {
        return evtComments;
    }

    public void setEvtComments(String evtComments) {
        this.evtComments = evtComments;
    }

    public String getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(String timelineId) {
        this.timelineId = timelineId;
    }

    public String getTimelineDescription() {
        return timelineDescription;
    }

    public void setTimelineDescription(String timelineDescription) {
        this.timelineDescription = timelineDescription;
    }

    public String getCmmtTlId() {
        return cmmtTlId;
    }

    public void setCmmtTlId(String cmmtTlId) {
        this.cmmtTlId = cmmtTlId;
    }

    public String getTlComments() {
        return tlComments;
    }

    public void setTlComments(String tlComments) {
        this.tlComments = tlComments;
    }

    public String getRevratId() {
        return revratId;
    }

    public void setRevratId(String revratId) {
        this.revratId = revratId;
    }

    public String getHotelReview() {
        return hotelReview;
    }

    public void setHotelReview(String hotelReview) {
        this.hotelReview = hotelReview;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCmmtHtlId() {
        return cmmtHtlId;
    }

    public void setCmmtHtlId(String cmmtHtlId) {
        this.cmmtHtlId = cmmtHtlId;
    }

    public String getHtlRevComments() {
        return htlRevComments;
    }

    public void setHtlRevComments(String htlRevComments) {
        this.htlRevComments = htlRevComments;
    }
}
