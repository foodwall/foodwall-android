package com.kaspontech.foodwall.notificationsPackage.Notification_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.R;

public class NotiEventCommentAdapter extends RecyclerView.ViewHolder {
    TextView txt_following_user_name, txt_follow_ago,txt_following_text;
    RelativeLayout rl_notification;
    ImageView notify_user_photo;

    public NotiEventCommentAdapter(View itemView) {
        super(itemView);




        txt_following_user_name = (TextView) itemView.findViewById(R.id.txt_following_user_name);
        txt_follow_ago = (TextView) itemView.findViewById(R.id.txt_follow_ago);
        txt_following_text = (TextView) itemView.findViewById(R.id.txt_following_text);

        //Imageview

        notify_user_photo = (ImageView) itemView.findViewById(R.id.notify_user_photo);

        //RelativeLayout
        rl_notification = (RelativeLayout) itemView.findViewById(R.id.rl_notification);
    }
}

