package com.kaspontech.foodwall.notificationsPackage.Notification_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_follow_req_input_count {
    @SerializedName("total_count")
    @Expose
    private String totalCount;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param totalCount
     */
    public Get_follow_req_input_count(String totalCount) {
        super();
        this.totalCount = totalCount;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }
}
