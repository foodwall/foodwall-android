package com.kaspontech.foodwall.notificationsPackage.Notification_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_Timeline_Notification_Images {

    @SerializedName("img_id")
    @Expose
    private String imgId;
    @SerializedName("img")
    @Expose
    private String img;

    /**
     * No args constructor for use in serialization
     *
     */
    public Get_Timeline_Notification_Images() {
    }

    /**
     *
     * @param imgId
     * @param img
     */
    public Get_Timeline_Notification_Images(String imgId, String img) {
        super();
        this.imgId = imgId;
        this.img = img;
    }

    public String getImgId() {
        return imgId;
    }

    public void setImgId(String imgId) {
        this.imgId = imgId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

}