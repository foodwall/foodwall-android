package com.kaspontech.foodwall.notificationsPackage;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.notificationsPackage.Notification_adapter.Get_follower_req_adapter;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_follower_req_input;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_follower_req_output;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Follower_request_activity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "Follower_request";

    // Action Bar
    Toolbar actionBar;
    ImageButton back;
    TextView toolbar_title;
    RelativeLayout rl_following, rl_following_list;
    RecyclerView rv_following;
    LinearLayoutManager llm;
    Get_follower_req_input getFollowingProfilePojo;
    ProgressBar progressBar;
    String userId;

    Get_follower_req_adapter getFollowerReqAdapter;
    ArrayList<Get_follower_req_input> getFollowerReqInputArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following_list);


        actionBar = (Toolbar) findViewById(R.id.actionbar_following);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText(R.string.follow_requests);
        rl_following = (RelativeLayout) findViewById(R.id.rl_following);
        rl_following_list = (RelativeLayout) findViewById(R.id.rl_following_list);
        rv_following = (RecyclerView) findViewById(R.id.rv_following);
        progressBar = (ProgressBar) findViewById(R.id.progress_dialog_following);
        llm = new LinearLayoutManager(this);
        rv_following.setLayoutManager(llm);

        try {
            rl_following.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            get_follow_request();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*Follow request API*/
    private void get_follow_request() {

        if (Utility.isConnected(Follower_request_activity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                userId = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_follower_req_output> call = apiService.get_follower_request("get_follower_request", Integer.parseInt(userId)); /*onlineCreateUserModel.getTimelineDescription(),onlineCreateUserModel.getCreatedBy(),onlineCreateUserModel.getImage()*/
                call.enqueue(new Callback<Get_follower_req_output>() {
                    @Override
                    public void onResponse(Call<Get_follower_req_output> call, Response<Get_follower_req_output> response) {

                        if (response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                String userid = response.body().getData().get(j).getUserid();
                                String created_on = response.body().getData().get(j).getCreatedOn();

                                String followerID = response.body().getData().get(j).getFollowerId();

                                String totalfollowers= response.body().getData().get(j).getTotalFollowers();
                                String totalfollowings = response.body().getData().get(j).getTotalFollowings();
                                String txt_username = response.body().getData().get(j).getFirstName();
                                String username = response.body().getData().get(j).getUsername();
                                String txt_lastusername = response.body().getData().get(j).getLastName();
                                String picture = response.body().getData().get(j).getPicture();

                                Get_follower_req_input getFollowerReqInput = new Get_follower_req_input(userid, created_on, followerID,
                                        "", "", txt_username, txt_lastusername, username,
                                        "", "", "",
                                        "", "", picture, "", "",
                                        "", "", "", totalfollowers,totalfollowings);

                                getFollowerReqInputArrayList.add(getFollowerReqInput);
                                getFollowerReqAdapter = new Get_follower_req_adapter(Follower_request_activity.this, getFollowerReqInputArrayList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Follower_request_activity.this);
                                rv_following.setLayoutManager(mLayoutManager);
                                rv_following.setHasFixedSize(true);
                                rv_following.setItemAnimator(new DefaultItemAnimator());
                                rv_following.setAdapter(getFollowerReqAdapter);
                                rv_following.setNestedScrollingEnabled(false);
                                getFollowerReqAdapter.notifyDataSetChanged();
                                rl_following.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);

                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Get_follower_req_output> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Snackbar snackbar = Snackbar.make(rl_following_list, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }



    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:

                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
