package com.kaspontech.foodwall.notificationsPackage.Notification_adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Getall_notification_pojo;
import com.kaspontech.foodwall.R;



import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

//

public class Notification_adapter extends RecyclerView.Adapter<Notification_adapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;

    private ArrayList<Getall_notification_pojo> getallNotificationPojoArrayList = new ArrayList<>();
    private Getall_notification_pojo getallNotificationPojo;


    private Pref_storage pref_storage;


    public Notification_adapter(Context c, ArrayList<Getall_notification_pojo> gettinglist) {
        this.context = c;
        this.getallNotificationPojoArrayList = gettinglist;
    }


    @NonNull
    @Override
    public Notification_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_notification_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Notification_adapter.MyViewHolder vh = new Notification_adapter.MyViewHolder(v);

        pref_storage = new Pref_storage();

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Notification_adapter.MyViewHolder holder, final int position) {


        getallNotificationPojo = getallNotificationPojoArrayList.get(position);

        //reviewer_name
        String reviewer_name = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
        holder.txt_following_user_name.setText(reviewer_name);
        holder.txt_following_user_name.setTextColor(Color.parseColor("#000000"));


        //Reviewer Image

        Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                0,holder.notify_user_photo, context);

//        GlideApp.with(context)
//                .load(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture())
//                .into(holder.notify_user_photo);


// Timestamp for notification
        String createdon = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

        try {
            long now = System.currentTimeMillis();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date convertedDate = dateFormat.parse(createdon);

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now,

                    DateUtils.FORMAT_ABBREV_RELATIVE);
            if (relavetime1.toString().equalsIgnoreCase("0 minutes ago")) {
                holder.txt_follow_ago.setText(R.string.just_now);
            } else if (relavetime1.toString().contains("minutes ago")) {
                holder.txt_follow_ago.setText(relavetime1.toString().replace("minutes ago", "").concat("m"));
            } else if (relavetime1.toString().contains("hours ago")) {
                holder.txt_follow_ago.setText(relavetime1.toString().replace("hours ago", "").concat("h"));
            } else if (relavetime1.toString().contains("days ago")) {
                holder.txt_follow_ago.setText(relavetime1.toString().replace("days ago", "").concat("dialog"));
            } else {
                holder.txt_follow_ago.append(relavetime1 + "\n\n");

            }

        } catch (ParseException e) {
            e.printStackTrace();
        }




 /*       //Total likes

        String total_likes= getallHotelReviewPojoArrayList.get(position).getTotalLikes();

        if(Integer.parseInt(total_likes)==0||total_likes.equalsIgnoreCase(null)||total_likes.equalsIgnoreCase("")){

            holder.txt_review_like.setVisibility(View.GONE);
        }
        if(Integer.parseInt(total_likes)==1){
            holder.txt_review_like.setText(getallHotelReviewPojoArrayList.get(position).getTotalLikes().concat("").concat("like"));
            holder.txt_review_like.setVisibility(View.VISIBLE);

        }else if(Integer.parseInt(total_likes)>1){
            holder.txt_review_like.setText(getallHotelReviewPojoArrayList.get(position).getTotalLikes().concat("").concat("likes"));
            holder.txt_review_like.setVisibility(View.VISIBLE);
        }

*/


    }


    @Override
    public int getItemCount() {

        return getallNotificationPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_following_user_name, txt_follow_ago;
        RelativeLayout rl_notification;
        ImageView notify_user_photo;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_following_user_name = (TextView) itemView.findViewById(R.id.txt_following_user_name);
            txt_follow_ago = (TextView) itemView.findViewById(R.id.txt_follow_ago);

            //Imageview

            notify_user_photo = (ImageView) itemView.findViewById(R.id.notify_user_photo);

            //RelativeLayout
            rl_notification = (RelativeLayout) itemView.findViewById(R.id.rl_notification);

        }


    }


}

