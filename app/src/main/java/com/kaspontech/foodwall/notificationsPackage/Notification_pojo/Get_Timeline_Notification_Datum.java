package com.kaspontech.foodwall.notificationsPackage.Notification_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_Timeline_Notification_Datum {

    @SerializedName("timeline_id")
    @Expose
    private String timelineId;
    @SerializedName("timeline_hotel")
    @Expose
    private String timelineHotel;
    @SerializedName("timeline_description")
    @Expose
    private String timelineDescription;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("total_likes")
    @Expose
    private String totalLikes;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("visibility_type")
    @Expose
    private String visibilityType;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("bio_description")
    @Expose
    private String bioDescription;
    @SerializedName("total_followers")
    @Expose
    private String totalFollowers;
    @SerializedName("total_followings")
    @Expose
    private String totalFollowings;
    @SerializedName("total_posts")
    @Expose
    private String totalPosts;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("likes_tl_id")
    @Expose
    private String likesTlId;
    @SerializedName("tl_likes")
    @Expose
    private String tlLikes;
    @SerializedName("event_id")
    @Expose
    private String eventId;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_description")
    @Expose
    private String eventDescription;
    @SerializedName("event_image")
    @Expose
    private String eventImage;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("following_id")
    @Expose
    private String followingId;
    @SerializedName("followuser_id")
    @Expose
    private String followuserId;
    @SerializedName("followuser_firstname")
    @Expose
    private String followuserFirstname;
    @SerializedName("followuser_lastname")
    @Expose
    private String followuserLastname;
    @SerializedName("followuser_picture")
    @Expose
    private String followuserPicture;
    @SerializedName("hotel_id")
    @Expose
    private String hotelId;
    @SerializedName("hotel")
    @Expose
    private String hotel;
    @SerializedName("review_id")
    @Expose
    private String reviewId;
    @SerializedName("review")
    @Expose
    private String review;
    @SerializedName("review_comments")
    @Expose
    private String reviewComments;
    @SerializedName("quest_id")
    @Expose
    private String questId;
    @SerializedName("ask_question")
    @Expose
    private String askQuestion;
    @SerializedName("ans_id")
    @Expose
    private String ansId;
    @SerializedName("ask_answer")
    @Expose
    private String askAnswer;
    @SerializedName("gng_evt_id")
    @Expose
    private String gngEvtId;
    @SerializedName("like_evt_id")
    @Expose
    private String likeEvtId;
    @SerializedName("foll_userid")
    @Expose
    private String follUserid;
    @SerializedName("like_rev_id")
    @Expose
    private String likeRevId;
    @SerializedName("cmmt_rev_id")
    @Expose
    private String cmmtRevId;
    @SerializedName("ans_ques_id")
    @Expose
    private String ansQuesId;
    @SerializedName("up_ans_id")
    @Expose
    private String upAnsId;
    @SerializedName("whom_count")
    @Expose
    private Integer whomCount;
    @SerializedName("image")
    @Expose
    private List<Get_Timeline_Notification_Images> image = null;
    @SerializedName("image_count")
    @Expose
    private Integer imageCount;

    /**
     * No args constructor for use in serialization
     *
     */
    public Get_Timeline_Notification_Datum() {
    }

    /**
     *
     * @param oauthProvider
     * @param likesTlId
     * @param location
     * @param totalFollowers
     * @param endDate
     * @param eventId
     * @param userId
     * @param ansQuesId
     * @param gender
     * @param followuserFirstname
     * @param longitude
     * @param questId
     * @param followuserLastname
     * @param lastName
     * @param imei
     * @param totalPosts
     * @param followingId
     * @param bioDescription
     * @param image
     * @param createdOn
     * @param picture
     * @param followuserPicture
     * @param eventDescription
     * @param likeRevId
     * @param ansId
     * @param createdBy
     * @param visibilityType
     * @param email
     * @param whomCount
     * @param totalFollowings
     * @param eventName
     * @param latitude
     * @param reviewComments
     * @param timelineDescription
     * @param review
     * @param likeEvtId
     * @param startDate
     * @param totalComments
     * @param imageCount
     * @param gngEvtId
     * @param reviewId
     * @param hotelId
     * @param follUserid
     * @param contNo
     * @param firstName
     * @param askQuestion
     * @param postType
     * @param tlLikes
     * @param upAnsId
     * @param cmmtRevId
     * @param oauthUid
     * @param askAnswer
     * @param totalLikes
     * @param address
     * @param dob
     * @param timelineId
     * @param timelineHotel
     * @param eventImage
     * @param hotel
     * @param followuserId
     */
    public Get_Timeline_Notification_Datum(String timelineId, String timelineHotel, String timelineDescription, String address, String createdBy, String createdOn, String totalLikes, String totalComments, String latitude, String longitude, String visibilityType, String postType, String userId, String oauthProvider, String oauthUid, String firstName, String lastName, String email, String imei, String contNo, String gender, String dob, String bioDescription, String totalFollowers, String totalFollowings, String totalPosts, String picture, String likesTlId, String tlLikes, String eventId, String eventName, String eventDescription, String eventImage, String startDate, String endDate, String location, String followingId, String followuserId, String followuserFirstname, String followuserLastname, String followuserPicture, String hotelId, String hotel, String reviewId, String review, String reviewComments, String questId, String askQuestion, String ansId, String askAnswer, String gngEvtId, String likeEvtId, String follUserid, String likeRevId, String cmmtRevId, String ansQuesId, String upAnsId, Integer whomCount, List<Get_Timeline_Notification_Images> image, Integer imageCount) {
        super();
        this.timelineId = timelineId;
        this.timelineHotel = timelineHotel;
        this.timelineDescription = timelineDescription;
        this.address = address;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.totalLikes = totalLikes;
        this.totalComments = totalComments;
        this.latitude = latitude;
        this.longitude = longitude;
        this.visibilityType = visibilityType;
        this.postType = postType;
        this.userId = userId;
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imei = imei;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.bioDescription = bioDescription;
        this.totalFollowers = totalFollowers;
        this.totalFollowings = totalFollowings;
        this.totalPosts = totalPosts;
        this.picture = picture;
        this.likesTlId = likesTlId;
        this.tlLikes = tlLikes;
        this.eventId = eventId;
        this.eventName = eventName;
        this.eventDescription = eventDescription;
        this.eventImage = eventImage;
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = location;
        this.followingId = followingId;
        this.followuserId = followuserId;
        this.followuserFirstname = followuserFirstname;
        this.followuserLastname = followuserLastname;
        this.followuserPicture = followuserPicture;
        this.hotelId = hotelId;
        this.hotel = hotel;
        this.reviewId = reviewId;
        this.review = review;
        this.reviewComments = reviewComments;
        this.questId = questId;
        this.askQuestion = askQuestion;
        this.ansId = ansId;
        this.askAnswer = askAnswer;
        this.gngEvtId = gngEvtId;
        this.likeEvtId = likeEvtId;
        this.follUserid = follUserid;
        this.likeRevId = likeRevId;
        this.cmmtRevId = cmmtRevId;
        this.ansQuesId = ansQuesId;
        this.upAnsId = upAnsId;
        this.whomCount = whomCount;
        this.image = image;
        this.imageCount = imageCount;
    }

    public String getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(String timelineId) {
        this.timelineId = timelineId;
    }

    public String getTimelineHotel() {
        return timelineHotel;
    }

    public void setTimelineHotel(String timelineHotel) {
        this.timelineHotel = timelineHotel;
    }

    public String getTimelineDescription() {
        return timelineDescription;
    }

    public void setTimelineDescription(String timelineDescription) {
        this.timelineDescription = timelineDescription;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getVisibilityType() {
        return visibilityType;
    }

    public void setVisibilityType(String visibilityType) {
        this.visibilityType = visibilityType;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getBioDescription() {
        return bioDescription;
    }

    public void setBioDescription(String bioDescription) {
        this.bioDescription = bioDescription;
    }

    public String getTotalFollowers() {
        return totalFollowers;
    }

    public void setTotalFollowers(String totalFollowers) {
        this.totalFollowers = totalFollowers;
    }

    public String getTotalFollowings() {
        return totalFollowings;
    }

    public void setTotalFollowings(String totalFollowings) {
        this.totalFollowings = totalFollowings;
    }

    public String getTotalPosts() {
        return totalPosts;
    }

    public void setTotalPosts(String totalPosts) {
        this.totalPosts = totalPosts;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLikesTlId() {
        return likesTlId;
    }

    public void setLikesTlId(String likesTlId) {
        this.likesTlId = likesTlId;
    }

    public String getTlLikes() {
        return tlLikes;
    }

    public void setTlLikes(String tlLikes) {
        this.tlLikes = tlLikes;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFollowingId() {
        return followingId;
    }

    public void setFollowingId(String followingId) {
        this.followingId = followingId;
    }

    public String getFollowuserId() {
        return followuserId;
    }

    public void setFollowuserId(String followuserId) {
        this.followuserId = followuserId;
    }

    public String getFollowuserFirstname() {
        return followuserFirstname;
    }

    public void setFollowuserFirstname(String followuserFirstname) {
        this.followuserFirstname = followuserFirstname;
    }

    public String getFollowuserLastname() {
        return followuserLastname;
    }

    public void setFollowuserLastname(String followuserLastname) {
        this.followuserLastname = followuserLastname;
    }

    public String getFollowuserPicture() {
        return followuserPicture;
    }

    public void setFollowuserPicture(String followuserPicture) {
        this.followuserPicture = followuserPicture;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getReviewComments() {
        return reviewComments;
    }

    public void setReviewComments(String reviewComments) {
        this.reviewComments = reviewComments;
    }

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public String getAskQuestion() {
        return askQuestion;
    }

    public void setAskQuestion(String askQuestion) {
        this.askQuestion = askQuestion;
    }

    public String getAnsId() {
        return ansId;
    }

    public void setAnsId(String ansId) {
        this.ansId = ansId;
    }

    public String getAskAnswer() {
        return askAnswer;
    }

    public void setAskAnswer(String askAnswer) {
        this.askAnswer = askAnswer;
    }

    public String getGngEvtId() {
        return gngEvtId;
    }

    public void setGngEvtId(String gngEvtId) {
        this.gngEvtId = gngEvtId;
    }

    public String getLikeEvtId() {
        return likeEvtId;
    }

    public void setLikeEvtId(String likeEvtId) {
        this.likeEvtId = likeEvtId;
    }

    public String getFollUserid() {
        return follUserid;
    }

    public void setFollUserid(String follUserid) {
        this.follUserid = follUserid;
    }

    public String getLikeRevId() {
        return likeRevId;
    }

    public void setLikeRevId(String likeRevId) {
        this.likeRevId = likeRevId;
    }

    public String getCmmtRevId() {
        return cmmtRevId;
    }

    public void setCmmtRevId(String cmmtRevId) {
        this.cmmtRevId = cmmtRevId;
    }

    public String getAnsQuesId() {
        return ansQuesId;
    }

    public void setAnsQuesId(String ansQuesId) {
        this.ansQuesId = ansQuesId;
    }

    public String getUpAnsId() {
        return upAnsId;
    }

    public void setUpAnsId(String upAnsId) {
        this.upAnsId = upAnsId;
    }

    public Integer getWhomCount() {
        return whomCount;
    }

    public void setWhomCount(Integer whomCount) {
        this.whomCount = whomCount;
    }

    public List<Get_Timeline_Notification_Images> getImage() {
        return image;
    }

    public void setImage(List<Get_Timeline_Notification_Images> image) {
        this.image = image;
    }

    public Integer getImageCount() {
        return imageCount;
    }

    public void setImageCount(Integer imageCount) {
        this.imageCount = imageCount;
    }

}