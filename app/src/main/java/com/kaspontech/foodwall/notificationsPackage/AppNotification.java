package com.kaspontech.foodwall.notificationsPackage;

import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.kaspontech.foodwall.notificationsPackage.Notification_adapter.Multi_view_notification_adapter;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_follower_req_output;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Get_notification_count_output;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Getall_inner_notification_pojo;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Getall_notification_pojo;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Notification_output_pojo;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.View_update_notification_output;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

@RequiresApi(api = Build.VERSION_CODES.M)
public class AppNotification extends android.support.v4.app.Fragment implements View.OnClickListener, ScrollView.OnScrollChangeListener {

    /*
     * Application TAG
     * */

    private static final String TAG = "AppNotification";

    /*
     * Widgets
     * */
    ImageView notify_user_photo, back;
    ImageView img_follow_req;
    TextView txt_following_user_name;
    TextView toolbar_name;
    TextView txt_no_notification;
    TextView txt_follow_req_count;
    TextView txt_notifi;
    LinearLayout rl_notification;
    LinearLayout ll_follow_req;
    ProgressBar progressbar_notification;

    //Loader animation

    LottieAnimationView animationView;

    /*Notification Pojo's*/
    Getall_notification_pojo getallNotificationPojo;
    Getall_inner_notification_pojo getallInnerNotificationPojo;

    /*Notification Pojo's arraylist*/

    ArrayList<Getall_inner_notification_pojo> getallInnerNotificationPojoArrayList = new ArrayList<>();

    ArrayList<Getall_notification_pojo> getallNotificationPojoArrayList = new ArrayList<>();

    //Pagination Arraylist

    ArrayList<Getall_notification_pojo> paginationArrayList = new ArrayList<>();

    /*
     * String results*/
    String post_type;
    String user__id;
    String related_id;
    String note_description;

    int multi_count;

    Multi_view_notification_adapter multiViewNotificationAdapter;
    RecyclerView rv_notification;
    View view;
    Context context;
    Toolbar actionbar_notification;


    //Scroll view

    public static ScrollView scroll_notify;


    int pagecount;

    Button page_loader;
    Button btn_size_check;

    public AppNotification() {
        // Required empty public constructor
    }

    SwipeRefreshLayout swiperefresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        onSaveInstanceState(savedInstanceState);

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.activity_notification_layout, container, false);
        context = view.getContext();
        actionbar_notification = (Toolbar) view.findViewById(R.id.actionbar_notification);


        //ImageView
        notify_user_photo = (ImageView) view.findViewById(R.id.notify_user_photo);
        img_follow_req = (ImageView) view.findViewById(R.id.img_follow_req);
        //TextView
        txt_following_user_name = (TextView) view.findViewById(R.id.txt_following_user_name);
        txt_follow_req_count = (TextView) view.findViewById(R.id.txt_follow_req_count);
        txt_notifi = (TextView) view.findViewById(R.id.txt_notifi);
        txt_no_notification = (TextView) view.findViewById(R.id.txt_no_notification);
        back = (ImageView) actionbar_notification.findViewById(R.id.back);
        back.setVisibility(View.GONE);
        toolbar_name = (TextView) actionbar_notification.findViewById(R.id.toolbar_title);
        toolbar_name.setText(R.string.notifications);
        //RelativeLayout
        rl_notification = (LinearLayout) view.findViewById(R.id.rl_notification);

        //LinearLayout
        ll_follow_req = (LinearLayout) view.findViewById(R.id.ll_follow_req);
        ll_follow_req.setOnClickListener(this);
        //RecyclerView
        rv_notification = (RecyclerView) view.findViewById(R.id.rv_notification);
        //ProgressBar
        rl_notification.setOnClickListener(this);

        //progressbar_notification

        animationView = (LottieAnimationView) view.findViewById(R.id.animation_view);

        progressbar_notification = (ProgressBar) view.findViewById(R.id.progress_dialog_notification);
        progressbar_notification.setVisibility(View.GONE);

        scroll_notify = (ScrollView) view.findViewById(R.id.scroll_notify);

//        scroll_notify.setOnScrollChangeListener(this);

        page_loader = (Button) view.findViewById(R.id.page_loader);
        btn_size_check = (Button) view.findViewById(R.id.btn_size_check);
        swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        //Reset page to 1 in shared preference

        Pref_storage.setDetail(context, "Page_notify", String.valueOf(1));




        get_notifications_all();


        //Calling API

        get_notifications_count();


        get_follow_request_count();

        createNotificationViewUpdate();

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                get_notifications_all();

                get_notifications_count();

                scroll_notify.smoothScrollTo(0, 0);

                swiperefresh.setRefreshing(false);

            }
        });


        return view;
    }


    //Calling API to get notification details

    private void get_notifications_all() {

        if (Utility.isConnected(context)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");
            Call<Notification_output_pojo> call = apiService.get_notifications_all("get_notifications_all", Integer.parseInt(userId), pagecount);
//            Call<Notification_output_pojo> call = apiService.get_notifications_all("get_notifications_all", Integer.parseInt(userId), 1);
            call.enqueue(new Callback<Notification_output_pojo>() {
                @Override
                public void onResponse(Call<Notification_output_pojo> call, Response<Notification_output_pojo> response) {


                    if (response.body().getResponseMessage().equalsIgnoreCase("nodata")) {
                        progressbar_notification.setIndeterminate(false);
                        progressbar_notification.setVisibility(View.GONE);

                        // animationView.setVisibility(View.GONE);

                          txt_no_notification.setVisibility(View.VISIBLE);

                    }else{
                        txt_no_notification.setVisibility(View.GONE);
                    }

                    if (response.body().getResponseCode() == 1) {


                        getallNotificationPojoArrayList.clear();
                        //   animationView.setVisibility(View.GONE);

                        progressbar_notification.setIndeterminate(false);
                        progressbar_notification.setVisibility(View.GONE);

                        if (response.body().getData().size() > 0) {
                            pagecount = 2;

                              txt_no_notification.setVisibility(View.GONE);

//                            Log.e(TAG, "onResponse:page-->"+pagecount++ );

                            Pref_storage.setDetail(context, "Page_notify", String.valueOf(2));

                            Log.e(TAG, "onResponse:shared_page-->" + Pref_storage.getDetail(context, "Page_notify"));
                        } else {
                            // txt_no_notification.setVisibility(View.GONE);
                        }



                        for (int j = 0; j < response.body().getData().size(); j++) {

                            try {

                                post_type = response.body().getData().get(j).getPostType();
                                user__id = response.body().getData().get(j).getUserid();
                                related_id = response.body().getData().get(j).getRelatedId();
                                note_description = response.body().getData().get(j).getNoteDescription();
                                multi_count = response.body().getData().get(j).getMultiCount();

                                Log.e("App_post_type", "-->" + post_type);


                                String noteId = response.body().getData().get(j).getMulti().get(0).getNoteId();
                                String viewStatus = response.body().getData().get(j).getMulti().get(0).getViewStatus();
                                String RelatedId = response.body().getData().get(j).getMulti().get(0).getRelatedId();
                                String posttype = response.body().getData().get(j).getMulti().get(0).getPostType();
                                String CreatedOn = response.body().getData().get(j).getMulti().get(0).getCreatedOn();
                                String UserId = response.body().getData().get(j).getMulti().get(0).getUserId();
                                String oauth_provider = response.body().getData().get(j).getMulti().get(0).getOauthProvider();

                                String Email = response.body().getData().get(j).getMulti().get(0).getEmail();
                                String FirstName = response.body().getData().get(j).getMulti().get(0).getFirstName();
                                String LastName = response.body().getData().get(j).getMulti().get(0).getLastName();
                                String picture = response.body().getData().get(j).getMulti().get(0).getPicture();
                                String BioDescription = response.body().getData().get(j).getMulti().get(0).getBioDescription();

                                //Review data's
                                String revrat_id = response.body().getData().get(j).getMulti().get(0).getRevratId();
                                String hotel_review = response.body().getData().get(j).getMulti().get(0).getHotelReview();
                                String category_type = response.body().getData().get(j).getMulti().get(0).getHotelReview();
                                String cmmt_htl_id = response.body().getData().get(j).getMulti().get(0).getCmmtHtlId();
                                String htl_rev_comments = response.body().getData().get(j).getMulti().get(0).getHtlRevComments();
                                //Timeline data's

                                String timeline_id = response.body().getData().get(j).getMulti().get(0).getTimelineId();
                                String timeline_description = response.body().getData().get(j).getMulti().get(0).getTimelineDescription();
                                String cmmt_tl_id = response.body().getData().get(j).getMulti().get(0).getCmmtTlId();
                                String tl_comments = response.body().getData().get(j).getMulti().get(0).getTlComments();
                                //Events data's
                                String event_id = response.body().getData().get(j).getMulti().get(0).getEventId();
                                String event_name = response.body().getData().get(j).getMulti().get(0).getEventName();
                                String cmmt_evt_id = response.body().getData().get(j).getMulti().get(0).getCmmtEvtId();
                                String evt_comments = response.body().getData().get(j).getMulti().get(0).getEvtComments();


                                getallInnerNotificationPojo = new Getall_inner_notification_pojo(noteId,
                                        viewStatus,
                                        RelatedId,
                                        posttype,
                                        CreatedOn,
                                        UserId,
                                        oauth_provider,
                                        "",
                                        FirstName,
                                        LastName,
                                        Email,
                                        "",
                                        "",
                                        "",
                                        "",
                                        BioDescription,
                                        picture,
                                        revrat_id,
                                        hotel_review,
                                        category_type,
                                        cmmt_htl_id,
                                        htl_rev_comments,
                                        timeline_id,
                                        timeline_description,
                                        cmmt_tl_id,
                                        tl_comments,
                                        event_id,
                                        event_name,
                                        cmmt_evt_id,
                                        evt_comments);

                                getallInnerNotificationPojoArrayList.add(getallInnerNotificationPojo);

                                getallNotificationPojo = new Getall_notification_pojo(post_type, note_description, user__id, related_id, getallInnerNotificationPojoArrayList, multi_count);
                                getallNotificationPojoArrayList.add(getallNotificationPojo);
                                multiViewNotificationAdapter.notifyDataSetChanged();


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        multiViewNotificationAdapter = new Multi_view_notification_adapter(context, getallNotificationPojoArrayList);
                        rv_notification.setLayoutManager(new LinearLayoutManager(getActivity()));
                        rv_notification.setItemAnimator(new DefaultItemAnimator());
                        rv_notification.setAdapter(multiViewNotificationAdapter);
                        rv_notification.setNestedScrollingEnabled(false);
                        multiViewNotificationAdapter.notifyDataSetChanged();

                    }

                    else if (response.body().getResponseMessage().equalsIgnoreCase("nodata")) {
                        progressbar_notification.setIndeterminate(false);
                        progressbar_notification.setVisibility(View.GONE);



                    }
                }

                @Override
                public void onFailure(Call<Notification_output_pojo> call, Throwable t) {
                    progressbar_notification.setIndeterminate(false);
                    progressbar_notification.setVisibility(View.GONE);
                    // animationView.setVisibility(View.GONE);

                    if(getallNotificationPojoArrayList.isEmpty())
                    {
                        txt_no_notification.setVisibility(View.VISIBLE);

                    }else
                    {
                        txt_no_notification.setVisibility(View.GONE);

                    }

                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });

        } else {

            Toast.makeText(context, "No Internet connection.", Toast.LENGTH_SHORT).show();
        }

    }


    /*Pagination API call*/
    private void get_notifications_all(int pagecount) {


        page_loader.setVisibility(View.VISIBLE);

        if (Utility.isConnected(context)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(context, "userId");
            Call<Notification_output_pojo> call = apiService.get_notifications_all("get_notifications_all", Integer.parseInt(userId), pagecount);
            call.enqueue(new Callback<Notification_output_pojo>() {
                @Override
                public void onResponse(Call<Notification_output_pojo> call, Response<Notification_output_pojo> response) {

                    if (response.body().getResponseCode() == 1) {


                         paginationArrayList.clear();


                        progressbar_notification.setIndeterminate(false);
                        progressbar_notification.setVisibility(View.GONE);


                        if (Pref_storage.getDetail(context, "Page_notify").isEmpty() || Pref_storage.getDetail(context, "Page_notify") == null) {
//                            Pref_storage.getDetail(context,"Page_count")

                        } else {
                            int page_incre = Integer.parseInt(Pref_storage.getDetail(context, "Page_notify"));

                            Log.e(TAG, "onResponsePagination: page_incre-->" + page_incre);


                            int page_incre_shared = page_incre + 1;

                            Pref_storage.setDetail(context, "Page_notify", String.valueOf(page_incre_shared));

                            Log.e(TAG, "onResponsePagination: page_count_incre-->" + Integer.parseInt(Pref_storage.getDetail(context, "Page_notify")));

                        }



                        for (int j = 0; j < response.body().getData().size(); j++) {
                            try {


                                post_type = response.body().getData().get(j).getPostType();
                                user__id = response.body().getData().get(j).getUserid();
                                related_id = response.body().getData().get(j).getRelatedId();
                                note_description = response.body().getData().get(j).getNoteDescription();
                                multi_count = response.body().getData().get(j).getMultiCount();

                                Log.e("App_post_type", "-->" + post_type);


                                String noteId = response.body().getData().get(j).getMulti().get(0).getNoteId();
                                String viewStatus = response.body().getData().get(j).getMulti().get(0).getViewStatus();
                                String RelatedId = response.body().getData().get(j).getMulti().get(0).getRelatedId();
                                String posttype = response.body().getData().get(j).getMulti().get(0).getPostType();
                                String CreatedOn = response.body().getData().get(j).getMulti().get(0).getCreatedOn();
                                String UserId = response.body().getData().get(j).getMulti().get(0).getUserId();
                                String oauth_provider = response.body().getData().get(j).getMulti().get(0).getOauthProvider();

                                String Email = response.body().getData().get(j).getMulti().get(0).getEmail();
                                String FirstName = response.body().getData().get(j).getMulti().get(0).getFirstName();
                                String LastName = response.body().getData().get(j).getMulti().get(0).getLastName();
                                String picture = response.body().getData().get(j).getMulti().get(0).getPicture();
                                String BioDescription = response.body().getData().get(j).getMulti().get(0).getBioDescription();

                                //Review data's
                                String revrat_id = response.body().getData().get(j).getMulti().get(0).getRevratId();
                                String hotel_review = response.body().getData().get(j).getMulti().get(0).getHotelReview();
                                String category_type = response.body().getData().get(j).getMulti().get(0).getHotelReview();
                                String cmmt_htl_id = response.body().getData().get(j).getMulti().get(0).getCmmtHtlId();
                                String htl_rev_comments = response.body().getData().get(j).getMulti().get(0).getHtlRevComments();
                                //Timeline data's

                                String timeline_id = response.body().getData().get(j).getMulti().get(0).getTimelineId();
                                String timeline_description = response.body().getData().get(j).getMulti().get(0).getTimelineDescription();
                                String cmmt_tl_id = response.body().getData().get(j).getMulti().get(0).getCmmtTlId();
                                String tl_comments = response.body().getData().get(j).getMulti().get(0).getTlComments();
                                //Events data's
                                String event_id = response.body().getData().get(j).getMulti().get(0).getEventId();
                                String event_name = response.body().getData().get(j).getMulti().get(0).getEventName();
                                String cmmt_evt_id = response.body().getData().get(j).getMulti().get(0).getCmmtEvtId();
                                String evt_comments = response.body().getData().get(j).getMulti().get(0).getEvtComments();


                                getallInnerNotificationPojo = new Getall_inner_notification_pojo(noteId, viewStatus,
                                        RelatedId, posttype, CreatedOn, UserId, oauth_provider,
                                        "", FirstName, LastName,
                                        Email, "", "", "", "", BioDescription,
                                        picture, revrat_id, hotel_review, category_type, cmmt_htl_id, htl_rev_comments,
                                        timeline_id, timeline_description, cmmt_tl_id, tl_comments, event_id,
                                        event_name, cmmt_evt_id, evt_comments);

                                getallInnerNotificationPojoArrayList.add(getallInnerNotificationPojo);


                                /*   }*/

                                getallNotificationPojo = new Getall_notification_pojo(post_type, note_description, user__id, related_id, getallInnerNotificationPojoArrayList, multi_count);
                                paginationArrayList.add(getallNotificationPojo);

                                getallNotificationPojoArrayList.addAll(paginationArrayList);

                                multiViewNotificationAdapter.notifyDataSetChanged();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        multiViewNotificationAdapter.notifyDataSetChanged();

                        page_loader.setVisibility(View.GONE);


                    } else if (response.body().getResponseMessage().equalsIgnoreCase("nodata")) {
                        progressbar_notification.setIndeterminate(false);
                        progressbar_notification.setVisibility(View.GONE);

                        //   animationView.setVisibility(View.GONE);

                           txt_no_notification.setVisibility(View.VISIBLE);

                    }
                }

                @Override
                public void onFailure(Call<Notification_output_pojo> call, Throwable t) {
                    progressbar_notification.setIndeterminate(false);
                    progressbar_notification.setVisibility(View.GONE);


                    page_loader.setVisibility(View.GONE);

                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });

        } else {

            Toast.makeText(context, "No Internet connection.", Toast.LENGTH_SHORT).show();
        }

    }


    //Calling view update API

    private void createNotificationViewUpdate() {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String userId = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<View_update_notification_output> call = apiService.create_notification_view_update("create_notification_view_update", Integer.parseInt(userId));
                call.enqueue(new Callback<View_update_notification_output>() {
                    @Override
                    public void onResponse(@NonNull Call<View_update_notification_output> call, @NonNull Response<View_update_notification_output> response) {
                        if (response.isSuccessful()) {
                            String data = response.body().getData();

                            toolbar_name.setText(R.string.notifications);
                        }

                    }

                    @Override
                    public void onFailure(Call<View_update_notification_output> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Toast.makeText(context, "No Internet connection.", Toast.LENGTH_SHORT).show();
        }


    }

    //Calling get notification count API

    private void get_notifications_count() {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String userId = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_notification_count_output> call = apiService.get_notifications_count("get_notifications_count",
                        Integer.parseInt(userId));
                call.enqueue(new Callback<Get_notification_count_output>() {
                    @Override
                    public void onResponse(Call<Get_notification_count_output> call, Response<Get_notification_count_output> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                final String count = response.body().getData().get(j).getTotalCount();


                                if (Integer.parseInt(count) > 1) {
                                    toolbar_name.setText("Notifications(".concat(count).concat(")"));
                                } else {
                                    toolbar_name.setText(R.string.notifications);
                                }


                                /*Saving the notification count in shared preference*/

                                Pref_storage.setDetail(context, "NotificationCount", count);


                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<Get_notification_count_output> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Toast.makeText(context, "No Internet connection.", Toast.LENGTH_SHORT).show();
        }


    }


    //Calling Get follow request count API
    private void get_follow_request_count() {


        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                String userId = Pref_storage.getDetail(getApplicationContext(), "userId");
                Call<Get_follower_req_output> call = apiService.get_follower_request("get_follower_request", Integer.parseInt(userId));
                call.enqueue(new Callback<Get_follower_req_output>() {
                    @Override
                    public void onResponse(Call<Get_follower_req_output> call, Response<Get_follower_req_output> response) {

                        if (response.body().getResponseCode() == 1) {

                            if (response.body().getResponseMessage().equalsIgnoreCase("nodata")) {

                                ll_follow_req.setVisibility(View.GONE);
                                txt_notifi.setVisibility(View.GONE);


                            } else {


                                for (int j = 0; j < response.body().getData().size(); j++) {

                                    String count = String.valueOf(response.body().getData().size());

                                    if (count.equalsIgnoreCase("0") || count.equalsIgnoreCase(null)) {

                                        ll_follow_req.setVisibility(View.GONE);
                                        txt_notifi.setVisibility(View.GONE);

                                    } else {

                                        ll_follow_req.setVisibility(View.VISIBLE);
                                        txt_notifi.setVisibility(View.VISIBLE);
                                        txt_follow_req_count.setText("Follow requests (".concat(count).concat(") "));
                                    }


                                }
                            }


                        }

                    }

                    @Override
                    public void onFailure(Call<Get_follower_req_output> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());


                        ll_follow_req.setVisibility(View.GONE);
                        txt_notifi.setVisibility(View.GONE);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Toast.makeText(context, "No Internet connection.", Toast.LENGTH_SHORT).show();

        }


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ll_follow_req:
                startActivity(new Intent(context, Follower_request_activity.class));
                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        View view = (View) scroll_notify.getChildAt(scroll_notify.getChildCount() - 1);
        int diff = (view.getBottom() - (scroll_notify.getHeight() + scroll_notify.getScrollY()));

        // if diff is zero, then the bottom has been reached

        Log.e(TAG, "onScrollChange: PageCount Shared--> " + Pref_storage.getDetail(context, "Page_notify"));

        if (diff == 0) {


            //scroll view is at bottom



            if (pagecount == 1) {

            } else {


                if (Pref_storage.getDetail(context, "Page_notify").isEmpty() || Pref_storage.getDetail(context, "Page_notify") == null) {

                } else {
                    pagecount = Integer.parseInt(Pref_storage.getDetail(context, "Page_notify"));

                    Log.e(TAG, "onScrollChange: pagecount " + pagecount);

                    get_notifications_all(pagecount);

                }


            }


            // do stuff
        }

        Utility.hideKeyboard(v);
    }


}