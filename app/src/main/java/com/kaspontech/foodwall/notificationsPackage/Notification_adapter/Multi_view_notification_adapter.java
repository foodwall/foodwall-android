package com.kaspontech.foodwall.notificationsPackage.Notification_adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.kaspontech.foodwall.eventspackage.ViewEvent;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Getall_inner_notification_pojo;
import com.kaspontech.foodwall.notificationsPackage.Notification_pojo.Getall_notification_pojo;
import com.kaspontech.foodwall.profilePackage.Image_self_activity;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.profilePackage.ReviewSinglePage;
import com.kaspontech.foodwall.profilePackage.User_profile_Activity;
import com.kaspontech.foodwall.questionspackage.ViewQuestionAnswer;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;
//

/**
 * Created by Vishnu on 08-06-2018.
 */

public class Multi_view_notification_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RecyclerView.OnItemTouchListener, View.OnTouchListener {

    private Context context;

    private List<Getall_notification_pojo> getallNotificationPojoArrayList = new ArrayList<>();
     private Getall_notification_pojo getallNotificationPojo;


    private Pref_storage pref_storage;


    public Multi_view_notification_adapter(Context c, List<Getall_notification_pojo> gettinglist/*,ArrayList<Getall_inner_notification_pojo> getallInnerNotificationPojoArrayList*/) {
        this.context = c;
        this.getallNotificationPojoArrayList = gettinglist;
//        this.getallInnerNotificationPojoArrayList = getallInnerNotificationPojoArrayList;
    }


    @Override
    public int getItemViewType(int position) {

//        Notification Post Type:
//
//        1- question request
//
//        2- answered
//
//        3- question follower
//
//        4 - user following
//
//        5- timeline likes
//
//        6- timeline comments
//
//        7- events likes
//
//        8- events going
//
//        9- events share
//
//        10- events comments
//
//        11- discussion create
//
//        12- discussion likes
//
//        13- discussion comments
//
//        14- answer upvote
//
//        15- answer comments
//
//        16- review likes
//
//        17- review share
//
//        18- review comments

//        19 -question comments


        Log.e("post_type", "-->" + getallNotificationPojoArrayList.get(position).getPostType());

        // Case 0 -  question request
        if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 1) {

            return 1;

            // Case 2 - question follower
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 2) {

            return 2;

            // Case 3 -  user following
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 3) {

            return 3;
            // Case 4 - - user following
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 4) {

            return 4;

        } // Case 5 -timeline likes
        else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 5) {

            return 5;
            // Case 6 -timeline comments
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 6) {

            return 6;
            // Case 7 -events interested
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 7) {

            return 7;

        }  // Case 8 -events going
        else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 8) {

            return 8;

        } // Case 9 -events share
        else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 9) {

            return 9;
            // Case 10 -events comments
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 10) {

            return 10;
            // Case 11  discussion create
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 11) {

            return 11;
            // Case 12 discussion likes
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 12) {

            return 12;
            // Case 13   discussion comments
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 13) {

            return 13;
            // Case 14  answer upvote
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 14) {

            return 14;
            // Case 15 answer comments
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 15) {

            return 15;
            // Case 16 review likes
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 16) {

            return 16;
            // Case 17 review share
        } else if (Integer.parseInt(getallNotificationPojoArrayList.get(position).getPostType()) == 17) {

            return 17;

        }

        // Case 18 - review comments
        else {

            return 18;

        }

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {

//        Notification Post Type:
//
//        1- question request
//
//        2- answered
//
//        3- question follower
//
//        4 - user following
//
//        5- timeline likes
//
//        6- timeline comments
//
//        7- events likes
//
//        8- events going
//
//        9- events share
//
//        10- events comments
//
//        11- discussion create
//
//        12- discussion likes
//
//        13- discussion comments
//
//        14- answer upvote
//
//        15- answer comments
//
//        16- review likes
//
//        17- review share
//
//        18- review comments
//
//        19- question comments


            // Case 0 - question request
            case 1:

                View question_request = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new Multi_view_holder_adapter(question_request);

            case 2:

                View answered = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new QuestionAnsweredAdapter(answered);


            case 3:
                View question_follower = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new QuestionFollowAdapter(question_follower);

            case 4:
                View user_following = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new UserFollowingAdapter(user_following);

            case 5:
                View timeline_likes = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new Notifi_timelinelikesAdapter(timeline_likes);
            case 6:

                View timeline_comments = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new Notifi_timelinecommentsAdapter(timeline_comments);
            case 7:

                View events_likes = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiEventLikeAdapter(events_likes);
            case 8:

                View events_going = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiEventGoingAdapter(events_going);
            case 9:

                View events_share = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiEventShareAdapter(events_share);
            case 10:

                View events_comments = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiEventCommentAdapter(events_comments);
            case 11:

                View discussion_create = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiDiscussionCreateAdapter(discussion_create);
            case 12:

                View discussion_likes = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiDiscussionLikeAdapter(discussion_likes);
            case 13:

                View discussion_comments = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiDiscussionCommentAdapter(discussion_comments);
            case 14:

                View answer_upvote = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiAnswerUpvoteAdapter(answer_upvote);
            case 15:

                View answer_comments = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiAnswerCommentsAdapter(answer_comments);

            case 16:

                View review_likes = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiReviewsLikesAdapter(review_likes);

            case 17:

                View review_share = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiReviewsShareAdapter(review_share);

            case 18:

                View review_comments = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiReviewsCommentAdapter(review_comments);
            case 19:

                View question_comments = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_layout, parent, false);
                return new NotiReviewsCommentAdapter(question_comments);

        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        getallNotificationPojo = getallNotificationPojoArrayList.get(position);

//        getallInnerNotificationPojo = getallInnerNotificationPojoArrayList.get(position);

        switch (holder.getItemViewType()) {

            case 1:

                final Multi_view_holder_adapter multiViewHolderAdapter = (Multi_view_holder_adapter) holder;

                int req_count = getallNotificationPojoArrayList.get(position).getMultiCount();


                Log.e("req_count", "" + req_count);

                if (req_count == 1) {

                    //reviewer_name
                    String reviewer_name = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter.txt_following_user_name.setText(reviewer_name);
                    multiViewHolderAdapter.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter.txt_following_text.setText(R.string.question_request);

                } else if (req_count == 2) {
                    //reviewer_name
                    String reviewer_name = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter.txt_following_user_name.setText(reviewer_name);
                    multiViewHolderAdapter.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter.txt_following_text.setText(R.string.question_request);
                } else if (req_count > 2) {
                    int follow_count_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(follow_count_dec)).concat(" others"));
                    multiViewHolderAdapter.txt_following_user_name.setText(reviewer_name);
                    multiViewHolderAdapter.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter.txt_following_text.setText(R.string.question_request);
                }


                //Reviewer Image

                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter.notify_user_photo, context);


// Timestamp for notification
                String createdon = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon, multiViewHolderAdapter.txt_follow_ago);





                multiViewHolderAdapter.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewQuestionAnswer.class);
                        intent.putExtra("quesId", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        intent.putExtra("position", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        context.startActivity(intent);

                    }
                });

                break;

            case 2:
                final QuestionAnsweredAdapter multiViewHolderAdapter2 = (QuestionAnsweredAdapter) holder;


                int ans_count = getallNotificationPojoArrayList.get(position).getMultiCount();


                Log.e("ans_count", "" + ans_count);

                if (ans_count == 1) {

                    //reviewer_name
                    String reviewer_name2 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter2.txt_following_user_name.setText(reviewer_name2);
                    multiViewHolderAdapter2.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter2.txt_following_text.setText(R.string.answered);
                } else if (ans_count == 2) {
                    //reviewer_name
                    String reviewer_name2 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter2.txt_following_user_name.setText(reviewer_name2);
                    multiViewHolderAdapter2.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter2.txt_following_text.setText(R.string.answered);
                } else if (ans_count > 2) {
                    int follow_count_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name2 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(follow_count_dec)).concat(" others"));
                    multiViewHolderAdapter2.txt_following_user_name.setText(reviewer_name2);
                    multiViewHolderAdapter2.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter2.txt_following_text.setText(R.string.answered);
                }


                //Reviewer Image
                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter2.notify_user_photo, context);


// Timestamp for notification
                String createdon2 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon2, multiViewHolderAdapter2.txt_follow_ago);



                multiViewHolderAdapter2.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewQuestionAnswer.class);
                        intent.putExtra("quesId", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        intent.putExtra("position", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        context.startActivity(intent);

                    }
                });

                break;
            case 3:

                final QuestionFollowAdapter multiViewHolderAdapter3 = (QuestionFollowAdapter) holder;


                int follow_que_count = getallNotificationPojoArrayList.get(position).getMultiCount();


                Log.e("follow_que_count", "" + follow_que_count);

                if (follow_que_count == 1) {

                    //reviewer_name
                    String reviewer_name3 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter3.txt_following_user_name.setText(reviewer_name3);
                    multiViewHolderAdapter3.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter3.txt_following_text.setText(R.string.following_this_question);


                } else if (follow_que_count == 2) {
                    //reviewer_name
                    String reviewer_name3 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter3.txt_following_user_name.setText(reviewer_name3);
                    multiViewHolderAdapter3.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter3.txt_following_text.setText(R.string.following_this_question);
                } else if (follow_que_count > 2) {
                    int follow_count_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name3 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(follow_count_dec)).concat(" others"));
                    multiViewHolderAdapter3.txt_following_user_name.setText(reviewer_name3);
                    multiViewHolderAdapter3.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter3.txt_following_text.setText(R.string.following_this_question);
                }


                //Reviewer Image

                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter3.notify_user_photo, context);

// Timestamp for notification
                String createdon3 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon3, multiViewHolderAdapter3.txt_follow_ago);



                multiViewHolderAdapter3.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewQuestionAnswer.class);
                        intent.putExtra("quesId", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        intent.putExtra("position", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        context.startActivity(intent);

                    }
                });


                break;


            case 4:
                final UserFollowingAdapter multiViewHolderAdapter4 = (UserFollowingAdapter) holder;


                int follow_count = getallNotificationPojoArrayList.get(position).getMultiCount();


                Log.e("follow_count", "" + follow_count);

                if (follow_count == 1) {

                    //reviewer_name
                    String reviewerFollow1 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter4.txt_following_user_name.setText(reviewerFollow1);
                    multiViewHolderAdapter4.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter4.txt_following_text.setText(R.string.started_following_you);


                } else if (follow_count == 2) {
                    //reviewer_name

                    try {

                        Log.e("App adapter", "onBindViewHolder: fname0" + (getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName()));
                        Log.e("App adapter", "onBindViewHolder: lname0" + (getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName()));

                        Log.e("App adapter", "onBindViewHolder: fname" + (getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName()));
                        Log.e("App adapter", "onBindViewHolder: fname" + (getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName()));

                        String reviewerFollow2 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                                concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                        concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                                concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));

                        multiViewHolderAdapter4.txt_following_user_name.setText(reviewerFollow2);
                        multiViewHolderAdapter4.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                        multiViewHolderAdapter4.txt_following_text.setText(R.string.started_following_you);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (follow_count > 2) {
                    int follow_count_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewerFollow3 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(follow_count_dec)).concat(" others"));
                    multiViewHolderAdapter4.txt_following_user_name.setText(reviewerFollow3);
                    multiViewHolderAdapter4.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter4.txt_following_text.setText(R.string.started_following_you);
                }


                //Reviewer Image
                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter4.notify_user_photo, context);



// Timestamp for notification
                String createdon4 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon4, multiViewHolderAdapter4.txt_follow_ago);




                multiViewHolderAdapter4.notify_user_photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = getallNotificationPojoArrayList.get(position).getMulti().get(position).getUserId();


                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }
                    }
                });

                multiViewHolderAdapter4.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = getallNotificationPojoArrayList.get(position).getMulti().get(position).getUserId();


                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }


                    }
                });


                multiViewHolderAdapter4.txt_following_user_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = getallNotificationPojoArrayList.get(position).getMulti().get(position).getUserId();


                        if (user_id.equalsIgnoreCase(Pref_storage.getDetail(context, "userId"))) {
                            context.startActivity(new Intent(context, Profile.class));
                        } else {

                            Intent intent = new Intent(context, User_profile_Activity.class);
                            intent.putExtra("created_by", user_id);
                            context.startActivity(intent);

                        }

                    }
                });


                break;

            case 5:
                final Notifi_timelinelikesAdapter multiViewHolderAdapter5 = (Notifi_timelinelikesAdapter) holder;

                //Liked user_name

                int likes_size = getallNotificationPojoArrayList.get(position).getMultiCount();

                Log.e("likes_size", "" + likes_size);

                if (likes_size == 1) {

                    String reviewer_name5 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter5.txt_following_user_name.setText(reviewer_name5);
                    multiViewHolderAdapter5.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter5.txt_following_text.setText(R.string.timeline_liked);
                } else if (likes_size == 2) {

                    String reviewer_name5 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter5.txt_following_user_name.setText(reviewer_name5);
                    multiViewHolderAdapter5.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter5.txt_following_text.setText(R.string.timeline_liked);
                } else if (likes_size > 2) {

                    int likes_others_size = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name5 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(likes_others_size).concat(" others")));
                    multiViewHolderAdapter5.txt_following_user_name.setText(reviewer_name5);
                    multiViewHolderAdapter5.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter5.txt_following_text.setText(R.string.timeline_liked);
                }


                //Liked Image

                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter5.notify_user_photo, context);


// Timestamp for notification
                String createdon5 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon5, multiViewHolderAdapter5.txt_follow_ago);

                final String timelineid = getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId();
                final String profileimage = getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture();



                //Likes redirection

                multiViewHolderAdapter5.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, Image_self_activity.class);
                        intent.putExtra("Clickedfrom_profile", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        intent.putExtra("Clickedfrom_profileuserid", getallNotificationPojoArrayList.get(position).getMulti().get(position).getUserId());
                        context.startActivity(intent);


                    }
                });


                break;

            case 6:
                final Notifi_timelinecommentsAdapter multiViewHolderAdapter6 = (Notifi_timelinecommentsAdapter) holder;

                //reviewer_name
                int size = getallNotificationPojoArrayList.get(position).getMultiCount();

                Log.e("size_adapter", "size_adapter" + size);

                if (size == 1) {

                    String reviewer_name6 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter6.txt_following_user_name.setText(reviewer_name6);
                    multiViewHolderAdapter6.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter6.txt_following_text.setText(R.string.timeline_commented);

                } else if (size == 2) {

                    String reviewer_name6 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName())));
                    multiViewHolderAdapter6.txt_following_user_name.setText(reviewer_name6);
                    multiViewHolderAdapter6.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter6.txt_following_text.setText(R.string.timeline_commented);
                } else if (size > 2) {

                    int others_size = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name6 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(others_size).concat(" others ")));
                    multiViewHolderAdapter6.txt_following_user_name.setText(reviewer_name6);
                    multiViewHolderAdapter6.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter6.txt_following_text.setText(R.string.timeline_commented);
                }


                //Reviewer Image
                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter6.notify_user_photo, context);



// Timestamp for notification
                String createdon6 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();
                Utility.setTimeStamp(createdon6, multiViewHolderAdapter6.txt_follow_ago);





                multiViewHolderAdapter6.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, Image_self_activity.class);
                        intent.putExtra("Clickedfrom_profile", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        intent.putExtra("Clickedfrom_profileuserid", getallNotificationPojoArrayList.get(position).getMulti().get(position).getUserId());
                        context.startActivity(intent);



                    }
                });


                break;

            case 7:
                final NotiEventLikeAdapter multiViewHolderAdapter7 = (NotiEventLikeAdapter) holder;


                int even_size = getallNotificationPojoArrayList.get(position).getMultiCount();

                Log.e("even_size", "" + even_size);

                if (even_size == 1) {

                    //reviewer_name
                    String reviewer_name7 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter7.txt_following_user_name.setText(reviewer_name7);
                    multiViewHolderAdapter7.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter7.txt_following_text.setText(R.string.event_likes);

                } else if (even_size == 2) {

                    String reviewer_name7 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter7.txt_following_user_name.setText(reviewer_name7);
                    multiViewHolderAdapter7.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter7.txt_following_text.setText(R.string.event_likes);
                } else if (even_size > 2) {

                    int even_size_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name5 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(even_size_dec).concat(" others")));
                    multiViewHolderAdapter7.txt_following_user_name.setText(reviewer_name5);
                    multiViewHolderAdapter7.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter7.txt_following_text.setText(R.string.event_likes);
                }


                //Reviewer Image
                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter7.notify_user_photo, context);


// Timestamp for notification
                String createdon7 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon7, multiViewHolderAdapter7.txt_follow_ago);

                multiViewHolderAdapter7.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ViewEvent.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("eventId", Integer.parseInt(getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId()));
                        intent.putExtras(bundle);
                        context.startActivity(intent);

                    }
                });


                break;


            case 8:
                final NotiEventGoingAdapter multiViewHolderAdapter8 = (NotiEventGoingAdapter) holder;

                int evt_gng_size = getallNotificationPojoArrayList.get(position).getMultiCount();
                Log.e("evt_gng_size", "" + evt_gng_size);

                if (evt_gng_size == 1) {

                    //reviewer_name
                    String reviewer_name8 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter8.txt_following_user_name.setText(reviewer_name8);
                    multiViewHolderAdapter8.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter8.txt_following_text.setText(R.string.event_going);

                } else if (evt_gng_size == 2) {
                    //reviewer_name
                    String reviewer_name8 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter8.txt_following_user_name.setText(reviewer_name8);
                    multiViewHolderAdapter8.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter8.txt_following_text.setText(R.string.event_going);
                } else if (evt_gng_size > 2) {
                    int evt_gng_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name8 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(evt_gng_dec)).concat(" others"));
                    multiViewHolderAdapter8.txt_following_user_name.setText(reviewer_name8);
                    multiViewHolderAdapter8.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter8.txt_following_text.setText(R.string.event_going);
                }

                //Reviewer Image
                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter8.notify_user_photo, context);


// Timestamp for notification
                String createdon8 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon8, multiViewHolderAdapter8.txt_follow_ago);




                multiViewHolderAdapter8.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewEvent.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("eventId", Integer.parseInt(getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId()));
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                });


                break;


            case 9:
                final NotiEventShareAdapter multiViewHolderAdapter9 = (NotiEventShareAdapter) holder;
                int evt_shr_size = getallNotificationPojoArrayList.get(position).getMultiCount();
                Log.e("evt_shr_size", "" + evt_shr_size);

                if (evt_shr_size == 1) {


                    //reviewer_name
                    String reviewer_name9 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter9.txt_following_user_name.setText(reviewer_name9);
                    multiViewHolderAdapter9.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter9.txt_following_text.setText(R.string.event_share);


                } else if (evt_shr_size == 2) {
                    //reviewer_name
                    String reviewer_name9 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter9.txt_following_user_name.setText(reviewer_name9);
                    multiViewHolderAdapter9.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter9.txt_following_text.setText(R.string.event_share);
                } else if (evt_shr_size > 2) {
                    int evt_cmnt_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name9 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(evt_cmnt_dec)).concat(" others"));
                    multiViewHolderAdapter9.txt_following_user_name.setText(reviewer_name9);
                    multiViewHolderAdapter9.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter9.txt_following_text.setText(R.string.event_share);
                }


                //Reviewer Image

                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter9.notify_user_photo, context);


// Timestamp for notification
                String createdon9 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon9, multiViewHolderAdapter9.txt_follow_ago);


                multiViewHolderAdapter9.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ViewEvent.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("eventId", Integer.parseInt(getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId()));
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                });

                break;

            case 10:
                final NotiEventCommentAdapter multiViewHolderAdapter10 = (NotiEventCommentAdapter) holder;

                int evt_cmnt_size = getallNotificationPojoArrayList.get(position).getMultiCount();
                Log.e("evt_cmnt_size", "" + evt_cmnt_size);

                if (evt_cmnt_size == 1) {

                    //reviewer_name
                    String reviewer_name10 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter10.txt_following_user_name.setText(reviewer_name10);
                    multiViewHolderAdapter10.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter10.txt_following_text.setText(R.string.event_comments);


                } else if (evt_cmnt_size == 2) {
                    //reviewer_name
                    String reviewer_name10 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter10.txt_following_user_name.setText(reviewer_name10);
                    multiViewHolderAdapter10.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter10.txt_following_text.setText(R.string.event_comments);
                } else if (evt_cmnt_size > 2) {
                    int evt_cmnt_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name10 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(evt_cmnt_dec)).concat(" others"));
                    multiViewHolderAdapter10.txt_following_user_name.setText(reviewer_name10);
                    multiViewHolderAdapter10.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter10.txt_following_text.setText(R.string.event_comments);
                }


                //Reviewer Image
                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter10.notify_user_photo, context);


// Timestamp for notification
                String createdon10 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon10, multiViewHolderAdapter10.txt_follow_ago);


                multiViewHolderAdapter10.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ViewEvent.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("eventId", Integer.parseInt(getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId()));
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                });
                break;

            case 11:
                final NotiDiscussionCreateAdapter multiViewHolderAdapter11 = (NotiDiscussionCreateAdapter) holder;


                int dis_start_size = getallNotificationPojoArrayList.get(position).getMultiCount();
                Log.e("dis_start_size", "" + dis_start_size);

                if (dis_start_size == 1) {


                    //reviewer_name
                    String reviewer_name11 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter11.txt_following_user_name.setText(reviewer_name11);
                    multiViewHolderAdapter11.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter11.txt_following_text.setText(R.string.discussion_create);


                } else if (dis_start_size == 2) {
                    //reviewer_name
                    String reviewer_name11 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter11.txt_following_user_name.setText(reviewer_name11);
                    multiViewHolderAdapter11.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter11.txt_following_text.setText(R.string.discussion_create);
                } else if (dis_start_size > 2) {
                    int dis_start_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name11 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(dis_start_dec)).concat(" others"));
                    multiViewHolderAdapter11.txt_following_user_name.setText(reviewer_name11);
                    multiViewHolderAdapter11.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter11.txt_following_text.setText(R.string.discussion_create);
                }


                //Reviewer Image
                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter11.notify_user_photo, context);



// Timestamp for notification
                String createdon11 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon11, multiViewHolderAdapter11.txt_follow_ago);


                break;

            case 12:
                final NotiDiscussionLikeAdapter multiViewHolderAdapter12 = (NotiDiscussionLikeAdapter) holder;


                int dis_like_size = getallNotificationPojoArrayList.get(position).getMultiCount();
                Log.e("dis_like_size", "" + dis_like_size);

                if (dis_like_size == 1) {

                    //reviewer_name
                    String reviewer_name12 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter12.txt_following_user_name.setText(reviewer_name12);
                    multiViewHolderAdapter12.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter12.txt_following_text.setText(R.string.discussion_likes);

                } else if (dis_like_size == 2) {
                    //reviewer_name
                    String reviewer_name12 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter12.txt_following_user_name.setText(reviewer_name12);
                    multiViewHolderAdapter12.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter12.txt_following_text.setText(R.string.discussion_likes);
                } else if (dis_like_size > 2) {
                    int dis_like_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name12 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(dis_like_dec)).concat(" others"));
                    multiViewHolderAdapter12.txt_following_user_name.setText(reviewer_name12);
                    multiViewHolderAdapter12.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter12.txt_following_text.setText(R.string.discussion_likes);
                }


                //Reviewer Image

                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter12.notify_user_photo, context);



// Timestamp for notification
                String createdon12 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();


                Utility.setTimeStamp(createdon12, multiViewHolderAdapter12.txt_follow_ago);



                break;
            case 13:
                final NotiDiscussionCommentAdapter multiViewHolderAdapter13 = (NotiDiscussionCommentAdapter) holder;


                int dis_cmt_size = getallNotificationPojoArrayList.get(position).getMultiCount();
                Log.e("dis_cmt_size", "" + dis_cmt_size);

                if (dis_cmt_size == 1) {

                    //reviewer_name
                    String reviewer_name13 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter13.txt_following_user_name.setText(reviewer_name13);
                    multiViewHolderAdapter13.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter13.txt_following_text.setText(R.string.discussion_comments);

                } else if (dis_cmt_size == 2) {
                    //reviewer_name
                    String reviewer_name13 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter13.txt_following_user_name.setText(reviewer_name13);
                    multiViewHolderAdapter13.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter13.txt_following_text.setText(R.string.discussion_comments);
                } else if (dis_cmt_size > 2) {
                    int dis_cmt_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name13 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(dis_cmt_dec)).concat(" others"));
                    multiViewHolderAdapter13.txt_following_user_name.setText(reviewer_name13);
                    multiViewHolderAdapter13.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter13.txt_following_text.setText(R.string.discussion_comments);
                }


                //Reviewer Image

                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter13.notify_user_photo, context);


// Timestamp for notification
                String createdon13 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon13, multiViewHolderAdapter13.txt_follow_ago);


                break;
            case 14:
                final NotiAnswerUpvoteAdapter multiViewHolderAdapter14 = (NotiAnswerUpvoteAdapter) holder;


                int ans_upv_size = getallNotificationPojoArrayList.get(position).getMultiCount();
                Log.e("ans_upv_size", "" + ans_upv_size);

                if (ans_upv_size == 1) {
                    //reviewer_name
                    String reviewer_name14 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter14.txt_following_user_name.setText(reviewer_name14);
                    multiViewHolderAdapter14.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter14.txt_following_text.setText(R.string.answer_upvote);

                } else if (ans_upv_size == 2) {
                    //reviewer_name
                    String reviewer_name14 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter14.txt_following_user_name.setText(reviewer_name14);
                    multiViewHolderAdapter14.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter14.txt_following_text.setText(R.string.answer_upvote);
                } else if (ans_upv_size > 2) {
                    int ans_upv_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name14 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(ans_upv_dec)).concat(" others"));
                    multiViewHolderAdapter14.txt_following_user_name.setText(reviewer_name14);
                    multiViewHolderAdapter14.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter14.txt_following_text.setText(R.string.answer_upvote);
                }


                //Reviewer Image

                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter14.notify_user_photo, context);


// Timestamp for notification
                String createdon14 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();

                Utility.setTimeStamp(createdon14, multiViewHolderAdapter14.txt_follow_ago);



                multiViewHolderAdapter14.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ViewQuestionAnswer.class);
                        intent.putExtra("quesId", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        intent.putExtra("position", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        context.startActivity(intent);
                    }
                });

                break;


            case 15:
                final NotiAnswerCommentsAdapter multiViewHolderAdapter15 = (NotiAnswerCommentsAdapter) holder;


                int ans_cmt_size = getallNotificationPojoArrayList.get(position).getMultiCount();
                Log.e("ans_cmt_size", "" + ans_cmt_size);

                if (ans_cmt_size == 1) {
                    //reviewer_name
                    String reviewer_name15 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter15.txt_following_user_name.setText(reviewer_name15);
                    multiViewHolderAdapter15.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter15.txt_following_text.setText(R.string.answer_comments);


                } else if (ans_cmt_size == 2) {
                    //reviewer_name
                    String reviewer_name15 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter15.txt_following_user_name.setText(reviewer_name15);
                    multiViewHolderAdapter15.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter15.txt_following_text.setText(R.string.answer_comments);
                } else if (ans_cmt_size > 2) {
                    int ans_cmt_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name15 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(ans_cmt_dec)).concat(" others"));
                    multiViewHolderAdapter15.txt_following_user_name.setText(reviewer_name15);
                    multiViewHolderAdapter15.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter15.txt_following_text.setText(R.string.answer_comments);
                }


                //Reviewer Image

                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter15.notify_user_photo, context);



// Timestamp for notification
                String createdon15 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();
                Utility.setTimeStamp(createdon15, multiViewHolderAdapter15.txt_follow_ago);




                multiViewHolderAdapter15.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ViewQuestionAnswer.class);
                        intent.putExtra("quesId", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        intent.putExtra("position", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        context.startActivity(intent);
                    }
                });


                break;
            case 16:

                final NotiReviewsLikesAdapter multiViewHolderAdapter16 = (NotiReviewsLikesAdapter) holder;
                int review_like_size = getallNotificationPojoArrayList.get(position).getMultiCount();

                Log.e("review_like_size", "" + review_like_size);

                if (review_like_size == 1) {
                    //reviewer_name
                    String reviewer_name16 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter16.txt_following_user_name.setText(reviewer_name16);
                    multiViewHolderAdapter16.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter16.txt_following_text.setText(R.string.review_likes);
                } else if (review_like_size == 2) {
                    //reviewer_name
                    String reviewer_name16 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter16.txt_following_user_name.setText(reviewer_name16);
                    multiViewHolderAdapter16.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter16.txt_following_text.setText(R.string.review_likes);
                } else if (review_like_size > 2) {
                    int rev_like_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name16 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(rev_like_dec)).concat(" others"));
                    multiViewHolderAdapter16.txt_following_user_name.setText(reviewer_name16);
                    multiViewHolderAdapter16.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter16.txt_following_text.setText(R.string.review_likes);
                }


                //Reviewer Image


                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter16.notify_user_photo, context);



// Timestamp for notification
                String createdon16 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();
                Utility.setTimeStamp(createdon16, multiViewHolderAdapter16.txt_follow_ago);



                //Likes redirect
                multiViewHolderAdapter16.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {



                        Intent intent = new Intent(context, ReviewSinglePage.class);
                        intent.putExtra("reviewId", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        intent.putExtra("userId", getallNotificationPojoArrayList.get(position).getMulti().get(position).getUserId());
                        context.startActivity(intent);


                    }
                });


                break;

            case 17:
                final NotiReviewsShareAdapter multiViewHolderAdapter17 = (NotiReviewsShareAdapter) holder;

                int review_shared_size = getallNotificationPojoArrayList.get(position).getMultiCount();

                Log.e("review_shared_size", "" + review_shared_size);

                if (review_shared_size == 1) {
                    //reviewer_name
                    String reviewer_name17 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter17.txt_following_user_name.setText(reviewer_name17);
                    multiViewHolderAdapter17.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter17.txt_following_text.setText(R.string.review_share);

                } else if (review_shared_size == 2) {
                    //reviewer_name
                    String reviewer_name17 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(1).getLastName())));
                    multiViewHolderAdapter17.txt_following_user_name.setText(reviewer_name17);
                    multiViewHolderAdapter17.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter17.txt_following_text.setText(R.string.review_share);
                } else if (review_shared_size > 2) {
                    int rev_shr_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name17 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(rev_shr_dec)).concat(" others"));
                    multiViewHolderAdapter17.txt_following_user_name.setText(reviewer_name17);
                    multiViewHolderAdapter17.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter17.txt_following_text.setText(R.string.review_share);
                }


                //Reviewer Image


                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter17.notify_user_photo, context);


// Timestamp for notification
                String createdon17 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();
                Utility.setTimeStamp(createdon17, multiViewHolderAdapter17.txt_follow_ago);




                break;

            case 18:
                final NotiReviewsCommentAdapter multiViewHolderAdapter18 = (NotiReviewsCommentAdapter) holder;


                int review_cmt_size = getallNotificationPojoArrayList.get(position).getMultiCount();

                Log.e("review_cmt_size", "" + review_cmt_size);

                if (review_cmt_size == 1) {
                    //reviewer_name
                    String reviewer_name18 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getFirstName().concat(" ").concat(getallNotificationPojoArrayList.get(position).getMulti().get(position).getLastName());
                    multiViewHolderAdapter18.txt_following_user_name.setText(reviewer_name18);
                    multiViewHolderAdapter18.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter18.txt_following_text.setText(R.string.review_comments);

                } else if (review_cmt_size == 2) {
                    //reviewer_name
                    String reviewer_name18 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").
                                    concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName())));
                    multiViewHolderAdapter18.txt_following_user_name.setText(reviewer_name18);
                    multiViewHolderAdapter18.txt_following_user_name.setTextColor(Color.parseColor("#000000"));

                    multiViewHolderAdapter18.txt_following_text.setText(R.string.review_comments);
                } else if (review_cmt_size > 2) {
                    int rev_cmt_dec = getallNotificationPojoArrayList.get(position).getMultiCount() - 1;
                    String reviewer_name18 = getallNotificationPojoArrayList.get(position).getMulti().get(0).getFirstName().concat(" ").
                            concat(getallNotificationPojoArrayList.get(position).getMulti().get(0).getLastName().concat(" and ").concat(String.valueOf(rev_cmt_dec)).concat(" others"));
                    multiViewHolderAdapter18.txt_following_user_name.setText(reviewer_name18);
                    multiViewHolderAdapter18.txt_following_user_name.setTextColor(Color.parseColor("#000000"));
                    multiViewHolderAdapter18.txt_following_text.setText(R.string.review_comments);
                }


                //Reviewer Image


                Utility.picassoImageLoader(getallNotificationPojoArrayList.get(position).getMulti().get(position).getPicture(),
                        0, multiViewHolderAdapter18.notify_user_photo, context);



// Timestamp for notification
                String createdon18 = getallNotificationPojoArrayList.get(position).getMulti().get(position).getCreatedOn();
                Utility.setTimeStamp(createdon18, multiViewHolderAdapter18.txt_follow_ago);



                //Redirecting to comment activity
                multiViewHolderAdapter18.rl_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, ReviewSinglePage.class);
                        intent.putExtra("reviewId", getallNotificationPojoArrayList.get(position).getMulti().get(position).getRelatedId());
                        intent.putExtra("userId", getallNotificationPojoArrayList.get(position).getMulti().get(position).getUserId());
                        context.startActivity(intent);
                    }
                });

                break;


        }

    }

    @Override
    public int getItemCount() {
        return getallNotificationPojoArrayList.size();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
