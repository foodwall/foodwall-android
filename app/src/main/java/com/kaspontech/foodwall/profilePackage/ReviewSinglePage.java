package com.kaspontech.foodwall.profilePackage;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewInputAllPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewOutputResponsePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.GetHotelDetailsAdapter;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewSinglePage extends _SwipeActivityClass implements View.OnClickListener {


    Toolbar actionBar;
    ImageButton back;

    TextView toolbar_title;

    int top_count, avoid_count, ambi_image_count, topdishimage_count, avoiddishimage_count;

    String fromwhich, txt_hotelname, user_id, hotel_id, txt_hotel_address, reviewId,
            txt_review_username, txt_review_userlastname, userimage, txt_review_follow, txt_review_like, txt_review_comment,
            txt_review_shares, txt_review_caption, hotel_review_rating, hotel_user_rating, total_followers, total_followings,
            hotelId, cat_type, veg_nonveg, revratID, ambiance, taste, total_package, total_timedelivery, service, valuemoney,
            createdby, createdon, googleID, placeID, opentimes, delivery_mode, dishname, topdishimage, topdishimage_name, avoiddishimage_name, avoiddishomage, ambiimage, dishtoavoid,
            category_type, latitude, longitude, phone, package_value, time_delivery, foodexp, photo_reference, emailID, likes_hotelID, revrathotel_likes,
            followingID, redirect_url, totalreview, total_ambiance, total_taste, total_service, modeid, company_name, total_value, total_good, total_bad, total_good_bad_user;


    int top_dish_img_count, avoid_dish_img_count, bucketlist,packCount;

    RecyclerView review_single_recylerview;
    ProgressBar progress_dialognew;
    GetHotelDetailsAdapter getHotelDetailsAdapter;
    public ArrayList<ReviewInputAllPojo> getallHotelReviewPojoArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_single_page);

        review_single_recylerview = (RecyclerView) findViewById(R.id.review_single_recylerview);
        progress_dialognew = (ProgressBar) findViewById(R.id.progress_dialognew);

        actionBar = (Toolbar) findViewById(R.id.actionbar_user_review);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);
        back.setOnClickListener(this);

        Intent intent = getIntent();
        reviewId = intent.getStringExtra("reviewId");
        user_id = intent.getStringExtra("userId");


        if (reviewId != null && user_id != null) {

            getReviewSingle();

        }


    }

    @Override
    public void onSwipeRight() {
        finish();
    }

    @Override
    protected void onSwipeLeft() {

    }

    private void getReviewSingle() {


        if (isConnected(ReviewSinglePage.this)) {

            progress_dialognew.setVisibility(View.VISIBLE);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            final String userId = Pref_storage.getDetail(ReviewSinglePage.this, "userId");

            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_single("get_hotel_review_single", Integer.parseInt(reviewId), Integer.parseInt(userId));
            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {

                        progress_dialognew.setVisibility(View.GONE);

                        getallHotelReviewPojoArrayList.clear();


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());


                            try {

                                hotelId = response.body().getData().get(j).getHotelId();
                                cat_type = response.body().getData().get(j).getCategoryType();
                                time_delivery = response.body().getData().get(j).getTimedelivery();
                                redirect_url = response.body().getData().get(j).getRedirect_url();
                                veg_nonveg = response.body().getData().get(j).getVegNonveg();
                                revratID = response.body().getData().get(j).getRevratId();
                                modeid = response.body().getData().get(j).getMode_id();
                                company_name = response.body().getData().get(j).getComp_name();
                                ambiance = response.body().getData().get(j).getAmbiance();
                                taste = response.body().getData().get(j).getTaste();
                                total_package = response.body().getData().get(j).getTotalPackage();
                                total_timedelivery = response.body().getData().get(j).getTotalTimedelivery();
                                service = response.body().getData().get(j).getService();
                                valuemoney = response.body().getData().get(j).getValueMoney();
                                createdby = response.body().getData().get(j).getCreatedBy();
                                createdon = response.body().getData().get(j).getCreatedOn();
                                googleID = response.body().getData().get(j).getGoogleId();
                                placeID = response.body().getData().get(j).getPlaceId();
                                opentimes = response.body().getData().get(j).getOpenTimes();
                                category_type = response.body().getData().get(j).getCategoryType();
                                latitude = response.body().getData().get(j).getLatitude();
                                longitude = response.body().getData().get(j).getLongitude();
                                phone = response.body().getData().get(j).getPhone();
                                photo_reference = response.body().getData().get(j).getPhotoReference();
                                emailID = response.body().getData().get(j).getEmail();
                                likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                                revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                                followingID = response.body().getData().get(j).getFollowingId();
                                totalreview = response.body().getData().get(j).getTotalReview();
                                total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                                total_taste = response.body().getData().get(j).getTotalTaste();
                                package_value = response.body().getData().get(j).get_package();
                                total_service = response.body().getData().get(j).getTotalService();
                                total_value = response.body().getData().get(j).getTotalValueMoney();
                                total_good = response.body().getData().get(j).getTotalGood();
                                total_bad = response.body().getData().get(j).getTotalBad();
                                total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();
                                txt_hotelname = response.body().getData().get(j).getHotelName();
                                user_id = response.body().getData().get(j).getUserId();
                                hotel_id = response.body().getData().get(j).getHotelId();
                                txt_hotel_address = response.body().getData().get(j).getAddress();
                                txt_review_username = response.body().getData().get(j).getFirstName();
                                txt_review_userlastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                txt_review_caption = response.body().getData().get(j).getHotelReview();
                                txt_review_like = response.body().getData().get(j).getTotalLikes();
                                txt_review_comment = response.body().getData().get(j).getTotalComments();
                                txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                                hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                                hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                                total_followers = response.body().getData().get(j).getTotalFollowers();
                                total_followings = response.body().getData().get(j).getTotalFollowings();
                                foodexp = response.body().getData().get(j).getFoodExprience();
                                top_count = response.body().getData().get(j).getTopCount();
                                dishtoavoid = response.body().getData().get(j).getAvoiddish();
//                                topdishimage_count= response.body().getData().get(j).getTopdishimageCount();
//                                avoiddishimage_count= response.body().getData().get(j).getAvoiddishimageCount();
                                avoid_count = response.body().getData().get(j).getAvoidCount();
                                ambi_image_count = response.body().getData().get(j).getAmbiImageCount();
                                dishname = response.body().getData().get(j).getTopdish();
                                delivery_mode = response.body().getData().get(j).getDelivery_mode();
                                top_dish_img_count = response.body().getData().get(j).getTop_dish_img_count();
                                avoid_dish_img_count = response.body().getData().get(j).getAvoid_dish_img_count();
                                bucketlist = response.body().getData().get(j).getBucket_list();
                                packCount = response.body().getData().get(j).getPackImageCount();

                                ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();

                                if (top_count == 0) {

                                } else {

                                    for (int b = 0; b < response.body().getData().get(j).getTopCount(); b++) {

                                        topdishimage = response.body().getData().get(j).getTopdishimage().get(b).getImg();
                                        topdishimage_name = response.body().getData().get(j).getTopdishimage().get(b).getDishname();
                                        ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimage_name);
                                        reviewTopDishPojoArrayList.add(reviewTopDishPojo);
                                    }


                                }

                                ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();

                                if (avoid_count == 0) {

                                } else {
                                    for (int a = 0; a < response.body().getData().get(j).getAvoidCount(); a++) {

                                        String dishavoidimage = response.body().getData().get(j).getAvoiddishimage().get(a).getImg();
                                        avoiddishimage_name = response.body().getData().get(j).getAvoiddishimage().get(a).getDishname();
                                        ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(dishavoidimage, avoiddishimage_name);
                                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);
                                    }

                                }


                                ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

                                //Ambiance
                                if (ambi_image_count == 0) {


                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getAmbiImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getAmbiImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(img);
                                        reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);
                                    }


                                }
                                //packaging

                                ArrayList<PackImage> reviewpackImagePojoArrayList = new ArrayList<>();


                                if (packCount == 0) {

                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getPackImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getPackImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        PackImage packImage = new PackImage(img);

                                        reviewpackImagePojoArrayList.add(packImage);
                                    }

                                }


                                ReviewInputAllPojo reviewInputAllPojo = new ReviewInputAllPojo(hotelId, revratID, txt_review_caption, txt_review_like, txt_review_comment,
                                        category_type, veg_nonveg, foodexp, ambiance, modeid, company_name, taste, service, package_value, time_delivery,
                                        valuemoney, createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude, longitude, phone,
                                        photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname, emailID, "", "", "", "",
                                        total_followers, total_followings, userimage, likes_hotelID, revrathotel_likes, followingID, totalreview,
                                        txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service, total_package, total_timedelivery,
                                        total_value, total_good, total_bad, total_good_bad_user, reviewTopDishPojoArrayList, topdishimage_count,
                                        dishname, top_count, reviewAvoidDishPojoArrayList, avoiddishimage_count, dishtoavoid, avoid_count,
                                        reviewAmbiImagePojoArrayList, ambi_image_count, false, delivery_mode, top_dish_img_count,
                                        avoid_dish_img_count, bucketlist, redirect_url,reviewpackImagePojoArrayList , packCount);

                                getallHotelReviewPojoArrayList.add(reviewInputAllPojo);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        toolbar_title.setText(response.body().getData().get(0).getHotelName());

                        getHotelDetailsAdapter = new GetHotelDetailsAdapter(ReviewSinglePage.this, getallHotelReviewPojoArrayList, fromwhich);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ReviewSinglePage.this);
                        review_single_recylerview.setLayoutManager(mLayoutManager);
                        review_single_recylerview.setItemAnimator(new DefaultItemAnimator());
                        review_single_recylerview.setAdapter(getHotelDetailsAdapter);
                        review_single_recylerview.setNestedScrollingEnabled(true);
                        getHotelDetailsAdapter.notifyDataSetChanged();


                    } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {

                        progress_dialognew.setVisibility(View.GONE);

                        /*img_no_dish.setVisibility(View.VISIBLE);
                        txt_no_dish.setVisibility(View.VISIBLE);*/
                    }

                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                    progress_dialognew.setVisibility(View.GONE);
                   /* img_no_dish.setVisibility(View.VISIBLE);
                    txt_no_dish.setVisibility(View.VISIBLE);*/
                }
            });
        } else {

            Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }

    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:
                finish();

                break;
        }
    }
}
