package com.kaspontech.foodwall.profilePackage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.kaspontech.foodwall.loginPackage.Login;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Timeline_create_output_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.utills.Utility.isValidPassword;

/**
 * Created by vishnukm on 27/3/18.
 */

public class Password_change_Activity extends AppCompatActivity implements View.OnClickListener {



    /**
     * Application tag
     **/
    private static final String TAG = "Password_Activity";
    /**
     * Toolbar
     **/
    Toolbar actionBar;
    /**
     * Back button
     **/
    ImageButton back;
    /**
     * Title text
     **/
    TextView toolbar_title;
    /**
     * Update button
     **/
    Button btn_updatepassword;
    /**
     * Password parent relative layout
     **/
    RelativeLayout pwdParent;

    /**
     * EditText's
     **/
    EditText editText_oldpassword, editText_newcnfpassword, editText_cnfpassword;

    /**
     * String result's
     **/
    String old_passwordresult, new_passwordresult, new_cnfpasswordresult;

    /**
     * boolean conditions
     **/
    private boolean  isValidPwd, isValidCnPwd;

    /**
     * GoogleApiClient
     **/
    private GoogleApiClient mGoogleApiClient;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_layout);

        // Action bar initialization
        actionBar = (Toolbar) findViewById(R.id.action_bar_profile);

        // Back button initialization

        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        // Title textview initialization
        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(R.string.click_here_to_change_password);

        // Update password button initialization

        btn_updatepassword = (Button) findViewById(R.id.btn_updatepassword);
        btn_updatepassword.setOnClickListener(this);

        // Parent password layout initialization

        pwdParent = (RelativeLayout) findViewById(R.id.pwdParent);

        // Edit text view's initializations

        editText_oldpassword = (EditText) findViewById(R.id.editText_oldpassword);
        editText_newcnfpassword = (EditText) findViewById(R.id.editText_newcnfpassword);
        editText_cnfpassword = (EditText) findViewById(R.id.editText_cnfpassword);


        /*
          Googleapiclient for google signin logout
          */
        initGoogleAPIClient();


        /*
          Here is a password validation when the users starts typing

          */
        editText_newcnfpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {



            }

            @Override
            public void afterTextChanged(Editable s) {

                // Validation
                if (isValidPassword(editText_newcnfpassword.getText().toString())) {




                    if (editText_newcnfpassword.getText().toString().length() < 9) {

                        editText_newcnfpassword.setError("Your password should be as per the instructions");
                        editText_newcnfpassword.setTextColor(getResources().getColor(R.color.red));

                    }else {

                        editText_newcnfpassword.setTextColor(getResources().getColor(R.color.Green));
                        editText_newcnfpassword.setError(null);
                        isValidPwd = true;

                    }
//                    enableSignUpButton();


                } else {

                    editText_newcnfpassword.setTextColor(getResources().getColor(R.color.colorAccent));

                    isValidPwd = false;

                    if (editText_newcnfpassword.getText().toString().length() < 9) {

                        editText_newcnfpassword.setError("Your password should be as per the instructions");
                        editText_newcnfpassword.setTextColor(getResources().getColor(R.color.red));

                    }
                }


                if (!editText_cnfpassword.getText().toString().equals(editText_newcnfpassword.getText().toString())) {

                    isValidCnPwd = false;
                    editText_cnfpassword.setTextColor(getResources().getColor(R.color.red));

                } else {

                    isValidCnPwd = true;
                    editText_cnfpassword.setTextColor(getResources().getColor(R.color.Green));

                }


            }
        });


        /*
          Here is a confirm password validation

          */
        editText_cnfpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {



            }

            @Override
            public void afterTextChanged(Editable s) {


                if (editText_newcnfpassword.getText().toString().length() > editText_cnfpassword.getText().toString().length()) {

                    editText_cnfpassword.setTextColor(getResources().getColor(R.color.colorAccent));
                    isValidCnPwd = false;

                }


                if (editText_newcnfpassword.getText().toString().length() <= editText_cnfpassword.getText().toString().length()) {

                    if (!editText_cnfpassword.getText().toString().equals(editText_newcnfpassword.getText().toString())) {

                        isValidCnPwd = false;
                        editText_cnfpassword.requestFocus();
                        editText_cnfpassword.setError("Your password does not matched");
                        editText_cnfpassword.setTextColor(getResources().getColor(R.color.red));
//                        enableSignUpButton();


                    } else {

                        if (editText_cnfpassword.getText().length() != 0) {

                            isValidCnPwd = true;

//                            enableSignUpButton();

                        }


                        editText_cnfpassword.setError(null);
                        editText_cnfpassword.setTextColor(getResources().getColor(R.color.Green));

                    }
                }


            }
        });




    }


    /** Initiate Google API Client  */
    private void initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(Password_change_Activity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();
        mGoogleApiClient.connect();
    }

/**
 * Unused method
 * **/
    private void enableSignUpButton() {

        if ( isValidPwd && isValidCnPwd) {

            btn_updatepassword.setEnabled(true);

        } else {

            btn_updatepassword.setEnabled(false);

        }

    }
    /**
     *Onclick method
     * **/
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {


            // Back button on click
            case R.id.back:
                finish();
                break;
            // Update password button click

            case R.id.btn_updatepassword:

                // Validation and API calling


                if (validate()) {

                    if (Utility.isConnected(this)) {

                        // API calling for updating password

                        update_change_password();

                    } else {

                        Snackbar snackbar = Snackbar.make(pwdParent, "Updating Password Failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        View view1 = snackbar.getView();
                        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                        textview.setTextColor(Color.WHITE);
                        snackbar.show();

                    }

                } else {

                    Snackbar snackbar = Snackbar.make(pwdParent, "Please check your password credentials", Snackbar.LENGTH_LONG);
                    snackbar.setActionTextColor(Color.RED);
                    View view1 = snackbar.getView();
                    TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                    textview.setTextColor(Color.WHITE);
                    snackbar.show();

                }


                break;
        }


    }

    /**
     * Logout  method
     * **/
    private void logout() {

        int loginType = Integer.parseInt(Pref_storage.getDetail(Password_change_Activity.this, "loginType"));

        switch (loginType) {

            case 1:

                // facebook
                LoginManager.getInstance().logOut();

                goToLogin();

                break;

            case 2:

                // Google+

                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                // ...

                                Log.d(TAG, "onResult: "+mGoogleApiClient.isConnected());
                                Log.d(TAG, "onResult: "+status.getStatus() );
                                goToLogin();

                            }
                        });

                break;

            case 3:

                // sign up

                goToLogin();

                break;
        }

    }

    /**
     * Redirecting to login page method
     * **/
    private void goToLogin() {

        Pref_storage.clearData();
//        ((ActivityManager) context.getSystemService(ACTIVITY_SERVICE)).clearApplicationUserData();
        Intent intent = new Intent(Password_change_Activity.this, Login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }



    /**
     *Validation
     * **/
    private boolean validate() {


        old_passwordresult = editText_oldpassword.getText().toString();
        new_passwordresult = editText_newcnfpassword.getText().toString();
        new_cnfpasswordresult = editText_cnfpassword.getText().toString();

        boolean valid = true;

        if (!new_passwordresult.equals(new_cnfpasswordresult)) {
            editText_newcnfpassword.requestFocus();
            editText_newcnfpassword.setError("Your password does not matched");
            valid = false;
        } else {
            editText_newcnfpassword.setError(null);
        }

        if(new_passwordresult.length() < 9){
            editText_newcnfpassword.requestFocus();
            editText_newcnfpassword.setError("Password password should be as per the instructions");
            valid = false;
        } else {
            editText_newcnfpassword.setError(null);
        }

        if (old_passwordresult.length() == 0) {
            editText_oldpassword.requestFocus();
            editText_oldpassword.setError(getString(R.string.password_field_not_be_empty));
            valid = false;
        } else {
            editText_oldpassword.setError(null);
        }


        if (new_passwordresult.length() == 0) {
            editText_newcnfpassword.requestFocus();
            editText_newcnfpassword.setError(getString(R.string.password_field_not_be_empty));
            valid = false;
        } else {
            editText_newcnfpassword.setError(null);
        }

        if (new_cnfpasswordresult.length() == 0) {
            editText_cnfpassword.requestFocus();
            editText_cnfpassword.setError(getString(R.string.password_field_not_be_empty));
            valid = false;
        } else {
            editText_cnfpassword.setError(null);
        }



        return valid;

    }



    /**
     * Calling Change password API
     **/
    private void update_change_password() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        String user_id = Pref_storage.getDetail(this, "userId");

        Log.e(TAG, "old_password" + old_passwordresult);
        Log.e(TAG, "new_password" + new_passwordresult);
        Log.e(TAG, "new_password" + new_cnfpasswordresult);

        String old_password = old_passwordresult;
        String new_password = new_passwordresult;


        Call<Timeline_create_output_pojo> call = apiService.update_change_password("update_change_password", Integer.parseInt(user_id), old_password, new_password);

        call.enqueue(new Callback<Timeline_create_output_pojo>() {
            @Override
            public void onResponse(Call<Timeline_create_output_pojo> call, Response<Timeline_create_output_pojo> response) {

                if (response.body().getResponseCode() == 1) {
                    Toast.makeText(Password_change_Activity.this, "Your password has been updated successfully", Toast.LENGTH_SHORT).show();
                    /*startActivity(new Intent(Password_change_Activity.this, Home.class));
                    finish();*/

                    // Redirect to login page
                    logout();


                } else if (response.body().getResponseMessage().contains("oldpasswordincorrect")) {

                    editText_oldpassword.requestFocus();
                    editText_oldpassword.setError("Please enter correct password");

                }
            }

            @Override
            public void onFailure(Call<Timeline_create_output_pojo> call, Throwable t) {
                //Error
                Log.e("FailureError", "" + t.getMessage());
            }
        });


    }

}
