package com.kaspontech.foodwall.profilePackage.profileTabFragments.viewFullImages;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kaspontech.foodwall.modelclasses.HistoricalMap.DisplayPojo.GetHistoryMapData;
import com.kaspontech.foodwall.R;

import java.util.ArrayList;
import java.util.List;

public class TimeLineImageFullView extends AppCompatActivity implements View.OnClickListener{


    Toolbar actionbar;
    ImageButton back;
    TextView tv_timeline_caption;
    TextView toolbar_title;


    GetHistoryMapData getHistoryMapData;
    RecyclerView recycler_timeline_full_image;
    TimeLineImageFullViewAdapter timeLineImageFullViewAdapter;
    List<GetHistoryMapData> getHistoryMapDataList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_line_image_full_view);


        actionbar = (Toolbar)findViewById(R.id.action_bar_view_images);
        back = (ImageButton)actionbar.findViewById(R.id.back);
        toolbar_title = (TextView)actionbar.findViewById(R.id.toolbar_title);
        tv_timeline_caption = (TextView)findViewById(R.id.tv_timeline_caption);
        recycler_timeline_full_image = (RecyclerView)findViewById(R.id.recycler_timeline_full_image);
        back.setOnClickListener(this);


        getHistoryMapData = (GetHistoryMapData) getIntent().getSerializableExtra("GetHistoryMapData");




        if(getHistoryMapData != null){

            String caption = getHistoryMapData.getHisDescripion();
            String hotelname = getHistoryMapData.getHotelName();
            toolbar_title.setText(hotelname);
            toolbar_title.setVisibility(View.VISIBLE);
            tv_timeline_caption.setText(caption);
            getHistoryMapDataList.add(getHistoryMapData);

            timeLineImageFullViewAdapter = new TimeLineImageFullViewAdapter(TimeLineImageFullView.this, getHistoryMapDataList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(TimeLineImageFullView.this);
            recycler_timeline_full_image.setLayoutManager(mLayoutManager);
            recycler_timeline_full_image.setItemAnimator(new DefaultItemAnimator());
            recycler_timeline_full_image.setAdapter(timeLineImageFullViewAdapter);
            recycler_timeline_full_image.hasFixedSize();
            recycler_timeline_full_image.setNestedScrollingEnabled(false);
            timeLineImageFullViewAdapter.notifyDataSetChanged();

        }




    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id){

            case R.id.back:

                finish();

                break;
        }
    }
}
