package com.kaspontech.foodwall.profilePackage.profileTabFragments.viewFullImages;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.DisplayPojo.GetHistoryMapData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Utility;


import java.util.ArrayList;
import java.util.List;

public class TimeLineImageFullViewAdapter extends RecyclerView.Adapter<TimeLineImageFullViewAdapter.MyViewHolder> implements RecyclerView.OnItemTouchListener {


    private Context context;
    private List<GetHistoryMapData> getHistoryMapDataList = new ArrayList<>();


    public TimeLineImageFullViewAdapter(Context c, List<GetHistoryMapData> getHistoryMapDataList) {
        this.context = c;
        this.getHistoryMapDataList = getHistoryMapDataList;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dishes_image_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        TimeLineImageFullViewAdapter.MyViewHolder vh = new TimeLineImageFullViewAdapter.MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        holder.rl_text_dish.setVisibility(View.GONE);

        String image = getHistoryMapDataList.get(0).getImage().get(position).getImg();


//        Utility.picassoImageLoader(image,1,holder.image_review, context);

        GlideApp.with(context)
                .load(image)
                .error(R.drawable.ic_no_post)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .centerCrop()
                .into(holder.image_review);



    }


    @Override
    public int getItemCount() {
        return getHistoryMapDataList.get(0).getImage().size();

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //TextView

        TextView txt_image;

        //Imageview

        PhotoView image_review;

        RelativeLayout rl_text_dish;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_image = (TextView) itemView.findViewById(R.id.txt_image);
            image_review = (PhotoView) itemView.findViewById(R.id.image_review);
            rl_text_dish = (RelativeLayout) itemView.findViewById(R.id.rl_text_dish);

        }


    }


}
