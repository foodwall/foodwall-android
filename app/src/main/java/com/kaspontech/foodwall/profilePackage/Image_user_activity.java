package com.kaspontech.foodwall.profilePackage;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.adapters.TimeLine.Single_user_timeline_adapter;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Image;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Timeline_single_user_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.profile_timeline_output_response;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Image_user_activity extends AppCompatActivity implements View.OnClickListener {

    Toolbar actionBar;
    ImageButton back;
    TextView toolbar_title;
    ProgressBar progress_dialog;
    RelativeLayout rl_timefeed_user;

    RecyclerView rv_user_timeline;
    Timeline_single_user_pojo timelineSingleUserPojo;
    String picture;
    List<Image> imageList = new ArrayList<>();

    ArrayList<Timeline_single_user_pojo> gettingtimelinesinglelist = new ArrayList<>();

    public static ArrayList<String> timeline_user_list = new ArrayList<>();
    List<Image> myList = new ArrayList<>();
    ArrayList<String> myListnew = new ArrayList<String>();
    Single_user_timeline_adapter singleUserTimelineAdapter;


    TextView username, userlastname, location, location_state, like, liketxt, user_txt_comment, count, comma, count_post, txt_followers_count, txt_followers, selfusername_caption, selfuserlastname_caption, user_name_comment, viewcomment, txt_timeline_follow, comment, commenttest, share, caption, captiontag, txt_created_on, txt_adapter_post;
    RelativeLayout rl_likes_layout, rl_comments_lay;
    RelativeLayout rl_share_adapter, rl_comment_adapter, rl_comments_layout, like_rl, rl_username, rl_more, rl_post_timeline, rl_follow_layout;

    ImageView img_view_more, userphoto_profile, img_follow_timeline, img_commentadapter, img_shareadapter, userphoto_comment;
    LinearLayout rl_row_layout;
    EditText edittxt_comment;
    ImageButton img_like;
    PhotoView image;

    private static ViewPager mviewpager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;


    private CircleIndicator circleIndicator;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_image_tile_activity);

        actionBar = (Toolbar) findViewById(R.id.actionbar_user_timeline);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(R.string.images);
        rl_timefeed_user = (RelativeLayout) findViewById(R.id.rl_timefeed_user);

        rv_user_timeline = (RecyclerView) findViewById(R.id.rv_user_timeline);


        img_like = (ImageButton) findViewById(R.id.img_like);

        //Edittext
        edittxt_comment = (EditText) findViewById(R.id.edittxt_comment);


        //ViewPager
        mviewpager = (ViewPager) findViewById(R.id.mviewpager);


        //Circle Indicator
        circleIndicator = (CircleIndicator) findViewById(R.id.indicator);

//            Image.setImage(ImageSource.resource(R.drawable.burger));


        //ImageView
        image = (PhotoView) findViewById(R.id.image);
        userphoto_profile = (ImageView) findViewById(R.id.userphoto_profile);
        userphoto_comment = (ImageView) findViewById(R.id.userphoto_comment);
        img_commentadapter = (ImageView) findViewById(R.id.img_commentadapter);
        img_shareadapter = (ImageView) findViewById(R.id.img_shareadapter);
        img_view_more = (ImageView) findViewById(R.id.img_view_more);
        img_follow_timeline = (ImageView) findViewById(R.id.img_follow_timeline);
        //TextView
        username = (TextView) findViewById(R.id.username);
        txt_adapter_post = (TextView) findViewById(R.id.txt_adapter_post);
        userlastname = (TextView) findViewById(R.id.userlastname);
        location = (TextView) findViewById(R.id.location);
//            location_state = (TextView) itemView.findViewById(R.id.location_state);
        caption = (TextView) findViewById(R.id.caption);
        captiontag = (TextView) findViewById(R.id.captiontag);
        txt_created_on = (TextView) findViewById(R.id.txt_created_on);
        like = (TextView) findViewById(R.id.like);
        liketxt = (TextView) findViewById(R.id.liketxt);
        comment = (TextView) findViewById(R.id.comment);
        comma = (TextView) findViewById(R.id.comma);
        user_txt_comment = (TextView) findViewById(R.id.user_txt_comment);
        user_name_comment = (TextView) findViewById(R.id.user_name_comment);
        selfusername_caption = (TextView) findViewById(R.id.selfusername_caption);
        selfuserlastname_caption = (TextView) findViewById(R.id.selfuserlastname_caption);
        viewcomment = (TextView) findViewById(R.id.View_all_comment);
        commenttest = (TextView) findViewById(R.id.commenttest);
        share = (TextView) findViewById(R.id.shares);
        count = (TextView) findViewById(R.id.count);
        count_post = (TextView) findViewById(R.id.count_post);
        txt_followers = (TextView) findViewById(R.id.txt_followers);
        txt_followers_count = (TextView) findViewById(R.id.txt_followers_count);

        //RelativeLayout
        rl_row_layout = (LinearLayout) findViewById(R.id.layout_one_foradapter);
        rl_likes_layout = (RelativeLayout) findViewById(R.id.rl_likes_layout);
        rl_comments_lay = (RelativeLayout) findViewById(R.id.rl_comments_layout);
        rl_share_adapter = (RelativeLayout) findViewById(R.id.rl_share_adapter);
        rl_comment_adapter = (RelativeLayout) findViewById(R.id.rl_comment_adapter);
        like_rl = (RelativeLayout) findViewById(R.id.like_rl);
        rl_username = (RelativeLayout) findViewById(R.id.rl_username);
        rl_more = (RelativeLayout) findViewById(R.id.rl_more);
        rl_follow_layout = (RelativeLayout) findViewById(R.id.rl_follow_layout);
        rl_post_timeline = (RelativeLayout) findViewById(R.id.rl_post_timeline);


        progress_dialog = (ProgressBar) findViewById(R.id.progress_dialognew);
        progress_dialog.setVisibility(View.VISIBLE);


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {


                get_timeline_single();


            }

        }, 1000);


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:
                finish();

                break;
        }
    }


    private void get_timeline_single() {

        if (isConnected(this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String timeline_clicked = Pref_storage.getDetail(this, "user_timeline_clicked");
            String userID_clicked = Pref_storage.getDetail(this, "user_timelineuserID_clicked");

            Call<profile_timeline_output_response> call = apiService.get_timeline_single("get_timeline_single", Integer.parseInt(timeline_clicked), Integer.parseInt(userID_clicked));


            call.enqueue(new Callback<profile_timeline_output_response>() {
                @Override
                public void onResponse(Call<profile_timeline_output_response> call, Response<profile_timeline_output_response> response) {

                    if (response.isSuccessful()) {

                        try {
                            progress_dialog.setVisibility(View.GONE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                       /* for (int j = 0; j < response.body().getData().size(); j++) {


                            String timelineId = response.body().getData().get(j).getTimelineId();
                            Pref_storage.setDetail(getApplicationContext(), "timelineid_selfSpecified", timelineId);

                            String timelineHotel = response.body().getData().get(j).getTimelineHotel();
                            String timelineDescription = response.body().getData().get(j).getTimelineDescription();
                            String address = response.body().getData().get(j).getAddress();
                            String createdBy = response.body().getData().get(j).getCreatedBy();
                            String createdOn = response.body().getData().get(j).getCreatedOn();
                            int totalLikes = response.body().getData().get(j).getTotalLikes();
                            String totalComments = response.body().getData().get(j).getTotalComments();
                            String latitude = response.body().getData().get(j).getLatitude();
                            String longitude = response.body().getData().get(j).getLongitude();
                            String visibilityType = response.body().getData().get(j).getVisibilityType();
                            String postType = response.body().getData().get(j).getPostType();
                            String userId = response.body().getData().get(j).getUserId();
                            String oauthProvider = response.body().getData().get(j).getOauthProvider();
                            String oauthUid = response.body().getData().get(j).getOauthUid();
                            String firstName = response.body().getData().get(j).getFirstName();
                            String lastName = response.body().getData().get(j).getLastName();
                            String email = response.body().getData().get(j).getEmail();
                            String imei = response.body().getData().get(j).getImei();
                            String contNo = response.body().getData().get(j).getContNo();
                            String gender = response.body().getData().get(j).getGender();
                            String dob = response.body().getData().get(j).getDob();
                            String bioDescription = response.body().getData().get(j).getBioDescription();
                            String totalFollowers = response.body().getData().get(j).getTotalFollowers();
                            String totalFollowings = response.body().getData().get(j).getTotalFollowings();
                            String totalPosts = response.body().getData().get(j).getTotalPosts();
                            String picture = response.body().getData().get(j).getPicture();
                            int likesTlId = response.body().getData().get(j).getLikesTlId();
                            String tlLikes = response.body().getData().get(j).getTlLikes();
                            int eventId = response.body().getData().get(j).getEventId();
                            String eventName = response.body().getData().get(j).getEventName();
                            String eventDescription = response.body().getData().get(j).getEventDescription();
                            String eventImage = response.body().getData().get(j).getEventImage();
                            String startDate = response.body().getData().get(j).getStartDate();
                            String endDate = response.body().getData().get(j).getEndDate();
                            String location = response.body().getData().get(j).getLocation();
                            String followingId = response.body().getData().get(j).getFollowingId();
                            int followuserId = response.body().getData().get(j).getFollowuserId();
                            String followuserFirstname = response.body().getData().get(j).getFollowuserFirstname();
                            String followuserLastname = response.body().getData().get(j).getFollowuserLastname();
                            String followuserPicture = response.body().getData().get(j).getFollowuserPicture();
                            String hotel = response.body().getData().get(j).getHotel();
                            int reviewId = response.body().getData().get(j).getReviewId();
                            String review = response.body().getData().get(j).getReview();
                            String reviewComments = response.body().getData().get(j).getReviewComments();
                            int questId = response.body().getData().get(j).getQuestId();
                            String askQuestion = response.body().getData().get(j).getAskQuestion();
                            int ansId = response.body().getData().get(j).getAnsId();
                            String askAnswer = response.body().getData().get(j).getAskAnswer();
                            int gngEvtId = response.body().getData().get(j).getGngEvtId();
                            int likeEvtId = response.body().getData().get(j).getLikeEvtId();
                            int follUserid = response.body().getData().get(j).getFollUserid();
                            int likeRevId = response.body().getData().get(j).getLikeRevId();
                            int cmmtRevId = response.body().getData().get(j).getCmmtRevId();
                            int ansQuesId = response.body().getData().get(j).getAnsQuesId();
                            int upAnsId = response.body().getData().get(j).getUpAnsId();
                            List<TaggedPeoplePojo> whom = response.body().getData().get(j).getWhom();
                            Integer whomCount = response.body().getData().get(j).getWhomCount();
                            List<Image> image = response.body().getData().get(j).getImage();
                            Integer imageCount = response.body().getData().get(j).getImageCount();


                            String getimage = response.body().getData().get(j).getImage().get(0).getImg();


                            List<String> newmyList = new ArrayList<String>(Arrays.asList(getimage));

                            timeline_user_list.add(String.valueOf(newmyList));


                            Pref_storage.setDetail(getApplicationContext(), "picture_user", picture);


                            Timeline_single_user_pojo timelineSingleUserPojo = new Timeline_single_user_pojo(
                                    timelineId, timelineHotel, timelineDescription, address, createdBy, createdOn,
                                    totalLikes, totalComments, latitude, longitude, visibilityType, postType, userId,
                                    oauthProvider, oauthUid, firstName, lastName, email, imei, contNo,
                                    gender, dob, bioDescription, totalFollowers, totalFollowings, totalPosts, picture, likesTlId,
                                    tlLikes, eventId,eventName, eventDescription, eventImage, startDate, endDate, location,
                                    followingId, followuserId, followuserFirstname, followuserLastname, followuserPicture,
                                    hotel, reviewId, review, reviewComments, questId, askQuestion, ansId,
                                    askAnswer, gngEvtId, likeEvtId, follUserid, likeRevId, cmmtRevId, ansQuesId,
                                    upAnsId, whom, whomCount, image, imageCount);*/

                        gettingtimelinesinglelist = response.body().getData();
                        singleUserTimelineAdapter = new Single_user_timeline_adapter(Image_user_activity.this, gettingtimelinesinglelist, timeline_user_list);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Image_user_activity.this);
                        rv_user_timeline.setLayoutManager(mLayoutManager);
                        rv_user_timeline.setItemAnimator(new DefaultItemAnimator());
                        rv_user_timeline.setAdapter(singleUserTimelineAdapter);
                        singleUserTimelineAdapter.notifyDataSetChanged();
                        rv_user_timeline.setNestedScrollingEnabled(false);
                    }

                }


            @Override
            public void onFailure (Call <profile_timeline_output_response> call, Throwable t){
                //Error
                Log.e("FailureError", "" + t.getMessage());
                Snackbar snackbar = Snackbar.make(rl_timefeed_user, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(Color.RED);
                View view1 = snackbar.getView();
                TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                textview.setTextColor(Color.WHITE);
                snackbar.show();
            }
        });
    } else

    {
        Snackbar snackbar = Snackbar.make(rl_timefeed_user, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View view1 = snackbar.getView();
        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
        textview.setTextColor(Color.WHITE);
        snackbar.show();
    }

}


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


}
