package com.kaspontech.foodwall.profilePackage.profileTabFragments;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.kaspontech.foodwall.adapters.HistoricalMap.DisplayHistoryMapAdapter;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.DisplayPojo.GetHistoricalMapOutputPojo;
import com.kaspontech.foodwall.modelclasses.HistoricalMap.DisplayPojo.GetHistoryMapData;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.RecyclerSectionItemDecoration;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoricalImagesFragment extends Fragment implements View.OnClickListener {

    View view;
    Context context;

    LottieAnimationView img_no_notify;


    private int prev, next;

    Button sort_history, sort_clear;
    TextView tv_restaurant_count, tv_sorted_month;
    LinearLayout ll_historical_map, ll_not_visited, ll_sorted_month;

    RecyclerView recyler_view_historical_map, sort_recyler_view_historical_map;
    DisplayHistoryMapAdapter displayHistoryMapAdapter;
    List<GetHistoryMapData> getHistoryMapDataList = new ArrayList<>();
    List<GetHistoryMapData> sortedHistoricalMapDataList = new ArrayList<>();

    // Var

    String userID;
    int total_restaurants;

    public HistoricalImagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_historical_map, container, false);
        context = view.getContext();


        sort_history = (Button) view.findViewById(R.id.sort_history);
        img_no_notify = (LottieAnimationView) view.findViewById(R.id.img_no_notify);

        sort_clear = (Button) view.findViewById(R.id.sort_clear);
        tv_restaurant_count = (TextView) view.findViewById(R.id.tv_restaurant_count);
        tv_sorted_month = (TextView) view.findViewById(R.id.tv_sorted_month);
        ll_historical_map = (LinearLayout) view.findViewById(R.id.ll_historical_map);
        ll_not_visited = (LinearLayout) view.findViewById(R.id.ll_not_visited);
        ll_sorted_month = (LinearLayout) view.findViewById(R.id.ll_sorted_month);
        recyler_view_historical_map = (RecyclerView) view.findViewById(R.id.recyler_view_historical_map);
        sort_recyler_view_historical_map = (RecyclerView) view.findViewById(R.id.sort_recyler_view_historical_map);

        if (getArguments() != null) {
            userID = getArguments().getString("userid");
        }

        sort_history.setOnClickListener(this);
        sort_clear.setOnClickListener(this);


        get_historical();


        return view;
    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            case R.id.sort_history:

                monthPickerDialog();

                break;

            case R.id.sort_clear:

                sortedHistoricalMapDataList.clear();
                ll_not_visited.setVisibility(View.GONE);

                if (sort_clear.getVisibility() == View.VISIBLE) {

                    sort_clear.setVisibility(View.GONE);

                }

                if (tv_sorted_month.getVisibility() == View.VISIBLE) {

                    tv_sorted_month.setVisibility(View.GONE);

                }

                if (ll_sorted_month.getVisibility() == View.VISIBLE) {

                    ll_sorted_month.setVisibility(View.GONE);

                }


                if (recyler_view_historical_map.getVisibility() == View.GONE) {

                    recyler_view_historical_map.setVisibility(View.VISIBLE);

                }


                break;

        }

    }


    private void monthPickerDialog() {

        final Calendar today = Calendar.getInstance();

        int size = getHistoryMapDataList.size();

        if (size != 0) {


            int maxYear = today.get(Calendar.YEAR);
            int minYear = maxYear - 10;


            MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(context, new MonthPickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(int selectedMonth, int selectedYear) {

                    int validMonth = selectedMonth + 1;
                    String month;


                    if (validMonth > 9) {

                        month = String.valueOf(validMonth);

                    } else {

                        month = "0" + String.valueOf(validMonth);


                    }

                    sortHistoryMap(Integer.parseInt(month), selectedYear);

                    Log.d("monthPickerDialog", "selectedMonth : " + month + " selectedYear : " + selectedYear);
//                Toast.makeText(context, "Date set with month " + month + " year " + selectedYear, Toast.LENGTH_SHORT).show();

                }
            }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

            builder.setActivatedMonth(today.get(Calendar.MONTH))
                    .setMinYear(minYear)
                    .setActivatedYear(today.get(Calendar.YEAR))
                    .setMaxYear(maxYear)
                    .setMinMonth(Calendar.JANUARY)
                    .setTitle("")
                    .setMonthRange(Calendar.JANUARY, Calendar.DECEMBER)
                    .setMaxMonth(today.get(Calendar.MONTH))
                    // .setYearRange(1890, 1890)
                    // .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
                    //.showMonthOnly()
                    // .showYearOnly()
                    .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                        @Override
                        public void onMonthChanged(int selectedMonth) {
                            Log.d("monthPickerDialog", "Selected month : " + selectedMonth + 1);
//                         Toast.makeText(context, " Selected month : " + selectedMonth+1, Toast.LENGTH_SHORT).show();


                        }
                    })
                    .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                        @Override
                        public void onYearChanged(int selectedYear) {
                            Log.d("monthPickerDialog", "Selected year : " + selectedYear);
//                         Toast.makeText(context, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                        }
                    })
                    .build()
                    .show();

        } else {


        }


    }

    private void sortHistoryMap(int month, int year) {


        String displayMonthYear = getMonthNameByNumber(month) + " " + String.valueOf(year);

        tv_sorted_month.setText(displayMonthYear);


        if (recyler_view_historical_map.getVisibility() == View.VISIBLE) {

            recyler_view_historical_map.setVisibility(View.GONE);

        }

        if (sort_recyler_view_historical_map.getVisibility() == View.GONE) {

            sort_recyler_view_historical_map.setVisibility(View.VISIBLE);

        }


        if (tv_sorted_month.getVisibility() == View.GONE) {

            tv_sorted_month.setVisibility(View.VISIBLE);

        }

        if (ll_sorted_month.getVisibility() == View.GONE) {

            ll_sorted_month.setVisibility(View.VISIBLE);

        }

        if (sort_clear.getVisibility() == View.GONE) {

            sort_clear.setVisibility(View.VISIBLE);

        }


        sortedHistoricalMapDataList.clear();

        for (int i = 0; i < getHistoryMapDataList.size(); i++) {

            String createdDate = getHistoryMapDataList.get(i).getCreatedOn();

            if (createdDate != null) {

                int tempMonth = Integer.parseInt(getHistoryMapDataList.get(i).getCreatedOn().substring(5, 7));
                int tempYear = Integer.parseInt(getYear(getHistoryMapDataList.get(i).getCreatedOn()));


                if (tempMonth == month && tempYear == year) {

                    sortedHistoricalMapDataList.add(getHistoryMapDataList.get(i));

                }


            }


        }

        if (sortedHistoricalMapDataList.size() == 0) {

            ll_not_visited.setVisibility(View.VISIBLE);
            Log.e("CheckingSorted", "CheckingSorted->" + sortedHistoricalMapDataList.size());

        } else {

            ll_not_visited.setVisibility(View.GONE);

            Log.e("CheckingSorted", "CheckingSorted->" + sortedHistoricalMapDataList.size());
            DisplayHistoryMapAdapter displayHistoryMapAdapter = new DisplayHistoryMapAdapter(context, sortedHistoricalMapDataList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            sort_recyler_view_historical_map.setLayoutManager(mLayoutManager);
            sort_recyler_view_historical_map.setItemAnimator(new DefaultItemAnimator());
            sort_recyler_view_historical_map.setAdapter(displayHistoryMapAdapter);
            sort_recyler_view_historical_map.hasFixedSize();
            sort_recyler_view_historical_map.setNestedScrollingEnabled(false);
            displayHistoryMapAdapter.notifyDataSetChanged();
        }


    }


    private void get_historical() {


        getHistoryMapDataList.clear();

        if (recyler_view_historical_map.getVisibility() == View.GONE) {

            recyler_view_historical_map.setVisibility(View.VISIBLE);

        }

        if (sort_recyler_view_historical_map.getVisibility() == View.VISIBLE) {

            sort_recyler_view_historical_map.setVisibility(View.GONE);

        }

        if (sort_clear.getVisibility() == View.VISIBLE) {

            sort_clear.setVisibility(View.GONE);

        }

        if (tv_sorted_month.getVisibility() == View.VISIBLE) {

            tv_sorted_month.setVisibility(View.GONE);

        }


        if (ll_sorted_month.getVisibility() == View.VISIBLE) {

            ll_sorted_month.setVisibility(View.GONE);

        }

        Activity activity = getActivity();

        if (activity != null) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            int userid = Integer.parseInt(userID);

            try {

                Call<GetHistoricalMapOutputPojo> call = apiService.getHistoricalMap("get_historical",
                        userid);

                call.enqueue(new Callback<GetHistoricalMapOutputPojo>() {
                    @Override
                    public void onResponse(Call<GetHistoricalMapOutputPojo> call, Response<GetHistoricalMapOutputPojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            //Success

                            ll_not_visited.setVisibility(View.GONE);
                            sort_history.setVisibility(View.VISIBLE);

                            getHistoryMapDataList = response.body().getData();

                            /*for (int i = 0; i < response.body().getData().size(); i++) {

                                String timeline_id = response.body().getData().get(i).getTimeline_id();
                                String hisId = response.body().getData().get(i).getHisId();
                                String hotelName = response.body().getData().get(i).getHotelName();
                                String hisDescripion = response.body().getData().get(i).getHisDescripion();
                                String withWhom = response.body().getData().get(i).getWithWhom();
                                String address = response.body().getData().get(i).getAddress();
                                String createdBy = response.body().getData().get(i).getCreatedBy();
                                String createdOn = response.body().getData().get(i).getCreatedOn();
                                String firstName = response.body().getData().get(i).getFirstName();
                                String lastName = response.body().getData().get(i).getLastName();
                                String picture = response.body().getData().get(i).getPicture();
                                List<GetHisMapImage> image = response.body().getData().get(i).getImage();
                                List<TaggedPeoplePojo> whom = response.body().getData().get(i).getWhom();
                                int imageCount = response.body().getData().get(i).getImageCount();
                                int whom_count = response.body().getData().get(i).getWhom_count();
                                total_restaurants = Integer.parseInt(response.body().getData().get(i).getTotalHotel());

                                GetHistoryMapData getHistoryMapData = new GetHistoryMapData(timeline_id, hisId, hotelName,
                                        hisDescripion, withWhom, address, createdBy, createdOn, firstName, lastName
                                        , picture, image, imageCount, whom_count, whom);


                                getHistoryMapDataList.add(getHistoryMapData);


                            }*/

//                        dialog.dismiss();
                            ll_historical_map.setVisibility(View.VISIBLE);

                            ValueAnimator animator = ValueAnimator.ofInt(0, total_restaurants);
                            animator.setDuration(1500);
                            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                public void onAnimationUpdate(ValueAnimator animation) {
                                    tv_restaurant_count.setText(animation.getAnimatedValue().toString());
                                }
                            });
                            animator.start();


                            displayHistoryMapAdapter = new DisplayHistoryMapAdapter(context, getHistoryMapDataList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                            recyler_view_historical_map.setLayoutManager(mLayoutManager);
                            recyler_view_historical_map.setItemAnimator(new DefaultItemAnimator());
                            recyler_view_historical_map.setAdapter(displayHistoryMapAdapter);
                            recyler_view_historical_map.hasFixedSize();
                            recyler_view_historical_map.setNestedScrollingEnabled(false);
                            displayHistoryMapAdapter.notifyDataSetChanged();

                            Activity activity = getActivity();
                            if (isAdded() && activity != null) {

                                RecyclerSectionItemDecoration sectionItemDecoration =
                                        new RecyclerSectionItemDecoration(getResources().getDimensionPixelSize(R.dimen.header),
                                                true,
                                                getSectionCallback(getHistoryMapDataList));
                                recyler_view_historical_map.addItemDecoration(sectionItemDecoration);

                            }


                        } else if (responseStatus.equals("nodata")) {

                            ll_not_visited.setVisibility(View.VISIBLE);
                            sort_history.setVisibility(View.GONE);

//                        dialog.dismiss();

                        }

                    }

                    @Override
                    public void onFailure(Call<GetHistoricalMapOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                        ll_not_visited.setVisibility(View.VISIBLE);
                        sort_history.setVisibility(View.GONE);

//                    dialog.dismiss();

                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        }


    }

    private RecyclerSectionItemDecoration.SectionCallback getSectionCallback(final List<GetHistoryMapData> getHistoryMapDataList) {
        return new RecyclerSectionItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {

                boolean show = false;

                if (position == 0) {

                    show = true;

                }

                if (position != 0) {

                    prev = Integer.parseInt(getHistoryMapDataList.get(position - 1).getCreatedOn().substring(5, 7));
                    next = Integer.parseInt(getHistoryMapDataList.get(position).getCreatedOn().substring(5, 7));

                    if (prev > next) {

                        show = true;

                    } else {

                        show = false;

                    }
                }

                return show;
            }

            @Override
            public CharSequence getSectionHeader(int position) {


                String date = getHistoryMapDataList.get(position).getCreatedOn();
                String displayDate = getMonthName(date) + " " + getYear(date);


                return displayDate;
            }
        };
    }


    private String getMonthName(String date) {

        String monthName = "";

        String month = date.substring(5, 7);

        switch (month) {


            case "01":
                monthName = "January";
                break;

            case "02":
                monthName = "February";
                break;

            case "03":
                monthName = "March";
                break;

            case "04":
                monthName = "April";
                break;

            case "05":
                monthName = "May";
                break;

            case "06":
                monthName = "June";
                break;

            case "07":
                monthName = "July";
                break;

            case "08":
                monthName = "August";
                break;

            case "09":
                monthName = "September";
                break;

            case "10":
                monthName = "October";
                break;

            case "11":
                monthName = "November";
                break;

            case "12":
                monthName = "December";
                break;
        }


        return monthName;
    }


    private String getMonthNameByNumber(int month) {

        String monthName = "";

        switch (month) {


            case 1:
                monthName = "January";
                break;

            case 2:
                monthName = "February";
                break;

            case 3:
                monthName = "March";
                break;

            case 4:
                monthName = "April";
                break;

            case 5:
                monthName = "May";
                break;

            case 6:
                monthName = "June";
                break;

            case 7:
                monthName = "July";
                break;

            case 8:
                monthName = "August";
                break;

            case 9:
                monthName = "September";
                break;

            case 10:
                monthName = "October";
                break;

            case 11:
                monthName = "November";
                break;

            case 12:
                monthName = "December";
                break;
        }


        return monthName;
    }

    private String getDate(String date) {

        String day = date.substring(8, 10);
        return day;

    }

    private String getYear(String date) {

        String year = date.substring(0, 4);
        return year;

    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


}

