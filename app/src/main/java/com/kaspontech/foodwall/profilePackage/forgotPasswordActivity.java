package com.kaspontech.foodwall.profilePackage;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Timeline_create_output_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Utility;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.utills.Utility.isValidEmail;


/**
 * Created by vishnukm on 28/3/18.
 */

public class forgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {


    /**
     * Application tag
     **/
    private static final String TAG = "ForgotPWD_activity";
    /**
     * Toolbar
     **/
    Toolbar actionBar;
    /**
     * Back button
     **/
    ImageButton back;

    /**
     * Resend code layout
     **/
    LinearLayout ll_resend_code;

    /**
     * Buttons
     **/
    Button button_reset, button_submit, button_updatenewpwd, btn_resend;

    /**
     * TextView's
     **/
    TextView toolbar_title, txt_verifydetails, txt_backtologin, tv_resend_counter, txt_forgetdetails;
    /**
     * EditText's
     **/
    EditText editText_forgetuseremailID, editText_verify, editText_verification;
    /**
     * String result's
     **/
    public static String mailid_result, verificode_result, newmaild_result;
    /**
     * Forgot password layouts
     **/
    RelativeLayout rl_forgotpwd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        /*
          Action bar
         */
        actionBar = (Toolbar) findViewById(R.id.action_bar_profile);
        /*
          Setting title text
         */
        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(R.string.reset);
        /*
          Text view initializations
         */

        txt_verifydetails = (TextView) findViewById(R.id.txt_verifydetails);
        txt_backtologin = (TextView) findViewById(R.id.txt_backtologin);
        txt_forgetdetails = (TextView) findViewById(R.id.txt_forgetdetails);
        tv_resend_counter = (TextView) findViewById(R.id.tv_resend_counter);


        /*
          EditText initializations
         */

        editText_forgetuseremailID = (EditText) findViewById(R.id.editText_forgetuseremailID);

        editText_verify = (EditText) findViewById(R.id.editText_verify);


        /*
          RelativeLayout initialization
         */
        rl_forgotpwd = (RelativeLayout) findViewById(R.id.rl_forgotpwd);
        txt_backtologin.setOnClickListener(this);

        /*
          On click listeners
         */
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        button_reset = (Button) findViewById(R.id.button_reset);
        btn_resend = (Button) findViewById(R.id.btn_resend);
        ll_resend_code = (LinearLayout) findViewById(R.id.ll_resend_code);

        button_reset.setOnClickListener(this);
        btn_resend.setOnClickListener(this);

        button_submit = (Button) findViewById(R.id.button_submit);
        button_submit.setOnClickListener(this);


        button_updatenewpwd = (Button) findViewById(R.id.button_updatenewpwd);
        button_updatenewpwd.setOnClickListener(this);


    }

    /**
     * On click listeners
     **/
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            /*
              Back button on click listeners

             */
            case R.id.back:
                finish();
                break;
            /*
            Text back button on click listeners
            */
            case R.id.txt_backtologin:
                finish();
                break;
            /*
              Back button on click listeners

             */
            case R.id.button_reset:
            /*
             Validation

             */
                if (validate()) {
                     /*
                       Internet connectivity checking

                     */
                    if (Utility.isConnected(this)) {

//                        ll_resend_code.setVisibility(View.VISIBLE);



                    /*
                       Calling update forgot password API

                     */
                        update_forgot_password();


                    } else {


                        Toast.makeText(this, "ResetPassword Failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();


                    }
                } else {



                }

                break;
              /*
              Button submit on click listeners
              */
            case R.id.button_submit:
             /*
              Validate verification code
              */
                if (validateVerifyCode()) {

                    if (Utility.isConnected(this)) {

                        button_submit.setEnabled(false);
                /*
                  Calling API for Validate verify code
                 */
                        updateVerifyCode();


                    } else {


                        Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();


                    }
                } else {



                }

                break;
             /*
              Button resend code
              */
            case R.id.btn_resend:
                /*
              Validation
              */
                if (validate()) {

                    if (Utility.isConnected(this)) {

                        btn_resend.setEnabled(false);

               /*
                Calling update forgot password API
              */

                        update_forgot_password();


                    } else {


                        Toast.makeText(this, "ResetPassword Failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();


                    }
                } else {

//                    Toast.makeText(this, "Please provide a valid email address", Toast.LENGTH_SHORT).show();


                }
                break;


        }

    }



    /**
    Validation
      **/

    private boolean validate() {

        mailid_result = editText_forgetuseremailID.getText().toString();

        boolean valid = true;

        if (!isValidEmail(mailid_result)) {
            editText_forgetuseremailID.requestFocus();
            if (mailid_result.isEmpty()) {
                editText_forgetuseremailID.setError("Please Enter Your Email-Id.");
                valid = false;

            } else {
                editText_forgetuseremailID.setError("Email address not valid");
                valid = false;

            }
        } else {
            editText_forgetuseremailID.setError(null);
        }


        return valid;
    }

    /**
     Validation - Verify code
     **/

    private boolean validateVerifyCode() {

        String veriCode_result = editText_verify.getText().toString();

        Log.e(TAG, "validateVerifyCode:String " + veriCode_result);
        Log.e(TAG, "validateVerifyCode: length" + veriCode_result.length());

        boolean valid = true;


        if (veriCode_result.isEmpty()) {

            valid = false;

            editText_verify.setError("Please enter the verification code");

            editText_forgetuseremailID.requestFocus();

        } else {

            if (veriCode_result.length() == 6) {
                editText_verify.setError(null);
                valid = true;

            } else {

                editText_verify.setError("Please enter the valid code");
                valid = false;
                editText_forgetuseremailID.requestFocus();


            }
        }


        return valid;
    }
    /**
    Calling API for update forgot password
     **/

    private void update_forgot_password() {


        final AlertDialog dialog1 = new SpotsDialog.Builder().setContext(forgotPasswordActivity.this).setMessage("").build();

        dialog1.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        mailid_result = editText_forgetuseremailID.getText().toString();

        Log.e(TAG, "mailid" + mailid_result);


        Call<Timeline_create_output_pojo> call = apiService.update_forgot_password("update_forgot_password", mailid_result);

        call.enqueue(new Callback<Timeline_create_output_pojo>() {

            @Override
            public void onResponse(Call<Timeline_create_output_pojo> call, Response<Timeline_create_output_pojo> response) {


                if (response.body() != null) {


                    if (response.body().getResponseCode() == 1) {

                        ll_resend_code.setVisibility(View.VISIBLE);
                        editText_forgetuseremailID.setError(null);
                        toolbar_title.setText(R.string.email_veri_title);
                        editText_forgetuseremailID.setVisibility(View.GONE);
                        button_reset.setVisibility(View.GONE);
                        editText_verify.setVisibility(View.VISIBLE);
                        button_submit.setVisibility(View.VISIBLE);
                        txt_verifydetails.setVisibility(View.VISIBLE);
                        txt_forgetdetails.setVisibility(View.GONE);
                        dialog1.dismiss();


                    } else {


                        editText_forgetuseremailID.setError("Your email id is not registered");
                        ll_resend_code.setVisibility(View.VISIBLE);
                        ll_resend_code.setVisibility(View.GONE);
                        dialog1.dismiss();


                    }

                } else {

                    Toast.makeText(forgotPasswordActivity.this, "Something went wrong please try again later.", Toast.LENGTH_SHORT).show();
                    dialog1.dismiss();

                }


            }

            @Override
            public void onFailure(Call<Timeline_create_output_pojo> call, Throwable t) {
                //Error
                Log.e("FailureError", "" + t.getMessage());
                dialog1.dismiss();

            }
        });

        /*
         Counter timer for resending the verification code
         */
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {

                tv_resend_counter.setVisibility(View.VISIBLE);
                tv_resend_counter.setText("Resend in : " + millisUntilFinished / 1000);

            }

            public void onFinish() {

                tv_resend_counter.setVisibility(View.GONE);
                btn_resend.setEnabled(true);

            }

        }.start();


    }

    /**
     Calling API for update verify code
     **/
    private void updateVerifyCode() {


        if (editText_verify.getText().toString().length() == 6) {

            final AlertDialog dialog1 = new SpotsDialog.Builder().setContext(forgotPasswordActivity.this).setMessage("").build();
            dialog1.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            verificode_result = editText_verify.getText().toString();

            Log.e(TAG, "verification_code-->" + verificode_result);

            Call<Timeline_create_output_pojo> call = apiService.update_verifycode("update_verifycode", mailid_result, Integer.parseInt(verificode_result));

            call.enqueue(new Callback<Timeline_create_output_pojo>() {
                @Override
                public void onResponse(Call<Timeline_create_output_pojo> call, Response<Timeline_create_output_pojo> response) {


                    if (response.body() != null) {

                        Log.e("ForgotPassword", "VerificationCodeResponse:" + response.body().getResponseMessage());

                        if (response.body().getResponseCode() == 1) {

                            dialog1.dismiss();
                            startActivity(new Intent(getApplicationContext(), Reset_new_password_Activity.class));
                            finish();

                        } else {

                            editText_verify.setError("Invalid verification code");
                            dialog1.dismiss();

                        }

                    } else {

                        Toast.makeText(forgotPasswordActivity.this, "Something went wrong please try again later.", Toast.LENGTH_SHORT).show();
                        dialog1.dismiss();

                    }


                }

                @Override
                public void onFailure(Call<Timeline_create_output_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    dialog1.dismiss();

                }
            });

        } else {

            editText_verify.setError("Please enter 6 digit code");

        }


    }


}
