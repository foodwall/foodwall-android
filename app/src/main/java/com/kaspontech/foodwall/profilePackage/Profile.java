package com.kaspontech.foodwall.profilePackage;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.REST_API.ProgressRequestBody;
import com.kaspontech.foodwall.adapters.TimeLine.Image_adapter;
import com.kaspontech.foodwall.adapters.TimeLine.Profile_adapter;
import com.kaspontech.foodwall.adapters.TimeLine.Timeline_all_image_adapter;
import com.kaspontech.foodwall.bucketlistpackage.CreateBucketlistActivty;
 import com.kaspontech.foodwall.followingPackage.Followerlist_Activity;
import com.kaspontech.foodwall.followingPackage.Following_listActivity;
import com.kaspontech.foodwall.loginPackage.Login;
import com.kaspontech.foodwall.menuPackage.UserMenu;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_timeline_image_all;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_timeline_output_response;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.User_image_input_response;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.User_image_update_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_profile_details_Pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.New_Getting_timeline_all_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.New_timeline_image_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Profile_output_pojo;
import com.kaspontech.foodwall.profilePackage.profileTabFragments.HistoricalImagesFragment;
import com.kaspontech.foodwall.profilePackage.profileTabFragments.ReviewsImageFragment;
import com.kaspontech.foodwall.profilePackage.profileTabFragments.TimelineImagesFragment;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.profilePackage.profileTabFragments.TimelineImagesFragment.allimagelist;

/**
 * A simple {@link Fragment} subclass.
 */
public class Profile extends _SwipeActivityClass implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener,
        ProgressRequestBody.UploadCallbacks {

    // Action Bar
    Toolbar actionBar;
    ImageButton back;
    Context context;
    TabLayout profileTablayout;
    public static CircleImageView img_profilepic;
    Pref_storage pref_storage;
    TextView user_name;
    TextView txt_total_post;
    TextView txt_total_followers;
    TextView txt_total_followings;
    TextView user_bio;
    Button edit_profile;
    ImageButton more_profile;

    Uri imageUri;


    private static final String TAG = "Profile";

    private static final int REQUEST_CAMERA_PERMISSION = 0;
    private static final int REQUEST_PERMISSION_SETTING = 5;

    private int REQUEST_CAMERA = 0;
    private int SELECT_FILE = 1;
    LinearLayout ll_post;
    LinearLayout ll_followers;
    LinearLayout ll_folowing;
    RelativeLayout rl_profile_update;
    RelativeLayout rl_profile;
    User_image_input_response userImageInputResponse;

    private static final int PICK_FROM_GALLERY = 1;
    public GoogleApiClient mGoogleApiClient;
    public GoogleSignInClient googleSignInOptions;
    ProgressBar progress_dialognew;
    GridView gridView;


    // Fragments
    Fragment timelineImagesFragment;
    Fragment reviewsImageFragment;
    Fragment historicalImagesFragment;


    ImageView img_sts_update;


    Image_adapter image_adapter;

     String username;
    String firstName1;
    String lastName1;
    String password;
    String email1;
    String gender;
    String userId;
    String logintype;
    String dob;
    String picture;
    String pro_status;
    String total_post;
    String follwing;
    String followers;
    String bio_description;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Vishnu Balaji
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
        /*Facebook initialization*/

        FacebookSdk.sdkInitialize(getApplicationContext());


        actionBar = (Toolbar) findViewById(R.id.action_bar_profile);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);
        gridView = (GridView) findViewById(R.id.gridview_profile);

        profileTablayout = (TabLayout) findViewById(R.id.tab_profile);
        img_profilepic = (CircleImageView) findViewById(R.id.img_profilepic);
        img_profilepic.setOnClickListener(this);

        img_sts_update = (ImageView) findViewById(R.id.img_sts_update);
        img_sts_update.setOnClickListener(this);
        user_name = (TextView) findViewById(R.id.user_name);
        user_bio = (TextView) findViewById(R.id.user_bio);
        txt_total_post = (TextView) findViewById(R.id.txt_total_post);
        txt_total_followers = (TextView) findViewById(R.id.txt_total_followers);
        txt_total_followings = (TextView) findViewById(R.id.txt_total_followings);
        edit_profile = (Button) findViewById(R.id.edit_profile);
        edit_profile.setOnClickListener(this);

        more_profile = (ImageButton) findViewById(R.id.more_profile);
        more_profile.setOnClickListener(this);

        rl_profile_update = (RelativeLayout) findViewById(R.id.rl_profile_update);
        rl_profile = (RelativeLayout) findViewById(R.id.rl_profile);
        rl_profile_update.setOnClickListener(this);

        ll_post = (LinearLayout) findViewById(R.id.ll_post);
        ll_post.setOnClickListener(this);
        ll_followers = (LinearLayout) findViewById(R.id.ll_followers);
        ll_followers.setOnClickListener(this);
        ll_folowing = (LinearLayout) findViewById(R.id.ll_folowing);
        ll_folowing.setOnClickListener(this);
        image_adapter = new Image_adapter(this);

        progress_dialognew = (ProgressBar) findViewById(R.id.progress_dialog_profile);


        profileTablayout.addTab(profileTablayout.newTab().setIcon(R.drawable.ic_historical_map));
        profileTablayout.addTab(profileTablayout.newTab().setIcon(R.drawable.ic_gallery_));
        profileTablayout.addTab(profileTablayout.newTab().setIcon(R.drawable.ic_review_photos));

        timelineImagesFragment = new TimelineImagesFragment();
        reviewsImageFragment = new ReviewsImageFragment();
        historicalImagesFragment = new HistoricalImagesFragment();

        /**
         * Googleapiclient for google signin logout
         *
         * */
        initGoogleAPIClient();

        final String userid = Pref_storage.getDetail(Profile.this, "userId");


        Bundle bundle = new Bundle();
        bundle.putString("userid", userid);
        historicalImagesFragment.setArguments(bundle);
        replaceFragment(historicalImagesFragment);



        profileTablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {

                    case 0:


                        Bundle bundle = new Bundle();
                        bundle.putString("userid", userid);
                        historicalImagesFragment.setArguments(bundle);
                        replaceFragment(historicalImagesFragment);

                        break;

                    case 1:

                        Bundle bundle1 = new Bundle();
                        bundle1.putString("userid", userid);
                        timelineImagesFragment.setArguments(bundle1);
                        replaceFragment(timelineImagesFragment);

                        break;

                    case 2:

                        Bundle bundle2 = new Bundle();
                        bundle2.putString("userid", userid);
                        reviewsImageFragment.setArguments(bundle2);
                        replaceFragment(reviewsImageFragment);

                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        /*Updating profile*/
        updateProfile();

        try {
            rl_profile.setVisibility(View.GONE);
            progress_dialognew.setVisibility(View.VISIBLE);

            get_profile();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSwipeRight() {
       /*Intent intent=new Intent(this,Home.class);
         this.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
         startActivity(intent);
         intent.putExtra("profile","1");*/
        finish();

    }

    @Override
    protected void onSwipeLeft() {

    }


    /*Fragment replacing*/

    public void replaceFragment(Fragment fragment) {

        try {

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.profile_container, fragment);
            ft.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*Updating profile API call*/

    private void updateProfile() {

        if (Utility.isConnected(getApplicationContext())) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String userId = Pref_storage.getDetail(Profile.this, "userId");

            Call<Profile_output_pojo> call = apiService.get_profile("get_profile", Integer.parseInt(userId), Integer.parseInt(userId));


            call.enqueue(new Callback<Profile_output_pojo>() {
                @Override
                public void onResponse(Call<Profile_output_pojo> call, Response<Profile_output_pojo> response) {

                    if (response.body() != null && response.body().getResponseCode() == 1) {


                        for (int j = 0; j < response.body().getData().size(); j++) {


                            picture = response.body().getData().get(j).getPicture();

                            Utility.picassoImageLoader(picture, 1, img_profilepic, getApplicationContext());


                        }
                    }
                }

                @Override
                public void onFailure(Call<Profile_output_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } else {

            Toast.makeText(context, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }


    }


    /*User profile calling API*/
    private void get_profile() {
        if (Utility.isConnected(getApplicationContext())) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String user_id = Pref_storage.getDetail(Profile.this, "userId");

            Call<Profile_output_pojo> call = apiService.get_profile("get_profile", Integer.parseInt(user_id), Integer.parseInt(user_id));


            call.enqueue(new Callback<Profile_output_pojo>() {
                @Override
                public void onResponse(Call<Profile_output_pojo> call, Response<Profile_output_pojo> response) {

                    if (response.body() != null && response.body().getResponseCode() == 1) {


                        for (int j = 0; j < response.body().getData().size(); j++) {


                            logintype = response.body().getData().get(j).getLoginType();

                            firstName1 = response.body().getData().get(j).getFirstName();
                            lastName1 = response.body().getData().get(j).getLastName();
                            password = response.body().getData().get(j).getPassword();

                            email1 = response.body().getData().get(j).getEmail();
                            gender = response.body().getData().get(j).getGender();
                            dob = response.body().getData().get(j).getDob();
                            picture = response.body().getData().get(j).getPicture();

                            Pref_storage.setDetail(Profile.this, "picture_user_login", picture);

                            total_post = response.body().getData().get(j).getTotalPosts();
                            follwing = response.body().getData().get(j).getTotalFollowings();
                            followers = response.body().getData().get(j).getTotalFollowers();
                            bio_description = response.body().getData().get(j).getBioDescription();
                            Log.e("bio_description", "onResponse: " + bio_description);
                            pro_status = response.body().getData().get(j).getProfileStatus();
                            username = response.body().getData().get(j).getUsername();


                            try {

                                user_name.setText(firstName1.concat(" ").concat(lastName1));
                                if (total_post.equalsIgnoreCase("0")) {
                                    txt_total_post.setText("-");

                                } else {
                                    txt_total_post.setText(total_post);

                                }

                                if (followers.equalsIgnoreCase("0")) {
                                    txt_total_followers.setText("-");

                                } else {
                                    txt_total_followers.setText(followers);

                                }
                                if (follwing.equalsIgnoreCase("0")) {
                                    txt_total_followings.setText("-");

                                } else {
                                    txt_total_followings.setText(follwing);

                                }


                                if (bio_description.equalsIgnoreCase("") || bio_description.equalsIgnoreCase("0") || bio_description.equalsIgnoreCase("\"\"")) {
                                    user_bio.setText("");
                                    user_bio.setVisibility(View.GONE);

                                } else {
                                    user_bio.setText(bio_description.replace("\"", ""));
                                    user_bio.setVisibility(View.VISIBLE);

                                }


                                rl_profile.setVisibility(View.VISIBLE);
                                progress_dialognew.setVisibility(View.GONE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    }
                }

                @Override
                public void onFailure(Call<Profile_output_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });

        } else {
            Toast.makeText(context, getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }

    }


    ///  Updating user profile image
    private void update_user_profile_image() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        String user_id = Pref_storage.getDetail(this, "userId");

        String user = Pref_storage.getDetail(this, "UserImage");


        try {

            File file = new File(user);

            File compressedImageFile = null;

            compressedImageFile = new Compressor(this).setQuality(100).compressToFile(file);

            ProgressRequestBody fileBody = new ProgressRequestBody(compressedImageFile, this);
            MultipartBody.Part image = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), fileBody);


            Call<User_image_update_pojo> call = apiService.update_user_profile_image("update_user_profile_image", Integer.parseInt(user_id), image);


            call.enqueue(new Callback<User_image_update_pojo>() {
                @Override
                public void onResponse(Call<User_image_update_pojo> call, Response<User_image_update_pojo> response) {

                    if (response.body().getResponseCode() == 1) {

                        for (int j = 0; j < response.body().getData().size(); j++) {
                            userImageInputResponse = new User_image_input_response("");


                            String responsenew = response.body().getData().get(j).getStatus();

                            if (Integer.parseInt(responsenew) == 1) {
                                Toast.makeText(Profile.this, "Profile picture has been updated successfully", Toast.LENGTH_SHORT).show();

                                get_profile();


                            } else {
                                Snackbar snackbar = Snackbar.make(rl_profile, "Profile Image update failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                                snackbar.setActionTextColor(Color.RED);
                                View view1 = snackbar.getView();
                                TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                                textview.setTextColor(Color.WHITE);
                                snackbar.show();
                            }
                        }


                    }
                }

                @Override
                public void onFailure(Call<User_image_update_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        /*Getting profile details calling API*/
        get_profile();

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.edit_profile:

                Intent i = new Intent(Profile.this, editProfileActivity.class);
                i.putExtra("firstname", firstName1);
                i.putExtra("userId", userId);

                i.putExtra("lastname", lastName1);
                i.putExtra("password", password);
                i.putExtra("email", email1);
                i.putExtra("gender", gender);
                i.putExtra("dob", dob);
                i.putExtra("picture", picture);
                i.putExtra("logintype", logintype);
                i.putExtra("bio_description", bio_description);
                i.putExtra("pro_status", pro_status);
                i.putExtra("username", username);

                startActivity(i);
                finish();


                break;

            case R.id.img_profilepic:

                camera_galleryalert();

                break;

            case R.id.img_sts_update:

                camera_galleryalert();

                break;


            case R.id.more_profile:
                CustomDialogClass ccd = new CustomDialogClass(this);
                ccd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ccd.setCancelable(true);
                ccd.show();


                break;

            case R.id.back:

                Intent intent = new Intent(Profile.this, UserMenu.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               // intent.putExtra("redirect", "profile");
                startActivity(intent);
                 finish();
                break;

            case R.id.ll_post:
                break;

            case R.id.ll_followers:
                startActivity(new Intent(this, Followerlist_Activity.class));
                break;

            case R.id.ll_folowing:

                startActivity(new Intent(this, Following_listActivity.class));

                break;

        }

    }

    @Override
    public void onProgressUpdate(int percentage) {
        Log.e(TAG, "onProgressUpdate: --->" + percentage);

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }


    public class CustomDialogClass extends Dialog implements
            View.OnClickListener {

        public AppCompatActivity c;
        public Dialog d;
        public AppCompatButton btn_Ok;
        TextView txt_editprofile, txt_changepasswrd, txt_about, txt_logout;

        CustomDialogClass(AppCompatActivity a) {
            super(a);
            // TODO Auto-generated constructor stub
            c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_dialog_edit_more);

            initGoogleAPIClient();

            txt_editprofile = (TextView) findViewById(R.id.txt_editprofile);
            txt_editprofile.setOnClickListener(this);

            txt_changepasswrd = (TextView) findViewById(R.id.txt_changepasswrd);
            txt_changepasswrd.setOnClickListener(this);

            txt_about = (TextView) findViewById(R.id.txt_about);
            txt_about.setOnClickListener(this);

            txt_logout = (TextView) findViewById(R.id.txt_logout);
            txt_logout.setOnClickListener(this);


            if (Integer.parseInt(logintype) == 1 || Integer.parseInt(logintype) == 2) {
                txt_changepasswrd.setVisibility(View.GONE);

            } else {
                txt_changepasswrd.setVisibility(View.VISIBLE);
            }


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {


                case R.id.txt_editprofile:
                    Intent i = new Intent(Profile.this, editProfileActivity.class);
                    i.putExtra("firstname", firstName1);
                    i.putExtra("lastname", lastName1);
                    i.putExtra("password", password);
                    i.putExtra("email", email1);
                    i.putExtra("gender", gender);
                    i.putExtra("dob", dob);
                    i.putExtra("logintype", logintype);
                    i.putExtra("picture", picture);
                    i.putExtra("bio_description", bio_description);
                    i.putExtra("pro_status", pro_status);
                    i.putExtra("username", username);

                    startActivity(i);
                    /* overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);*/

                    dismiss();
                    break;
                case R.id.txt_changepasswrd:
                    startActivity(new Intent(Profile.this, Password_change_Activity.class));
                    /*overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);*/
                    dismiss();
                    break;

                case R.id.txt_about:
                    startActivity(new Intent(Profile.this, Version_info_activity.class));
                    /*  overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);*/
                    dismiss();
                    break;

                case R.id.txt_logout:
                    new SweetAlertDialog(Profile.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to logout?")
                            .setConfirmText("Yes")
                            .setCancelText("No")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                    int loginType = Integer.parseInt(Pref_storage.getDetail(Profile.this, "loginType"));

                                    switch (loginType) {

                                        case 1:

                                            // facebook

                                            LoginManager.getInstance().logOut();
                                            goToLogin();

                                            break;

                                        case 2:

                                            // Google+

                                            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                                    new ResultCallback<Status>() {
                                                        @Override
                                                        public void onResult(Status status) {
                                                            // ...
                                                            goToLogin();

                                                        }
                                                    });

                                            break;
                                        case 3:

                                            // sign up

                                            goToLogin();

                                            break;
                                    }


                                }
                            })
                            .show();

                    dismiss();
                    break;

                default:
                    break;
            }
        }
    }


    public void clearApplicationData() {
        File cacheDirectory = getCacheDir();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {
            String[] fileNames = applicationDirectory.list();
            for (String fileName : fileNames) {
                if (!fileName.equals("lib")) {
                    deleteFile(new File(applicationDirectory, fileName));
                }
            }
        }
    }

    public static boolean deleteFile(File file) {
        boolean deletedAll = true;
        if (file != null) {
            if (file.isDirectory()) {
                String[] children = file.list();
                for (int i = 0; i < children.length; i++) {
                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;
                }
            } else {
                deletedAll = file.delete();
            }
        }

        return deletedAll;
    }

    private void galleryIntent() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_FILE);
    }

    private void cameraIntent() {

        if (ActivityCompat.checkSelfPermission(Profile.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(Profile.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Profile.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, REQUEST_CAMERA);

        } else {


            ActivityCompat.requestPermissions(Profile.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {

        int permissionSize = permissions.length;

        Log.e(TAG, "onRequestPermissionsResult:" + "requestCode->" + requestCode);

        switch (requestCode) {

            case REQUEST_CAMERA_PERMISSION:

                for (int i = 0; i < permissionSize; i++) {


                    if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission granted");

                    } else if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission denied");
                        cameraStorageGrantRequest();
                    }

                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");

                        if (ActivityCompat.checkSelfPermission(Profile.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraIntent();

                        }


                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");

                        if (ActivityCompat.checkSelfPermission(Profile.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraStorageGrantRequest();

                        }

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

            case PICK_FROM_GALLERY:

                for (int i = 0; i < permissionSize; i++) {


                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");
                        galleryIntent();

                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");
                        cameraStorageGrantRequest();

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                img_profilepic.setImageResource(0);
                String result = data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == REQUEST_CAMERA) {


                Bitmap thumbnail = null;
                try {
                    thumbnail = getThumbnail(imageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("CameraCaptureError", "CameraCaptureError->" + e.getMessage());
                }

                String folder_main = "FoodWall";

                File f = new File(Environment.getExternalStorageDirectory(), folder_main);
                if (!f.exists()) {
                    f.mkdirs();
                }

                File newFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                Log.e("ImageFile", "ImageFile->" + newFile);

                try {

                    FileOutputStream fileOutputStream = new FileOutputStream(newFile);
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    String imagepath = String.valueOf(newFile);

                    Pref_storage.setDetail(this, "UserImage", imagepath);


                    Utility.picassoImageLoader(imagepath, 1, img_profilepic, getApplicationContext());


                    Log.e("MyPath", "MyImagePath->" + imagepath);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ///  Updating user profile image

                update_user_profile_image();


            } else if (requestCode == SELECT_FILE) {

                try {

                    img_profilepic.setImageResource(0);
                    onSelectFromGalleryResult(data);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*Camera Storage Grant Request*/
    private void cameraStorageGrantRequest() {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                Profile.this);

        builder.setTitle("Food Wall would like to access your camera and storage")
                .setMessage("You previously chose not to use Android's camera or storage with Food Wall. To set your profile pic, allow camera and storage permission in App Settings. Click Ok to allow.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // permission dialog
                        dialog.dismiss();
                        try {

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();


    }


    public Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException {

        InputStream input = Profile.this.getContentResolver().openInputStream(uri);
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 800) ? (originalSize / 800) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Uri selectedImage = data.getData();


        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor c = getApplicationContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);

        c.moveToFirst();

        int columnIndex = c.getColumnIndex(filePathColumn[0]);
        String picturePath = c.getString(columnIndex);
        c.close();

        img_profilepic.setImageURI(Uri.parse(picturePath));

        Pref_storage.setDetail(getApplicationContext(), "UserImage", picturePath);


        ///  Updating user profile image

        update_user_profile_image();


    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getApplicationContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;

    }

    //Camera Alert
    public void camera_galleryalert() {

        final CharSequence[] items = {"Camera", "Select from Gallery",
                "Cancel"};

        AlertDialog.Builder front_builder = new AlertDialog.Builder(this);
        front_builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (items[which].equals("Camera")) {

                    try {
                        if (ActivityCompat.checkSelfPermission(Profile.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(Profile.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(Profile.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);

                        } else {

                            cameraIntent();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (items[which].equals("Select from Gallery")) {

                    try {
                        if (ActivityCompat.checkSelfPermission(Profile.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(Profile.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);

                        } else {

                            galleryIntent();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (items[which].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        front_builder.show();
    }

    @SuppressLint("RestrictedApi")
    private void signOut() {

        googleSignInOptions.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                    }
                });
    }

    public void more_setting() {

        final CharSequence[] items = {"Edit Profile", "Change Password", "About",
                "Logout"};

        AlertDialog.Builder front_builder = new AlertDialog.Builder(this);
        front_builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (items[which].equals("Edit Profile")) {

                    Intent i = new Intent(Profile.this, editProfileActivity.class);
                    i.putExtra("firstname", firstName1);
                    i.putExtra("lastname", lastName1);
                    i.putExtra("password", password);
                    i.putExtra("email", email1);
                    i.putExtra("gender", gender);
                    i.putExtra("dob", dob);
                    i.putExtra("picture", picture);

                    startActivity(i);


                } else if (items[which].equals("Change Password")) {
                    Intent i = new Intent(Profile.this, Password_change_Activity.class);
                    startActivity(i);


                } else if (items[which].equals("About")) {
                    dialog.dismiss();
                } else if (items[which].equals("Cancel")) {
                    dialog.dismiss();
                } else if (items[which].equals("Logout")) {

                    new SweetAlertDialog(Profile.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure want to logout?")
                            .setConfirmText("Yes")
                            .setCancelText("No")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                    int loginType = Integer.parseInt(Pref_storage.getDetail(Profile.this, "loginType"));

                                    switch (loginType) {

                                        case 1:

                                            // facebook

                                            LoginManager.getInstance().logOut();
                                            goToLogin();

                                            break;

                                        case 2:

                                            // Google+

                                            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                                    new ResultCallback<Status>() {
                                                        @Override
                                                        public void onResult(Status status) {
                                                            // ...
                                                            goToLogin();

                                                        }
                                                    });

                                            break;
                                        case 3:

                                            // sign up

                                            goToLogin();

                                            break;
                                    }


                                }
                            })
                            .show();


                }
            }
        });
        front_builder.show();
    }


    /*Redirect to login page*/
    private void goToLogin() {

        deleteCache(Profile.this);
        Pref_storage.clearData();
//        ((ActivityManager) context.getSystemService(ACTIVITY_SERVICE)).clearApplicationUserData();
        Intent intent = new Intent(Profile.this, Login.class);
        intent.setFlags(/*Intent.FLAG_ACTIVITY_CLEAR_TASK |*/ Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    /*Deleting cache*/

    public static void deleteCache(Context context) {

        try {

            File dir = context.getCacheDir();
            deleteDir(dir);

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    /*Deleting directory*/
    public static boolean deleteDir(File dir) {

        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }

    }


    /**
     * Initiate Google API Client
     */
    private void initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(Profile.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(Profile.this, "No internet connection", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        initGoogleAPIClient();
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        super.onStart();
    }






}


