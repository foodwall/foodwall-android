package com.kaspontech.foodwall.profilePackage.profileTabFragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.modelclasses.Review_pojo.ReviewCommentsPojo;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
import com.kaspontech.foodwall.reviewpackage.profileReviewAdapter.ProfileReviewImageUserAdapter;
import com.kaspontech.foodwall.reviewpackage.ProfileReviewPojo.GetReviewProfileImage;
import com.kaspontech.foodwall.reviewpackage.ProfileReviewPojo.GetReviewProfileImageOutput;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewInputAllPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewOutputResponsePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updater_adapter.GetHotelDetailsAdapter;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.Objects;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewsImageFragment extends Fragment implements GetHotelDetailsAdapter.onCommentsPostListener {

    Context context;
    View view;

    String userID, fromwhich;

    LinearLayout ll_no_post;

    RecyclerView review_images_recylerview;
    ProfileReviewImageUserAdapter profileReviewImageUserAdapter;
    ArrayList<GetReviewProfileImage> getReviewProfileImageArrayList = new ArrayList<>();

    LinearLayout frame_layout;

    // getallHotelReviewPojoArrayList
    ArrayList<ReviewInputAllPojo> getallHotelReviewPojoArrayList = new ArrayList<>();

    /*// topDishImagePojoArrayList
    ArrayList<ReviewTopDishPojo> topDishImagePojoArrayList = new ArrayList<>();

    //avoidDishImagePojoArrayList
    ArrayList<ReviewAvoidDishPojo> avoidDishImagePojoArrayList = new ArrayList<>();

    //ambiImagePojoArrayList
    ArrayList<ReviewAmbiImagePojo> ambiImagePojoArrayList = new ArrayList<>();*/


    RelativeLayout rl_hotel;


    int top_count, avoid_count, ambi_image_count, topdishimage_count, avoiddishimage_count,packCount;

    //    Get_all_hotel_review_adapter get_all_hotel_review_adapter;
    GetHotelDetailsAdapter getHotelDetailsAdapter;

    String txt_hotelname, user_id, hotel_id, txt_hotel_address,
            txt_review_username, txt_review_userlastname, userimage, txt_review_follow, txt_review_like, txt_review_comment,
            txt_review_shares, txt_review_caption, hotel_review_rating, hotel_user_rating, total_followers, total_followings,
            hotelId, cat_type, veg_nonveg, revratID, ambiance, taste, total_package, total_timedelivery, service, valuemoney,
            createdby, createdon, googleID, placeID, opentimes, delivery_mode, dishname, topdishimage, topdishimage_name, avoiddishimage_name, avoiddishomage, ambiimage, dishtoavoid,
            category_type, latitude, longitude, phone, package_value, time_delivery, foodexp, photo_reference, emailID, likes_hotelID, revrathotel_likes,
            followingID, totalreview,redirect_url, total_ambiance, total_taste, total_service, modeid, company_name, total_value, total_good, total_bad, total_good_bad_user;


    int top_dish_img_count,
            avoid_dish_img_count, bucketlist;


    public ReviewsImageFragment() {
        // Required empty public constructor
    }

    TextView tv_view_all_comments;

    private ProgressBar comments_progressbar;

    private EmojiconEditText review_comment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_reviews_images, container, false);
        context = view.getContext();


        review_images_recylerview = (RecyclerView) view.findViewById(R.id.my_bucket_recyclerview);
        ll_no_post = (LinearLayout) view.findViewById(R.id.ll_no_post);

        frame_layout = (LinearLayout) view.findViewById(R.id.frame_layout);




        if (getArguments() != null) {
            userID = getArguments().getString("userid");
        }

        get_hotel_review_user(userID);


        return view;
    }


    public void getReviewImages() {

        getReviewProfileImageArrayList.clear();

        if (isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<GetReviewProfileImageOutput> call = apiService.getProfileReviewImagesUser("get_hotel_review_ambiance_image", Integer.parseInt(userID));

            call.enqueue(new Callback<GetReviewProfileImageOutput>() {
                @Override
                public void onResponse(Call<GetReviewProfileImageOutput> call, Response<GetReviewProfileImageOutput> response) {

                    if (response.body().getResponseMessage().equals("success")) {

                        ll_no_post.setVisibility(View.GONE);


                        if(response.body().getData().size()>0) {
                            for (int j = 0; j < response.body().getData().size(); j++) {


                                String photoId = response.body().getData().get(j).getPhotoId();
                                String reviewid = response.body().getData().get(j).getReviewid();
                                String userid = response.body().getData().get(j).getUserid();
                                String ambImage = response.body().getData().get(j).getAmbImage();


                                GetReviewProfileImage getReviewProfileImage = new GetReviewProfileImage(photoId,
                                        reviewid, userid, ambImage);
                                getReviewProfileImageArrayList.add(getReviewProfileImage);


                            }

                            Activity activity = getActivity();

                            if (isAdded() && activity != null) {

                                profileReviewImageUserAdapter = new ProfileReviewImageUserAdapter(context, getReviewProfileImageArrayList);
                                review_images_recylerview.setLayoutManager(new GridLayoutManager(context, 3));
                                review_images_recylerview.setAdapter(profileReviewImageUserAdapter);
                                review_images_recylerview.setNestedScrollingEnabled(true);
                                profileReviewImageUserAdapter.notifyDataSetChanged();
                            }
                        }else
                        {
                            ll_no_post.setVisibility(View.VISIBLE);

                        }


                    } else {

                        ll_no_post.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<GetReviewProfileImageOutput> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    ll_no_post.setVisibility(View.VISIBLE);

                }
            });

        } else {
            Toast.makeText(getContext(), "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }


    }


    private void get_hotel_review_user(String userid) {


        if (isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_user("get_hotel_review_user", Integer.parseInt(userid));
            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {


                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {

                        getallHotelReviewPojoArrayList.clear();

                        if (isAdded()) {
                            frame_layout.setBackgroundColor(context.getResources().getColor(R.color.page_background));
                        }


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());

                            try {


                                hotelId = response.body().getData().get(j).getHotelId();
                                cat_type = response.body().getData().get(j).getCategoryType();
                                time_delivery = response.body().getData().get(j).getTimedelivery();
                                redirect_url = response.body().getData().get(j).getRedirect_url();
                                veg_nonveg = response.body().getData().get(j).getVegNonveg();
                                revratID = response.body().getData().get(j).getRevratId();
                                modeid = response.body().getData().get(j).getMode_id();
                                company_name = response.body().getData().get(j).getComp_name();

                                ambiance = response.body().getData().get(j).getAmbiance();
                                taste = response.body().getData().get(j).getTaste();
                                total_package = response.body().getData().get(j).getTotalPackage();
                                total_timedelivery = response.body().getData().get(j).getTotalTimedelivery();
                                service = response.body().getData().get(j).getService();
                                valuemoney = response.body().getData().get(j).getValueMoney();
                                createdby = response.body().getData().get(j).getCreatedBy();
                                createdon = response.body().getData().get(j).getCreatedOn();
                                googleID = response.body().getData().get(j).getGoogleId();
                                placeID = response.body().getData().get(j).getPlaceId();
                                opentimes = response.body().getData().get(j).getOpenTimes();
                                category_type = response.body().getData().get(j).getCategoryType();
                                latitude = response.body().getData().get(j).getLatitude();
                                longitude = response.body().getData().get(j).getLongitude();
                                phone = response.body().getData().get(j).getPhone();
                                photo_reference = response.body().getData().get(j).getPhotoReference();
                                emailID = response.body().getData().get(j).getEmail();
                                likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                                revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                                followingID = response.body().getData().get(j).getFollowingId();
                                totalreview = response.body().getData().get(j).getTotalReview();
                                total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                                total_taste = response.body().getData().get(j).getTotalTaste();
                                package_value = response.body().getData().get(j).get_package();
                                total_service = response.body().getData().get(j).getTotalService();
                                total_value = response.body().getData().get(j).getTotalValueMoney();
                                total_good = response.body().getData().get(j).getTotalGood();
                                total_bad = response.body().getData().get(j).getTotalBad();
                                total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();

                                txt_hotelname = response.body().getData().get(j).getHotelName();
                                user_id = response.body().getData().get(j).getUserId();
                                hotel_id = response.body().getData().get(j).getHotelId();
                                txt_hotel_address = response.body().getData().get(j).getAddress();

                                txt_review_username = response.body().getData().get(j).getFirstName();
                                txt_review_userlastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                txt_review_caption = response.body().getData().get(j).getHotelReview();
                                txt_review_like = response.body().getData().get(j).getTotalLikes();
                                txt_review_comment = response.body().getData().get(j).getTotalComments();
                                txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                                hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                                hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                                total_followers = response.body().getData().get(j).getTotalFollowers();
                                total_followings = response.body().getData().get(j).getTotalFollowings();
                                foodexp = response.body().getData().get(j).getFoodExprience();
                                top_count = response.body().getData().get(j).getTopCount();
                                dishtoavoid = response.body().getData().get(j).getAvoiddish();
//                                topdishimage_count= response.body().getData().get(j).getTopdishimageCount();
//                                avoiddishimage_count= response.body().getData().get(j).getAvoiddishimageCount();
                                avoid_count = response.body().getData().get(j).getAvoidCount();
                                ambi_image_count = response.body().getData().get(j).getAmbiImageCount();
                                dishname = response.body().getData().get(j).getTopdish();
                                delivery_mode = response.body().getData().get(j).getDelivery_mode();
                                top_dish_img_count = response.body().getData().get(j).getTop_dish_img_count();
                                avoid_dish_img_count = response.body().getData().get(j).getAvoid_dish_img_count();
                                bucketlist = response.body().getData().get(j).getBucket_list();
                                packCount = response.body().getData().get(j).getPackImageCount();


                                ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();

                                if (top_count == 0) {

                                } else {

                                    for (int b = 0; b < response.body().getData().get(j).getTopCount(); b++) {

                                        topdishimage = response.body().getData().get(j).getTopdishimage().get(b).getImg();
                                        topdishimage_name = response.body().getData().get(j).getTopdishimage().get(b).getDishname();
                                        ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimage_name);
                                        reviewTopDishPojoArrayList.add(reviewTopDishPojo);
                                    }


                                }

                                ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();

                                if (avoid_count == 0) {

                                } else {
                                    for (int a = 0; a < response.body().getData().get(j).getAvoidCount(); a++) {

                                        String dishavoidimage = response.body().getData().get(j).getAvoiddishimage().get(a).getImg();
                                        avoiddishimage_name = response.body().getData().get(j).getAvoiddishimage().get(a).getDishname();
                                        ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(dishavoidimage, avoiddishimage_name);
                                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);
                                    }

                                }


                                ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

                                //Ambiance
                                if (ambi_image_count == 0) {


                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getAmbiImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getAmbiImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(img);

                                        reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);
                                    }


                                }

                                //packaging

                                ArrayList<PackImage> reviewpackImagePojoArrayList = new ArrayList<>();


                                if (packCount == 0) {

                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getPackImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getPackImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        PackImage packImage = new PackImage(img);

                                        reviewpackImagePojoArrayList.add(packImage);
                                    }

                                }
                                ReviewInputAllPojo reviewInputAllPojo = new ReviewInputAllPojo(hotelId, revratID, txt_review_caption, txt_review_like, txt_review_comment,
                                        category_type, veg_nonveg, foodexp, ambiance, modeid, company_name, taste, service, package_value, time_delivery,
                                        valuemoney, createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude, longitude, phone,
                                        photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname, emailID, "", "", "", "",
                                        total_followers, total_followings, userimage, likes_hotelID, revrathotel_likes, followingID, totalreview,
                                        txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service, total_package, total_timedelivery,
                                        total_value, total_good, total_bad, total_good_bad_user, reviewTopDishPojoArrayList, topdishimage_count,
                                        dishname, top_count, reviewAvoidDishPojoArrayList, avoiddishimage_count, dishtoavoid, avoid_count,
                                        reviewAmbiImagePojoArrayList, ambi_image_count, false, delivery_mode, top_dish_img_count,
                                        avoid_dish_img_count, bucketlist,redirect_url, reviewpackImagePojoArrayList,packCount);

                                getallHotelReviewPojoArrayList.add(reviewInputAllPojo);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList, fromwhich,ReviewsImageFragment.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                        review_images_recylerview.setLayoutManager(mLayoutManager);
                        review_images_recylerview.setItemAnimator(new DefaultItemAnimator());
                        review_images_recylerview.setAdapter(getHotelDetailsAdapter);
                        review_images_recylerview.setNestedScrollingEnabled(false);
                        getHotelDetailsAdapter.notifyDataSetChanged();


                    } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {
                        ll_no_post.setVisibility(View.VISIBLE);
                        frame_layout.setBackgroundColor(context.getResources().getColor(R.color.white));

                    }

                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    ll_no_post.setVisibility(View.VISIBLE);
                    frame_layout.setBackgroundColor(context.getResources().getColor(R.color.white));

                }
            });
        } else {


            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();
            /*Snackbar snackbar = Snackbar.make(rl_hotel, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();*/
            frame_layout.setBackgroundColor(getResources().getColor(R.color.white));

        }


    }


    private void get_hotel_review_user_resume() {


        if (isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<ReviewOutputResponsePojo> call = apiService.get_hotel_review_user("get_hotel_review_user", Integer.parseInt(userID));
            call.enqueue(new Callback<ReviewOutputResponsePojo>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<ReviewOutputResponsePojo> call, Response<ReviewOutputResponsePojo> response) {

                    if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {
                        getallHotelReviewPojoArrayList.clear();


                        for (int j = 0; j < response.body().getData().size(); j++) {

                            Log.e("size-->", "" + response.body().getData().size());

                            try {

                                hotelId = response.body().getData().get(j).getHotelId();
                                cat_type = response.body().getData().get(j).getCategoryType();
                                redirect_url = response.body().getData().get(j).getRedirect_url();
                                veg_nonveg = response.body().getData().get(j).getVegNonveg();
                                revratID = response.body().getData().get(j).getRevratId();
                                modeid = response.body().getData().get(j).getMode_id();
                                company_name = response.body().getData().get(j).getComp_name();

                                ambiance = response.body().getData().get(j).getAmbiance();
                                taste = response.body().getData().get(j).getTaste();
                                total_package = response.body().getData().get(j).getTotalPackage();
                                total_timedelivery = response.body().getData().get(j).getTotalTimedelivery();
                                service = response.body().getData().get(j).getService();
                                valuemoney = response.body().getData().get(j).getValueMoney();
                                createdby = response.body().getData().get(j).getCreatedBy();
                                createdon = response.body().getData().get(j).getCreatedOn();
                                googleID = response.body().getData().get(j).getGoogleId();
                                placeID = response.body().getData().get(j).getPlaceId();
                                opentimes = response.body().getData().get(j).getOpenTimes();
                                category_type = response.body().getData().get(j).getCategoryType();
                                latitude = response.body().getData().get(j).getLatitude();
                                longitude = response.body().getData().get(j).getLongitude();
                                phone = response.body().getData().get(j).getPhone();
                                photo_reference = response.body().getData().get(j).getPhotoReference();
                                emailID = response.body().getData().get(j).getEmail();
                                likes_hotelID = response.body().getData().get(j).getLikesHtlId();
                                revrathotel_likes = response.body().getData().get(j).getHtlRevLikes();
                                followingID = response.body().getData().get(j).getFollowingId();
                                totalreview = response.body().getData().get(j).getTotalReview();
                                total_ambiance = response.body().getData().get(j).getTotalAmbiance();
                                total_taste = response.body().getData().get(j).getTotalTaste();
                                package_value = response.body().getData().get(j).get_package();
                                total_service = response.body().getData().get(j).getTotalService();
                                total_value = response.body().getData().get(j).getTotalValueMoney();
                                total_good = response.body().getData().get(j).getTotalGood();
                                total_bad = response.body().getData().get(j).getTotalBad();
                                total_good_bad_user = response.body().getData().get(j).getTotalGoodBadUser();

                                txt_hotelname = response.body().getData().get(j).getHotelName();
                                user_id = response.body().getData().get(j).getUserId();
                                hotel_id = response.body().getData().get(j).getHotelId();
                                txt_hotel_address = response.body().getData().get(j).getAddress();

                                txt_review_username = response.body().getData().get(j).getFirstName();
                                txt_review_userlastname = response.body().getData().get(j).getLastName();
                                userimage = response.body().getData().get(j).getPicture();
                                txt_review_caption = response.body().getData().get(j).getHotelReview();
                                txt_review_like = response.body().getData().get(j).getTotalLikes();
                                txt_review_comment = response.body().getData().get(j).getTotalComments();
                                txt_review_follow = response.body().getData().get(j).getTotalReviewUsers();
                                hotel_review_rating = response.body().getData().get(j).getTotalFoodExprience();
                                hotel_user_rating = response.body().getData().get(j).getFoodExprience();
                                total_followers = response.body().getData().get(j).getTotalFollowers();
                                total_followings = response.body().getData().get(j).getTotalFollowings();
                                foodexp = response.body().getData().get(j).getFoodExprience();
                                top_count = response.body().getData().get(j).getTopCount();
                                dishtoavoid = response.body().getData().get(j).getAvoiddish();
//                                topdishimage_count= response.body().getData().get(j).getTopdishimageCount();
//                                avoiddishimage_count= response.body().getData().get(j).getAvoiddishimageCount();
                                avoid_count = response.body().getData().get(j).getAvoidCount();
                                ambi_image_count = response.body().getData().get(j).getAmbiImageCount();
                                dishname = response.body().getData().get(j).getTopdish();
                                delivery_mode = response.body().getData().get(j).getDelivery_mode();
                                top_dish_img_count = response.body().getData().get(j).getTop_dish_img_count();
                                avoid_dish_img_count = response.body().getData().get(j).getAvoid_dish_img_count();
                                bucketlist = response.body().getData().get(j).getBucket_list();
                                packCount = response.body().getData().get(j).getPackImageCount();


                                ArrayList<ReviewTopDishPojo> reviewTopDishPojoArrayList = new ArrayList<>();

                                if (top_count == 0) {

                                } else {

                                    for (int b = 0; b < response.body().getData().get(j).getTopCount(); b++) {

                                        topdishimage = response.body().getData().get(j).getTopdishimage().get(b).getImg();
                                        topdishimage_name = response.body().getData().get(j).getTopdishimage().get(b).getDishname();
                                        ReviewTopDishPojo reviewTopDishPojo = new ReviewTopDishPojo(topdishimage, topdishimage_name);
                                        reviewTopDishPojoArrayList.add(reviewTopDishPojo);
                                    }


                                }

                                ArrayList<ReviewAvoidDishPojo> reviewAvoidDishPojoArrayList = new ArrayList<>();

                                if (avoid_count == 0) {

                                } else {
                                    for (int a = 0; a < response.body().getData().get(j).getAvoidCount(); a++) {

                                        String dishavoidimage = response.body().getData().get(j).getAvoiddishimage().get(a).getImg();
                                        avoiddishimage_name = response.body().getData().get(j).getAvoiddishimage().get(a).getDishname();
                                        ReviewAvoidDishPojo reviewAvoidDishPojo = new ReviewAvoidDishPojo(dishavoidimage, avoiddishimage_name);
                                        reviewAvoidDishPojoArrayList.add(reviewAvoidDishPojo);
                                    }

                                }


                                ArrayList<ReviewAmbiImagePojo> reviewAmbiImagePojoArrayList = new ArrayList<>();

                                //Ambiance
                                if (ambi_image_count == 0) {


                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getAmbiImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getAmbiImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        ReviewAmbiImagePojo reviewAmbiImagePojo = new ReviewAmbiImagePojo(img);

                                        reviewAmbiImagePojoArrayList.add(reviewAmbiImagePojo);
                                    }


                                }

                                //packaging

                                ArrayList<PackImage> reviewpackImagePojoArrayList = new ArrayList<>();


                                if (packCount == 0) {

                                } else {

                                    for (int l = 0; l < response.body().getData().get(j).getPackImageCount(); l++) {
                                        //Ambi Image
                                        String img = response.body().getData().get(j).getPackImage().get(l).getImg();
                                        Log.e("ambi_all", "-->" + img);
                                        PackImage packImage = new PackImage(img);

                                        reviewpackImagePojoArrayList.add(packImage);
                                    }

                                }

                                ReviewInputAllPojo reviewInputAllPojo = new ReviewInputAllPojo(hotelId, revratID, txt_review_caption, txt_review_like, txt_review_comment,
                                        category_type, veg_nonveg, foodexp, ambiance, modeid, company_name, taste, service, package_value, time_delivery,
                                        valuemoney, createdby, createdon, googleID, txt_hotelname, placeID, opentimes, txt_hotel_address, latitude, longitude, phone,
                                        photo_reference, user_id, "", "", txt_review_username, txt_review_userlastname, emailID, "", "", "", "",
                                        total_followers, total_followings, userimage, likes_hotelID, revrathotel_likes, followingID, totalreview,
                                        txt_review_follow, hotel_review_rating, total_ambiance, total_taste, total_service, total_package, total_timedelivery,
                                        total_value, total_good, total_bad, total_good_bad_user, reviewTopDishPojoArrayList, topdishimage_count,
                                        dishname, top_count, reviewAvoidDishPojoArrayList, avoiddishimage_count, dishtoavoid, avoid_count,
                                        reviewAmbiImagePojoArrayList, ambi_image_count, false, delivery_mode, top_dish_img_count,
                                        avoid_dish_img_count, bucketlist,redirect_url,reviewpackImagePojoArrayList,packCount);

                                getallHotelReviewPojoArrayList.add(reviewInputAllPojo);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        getHotelDetailsAdapter = new GetHotelDetailsAdapter(context, getallHotelReviewPojoArrayList, fromwhich,ReviewsImageFragment.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                        review_images_recylerview.setLayoutManager(mLayoutManager);
                        review_images_recylerview.setItemAnimator(new DefaultItemAnimator());
                        review_images_recylerview.setAdapter(getHotelDetailsAdapter);
                        review_images_recylerview.setNestedScrollingEnabled(false);
                        getHotelDetailsAdapter.notifyDataSetChanged();


                    } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("nodata")) {
                        ll_no_post.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onFailure(Call<ReviewOutputResponsePojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    ll_no_post.setVisibility(View.VISIBLE);
                }
            });
        } else {

            /*Snackbar snackbar = Snackbar.make(rl_hotel, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();*/
        }


    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (getallHotelReviewPojoArrayList.size() > 0) {
            get_hotel_review_user_resume();
        } else {
            get_hotel_review_user(userID);
        }






    }

    @Override
    public void onPostCommitedView(View view, final int adapterPosition, View hotelDetailsView) {

        if (Utility.isConnected(Objects.requireNonNull(getActivity()))) {

            review_comment = (EmojiconEditText) hotelDetailsView.findViewById(R.id.review_comment);

            comments_progressbar = (ProgressBar) hotelDetailsView.findViewById(R.id.comments_progressbar);
            comments_progressbar.setVisibility(View.VISIBLE);
            comments_progressbar.setIndeterminate(true);

            tv_view_all_comments = (TextView) hotelDetailsView.findViewById(R.id.tv_view_all_comments);

            if (review_comment.getText().toString().isEmpty()) {

                Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();

            } else {

                Utility.hideKeyboard(view);

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    int userid = Integer.parseInt(Pref_storage.getDetail(getActivity(), "userId"));

                    Call<ReviewCommentsPojo> call = apiService.get_create_edit_hotel_review_comments("create_edit_hotel_review_comments",
                            Integer.parseInt(getallHotelReviewPojoArrayList.get(adapterPosition).getHotelId()), 0, userid, review_comment.getText().toString(), Integer.parseInt(getallHotelReviewPojoArrayList.get(adapterPosition).getRevratId()));

                    call.enqueue(new Callback<ReviewCommentsPojo>() {

                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onResponse(Call<ReviewCommentsPojo> call, Response<ReviewCommentsPojo> response) {

                            review_comment.setText("");

                            if (response.code() == 500) {

                                comments_progressbar.setVisibility(View.GONE);
                                comments_progressbar.setIndeterminate(false);

                                Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();

                            } else {

                                comments_progressbar.setVisibility(View.GONE);
                                comments_progressbar.setIndeterminate(false);


                                if (response.body() != null) {

                                    if (Objects.requireNonNull(response.body()).getResponseMessage().equals("success")) {

                                        if (response.body().getData().size() > 0) {

                                            if (tv_view_all_comments.getVisibility() == View.GONE) {

                                                tv_view_all_comments.setVisibility(View.VISIBLE);
                                                //String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                                String comments = getString(R.string.view_one_comment);
                                                tv_view_all_comments.setText(comments);

                                            } else {

                                                String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                                tv_view_all_comments.setText(comments);
                                            }

                                            Intent intent = new Intent(getActivity(), Reviews_comments_all_activity.class);
                                            intent.putExtra("posttype", getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType());
                                            intent.putExtra("hotelid", getallHotelReviewPojoArrayList.get(adapterPosition).getHotelId());
                                            intent.putExtra("reviewid", getallHotelReviewPojoArrayList.get(adapterPosition).getRevratId());
                                            intent.putExtra("userid", getallHotelReviewPojoArrayList.get(adapterPosition).getUserId());
                                            intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(adapterPosition).getHotelName());
                                            intent.putExtra("overallrating", getallHotelReviewPojoArrayList.get(adapterPosition).getFoodExprience());

                                            if (getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                                intent.putExtra("totaltimedelivery", getallHotelReviewPojoArrayList.get(adapterPosition).getTotalTimedelivery());

                                            } else {
                                                intent.putExtra("totaltimedelivery", getallHotelReviewPojoArrayList.get(adapterPosition).getAmbiance());

                                            }
                                            intent.putExtra("taste_count", getallHotelReviewPojoArrayList.get(adapterPosition).getTaste());
                                            intent.putExtra("vfm_rating", getallHotelReviewPojoArrayList.get(adapterPosition).getValueMoney());
                                            if (getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                                intent.putExtra("package_rating", getallHotelReviewPojoArrayList.get(adapterPosition).get_package());

                                            } else {
                                                intent.putExtra("package_rating", getallHotelReviewPojoArrayList.get(adapterPosition).getService());

                                            }
                                            intent.putExtra("hotelname", getallHotelReviewPojoArrayList.get(adapterPosition).getHotelName());
                                            intent.putExtra("reviewprofile", getallHotelReviewPojoArrayList.get(adapterPosition).getPicture());
                                            intent.putExtra("createdon", getallHotelReviewPojoArrayList.get(adapterPosition).getCreatedOn());
                                            intent.putExtra("firstname", getallHotelReviewPojoArrayList.get(adapterPosition).getFirstName());
                                            intent.putExtra("lastname", getallHotelReviewPojoArrayList.get(adapterPosition).getLastName());
                                            if (getallHotelReviewPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                                intent.putExtra("title_ambience", "Packaging");
                                            } else {
                                                intent.putExtra("title_ambience", "Hotel Ambience");
                                            }
                                            if (getallHotelReviewPojoArrayList.get(adapterPosition).getBucket_list() != 0) {
                                                intent.putExtra("addbucketlist", "yes");
                                            } else {
                                                intent.putExtra("addbucketlist", "no");
                                            }
                                            startActivity(intent);


                                        } else {

                                        }

                                    } else {

                                    }
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<ReviewCommentsPojo> call, Throwable t) {
                            //Error

                            comments_progressbar.setVisibility(View.GONE);
                            comments_progressbar.setIndeterminate(false);

                            Log.e("Balaji", "" + t.getMessage());
                        }
                    });

                } catch (Exception e) {

                    Log.e("Balaji", "" + e.getMessage());
                    e.printStackTrace();

                }
            }

        } else {

            Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

}

