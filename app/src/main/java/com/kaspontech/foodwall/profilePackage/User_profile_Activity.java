package com.kaspontech.foodwall.profilePackage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.adapters.Follow_following.Timeline_userimageall_adapter;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.followingPackage.Followerlist_user_activity;
import com.kaspontech.foodwall.followingPackage.Following_list_user_Activity;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_output_Pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_following_profile_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_timeline_image_all;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_profile_details_Pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Profile_output_pojo;
import com.kaspontech.foodwall.profilePackage.profileTabFragments.HistoricalImagesFragment;
import com.kaspontech.foodwall.profilePackage.profileTabFragments.ReviewsImageFragment;
import com.kaspontech.foodwall.profilePackage.profileTabFragments.UserTimelineImagesFragment;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.GlideApp;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//

/**
 * Created by vishnukm on 29/3/18.
 */

public class User_profile_Activity extends _SwipeActivityClass
        implements View.OnClickListener {


    Toolbar actionBar;
    ImageButton back;
    View view;
    Context context;
    TabLayout profileTablayout;
    ImageView img_profilepic, img_unfollow;
    ProgressBar progress_dialog;

    ImageButton more_profile;

    Button btnFollow;
    int follow_unfollow_code= 1;

    Pref_storage pref_storage;
    LinearLayout ll_userphoto_area, ll_followers, ll_following, ll_public_account, ll_private_account;
    RelativeLayout rl_profile_update, rl_follow, rl_user_pro, rl_messsage;
    Get_following_profile_pojo getFollowingProfilePojo;
    Get_profile_details_Pojo getProfileDetailsPojo;
    public static int totalsize;

    // Fragments
    Fragment timelineImagesFragment, reviewsImageFragment, historicalImagesFragment;

    ProgressBar progress_dialog_profile;

    public static ArrayList<Get_timeline_image_all> allimagelist_user = new ArrayList<>();
    List<String> myList_user = new ArrayList<>();
    Get_timeline_image_all getTimelineImageAll;


    Timeline_userimageall_adapter timelineUserimageallAdapter;

    TextView user_name, dateofbday, txt_total_post, txt_total_followers, txt_total_followings, toolbar_title, txt_bio_descrip, txt_follow;
    String userid, firstname, lastname, emailid, password, confirmpassword, gender, dateofbirth, ageresult, picture, genderupdate, firstnameresult, lastnameresult, birthdate,
            bio, pro_status, usr_name, followingid,reqFollowerId, following, followers, total_post, userid_story, userid_review, userid_notification;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);


        actionBar = (Toolbar) findViewById(R.id.actionbar_userprofile);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);
        more_profile = (ImageButton) actionBar.findViewById(R.id.more_profile);

        btnFollow = (Button) findViewById(R.id.btnFollow);

        more_profile.setVisibility(View.GONE);

        progress_dialog = (ProgressBar) findViewById(R.id.progress_dialognew);
        progress_dialog_profile = (ProgressBar) findViewById(R.id.progress_dialog_profile);


        profileTablayout = (TabLayout) findViewById(R.id.tab_profile);
        ll_userphoto_area = (LinearLayout) findViewById(R.id.ll_userphoto_area);
        ll_followers = (LinearLayout) findViewById(R.id.ll_followers);
        ll_public_account = (LinearLayout) findViewById(R.id.ll_public_account);
        ll_private_account = (LinearLayout) findViewById(R.id.ll_private_account);
        ll_followers.setOnClickListener(this);


        ll_following = (LinearLayout) findViewById(R.id.ll_following);
        ll_following.setOnClickListener(this);

        rl_profile_update = (RelativeLayout) findViewById(R.id.rl_profile_update);
        rl_user_pro = (RelativeLayout) findViewById(R.id.rl_user_pro);

        rl_follow = (RelativeLayout) findViewById(R.id.rl_follow);
        rl_messsage = (RelativeLayout) findViewById(R.id.rl_messsage);
        rl_follow.setOnClickListener(this);
        rl_messsage.setOnClickListener(this);
        btnFollow.setOnClickListener(this);

        img_profilepic = (ImageView) findViewById(R.id.img_profilepic);
        img_unfollow = (ImageView) findViewById(R.id.img_unfollow);
        img_unfollow.setOnClickListener(this);
        user_name = (TextView) findViewById(R.id.user_name);
//        dateofbday = (TextView) findViewById(R.id.dateofbday);
        txt_total_post = (TextView) findViewById(R.id.txt_total_post);
        txt_total_followers = (TextView) findViewById(R.id.txt_total_followers);
        txt_total_followings = (TextView) findViewById(R.id.txt_total_followings);
        txt_follow = (TextView) findViewById(R.id.txt_follow);
        txt_follow.setOnClickListener(this);
        txt_bio_descrip = (TextView) findViewById(R.id.txt_bio_descrip);


        profileTablayout.addTab(profileTablayout.newTab().setIcon(R.drawable.ic_historical_map));
        profileTablayout.addTab(profileTablayout.newTab().setIcon(R.drawable.ic_gallery_));
        profileTablayout.addTab(profileTablayout.newTab().setIcon(R.drawable.ic_review_photos));


        timelineImagesFragment = new UserTimelineImagesFragment();
        reviewsImageFragment = new ReviewsImageFragment();
        historicalImagesFragment = new HistoricalImagesFragment();

        Intent intent = getIntent();

        if (intent != null) {

            userid = intent.getStringExtra("created_by");

        }


        Log.e("UserProfile", "onCreate: "+userid );

        if (userid != null) {

            Bundle bundle = new Bundle();
            bundle.putString("userid", userid);
            historicalImagesFragment.setArguments(bundle);
            replaceFragment(historicalImagesFragment);

        }


        profileTablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {

                    case 0:


                        Bundle bundle = new Bundle();
                        bundle.putString("userid", userid);
                        historicalImagesFragment.setArguments(bundle);
                        replaceFragment(historicalImagesFragment);

                        break;

                    case 1:

                        Bundle bundle1 = new Bundle();
                        bundle1.putString("userid", userid);
                        timelineImagesFragment.setArguments(bundle1);
                        replaceFragment(timelineImagesFragment);

                        break;

                    case 2:

                        Bundle bundle2 = new Bundle();
                        bundle2.putString("userid", userid);
                        reviewsImageFragment.setArguments(bundle2);
                        replaceFragment(reviewsImageFragment);

                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        try {

            rl_user_pro.setVisibility(View.GONE);
            progress_dialog_profile.setVisibility(View.VISIBLE);
            get_profile();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onSwipeRight() {
            finish();
    }

    @Override
    protected void onSwipeLeft() {

    }


    //Replace fragment

    public void replaceFragment(Fragment fragment) {

        try {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.user_profile_container, fragment);
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        get_profile();
    }



    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:

                finish();

                break;

            case R.id.ll_followers:

                Intent intent = new Intent(User_profile_Activity.this, Followerlist_user_activity.class);
                intent.putExtra("created_by", userid);
                startActivity(intent);

                break;

            case R.id.ll_following:

                Intent intent1 = new Intent(User_profile_Activity.this, Following_list_user_Activity.class);
                intent1.putExtra("created_by", userid);
                startActivity(intent1);

                break;


            case R.id.rl_follow:

                startActivity(new Intent(this, ChatActivity.class));


                break;

            case R.id.rl_messsage:

                createFollower();

                break;

            case R.id.btnFollow:

                createFollower();

                break;

            case R.id.txt_follow:

                createFollower();

                break;

            case R.id.img_unfollow:

                createFollower();

                break;
        }
    }


    /// Get Profile details


    private void get_profile() {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Profile_output_pojo> call = apiService.get_profile("get_profile", Integer.parseInt(userid),
                Integer.parseInt(Pref_storage.getDetail(User_profile_Activity.this, "userId")));


        call.enqueue(new Callback<Profile_output_pojo>() {
            @Override
            public void onResponse(Call<Profile_output_pojo> call, Response<Profile_output_pojo> response) {

                if (response.body().getResponseCode() == 1) {


                    for (int j = 0; j < response.body().getData().size(); j++) {


                        String userId = response.body().getData().get(j).getUserId();
                        firstname = response.body().getData().get(j).getFirstName();
                        lastname = response.body().getData().get(j).getLastName();
                        password = response.body().getData().get(j).getPassword();
                        gender = response.body().getData().get(j).getGender();
                        dateofbirth = response.body().getData().get(j).getDob().replace("\"", "");
                        picture = response.body().getData().get(j).getPicture();
                        total_post = response.body().getData().get(j).getTotalPosts();
                        following = response.body().getData().get(j).getTotalFollowings();
                        followers = response.body().getData().get(j).getTotalFollowers();
                        bio = response.body().getData().get(j).getBioDescription();
                        pro_status = response.body().getData().get(j).getProfileStatus();
                        usr_name = response.body().getData().get(j).getUsername();
                        followingid = response.body().getData().get(j).getFollowingId();
                        reqFollowerId = response.body().getData().get(j).getReqFollowerId();


                        try {


                            String ownuser_id = Pref_storage.getDetail(User_profile_Activity.this, "userId");
                            String followeruser_id = Pref_storage.getDetail(User_profile_Activity.this, "followeruserID");

                            /*   if (Integer.parseInt(ownuser_id) == Integer.parseInt(followeruser_id) ) {
                                txt_follow.setText(R.string.txt_following);
                            } else {
                                txt_follow.setText(R.string.follow);
                            } */



                            /* if ( ownuser_id.contains(followeruser_id) ) {
                             img_unfollow.setImageResource(R.drawable.ic_followers);
                            } else {
                             img_unfollow.setImageResource(R.drawable.ic_add_follow);
                            }*/

                            if (Integer.parseInt(followingid) != 0) {
                                img_unfollow.setImageResource(R.drawable.ic_followers);
                                btnFollow.setText(R.string.txt_following);
//                                rl_messsage.setVisibility(View.VISIBLE);
                                btnFollow.setTextColor(Color.BLACK);
                                btnFollow.setBackground(getResources().getDrawable(R.drawable.editext_login_background));


                            } else if (Integer.parseInt(followingid) == 0) {
                                img_unfollow.setImageResource(R.drawable.ic_add_follow);
                                btnFollow.setText(R.string.follow);
//                                rl_messsage.setVisibility(View.VISIBLE);
                                btnFollow.setTextColor(Color.WHITE);
                                btnFollow.setBackground(getResources().getDrawable(R.drawable.button_login_background));
                            }


                            if (bio.equalsIgnoreCase(null) || bio.equalsIgnoreCase("") || bio.equalsIgnoreCase("0")) {
                                txt_bio_descrip.setText(" ");
                                txt_bio_descrip.setVisibility(View.GONE);
                            } else {
                                txt_bio_descrip.setText(bio.replace("\"",""));
                                txt_bio_descrip.setVisibility(View.VISIBLE);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


//                        Utility.picassoImageLoader(picture,1,img_profilepic, getApplicationContext());

                        GlideApp.with(User_profile_Activity.this)
                                .load(picture)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .thumbnail(0.1f)
                                .placeholder(R.drawable.ic_add_photo)
                                .into(img_profilepic);

                        txt_total_post.setText(total_post);
                        txt_total_followings.setText(following);
                        txt_total_followers.setText(followers);
                        String fullname = firstname.concat(" ").concat(lastname);
                        user_name.setText(fullname);
                        toolbar_title.setText(fullname);

                        if (gender.equalsIgnoreCase("male")) {
                            gender = "Male";
                        } else if (gender.equalsIgnoreCase("female")) {
                            gender = "Female";
                        }
//                        agecalculation();
                        progress_dialog_profile.setVisibility(View.GONE);
                        rl_user_pro.setVisibility(View.VISIBLE);

                        if (pro_status.equals("0")) {

                            ll_private_account.setVisibility(View.GONE);
                            ll_public_account.setVisibility(View.VISIBLE);


                        } else if (pro_status.equals("1") && Integer.parseInt(followingid) == 0) {

                            ll_private_account.setVisibility(View.VISIBLE);
                            ll_public_account.setVisibility(View.GONE);

                        }

                        if(pro_status.equals("1") && !reqFollowerId.equals("0")){

                            btnFollow.setText(R.string.requested);
                            btnFollow.setTextColor(Color.BLACK);
                            btnFollow.setBackground(getResources().getDrawable(R.drawable.editext_login_background));

                        }




                    }
                }
            }

            @Override
            public void onFailure(Call<Profile_output_pojo> call, Throwable t) {
                //Error
                Log.e("FailureError", "" + t.getMessage());

                Snackbar snackbar = Snackbar.make(rl_user_pro, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(Color.RED);
                View view1 = snackbar.getView();
                TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                textview.setTextColor(Color.WHITE);
                snackbar.show();
            }
        });
    }


    private void createFollower() {




        if(pro_status != null){

            if(pro_status.equals("0")){

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                try {

                    if(btnFollow.getText().toString().equalsIgnoreCase("Following")){
                        follow_unfollow_code=0;
                    }else {
                        follow_unfollow_code=1;
                    }

                    String createdby = Pref_storage.getDetail(User_profile_Activity.this, "userId");

                    Call<Get_following_output_Pojo> call = apiService.create_follower("create_follower", Integer.parseInt(createdby), Integer.parseInt(userid), follow_unfollow_code);
                    call.enqueue(new Callback<Get_following_output_Pojo>() {
                        @Override
                        public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {


                            if (response.body().getResponseCode() == 1) {


                                for (int j = 0; j < response.body().getData().size(); j++) {


                                    String followeruserID = response.body().getData().get(j).getUser_id();
                                    Pref_storage.setDetail(User_profile_Activity.this, "followeruserID", followeruserID);


                                    if(follow_unfollow_code==1){
                                        btnFollow.setText(R.string.txt_following);
                                        btnFollow.setTextColor(Color.BLACK);
                                        btnFollow.setBackground(getResources().getDrawable(R.drawable.editext_login_background));
                                        img_unfollow.setImageResource(R.drawable.ic_followers);
                                        rl_messsage.setVisibility(View.VISIBLE);
                                    }else {

                                        btnFollow.setText(R.string.follow);
                                        btnFollow.setTextColor(Color.WHITE);
                                        btnFollow.setBackground(getResources().getDrawable(R.drawable.button_login_background));
                                        img_unfollow.setImageResource(R.drawable.ic_add_follow);
                                        rl_messsage.setVisibility(View.VISIBLE);
                                    }

                                    /// Update profile
                                    get_profile();


                                }


                            }

                        }

                        @Override
                        public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else if(pro_status.equals("1")){


                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                try {

                    String createdby = Pref_storage.getDetail(context, "userId");

                    Call<CommonOutputPojo> call = apiService.create_follower_request("create_follower_request", Integer.parseInt(createdby), Integer.parseInt(userid), 1);
                    call.enqueue(new Callback<CommonOutputPojo>() {
                        @Override
                        public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {


                            if (response.body().getResponseMessage().equals("success")) {


                                btnFollow.setText(R.string.requested);
                                btnFollow.setTextColor(Color.BLACK);
                                btnFollow.setBackground(getResources().getDrawable(R.drawable.editext_login_background));



                            }

                        }

                        @Override
                        public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }



            }

        }


    }

    private void unfollow() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            String createdby = Pref_storage.getDetail(User_profile_Activity.this, "userId");
            String going_to_follow_user_id = Pref_storage.getDetail(User_profile_Activity.this, "clickeduser_id");

            Call<Get_following_output_Pojo> call = apiService.create_follower("createFollower", Integer.parseInt(createdby), Integer.parseInt(going_to_follow_user_id), 0);
            call.enqueue(new Callback<Get_following_output_Pojo>() {
                @Override
                public void onResponse(Call<Get_following_output_Pojo> call, Response<Get_following_output_Pojo> response) {


//                    Toast.makeText(context, "vishnulikes" + response.body().getResponseCode(), Toast.LENGTH_SHORT).show();

                    if (response.body().getResponseCode() == 1) {


                        for (int j = 0; j < response.body().getData().size(); j++) {


                            String followeruserID = response.body().getData().get(j).getUser_id();
                            Pref_storage.setDetail(User_profile_Activity.this, "followeruserID", followeruserID);

                            String createdOn = response.body().getData().get(j).getCreated_on();
                            String Total_followers = response.body().getData().get(j).getTotal_followers();
                            String total_followings = response.body().getData().get(j).getTotal_followings();
                            String userId = response.body().getData().get(j).getUser_id();
                            String oauthProvider = response.body().getData().get(j).getOauth_provider();
                            String oauthUid = response.body().getData().get(j).getOauth_uid();
                            String firstName1 = response.body().getData().get(j).getFirst_name();
                            String lastName1 = response.body().getData().get(j).getLast_name();
                            String location = response.body().getData().get(j).getLatitude();
                            String locationstate = response.body().getData().get(j).getLongitude();
                            String email1 = response.body().getData().get(j).getEmail();
                            String contNo = response.body().getData().get(j).getCont_no();
                            String gender = response.body().getData().get(j).getGender();
                            String dob = response.body().getData().get(j).getDob();
                            String following_picture = response.body().getData().get(j).getPicture();
                            txt_follow.setText(R.string.follow);


                            get_profile();

                        /*    if (Integer.parseInt(Total_followers) == 0||Total_followers.equalsIgnoreCase(null)) {
                                txt_total_followers.setText("0");
                            } else {
                                txt_total_followers.setText(Total_followers);

                            }*/

                        }


                    }

                }

                @Override
                public void onFailure(Call<Get_following_output_Pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
