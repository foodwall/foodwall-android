package com.kaspontech.foodwall.profilePackage;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kaspontech.foodwall.REST_API.ProgressRequestBody;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Profile_get_username_output;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.User_image_input_response;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.User_image_update_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Get_profile_details_Pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Profile_output_pojo;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Timeline_create_output_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import com.kaspontech.foodwall.utills.CustomGIFview;
 import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import org.joda.time.DateTime;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.utills.Utility.hideKeyboard;

//

/**
 * Created by vishnukm on 26/3/18.
 */

public class editProfileActivity extends AppCompatActivity implements View.OnClickListener, ProgressRequestBody.UploadCallbacks{


    /*
    * TAG
    * */
    private static final String TAG = "editProfileActivity";

    Toolbar actionBar;
    Button back,save;
    RadioGroup genderGroup;
    DatePicker dob_picker;

    Switch switch_pvt_acc;

    Button button_update;
    Button btn_dob_done;
    EditText editText_username;
    EditText edt_username;
    EditText editText_lastname;
    EditText editText_useremailID;
    EditText edit_dob;
    EditText editText_bio;
    //    EmojiEditText editText_bio;
    RadioButton radioM;RadioButton radioF;
    RadioButton radioTransgender;RadioButton radioRathernotsay;
    CircleImageView userimage;
    ImageView img_sts_update;

    TextView txt_change_photo;
    TextView txt_private_txt;
    RelativeLayout emailParent;
    ProgressBar pro_edt_profile;

    User_image_input_response userImageInputResponse;




    //Loader
    ProgressBar progressBar;
    TextView prg_textView;
    CustomGIFview loaderimage;
    AlertDialog dialog1;


    Uri imageUri;

    private static final int REQUEST_CAMERA_PERMISSION = 0;
    private static final int REQUEST_GALLERY_PERMISSION = 2;
    private static final int REQUEST_PERMISSION_SETTING = 5;

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1, account_status = 0;


    String firstname, userId, lastname, emailid, password, pro_status, username, confirmpassword, bio_description, logintype, gender, dateofbirth, picture, genderupdate, firstnameresult, lastnameresult, username_result, bioupdate_result, birthdate, bioupdate;
    private static final int PICK_FROM_GALLERY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

        actionBar = (Toolbar) findViewById(R.id.action_bar_profile);
        save = (Button) actionBar.findViewById(R.id.save);
        back = (Button) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);
        save.setOnClickListener(this);

        button_update = (Button) findViewById(R.id.button_update);
        button_update.setOnClickListener(this);

        /*btn_password = (Button) findViewById(R.id.btn_password);
        btn_password.setOnClickListener(this);*/


        userimage = (CircleImageView) findViewById(R.id.img_circular);
        userimage.setOnClickListener(this);

        txt_private_txt = (TextView) findViewById(R.id.txt_private_txt);
        img_sts_update = (ImageView) findViewById(R.id.img_sts_update);

        txt_change_photo = (TextView) findViewById(R.id.txt_change_photo);
        txt_change_photo.setOnClickListener(this);

        editText_username = (EditText) findViewById(R.id.editText_username);
        edt_username = (EditText) findViewById(R.id.edt_username);
        editText_lastname = (EditText) findViewById(R.id.editText_lastname);
        editText_useremailID = (EditText) findViewById(R.id.editText_useremailID);
        editText_bio = (EditText) findViewById(R.id.editText_bio);
        edit_dob = (EditText) findViewById(R.id.edit_dob);
        edit_dob.setOnClickListener(this);

        emailParent = (RelativeLayout) findViewById(R.id.emailParent);
        pro_edt_profile = (ProgressBar) findViewById(R.id.pro_edt_profile);

        radioM = (RadioButton) findViewById(R.id.radioM);
        radioF = (RadioButton) findViewById(R.id.radioF);
        radioTransgender = (RadioButton) findViewById(R.id.radioTransgender);
        radioRathernotsay = (RadioButton) findViewById(R.id.radioRathernotSay);
        genderGroup = (RadioGroup) findViewById(R.id.radioGrp);

        switch_pvt_acc = (Switch) findViewById(R.id.switch_pvt_acc);

        dob_picker = (DatePicker) findViewById(R.id.dob_picker);
        btn_dob_done = (Button) findViewById(R.id.btn_dob_done);
        btn_dob_done.setOnClickListener(this);

        switch_pvt_acc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    txt_private_txt.setVisibility(View.VISIBLE);
                    account_status = 1;
                } else {
                    account_status = 0;
                    txt_private_txt.setVisibility(View.GONE);
                }
            }
        });


        Intent intent = getIntent();

        firstname = intent.getStringExtra("firstname");
        userId = intent.getStringExtra("userId");
        editText_username.setText(firstname);

        lastname = intent.getStringExtra("lastname");
        editText_lastname.setText(lastname);


        dateofbirth = intent.getStringExtra("dob");

        Log.e("dateofbirth", "-->" + dateofbirth);

        if (dateofbirth == null) {
            edit_dob.setText("");

        } else {
            if (dateofbirth.equalsIgnoreCase("0")) {
                edit_dob.setText("");
            } else {
                edit_dob.setText(dateofbirth);
            }
        }


        username = intent.getStringExtra("username");
        edt_username.setText(username);


        emailid = intent.getStringExtra("email");
        editText_useremailID.setText(emailid);

        gender = intent.getStringExtra("gender");
        Log.e(TAG, "onCreate:gender "+gender );
        logintype = intent.getStringExtra("logintype");

        if (logintype == null) {
            logintype = Pref_storage.getDetail(editProfileActivity.this, "loginType");
        }
//        Log.e(TAG,"logintype-->"+logintype);

        bio_description = intent.getStringExtra("bio_description").replace("\"", "");

        pro_status = intent.getStringExtra("pro_status");


        if (pro_status.equalsIgnoreCase("1")) {
            switch_pvt_acc.setChecked(true);
            txt_private_txt.setVisibility(View.VISIBLE);
        } else {
            switch_pvt_acc.setChecked(false);
        }

        if (bio_description.equalsIgnoreCase("0") || bio_description.trim().equalsIgnoreCase("\"\"") || bio_description.trim().equalsIgnoreCase("")) {
            editText_bio.setHint("Write something about you.");
        } else {
            editText_bio.setText(bio_description);
        }


        try {
            if (gender.equalsIgnoreCase("male")) {
                radioM.setChecked(true);
                radioF.setChecked(false);
                radioRathernotsay.setChecked(false);
                radioTransgender.setChecked(false);

            } else if (gender.equalsIgnoreCase("female")) {
                radioF.setChecked(true);
                radioM.setChecked(false);
                radioRathernotsay.setChecked(false);
                radioTransgender.setChecked(false);

            } else if (gender.equalsIgnoreCase("transgende")) {
                    radioM.setChecked(false);
                    radioF.setChecked(false);
                    radioRathernotsay.setChecked(false);

                    radioTransgender.setChecked(true);


                } else if (gender.contains("rathernots")) {
                    radioM.setChecked(false);
                    radioF.setChecked(false);
                    radioTransgender.setChecked(false);

                    radioRathernotsay.setChecked(true);



            }

        } catch (Exception e) {
            e.printStackTrace();
        }




        // Gender checked change listener
        radioM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioM.setChecked(true);
                genderupdate = "male";
                radioF.setChecked(false);
                radioTransgender.setChecked(false);
                radioRathernotsay.setChecked(false);
            }
        });

        radioF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioF.setChecked(true);
                genderupdate = "female";
                radioM.setChecked(false);
                radioTransgender.setChecked(false);
                radioRathernotsay.setChecked(false);
            }
        });


        radioTransgender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioTransgender.setChecked(true);
                genderupdate = "transgender";
                radioM.setChecked(false);
                radioF.setChecked(false);
                radioRathernotsay.setChecked(false);
            }
        });
        radioRathernotsay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioRathernotsay.setChecked(true);
                genderupdate = "rathernotsay";
                radioM.setChecked(false);
                radioF.setChecked(false);
                radioTransgender.setChecked(false);
            }
        });

        picture = intent.getStringExtra("picture");


        try {

            Utility.picassoImageLoader(picture,1,userimage, getApplicationContext());

        } catch (Exception e) {
            e.printStackTrace();
        }



        editText_username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (Integer.parseInt(logintype) == 1 || Integer.parseInt(logintype) == 2) {
                    Toast.makeText(editProfileActivity.this, "You are linked with your social account. Since you cannot update your data. Please sign up for full access", Toast.LENGTH_SHORT).show();
                    editText_username.setEnabled(false);
                } else {
                    editText_username.setEnabled(true);
                }
            }
        });
        editText_lastname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (Integer.parseInt(logintype) == 1 || Integer.parseInt(logintype) == 2) {
                    Toast.makeText(editProfileActivity.this, "You are linked with your social account. Since you cannot update your data. Please sign up for full access", Toast.LENGTH_SHORT).show();
                    editText_lastname.setEnabled(false);
                } else {
                    editText_lastname.setEnabled(true);
                }
            }
        });

        editText_useremailID.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (Integer.parseInt(logintype) == 1 || Integer.parseInt(logintype) == 2) {
                    Toast.makeText(editProfileActivity.this, "You are linked with your social account. Since you cannot update your data. Please sign up for full access", Toast.LENGTH_SHORT).show();
                    editText_useremailID.setEnabled(false);
                } else {
                    editText_useremailID.setEnabled(true);
                }
            }
        });


    }


    /*Validation*/
    private boolean validate() {

        boolean valid = true;

        String firstName = editText_username.getText().toString().trim();
        String lastName = editText_lastname.getText().toString().trim();
        String userName = edt_username.getText().toString().trim();


        if (firstName.length() == 0) {
            editText_username.setError("Enter valid First Name");
            valid = false;
        } else {
            editText_username.setError(null);
        }

        if (lastName.length() == 0) {
            editText_lastname.setError("Enter valid Last Name");
            valid = false;
        } else {
            editText_lastname.setError(null);
        }

        if (userName.length() == 0) {
            edt_username.setError("Enter valid username");
            valid = false;
        } else {
            edt_username.setError(null);
        }


        return valid;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.btn_dob_done:

                dob_picker.setVisibility(View.GONE);
                btn_dob_done.setVisibility(View.GONE);

                break;

            case R.id.back:

                AlertDialog.Builder builder=new AlertDialog.Builder(this);
                builder.setTitle("Confirm")
                        .setMessage("Are you sure you want to skip ?..")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                               // finish();
                                startActivity(new Intent(editProfileActivity.this,Profile.class));
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alt=builder.create();
                alt.show();

                break;

            case R.id.txt_change_photo:

                camera_galleryalert();


                break;

            case R.id.img_circular:

                camera_galleryalert();

                break;


            case R.id.edit_dob:

                if (Integer.parseInt(logintype) == 1 || Integer.parseInt(logintype) == 2) {

                    Toast.makeText(this, "You are linked with your social account. Since you cannot update all your data. Please sign up for full access", Toast.LENGTH_SHORT).show();

                  /*  editText_username.setEnabled(false);
                    editText_lastname.setEnabled(false);
                    edit_dob.setEnabled(false);
                    editText_useremailID.setEnabled(false);
                    radioM.setEnabled(false);
                    radioF.setEnabled(false);
                    userimage.setEnabled(false);


                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    String user_id = Pref_storage.getDetail(this, "userId");

                    String user = Pref_storage.getDetail(this, "UserImage");

                    File sourceFile = new File(user);

                    RequestBody imageupload = RequestBody.create(MediaType.parse("Image/jpeg"), sourceFile);

                    MultipartBody.Part image = MultipartBody.Part.createFormData("image", sourceFile.getName(), imageupload);

                    Call<User_image_update_pojo> call = apiService.update_user_profile_image("update_user_profile_image", Integer.parseInt(user_id), image);

                    call.enqueue(new Callback<User_image_update_pojo>() {
                        @Override
                        public void onResponse(Call<User_image_update_pojo> call, Response<User_image_update_pojo> response) {

                            if (response.body().getResponseCode() == 1) {

                                for (int j = 0; j < response.body().getData().size(); j++) {
                                    userImageInputResponse = new User_image_input_response("");

                                    String responsenew = response.body().getData().get(j).getStatus();

                                    if (Integer.parseInt(responsenew) == 1) {
                                        Toast.makeText(editProfileActivity.this, "Profile picture has been updated successfully", Toast.LENGTH_SHORT).show();
                                        get_profile();

                                    } else {
                                        Snackbar snackbar = Snackbar.make(emailParent, "Profile Image update failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                                        snackbar.setActionTextColor(Color.RED);
                                        View view1 = snackbar.getView();
                                        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                                        textview.setTextColor(Color.WHITE);
                                        snackbar.show();
                                    }
                                }


                            }
                        }

                        @Override
                        public void onFailure(Call<User_image_update_pojo> call, Throwable t) {
                            //Error
                            Log.e("FailureError", "" + t.getMessage());
                        }
                    });

                    txt_change_photo.setEnabled(false);*/

                } else {

                    hideKeyboard(v);

                    dob_picker.setVisibility(View.VISIBLE);
                    btn_dob_done.setVisibility(View.VISIBLE);


                    final Calendar c = Calendar.getInstance();
                    c.add(Calendar.YEAR, -3);
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);


                    Log.e(TAG, "Calender Year: " + Calendar.YEAR);

                    dob_picker.init(mYear, mMonth, mDay, new DatePicker.OnDateChangedListener() {

                        @Override
                        public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {

                            String frmdate = dayOfMonth + "-" + (month + 1) + "-" + year;


                            edit_dob.setText(frmdate);

                            edit_dob.setError(null);


                        }
                    });

                    // Joda Time @https://www.joda.org
                    DateTime dateTime = new DateTime().minusYears(3);
                    dob_picker.setMaxDate(dateTime.getMillis());



                    /*final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    String frmdate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                    edit_dob.setText(frmdate);
                                    edit_dob.setError(null);


                                }
                            }, mYear, mMonth, mDay);


                    datePickerDialog.show();
*/
                }


                break;

            case R.id.save:

                button_update.setVisibility(View.GONE);
                pro_edt_profile.setVisibility(View.VISIBLE);

                if (Integer.parseInt(logintype) == 1 || Integer.parseInt(logintype) == 2) {

//                    Toast.makeText(this, "You are linked with your social account. Since you cannot update all your data. Please sign up for full access", Toast.LENGTH_LONG).show();
                    editText_username.setEnabled(false);
                    editText_lastname.setEnabled(false);
                    edit_dob.setEnabled(false);
                    editText_useremailID.setEnabled(false);
                    radioM.setEnabled(false);
                    radioF.setEnabled(false);
                    radioTransgender.setEnabled(false);
                    radioRathernotsay.setEnabled(false);



                    int genderId = genderGroup.getCheckedRadioButtonId();


//
//                    if (genderId == radioM.getId()) {
//
//                        genderupdate = "male";
//
//                    } else {
//
//                        genderupdate = "female";
//
//                    }

                    firstnameresult = editText_username.getText().toString();
                    lastnameresult = editText_lastname.getText().toString();
                    username_result = edt_username.getText().toString();
                    birthdate = edit_dob.getText().toString();
                    bioupdate = editText_bio.getText().toString();

                    bioupdate_result = bioupdate;

                    if (username_result.equalsIgnoreCase(username)) {
                        /*Updating user profiel API*/

                        update_user_profile();

                    } else {

                        if (validate()) {
                            /*Calling API for user name availability checking */

                            get_username();

                        }else {
                            button_update.setVisibility(View.VISIBLE);
                            pro_edt_profile.setVisibility(View.GONE);
                        }
                    }


                } else {

                    int genderId = genderGroup.getCheckedRadioButtonId();

                    Log.e(TAG, "onClick:genderId "+ genderId);
                    Log.e(TAG, "onClick:maleId "+ radioM.getId());
                    Log.e(TAG, "onClick:femaleId "+ radioF.getId());
                    Log.e(TAG, "onClick:transgenderId "+ radioTransgender.getId());
                    Log.e(TAG, "onClick:ratherId "+radioRathernotsay.getId());

//                    if (genderId == radioM.getId()) {
//
//                        genderupdate = "male";
//
//                    } else {
//
//                        genderupdate = "female";
//
//                    }
                    firstnameresult = editText_username.getText().toString();
                    lastnameresult = editText_lastname.getText().toString();
                    username_result = edt_username.getText().toString();
                    birthdate = edit_dob.getText().toString();
                    bioupdate = editText_bio.getText().toString();

                    bioupdate_result = bioupdate;
                    String result = org.apache.commons.text.StringEscapeUtils.escapeJava(bioupdate_result);
                    Log.e(TAG, "bio\t" + result);
                    System.out.println(result);

                    if (username_result.equalsIgnoreCase(username)) {
                        /*Updating user profiel API*/

                        update_user_profile();

                    } else {

                        if (validate()) {
                            /*Calling API for user name availability checking */

                            get_username();

                        }else {
                            button_update.setVisibility(View.VISIBLE);
                            pro_edt_profile.setVisibility(View.GONE);
                        }

                    }
                }

                break;
            case R.id.button_update:

                button_update.setVisibility(View.GONE);
                pro_edt_profile.setVisibility(View.VISIBLE);

                if (Integer.parseInt(logintype) == 1 || Integer.parseInt(logintype) == 2) {

//                    Toast.makeText(this, "You are linked with your social account. Since you cannot update all your data. Please sign up for full access", Toast.LENGTH_LONG).show();
                    editText_username.setEnabled(false);
                    editText_lastname.setEnabled(false);
                    edit_dob.setEnabled(false);
                    editText_useremailID.setEnabled(false);
                    radioM.setEnabled(false);
                    radioF.setEnabled(false);
                    radioTransgender.setEnabled(false);
                    radioRathernotsay.setEnabled(false);



                    int genderId = genderGroup.getCheckedRadioButtonId();


//
//                    if (genderId == radioM.getId()) {
//
//                        genderupdate = "male";
//
//                    } else {
//
//                        genderupdate = "female";
//
//                    }

                    firstnameresult = editText_username.getText().toString();
                    lastnameresult = editText_lastname.getText().toString();
                    username_result = edt_username.getText().toString();
                    birthdate = edit_dob.getText().toString();
                    bioupdate = editText_bio.getText().toString();

                    bioupdate_result = bioupdate;

                    if (username_result.equalsIgnoreCase(username)) {
                        /*Updating user profiel API*/

                        update_user_profile();

                    } else {

                        if (validate()) {
                            /*Calling API for user name availability checking */

                            get_username();

                        }else {
                            button_update.setVisibility(View.VISIBLE);
                            pro_edt_profile.setVisibility(View.GONE);
                        }
                    }


                } else {

                    int genderId = genderGroup.getCheckedRadioButtonId();

                    Log.e(TAG, "onClick:genderId "+ genderId);
                    Log.e(TAG, "onClick:maleId "+ radioM.getId());
                    Log.e(TAG, "onClick:femaleId "+ radioF.getId());
                    Log.e(TAG, "onClick:transgenderId "+ radioTransgender.getId());
                    Log.e(TAG, "onClick:ratherId "+radioRathernotsay.getId());

//                    if (genderId == radioM.getId()) {
//
//                        genderupdate = "male";
//
//                    } else {
//
//                        genderupdate = "female";
//
//                    }
                    firstnameresult = editText_username.getText().toString();
                    lastnameresult = editText_lastname.getText().toString();
                    username_result = edt_username.getText().toString();
                    birthdate = edit_dob.getText().toString();
                    bioupdate = editText_bio.getText().toString();

                    bioupdate_result = bioupdate;
                    String result = org.apache.commons.text.StringEscapeUtils.escapeJava(bioupdate_result);
                    Log.e(TAG, "bio\t" + result);
                    System.out.println(result);

                    if (username_result.equalsIgnoreCase(username)) {
                        /*Updating user profiel API*/

                        update_user_profile();

                    } else {

                        if (validate()) {
                            /*Calling API for user name availability checking */

                            get_username();

                        }else {
                            button_update.setVisibility(View.VISIBLE);
                            pro_edt_profile.setVisibility(View.GONE);
                        }

                    }
                }

                break;



        }
    }


    /*To take gallery images*/
    private void galleryIntent() {

        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_FILE);

    }

    /*To take camera images*/
    private void cameraIntent() {

        if (ActivityCompat.checkSelfPermission(editProfileActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(editProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(editProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, REQUEST_CAMERA);

        } else {


            ActivityCompat.requestPermissions(editProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {

        int permissionSize = permissions.length;

        Log.e(TAG, "onRequestPermissionsResult:" + "requestCode->" + requestCode);

        switch (requestCode) {

            case REQUEST_CAMERA_PERMISSION:

                for (int i = 0; i < permissionSize; i++) {

                    if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission granted");

                    } else if (permissions[i].equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "CAMERA permission denied");
                        cameraStorageGrantRequest();
                    }

                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");

                        if (ActivityCompat.checkSelfPermission(editProfileActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraIntent();

                        }


                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");

                        if (ActivityCompat.checkSelfPermission(editProfileActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                            cameraStorageGrantRequest();

                        }

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

            case PICK_FROM_GALLERY:

                for (int i = 0; i < permissionSize; i++) {


                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission granted");
                        galleryIntent();

                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "WRITE_EXTERNAL_STORAGE permission denied");
                        cameraStorageGrantRequest();

                    }

                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission granted");

                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {

                        Log.e(TAG, "onRequestPermissionsResult:" + "READ_EXTERNAL_STORAGE permission denied");

                    }

                }

                break;

        }


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == REQUEST_CAMERA) {


                Bitmap thumbnail = null;
                try {
                    thumbnail = getThumbnail(imageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("CameraCaptureError", "CameraCaptureError->" + e.getMessage());
                }

                String folder_main = "FoodWall";

                File f = new File(Environment.getExternalStorageDirectory(), folder_main);
                if (!f.exists()) {
                    f.mkdirs();
                }

                File newFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                Log.e("ImageFile", "ImageFile->" + newFile);

                try {

                    FileOutputStream fileOutputStream = new FileOutputStream(newFile);
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    String imagepath = String.valueOf(newFile);

                    Pref_storage.setDetail(this, "UserImage", imagepath);


                    Utility.picassoImageLoader(imagepath,1,userimage,getApplicationContext() );





                    Log.e("MyPath", "MyImagePath->" + imagepath);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                /*Update user profile image */
                update_user_profile_image();


            } else if (requestCode == SELECT_FILE) {

                try {
                    onSelectFromGalleryResult(data);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /*Storage permission request*/
    private void cameraStorageGrantRequest() {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                editProfileActivity.this);

        builder.setTitle("Food Wall would like to access your camera and storage")
                .setMessage("You previously chose not to use Android's camera or storage with Food Wall. To set your profile pic, allow camera and storage permission in App Settings. Click Ok to allow.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // permission dialog
                        dialog.dismiss();
                        try {

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();


    }

    /*Getting user image thumbnail*/
    public Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException {

        InputStream input = editProfileActivity.this.getContentResolver().openInputStream(uri);
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 800) ? (originalSize / 800) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }


    /*Getting image URI*/
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Uri selectedImage = data.getData();


        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor c = getApplicationContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);

        c.moveToFirst();


        int columnIndex = c.getColumnIndex(filePathColumn[0]);
        String picturePath = c.getString(columnIndex);
        c.close();


        Bitmap photo1 = (BitmapFactory.decodeFile(picturePath));
//        userimage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        userimage.setImageURI(Uri.parse(picturePath));

        Pref_storage.setDetail(this, "UserImage", picturePath);
        update_user_profile_image();


    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getApplicationContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
//            Toast.makeText(mContext, "vis"+result, Toast.LENGTH_SHORT).show();
            cursor.close();
        }
        return result;

    }

    //Camera Alert
    public void camera_galleryalert() {

        final CharSequence[] items = {"Camera", "Select from Gallery",
                "Cancel"};

        AlertDialog.Builder front_builder = new AlertDialog.Builder(this);
        front_builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (items[which].equals("Camera")) {


                    try {

                        if (ActivityCompat.checkSelfPermission(editProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(editProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(editProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);

                        } else {

                            cameraIntent();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else if (items[which].equals("Select from Gallery")) {
                    try {
                        if (ActivityCompat.checkSelfPermission(editProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(editProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);

                        } else {

                            galleryIntent();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (items[which].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        front_builder.show();
    }


    /// Update User Profile Image

    private void update_user_profile_image() {


        if (Utility.isConnected(editProfileActivity.this)) {
//
//            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(editProfileActivity.this);
//            LayoutInflater inflater1 = LayoutInflater.from(editProfileActivity.this);
//            @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.loader_layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setTitle("");
//            progressBar= (ProgressBar) dialogView.findViewById(R.id.loader_login);
//            progressBar.setVisibility(View.GONE);
//
//            prg_textView= (TextView) dialogView.findViewById(R.id.prg_textView);
//
//            loaderimage = (CustomGIFview) findViewById(R.id.img_loader);
//
//            dialog1 = dialogBuilder.create();
//            dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            dialog1.show();



            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String user_id = Pref_storage.getDetail(this, "userId");

            String user = Pref_storage.getDetail(this, "UserImage");


            try {
                File file = new File(user);

                File compressedImageFile = null;

                compressedImageFile = new Compressor(this).setQuality(100).compressToFile(file);

                ProgressRequestBody fileBody = new ProgressRequestBody(compressedImageFile, this);
                MultipartBody.Part image = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), fileBody);


//            File sourceFile = new File(user);

//            RequestBody imageupload = RequestBody.create(MediaType.parse("Image/jpeg"), sourceFile);

//            MultipartBody.Part image = MultipartBody.Part.createFormData("image", sourceFile.getName(), imageupload);

//            Call<User_image_update_pojo> call = apiService.update_user_profile_image("update_user_profile_image", Integer.parseInt(user_id), image);
                Call<User_image_update_pojo> call = apiService.update_user_profile_image("update_user_profile_image", Integer.parseInt(user_id), image);


                call.enqueue(new Callback<User_image_update_pojo>() {
                    @Override
                    public void onResponse(Call<User_image_update_pojo> call, Response<User_image_update_pojo> response) {

                        if (response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {
                                userImageInputResponse = new User_image_input_response("");

                                String responsenew = response.body().getData().get(j).getStatus();

                                if (Integer.parseInt(responsenew) == 1) {
                                    Toast.makeText(editProfileActivity.this, "Profile picture has been updated successfully", Toast.LENGTH_SHORT).show();

//                                    dialog1.cancel();

                                    get_profile();

                                } else {
//                                    dialog1.cancel();

                                    Snackbar snackbar = Snackbar.make(emailParent, "Profile Image update failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                                    snackbar.setActionTextColor(Color.RED);
                                    View view1 = snackbar.getView();
                                    TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                                    textview.setTextColor(Color.WHITE);
                                    snackbar.show();
                                }
                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<User_image_update_pojo> call, Throwable t) {
                        //Error
                        dialog1.cancel();
                        Snackbar snackbar = Snackbar.make(emailParent, "Profile Image update failed. Please try again later.", Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        View view1 = snackbar.getView();
                        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                        textview.setTextColor(Color.WHITE);
                        snackbar.show();
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            Snackbar snackbar = Snackbar.make(emailParent, "Profile update failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }


    /*Calling user profile API*/
    private void get_profile() {
        if (Utility.isConnected(editProfileActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String user_id = Pref_storage.getDetail(editProfileActivity.this, "userId");

            Call<Profile_output_pojo> call = apiService.get_profile("get_profile", Integer.parseInt(user_id), Integer.parseInt(user_id));


            call.enqueue(new Callback<Profile_output_pojo>() {
                @Override
                public void onResponse(Call<Profile_output_pojo> call, Response<Profile_output_pojo> response) {

                    if (response.body().getResponseCode() == 1) {

                        for (int j = 0; j < response.body().getData().size(); j++) {


                            String userId = response.body().getData().get(j).getUserId();
                            logintype = response.body().getData().get(j).getLoginType();
                            password = response.body().getData().get(j).getPassword();
                            gender = response.body().getData().get(j).getGender();
                            picture = response.body().getData().get(j).getPicture();

                            Pref_storage.setDetail(getApplicationContext(), "picture_user_login", picture);

                            String acc_stat = response.body().getData().get(j).getProfileStatus();

                            if (acc_stat.equalsIgnoreCase("1")) {
                                switch_pvt_acc.setChecked(true);
                                txt_private_txt.setVisibility(View.VISIBLE);
                            } else {
                                switch_pvt_acc.setChecked(false);
                            }

//                            GlideApp.with(editProfileActivity.this).load(picture).centerCrop().into(userimage);


                        }
                    }
                }

                @Override
                public void onFailure(Call<Profile_output_pojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });

        } else {
            Snackbar snackbar = Snackbar.make(emailParent, "Profile update failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }

    /*Calling API for user name availability checking */

    private void get_username() {

        if (Utility.isConnected(editProfileActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String user_id = Pref_storage.getDetail(editProfileActivity.this, "userId");

            Call<Profile_get_username_output> call = apiService.get_username("get_username", Integer.parseInt(user_id), username_result);

            call.enqueue(new Callback<Profile_get_username_output>() {
                @Override
                public void onResponse(Call<Profile_get_username_output> call, Response<Profile_get_username_output> response) {

                    if (response.body().getResponseCode() == 1) {

                        String data = response.body().getData();

                        if (Integer.parseInt(data) == 1) {

                            edt_username.requestFocus();
                            edt_username.setError("Username already exists");

                        } else if (Integer.parseInt(data) == 0) {
                            /*Updating user profiel API*/
                            update_user_profile();

                        }

                    }
                }


                @Override
                public void onFailure(Call<Profile_get_username_output> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                }
            });
        } else {

            Snackbar snackbar = Snackbar.make(emailParent, "Profile update failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }


    }

    /*Updating user profiel API*/

    private void update_user_profile() {

        if (Utility.isConnected(editProfileActivity.this)) {


            if (validate()) {


                if(radioM.isChecked())
                {
                    genderupdate="male";
                }else if(radioF.isChecked())
                {
                    genderupdate="female";
                }else if(radioTransgender.isChecked())
                {
                    genderupdate="transgender";
                }else if(radioRathernotsay.isChecked())
                {
                    genderupdate="rathernotsay";
                }

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                String user_id = Pref_storage.getDetail(this, "userId");

                RequestBody first_name = RequestBody.create(MediaType.parse("text/plain"), firstnameresult);
                RequestBody user_name_change = RequestBody.create(MediaType.parse("text/plain"), username_result);
                RequestBody last_name = RequestBody.create(MediaType.parse("text/plain"), lastnameresult);
                RequestBody dob = RequestBody.create(MediaType.parse("text/plain"), birthdate);
                RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), genderupdate);
                RequestBody bio_description = RequestBody.create(MediaType.parse("text/plain"), bioupdate_result);
                RequestBody acc_status = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(account_status));


                Call<Timeline_create_output_pojo> call = apiService.update_user_profile("update_user_profile", Integer.parseInt(user_id), user_name_change, first_name, last_name, gender, dob, bio_description, acc_status);
                call.enqueue(new Callback<Timeline_create_output_pojo>() {
                    @Override
                    public void onResponse(Call<Timeline_create_output_pojo> call, Response<Timeline_create_output_pojo> response) {

                        if (response.body().getResponseCode() == 1) {

                            button_update.setVisibility(View.VISIBLE);
                            pro_edt_profile.setVisibility(View.GONE);

                            Pref_storage.setDetail(editProfileActivity.this, "firstName", firstnameresult);
                            Pref_storage.setDetail(editProfileActivity.this, "lastName", lastnameresult);

                            Toast.makeText(editProfileActivity.this, "Profile has been updated successfully", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(editProfileActivity.this, Profile.class));
                            finish();

                        } else {
                            Snackbar snackbar = Snackbar.make(emailParent, "Profile update failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                            snackbar.setActionTextColor(Color.RED);
                            View view1 = snackbar.getView();
                            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                            textview.setTextColor(Color.WHITE);
                            snackbar.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Timeline_create_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });


            } else {

                button_update.setVisibility(View.VISIBLE);
                pro_edt_profile.setVisibility(View.GONE);
            }


        } else {

            Snackbar snackbar = Snackbar.make(emailParent, "Profile update failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
            button_update.setVisibility(View.VISIBLE);
            pro_edt_profile.setVisibility(View.GONE);

        }


    }




    @Override
    public void onProgressUpdate(int percentage) {
        Log.e(TAG, "onProgressUpdate: --->" + percentage);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
