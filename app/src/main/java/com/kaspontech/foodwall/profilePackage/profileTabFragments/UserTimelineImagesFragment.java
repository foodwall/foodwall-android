package com.kaspontech.foodwall.profilePackage.profileTabFragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.TimeLine.TimeLineImagesUser;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_timeline_image_all;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_timeline_output_response;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserTimelineImagesFragment extends Fragment {



    View view;
    Context context;


    String userID, timelineid;
    LinearLayout ll_no_post;
    RecyclerView gridview_userprofile;
    TimeLineImagesUser timeLineImagesUser;


    public static ArrayList<Get_timeline_image_all> allimagelist_user = new ArrayList<>();
    public static int totalsize_user;

    public UserTimelineImagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_timeline_images, container, false);
        context = view.getContext();

        gridview_userprofile = (RecyclerView) view.findViewById(R.id.gridview_profile);
        ll_no_post = (LinearLayout)view.findViewById(R.id.ll_no_post);

        if (getArguments() != null) {
            userID = getArguments().getString("userid");
        }

        get_alluser_timeline_images();

        return view;
    }


    private void get_alluser_timeline_images() {


        if (isConnected(context)) {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<Get_timeline_output_response> call = apiService.get_alluser_timeline_images("get_timeline_image_user", Integer.valueOf(userID));

            call.enqueue(new Callback<Get_timeline_output_response>() {
                @Override
                public void onResponse(Call<Get_timeline_output_response> call, Response<Get_timeline_output_response> response) {

                    if (response.body().getResponseMessage().equals("success")) {

                        ll_no_post.setVisibility(View.GONE);

                        ArrayList<String> imageList = new ArrayList<>();

                        for (int j = 0; j < response.body().getData().size(); j++) {

                            totalsize_user = response.body().getData().size();

                            userID = response.body().getData().get(j).getUserid();

                            timelineid = response.body().getData().get(j).getTimelineid();
                            String imageall = response.body().getData().get(j).getTimelineimage();


                            imageList.add(imageall);

                            Get_timeline_image_all getTimelineImageAll = new Get_timeline_image_all(timelineid, userID, imageall);
                            allimagelist_user.add(getTimelineImageAll);

                            timeLineImagesUser = new TimeLineImagesUser(context,imageList,allimagelist_user);
                            gridview_userprofile.setLayoutManager(new GridLayoutManager(context, 3));
                            gridview_userprofile.setAdapter(timeLineImagesUser);
                            gridview_userprofile.setNestedScrollingEnabled(true);
                            timeLineImagesUser.notifyDataSetChanged();



                        }

                    }else {

                        ll_no_post.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<Get_timeline_output_response> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    ll_no_post.setVisibility(View.VISIBLE);

                }
            });

        } else {
            Toast.makeText(getContext(), "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }

    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

}
