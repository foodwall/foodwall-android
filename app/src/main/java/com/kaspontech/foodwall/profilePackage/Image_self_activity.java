package com.kaspontech.foodwall.profilePackage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.kaspontech.foodwall.adapters.TimeLine.Single_self_timeline_adapter;
import com.kaspontech.foodwall.foodFeedsPackage.newsfeedPostPackage.Comments_activity;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.CreateEditTimeline_Comments;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Image;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Timeline_single_user_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.profile_timeline_output_response;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.CreateUserPojoOutput;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.VISIBLE;

public class Image_self_activity extends _SwipeActivityClass implements View.OnClickListener, onCommentsPostListener {


    Toolbar actionBar;
    ImageButton back;
    TextView toolbar_title;
    ProgressBar progress_dialog;
    RelativeLayout rl_timefeed_user;

    RecyclerView rv_user_timeline;
    Timeline_single_user_pojo timelineSingleUserPojo;
    String picture;
    List<Image> imageList = new ArrayList<>();

    ArrayList<Timeline_single_user_pojo> gettingtimelinesinglelist = new ArrayList<>();

    public static ArrayList<String> timeline_self_list = new ArrayList<>();
    List<Image> myList = new ArrayList<>();
    ArrayList<String> myListnew = new ArrayList<String>();
    Single_self_timeline_adapter singleSelfTimelineAdapter;
    TextView txt_no_data;


    TextView username, userlastname, location, location_state, like, liketxt, user_txt_comment, count, comma, count_post, txt_followers_count, txt_followers, selfusername_caption, selfuserlastname_caption, user_name_comment, viewcomment, txt_timeline_follow, comment, commenttest, share, caption, captiontag, txt_created_on, txt_adapter_post;
    RelativeLayout rl_likes_layout, rl_comments_lay;
    RelativeLayout rl_share_adapter, rl_comment_adapter, rl_comments_layout, like_rl, rl_username, rl_more, rl_post_timeline, rl_follow_layout;

    LinearLayout rl_row_layout;
    ImageView img_view_more, userphoto_profile, img_follow_timeline, img_commentadapter, img_shareadapter, userphoto_comment;

    EditText edittxt_comment;
    ProgressBar progressbar_review;
    ImageButton img_like;
    PhotoView image;

    private static ViewPager mviewpager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;


    private CircleIndicator circleIndicator;


    String timelineid, userid, hotelname;


    String TAG = "Image_self_activity";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_image_tile_activity);

        actionBar = (Toolbar) findViewById(R.id.actionbar_user_timeline);
        back = (ImageButton) actionBar.findViewById(R.id.back);
        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);
        txt_no_data = (TextView) findViewById(R.id.txt_no_data);

        back.setOnClickListener(this);

        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(R.string.images);
        rl_timefeed_user = (RelativeLayout) findViewById(R.id.rl_timefeed_user);

        rv_user_timeline = (RecyclerView) findViewById(R.id.rv_user_timeline);


        img_like = (ImageButton) findViewById(R.id.img_like);

        //Edittext
        edittxt_comment = (EditText) findViewById(R.id.edittxt_comment);


        //ViewPager
        mviewpager = (ViewPager) findViewById(R.id.mviewpager);


        //Circle Indicator
        circleIndicator = (CircleIndicator) findViewById(R.id.indicator);

//            Image.setImage(ImageSource.resource(R.drawable.burger));


        //ImageView
        image = (PhotoView) findViewById(R.id.image);
        userphoto_profile = (ImageView) findViewById(R.id.userphoto_profile);
        userphoto_comment = (ImageView) findViewById(R.id.userphoto_comment);
        img_commentadapter = (ImageView) findViewById(R.id.img_commentadapter);
        img_shareadapter = (ImageView) findViewById(R.id.img_shareadapter);
        img_view_more = (ImageView) findViewById(R.id.img_view_more);
        img_follow_timeline = (ImageView) findViewById(R.id.img_follow_timeline);
        //TextView
        username = (TextView) findViewById(R.id.username);
        txt_adapter_post = (TextView) findViewById(R.id.txt_adapter_post);
        userlastname = (TextView) findViewById(R.id.userlastname);
        location = (TextView) findViewById(R.id.location);
//            location_state = (TextView) itemView.findViewById(R.id.location_state);
        caption = (TextView) findViewById(R.id.caption);
        captiontag = (TextView) findViewById(R.id.captiontag);
        txt_created_on = (TextView) findViewById(R.id.txt_created_on);
        like = (TextView) findViewById(R.id.like);
        liketxt = (TextView) findViewById(R.id.liketxt);
        comment = (TextView) findViewById(R.id.comment);
        comma = (TextView) findViewById(R.id.comma);
        user_txt_comment = (TextView) findViewById(R.id.user_txt_comment);
        user_name_comment = (TextView) findViewById(R.id.user_name_comment);
        selfusername_caption = (TextView) findViewById(R.id.selfusername_caption);
        selfuserlastname_caption = (TextView) findViewById(R.id.selfuserlastname_caption);
        viewcomment = (TextView) findViewById(R.id.View_all_comment);
        commenttest = (TextView) findViewById(R.id.commenttest);
        share = (TextView) findViewById(R.id.shares);
        count = (TextView) findViewById(R.id.count);
        count_post = (TextView) findViewById(R.id.count_post);
        txt_followers = (TextView) findViewById(R.id.txt_followers);
        txt_followers_count = (TextView) findViewById(R.id.txt_followers_count);

        //RelativeLayout
        rl_row_layout = (LinearLayout) findViewById(R.id.layout_one_foradapter);
        rl_likes_layout = (RelativeLayout) findViewById(R.id.rl_likes_layout);
        rl_comments_lay = (RelativeLayout) findViewById(R.id.rl_comments_layout);
        rl_share_adapter = (RelativeLayout) findViewById(R.id.rl_share_adapter);
        rl_comment_adapter = (RelativeLayout) findViewById(R.id.rl_comment_adapter);
        like_rl = (RelativeLayout) findViewById(R.id.like_rl);
        rl_username = (RelativeLayout) findViewById(R.id.rl_username);
        rl_more = (RelativeLayout) findViewById(R.id.rl_more);
        rl_follow_layout = (RelativeLayout) findViewById(R.id.rl_follow_layout);
        rl_post_timeline = (RelativeLayout) findViewById(R.id.rl_post_timeline);


        progress_dialog = (ProgressBar) findViewById(R.id.progress_dialognew);
        progress_dialog.setVisibility(View.VISIBLE);

        Intent intent = getIntent();
        timelineid = intent.getStringExtra("Clickedfrom_profile");
        hotelname = intent.getStringExtra("Clickedfrom_hotelname");
        userid = intent.getStringExtra("Clickedfrom_profileuserid");

        Log.e(TAG, "timelineid: " + timelineid);
        Log.e(TAG, "userid: " + userid);
        Log.e(TAG, "hotelname: " + hotelname);

        toolbar_title.setText(hotelname);
        toolbar_title.setGravity(View.TEXT_ALIGNMENT_CENTER);
        toolbar_title.setVisibility(View.VISIBLE);


        if (userid == null) {

            userid = Pref_storage.getDetail(Image_self_activity.this, "userId");

        }

        singleSelfTimelineAdapter = new Single_self_timeline_adapter(Image_self_activity.this, gettingtimelinesinglelist, timeline_self_list, Image_self_activity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Image_self_activity.this);
        rv_user_timeline.setLayoutManager(mLayoutManager);
        rv_user_timeline.setItemAnimator(new DefaultItemAnimator());
        rv_user_timeline.setNestedScrollingEnabled(false);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                get_timeline_single();


            }

        }, 1000);


    }

    @Override
    public void onSwipeRight() {
        finish();
    }

    @Override
    protected void onSwipeLeft() {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.back:
                finish();

                break;
        }
    }


    private void get_timeline_single() {

        if (isConnected(this)) {

            progress_dialog.setVisibility(VISIBLE);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<profile_timeline_output_response> call = apiService.get_timeline_single("get_timeline_single",
                    Integer.parseInt(timelineid),
                    Integer.parseInt(userid));
            call.enqueue(new Callback<profile_timeline_output_response>() {
                @Override
                public void onResponse(Call<profile_timeline_output_response> call, Response<profile_timeline_output_response> response) {

                    if (response.isSuccessful()) {




                       /* for (int j = 0; j < response.body().getData().size(); j++) {


                            String timelineId = response.body().getData().get(j).getTimelineId();

                            String timelineHotel = response.body().getData().get(j).getTimelineHotel();
                            String timelineDescription = response.body().getData().get(j).getTimelineDescription();
                            String address = response.body().getData().get(j).getAddress();
                            String createdBy = response.body().getData().get(j).getCreatedBy();
                            String createdOn = response.body().getData().get(j).getCreatedOn();
                            int totalLikes = response.body().getData().get(j).getTotalLikes();
                            String totalComments = response.body().getData().get(j).getTotalComments();
                            String latitude = response.body().getData().get(j).getLatitude();
                            String longitude = response.body().getData().get(j).getLongitude();
                            String visibilityType = response.body().getData().get(j).getVisibilityType();
                            String postType = response.body().getData().get(j).getPostType();
                            String userId = response.body().getData().get(j).getUserId();
                            String oauthProvider = response.body().getData().get(j).getOauthProvider();
                            String oauthUid = response.body().getData().get(j).getOauthUid();
                            String firstName = response.body().getData().get(j).getFirstName();
                            String lastName = response.body().getData().get(j).getLastName();
                            String email = response.body().getData().get(j).getEmail();
                            String imei = response.body().getData().get(j).getImei();
                            String contNo = response.body().getData().get(j).getContNo();
                            String gender = response.body().getData().get(j).getGender();
                            String dob = response.body().getData().get(j).getDob();
                            String bioDescription = response.body().getData().get(j).getBioDescription();
                            String totalFollowers = response.body().getData().get(j).getTotalFollowers();
                            String totalFollowings = response.body().getData().get(j).getTotalFollowings();
                            String totalPosts = response.body().getData().get(j).getTotalPosts();
                            String picture = response.body().getData().get(j).getPicture();
                            int likesTlId = response.body().getData().get(j).getLikesTlId();
                            String tlLikes = response.body().getData().get(j).getTlLikes();
                            int eventId = response.body().getData().get(j).getEventId();
                            String eventName = response.body().getData().get(j).getEventName();
                            String eventDescription = response.body().getData().get(j).getEventDescription();
                            String eventImage = response.body().getData().get(j).getEventImage();
                            String startDate = response.body().getData().get(j).getStartDate();
                            String endDate = response.body().getData().get(j).getEndDate();
                            String location = response.body().getData().get(j).getLocation();
                            String followingId = response.body().getData().get(j).getFollowingId();
                            int followuserId = response.body().getData().get(j).getFollowuserId();
                            String followuserFirstname = response.body().getData().get(j).getFollowuserFirstname();
                            String followuserLastname = response.body().getData().get(j).getFollowuserLastname();
                            String followuserPicture = response.body().getData().get(j).getFollowuserPicture();
                            String hotel = response.body().getData().get(j).getHotel();
                            int reviewId = response.body().getData().get(j).getReviewId();
                            String review = response.body().getData().get(j).getReview();
                            String reviewComments = response.body().getData().get(j).getReviewComments();
                            int questId = response.body().getData().get(j).getQuestId();
                            String askQuestion = response.body().getData().get(j).getAskQuestion();
                            int ansId = response.body().getData().get(j).getAnsId();
                            String askAnswer = response.body().getData().get(j).getAskAnswer();
                            int gngEvtId = response.body().getData().get(j).getGngEvtId();
                            int likeEvtId = response.body().getData().get(j).getLikeEvtId();
                            int follUserid = response.body().getData().get(j).getFollUserid();
                            int likeRevId = response.body().getData().get(j).getLikeRevId();
                            int cmmtRevId = response.body().getData().get(j).getCmmtRevId();
                            int ansQuesId = response.body().getData().get(j).getAnsQuesId();
                            int upAnsId = response.body().getData().get(j).getUpAnsId();
                            List<TaggedPeoplePojo> whom = response.body().getData().get(j).getWhom();
                            Integer whomCount = response.body().getData().get(j).getWhomCount();
                            List<Image> image = response.body().getData().get(j).getImage();
                            Integer imageCount = response.body().getData().get(j).getImageCount();

                            if(imageCount != 0){

                                String getimage = response.body().getData().get(j).getImage().get(0).getImg();
                                List<String> newmyList = new ArrayList<String>(Arrays.asList(getimage));
                                timeline_self_list.add(String.valueOf(newmyList));


                            }



//                            myList= myListnew.add(String.valueOf(newmyList));



                            Timeline_single_user_pojo timelineSingleUserPojo = new Timeline_single_user_pojo(
                                    timelineId, timelineHotel, timelineDescription, address, createdBy, createdOn,
                                    totalLikes, totalComments, latitude, longitude, visibilityType, postType, userId,
                                    oauthProvider, oauthUid, firstName, lastName, email, imei, contNo,
                                    gender, dob, bioDescription, totalFollowers, totalFollowings, totalPosts, picture, likesTlId,
                                    tlLikes, eventId,eventName, eventDescription, eventImage, startDate, endDate, location,
                                    followingId, followuserId, followuserFirstname, followuserLastname, followuserPicture,
                                    hotel, reviewId, review, reviewComments, questId, askQuestion, ansId,
                                    askAnswer, gngEvtId, likeEvtId, follUserid, likeRevId, cmmtRevId, ansQuesId,
                                    upAnsId, whom, whomCount, image, imageCount);
*/
                        gettingtimelinesinglelist = response.body().getData();

                        toolbar_title.setText(response.body().getData().get(0).getHotel());
                        toolbar_title.setGravity(View.TEXT_ALIGNMENT_CENTER);
                        toolbar_title.setVisibility(View.VISIBLE);

                        Log.e(TAG, "onResponse:single_size--> " + gettingtimelinesinglelist.size());

                        Set<Timeline_single_user_pojo> s = new LinkedHashSet<>(gettingtimelinesinglelist);
                        gettingtimelinesinglelist.clear();
                        gettingtimelinesinglelist.addAll(s);


                        // Setting Adapter
                        singleSelfTimelineAdapter = new Single_self_timeline_adapter(Image_self_activity.this, gettingtimelinesinglelist, timeline_self_list, Image_self_activity.this);
                        rv_user_timeline.setAdapter(singleSelfTimelineAdapter);
                        singleSelfTimelineAdapter.notifyDataSetChanged();

                        progress_dialog.setVisibility(View.GONE);


                    }
                }


                @Override
                public void onFailure(Call<profile_timeline_output_response> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                    if (gettingtimelinesinglelist.isEmpty()) {
                        progress_dialog.setVisibility(View.GONE);

                        txt_no_data.setVisibility(View.VISIBLE);

                    } else {
                        progress_dialog.setVisibility(View.GONE);

                        Snackbar snackbar = Snackbar.make(rl_timefeed_user, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        View view1 = snackbar.getView();
                        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                        textview.setTextColor(Color.WHITE);
                        snackbar.show();
                    }


                }
            });
        } else {

           /* Snackbar snackbar = Snackbar.make(rl_timefeed_user, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();*/
        }

    }


    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        singleSelfTimelineAdapter.notifyDataSetChanged();

        if (!gettingtimelinesinglelist.isEmpty()) {
            get_timeline_single();

        }
    }

    @Override
    public void onPostCommitedView(View view, final int adapterPosition, View hotelDetailsView) {

        switch (view.getId()) {

            case R.id.txt_adapter_post:

                //Edittext
                edittxt_comment = (EmojiconEditText) hotelDetailsView.findViewById(R.id.edittxt_comment);
                progressbar_review = (ProgressBar) hotelDetailsView.findViewById(R.id.progressbar_review);
                rl_comments_layout = (RelativeLayout) hotelDetailsView.findViewById(R.id.rl_comments_layout);
                commenttest = (TextView) hotelDetailsView.findViewById(R.id.commenttest);
                viewcomment = (TextView) hotelDetailsView.findViewById(R.id.View_all_comment);
                comment = (TextView) hotelDetailsView.findViewById(R.id.comment);


                progressbar_review.setVisibility(VISIBLE);
                progressbar_review.setIndeterminate(true);

                if (Utility.isConnected(Image_self_activity.this)) {

                    Utility.hideKeyboard(view);
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    try {

                        String createuserid = Pref_storage.getDetail(Image_self_activity.this, "userId");

                        Call<CreateEditTimeline_Comments> call = apiService.create_edit_timeline_comments1("create_edit_timeline_comments",
                                0,
                                Integer.parseInt(gettingtimelinesinglelist.get(adapterPosition).getTimelineId()),
                                edittxt_comment.getText().toString(),
                                Integer.parseInt(createuserid));
                        call.enqueue(new Callback<CreateEditTimeline_Comments>() {
                            @Override
                            public void onResponse(Call<CreateEditTimeline_Comments> call, Response<CreateEditTimeline_Comments> response) {

                                if (response.code() == 500) {

                                    Toast.makeText(Image_self_activity.this, "Internal Server Error", Toast.LENGTH_SHORT).show();

                                    progressbar_review.setVisibility(View.GONE);
                                    progressbar_review.setIndeterminate(false);

                                } else {

                                    progressbar_review.setVisibility(View.GONE);
                                    progressbar_review.setIndeterminate(false);

                                    if (response.body().getResponseCode() == 1) {

                                        edittxt_comment.setText("");

                                        int Cmnt = Integer.parseInt(response.body().getData().get(0).getTotalComments());

                                        if (rl_comments_layout.getVisibility() == View.GONE) {

                                            viewcomment.setText(R.string.view);
                                            viewcomment.setVisibility(VISIBLE);
                                            commenttest.setText(R.string.commenttt);
                                            commenttest.setVisibility(VISIBLE);

                                        } else {

                                            comment.setText(String.valueOf(Cmnt));
                                            comment.setVisibility(VISIBLE);
                                            viewcomment.setVisibility(VISIBLE);
                                            commenttest.setVisibility(VISIBLE);
                                        }

                                        Intent intent = new Intent(getApplicationContext(), Comments_activity.class);
                                        // Bundle bundle = new Bundle();
                                        intent.putExtra("comment_timelineid", gettingtimelinesinglelist.get(adapterPosition).getTimelineId());
                                        intent.putExtra("comment_timelinepicture", gettingtimelinesinglelist.get(adapterPosition).getPicture());
                                        intent.putExtra("comment_posttpe", gettingtimelinesinglelist.get(adapterPosition).getPostType());
                                        intent.putExtra("comment_profile", gettingtimelinesinglelist.get(adapterPosition).getPicture());
                                        intent.putExtra("comment_created_on", gettingtimelinesinglelist.get(adapterPosition).getCreatedOn());
                                        intent.putExtra("username", gettingtimelinesinglelist.get(adapterPosition).getFirstName());

                                        try {
                                            intent.putExtra("comment_image", gettingtimelinesinglelist.get(adapterPosition).getImage().get(0).getImg());

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        startActivity(intent);

                                    }
                                }

                            }

                            @Override
                            public void onFailure(Call<CreateEditTimeline_Comments> call, Throwable t) {
                                //Error
                                Log.e("FailureError", "" + t.getMessage());

                                progressbar_review.setVisibility(View.GONE);
                                progressbar_review.setIndeterminate(false);

                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {

                    Toast.makeText(this, "Check intenet Connection", Toast.LENGTH_SHORT).show();
                }
        }
    }
}
