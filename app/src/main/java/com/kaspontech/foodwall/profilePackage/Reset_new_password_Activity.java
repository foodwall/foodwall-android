package com.kaspontech.foodwall.profilePackage;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.loginPackage.Login;
import com.kaspontech.foodwall.modelclasses.TimeLinePojo.Timeline_create_output_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Utility;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kaspontech.foodwall.profilePackage.forgotPasswordActivity.mailid_result;
import static com.kaspontech.foodwall.profilePackage.forgotPasswordActivity.verificode_result;
import static com.kaspontech.foodwall.utills.Utility.isValidPassword;

/**
 * Created by vishnukm on 28/3/18.
 */

public class Reset_new_password_Activity extends AppCompatActivity implements View.OnClickListener {


    /**
     * Application tag
     **/
    private static final String TAG = "Reset_newPWD_Activity";

    /**
     * Action bar
     **/
    Toolbar actionBar;
    /**
     * Back Button
     **/
    ImageButton back;

    /**
     * Update password Button
     **/
    Button btn_updatepassword;
    /**
     * Title textview
     **/
    TextView toolbar_title;
    /**
     * Edit text views
     **/
    EditText editText_oldpassword, editText_newcnfpassword, editText_cnfpassword;

    /**
     * Text input layout
     **/
    TextInputLayout oldpwd_textinput;

    /**
     * Parent relative layout
     **/
    RelativeLayout pwdParent;

    /**
     * String result's
     **/
    String old_passwordresult, new_passwordresult, new_cnfpasswordresult;

    /**
     * boolean condition for password validation
     **/
    boolean pwd_valid = true;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_layout);

        // Action bar initialization

        actionBar = (Toolbar) findViewById(R.id.action_bar_profile);
        // Back button initialization

        back = (ImageButton) actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        // Title textview initialization

        toolbar_title = (TextView) actionBar.findViewById(R.id.toolbar_title);
        toolbar_title.setText(R.string.click_here_to_change_password);

        //  TextInputLayout initialization

        oldpwd_textinput = (TextInputLayout) findViewById(R.id.oldpwd_textinput);

        //EditText view's Initialization

        editText_oldpassword = (EditText) findViewById(R.id.editText_oldpassword);

        editText_newcnfpassword = (EditText) findViewById(R.id.editText_newcnfpassword);

        editText_cnfpassword = (EditText) findViewById(R.id.editText_cnfpassword);

        //Visibility off for old password field

        editText_oldpassword.setVisibility(View.GONE);
        oldpwd_textinput.setVisibility(View.GONE);

        // Update password button initialization

        btn_updatepassword = (Button) findViewById(R.id.btn_updatepassword);
        btn_updatepassword.setOnClickListener(this);

        pwdParent = (RelativeLayout) findViewById(R.id.pwdParent);

        /*
          Here is a password validation when the users starts typing

          */
        editText_newcnfpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {



            }

            @Override
            public void afterTextChanged(Editable s) {

                // Validation
                if (isValidPassword(editText_newcnfpassword.getText().toString())) {

                    editText_newcnfpassword.setTextColor(getResources().getColor(R.color.Green));
                    pwd_valid = true;
                    editText_newcnfpassword.setError(null);


                } else {

                    editText_newcnfpassword.setTextColor(getResources().getColor(R.color.colorAccent));

                    pwd_valid = false;

                    if(editText_newcnfpassword.getText().toString().length() > 5){

                        editText_newcnfpassword.setError("Your password should be as per the instructions");
                        editText_newcnfpassword.setTextColor(getResources().getColor(R.color.red));

                    }
                }


            }
        });

        /*
          Here is a Confirm password validation when the users starts typing

          */
        editText_cnfpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {



            }

            @Override
            public void afterTextChanged(Editable s) {


                if (editText_newcnfpassword.getText().toString().length() > editText_cnfpassword.getText().toString().length()) {

                    editText_cnfpassword.setTextColor(getResources().getColor(R.color.colorAccent));

                }


                if (editText_newcnfpassword.getText().toString().length() <= editText_cnfpassword.getText().toString().length()) {

                    if (!editText_cnfpassword.getText().toString().equals(editText_newcnfpassword.getText().toString())) {

                        editText_cnfpassword.requestFocus();
                        editText_cnfpassword.setError("Your password does not matched");
                        editText_cnfpassword.setTextColor(getResources().getColor(R.color.red));


                    } else {

                        editText_cnfpassword.setError(null);
                        editText_cnfpassword.setTextColor(getResources().getColor(R.color.Green));

                    }
                }


            }
        });


    }


    /**
     * Onclick method
     * **/
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            // Back button on click

            case R.id.back:
                finish();
                break;

            // Update password button click

            case R.id.btn_updatepassword:

                Log.e("ForgotPassword","CheckValid->"+pwd_valid);

                // Validation and API calling

                if (pwd_valid) {

                    if(validate()){

                        if (Utility.isConnected(this)) {

                            // API calling
                            update_new_password();

                        } else {

                            Snackbar snackbar = Snackbar.make(pwdParent, "Updating Password Failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                            snackbar.setActionTextColor(Color.RED);
                            View view1 = snackbar.getView();
                            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                            textview.setTextColor(Color.WHITE);
                            snackbar.show();

                        }
                    }


                } else {

                    editText_newcnfpassword.setError("Your password should be as per the instructions");


                    /*Snackbar snackbar = Snackbar.make(pwdParent, "Please check your password credentials", Snackbar.LENGTH_LONG);
                    snackbar.setActionTextColor(Color.RED);
                    View view1 = snackbar.getView();
                    TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                    textview.setTextColor(Color.WHITE);
                    snackbar.show();*/

                }


                break;
        }


    }



    /**
     * Validation method
     * **/

    private boolean validate() {

        old_passwordresult = editText_oldpassword.getText().toString();
        new_passwordresult = editText_newcnfpassword.getText().toString();
        new_cnfpasswordresult = editText_cnfpassword.getText().toString();

        boolean valid = true;

        if(pwd_valid){

            if (new_passwordresult.length() == 0) {

                editText_newcnfpassword.requestFocus();
                editText_newcnfpassword.setError("Password field should not be empty");
                valid = false;

            } else {

                editText_newcnfpassword.setError(null);

            }

            if (!new_passwordresult.equals(new_cnfpasswordresult)) {

                editText_cnfpassword.requestFocus();
                editText_cnfpassword.setError("Your password does not matched");

                valid = false;

            } else {

                editText_cnfpassword.setError(null);

            }

        }



        return valid;

    }

    /**
     *Calling API for changinig new password
      **/
    private void update_new_password() {


        // Alert dialog

        final AlertDialog dialog1 = new SpotsDialog.Builder().setContext(Reset_new_password_Activity.this).setMessage("").build();

        dialog1.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Timeline_create_output_pojo> call = apiService.update_new_password("update_new_password", mailid_result, Integer.parseInt(verificode_result), new_passwordresult);

        call.enqueue(new Callback<Timeline_create_output_pojo>() {
            @Override
            public void onResponse(Call<Timeline_create_output_pojo> call, Response<Timeline_create_output_pojo> response) {


                if (response.body() != null) {

                    if (response.body().getResponseCode() == 1) {

                        dialog1.dismiss();
                        Toast.makeText(getApplicationContext(), "Your password has been updated successfully", Toast.LENGTH_LONG).show();

                      // Redirect to login page
                        startActivity(new Intent(getApplicationContext(), Login.class));
                        finish();

                    }else{

                        Toast.makeText(Reset_new_password_Activity.this, "Failed to update password", Toast.LENGTH_SHORT).show();

                    }

                }else{

                    Toast.makeText(Reset_new_password_Activity.this, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();


                }



            }

            @Override
            public void onFailure(Call<Timeline_create_output_pojo> call, Throwable t) {
                //Error
                Log.e("FailureError", "" + t.getMessage());
                dialog1.dismiss();

            }
        });


    }

}
