package com.kaspontech.foodwall.profilePackage.profileTabFragments;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.TimeLine.TimeLineImagesUser;
import com.kaspontech.foodwall.adapters.TimeLine.Timeline_all_image_adapter;
import com.kaspontech.foodwall.adapters.bucketadapter.bucketmyselfAdapter;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_timeline_image_all;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_timeline_output_response;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.modelclasses.Review_pojo.GetTimelineTaggedImage_Datum;
import com.kaspontech.foodwall.modelclasses.Review_pojo.GetTimelineTaggedImage_Response;
import com.kaspontech.foodwall.profilePackage.Profile;
import com.kaspontech.foodwall.reviewpackage.ProfileReviewPojo.GetReviewProfileImage;
import com.kaspontech.foodwall.reviewpackage.profileReviewAdapter.ProfileReviewImageUserAdapter;
import com.kaspontech.foodwall.reviewpackage.profileReviewAdapter.ProfileReviewTaggedImageUserAdapter;
import com.kaspontech.foodwall.utills.Pref_storage;

import java.util.ArrayList;
import java.util.Arrays;

import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimelineImagesFragment extends Fragment implements View.OnClickListener {

    Context context;
    View view;


    RecyclerView tagged_bucket_recyclerview;
    bucketmyselfAdapter bucketMyselfAdapter;
    public static ArrayList<Get_timeline_image_all> allimagelist = new ArrayList<>();
    ArrayList<String> myList = new ArrayList<>();

    LinearLayout ll_no_post;
    Get_timeline_image_all getTimelineImageAll;
    Timeline_all_image_adapter timelineAllImageAdapter;
    TimeLineImagesUser timeLineImagesUser;
    String userID, timelineid;
    ProgressBar progress_dialognew;
    public static int totalselfsize;


    /**
     * Segment groups
     */
    SegmentedGroup segmentedGroup;
    /**
     * Radio button for question type
     */
    RadioButton self;
    /**
     * Radio button for poll type
     */
    RadioButton tagged;


    private static final int NUM_GRID_COLUMNS = 3;

    //    GridView gridView;
    RecyclerView gridview_profile;

    //tagged imagelist
    ArrayList<GetTimelineTaggedImage_Datum> getTimelineTaggedImageDatumArrayList = new ArrayList<>();
     ProfileReviewTaggedImageUserAdapter profileReviewTaggedImageUserAdapter;

    public TimelineImagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_timeline_images, container, false);
        context = view.getContext();

//        gridView = (GridView) view.findViewById(R.id.gridview_profile);
        gridview_profile = (RecyclerView) view.findViewById(R.id.gridview_profile);
        tagged_bucket_recyclerview = (RecyclerView) view.findViewById(R.id.tagged_bucket_recyclerview);

        ll_no_post = (LinearLayout) view.findViewById(R.id.ll_no_post);

        /*Segment group initialization*/
        segmentedGroup = (SegmentedGroup) view.findViewById(R.id.segmented_question);
        self = (RadioButton) view.findViewById(R.id.self);
        tagged = (RadioButton) view.findViewById(R.id.tagged);

        self.setOnClickListener(this);
        tagged.setOnClickListener(this);

        if (getArguments() != null) {
            userID = getArguments().getString("userid");
        }

       // self.setChecked(true);
        get_timeline_images();


        return view;
    }

    private void get_timeline_images() {


        if (isConnected(context)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<Get_timeline_output_response> call = apiService.get_all_timeline_images("get_timeline_image_user",
                    Integer.parseInt(userID));


            call.enqueue(new Callback<Get_timeline_output_response>() {
                @Override
                public void onResponse(Call<Get_timeline_output_response> call, Response<Get_timeline_output_response> response) {

                    if (response.body().getResponseMessage().equals("success")) {

                        ll_no_post.setVisibility(View.GONE);

                        ArrayList<String> imageList = new ArrayList<>();

                        for (int j = 0; j < response.body().getData().size(); j++) {

                            totalselfsize = response.body().getData().size();

                            getTimelineImageAll = new Get_timeline_image_all("", "", "");
                            userID = response.body().getData().get(j).getUserid();
                            timelineid = response.body().getData().get(j).getTimelineid();
                            String imageall = response.body().getData().get(j).getTimelineimage();
                            imageList.add(imageall);
                            myList = new ArrayList<String>(Arrays.asList(imageall));
                            Log.e("allimage", "" + allimagelist.toString());


                            Get_timeline_image_all getTimelineImageAll = new Get_timeline_image_all(timelineid, userID, imageall);
                            allimagelist.add(getTimelineImageAll);


                        }

                        Activity activity = getActivity();
                        if (isAdded() && activity != null) {

                            Log.e("CheckingImages", "imageList->" + imageList.toString());


                            timeLineImagesUser = new TimeLineImagesUser(context, imageList, allimagelist);
                            gridview_profile.setLayoutManager(new GridLayoutManager(context, NUM_GRID_COLUMNS));
                            gridview_profile.setAdapter(timeLineImagesUser);
                            gridview_profile.setNestedScrollingEnabled(true);
                            timeLineImagesUser.notifyDataSetChanged();


                        }

                    } else {

                        ll_no_post.setVisibility(View.VISIBLE);


                    }
                }

                @Override
                public void onFailure(Call<Get_timeline_output_response> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());
                    ll_no_post.setVisibility(View.VISIBLE);

                }
            });

        } else {
            Toast.makeText(getContext(), "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }

    }

    private void get_timeline_image_tagged_user() {

        if (isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            final String userid = Pref_storage.getDetail(getActivity(), "userId");

            Call<GetTimelineTaggedImage_Response> call = apiService.get_timeline_image_tagged_user("get_timeline_image_tagged_user",
                    Integer.parseInt(userid));
            call.enqueue(new Callback<GetTimelineTaggedImage_Response>() {


                @Override
                public void onResponse(Call<GetTimelineTaggedImage_Response> call, Response<GetTimelineTaggedImage_Response> response) {

                    try{

                        getTimelineTaggedImageDatumArrayList.clear();
                        if(response.body().getResponseMessage().equalsIgnoreCase("success"))
                        {
                            Log.e("taggedimagelistsize", "onResponse: "+response.body().getData().size() );

                            for (int j = 0; j < response.body().getData().size(); j++) {



                                String timelineimage = response.body().getData().get(j).getTimelineimage();
                                String timelineid = response.body().getData().get(j).getTimelineid();
                                String userid = response.body().getData().get(j).getUserid();


                                GetTimelineTaggedImage_Datum getReviewProfileImage = new GetTimelineTaggedImage_Datum(timelineid,
                                        userid, timelineimage);
                                getTimelineTaggedImageDatumArrayList.add(getReviewProfileImage);

                            }

                            Activity activity = getActivity();

                            if (isAdded() && activity != null) {

                                profileReviewTaggedImageUserAdapter = new ProfileReviewTaggedImageUserAdapter(context, getTimelineTaggedImageDatumArrayList);
                                tagged_bucket_recyclerview.setLayoutManager(new GridLayoutManager(context, 3));
                                tagged_bucket_recyclerview.setAdapter(profileReviewTaggedImageUserAdapter);
                                tagged_bucket_recyclerview.setNestedScrollingEnabled(true);
                                profileReviewTaggedImageUserAdapter.notifyDataSetChanged();
                            }

                        }

                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GetTimelineTaggedImage_Response> call, Throwable t) {

                }
            });
        } else {


            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();


        }
    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.self :

                gridview_profile.setVisibility(View.VISIBLE);
                tagged_bucket_recyclerview.setVisibility(View.GONE);
                get_timeline_images();


                break;
            case R.id.tagged :
                gridview_profile.setVisibility(View.GONE);
                tagged_bucket_recyclerview.setVisibility(View.VISIBLE);

                get_timeline_image_tagged_user( );

                break;


        }


    }

}
