package com.kaspontech.foodwall.modelclasses.Review_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopDishImagePojo {
    @SerializedName("dishname")
    @Expose
    private String dishname;

    /**
     * No args constructor for use in serialization
     *
     */

    /**
     *
     * @param dishname
     */
    public TopDishImagePojo(String dishname) {
        super();
        this.dishname = dishname;
    }

    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }
}
