package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerComments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AnswerCmtData {

    @SerializedName("ans_id")
    @Expose
    private int ansId;
    @SerializedName("quest_id")
    @Expose
    private int questId;
    @SerializedName("ask_answer")
    @Expose
    private String askAnswer;
    @SerializedName("total_comments")
    @Expose
    private int totalComments;
    @SerializedName("cmmt_ans_id")
    @Expose
    private int cmmtAnsId;
    @SerializedName("ans_comments")
    @Expose
    private String ansComments;
    @SerializedName("total_cmmt_likes")
    @Expose
    private int totalCmmtLikes;
    @SerializedName("total_cmmt_reply")
    @Expose
    private int totalCmmtReply;
    @SerializedName("created_by")
    @Expose
    private int createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("cmmt_likes_id")
    @Expose
    private int cmmtLikesId;
    @SerializedName("ans_cmmt_likes")
    @Expose
    private int ansCmmtLikes;
    @SerializedName("upVoted")
    @Expose
    private boolean upVoted;
    @SerializedName("downVoted")
    @Expose
    private boolean downVoted;
    @SerializedName("reply")
    @Expose
    private List<CommentReplyData> reply = null;


    public AnswerCmtData(int ansId, int questId, String askAnswer, int totalComments, int cmmtAnsId, String ansComments, int totalCmmtLikes, int totalCmmtReply, int createdBy, String createdOn, int userId, String firstName, String lastName, String picture, Integer cmmtLikesId, Integer ansCmmtLikes, List<CommentReplyData> reply) {
        this.ansId = ansId;
        this.questId = questId;
        this.askAnswer = askAnswer;
        this.totalComments = totalComments;
        this.cmmtAnsId = cmmtAnsId;
        this.ansComments = ansComments;
        this.totalCmmtLikes = totalCmmtLikes;
        this.totalCmmtReply = totalCmmtReply;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.cmmtLikesId = cmmtLikesId;
        this.ansCmmtLikes = ansCmmtLikes;
        this.reply = reply;
    }


    public boolean isDownVoted() {
        return downVoted;
    }

    public void setDownVoted(boolean downVoted) {
        this.downVoted = downVoted;
    }

    public boolean isUpVoted() {
        return upVoted;
    }

    public void setUpVoted(boolean upVoted) {
        this.upVoted = upVoted;
    }

    public int getAnsId() {
        return ansId;
    }

    public void setAnsId(int ansId) {
        this.ansId = ansId;
    }

    public int getQuestId() {
        return questId;
    }

    public void setQuestId(int questId) {
        this.questId = questId;
    }

    public String getAskAnswer() {
        return askAnswer;
    }

    public void setAskAnswer(String askAnswer) {
        this.askAnswer = askAnswer;
    }

    public int getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(int totalComments) {
        this.totalComments = totalComments;
    }

    public int getCmmtAnsId() {
        return cmmtAnsId;
    }

    public void setCmmtAnsId(int cmmtAnsId) {
        this.cmmtAnsId = cmmtAnsId;
    }

    public String getAnsComments() {
        return ansComments;
    }

    public void setAnsComments(String ansComments) {
        this.ansComments = ansComments;
    }

    public int getTotalCmmtLikes() {
        return totalCmmtLikes;
    }

    public void setTotalCmmtLikes(int totalCmmtLikes) {
        this.totalCmmtLikes = totalCmmtLikes;
    }

    public int getTotalCmmtReply() {
        return totalCmmtReply;
    }

    public void setTotalCmmtReply(int totalCmmtReply) {
        this.totalCmmtReply = totalCmmtReply;
    }

    public void setCmmtLikesId(int cmmtLikesId) {
        this.cmmtLikesId = cmmtLikesId;
    }

    public void setAnsCmmtLikes(int ansCmmtLikes) {
        this.ansCmmtLikes = ansCmmtLikes;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getCmmtLikesId() {
        return cmmtLikesId;
    }

    public void setCmmtLikesId(Integer cmmtLikesId) {
        this.cmmtLikesId = cmmtLikesId;
    }

    public Integer getAnsCmmtLikes() {
        return ansCmmtLikes;
    }

    public void setAnsCmmtLikes(Integer ansCmmtLikes) {
        this.ansCmmtLikes = ansCmmtLikes;
    }

    public List<CommentReplyData> getReply() {
        return reply;
    }

    public void setReply(List<CommentReplyData> reply) {
        this.reply = reply;
    }

}

