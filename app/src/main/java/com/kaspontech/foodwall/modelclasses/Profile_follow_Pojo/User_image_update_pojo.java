package com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo;

import java.util.List;

/**
 * Created by vishnukm on 31/3/18.
 */

public class User_image_update_pojo {


    private List<User_image_input_response> Data = null;
    private String status;

    private int ResponseCode;

    private String methodName;

    private String ResponseMessage;

    private String custom;

    public List<User_image_input_response> getData ()
    {
        return Data;
    }

    public void setData (List<User_image_input_response> Data)
    {
        this.Data = Data;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public int getResponseCode ()
    {
        return ResponseCode;
    }

    public void setResponseCode (int ResponseCode)
    {
        this.ResponseCode = ResponseCode;
    }

    public String getMethodName ()
    {
        return methodName;
    }

    public void setMethodName (String methodName)
    {
        this.methodName = methodName;
    }

    public String getResponseMessage ()
    {
        return ResponseMessage;
    }

    public void setResponseMessage (String ResponseMessage)
    {
        this.ResponseMessage = ResponseMessage;
    }

    public String getCustom ()
    {
        return custom;
    }

    public void setCustom (String custom)
    {
        this.custom = custom;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Data = "+Data+", status = "+status+", ResponseCode = "+ResponseCode+", methodName = "+methodName+", ResponseMessage = "+ResponseMessage+", custom = "+custom+"]";
    }
}
