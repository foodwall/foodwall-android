package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI.AnswerComments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by BalajiPrabhu on 6/3/2018.
 */

public class CommentReplyData {

    @SerializedName("cmmt_ans_id")
    @Expose
    private int cmmtAnsId;
    @SerializedName("ans_id")
    @Expose
    private int ansId;
    @SerializedName("ans_comments")
    @Expose
    private String ansComments;
    @SerializedName("total_cmmt_reply")
    @Expose
    private int totalCmmtReply;
    @SerializedName("reply_id")
    @Expose
    private int replyId;
    @SerializedName("ans_cmmt_reply")
    @Expose
    private String ansCmmtReply;
    @SerializedName("created_by")
    @Expose
    private int createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;

    public int getCmmtAnsId() {
        return cmmtAnsId;
    }

    public void setCmmtAnsId(int cmmtAnsId) {
        this.cmmtAnsId = cmmtAnsId;
    }

    public int getAnsId() {
        return ansId;
    }

    public void setAnsId(int ansId) {
        this.ansId = ansId;
    }

    public String getAnsComments() {
        return ansComments;
    }

    public void setAnsComments(String ansComments) {
        this.ansComments = ansComments;
    }

    public int getTotalCmmtReply() {
        return totalCmmtReply;
    }

    public void setTotalCmmtReply(int totalCmmtReply) {
        this.totalCmmtReply = totalCmmtReply;
    }

    public int getReplyId() {
        return replyId;
    }

    public void setReplyId(int replyId) {
        this.replyId = replyId;
    }

    public String getAnsCmmtReply() {
        return ansCmmtReply;
    }

    public void setAnsCmmtReply(String ansCmmtReply) {
        this.ansCmmtReply = ansCmmtReply;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
