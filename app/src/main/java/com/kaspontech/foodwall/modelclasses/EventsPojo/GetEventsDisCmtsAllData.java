package com.kaspontech.foodwall.modelclasses.EventsPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetEventsDisCmtsAllData {

    @SerializedName("cmmt_dis_id")
    @Expose
    private String cmmtDisId;
    @SerializedName("dis_comments")
    @Expose
    private String disComments;
    @SerializedName("total_cmmt_likes")
    @Expose
    private String totalCmmtLikes;
    @SerializedName("total_cmmt_reply")
    @Expose
    private String totalCmmtReply;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("dis_evt_id")
    @Expose
    private String disEvtId;
    @SerializedName("dis_description")
    @Expose
    private String disDescription;
    @SerializedName("dis_image")
    @Expose
    private String disImage;
    @SerializedName("total_likes")
    @Expose
    private String totalLikes;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("dis_created")
    @Expose
    private String disCreated;
    @SerializedName("dis_creator_firstname")
    @Expose
    private String disCreatorFirstname;
    @SerializedName("dis_creator_lastname")
    @Expose
    private String disCreatorLastname;
    @SerializedName("dis_creator_picture")
    @Expose
    private String disCreatorPicture;
    @SerializedName("dis_cmmt_likes")
    @Expose
    private String disCmmtLikes;

    @SerializedName("liked")
    @Expose
    private boolean liked;

    @SerializedName("reply")
    @Expose
    private List<GetEventsDisCmtsAllReplyData> reply = null;


    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public String getCmmtDisId() {
        return cmmtDisId;
    }

    public void setCmmtDisId(String cmmtDisId) {
        this.cmmtDisId = cmmtDisId;
    }

    public String getDisComments() {
        return disComments;
    }

    public void setDisComments(String disComments) {
        this.disComments = disComments;
    }

    public String getTotalCmmtLikes() {
        return totalCmmtLikes;
    }

    public void setTotalCmmtLikes(String totalCmmtLikes) {
        this.totalCmmtLikes = totalCmmtLikes;
    }

    public String getTotalCmmtReply() {
        return totalCmmtReply;
    }

    public void setTotalCmmtReply(String totalCmmtReply) {
        this.totalCmmtReply = totalCmmtReply;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getDisEvtId() {
        return disEvtId;
    }

    public void setDisEvtId(String disEvtId) {
        this.disEvtId = disEvtId;
    }

    public String getDisDescription() {
        return disDescription;
    }

    public void setDisDescription(String disDescription) {
        this.disDescription = disDescription;
    }

    public String getDisImage() {
        return disImage;
    }

    public void setDisImage(String disImage) {
        this.disImage = disImage;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getDisCreated() {
        return disCreated;
    }

    public void setDisCreated(String disCreated) {
        this.disCreated = disCreated;
    }

    public String getDisCreatorFirstname() {
        return disCreatorFirstname;
    }

    public void setDisCreatorFirstname(String disCreatorFirstname) {
        this.disCreatorFirstname = disCreatorFirstname;
    }

    public String getDisCreatorLastname() {
        return disCreatorLastname;
    }

    public void setDisCreatorLastname(String disCreatorLastname) {
        this.disCreatorLastname = disCreatorLastname;
    }

    public String getDisCreatorPicture() {
        return disCreatorPicture;
    }

    public void setDisCreatorPicture(String disCreatorPicture) {
        this.disCreatorPicture = disCreatorPicture;
    }

    public String getDisCmmtLikes() {
        return disCmmtLikes;
    }

    public void setDisCmmtLikes(String disCmmtLikes) {
        this.disCmmtLikes = disCmmtLikes;
    }

    public List<GetEventsDisCmtsAllReplyData> getReply() {
        return reply;
    }

    public void setReply(List<GetEventsDisCmtsAllReplyData> reply) {
        this.reply = reply;
    }

}
