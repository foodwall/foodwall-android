package com.kaspontech.foodwall.modelclasses.Review_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetHotelAllPojo {
    @SerializedName("hotel_id")
    @Expose
    private String hotelId;
    @SerializedName("google_id")
    @Expose
    private String googleId;
    @SerializedName("hotel_name")
    @Expose
    private String hotelName;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("category_type")
    @Expose
    private String categoryType;
    @SerializedName("open_times")
    @Expose
    private String openTimes;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("photo_reference")
    @Expose
    private String photoReference;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("total_review")
    @Expose
    private String totalReview;
    @SerializedName("total_review_users")
    @Expose
    private String totalReviewUsers;
    @SerializedName("total_food_exprience")
    @Expose
    private String totalFoodExprience;
    @SerializedName("total_ambiance")
    @Expose
    private String totalAmbiance;
    @SerializedName("total_taste")
    @Expose
    private String totalTaste;
    @SerializedName("total_service")
    @Expose
    private String totalService;
    @SerializedName("total_value_money")
    @Expose
    private String totalValueMoney;
    @SerializedName("total_good")
    @Expose
    private String totalGood;
    @SerializedName("total_bad")
    @Expose
    private String totalBad;
    @SerializedName("total_good_bad_user")
    @Expose
    private String totalGoodBadUser;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param phone
     * @param totalAmbiance
     * @param totalFoodExprience
     * @param totalGoodBadUser
     * @param categoryType
     * @param totalBad
     * @param totalReviewUsers
     * @param hotelId
     * @param googleId
     * @param totalTaste
     * @param photoReference
     * @param createdOn
     * @param totalService
     * @param totalReview
     * @param createdBy
     * @param hotelName
     * @param placeId
     * @param openTimes
     * @param address
     * @param longitude
     * @param totalGood
     * @param latitude
     * @param totalValueMoney
     */
    public GetHotelAllPojo(String hotelId, String googleId, String hotelName, String placeId, String categoryType, String openTimes, String address, String latitude, String longitude, String phone, String photoReference, String createdBy, String createdOn, String totalReview, String totalReviewUsers, String totalFoodExprience, String totalAmbiance, String totalTaste, String totalService, String totalValueMoney, String totalGood, String totalBad, String totalGoodBadUser) {
        super();
        this.hotelId = hotelId;
        this.googleId = googleId;
        this.hotelName = hotelName;
        this.placeId = placeId;
        this.categoryType = categoryType;
        this.openTimes = openTimes;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
        this.photoReference = photoReference;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.totalReview = totalReview;
        this.totalReviewUsers = totalReviewUsers;
        this.totalFoodExprience = totalFoodExprience;
        this.totalAmbiance = totalAmbiance;
        this.totalTaste = totalTaste;
        this.totalService = totalService;
        this.totalValueMoney = totalValueMoney;
        this.totalGood = totalGood;
        this.totalBad = totalBad;
        this.totalGoodBadUser = totalGoodBadUser;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(String openTimes) {
        this.openTimes = openTimes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTotalReview() {
        return totalReview;
    }

    public void setTotalReview(String totalReview) {
        this.totalReview = totalReview;
    }

    public String getTotalReviewUsers() {
        return totalReviewUsers;
    }

    public void setTotalReviewUsers(String totalReviewUsers) {
        this.totalReviewUsers = totalReviewUsers;
    }

    public String getTotalFoodExprience() {
        return totalFoodExprience;
    }

    public void setTotalFoodExprience(String totalFoodExprience) {
        this.totalFoodExprience = totalFoodExprience;
    }

    public String getTotalAmbiance() {
        return totalAmbiance;
    }

    public void setTotalAmbiance(String totalAmbiance) {
        this.totalAmbiance = totalAmbiance;
    }

    public String getTotalTaste() {
        return totalTaste;
    }

    public void setTotalTaste(String totalTaste) {
        this.totalTaste = totalTaste;
    }

    public String getTotalService() {
        return totalService;
    }

    public void setTotalService(String totalService) {
        this.totalService = totalService;
    }

    public String getTotalValueMoney() {
        return totalValueMoney;
    }

    public void setTotalValueMoney(String totalValueMoney) {
        this.totalValueMoney = totalValueMoney;
    }

    public String getTotalGood() {
        return totalGood;
    }

    public void setTotalGood(String totalGood) {
        this.totalGood = totalGood;
    }

    public String getTotalBad() {
        return totalBad;
    }

    public void setTotalBad(String totalBad) {
        this.totalBad = totalBad;
    }

    public String getTotalGoodBadUser() {
        return totalGoodBadUser;
    }

    public void setTotalGoodBadUser(String totalGoodBadUser) {
        this.totalGoodBadUser = totalGoodBadUser;
    }

}
