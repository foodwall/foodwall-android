package com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchReviewAmbImage  {

    @SerializedName("img")
    @Expose
    private String img;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

}
