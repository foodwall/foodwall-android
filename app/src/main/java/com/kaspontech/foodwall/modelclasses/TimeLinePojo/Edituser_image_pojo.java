package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

/**
 * Created by vishnukm on 31/3/18.
 */

public class Edituser_image_pojo {
    private String image;

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Image = "+image+"]";
    }

}
