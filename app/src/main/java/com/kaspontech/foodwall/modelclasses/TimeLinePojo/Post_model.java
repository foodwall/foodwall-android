package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vishnukm on 7/3/18.
 */

public class Post_model {

    @SerializedName("timeline_id")
    @Expose
    private String timelineId;
    @SerializedName("timeline_description")
    @Expose
    private String timelineDescription;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("image")
    @Expose
    private String image;


    @SerializedName("created_on")
    @Expose
    private String createdOn;


    public String getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(String timelineId) {
        this.timelineId = timelineId;
    }

    public String getTimelineDescription() {
        return timelineDescription;
    }

    public void setTimelineDescription(String timelineDescription) {
        this.timelineDescription = timelineDescription;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Post_model(String timelineId, String timelineDescription, String createdBy, String image, String createdOn) {
        this.timelineId = timelineId;
        this.timelineDescription = timelineDescription;
        this.createdBy = createdBy;
        this.image = image;
        this.createdOn = createdOn;
    }
}
