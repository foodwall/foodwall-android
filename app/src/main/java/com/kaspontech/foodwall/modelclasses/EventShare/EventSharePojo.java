package com.kaspontech.foodwall.modelclasses.EventShare;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EventSharePojo {

        @SerializedName("eventid")
        @Expose
        private int eventid;
        @SerializedName("userid")
        @Expose
        private int userid;
        @SerializedName("friends")
        @Expose
        private List<FollowersId> friends = null;

        public EventSharePojo(int eventid, int userid, List<FollowersId> friends) {
            this.eventid = eventid;
            this.userid = userid;
            this.friends = friends;
        }

        public int getEventid() {
            return eventid;
        }

        public void setEventid(int eventid) {
            this.eventid = eventid;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }

        public List<FollowersId> getFriends() {
            return friends;
        }

        public void setFriends(List<FollowersId> friends) {
            this.friends = friends;
        }

}
