package com.kaspontech.foodwall.modelclasses.Chat_Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_chat_history_details_pojo {

    String redirect_url;
    @SerializedName("hist_id")
    @Expose
    private String histId;
    @SerializedName("sessionid")
    @Expose
    private String sessionid;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("group_icon")
    @Expose
    private String group_icon;

    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("type_yourid")
    @Expose
    private String typeYourid;
    @SerializedName("type_status")
    @Expose
    private String typeStatus;
    @SerializedName("your_firstname")
    @Expose
    private String yourFirstname;
    @SerializedName("your_lastname")
    @Expose
    private String yourLastname;
    @SerializedName("lastmessage")
    @Expose
    private String lastmessage;
    @SerializedName("lastseen")
    @Expose
    private String lastseen;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("from_firstname")
    @Expose
    private String fromFirstname;
    @SerializedName("from_lastname")
    @Expose
    private String fromLastname;
    @SerializedName("from_picture")
    @Expose
    private String fromPicture;
    @SerializedName("friendid")
    @Expose
    private String friendid;
    @SerializedName("to_firstname")
    @Expose
    private String toFirstname;
    @SerializedName("to_lastname")
    @Expose
    private String toLastname;
    @SerializedName("to_picture")
    @Expose
    private String toPicture;
    @SerializedName("to_online_status")
    @Expose
    private String toOnlineStatus;
    @SerializedName("to_lastvisited")
    @Expose
    private String toLastvisited;
    @SerializedName("group_name")
    @Expose
    private String groupName;
    @SerializedName("group_createdby")
    @Expose
    private String groupCreatedby;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("group_created_firstname")
    @Expose
    private String groupCreatedFirstname;
    @SerializedName("group_created_lastname")
    @Expose
    private String groupCreatedLastname;
    @SerializedName("member")
    @Expose
    private List<Chat_group_member_pojo> member = null;
    @SerializedName("total_member")
    @Expose
    private Integer totalMember;



    /**
     *
     * @param groupName
     * @param userid
     * @param fromFirstname
     * @param fromLastname
     * @param fromPicture
     * @param toLastvisited
     * @param yourLastname
     * @param groupid
     * @param yourFirstname
     * @param createdDate
     * @param groupCreatedLastname
     * @param member
     * @param groupCreatedFirstname
     * @param lastmessage
     * @param totalMember
     * @param toPicture
     * @param groupCreatedby
     * @param typeStatus
     * @param typeYourid
     * @param sessionid
     * @param lastseen
     * @param createdOn
     * @param friendid
     * @param toFirstname
     * @param toOnlineStatus
     * @param toLastname
     * @param histId
     */
    public Get_chat_history_details_pojo(String redirect_url,String histId, String sessionid, String groupid, String group_icon, String createdOn, String typeYourid, String typeStatus, String yourFirstname, String yourLastname, String lastmessage, String lastseen, String userid, String fromFirstname, String fromLastname, String fromPicture, String friendid, String toFirstname, String toLastname, String toPicture, String toOnlineStatus, String toLastvisited, String groupName, String groupCreatedby, String createdDate, String groupCreatedFirstname, String groupCreatedLastname, List<Chat_group_member_pojo> member, Integer totalMember) {
        super();
        this.redirect_url = redirect_url;
        this.histId = histId;
        this.sessionid = sessionid;
        this.groupid = groupid;
        this.group_icon = group_icon;
        this.createdOn = createdOn;
        this.typeYourid = typeYourid;
        this.typeStatus = typeStatus;
        this.yourFirstname = yourFirstname;
        this.yourLastname = yourLastname;
        this.lastmessage = lastmessage;
        this.lastseen = lastseen;
        this.userid = userid;
        this.fromFirstname = fromFirstname;
        this.fromLastname = fromLastname;
        this.fromPicture = fromPicture;
        this.friendid = friendid;
        this.toFirstname = toFirstname;
        this.toLastname = toLastname;
        this.toPicture = toPicture;
        this.toOnlineStatus = toOnlineStatus;
        this.toLastvisited = toLastvisited;
        this.groupName = groupName;
        this.groupCreatedby = groupCreatedby;
        this.createdDate = createdDate;
        this.groupCreatedFirstname = groupCreatedFirstname;
        this.groupCreatedLastname = groupCreatedLastname;
        this.member = member;
        this.totalMember = totalMember;
    }

    public Get_chat_history_details_pojo(String histId, String sessionid, String groupid, String group_icon, String createdOn, String typeYourid, String typeStatus, String yourFirstname, String yourLastname, String lastmessage, String lastseen, String userid, String fromFirstname, String fromLastname, String fromPicture, String friendid, String toFirstname, String toLastname, String toPicture, String toOnlineStatus, String toLastvisited, String groupName, String groupCreatedby, String createdDate, String groupCreatedFirstname, String groupCreatedLastname) {
        this.histId = histId;
        this.sessionid = sessionid;
        this.groupid = groupid;
        this.group_icon = group_icon;
        this.createdOn = createdOn;
        this.typeYourid = typeYourid;
        this.typeStatus = typeStatus;
        this.yourFirstname = yourFirstname;
        this.yourLastname = yourLastname;
        this.lastmessage = lastmessage;
        this.lastseen = lastseen;
        this.userid = userid;
        this.fromFirstname = fromFirstname;
        this.fromLastname = fromLastname;
        this.fromPicture = fromPicture;
        this.friendid = friendid;
        this.toFirstname = toFirstname;
        this.toLastname = toLastname;
        this.toPicture = toPicture;
        this.toOnlineStatus = toOnlineStatus;
        this.toLastvisited = toLastvisited;
        this.groupName = groupName;
        this.groupCreatedby = groupCreatedby;
        this.createdDate = createdDate;
        this.groupCreatedFirstname = groupCreatedFirstname;
        this.groupCreatedLastname = groupCreatedLastname;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public void setRedirect_url(String redirect_url) {
        this.redirect_url = redirect_url;
    }

    public String getHistId() {
        return histId;
    }

    public void setHistId(String histId) {
        this.histId = histId;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getGroupid() {
        return groupid;
    }

    public String getGroup_icon() {
        return group_icon;
    }

    public void setGroup_icon(String group_icon) {
        this.group_icon = group_icon;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTypeYourid() {
        return typeYourid;
    }

    public void setTypeYourid(String typeYourid) {
        this.typeYourid = typeYourid;
    }

    public String getTypeStatus() {
        return typeStatus;
    }

    public void setTypeStatus(String typeStatus) {
        this.typeStatus = typeStatus;
    }

    public String getYourFirstname() {
        return yourFirstname;
    }

    public void setYourFirstname(String yourFirstname) {
        this.yourFirstname = yourFirstname;
    }

    public String getYourLastname() {
        return yourLastname;
    }

    public void setYourLastname(String yourLastname) {
        this.yourLastname = yourLastname;
    }

    public String getLastmessage() {
        return lastmessage;
    }

    public void setLastmessage(String lastmessage) {
        this.lastmessage = lastmessage;
    }

    public String getLastseen() {
        return lastseen;
    }

    public void setLastseen(String lastseen) {
        this.lastseen = lastseen;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFromFirstname() {
        return fromFirstname;
    }

    public void setFromFirstname(String fromFirstname) {
        this.fromFirstname = fromFirstname;
    }

    public String getFromLastname() {
        return fromLastname;
    }

    public void setFromLastname(String fromLastname) {
        this.fromLastname = fromLastname;
    }

    public String getFromPicture() {
        return fromPicture;
    }

    public void setFromPicture(String fromPicture) {
        this.fromPicture = fromPicture;
    }

    public String getFriendid() {
        return friendid;
    }

    public void setFriendid(String friendid) {
        this.friendid = friendid;
    }

    public String getToFirstname() {
        return toFirstname;
    }

    public void setToFirstname(String toFirstname) {
        this.toFirstname = toFirstname;
    }

    public String getToLastname() {
        return toLastname;
    }

    public void setToLastname(String toLastname) {
        this.toLastname = toLastname;
    }

    public String getToPicture() {
        return toPicture;
    }

    public void setToPicture(String toPicture) {
        this.toPicture = toPicture;
    }

    public String getToOnlineStatus() {
        return toOnlineStatus;
    }

    public void setToOnlineStatus(String toOnlineStatus) {
        this.toOnlineStatus = toOnlineStatus;
    }

    public String getToLastvisited() {
        return toLastvisited;
    }

    public void setToLastvisited(String toLastvisited) {
        this.toLastvisited = toLastvisited;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupCreatedby() {
        return groupCreatedby;
    }

    public void setGroupCreatedby(String groupCreatedby) {
        this.groupCreatedby = groupCreatedby;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getGroupCreatedFirstname() {
        return groupCreatedFirstname;
    }

    public void setGroupCreatedFirstname(String groupCreatedFirstname) {
        this.groupCreatedFirstname = groupCreatedFirstname;
    }

    public String getGroupCreatedLastname() {
        return groupCreatedLastname;
    }

    public void setGroupCreatedLastname(String groupCreatedLastname) {
        this.groupCreatedLastname = groupCreatedLastname;
    }

    public List<Chat_group_member_pojo> getMember() {
        return member;
    }

    public void setMember(List<Chat_group_member_pojo> member) {
        this.member = member;
    }

    public Integer getTotalMember() {
        return totalMember;
    }

    public void setTotalMember(Integer totalMember) {
        this.totalMember = totalMember;
    }

}
