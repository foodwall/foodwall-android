package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reply_pojo {
    @SerializedName("cmmt_tl_id")
    @Expose
    private String cmmtTlId;
    @SerializedName("timeline_id")
    @Expose
    private String timelineId;
    @SerializedName("tl_comments")
    @Expose
    private String tlComments;
    @SerializedName("total_cmmt_reply")
    @Expose
    private String totalCmmtReply;
    @SerializedName("reply_id")
    @Expose
    private String replyId;
    @SerializedName("tl_cmmt_reply")
    @Expose
    private String tlCmmtReply;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param lastName
     * @param tlCmmtReply
     * @param cmmtTlId
     * @param picture
     * @param createdOn
     * @param totalCmmtReply
     * @param createdBy
     * @param email
     * @param dob
     * @param tlComments
     * @param userId
     * @param gender
     * @param contNo
     * @param timelineId
     * @param replyId
     * @param firstName
     */
    public Reply_pojo(String cmmtTlId, String timelineId, String tlComments, String totalCmmtReply, String replyId, String tlCmmtReply, String createdBy, String createdOn, String userId, String firstName, String lastName, String email, String contNo, String gender, String dob, String picture) {
        super();
        this.cmmtTlId = cmmtTlId;
        this.timelineId = timelineId;
        this.tlComments = tlComments;
        this.totalCmmtReply = totalCmmtReply;
        this.replyId = replyId;
        this.tlCmmtReply = tlCmmtReply;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
    }

    public String getCmmtTlId() {
        return cmmtTlId;
    }

    public void setCmmtTlId(String cmmtTlId) {
        this.cmmtTlId = cmmtTlId;
    }

    public String getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(String timelineId) {
        this.timelineId = timelineId;
    }

    public String getTlComments() {
        return tlComments;
    }

    public void setTlComments(String tlComments) {
        this.tlComments = tlComments;
    }

    public String getTotalCmmtReply() {
        return totalCmmtReply;
    }

    public void setTotalCmmtReply(String totalCmmtReply) {
        this.totalCmmtReply = totalCmmtReply;
    }

    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public String getTlCmmtReply() {
        return tlCmmtReply;
    }

    public void setTlCmmtReply(String tlCmmtReply) {
        this.tlCmmtReply = tlCmmtReply;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
