package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vishnukm on 20/3/18.
 */

public class Likes_2_pojo {

    private ArrayList<GetallLikesPojo> Data;

    private String status;

    private int ResponseCode;

    private String methodName;

    private String ResponseMessage;

    private String custom;

    public ArrayList<GetallLikesPojo> getData ()
    {
        return Data;
    }

    public void setData (ArrayList<GetallLikesPojo> Data)
    {
        this.Data = Data;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(int responseCode) {
        ResponseCode = responseCode;
    }

    public String getMethodName ()
    {
        return methodName;
    }

    public void setMethodName (String methodName)
    {
        this.methodName = methodName;
    }

    public String getResponseMessage ()
    {
        return ResponseMessage;
    }

    public void setResponseMessage (String ResponseMessage)
    {
        this.ResponseMessage = ResponseMessage;
    }

    public String getCustom ()
    {
        return custom;
    }

    public void setCustom (String custom)
    {
        this.custom = custom;
    }


}
