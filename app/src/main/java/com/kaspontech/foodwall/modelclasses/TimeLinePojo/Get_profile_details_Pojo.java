package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vishnukm on 26/3/18.
 */

public class Get_profile_details_Pojo {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gcmkey")
    @Expose
    private String gcmkey;
    @SerializedName("ip_address")
    @Expose
    private String ipAddress;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("bio_description")
    @Expose
    private String bioDescription;
    @SerializedName("locale")
    @Expose
    private String locale;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("countrycode")
    @Expose
    private String countrycode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("login_type")
    @Expose
    private String loginType;
    @SerializedName("profile_status")
    @Expose
    private String profileStatus;
    @SerializedName("total_followers")
    @Expose
    private String totalFollowers;
    @SerializedName("total_followings")
    @Expose
    private String totalFollowings;
    @SerializedName("total_posts")
    @Expose
    private String totalPosts;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("modified_time")
    @Expose
    private String modifiedTime;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("following_id")
    @Expose
    private String followingId;
    @SerializedName("req_follower_id")
    @Expose
    private String reqFollowerId;
    @SerializedName("req_follow")
    @Expose
    private String reqFollow;

    /**
     * No args constructor for use in serialization
     *
     */
    public Get_profile_details_Pojo() {
    }

    /**
     *
     * @param gcmkey
     * @param oauthProvider
     * @param countrycode
     * @param totalFollowers
     * @param locale
     * @param link
     * @param password
     * @param loginType
     * @param city
     * @param profileStatus
     * @param username
     * @param userId
     * @param contNo
     * @param gender
     * @param longitude
     * @param firstName
     * @param ipAddress
     * @param lastName
     * @param createdTime
     * @param followingId
     * @param modifiedTime
     * @param totalPosts
     * @param imei
     * @param reqFollow
     * @param bioDescription
     * @param deleted
     * @param oauthUid
     * @param country
     * @param picture
     * @param email
     * @param reqFollowerId
     * @param dob
     * @param totalFollowings
     * @param latitude
     */
    public Get_profile_details_Pojo(String userId, String oauthProvider, String oauthUid, String firstName, String lastName, String username, String email, String password, String imei, String contNo, String gcmkey, String ipAddress, String gender, String dob, String bioDescription, String locale, String picture, String link, String latitude, String longitude, String country, String countrycode, String city, String loginType, String profileStatus, String totalFollowers, String totalFollowings, String totalPosts, String createdTime, String modifiedTime, String deleted, String followingId, String reqFollowerId, String reqFollow) {
        super();
        this.userId = userId;
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.imei = imei;
        this.contNo = contNo;
        this.gcmkey = gcmkey;
        this.ipAddress = ipAddress;
        this.gender = gender;
        this.dob = dob;
        this.bioDescription = bioDescription;
        this.locale = locale;
        this.picture = picture;
        this.link = link;
        this.latitude = latitude;
        this.longitude = longitude;
        this.country = country;
        this.countrycode = countrycode;
        this.city = city;
        this.loginType = loginType;
        this.profileStatus = profileStatus;
        this.totalFollowers = totalFollowers;
        this.totalFollowings = totalFollowings;
        this.totalPosts = totalPosts;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
        this.deleted = deleted;
        this.followingId = followingId;
        this.reqFollowerId = reqFollowerId;
        this.reqFollow = reqFollow;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGcmkey() {
        return gcmkey;
    }

    public void setGcmkey(String gcmkey) {
        this.gcmkey = gcmkey;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getBioDescription() {
        return bioDescription;
    }

    public void setBioDescription(String bioDescription) {
        this.bioDescription = bioDescription;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getTotalFollowers() {
        return totalFollowers;
    }

    public void setTotalFollowers(String totalFollowers) {
        this.totalFollowers = totalFollowers;
    }

    public String getTotalFollowings() {
        return totalFollowings;
    }

    public void setTotalFollowings(String totalFollowings) {
        this.totalFollowings = totalFollowings;
    }

    public String getTotalPosts() {
        return totalPosts;
    }

    public void setTotalPosts(String totalPosts) {
        this.totalPosts = totalPosts;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getFollowingId() {
        return followingId;
    }

    public void setFollowingId(String followingId) {
        this.followingId = followingId;
    }

    public String getReqFollowerId() {
        return reqFollowerId;
    }

    public void setReqFollowerId(String reqFollowerId) {
        this.reqFollowerId = reqFollowerId;
    }

    public String getReqFollow() {
        return reqFollow;
    }

    public void setReqFollow(String reqFollow) {
        this.reqFollow = reqFollow;
    }
}
