package com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class profile_timeline_output_response {

    @SerializedName("methodName")
    @Expose
    private String methodName;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Data")
    @Expose
    private ArrayList<Timeline_single_user_pojo> data = null;
    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("ResponseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("custom")
    @Expose
    private String custom;

    /**
     * No args constructor for use in serialization
     *
     */
    public profile_timeline_output_response() {
    }

    /**
     *
     * @param responseMessage
     * @param responseCode
     * @param status
     * @param data
     * @param methodName
     * @param custom
     */
    public profile_timeline_output_response(String methodName, Integer status, ArrayList<Timeline_single_user_pojo> data, Integer responseCode, String responseMessage, String custom) {
        super();
        this.methodName = methodName;
        this.status = status;
        this.data = data;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.custom = custom;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<Timeline_single_user_pojo> getData() {
        return data;
    }

    public void setData(ArrayList<Timeline_single_user_pojo> data) {

        this.data = data;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }
}