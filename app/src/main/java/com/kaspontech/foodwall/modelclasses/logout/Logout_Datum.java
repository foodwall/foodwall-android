package com.kaspontech.foodwall.modelclasses.logout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Logout_Datum {

    @SerializedName("userid")
    @Expose
    private String userid;

    /**
     * No args constructor for use in serialization
     *
     */
    public Logout_Datum() {
    }

    /**
     *
     * @param userid
     */
    public Logout_Datum(String userid) {
        super();
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}