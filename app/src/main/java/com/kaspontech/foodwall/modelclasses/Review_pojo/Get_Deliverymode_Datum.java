package com.kaspontech.foodwall.modelclasses.Review_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_Deliverymode_Datum {

    @SerializedName("mode_id")
    @Expose
    private String modeId;
    @SerializedName("comp_name")
    @Expose
    private String compName;
    @SerializedName("comp_img")
    @Expose
    private String compImg;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;

    /**
     * No args constructor for use in serialization
     *
     */
    public Get_Deliverymode_Datum() {
    }

    /**
     *
     * @param createdOn
     * @param createdBy
     * @param compName
     * @param compImg
     * @param modeId
     * @param deleted
     */
    public Get_Deliverymode_Datum(String modeId, String compName, String compImg, String deleted, String createdBy, String createdOn) {
        super();
        this.modeId = modeId;
        this.compName = compName;
        this.compImg = compImg;
        this.deleted = deleted;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
    }

    public String getModeId() {
        return modeId;
    }

    public void setModeId(String modeId) {
        this.modeId = modeId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getCompImg() {
        return compImg;
    }

    public void setCompImg(String compImg) {
        this.compImg = compImg;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

}