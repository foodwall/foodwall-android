package com.kaspontech.foodwall.modelclasses.BucketList_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Topdishimage  {

    @SerializedName("dishname")
    @Expose
    private String dishname;
    @SerializedName("img")
    @Expose
    private String img;

    /**
     * No args constructor for use in serialization
     *
     */
    public Topdishimage() {
    }

    /**
     *
     * @param dishname
     * @param img
     */
    public Topdishimage(String dishname, String img) {
        super();
        this.dishname = dishname;
        this.img = img;
    }

    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

}
