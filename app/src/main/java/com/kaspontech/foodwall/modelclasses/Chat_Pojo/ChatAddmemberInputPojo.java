package com.kaspontech.foodwall.modelclasses.Chat_Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatAddmemberInputPojo {
    @SerializedName("friendid")
    @Expose
    private String friendid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("picture")
    @Expose
    private String picture;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param picture
     * @param friendid
     * @param lastName
     * @param username
     * @param firstName
     */
    public ChatAddmemberInputPojo(String friendid, String firstName, String lastName, String username, String picture) {
        super();
        this.friendid = friendid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.picture = picture;
    }

    public String getFriendid() {
        return friendid;
    }

    public void setFriendid(String friendid) {
        this.friendid = friendid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
