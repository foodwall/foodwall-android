package com.kaspontech.foodwall.modelclasses.LoginPojo;

/**
 * Created by balaji on 8/3/18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateUserPojoInput {

    @SerializedName("oauthprovider")
    @Expose
    private String oauthProvider;

    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;

    @SerializedName("first_name")
    @Expose
    private String firstName;

    @SerializedName("last_name")
    @Expose
    private String lastName;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("imei")
    @Expose
    private String imei;

    @SerializedName("gcmkey")
    @Expose
    private String gcmkey;

    @SerializedName("ip")
    @Expose
    private String ip;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("locale")
    @Expose
    private String locale;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("link")
    @Expose
    private String link;

    @SerializedName("logintype")
    @Expose
    private int loginType;

    @SerializedName("latitude")
    @Expose
    private double latitude;

    @SerializedName("longitude")
    @Expose
    private double longitude;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("countrycode")
    @Expose
    private String countrycode;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("register_type")
    @Expose
    private int register_type;

    @SerializedName("mobile_os")
    @Expose
    private int mobile_os;

    @SerializedName("dob")
    @Expose
    private String dob;

    @SerializedName("picture")
    @Expose
    private String picture;

    public CreateUserPojoInput(String oauthProvider, String oauthUid, String firstName, String lastName, String email, String imei, String gcmkey, String ip, String password, String gender, String locale, String image, String link, int loginType, double latitude, double longitude, String country, String countrycode, String city, int register_type, int mobile_os, String dob, String picture) {
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imei = imei;
        this.gcmkey = gcmkey;
        this.ip = ip;
        this.password = password;
        this.gender = gender;
        this.locale = locale;
        this.image = image;
        this.link = link;
        this.loginType = loginType;
        this.latitude = latitude;
        this.longitude = longitude;
        this.country = country;
        this.countrycode = countrycode;
        this.city = city;
        this.register_type = register_type;
        this.mobile_os = mobile_os;
        this.dob = dob;
        this.picture = picture;
    }

    public CreateUserPojoInput(String oauthProvider, String oauthUid, String firstName, String lastName, String email, String imei, String gcmkey, String ip, String password, String gender, String locale ,String link, int loginType, double latitude, double longitude, String country, String countrycode, String city, int register_type, int mobile_os, String dob, String picture) {
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imei = imei;
        this.gcmkey = gcmkey;
        this.ip = ip;
        this.password = password;
        this.gender = gender;
        this.locale = locale;
        this.link = link;
        this.loginType = loginType;
        this.latitude = latitude;
        this.longitude = longitude;
        this.country = country;
        this.countrycode = countrycode;
        this.city = city;
        this.register_type = register_type;
        this.mobile_os = mobile_os;
        this.dob = dob;
        this.picture = picture;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getGcmkey() {
        return gcmkey;
    }

    public void setGcmkey(String gcmkey) {
        this.gcmkey = gcmkey;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

  /*  public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }
*/
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getLoginType() {
        return loginType;
    }

    public void setLoginType(int loginType) {
        this.loginType = loginType;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getRegister_type() {
        return register_type;
    }

    public void setRegister_type(int register_type) {
        this.register_type = register_type;
    }

    public int getMobile_os() {
        return mobile_os;
    }

    public void setMobile_os(int mobile_os) {
        this.mobile_os = mobile_os;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}