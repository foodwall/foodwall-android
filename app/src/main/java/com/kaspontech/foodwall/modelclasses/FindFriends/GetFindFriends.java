package com.kaspontech.foodwall.modelclasses.FindFriends;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetFindFriends {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("profile_status")
    @Expose
    private String profileStatus;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("follower_id")
    @Expose
    private String followerId;
    @SerializedName("req_follower_id")
    @Expose
    private String reqFollowerId;
    @SerializedName("req_follow")
    @Expose
    private String reqFollow;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getFollowerId() {
        return followerId;
    }

    public void setFollowerId(String followerId) {
        this.followerId = followerId;
    }

    public String getReqFollowerId() {
        return reqFollowerId;
    }

    public void setReqFollowerId(String reqFollowerId) {
        this.reqFollowerId = reqFollowerId;
    }

    public String getReqFollow() {
        return reqFollow;
    }

    public void setReqFollow(String reqFollow) {
        this.reqFollow = reqFollow;
    }

}
