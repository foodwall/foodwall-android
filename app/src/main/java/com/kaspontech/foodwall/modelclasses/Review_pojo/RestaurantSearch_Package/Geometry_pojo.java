package com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package;

import android.location.Location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Geometry_pojo {
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("viewport")
    @Expose
    private View_port_pojo viewport;

    /**
     * No args constructor for use in serialization
     */
  /*  public Geometry_pojo(Location location) {
        super();
        this.location = location;
    }*/


    public Geometry_pojo(Location location, View_port_pojo viewport) {
        super();
        this.location = location;
        this.viewport = viewport;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public View_port_pojo getViewport() {
        return viewport;
    }

    public void setViewport(View_port_pojo viewport) {
        this.viewport = viewport;
    }
}
