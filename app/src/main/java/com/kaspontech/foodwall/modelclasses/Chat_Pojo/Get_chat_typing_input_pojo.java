package com.kaspontech.foodwall.modelclasses.Chat_Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_chat_typing_input_pojo {

    @SerializedName("type_yourid")
    @Expose
    private String typeYourid;
    @SerializedName("type_status")
    @Expose
    private String typeStatus;
    @SerializedName("your_firstname")
    @Expose
    private String yourFirstname;
    @SerializedName("your_lastname")
    @Expose
    private String yourLastname;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param yourLastname
     * @param typeStatus
     * @param typeYourid
     * @param yourFirstname
     */
    public Get_chat_typing_input_pojo(String typeYourid, String typeStatus, String yourFirstname, String yourLastname) {
        super();
        this.typeYourid = typeYourid;
        this.typeStatus = typeStatus;
        this.yourFirstname = yourFirstname;
        this.yourLastname = yourLastname;
    }

    public String getTypeYourid() {
        return typeYourid;
    }

    public void setTypeYourid(String typeYourid) {
        this.typeYourid = typeYourid;
    }

    public String getTypeStatus() {
        return typeStatus;
    }

    public void setTypeStatus(String typeStatus) {
        this.typeStatus = typeStatus;
    }

    public String getYourFirstname() {
        return yourFirstname;
    }

    public void setYourFirstname(String yourFirstname) {
        this.yourFirstname = yourFirstname;
    }

    public String getYourLastname() {
        return yourLastname;
    }

    public void setYourLastname(String yourLastname) {
        this.yourLastname = yourLastname;
    }

}
