package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Answer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GetAnswerData {

    @SerializedName("ans_id")
    @Expose
    private String ansId;
    @SerializedName("quest_id")
    @Expose
    private String questId;
    @SerializedName("ask_answer")
    @Expose
    private String askAnswer;
    @SerializedName("ques_type")
    @Expose
    private String quesType;
    @SerializedName("poll_id")
    @Expose
    private String pollId;
    @SerializedName("country")
    @Expose
    private Integer country;
    @SerializedName("city")
    @Expose
    private Integer city;
    @SerializedName("total_upvote")
    @Expose
    private String totalUpvote;
    @SerializedName("total_downvote")
    @Expose
    private String totalDownvote;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private Integer oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private Integer contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("ask_question")
    @Expose
    private String askQuestion;
    @SerializedName("total_answers")
    @Expose
    private String totalAnswers;
    @SerializedName("vote_id")
    @Expose
    private Integer voteId;
    @SerializedName("up_down_vote")
    @Expose
    private int upDownVote;
    @SerializedName("isUpVoted")
    @Expose
    private boolean upVoted;

    @SerializedName("isDownVoted")
    @Expose
    private boolean downVoted;


    public GetAnswerData(String ansId, String questId, String askAnswer, String quesType, String totalUpvote, String totalDownvote, String totalComments, String createdBy, String createdOn, String firstName, String lastName, String picture, String askQuestion, String totalAnswers) {
        this.ansId = ansId;
        this.questId = questId;
        this.askAnswer = askAnswer;
        this.quesType = quesType;
        this.totalUpvote = totalUpvote;
        this.totalDownvote = totalDownvote;
        this.totalComments = totalComments;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.askQuestion = askQuestion;
        this.totalAnswers = totalAnswers;
    }


    public boolean isDownVoted() {
        return downVoted;
    }

    public void setDownVoted(boolean downVoted) {
        this.downVoted = downVoted;
    }

    public boolean isUpVoted() {
        return upVoted;
    }

    public void setUpVoted(boolean upVoted) {
        this.upVoted = upVoted;
    }

    public String getAnsId() {
        return ansId;
    }

    public void setAnsId(String ansId) {
        this.ansId = ansId;
    }

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public String getAskAnswer() {
        return askAnswer;
    }

    public void setAskAnswer(String askAnswer) {
        this.askAnswer = askAnswer;
    }

    public String getQuesType() {
        return quesType;
    }

    public void setQuesType(String quesType) {
        this.quesType = quesType;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public String getTotalUpvote() {
        return totalUpvote;
    }

    public void setTotalUpvote(String totalUpvote) {
        this.totalUpvote = totalUpvote;
    }

    public String getTotalDownvote() {
        return totalDownvote;
    }

    public void setTotalDownvote(String totalDownvote) {
        this.totalDownvote = totalDownvote;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public Integer getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(Integer oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getContNo() {
        return contNo;
    }

    public void setContNo(Integer contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getAskQuestion() {
        return askQuestion;
    }

    public void setAskQuestion(String askQuestion) {
        this.askQuestion = askQuestion;
    }

    public String getTotalAnswers() {
        return totalAnswers;
    }

    public void setTotalAnswers(String totalAnswers) {
        this.totalAnswers = totalAnswers;
    }

    public Integer getVoteId() {
        return voteId;
    }

    public void setVoteId(Integer voteId) {
        this.voteId = voteId;
    }

    public int getUpDownVote() {
        return upDownVote;
    }

    public void setUpDownVote(int upDownVote) {
        this.upDownVote = upDownVote;
    }
}
