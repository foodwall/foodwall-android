package com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image {

    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("img_id")
    @Expose
    private String img_id;
    @SerializedName("localImage")
    @Expose
    private boolean localImage;

    /**
     * No args constructor for use in serialization
     */
    public Image() {
    }

    /**
     * @param img
     */
    public Image(String img) {
        super();
        this.img = img;
    }

    public Image(String img, String img_id) {
        this.img = img;
        this.img_id = img_id;
    }


    public boolean isLocalImage() {
        return localImage;
    }

    public void setLocalImage(boolean localImage) {
        this.localImage = localImage;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg_id() {
        return img_id;
    }

    public void setImg_id(String img_id) {
        this.img_id = img_id;
    }
}

