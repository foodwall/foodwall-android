package com.kaspontech.foodwall.modelclasses.Chat_Pojo.NewChatPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewChatInputPojo {
    @SerializedName("1")
    @Expose
    private List<NewChatgroupusernamepojo> newChatgroupusernamepojos = null;
    @SerializedName("0")
    @Expose
    private NewChatgroupinputPojo newChatgroupinputPojo;

    /**
     * No args constructor for use in serialization
     *
     */
    public NewChatInputPojo() {
    }


    public NewChatInputPojo(List<NewChatgroupusernamepojo> newChatgroupusernamepojos1, NewChatgroupinputPojo newChatgroupinputPojo) {
        super();
        this.newChatgroupusernamepojos = newChatgroupusernamepojos1;
        this.newChatgroupinputPojo = newChatgroupinputPojo;
    }

    public List<NewChatgroupusernamepojo> get1() {
        return newChatgroupusernamepojos;
    }

    public void set1(List<NewChatgroupusernamepojo> newChatgroupusernamepojos1) {
        this.newChatgroupusernamepojos = newChatgroupusernamepojos1;
    }

    public NewChatgroupinputPojo getNewChatgroupinputPojo() {
        return newChatgroupinputPojo;
    }

    public void set0(NewChatgroupinputPojo newChatgroupinputPojo1) {
        this.newChatgroupinputPojo = newChatgroupinputPojo1;
    }
}
