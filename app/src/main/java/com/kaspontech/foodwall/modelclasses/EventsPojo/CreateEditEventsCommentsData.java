package com.kaspontech.foodwall.modelclasses.EventsPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateEditEventsCommentsData {

    @SerializedName("event_id")
    @Expose
    private String eventId;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_description")
    @Expose
    private String eventDescription;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("cmmt_evt_id")
    @Expose
    private String cmmtEvtId;
    @SerializedName("evt_comments")
    @Expose
    private String evtComments;
    @SerializedName("total_cmmt_likes")
    @Expose
    private String totalCmmtLikes;
    @SerializedName("total_cmmt_reply")
    @Expose
    private String totalCmmtReply;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("cmmt_likes_id")
    @Expose
    private String cmmtLikesId;
    @SerializedName("evt_cmmt_likes")
    @Expose
    private String evtCmmtLikes;

    /**
     * No args constructor for use in serialization
     *
     */
    public CreateEditEventsCommentsData() {
    }

    /**
     *
     * @param lastName
     * @param totalComments
     * @param evtComments
     * @param cmmtEvtId
     * @param picture
     * @param createdOn
     * @param totalCmmtReply
     * @param eventDescription
     * @param eventId
     * @param createdBy
     * @param email
     * @param cmmtLikesId
     * @param evtCmmtLikes
     * @param dob
     * @param userId
     * @param gender
     * @param contNo
     * @param eventName
     * @param firstName
     * @param totalCmmtLikes
     */
    public CreateEditEventsCommentsData(String eventId, String eventName, String eventDescription, String totalComments, String cmmtEvtId, String evtComments, String totalCmmtLikes, String totalCmmtReply, String createdBy, String createdOn, String userId, String firstName, String lastName, String email, String contNo, String gender, String dob, String picture, String cmmtLikesId, String evtCmmtLikes) {
        super();
        this.eventId = eventId;
        this.eventName = eventName;
        this.eventDescription = eventDescription;
        this.totalComments = totalComments;
        this.cmmtEvtId = cmmtEvtId;
        this.evtComments = evtComments;
        this.totalCmmtLikes = totalCmmtLikes;
        this.totalCmmtReply = totalCmmtReply;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
        this.cmmtLikesId = cmmtLikesId;
        this.evtCmmtLikes = evtCmmtLikes;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getCmmtEvtId() {
        return cmmtEvtId;
    }

    public void setCmmtEvtId(String cmmtEvtId) {
        this.cmmtEvtId = cmmtEvtId;
    }

    public String getEvtComments() {
        return evtComments;
    }

    public void setEvtComments(String evtComments) {
        this.evtComments = evtComments;
    }

    public String getTotalCmmtLikes() {
        return totalCmmtLikes;
    }

    public void setTotalCmmtLikes(String totalCmmtLikes) {
        this.totalCmmtLikes = totalCmmtLikes;
    }

    public String getTotalCmmtReply() {
        return totalCmmtReply;
    }

    public void setTotalCmmtReply(String totalCmmtReply) {
        this.totalCmmtReply = totalCmmtReply;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getCmmtLikesId() {
        return cmmtLikesId;
    }

    public void setCmmtLikesId(String cmmtLikesId) {
        this.cmmtLikesId = cmmtLikesId;
    }

    public String getEvtCmmtLikes() {
        return evtCmmtLikes;
    }

    public void setEvtCmmtLikes(String evtCmmtLikes) {
        this.evtCmmtLikes = evtCmmtLikes;
    }
}
