package com.kaspontech.foodwall.modelclasses.Review_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReviewCommentsDataPojo {


    @SerializedName("hotel_id")
    @Expose
    private String hotelId;
    @SerializedName("google_id")
    @Expose
    private String googleId;
    @SerializedName("hotel_name")
    @Expose
    private String hotelName;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("category_type")
    @Expose
    private String categoryType;
    @SerializedName("open_times")
    @Expose
    private String openTimes;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("photo_reference")
    @Expose
    private String photoReference;
    @SerializedName("revrat_id")
    @Expose
    private String revratId;
    @SerializedName("cmmt_htl_id")
    @Expose
    private String cmmtHtlId;
    @SerializedName("htl_rev_comments")
    @Expose
    private String htlRevComments;
    @SerializedName("cmmt_tot_likes")
    @Expose
    private String cmmtTotLikes;
    @SerializedName("cmmt_tot_reply")
    @Expose
    private String cmmtTotReply;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("hotel_review")
    @Expose
    private String hotelReview;
    @SerializedName("total_likes")
    @Expose
    private String totalLikes;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("cmmt_likes_id")
    @Expose
    private String cmmtLikesId;
    @SerializedName("rev_cmmt_likes")
    @Expose
    private String revCmmtLikes;

    /**
     * No args constructor for use in serialization
     *
     */
    public ReviewCommentsDataPojo() {
    }

    /**
     *
     * @param phone
     * @param totalComments
     * @param cmmtTotLikes
     * @param hotelReview
     * @param hotelId
     * @param photoReference
     * @param revCmmtLikes
     * @param openTimes
     * @param placeId
     * @param hotelName
     * @param cmmtLikesId
     * @param userId
     * @param gender
     * @param contNo
     * @param longitude
     * @param cmmtTotReply
     * @param firstName
     * @param lastName
     * @param revratId
     * @param categoryType
     * @param googleId
     * @param picture
     * @param createdOn
     * @param totalLikes
     * @param createdBy
     * @param email
     * @param address
     * @param cmmtHtlId
     * @param dob
     * @param latitude
     * @param htlRevComments
     */
    public ReviewCommentsDataPojo(String hotelId, String googleId, String hotelName, String placeId, String categoryType, String openTimes, String address, String latitude, String longitude, String phone, String photoReference, String revratId, String cmmtHtlId, String htlRevComments, String cmmtTotLikes, String cmmtTotReply, String createdBy, String createdOn, String hotelReview, String totalLikes, String totalComments, String userId, String firstName, String lastName, String email, String contNo, String gender, String dob, String picture, String cmmtLikesId, String revCmmtLikes) {
        super();
        this.hotelId = hotelId;
        this.googleId = googleId;
        this.hotelName = hotelName;
        this.placeId = placeId;
        this.categoryType = categoryType;
        this.openTimes = openTimes;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
        this.photoReference = photoReference;
        this.revratId = revratId;
        this.cmmtHtlId = cmmtHtlId;
        this.htlRevComments = htlRevComments;
        this.cmmtTotLikes = cmmtTotLikes;
        this.cmmtTotReply = cmmtTotReply;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.hotelReview = hotelReview;
        this.totalLikes = totalLikes;
        this.totalComments = totalComments;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
        this.cmmtLikesId = cmmtLikesId;
        this.revCmmtLikes = revCmmtLikes;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(String openTimes) {
        this.openTimes = openTimes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public String getRevratId() {
        return revratId;
    }

    public void setRevratId(String revratId) {
        this.revratId = revratId;
    }

    public String getCmmtHtlId() {
        return cmmtHtlId;
    }

    public void setCmmtHtlId(String cmmtHtlId) {
        this.cmmtHtlId = cmmtHtlId;
    }

    public String getHtlRevComments() {
        return htlRevComments;
    }

    public void setHtlRevComments(String htlRevComments) {
        this.htlRevComments = htlRevComments;
    }

    public String getCmmtTotLikes() {
        return cmmtTotLikes;
    }

    public void setCmmtTotLikes(String cmmtTotLikes) {
        this.cmmtTotLikes = cmmtTotLikes;
    }

    public String getCmmtTotReply() {
        return cmmtTotReply;
    }

    public void setCmmtTotReply(String cmmtTotReply) {
        this.cmmtTotReply = cmmtTotReply;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getHotelReview() {
        return hotelReview;
    }

    public void setHotelReview(String hotelReview) {
        this.hotelReview = hotelReview;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getCmmtLikesId() {
        return cmmtLikesId;
    }

    public void setCmmtLikesId(String cmmtLikesId) {
        this.cmmtLikesId = cmmtLikesId;
    }

    public String getRevCmmtLikes() {
        return revCmmtLikes;
    }

    public void setRevCmmtLikes(String revCmmtLikes) {
        this.revCmmtLikes = revCmmtLikes;
    }
}
