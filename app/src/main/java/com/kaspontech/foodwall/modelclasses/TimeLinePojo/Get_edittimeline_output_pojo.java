package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

import java.util.List;

/**
 * Created by vishnukm on 31/3/18.
 */

public class Get_edittimeline_output_pojo {

    private List<Get_edittimeline_datas_pojo> Data = null;
    private List<Edituser_image_pojo> Dataimage = null;

    private String status;

    private int ResponseCode;

    private String methodName;

    private String ResponseMessage;

    private String custom;

    public List<Get_edittimeline_datas_pojo> getData ()
    {
        return Data;
    }


 public List<Edituser_image_pojo> getDataimage ()
    {
        return Dataimage;
    }

    public void setData (List<Get_edittimeline_datas_pojo>Data)
    {
        this.Data = Data;
    }

 public void setDataimage (List<Edituser_image_pojo>Dataimage)
    {
        this.Dataimage = Dataimage;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(int responseCode) {
        ResponseCode = responseCode;
    }

    public String getMethodName ()
    {
        return methodName;
    }

    public void setMethodName (String methodName)
    {
        this.methodName = methodName;
    }

    public String getResponseMessage ()
    {
        return ResponseMessage;
    }

    public void setResponseMessage (String ResponseMessage)
    {
        this.ResponseMessage = ResponseMessage;
    }

    public String getCustom ()
    {
        return custom;
    }

    public void setCustom (String custom)
    {
        this.custom = custom;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Data = "+Data+", status = "+status+", ResponseCode = "+ResponseCode+", methodName = "+methodName+", ResponseMessage = "+ResponseMessage+", custom = "+custom+"]";
    }
}
