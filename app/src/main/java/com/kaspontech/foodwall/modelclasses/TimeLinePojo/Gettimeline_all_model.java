package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

/**
 * Created by vishnukm on 14/3/18.
 */

public class Gettimeline_all_model {
   private String created_by;

    private String created_on;

    private String imei;

    private Image image;

    private String cont_no;

    private String total_likes;

    private String picture;

    private String oauth_uid;

    private String first_name;

    private String email;

    private String oauth_provider;

    private String dob;

    private String timeline_description;

    private String last_name;

    private String total_comments;

    private String gender;

    private String longitude;

    private String user_id;

    private String latitude;

    private String timeline_id;

    public Gettimeline_all_model(String s, String s1, Image imagePojo, String s2, String s3, String s4, String s5, String s6, String s7, String s8, String s9, String s10, String s11, String s12, String s13, String s14, String s15, String s16, String s17, String s18, String s19) {

        this.created_by = s;
        this.created_on = s1;
        this.imei = s3;
        this.image = imagePojo;
        this.cont_no = s4;
        this.total_likes = s5;
        this.picture = s6;
        this.oauth_uid = s7;
        this.first_name = s8;
        this.email = s9;
        this.oauth_provider = s10;
        this.dob = s11;
        this.timeline_description = s12;
        this.last_name = s13;
        this.total_comments = s14;
        this.gender = s15;
        this.longitude = s16;
        this.user_id = s17;
        this.latitude = s18;
        this.timeline_id = s19;


    }

    public String getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (String created_by)
    {
        this.created_by = created_by;
    }

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getImei ()
    {
        return imei;
    }

    public void setImei (String imei)
    {
        this.imei = imei;
    }

    public Image getImage ()
    {
        return image;
    }

    public void setImage (Image image)
    {
        this.image = image;
    }

    public String getCont_no ()
    {
        return cont_no;
    }

    public void setCont_no (String cont_no)
    {
        this.cont_no = cont_no;
    }

    public String getTotal_likes ()
    {
        return total_likes;
    }

    public void setTotal_likes (String total_likes)
    {
        this.total_likes = total_likes;
    }

    public String getPicture ()
    {
        return picture;
    }

    public void setPicture (String picture)
    {
        this.picture = picture;
    }

    public String getOauth_uid ()
    {
        return oauth_uid;
    }

    public void setOauth_uid (String oauth_uid)
    {
        this.oauth_uid = oauth_uid;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getOauth_provider ()
    {
        return oauth_provider;
    }

    public void setOauth_provider (String oauth_provider)
    {
        this.oauth_provider = oauth_provider;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getTimeline_description ()
    {
        return timeline_description;
    }

    public void setTimeline_description (String timeline_description)
    {
        this.timeline_description = timeline_description;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getTotal_comments ()
    {
        return total_comments;
    }

    public void setTotal_comments (String total_comments)
    {
        this.total_comments = total_comments;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getTimeline_id ()
    {
        return timeline_id;
    }

    public void setTimeline_id (String timeline_id)
    {
        this.timeline_id = timeline_id;
    }

    public Gettimeline_all_model(String created_by, String created_on, String imei, Image image, String cont_no, String total_likes, String picture, String oauth_uid, String first_name, String email, String oauth_provider, String dob, String timeline_description, String last_name, String total_comments, String gender, String longitude, String user_id, String latitude, String timeline_id) {
        this.created_by = created_by;
        this.created_on = created_on;
        this.imei = imei;
        this.image = image;
        this.cont_no = cont_no;
        this.total_likes = total_likes;
        this.picture = picture;
        this.oauth_uid = oauth_uid;
        this.first_name = first_name;
        this.email = email;
        this.oauth_provider = oauth_provider;
        this.dob = dob;
        this.timeline_description = timeline_description;
        this.last_name = last_name;
        this.total_comments = total_comments;
        this.gender = gender;
        this.longitude = longitude;
        this.user_id = user_id;
        this.latitude = latitude;
        this.timeline_id = timeline_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [created_by = "+created_by+", created_on = "+created_on+", imei = "+imei+", Image = "+image+", cont_no = "+cont_no+", total_likes = "+total_likes+", picture = "+picture+", oauth_uid = "+oauth_uid+", first_name = "+first_name+", email = "+email+", oauth_provider = "+oauth_provider+", dob = "+dob+", timeline_description = "+timeline_description+", last_name = "+last_name+", total_comments = "+total_comments+", gender = "+gender+", longitude = "+longitude+", user_id = "+user_id+", latitude = "+latitude+", timeline_id = "+timeline_id+"]";
    }


}
