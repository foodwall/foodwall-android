package com.kaspontech.foodwall.modelclasses.Chat_Pojo.NewChatPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewChatOuputPojo {

    @SerializedName("methodName")
    @Expose
    private String methodName;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Data")
    @Expose
    private NewChatInputPojo data;
    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("ResponseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("custom")
    @Expose
    private String custom;

    /**
     * No args constructor for use in serialization
     *
     */
    public NewChatOuputPojo() {
    }

    /**
     *
     * @param responseMessage
     * @param responseCode
     * @param status
     * @param data
     * @param methodName
     * @param custom
     */
    public NewChatOuputPojo(String methodName, Integer status, NewChatInputPojo data, Integer responseCode, String responseMessage, String custom) {
        super();
        this.methodName = methodName;
        this.status = status;
        this.data = data;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.custom = custom;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public NewChatInputPojo getData() {
        return data;
    }

    public void setData(NewChatInputPojo data) {
        this.data = data;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

}
