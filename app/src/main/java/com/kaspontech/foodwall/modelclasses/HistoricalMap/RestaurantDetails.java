
package com.kaspontech.foodwall.modelclasses.HistoricalMap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestaurantDetails {

    @SerializedName("html_attributions")
    @Expose
    private List<Object> htmlAttributions = null;
    @SerializedName("results")
    @Expose
    private List<GetRestaurantsResult> getRestaurantsResults = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("next_page_token")
    @Expose
    private String next_page_token;

    public String getNext_page_token() {
        return next_page_token;
    }

    public void setNext_page_token(String next_page_token) {
        this.next_page_token = next_page_token;
    }

    public List<Object> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<Object> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }

    public List<GetRestaurantsResult> getGetRestaurantsResults() {
        return getRestaurantsResults;
    }

    public void setGetRestaurantsResults(List<GetRestaurantsResult> getRestaurantsResults) {
        this.getRestaurantsResults = getRestaurantsResults;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
