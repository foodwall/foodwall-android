package com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchEventPojo {

    @SerializedName("event_id")
    @Expose
    private int eventId;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_description")
    @Expose
    private String eventDescription;
    @SerializedName("event_image")
    @Expose
    private String eventImage;
    @SerializedName("total_likes")
    @Expose
    private String totalLikes;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("total_going")
    @Expose
    private String totalGoing;
    @SerializedName("event_type")
    @Expose
    private int eventType;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("ticket_url")
    @Expose
    private String ticketUrl;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("search_val")
    @Expose
    private String search_val;
    @SerializedName("total_discussion")
    @Expose
    private String totalDiscussion;
    @SerializedName("likes_evt_id")
    @Expose
    private String likesEvtId;
    @SerializedName("evt_likes")
    @Expose
    private int evtLikes;
    @SerializedName("going_evt_id")
    @Expose
    private String goingEvtId;
    @SerializedName("gng_likes")
    @Expose
    private int gngLikes;
    @SerializedName("following_id")
    @Expose
    private String followingId;
    @SerializedName("userinterested")
    @Expose
    private boolean userInterested;
    @SerializedName("userGoing")
    @Expose
    private boolean userGoing;

    public boolean isUserInterested() {
        return userInterested;
    }

    public void setUserInterested(boolean userInterested) {
        this.userInterested = userInterested;
    }

    public boolean isUserGoing() {
        return userGoing;
    }

    public void setUserGoing(boolean userGoing) {
        this.userGoing = userGoing;
    }

    public String getTotalDiscussion() {
        return totalDiscussion;
    }

    public void setTotalDiscussion(String totalDiscussion) {
        this.totalDiscussion = totalDiscussion;
    }

    public String getLikesEvtId() {
        return likesEvtId;
    }

    public void setLikesEvtId(String likesEvtId) {
        this.likesEvtId = likesEvtId;
    }

    public int getEvtLikes() {
        return evtLikes;
    }

    public void setEvtLikes(int evtLikes) {
        this.evtLikes = evtLikes;
    }

    public String getGoingEvtId() {
        return goingEvtId;
    }

    public void setGoingEvtId(String goingEvtId) {
        this.goingEvtId = goingEvtId;
    }

    public int getGngLikes() {
        return gngLikes;
    }

    public void setGngLikes(int gngLikes) {
        this.gngLikes = gngLikes;
    }

    public String getFollowingId() {
        return followingId;
    }

    public void setFollowingId(String followingId) {
        this.followingId = followingId;
    }

    public String getSearch_val() {
        return search_val;
    }

    public void setSearch_val(String search_val) {
        this.search_val = search_val;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getTotalGoing() {
        return totalGoing;
    }

    public void setTotalGoing(String totalGoing) {
        this.totalGoing = totalGoing;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTicketUrl() {
        return ticketUrl;
    }

    public void setTicketUrl(String ticketUrl) {
        this.ticketUrl = ticketUrl;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}
