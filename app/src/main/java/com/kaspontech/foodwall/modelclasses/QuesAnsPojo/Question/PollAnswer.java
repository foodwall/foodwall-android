package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Question;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PollAnswer {

    @SerializedName("pollName")
    @Expose
    List<String> pollname = new ArrayList<>();


    public PollAnswer(List<String> pollname) {
        this.pollname = pollname;
    }


    public List<String> getPollname() {
        return pollname;
    }

    public void setPollname(List<String> pollname) {
        this.pollname = pollname;
    }
}
