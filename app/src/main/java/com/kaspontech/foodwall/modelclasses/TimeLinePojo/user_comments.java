package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

/**
 * Created by vishnukm on 19/3/18.
 */


public class user_comments {

    private String timeline_id;
    private String timeline_description;
    private String total_comments;
    private String cmmt_tl_id;
    private String tl_comments;
    private String created_by;
    private String created_on;
    private String user_id;
    private String first_name;
    private String last_name;
    private String email;
    private String cont_no;
    private String gender;
    private String dob;
    private String picture;

    /**
     * No args constructor for use in serialization
     *
     */
    public user_comments() {
    }

    public user_comments(String timeline_id, String timeline_description, String total_comments, String cmmt_tl_id, String tl_comments, String created_by, String created_on, String user_id, String first_name, String last_name, String email, String cont_no, String gender, String dob, String picture) {
        this.timeline_id = timeline_id;
        this.timeline_description = timeline_description;
        this.total_comments = total_comments;
        this.cmmt_tl_id = cmmt_tl_id;
        this.tl_comments = tl_comments;
        this.created_by = created_by;
        this.created_on = created_on;
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.cont_no = cont_no;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
    }



    public String getTimeline_id() {
        return timeline_id;
    }

    public void setTimeline_id(String timeline_id) {
        this.timeline_id = timeline_id;
    }

    public String getTimeline_description() {
        return timeline_description;
    }

    public void setTimeline_description(String timeline_description) {
        this.timeline_description = timeline_description;
    }

    public String getTotal_comments() {
        return total_comments;
    }

    public void setTotal_comments(String total_comments) {
        this.total_comments = total_comments;
    }

    public String getCmmt_tl_id() {
        return cmmt_tl_id;
    }

    public void setCmmt_tl_id(String cmmt_tl_id) {
        this.cmmt_tl_id = cmmt_tl_id;
    }

    public String getTl_comments() {
        return tl_comments;
    }

    public void setTl_comments(String tl_comments) {
        this.tl_comments = tl_comments;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}