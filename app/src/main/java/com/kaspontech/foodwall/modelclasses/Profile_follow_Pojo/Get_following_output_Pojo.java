package com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo;

import java.util.List;

/**
 * Created by vishnukm on 30/3/18.
 */

public class Get_following_output_Pojo {


    private List<Get_following_profile_pojo> Data = null;
//
//    private GetallCommentsPojo[] Data;

    private String status;

    private int ResponseCode;

    private String methodName;

    private String ResponseMessage;

    private String custom;

    public  List<Get_following_profile_pojo> getData ()
    {
        return Data;
    }

    public void setData ( List<Get_following_profile_pojo> Data)
    {
        this.Data = Data;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(int responseCode) {
        ResponseCode = responseCode;
    }

    public String getMethodName ()
    {
        return methodName;
    }

    public void setMethodName (String methodName)
    {
        this.methodName = methodName;
    }

    public String getResponseMessage ()
    {
        return ResponseMessage;
    }

    public void setResponseMessage (String ResponseMessage)
    {
        this.ResponseMessage = ResponseMessage;
    }

    public String getCustom ()
    {
        return custom;
    }

    public void setCustom (String custom)
    {
        this.custom = custom;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Data = "+Data+", status = "+status+", ResponseCode = "+ResponseCode+", methodName = "+methodName+", ResponseMessage = "+ResponseMessage+", custom = "+custom+"]";
    }
}
