package com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Image;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.TaggedPeoplePojo;

import java.util.List;

public class SearchTimelinePojo {


    @SerializedName("search_val")
    @Expose
    private String searchVal;
    @SerializedName("timeline_id")
    @Expose
    private String timelineId;
    @SerializedName("hotel_id")
    @Expose
    private String hotelId;
    @SerializedName("timeline_hotel")
    @Expose
    private String timelineHotel;
    @SerializedName("timeline_description")
    @Expose
    private String timelineDescription;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("total_likes")
    @Expose
    private Integer totalLikes;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("total_posts")
    @Expose
    private String totalPosts;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("likes_tl_id")
    @Expose
    private String likesTlId;
    @SerializedName("tl_likes")
    @Expose
    private String tlLikes;
    @SerializedName("whom")
    @Expose
    private List<TaggedPeoplePojo> whom = null;
    @SerializedName("whom_count")
    @Expose
    private Integer whomCount;
    @SerializedName("image")
    @Expose
    private List<Image> image = null;
    @SerializedName("image_count")
    @Expose
    private Integer imageCount;
    @SerializedName("liked")
    @Expose
    private boolean liked;
    @SerializedName("total_followers")
    @Expose
    private String totalFollowers;
    @SerializedName("total_followings")
    @Expose
    private String totalFollowings;


    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getTotalFollowers() {
        return totalFollowers;
    }

    public void setTotalFollowers(String totalFollowers) {
        this.totalFollowers = totalFollowers;
    }

    public String getTotalFollowings() {
        return totalFollowings;
    }

    public void setTotalFollowings(String totalFollowings) {
        this.totalFollowings = totalFollowings;
    }

    public String getSearchVal() {
        return searchVal;
    }

    public void setSearchVal(String searchVal) {
        this.searchVal = searchVal;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }


    public String getTimelineHotel() {
        return timelineHotel;
    }

    public void setTimelineHotel(String timelineHotel) {
        this.timelineHotel = timelineHotel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(Integer totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getTotalPosts() {
        return totalPosts;
    }

    public void setTotalPosts(String totalPosts) {
        this.totalPosts = totalPosts;
    }

    public String getLikesTlId() {
        return likesTlId;
    }

    public void setLikesTlId(String likesTlId) {
        this.likesTlId = likesTlId;
    }

    public String getTlLikes() {
        return tlLikes;
    }

    public void setTlLikes(String tlLikes) {
        this.tlLikes = tlLikes;
    }

    public Integer getWhomCount() {
        return whomCount;
    }

    public void setWhomCount(Integer whomCount) {
        this.whomCount = whomCount;
    }

    public List<Image> getImage() {
        return image;
    }

    public void setImage(List<Image> image) {
        this.image = image;
    }

    public Integer getImageCount() {
        return imageCount;
    }

    public void setImageCount(Integer imageCount) {
        this.imageCount = imageCount;
    }

    public List<TaggedPeoplePojo> getWhom() {
        return whom;
    }

    public void setWhom(List<TaggedPeoplePojo> whom) {
        this.whom = whom;
    }

    public String getSearch_val() {
        return searchVal;
    }

    public void setSearch_val(String search_val) {
        this.searchVal = search_val;
    }

    public String getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(String timelineId) {
        this.timelineId = timelineId;
    }

    public String getTimelineDescription() {
        return timelineDescription;
    }

    public void setTimelineDescription(String timelineDescription) {
        this.timelineDescription = timelineDescription;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}
