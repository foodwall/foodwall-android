package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Question;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PollListPojo {

    @SerializedName("pollname")
    @Expose
    private List<String> pollname;

    public PollListPojo(List<String> pollList) {
        this.pollname = pollList;
    }

    public List<String> getPollList() {
        return pollname;
    }

    public void setPollList(List<String> pollList) {
        this.pollname = pollList;
    }


}
