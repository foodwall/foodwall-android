package com.kaspontech.foodwall.modelclasses.EventsPojo.EventLikes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiscussionLikesInputPojo {

    @SerializedName("likes_dis_id")
    @Expose
    private String likesDisId;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;

    /**
     * No args constructor for use in serialization
     *
     */
    public DiscussionLikesInputPojo() {
    }

    /**
     *
     * @param picture
     * @param createdOn
     * @param lastName
     * @param createdBy
     * @param email
     * @param dob
     * @param userId
     * @param likesDisId
     * @param gender
     * @param contNo
     * @param firstName
     */
    public DiscussionLikesInputPojo(String likesDisId, String createdBy, String createdOn, String userId, String firstName, String lastName, String email, String contNo, String gender, String dob, String picture) {
        super();
        this.likesDisId = likesDisId;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
    }

    public String getLikesDisId() {
        return likesDisId;
    }

    public void setLikesDisId(String likesDisId) {
        this.likesDisId = likesDisId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}
