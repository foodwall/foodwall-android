package com.kaspontech.foodwall.modelclasses.LoginPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by balaji on 20/3/18.
 */

public class UserData {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("gcmkey")
    @Expose
    private String gcmkey;
    @SerializedName("ip_address")
    @Expose
    private String ipAddress;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("locale")
    @Expose
    private String locale;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("auth_type")
    @Expose
    private String authType;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("login_type")
    @Expose
    private String loginType;
    @SerializedName("profile_status")
    @Expose
    private String profileStatus;
    @SerializedName("total_followers")
    @Expose
    private String totalFollowers;
    @SerializedName("total_followings")
    @Expose
    private String totalFollowings;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("modified_time")
    @Expose
    private String modifiedTime;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("countrycode")
    @Expose
    private String countrycode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("otp_number")
    @Expose
    private String otpNumber;
    @SerializedName("verify_otp")
    @Expose
    private String verifyOtp;

    /**
     * No args constructor for use in serialization
     *
     */
    public UserData() {
    }

    /**
     *
     * @param gcmkey
     * @param otpNumber
     * @param oauthProvider
     * @param countrycode
     * @param locale
     * @param link
     * @param totalFollowers
     * @param password
     * @param loginType
     * @param city
     * @param profileStatus
     * @param username
     * @param userId
     * @param gender
     * @param longitude
     * @param authType
     * @param firstName
     * @param ipAddress
     * @param lastName
     * @param createdTime
     * @param imei
     * @param modifiedTime
     * @param verifyOtp
     * @param deleted
     * @param country
     * @param oauthUid
     * @param picture
     * @param email
     * @param totalFollowings
     * @param latitude
     */
    public UserData(String userId, String oauthProvider, String oauthUid, String firstName, String lastName, String username, String email, String imei, String gcmkey, String ipAddress, String password, String gender, String locale, String picture, String authType, String link, String loginType, String profileStatus, String totalFollowers, String totalFollowings, String createdTime, String modifiedTime, String deleted, String latitude, String longitude, String country, String countrycode, String city, String otpNumber, String verifyOtp) {
        super();
        this.userId = userId;
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.imei = imei;
        this.gcmkey = gcmkey;
        this.ipAddress = ipAddress;
        this.password = password;
        this.gender = gender;
        this.locale = locale;
        this.picture = picture;
        this.authType = authType;
        this.link = link;
        this.loginType = loginType;
        this.profileStatus = profileStatus;
        this.totalFollowers = totalFollowers;
        this.totalFollowings = totalFollowings;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
        this.deleted = deleted;
        this.latitude = latitude;
        this.longitude = longitude;
        this.country = country;
        this.countrycode = countrycode;
        this.city = city;
        this.otpNumber = otpNumber;
        this.verifyOtp = verifyOtp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getGcmkey() {
        return gcmkey;
    }

    public void setGcmkey(String gcmkey) {
        this.gcmkey = gcmkey;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getTotalFollowers() {
        return totalFollowers;
    }

    public void setTotalFollowers(String totalFollowers) {
        this.totalFollowers = totalFollowers;
    }

    public String getTotalFollowings() {
        return totalFollowings;
    }

    public void setTotalFollowings(String totalFollowings) {
        this.totalFollowings = totalFollowings;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOtpNumber() {
        return otpNumber;
    }

    public void setOtpNumber(String otpNumber) {
        this.otpNumber = otpNumber;
    }

    public String getVerifyOtp() {
        return verifyOtp;
    }

    public void setVerifyOtp(String verifyOtp) {
        this.verifyOtp = verifyOtp;
    }

}