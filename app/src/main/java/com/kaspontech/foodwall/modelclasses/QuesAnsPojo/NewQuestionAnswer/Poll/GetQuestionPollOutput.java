package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewQuestionAnswer.Poll;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewQuestionAnswer.Answer.GetQuestionAnswerData;

import java.util.List;

public class GetQuestionPollOutput {

    @SerializedName("methodName")
    @Expose
    private String methodName;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Data")
    @Expose
    private List<GetQuestionAnswerData> data = null;
    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("ResponseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("custom")
    @Expose
    private String custom;

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<GetQuestionAnswerData> getData() {
        return data;
    }

    public void setData(List<GetQuestionAnswerData> data) {
        this.data = data;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }


}
