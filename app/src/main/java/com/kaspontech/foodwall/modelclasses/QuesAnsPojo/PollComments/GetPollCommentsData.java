package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.PollComments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetPollCommentsData {

    @SerializedName("quest_id")
    @Expose
    private String questId;
    @SerializedName("ask_question")
    @Expose
    private String askQuestion;
    @SerializedName("total_answers")
    @Expose
    private String totalAnswers;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("cmmt_ques_id")
    @Expose
    private String cmmtQuesId;
    @SerializedName("ques_comments")
    @Expose
    private String quesComments;
    @SerializedName("total_cmmt_likes")
    @Expose
    private String totalCmmtLikes;
    @SerializedName("total_cmmt_reply")
    @Expose
    private String totalCmmtReply;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("cmmt_likes_id")
    @Expose
    private String cmmtLikesId;
    @SerializedName("ques_cmmt_likes")
    @Expose
    private String quesCmmtLikes;
    @SerializedName("reply")
    @Expose
    private List<GetPollCmtReply> reply = null;

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public String getAskQuestion() {
        return askQuestion;
    }

    public void setAskQuestion(String askQuestion) {
        this.askQuestion = askQuestion;
    }

    public String getTotalAnswers() {
        return totalAnswers;
    }

    public void setTotalAnswers(String totalAnswers) {
        this.totalAnswers = totalAnswers;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getCmmtQuesId() {
        return cmmtQuesId;
    }

    public void setCmmtQuesId(String cmmtQuesId) {
        this.cmmtQuesId = cmmtQuesId;
    }

    public String getQuesComments() {
        return quesComments;
    }

    public void setQuesComments(String quesComments) {
        this.quesComments = quesComments;
    }

    public String getTotalCmmtLikes() {
        return totalCmmtLikes;
    }

    public void setTotalCmmtLikes(String totalCmmtLikes) {
        this.totalCmmtLikes = totalCmmtLikes;
    }

    public String getTotalCmmtReply() {
        return totalCmmtReply;
    }

    public void setTotalCmmtReply(String totalCmmtReply) {
        this.totalCmmtReply = totalCmmtReply;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getCmmtLikesId() {
        return cmmtLikesId;
    }

    public void setCmmtLikesId(String cmmtLikesId) {
        this.cmmtLikesId = cmmtLikesId;
    }

    public String getQuesCmmtLikes() {
        return quesCmmtLikes;
    }

    public void setQuesCmmtLikes(String quesCmmtLikes) {
        this.quesCmmtLikes = quesCmmtLikes;
    }

    public List<GetPollCmtReply> getReply() {
        return reply;
    }

    public void setReply(List<GetPollCmtReply> reply) {
        this.reply = reply;
    }
}
