package com.kaspontech.foodwall.modelclasses.Review_pojo;

public class ReviewSpinner_Pojo {
    private String name;
    private String id;


    public ReviewSpinner_Pojo(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
