package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

public class CreateUserPojoOutput {
    private String Data;

    private String status;

    private int ResponseCode;

    private String methodName;

    private String ResponseMessage;

    private String custom;

    public String getData ()
    {
        return Data;
    }

    public CreateUserPojoOutput(String data, String status, int responseCode, String methodName, String responseMessage, String custom) {
        Data = data;
        this.status = status;
        ResponseCode = responseCode;
        this.methodName = methodName;
        ResponseMessage = responseMessage;
        this.custom = custom;
    }

    public void setData (String Data)
    {
        this.Data = Data;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public int getResponseCode ()
    {
        return ResponseCode;
    }

    public void setResponseCode (int ResponseCode)
    {
        this.ResponseCode = ResponseCode;
    }

    public String getMethodName ()
    {
        return methodName;
    }

    public void setMethodName (String methodName)
    {
        this.methodName = methodName;
    }

    public String getResponseMessage ()
    {
        return ResponseMessage;
    }

    public void setResponseMessage (String ResponseMessage)
    {
        this.ResponseMessage = ResponseMessage;
    }

    public String getCustom ()
    {
        return custom;
    }

    public void setCustom (String custom)
    {
        this.custom = custom;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Data = "+Data+", status = "+status+", ResponseCode = "+ResponseCode+", methodName = "+methodName+", ResponseMessage = "+ResponseMessage+", custom = "+custom+"]";
    }

}