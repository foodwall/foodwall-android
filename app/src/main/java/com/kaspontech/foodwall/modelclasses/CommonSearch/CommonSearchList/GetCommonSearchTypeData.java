package com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCommonSearchTypeData {

    @SerializedName("searchtext")
    @Expose
    private String searchtext;
    @SerializedName("searchtype")
    @Expose
    private String searchtype;

    public GetCommonSearchTypeData(String searchtext, String searchtype) {
        this.searchtext = searchtext;
        this.searchtype = searchtype;
    }

    public String getSearchtext() {
        return searchtext;
    }

    public void setSearchtext(String searchtext) {
        this.searchtext = searchtext;
    }

    public String getSearchtype() {
        return searchtype;
    }

    public void setSearchtype(String searchtype) {
        this.searchtype = searchtype;
    }

}
