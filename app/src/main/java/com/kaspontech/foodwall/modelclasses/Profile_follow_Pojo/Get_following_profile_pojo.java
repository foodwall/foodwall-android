package com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo;

/**
 * Created by vishnukm on 30/3/18.
 */

public class Get_following_profile_pojo {

    private String created_on;

    private String link;

    private String locale;

    private String city;

    private String country;

    private String cont_no;

    private String picture;

    private String oauth_uid;

    private String first_name;

    private String email;

    private String oauth_provider;

    private String dob;

    private String last_name;

    private String gender;

    private String total_followings;

    private String longitude;

    private String user_id;

    private String latitude;

    private String following_id;

    private String total_followers;

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getLink ()
    {
        return link;
    }

    public void setLink (String link)
    {
        this.link = link;
    }

    public String getLocale ()
    {
        return locale;
    }

    public void setLocale (String locale)
    {
        this.locale = locale;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    public String getCont_no ()
    {
        return cont_no;
    }

    public void setCont_no (String cont_no)
    {
        this.cont_no = cont_no;
    }

    public String getPicture ()
    {
        return picture;
    }

    public void setPicture (String picture)
    {
        this.picture = picture;
    }

    public String getOauth_uid ()
    {
        return oauth_uid;
    }

    public void setOauth_uid (String oauth_uid)
    {
        this.oauth_uid = oauth_uid;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getOauth_provider ()
    {
        return oauth_provider;
    }

    public void setOauth_provider (String oauth_provider)
    {
        this.oauth_provider = oauth_provider;
    }

    public String getDob ()
    {
        return dob;
    }

    public Get_following_profile_pojo(String created_on, String link, String locale, String city, String country, String cont_no, String picture, String oauth_uid, String first_name, String email, String oauth_provider, String dob, String last_name, String gender, String total_followings, String longitude, String user_id, String latitude, String following_id, String total_followers) {
        this.created_on = created_on;
        this.link = link;
        this.locale = locale;
        this.city = city;
        this.country = country;
        this.cont_no = cont_no;
        this.picture = picture;
        this.oauth_uid = oauth_uid;
        this.first_name = first_name;
        this.email = email;
        this.oauth_provider = oauth_provider;
        this.dob = dob;
        this.last_name = last_name;
        this.gender = gender;
        this.total_followings = total_followings;
        this.longitude = longitude;
        this.user_id = user_id;
        this.latitude = latitude;
        this.following_id = following_id;
        this.total_followers = total_followers;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getTotal_followings ()
    {
        return total_followings;
    }

    public void setTotal_followings (String total_followings)
    {
        this.total_followings = total_followings;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getFollowing_id ()
    {
        return following_id;
    }

    public void setFollowing_id (String following_id)
    {
        this.following_id = following_id;
    }

    public String getTotal_followers ()
    {
        return total_followers;
    }

    public void setTotal_followers (String total_followers)
    {
        this.total_followers = total_followers;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [created_on = "+created_on+", link = "+link+", locale = "+locale+", city = "+city+", country = "+country+", cont_no = "+cont_no+", picture = "+picture+", oauth_uid = "+oauth_uid+", first_name = "+first_name+", email = "+email+", oauth_provider = "+oauth_provider+", dob = "+dob+", last_name = "+last_name+", gender = "+gender+", total_followings = "+total_followings+", longitude = "+longitude+", user_id = "+user_id+", latitude = "+latitude+", following_id = "+following_id+", total_followers = "+total_followers+"]";
    }
}
