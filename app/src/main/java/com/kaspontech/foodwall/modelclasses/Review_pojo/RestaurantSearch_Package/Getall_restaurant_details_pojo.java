package com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Getall_restaurant_details_pojo {
    @SerializedName("geometry")
    @Expose
    private Geometry_pojo geometry;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("opening_hours")
    @Expose
    private OpeningHours_pojo openingHours;
    @SerializedName("photos")
    @Expose
    private List<Get_hotel_photo_pojo> photos = null;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("scope")
    @Expose
    private String scope;
    @SerializedName("types")
    @Expose
    private List<String> types = null;
    @SerializedName("formatted_address")
    @Expose
    private String formatted_address;


    /**
     * No args constructor for use in serialization
     *
     */


    /**
     * @param photos
     * @param id
     * @param icon
     * @param formatted_address
     * @param scope
     * @param placeId
     * @param openingHours
     * @param name
     * @param rating
     * @param types
     * @param reference
     * @param geometry
     */
    public Getall_restaurant_details_pojo(Geometry_pojo geometry, String icon, String id, String name, OpeningHours_pojo openingHours, List<Get_hotel_photo_pojo> photos, String placeId, String rating, String reference, String scope, List<String> types, String formatted_address) {
        super();
        this.geometry = geometry;
        this.icon = icon;
        this.id = id;
        this.name = name;
        this.openingHours = openingHours;
        this.photos = photos;
        this.placeId = placeId;
        this.rating = rating;
        this.reference = reference;
        this.scope = scope;
        this.types = types;
        this.formatted_address = formatted_address;

    }

    public Geometry_pojo getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry_pojo geometry) {
        this.geometry = geometry;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OpeningHours_pojo getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(OpeningHours_pojo openingHours) {
        this.openingHours = openingHours;
    }

    public List<Get_hotel_photo_pojo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Get_hotel_photo_pojo> photos) {
        this.photos = photos;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }
}
