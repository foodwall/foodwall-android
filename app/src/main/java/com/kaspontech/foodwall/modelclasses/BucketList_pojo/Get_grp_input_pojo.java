package com.kaspontech.foodwall.modelclasses.BucketList_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_grp_input_pojo {
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("from_firstname")
    @Expose
    private String fromFirstname;
    @SerializedName("from_lastname")
    @Expose
    private String fromLastname;
    @SerializedName("from_picture")
    @Expose
    private String fromPicture;
    @SerializedName("group_name")
    @Expose
    private String groupName;

    @SerializedName("group_id")
    @Expose
    private String group_id;
    @SerializedName("group_icon")
    @Expose
    private String groupIcon;
    @SerializedName("changed_which")
    @Expose
    private String changedWhich;
    @SerializedName("group_createdby")
    @Expose
    private String groupCreatedby;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("group_created_firstname")
    @Expose
    private String groupCreatedFirstname;
    @SerializedName("group_created_lastname")
    @Expose
    private String groupCreatedLastname;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param groupCreatedFirstname
     * @param groupName
     * @param groupIcon
     * @param fromLastname
     * @param fromPicture
     * @param groupCreatedby
     * @param userid
     * @param changedWhich
     * @param createdDate
     * @param groupCreatedLastname
     * @param fromFirstname
     */
    public Get_grp_input_pojo(String userid, String fromFirstname, String fromLastname, String fromPicture, String group_id,String groupName, String groupIcon, String changedWhich, String groupCreatedby, String createdDate, String groupCreatedFirstname, String groupCreatedLastname) {
        super();
        this.userid = userid;
        this.fromFirstname = fromFirstname;
        this.fromLastname = fromLastname;
        this.fromPicture = fromPicture;
        this.groupName = groupName;
        this.group_id = group_id;
        this.groupIcon = groupIcon;
        this.changedWhich = changedWhich;
        this.groupCreatedby = groupCreatedby;
        this.createdDate = createdDate;
        this.groupCreatedFirstname = groupCreatedFirstname;
        this.groupCreatedLastname = groupCreatedLastname;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFromFirstname() {
        return fromFirstname;
    }

    public void setFromFirstname(String fromFirstname) {
        this.fromFirstname = fromFirstname;
    }

    public String getFromLastname() {
        return fromLastname;
    }

    public void setFromLastname(String fromLastname) {
        this.fromLastname = fromLastname;
    }

    public String getFromPicture() {
        return fromPicture;
    }

    public void setFromPicture(String fromPicture) {
        this.fromPicture = fromPicture;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupIcon() {
        return groupIcon;
    }

    public void setGroupIcon(String groupIcon) {
        this.groupIcon = groupIcon;
    }

    public String getChangedWhich() {
        return changedWhich;
    }

    public void setChangedWhich(String changedWhich) {
        this.changedWhich = changedWhich;
    }

    public String getGroupCreatedby() {
        return groupCreatedby;
    }

    public void setGroupCreatedby(String groupCreatedby) {
        this.groupCreatedby = groupCreatedby;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getGroupCreatedFirstname() {
        return groupCreatedFirstname;
    }

    public void setGroupCreatedFirstname(String groupCreatedFirstname) {
        this.groupCreatedFirstname = groupCreatedFirstname;
    }

    public String getGroupCreatedLastname() {
        return groupCreatedLastname;
    }

    public void setGroupCreatedLastname(String groupCreatedLastname) {
        this.groupCreatedLastname = groupCreatedLastname;
    }
}
