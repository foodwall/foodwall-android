package com.kaspontech.foodwall.modelclasses.Review_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAllInsideReviewPojo {

    @SerializedName("hotel_id")
    @Expose
    private String hotelId;
    @SerializedName("google_id")
    @Expose
    private String googleId;
    @SerializedName("hotel_name")
    @Expose
    private String hotelName;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("open_times")
    @Expose
    private String openTimes;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("photo_reference")
    @Expose
    private String photoReference;
    @SerializedName("bucket_list")
    @Expose
    private String bucketList;
    @SerializedName("total_review")
    @Expose
    private String totalReview;
    @SerializedName("total_food_exprience")
    @Expose
    private String totalFoodExprience;
    @SerializedName("total_ambiance")
    @Expose
    private String totalAmbiance;
    @SerializedName("total_taste")
    @Expose
    private String totalTaste;
    @SerializedName("total_service")
    @Expose
    private String totalService;
    @SerializedName("total_package")
    @Expose
    private String totalPackage;
    @SerializedName("total_timedelivery")
    @Expose
    private String totalTimedelivery;
    @SerializedName("total_value_money")
    @Expose
    private String totalValueMoney;
    @SerializedName("total_good")
    @Expose
    private String totalGood;
    @SerializedName("total_bad")
    @Expose
    private String totalBad;
    @SerializedName("total_good_bad_user")
    @Expose
    private String totalGoodBadUser;
    @SerializedName("ambi_image_count")
    @Expose
    private Integer ambiImageCount;
    @SerializedName("topdish")
    @Expose
    private String topdish;
    @SerializedName("top_count")
    @Expose
    private Integer topCount;
    @SerializedName("top_dish_img_count")
    @Expose
    private Integer topDishImgCount;
    @SerializedName("avoiddish")
    @Expose
    private String avoiddish;
    @SerializedName("avoid_count")
    @Expose
    private Integer avoidCount;
    @SerializedName("avoid_dish_img_count")
    @Expose
    private Integer avoidDishImgCount;
    @SerializedName("review")
    @Expose
    private List<GetAllReviewPojo> review = null;
    @SerializedName("review_count")
    @Expose
    private Integer reviewCount;

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(String openTimes) {
        this.openTimes = openTimes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public String getBucketList() {
        return bucketList;
    }

    public void setBucketList(String bucketList) {
        this.bucketList = bucketList;
    }

    public String getTotalReview() {
        return totalReview;
    }

    public void setTotalReview(String totalReview) {
        this.totalReview = totalReview;
    }

    public String getTotalFoodExprience() {
        return totalFoodExprience;
    }

    public void setTotalFoodExprience(String totalFoodExprience) {
        this.totalFoodExprience = totalFoodExprience;
    }

    public String getTotalAmbiance() {
        return totalAmbiance;
    }

    public void setTotalAmbiance(String totalAmbiance) {
        this.totalAmbiance = totalAmbiance;
    }

    public String getTotalTaste() {
        return totalTaste;
    }

    public void setTotalTaste(String totalTaste) {
        this.totalTaste = totalTaste;
    }

    public String getTotalService() {
        return totalService;
    }

    public void setTotalService(String totalService) {
        this.totalService = totalService;
    }

    public String getTotalPackage() {
        return totalPackage;
    }

    public void setTotalPackage(String totalPackage) {
        this.totalPackage = totalPackage;
    }

    public String getTotalTimedelivery() {
        return totalTimedelivery;
    }

    public void setTotalTimedelivery(String totalTimedelivery) {
        this.totalTimedelivery = totalTimedelivery;
    }

    public String getTotalValueMoney() {
        return totalValueMoney;
    }

    public void setTotalValueMoney(String totalValueMoney) {
        this.totalValueMoney = totalValueMoney;
    }

    public String getTotalGood() {
        return totalGood;
    }

    public void setTotalGood(String totalGood) {
        this.totalGood = totalGood;
    }

    public String getTotalBad() {
        return totalBad;
    }

    public void setTotalBad(String totalBad) {
        this.totalBad = totalBad;
    }

    public String getTotalGoodBadUser() {
        return totalGoodBadUser;
    }

    public void setTotalGoodBadUser(String totalGoodBadUser) {
        this.totalGoodBadUser = totalGoodBadUser;
    }

    public Integer getAmbiImageCount() {
        return ambiImageCount;
    }

    public void setAmbiImageCount(Integer ambiImageCount) {
        this.ambiImageCount = ambiImageCount;
    }

    public String getTopdish() {
        return topdish;
    }

    public void setTopdish(String topdish) {
        this.topdish = topdish;
    }

    public Integer getTopCount() {
        return topCount;
    }

    public void setTopCount(Integer topCount) {
        this.topCount = topCount;
    }

    public Integer getTopDishImgCount() {
        return topDishImgCount;
    }

    public void setTopDishImgCount(Integer topDishImgCount) {
        this.topDishImgCount = topDishImgCount;
    }

    public String getAvoiddish() {
        return avoiddish;
    }

    public void setAvoiddish(String avoiddish) {
        this.avoiddish = avoiddish;
    }

    public Integer getAvoidCount() {
        return avoidCount;
    }

    public void setAvoidCount(Integer avoidCount) {
        this.avoidCount = avoidCount;
    }

    public Integer getAvoidDishImgCount() {
        return avoidDishImgCount;
    }

    public void setAvoidDishImgCount(Integer avoidDishImgCount) {
        this.avoidDishImgCount = avoidDishImgCount;
    }

    public List<GetAllReviewPojo> getReview() {
        return review;
    }

    public void setReview(List<GetAllReviewPojo> review) {
        this.review = review;
    }

    public Integer getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }
}
