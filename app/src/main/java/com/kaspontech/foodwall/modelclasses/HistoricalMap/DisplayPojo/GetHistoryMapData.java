package com.kaspontech.foodwall.modelclasses.HistoricalMap.DisplayPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.TaggedPeoplePojo;

import java.io.Serializable;
import java.util.List;

public class GetHistoryMapData implements Serializable {


    @SerializedName("timeline_id")
    @Expose
    private String timeline_id;
    @SerializedName("his_id")
    @Expose
    private String hisId;
    @SerializedName("hotel_name")
    @Expose
    private String hotelName;
    @SerializedName("his_descripion")
    @Expose
    private String hisDescripion;
    @SerializedName("with_whom")
    @Expose
    private String withWhom;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("total_hotel")
    @Expose
    private String totalHotel;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("image")
    @Expose
    private List<GetHisMapImage> image = null;
    @SerializedName("whom")
    @Expose
    private List<TaggedPeoplePojo> whom = null;
    @SerializedName("image_count")
    @Expose
    private Integer imageCount;
    @SerializedName("whom_count")
    @Expose
    private Integer whom_count;


    public GetHistoryMapData(String timeline_id,String hisId, String hotelName, String hisDescripion, String withWhom, String address, String createdBy, String createdOn, String firstName, String lastName, String picture, List<GetHisMapImage> image, Integer imageCount,Integer whom_count,List<TaggedPeoplePojo> whom) {
        this.hisId = hisId;
        this.hotelName = hotelName;
        this.hisDescripion = hisDescripion;
        this.withWhom = withWhom;
        this.address = address;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.image = image;
        this.imageCount = imageCount;
        this.timeline_id = timeline_id;
        this.whom_count = whom_count;
        this.whom = whom;
    }


    public String getTimeline_id() {
        return timeline_id;
    }

    public void setTimeline_id(String timeline_id) {
        this.timeline_id = timeline_id;
    }

    public List<TaggedPeoplePojo> getWhom() {
        return whom;
    }

    public void setWhom(List<TaggedPeoplePojo> whom) {
        this.whom = whom;
    }

    public Integer getWhom_count() {
        return whom_count;
    }

    public void setWhom_count(Integer whom_count) {
        this.whom_count = whom_count;
    }

    public String getHisId() {
        return hisId;
    }

    public void setHisId(String hisId) {
        this.hisId = hisId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHisDescripion() {
        return hisDescripion;
    }

    public void setHisDescripion(String hisDescripion) {
        this.hisDescripion = hisDescripion;
    }

    public String getWithWhom() {
        return withWhom;
    }

    public void setWithWhom(String withWhom) {
        this.withWhom = withWhom;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTotalHotel() {
        return totalHotel;
    }

    public void setTotalHotel(String totalHotel) {
        this.totalHotel = totalHotel;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<GetHisMapImage> getImage() {
        return image;
    }

    public void setImage(List<GetHisMapImage> image) {
        this.image = image;
    }

    public Integer getImageCount() {
        return imageCount;
    }

    public void setImageCount(Integer imageCount) {
        this.imageCount = imageCount;
    }


}
