package com.kaspontech.foodwall.modelclasses.Review_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;

import java.util.List;

public class Get_hotel_all_inner_review_pojo {


    @SerializedName("search_val")
    @Expose
    private String searchVal;
    @SerializedName("hotel_id")
    @Expose
    private String hotelId;
    @SerializedName("revrat_id")
    @Expose
    private String revratId;
    @SerializedName("hotel_review")
    @Expose
    private String hotelReview;
    @SerializedName("total_likes")
    @Expose
    private String totalLikes;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("category_type")
    @Expose
    private String categoryType;
    @SerializedName("veg_nonveg")
    @Expose
    private String vegNonveg;
    @SerializedName("food_exprience")
    @Expose
    private String foodExprience;
    @SerializedName("ambiance")
    @Expose
    private String ambiance;
    @SerializedName("taste")
    @Expose
    private String taste;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("package")
    @Expose
    private String _package;
    @SerializedName("timedelivery")
    @Expose
    private String timedelivery;
    @SerializedName("delivery_mode")
    @Expose
    private String deliveryMode;
    @SerializedName("value_money")
    @Expose
    private String valueMoney;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("google_id")
    @Expose
    private String googleId;
    @SerializedName("hotel_name")
    @Expose
    private String hotelName;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("open_times")
    @Expose
    private String openTimes;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("photo_reference")
    @Expose
    private String photoReference;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("total_followers")
    @Expose
    private String totalFollowers;
    @SerializedName("total_followings")
    @Expose
    private String totalFollowings;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("likes_htl_id")
    @Expose
    private String likesHtlId;
    @SerializedName("htl_rev_likes")
    @Expose
    private String htlRevLikes;
    @SerializedName("following_id")
    @Expose
    private String followingId;
    @SerializedName("mode_id")
    @Expose
    private String modeId;
    @SerializedName("comp_name")
    @Expose
    private String compName;
    @SerializedName("comp_img")
    @Expose
    private String compImg;
    @SerializedName("total_review")
    @Expose
    private String totalReview;
    @SerializedName("total_review_users")
    @Expose
    private String totalReviewUsers;
    @SerializedName("total_food_exprience")
    @Expose
    private String totalFoodExprience;
    @SerializedName("total_ambiance")
    @Expose
    private String totalAmbiance;
    @SerializedName("total_taste")
    @Expose
    private String totalTaste;
    @SerializedName("total_service")
    @Expose
    private String totalService;
    @SerializedName("total_package")
    @Expose
    private String totalPackage;
    @SerializedName("total_timedelivery")
    @Expose
    private String totalTimedelivery;
    @SerializedName("total_value_money")
    @Expose
    private String totalValueMoney;
    @SerializedName("total_good")
    @Expose
    private String totalGood;
    @SerializedName("total_bad")
    @Expose
    private String totalBad;
    @SerializedName("total_good_bad_user")
    @Expose
    private String totalGoodBadUser;
    @SerializedName("topdishimage")
    @Expose
    private List<ReviewTopDishPojo> topdishimage = null;
    @SerializedName("topdish")
    @Expose
    private String topdish;
    @SerializedName("top_count")
    @Expose
    private Integer topCount;
    @SerializedName("avoiddishimage")
    @Expose
    private List<ReviewAvoidDishPojo> avoiddishimage = null;
    @SerializedName("avoiddish")
    @Expose
    private String avoiddish;
    @SerializedName("avoid_count")
    @Expose
    private Integer avoidCount;
    @SerializedName("ambi_image")
    @Expose
    private List<ReviewAmbiImagePojo> ambiImage = null;
    @SerializedName("ambi_image_count")
    @Expose
    private Integer ambiImageCount;

    @SerializedName("bucket_list")
    @Expose
    private Integer bucket_list;
    @SerializedName("userliked")
    @Expose
    private boolean userliked;


    public Integer getBucket_list() {
        return bucket_list;
    }

    public void setBucket_list(Integer bucket_list) {
        this.bucket_list = bucket_list;
    }

    public String getSearchVal() {
        return searchVal;
    }

    public void setSearchVal(String searchVal) {
        this.searchVal = searchVal;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getRevratId() {
        return revratId;
    }

    public void setRevratId(String revratId) {
        this.revratId = revratId;
    }

    public String getHotelReview() {
        return hotelReview;
    }

    public void setHotelReview(String hotelReview) {
        this.hotelReview = hotelReview;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getVegNonveg() {
        return vegNonveg;
    }

    public void setVegNonveg(String vegNonveg) {
        this.vegNonveg = vegNonveg;
    }

    public String getFoodExprience() {
        return foodExprience;
    }

    public void setFoodExprience(String foodExprience) {
        this.foodExprience = foodExprience;
    }

    public String getAmbiance() {
        return ambiance;
    }

    public void setAmbiance(String ambiance) {
        this.ambiance = ambiance;
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String get_package() {
        return _package;
    }

    public void set_package(String _package) {
        this._package = _package;
    }

    public String getTimedelivery() {
        return timedelivery;
    }

    public void setTimedelivery(String timedelivery) {
        this.timedelivery = timedelivery;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String getValueMoney() {
        return valueMoney;
    }

    public void setValueMoney(String valueMoney) {
        this.valueMoney = valueMoney;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(String openTimes) {
        this.openTimes = openTimes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getTotalFollowers() {
        return totalFollowers;
    }

    public void setTotalFollowers(String totalFollowers) {
        this.totalFollowers = totalFollowers;
    }

    public String getTotalFollowings() {
        return totalFollowings;
    }

    public void setTotalFollowings(String totalFollowings) {
        this.totalFollowings = totalFollowings;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLikesHtlId() {
        return likesHtlId;
    }

    public void setLikesHtlId(String likesHtlId) {
        this.likesHtlId = likesHtlId;
    }

    public String getHtlRevLikes() {
        return htlRevLikes;
    }

    public void setHtlRevLikes(String htlRevLikes) {
        this.htlRevLikes = htlRevLikes;
    }

    public String getFollowingId() {
        return followingId;
    }

    public void setFollowingId(String followingId) {
        this.followingId = followingId;
    }

    public String getModeId() {
        return modeId;
    }

    public void setModeId(String modeId) {
        this.modeId = modeId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getCompImg() {
        return compImg;
    }

    public void setCompImg(String compImg) {
        this.compImg = compImg;
    }

    public String getTotalReview() {
        return totalReview;
    }

    public void setTotalReview(String totalReview) {
        this.totalReview = totalReview;
    }

    public String getTotalReviewUsers() {
        return totalReviewUsers;
    }

    public void setTotalReviewUsers(String totalReviewUsers) {
        this.totalReviewUsers = totalReviewUsers;
    }

    public String getTotalFoodExprience() {
        return totalFoodExprience;
    }

    public void setTotalFoodExprience(String totalFoodExprience) {
        this.totalFoodExprience = totalFoodExprience;
    }

    public String getTotalAmbiance() {
        return totalAmbiance;
    }

    public void setTotalAmbiance(String totalAmbiance) {
        this.totalAmbiance = totalAmbiance;
    }

    public String getTotalTaste() {
        return totalTaste;
    }

    public void setTotalTaste(String totalTaste) {
        this.totalTaste = totalTaste;
    }

    public String getTotalService() {
        return totalService;
    }

    public void setTotalService(String totalService) {
        this.totalService = totalService;
    }

    public String getTotalPackage() {
        return totalPackage;
    }

    public void setTotalPackage(String totalPackage) {
        this.totalPackage = totalPackage;
    }

    public String getTotalTimedelivery() {
        return totalTimedelivery;
    }

    public void setTotalTimedelivery(String totalTimedelivery) {
        this.totalTimedelivery = totalTimedelivery;
    }

    public String getTotalValueMoney() {
        return totalValueMoney;
    }

    public void setTotalValueMoney(String totalValueMoney) {
        this.totalValueMoney = totalValueMoney;
    }

    public String getTotalGood() {
        return totalGood;
    }

    public void setTotalGood(String totalGood) {
        this.totalGood = totalGood;
    }

    public String getTotalBad() {
        return totalBad;
    }

    public void setTotalBad(String totalBad) {
        this.totalBad = totalBad;
    }

    public String getTotalGoodBadUser() {
        return totalGoodBadUser;
    }

    public void setTotalGoodBadUser(String totalGoodBadUser) {
        this.totalGoodBadUser = totalGoodBadUser;
    }

    public List<ReviewTopDishPojo> getTopdishimage() {
        return topdishimage;
    }

    public void setTopdishimage(List<ReviewTopDishPojo> topdishimage) {
        this.topdishimage = topdishimage;
    }

    public String getTopdish() {
        return topdish;
    }

    public void setTopdish(String topdish) {
        this.topdish = topdish;
    }

    public Integer getTopCount() {
        return topCount;
    }

    public void setTopCount(Integer topCount) {
        this.topCount = topCount;
    }

    public List<ReviewAvoidDishPojo> getAvoiddishimage() {
        return avoiddishimage;
    }

    public void setAvoiddishimage(List<ReviewAvoidDishPojo> avoiddishimage) {
        this.avoiddishimage = avoiddishimage;
    }

    public String getAvoiddish() {
        return avoiddish;
    }

    public void setAvoiddish(String avoiddish) {
        this.avoiddish = avoiddish;
    }

    public Integer getAvoidCount() {
        return avoidCount;
    }

    public void setAvoidCount(Integer avoidCount) {
        this.avoidCount = avoidCount;
    }

    public List<ReviewAmbiImagePojo> getAmbiImage() {
        return ambiImage;
    }

    public void setAmbiImage(List<ReviewAmbiImagePojo> ambiImage) {
        this.ambiImage = ambiImage;
    }

    public Integer getAmbiImageCount() {
        return ambiImageCount;
    }

    public void setAmbiImageCount(Integer ambiImageCount) {
        this.ambiImageCount = ambiImageCount;
    }

    public boolean isUserliked() {
        return userliked;
    }

    public void setUserliked(boolean userliked) {
        this.userliked = userliked;
    }
}
