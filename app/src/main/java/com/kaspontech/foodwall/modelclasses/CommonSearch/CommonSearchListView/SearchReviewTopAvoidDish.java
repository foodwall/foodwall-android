package com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchReviewTopAvoidDish {

    @SerializedName("dishname")
    @Expose
    private String dishname;

    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

}
