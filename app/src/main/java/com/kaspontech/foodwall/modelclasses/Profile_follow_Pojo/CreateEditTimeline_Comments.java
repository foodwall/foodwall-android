package com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateEditTimeline_Comments {

    @SerializedName("methodName")
    @Expose
    private String methodName;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Data")
    @Expose
    private List<CreateEditTimeline_Datum> data = null;
    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("ResponseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("custom")
    @Expose
    private String custom;
    @SerializedName("noofrow")
    @Expose
    private Integer noofrow;

    /**
     * No args constructor for use in serialization
     *
     */
    public CreateEditTimeline_Comments() {
    }

    /**
     *
     * @param responseMessage
     * @param responseCode
     * @param status
     * @param data
     * @param noofrow
     * @param methodName
     * @param custom
     */
    public CreateEditTimeline_Comments(String methodName, Integer status, List<CreateEditTimeline_Datum> data, Integer responseCode, String responseMessage, String custom, Integer noofrow) {
        super();
        this.methodName = methodName;
        this.status = status;
        this.data = data;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.custom = custom;
        this.noofrow = noofrow;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<CreateEditTimeline_Datum> getData() {
        return data;
    }

    public void setData(List<CreateEditTimeline_Datum> data) {
        this.data = data;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public Integer getNoofrow() {
        return noofrow;
    }

    public void setNoofrow(Integer noofrow) {
        this.noofrow = noofrow;
    }

}
