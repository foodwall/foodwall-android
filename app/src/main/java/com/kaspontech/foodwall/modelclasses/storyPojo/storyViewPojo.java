package com.kaspontech.foodwall.modelclasses.storyPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class storyViewPojo {

    @SerializedName("methodName")
    @Expose
    private String methodName;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Data")
    @Expose
    private ArrayList<storyViewInputPojo> data = null;
    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("ResponseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("custom")
    @Expose
    private String custom;
    @SerializedName("noofrow")
    @Expose
    private Integer noofrow;

    /**
     * No args constructor for use in serialization
     *
     */
    public storyViewPojo() {
    }

    /**
     *
     * @param responseMessage
     * @param responseCode
     * @param status
     * @param data
     * @param noofrow
     * @param methodName
     * @param custom
     */
    public storyViewPojo(String methodName, Integer status, ArrayList<storyViewInputPojo> data, Integer responseCode, String responseMessage, String custom, Integer noofrow) {
        super();
        this.methodName = methodName;
        this.status = status;
        this.data = data;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.custom = custom;
        this.noofrow = noofrow;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<storyViewInputPojo> getData() {
        return data;
    }

    public void setData(ArrayList<storyViewInputPojo> data) {
        this.data = data;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public Integer getNoofrow() {
        return noofrow;
    }

    public void setNoofrow(Integer noofrow) {
        this.noofrow = noofrow;
    }
}
