package com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class View_port_pojo {
    @SerializedName("northeast")
    @Expose
    private North_east northeast;
    @SerializedName("southwest")
    @Expose
    private Southwest southwest;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     * @param southwest
     * @param northeast
     */
    public View_port_pojo(North_east northeast, Southwest southwest) {
        super();
        this.northeast = northeast;
        this.southwest = southwest;
    }

    public North_east getNortheast() {
        return northeast;
    }

    public void setNortheast(North_east northeast) {
        this.northeast = northeast;
    }

    public Southwest getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Southwest southwest) {
        this.southwest = southwest;
    }
}
