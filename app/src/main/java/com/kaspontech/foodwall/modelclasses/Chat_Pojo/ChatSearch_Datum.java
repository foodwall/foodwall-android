package com.kaspontech.foodwall.modelclasses.Chat_Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatSearch_Datum  {

    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("hist_id")
    @Expose
    private String histId;
    @SerializedName("sessionid")
    @Expose
    private String sessionid;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("type_yourid")
    @Expose
    private String typeYourid;
    @SerializedName("type_status")
    @Expose
    private String typeStatus;
    @SerializedName("type_message")
    @Expose
    private String typeMessage;
    @SerializedName("your_firstname")
    @Expose
    private Object yourFirstname;
    @SerializedName("your_lastname")
    @Expose
    private Object yourLastname;
    @SerializedName("lastmessage")
    @Expose
    private String lastmessage;
    @SerializedName("lastseen")
    @Expose
    private String lastseen;
    @SerializedName("from_firstname")
    @Expose
    private String fromFirstname;
    @SerializedName("from_lastname")
    @Expose
    private String fromLastname;
    @SerializedName("from_picture")
    @Expose
    private String fromPicture;
    @SerializedName("friendid")
    @Expose
    private String friendid;
    @SerializedName("to_firstname")
    @Expose
    private String toFirstname;
    @SerializedName("to_lastname")
    @Expose
    private String toLastname;
    @SerializedName("to_picture")
    @Expose
    private String toPicture;
    @SerializedName("to_online_status")
    @Expose
    private String toOnlineStatus;
    @SerializedName("to_lastvisited")
    @Expose
    private String toLastvisited;
    @SerializedName("group_name")
    @Expose
    private Object groupName;
    @SerializedName("group_icon")
    @Expose
    private Object groupIcon;
    @SerializedName("changed_which")
    @Expose
    private Object changedWhich;
    @SerializedName("group_createdby")
    @Expose
    private Object groupCreatedby;
    @SerializedName("created_date")
    @Expose
    private Object createdDate;
    @SerializedName("group_created_firstname")
    @Expose
    private Object groupCreatedFirstname;
    @SerializedName("group_created_lastname")
    @Expose
    private Object groupCreatedLastname;
    @SerializedName("exist_group_userid")
    @Expose
    private Object existGroupUserid;
    @SerializedName("group_exits")
    @Expose
    private Object groupExits;
    @SerializedName("group_exits_time")
    @Expose
    private Object groupExitsTime;

    /**
     * No args constructor for use in serialization
     *
     */
    public ChatSearch_Datum() {
    }


    public ChatSearch_Datum(String userid, String histId, String sessionid, String groupid, String createdOn, String typeYourid, String typeStatus, String typeMessage, Object yourFirstname, Object yourLastname, String lastmessage, String lastseen, String fromFirstname, String fromLastname, String fromPicture, String friendid, String toFirstname, String toLastname, String toPicture, String toOnlineStatus, String toLastvisited, Object groupName, Object groupIcon, Object changedWhich, Object groupCreatedby, Object createdDate, Object groupCreatedFirstname, Object groupCreatedLastname, Object existGroupUserid, Object groupExits, Object groupExitsTime) {
        super();
        this.userid = userid;
        this.histId = histId;
        this.sessionid = sessionid;
        this.groupid = groupid;
        this.createdOn = createdOn;
        this.typeYourid = typeYourid;
        this.typeStatus = typeStatus;
        this.typeMessage = typeMessage;
        this.yourFirstname = yourFirstname;
        this.yourLastname = yourLastname;
        this.lastmessage = lastmessage;
        this.lastseen = lastseen;
        this.fromFirstname = fromFirstname;
        this.fromLastname = fromLastname;
        this.fromPicture = fromPicture;
        this.friendid = friendid;
        this.toFirstname = toFirstname;
        this.toLastname = toLastname;
        this.toPicture = toPicture;
        this.toOnlineStatus = toOnlineStatus;
        this.toLastvisited = toLastvisited;
        this.groupName = groupName;
        this.groupIcon = groupIcon;
        this.changedWhich = changedWhich;
        this.groupCreatedby = groupCreatedby;
        this.createdDate = createdDate;
        this.groupCreatedFirstname = groupCreatedFirstname;
        this.groupCreatedLastname = groupCreatedLastname;
        this.existGroupUserid = existGroupUserid;
        this.groupExits = groupExits;
        this.groupExitsTime = groupExitsTime;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getHistId() {
        return histId;
    }

    public void setHistId(String histId) {
        this.histId = histId;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTypeYourid() {
        return typeYourid;
    }

    public void setTypeYourid(String typeYourid) {
        this.typeYourid = typeYourid;
    }

    public String getTypeStatus() {
        return typeStatus;
    }

    public void setTypeStatus(String typeStatus) {
        this.typeStatus = typeStatus;
    }

    public String getTypeMessage() {
        return typeMessage;
    }

    public void setTypeMessage(String typeMessage) {
        this.typeMessage = typeMessage;
    }

    public Object getYourFirstname() {
        return yourFirstname;
    }

    public void setYourFirstname(Object yourFirstname) {
        this.yourFirstname = yourFirstname;
    }

    public Object getYourLastname() {
        return yourLastname;
    }

    public void setYourLastname(Object yourLastname) {
        this.yourLastname = yourLastname;
    }

    public String getLastmessage() {
        return lastmessage;
    }

    public void setLastmessage(String lastmessage) {
        this.lastmessage = lastmessage;
    }

    public String getLastseen() {
        return lastseen;
    }

    public void setLastseen(String lastseen) {
        this.lastseen = lastseen;
    }

    public String getFromFirstname() {
        return fromFirstname;
    }

    public void setFromFirstname(String fromFirstname) {
        this.fromFirstname = fromFirstname;
    }

    public String getFromLastname() {
        return fromLastname;
    }

    public void setFromLastname(String fromLastname) {
        this.fromLastname = fromLastname;
    }

    public String getFromPicture() {
        return fromPicture;
    }

    public void setFromPicture(String fromPicture) {
        this.fromPicture = fromPicture;
    }

    public String getFriendid() {
        return friendid;
    }

    public void setFriendid(String friendid) {
        this.friendid = friendid;
    }

    public String getToFirstname() {
        return toFirstname;
    }

    public void setToFirstname(String toFirstname) {
        this.toFirstname = toFirstname;
    }

    public String getToLastname() {
        return toLastname;
    }

    public void setToLastname(String toLastname) {
        this.toLastname = toLastname;
    }

    public String getToPicture() {
        return toPicture;
    }

    public void setToPicture(String toPicture) {
        this.toPicture = toPicture;
    }

    public String getToOnlineStatus() {
        return toOnlineStatus;
    }

    public void setToOnlineStatus(String toOnlineStatus) {
        this.toOnlineStatus = toOnlineStatus;
    }

    public String getToLastvisited() {
        return toLastvisited;
    }

    public void setToLastvisited(String toLastvisited) {
        this.toLastvisited = toLastvisited;
    }

    public Object getGroupName() {
        return groupName;
    }

    public void setGroupName(Object groupName) {
        this.groupName = groupName;
    }

    public Object getGroupIcon() {
        return groupIcon;
    }

    public void setGroupIcon(Object groupIcon) {
        this.groupIcon = groupIcon;
    }

    public Object getChangedWhich() {
        return changedWhich;
    }

    public void setChangedWhich(Object changedWhich) {
        this.changedWhich = changedWhich;
    }

    public Object getGroupCreatedby() {
        return groupCreatedby;
    }

    public void setGroupCreatedby(Object groupCreatedby) {
        this.groupCreatedby = groupCreatedby;
    }

    public Object getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Object createdDate) {
        this.createdDate = createdDate;
    }

    public Object getGroupCreatedFirstname() {
        return groupCreatedFirstname;
    }

    public void setGroupCreatedFirstname(Object groupCreatedFirstname) {
        this.groupCreatedFirstname = groupCreatedFirstname;
    }

    public Object getGroupCreatedLastname() {
        return groupCreatedLastname;
    }

    public void setGroupCreatedLastname(Object groupCreatedLastname) {
        this.groupCreatedLastname = groupCreatedLastname;
    }

    public Object getExistGroupUserid() {
        return existGroupUserid;
    }

    public void setExistGroupUserid(Object existGroupUserid) {
        this.existGroupUserid = existGroupUserid;
    }

    public Object getGroupExits() {
        return groupExits;
    }

    public void setGroupExits(Object groupExits) {
        this.groupExits = groupExits;
    }

    public Object getGroupExitsTime() {
        return groupExitsTime;
    }

    public void setGroupExitsTime(Object groupExitsTime) {
        this.groupExitsTime = groupExitsTime;
    }

}