package com.kaspontech.foodwall.modelclasses.EventsPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetEventsDisCmtsAllReplyData {

    @SerializedName("reply_id")
    @Expose
    private String replyId;
    @SerializedName("dis_cmmt_reply")
    @Expose
    private String disCmmtReply;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("cmmt_dis_id")
    @Expose
    private String cmmtDisId;
    @SerializedName("dis_comments")
    @Expose
    private String disComments;
    @SerializedName("total_cmmt_likes")
    @Expose
    private String totalCmmtLikes;
    @SerializedName("total_cmmt_reply")
    @Expose
    private String totalCmmtReply;
    @SerializedName("dis_cmmt_created")
    @Expose
    private String disCmmtCreated;
    @SerializedName("dis_cmmt_creator_firstname")
    @Expose
    private String disCmmtCreatorFirstname;
    @SerializedName("dis_cmmt_creator_lastname")
    @Expose
    private String disCmmtCreatorLastname;
    @SerializedName("dis_cmmt_creator_picture")
    @Expose
    private String disCmmtCreatorPicture;


    public GetEventsDisCmtsAllReplyData(String replyId, String disCmmtReply, String createdBy, String createdOn, String userId, String firstName, String lastName, String email, String contNo, String gender, String dob, String picture, String cmmtDisId, String disComments, String totalCmmtLikes, String totalCmmtReply, String disCmmtCreated, String disCmmtCreatorFirstname, String disCmmtCreatorLastname, String disCmmtCreatorPicture) {
        this.replyId = replyId;
        this.disCmmtReply = disCmmtReply;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
        this.cmmtDisId = cmmtDisId;
        this.disComments = disComments;
        this.totalCmmtLikes = totalCmmtLikes;
        this.totalCmmtReply = totalCmmtReply;
        this.disCmmtCreated = disCmmtCreated;
        this.disCmmtCreatorFirstname = disCmmtCreatorFirstname;
        this.disCmmtCreatorLastname = disCmmtCreatorLastname;
        this.disCmmtCreatorPicture = disCmmtCreatorPicture;
    }

    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public String getDisCmmtReply() {
        return disCmmtReply;
    }

    public void setDisCmmtReply(String disCmmtReply) {
        this.disCmmtReply = disCmmtReply;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getCmmtDisId() {
        return cmmtDisId;
    }

    public void setCmmtDisId(String cmmtDisId) {
        this.cmmtDisId = cmmtDisId;
    }

    public String getDisComments() {
        return disComments;
    }

    public void setDisComments(String disComments) {
        this.disComments = disComments;
    }

    public String getTotalCmmtLikes() {
        return totalCmmtLikes;
    }

    public void setTotalCmmtLikes(String totalCmmtLikes) {
        this.totalCmmtLikes = totalCmmtLikes;
    }

    public String getTotalCmmtReply() {
        return totalCmmtReply;
    }

    public void setTotalCmmtReply(String totalCmmtReply) {
        this.totalCmmtReply = totalCmmtReply;
    }

    public String getDisCmmtCreated() {
        return disCmmtCreated;
    }

    public void setDisCmmtCreated(String disCmmtCreated) {
        this.disCmmtCreated = disCmmtCreated;
    }

    public String getDisCmmtCreatorFirstname() {
        return disCmmtCreatorFirstname;
    }

    public void setDisCmmtCreatorFirstname(String disCmmtCreatorFirstname) {
        this.disCmmtCreatorFirstname = disCmmtCreatorFirstname;
    }

    public String getDisCmmtCreatorLastname() {
        return disCmmtCreatorLastname;
    }

    public void setDisCmmtCreatorLastname(String disCmmtCreatorLastname) {
        this.disCmmtCreatorLastname = disCmmtCreatorLastname;
    }

    public String getDisCmmtCreatorPicture() {
        return disCmmtCreatorPicture;
    }

    public void setDisCmmtCreatorPicture(String disCmmtCreatorPicture) {
        this.disCmmtCreatorPicture = disCmmtCreatorPicture;
    }


}
