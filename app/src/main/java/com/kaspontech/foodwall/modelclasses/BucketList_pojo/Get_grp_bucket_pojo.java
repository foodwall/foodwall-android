package com.kaspontech.foodwall.modelclasses.BucketList_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_grp_bucket_pojo {

    @SerializedName("methodName")
    @Expose
    private String methodName;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Data")
    @Expose
    private List<Get_grp_input_pojo> data = null;
    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("ResponseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("custom")
    @Expose
    private String custom;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param responseMessage
     * @param responseCode
     * @param status
     * @param data
     * @param methodName
     * @param custom
     */
    public Get_grp_bucket_pojo(String methodName, Integer status, List<Get_grp_input_pojo> data, Integer responseCode, String responseMessage, String custom) {
        super();
        this.methodName = methodName;
        this.status = status;
        this.data = data;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.custom = custom;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Get_grp_input_pojo> getData() {
        return data;
    }

    public void setData(List<Get_grp_input_pojo> data) {
        this.data = data;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }
}
