package com.kaspontech.foodwall.modelclasses.EventShare;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetEventsCommentsAll_Datum {

    @SerializedName("event_id")
    @Expose
    private String eventId;
    @SerializedName("redirect_url")
    @Expose
    private String redirectUrl;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_description")
    @Expose
    private String eventDescription;
    @SerializedName("total_likes")
    @Expose
    private String totalLikes;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("total_going")
    @Expose
    private String totalGoing;
    @SerializedName("event_image")
    @Expose
    private String eventImage;
    @SerializedName("event_type")
    @Expose
    private String eventType;
    @SerializedName("share_type")
    @Expose
    private String shareType;
    @SerializedName("ticket_url")
    @Expose
    private String ticketUrl;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("total_discussion")
    @Expose
    private String totalDiscussion;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("likes_evt_id")
    @Expose
    private String likesEvtId;
    @SerializedName("evt_likes")
    @Expose
    private String evtLikes;
    @SerializedName("going_evt_id")
    @Expose
    private String goingEvtId;
    @SerializedName("gng_likes")
    @Expose
    private String gngLikes;
    @SerializedName("following_id")
    @Expose
    private String followingId;
    @SerializedName("evt_last_cmt")
    @Expose
    private List<GetEventsCommentsAll_Comments> evtLastCmt = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetEventsCommentsAll_Datum() {
    }

    /**
     *
     * @param startDate
     * @param oauthProvider
     * @param totalComments
     * @param gngLikes
     * @param location
     * @param ticketUrl
     * @param endDate
     * @param eventType
     * @param eventId
     * @param likesEvtId
     * @param userId
     * @param gender
     * @param contNo
     * @param longitude
     * @param firstName
     * @param redirectUrl
     * @param evtLastCmt
     * @param lastName
     * @param totalDiscussion
     * @param followingId
     * @param imei
     * @param deleted
     * @param oauthUid
     * @param goingEvtId
     * @param evtLikes
     * @param picture
     * @param createdOn
     * @param totalLikes
     * @param eventDescription
     * @param createdBy
     * @param email
     * @param dob
     * @param totalGoing
     * @param active
     * @param eventImage
     * @param eventName
     * @param latitude
     * @param shareType
     */
    public GetEventsCommentsAll_Datum(String eventId, String redirectUrl, String eventName, String eventDescription,
                                      String totalLikes, String totalComments, String totalGoing,
                                      String eventImage, String eventType, String shareType,
                                      String ticketUrl, String location, String startDate, String endDate,
                                      String latitude, String longitude, String createdBy, String createdOn,
                                      String totalDiscussion, String active, String deleted, String userId,
                                      String oauthProvider, String oauthUid, String firstName, String lastName,
                                      String email, String imei, String contNo, String gender, String dob,
                                      String picture, String likesEvtId, String evtLikes, String goingEvtId,
                                      String gngLikes, String followingId, List<GetEventsCommentsAll_Comments> evtLastCmt) {
        super();
        this.eventId = eventId;
        this.redirectUrl = redirectUrl;
        this.eventName = eventName;
        this.eventDescription = eventDescription;
        this.totalLikes = totalLikes;
        this.totalComments = totalComments;
        this.totalGoing = totalGoing;
        this.eventImage = eventImage;
        this.eventType = eventType;
        this.shareType = shareType;
        this.ticketUrl = ticketUrl;
        this.location = location;
        this.startDate = startDate;
        this.endDate = endDate;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.totalDiscussion = totalDiscussion;
        this.active = active;
        this.deleted = deleted;
        this.userId = userId;
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imei = imei;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
        this.likesEvtId = likesEvtId;
        this.evtLikes = evtLikes;
        this.goingEvtId = goingEvtId;
        this.gngLikes = gngLikes;
        this.followingId = followingId;
        this.evtLastCmt = evtLastCmt;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getTotalGoing() {
        return totalGoing;
    }

    public void setTotalGoing(String totalGoing) {
        this.totalGoing = totalGoing;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getShareType() {
        return shareType;
    }

    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    public String getTicketUrl() {
        return ticketUrl;
    }

    public void setTicketUrl(String ticketUrl) {
        this.ticketUrl = ticketUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTotalDiscussion() {
        return totalDiscussion;
    }

    public void setTotalDiscussion(String totalDiscussion) {
        this.totalDiscussion = totalDiscussion;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLikesEvtId() {
        return likesEvtId;
    }

    public void setLikesEvtId(String likesEvtId) {
        this.likesEvtId = likesEvtId;
    }

    public String getEvtLikes() {
        return evtLikes;
    }

    public void setEvtLikes(String evtLikes) {
        this.evtLikes = evtLikes;
    }

    public String getGoingEvtId() {
        return goingEvtId;
    }

    public void setGoingEvtId(String goingEvtId) {
        this.goingEvtId = goingEvtId;
    }

    public String getGngLikes() {
        return gngLikes;
    }

    public void setGngLikes(String gngLikes) {
        this.gngLikes = gngLikes;
    }

    public String getFollowingId() {
        return followingId;
    }

    public void setFollowingId(String followingId) {
        this.followingId = followingId;
    }

    public List<GetEventsCommentsAll_Comments> getEvtLastCmt() {
        return evtLastCmt;
    }

    public void setEvtLastCmt(List<GetEventsCommentsAll_Comments> evtLastCmt) {
        this.evtLastCmt = evtLastCmt;
    }

}