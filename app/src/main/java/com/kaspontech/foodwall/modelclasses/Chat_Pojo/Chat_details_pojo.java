package com.kaspontech.foodwall.modelclasses.Chat_Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Chat_details_pojo {


    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("groupname")
    @Expose
    private String groupname;
    @SerializedName("groupicon")
    @Expose
    private String groupicon;
    @SerializedName("group_created_by")
    @Expose
    private String groupCreatedBy;
    @SerializedName("group_created_on")
    @Expose
    private String groupCreatedOn;
    @SerializedName("sessionid")
    @Expose
    private Integer sessionid;
    @SerializedName("userid")
    @Expose
    private String userid;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param userid
     * @param groupCreatedOn
     * @param groupname
     * @param groupid
     * @param sessionid
     * @param groupCreatedBy
     * @param groupicon
     */
    public Chat_details_pojo(String groupid, String groupname, String groupicon, String groupCreatedBy, String groupCreatedOn, Integer sessionid, String userid) {
        super();
        this.groupid = groupid;
        this.groupname = groupname;
        this.groupicon = groupicon;
        this.groupCreatedBy = groupCreatedBy;
        this.groupCreatedOn = groupCreatedOn;
        this.sessionid = sessionid;
        this.userid = userid;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getGroupicon() {
        return groupicon;
    }

    public void setGroupicon(String groupicon) {
        this.groupicon = groupicon;
    }

    public String getGroupCreatedBy() {
        return groupCreatedBy;
    }

    public void setGroupCreatedBy(String groupCreatedBy) {
        this.groupCreatedBy = groupCreatedBy;
    }

    public String getGroupCreatedOn() {
        return groupCreatedOn;
    }

    public void setGroupCreatedOn(String groupCreatedOn) {
        this.groupCreatedOn = groupCreatedOn;
    }

    public Integer getSessionid() {
        return sessionid;
    }

    public void setSessionid(Integer sessionid) {
        this.sessionid = sessionid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
