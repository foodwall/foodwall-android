package com.kaspontech.foodwall.modelclasses.Review_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lalith on 21-06-2018.
 */

public class Get_create_review_input_pojo {

    @SerializedName("hotelid")
    @Expose
    private String hotelid;
    @SerializedName("reviewid")
    @Expose
    private String reviewid;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param hotelid
     * @param reviewid
     */
    public Get_create_review_input_pojo(String hotelid, String reviewid) {
        super();
        this.hotelid = hotelid;
        this.reviewid = reviewid;
    }

    public String getHotelid() {
        return hotelid;
    }

    public void setHotelid(String hotelid) {
        this.hotelid = hotelid;
    }

    public String getReviewid() {
        return reviewid;
    }

    public void setReviewid(String reviewid) {
        this.reviewid = reviewid;
    }
}
