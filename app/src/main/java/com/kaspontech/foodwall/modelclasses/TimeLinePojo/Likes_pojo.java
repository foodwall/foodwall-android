package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vishnukm on 15/3/18.
 */

public class Likes_pojo {


    @SerializedName("cmmt_tl_id")
    @Expose
    private String cmmtTlId;
    @SerializedName("timeline_id")
    @Expose
    private String timelineId;
    @SerializedName("tl_comments")
    @Expose
    private String tlComments;
    @SerializedName("total_cmmt_likes")
    @Expose
    private String totalCmmtLikes;
    @SerializedName("cmmt_likes_id")
    @Expose
    private String cmmtLikesId;
    @SerializedName("tl_cmmt_likes")
    @Expose
    private String tlCmmtLikes;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;

    /**
     * No args constructor for use in serialization
     *
     */

    /**
     * @param lastName
     * @param tlCmmtLikes
     * @param cmmtTlId
     * @param picture
     * @param createdOn
     * @param createdBy
     * @param email
     * @param cmmtLikesId
     * @param dob
     * @param tlComments
     * @param userId
     * @param gender
     * @param contNo
     * @param timelineId
     * @param firstName
     * @param totalCmmtLikes
     */
    public Likes_pojo(String cmmtTlId, String timelineId, String tlComments, String totalCmmtLikes, String cmmtLikesId, String tlCmmtLikes, String createdBy, String createdOn, String userId, String firstName, String lastName, String email, String contNo, String gender, String dob, String picture) {
        super();
        this.cmmtTlId = cmmtTlId;
        this.timelineId = timelineId;
        this.tlComments = tlComments;
        this.totalCmmtLikes = totalCmmtLikes;
        this.cmmtLikesId = cmmtLikesId;
        this.tlCmmtLikes = tlCmmtLikes;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
    }

    public String getCmmtTlId() {
        return cmmtTlId;
    }

    public void setCmmtTlId(String cmmtTlId) {
        this.cmmtTlId = cmmtTlId;
    }

    public String getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(String timelineId) {
        this.timelineId = timelineId;
    }

    public String getTlComments() {
        return tlComments;
    }

    public void setTlComments(String tlComments) {
        this.tlComments = tlComments;
    }

    public String getTotalCmmtLikes() {
        return totalCmmtLikes;
    }

    public void setTotalCmmtLikes(String totalCmmtLikes) {
        this.totalCmmtLikes = totalCmmtLikes;
    }

    public String getCmmtLikesId() {
        return cmmtLikesId;
    }

    public void setCmmtLikesId(String cmmtLikesId) {
        this.cmmtLikesId = cmmtLikesId;
    }

    public String getTlCmmtLikes() {
        return tlCmmtLikes;
    }

    public void setTlCmmtLikes(String tlCmmtLikes) {
        this.tlCmmtLikes = tlCmmtLikes;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
