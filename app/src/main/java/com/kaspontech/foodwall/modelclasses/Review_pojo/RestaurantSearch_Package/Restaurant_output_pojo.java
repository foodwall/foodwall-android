package com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Restaurant_output_pojo {

    @SerializedName("html_attributions")
    @Expose
    private List<Object> htmlAttributions = null;
    @SerializedName("results")
    @Expose
    private List<com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Getall_restaurant_details_pojo> Getall_restaurant_details_pojo = null;
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("next_page_token")
    @Expose
    private String nextPageToken;

    /**
     * No args constructor for use in serialization
     *
     */

    /**
     * @param results
     * @param status
     * @param htmlAttributions
     */
    public Restaurant_output_pojo(List<Object> htmlAttributions, List<com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Getall_restaurant_details_pojo> results, String status, String nextPageToken) {
        super();
        this.htmlAttributions = htmlAttributions;
        this.Getall_restaurant_details_pojo = results;
        this.status = status;
        this.nextPageToken = nextPageToken;
    }

    public List<Object> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<Object> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }


    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public List<com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Getall_restaurant_details_pojo> getResults() {
        return Getall_restaurant_details_pojo;
    }

    public void setResults(List<com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package.Getall_restaurant_details_pojo> results) {
        this.Getall_restaurant_details_pojo = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
