package com.kaspontech.foodwall.modelclasses.EventsPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetEventDiscussionData {

    @SerializedName("dis_evt_id")
    @Expose
    private int disEvtId;
    @SerializedName("dis_description")
    @Expose
    private String disDescription;
    @SerializedName("dis_image")
    @Expose
    private String disImage;
    @SerializedName("total_likes")
    @Expose
    private int totalLikes;
    @SerializedName("total_comments")
    @Expose
    private int totalComments;
    @SerializedName("created_by")
    @Expose
    private int createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("event_id")
    @Expose
    private int eventId;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_description")
    @Expose
    private String eventDescription;
    @SerializedName("event_image")
    @Expose
    private String eventImage;
    @SerializedName("event_created")
    @Expose
    private String eventCreated;
    @SerializedName("event_creator_firstname")
    @Expose
    private String eventCreatorFirstname;
    @SerializedName("event_creator_lastname")
    @Expose
    private String eventCreatorLastname;
    @SerializedName("event_creator_picture")
    @Expose
    private String eventCreatorPicture;
    @SerializedName("dis_likes")
    @Expose
    private int disLikes;
    @SerializedName("liked")
    @Expose
    private boolean liked;

    public GetEventDiscussionData(int disEvtId, String disDescription, String disImage, int totalLikes, int totalComments, int createdBy, String createdOn, int userId, String firstName, String lastName,
                                  String picture, int eventId, String eventName, int disLikes) {
        this.disEvtId = disEvtId;
        this.disDescription = disDescription;
        this.disImage = disImage;
        this.totalLikes = totalLikes;
        this.totalComments = totalComments;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.eventId = eventId;
        this.eventName = eventName;
        this.disLikes = disLikes;
    }


    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public int getDisEvtId() {
        return disEvtId;
    }

    public void setDisEvtId(int disEvtId) {
        this.disEvtId = disEvtId;
    }

    public String getDisDescription() {
        return disDescription;
    }

    public void setDisDescription(String disDescription) {
        this.disDescription = disDescription;
    }

    public String getDisImage() {
        return disImage;
    }

    public void setDisImage(String disImage) {
        this.disImage = disImage;
    }

    public int getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(int totalLikes) {
        this.totalLikes = totalLikes;
    }

    public int getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(int totalComments) {
        this.totalComments = totalComments;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getEventCreated() {
        return eventCreated;
    }

    public void setEventCreated(String eventCreated) {
        this.eventCreated = eventCreated;
    }

    public String getEventCreatorFirstname() {
        return eventCreatorFirstname;
    }

    public void setEventCreatorFirstname(String eventCreatorFirstname) {
        this.eventCreatorFirstname = eventCreatorFirstname;
    }

    public String getEventCreatorLastname() {
        return eventCreatorLastname;
    }

    public void setEventCreatorLastname(String eventCreatorLastname) {
        this.eventCreatorLastname = eventCreatorLastname;
    }

    public String getEventCreatorPicture() {
        return eventCreatorPicture;
    }

    public void setEventCreatorPicture(String eventCreatorPicture) {
        this.eventCreatorPicture = eventCreatorPicture;
    }

    public int getDisLikes() {
        return disLikes;
    }

    public void setDisLikes(int disLikes) {
        this.disLikes = disLikes;
    }
}
