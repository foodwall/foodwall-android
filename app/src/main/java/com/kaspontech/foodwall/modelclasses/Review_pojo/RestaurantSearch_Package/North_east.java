package com.kaspontech.foodwall.modelclasses.Review_pojo.RestaurantSearch_Package;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class North_east {
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     * @param lng
     * @param lat
     */
    public North_east(Double lat, Double lng) {
        super();
        this.lat = lat;
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
