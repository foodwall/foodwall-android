package com.kaspontech.foodwall.modelclasses.TimeLinePojo;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetallCommentsPojo {
    @SerializedName("timeline_id")
    @Expose
    private String timelineId;
    @SerializedName("timeline_description")
    @Expose
    private String timelineDescription;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("cmmt_tl_id")
    @Expose
    private String cmmtTlId;
    @SerializedName("tl_comments")
    @Expose
    private String tlComments;
    @SerializedName("total_cmmt_likes")
    @Expose
    private String totalCmmtLikes;
    @SerializedName("total_cmmt_reply")
    @Expose
    private String totalCmmtReply;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("cmmt_likes_id")
    @Expose
    private String cmmtLikesId;
    @SerializedName("tl_cmmt_likes")
    @Expose
    private String tlCmmtLikes;
    @SerializedName("reply")
    @Expose
    private List<Reply_pojo> reply = null;
    @SerializedName("liked")
    @Expose
    private boolean liked;



    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param lastName
     * @param tlCmmtLikes
     * @param cmmtTlId
     * @param totalComments
     * @param picture
     * @param createdOn
     * @param totalCmmtReply
     * @param createdBy
     * @param email
     * @param cmmtLikesId
     * @param dob
     * @param tlComments
     * @param userId
     * @param reply
     * @param gender
     * @param contNo
     * @param timelineId
     * @param firstName
     * @param timelineDescription
     * @param totalCmmtLikes
     */
    public GetallCommentsPojo(String timelineId, String timelineDescription, String totalComments, String cmmtTlId, String tlComments, String totalCmmtLikes, String totalCmmtReply, String createdBy, String createdOn, String userId, String firstName, String lastName, String email, String contNo, String gender, String dob, String picture, String cmmtLikesId, String tlCmmtLikes, List<Reply_pojo> reply) {
        super();
        this.timelineId = timelineId;
        this.timelineDescription = timelineDescription;
        this.totalComments = totalComments;
        this.cmmtTlId = cmmtTlId;
        this.tlComments = tlComments;
        this.totalCmmtLikes = totalCmmtLikes;
        this.totalCmmtReply = totalCmmtReply;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
        this.cmmtLikesId = cmmtLikesId;
        this.tlCmmtLikes = tlCmmtLikes;
        this.reply = reply;
    }


    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public String getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(String timelineId) {
        this.timelineId = timelineId;
    }

    public String getTimelineDescription() {
        return timelineDescription;
    }

    public void setTimelineDescription(String timelineDescription) {
        this.timelineDescription = timelineDescription;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getCmmtTlId() {
        return cmmtTlId;
    }

    public void setCmmtTlId(String cmmtTlId) {
        this.cmmtTlId = cmmtTlId;
    }

    public String getTlComments() {
        return tlComments;
    }

    public void setTlComments(String tlComments) {
        this.tlComments = tlComments;
    }

    public String getTotalCmmtLikes() {
        return totalCmmtLikes;
    }

    public void setTotalCmmtLikes(String totalCmmtLikes) {
        this.totalCmmtLikes = totalCmmtLikes;
    }

    public String getTotalCmmtReply() {
        return totalCmmtReply;
    }

    public void setTotalCmmtReply(String totalCmmtReply) {
        this.totalCmmtReply = totalCmmtReply;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getCmmtLikesId() {
        return cmmtLikesId;
    }

    public void setCmmtLikesId(String cmmtLikesId) {
        this.cmmtLikesId = cmmtLikesId;
    }

    public String getTlCmmtLikes() {
        return tlCmmtLikes;
    }

    public void setTlCmmtLikes(String tlCmmtLikes) {
        this.tlCmmtLikes = tlCmmtLikes;
    }

    public List<Reply_pojo> getReply() {
        return reply;
    }

    public void setReply(List<Reply_pojo> reply) {
        this.reply = reply;
    }

}