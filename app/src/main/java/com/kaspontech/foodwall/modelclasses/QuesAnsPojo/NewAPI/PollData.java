package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PollData {

    @SerializedName("poll_id")
    @Expose
    private String pollId;
    @SerializedName("ques_id")
    @Expose
    private String quesId;
    @SerializedName("poll_list")
    @Expose
    private String pollList;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private Object contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("total_poll")
    @Expose
    private int totalPoll;
    @SerializedName("total_poll_user")
    @Expose
    private int totalPollUser;
    @SerializedName("ask_question")
    @Expose
    private String askQuestion;

    /**
     * No args constructor for use in serialization
     *
     */
    public PollData() {
    }

    /**
     *
     * @param askQuestion
     * @param oauthProvider
     * @param lastName
     * @param pollId
     * @param imei
     * @param oauthUid
     * @param picture
     * @param createdOn
     * @param quesId
     * @param totalPollUser
     * @param createdBy
     * @param email
     * @param dob
     * @param pollList
     * @param userId
     * @param totalPoll
     * @param gender
     * @param contNo
     * @param firstName
     */
    public PollData(String pollId, String quesId, String pollList, String createdBy, String createdOn, String userId, String oauthProvider, String oauthUid, String firstName, String lastName, String email, String imei, Object contNo, String gender,
                    String dob, String picture, int totalPoll, int totalPollUser, String askQuestion) {
        super();
        this.pollId = pollId;
        this.quesId = quesId;
        this.pollList = pollList;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.userId = userId;
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imei = imei;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
        this.totalPoll = totalPoll;
        this.totalPollUser = totalPollUser;
        this.askQuestion = askQuestion;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getQuesId() {
        return quesId;
    }

    public void setQuesId(String quesId) {
        this.quesId = quesId;
    }

    public String getPollList() {
        return pollList;
    }

    public void setPollList(String pollList) {
        this.pollList = pollList;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Object getContNo() {
        return contNo;
    }

    public void setContNo(Object contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getTotalPoll() {
        return totalPoll;
    }

    public void setTotalPoll(int totalPoll) {
        this.totalPoll = totalPoll;
    }

    public int getTotalPollUser() {
        return totalPollUser;
    }

    public void setTotalPollUser(int totalPollUser) {
        this.totalPollUser = totalPollUser;
    }

    public String getAskQuestion() {
        return askQuestion;
    }

    public void setAskQuestion(String askQuestion) {
        this.askQuestion = askQuestion;
    }

}