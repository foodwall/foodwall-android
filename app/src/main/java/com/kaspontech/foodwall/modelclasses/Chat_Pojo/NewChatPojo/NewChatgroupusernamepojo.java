package com.kaspontech.foodwall.modelclasses.Chat_Pojo.NewChatPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewChatgroupusernamepojo {
    @SerializedName("name")
    @Expose
    private String name;

    /**
     * No args constructor for use in serialization
     *
     */
    public NewChatgroupusernamepojo() {
    }

    /**
     *
     * @param name
     */
    public NewChatgroupusernamepojo(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
