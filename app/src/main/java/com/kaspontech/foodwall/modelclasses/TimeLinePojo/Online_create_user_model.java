package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vishnukm on 13/3/18.
 */

public class Online_create_user_model {

    @SerializedName("timeline_id")
    @Expose
    private String timelineId;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("image")
    @Expose
    private String image;


    @SerializedName("timeline_description")
    @Expose
    private String timelineDescription;



    public String getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(String timelineId) {
        this.timelineId = timelineId;
    }

    public String getTimelineDescription() {
        return timelineDescription;
    }

    public void setTimelineDescription(String timelineDescription) {
        this.timelineDescription = timelineDescription;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Online_create_user_model(String timelineId,String timelineDescription, String createdBy, String image) {
        this.timelineId = timelineId;
        this.timelineDescription = timelineDescription;
        this.createdBy = createdBy;
        this.image = image;

    }

}
