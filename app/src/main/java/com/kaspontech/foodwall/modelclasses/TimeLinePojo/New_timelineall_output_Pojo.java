package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

import java.util.List;

/**
 * Created by vishnukm on 21/3/18.
 */

public class New_timelineall_output_Pojo {

    private String methodName;
    private Integer status;
    private List<New_timeline_all_pojo> data = null;
    private Integer responseCode;
    private String responseMessage;
    private String custom;

    /**
     * No args constructor for use in serialization
     *
     */
    public New_timelineall_output_Pojo() {
    }

    /**
     *
     * @param responseMessage
     * @param responseCode
     * @param status
     * @param data
     * @param methodName
     * @param custom
     */
    public New_timelineall_output_Pojo(String methodName, Integer status, List<New_timeline_all_pojo> data, Integer responseCode, String responseMessage, String custom) {
        super();
        this.methodName = methodName;
        this.status = status;
        this.data = data;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.custom = custom;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<New_timeline_all_pojo> getData() {
        return data;
    }

    public void setData(List<New_timeline_all_pojo> data) {
        this.data = data;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }
}
