package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vishnukm on 16/3/18.
 */


public class GetallLikesPojo {

    @SerializedName("timeline_id")
    @Expose
    private String timelineId;
    @SerializedName("timeline_description")
    @Expose
    private String timelineDescription;
    @SerializedName("total_likes")
    @Expose
    private String totalLikes;
    @SerializedName("likes_tl_id")
    @Expose
    private String likesTlId;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private Object contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;

    public GetallLikesPojo(String timelineId, String timelineDescription, String totalLikes, String likesTlId, String createdBy, String createdOn, String userId, String firstName, String lastName, String email, Object contNo, String gender, String dob, String picture) {
        this.timelineId = timelineId;
        this.timelineDescription = timelineDescription;
        this.totalLikes = totalLikes;
        this.likesTlId = likesTlId;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
    }

    public String getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(String timelineId) {
        this.timelineId = timelineId;
    }

    public String getTimelineDescription() {
        return timelineDescription;
    }

    public void setTimelineDescription(String timelineDescription) {
        this.timelineDescription = timelineDescription;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getLikesTlId() {
        return likesTlId;
    }

    public void setLikesTlId(String likesTlId) {
        this.likesTlId = likesTlId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getContNo() {
        return contNo;
    }

    public void setContNo(Object contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}


