package com.kaspontech.foodwall.modelclasses.HistoricalMap.NearByRestaurant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NearByGeometry {

    @SerializedName("location")
    @Expose
    private NearByLocation location;
    @SerializedName("viewport")
    @Expose
    private NearByViewport viewport;

    public NearByLocation getLocation() {
        return location;
    }

    public void setLocation(NearByLocation location) {
        this.location = location;
    }

    public NearByViewport getViewport() {
        return viewport;
    }

    public void setViewport(NearByViewport viewport) {
        this.viewport = viewport;
    }
}
