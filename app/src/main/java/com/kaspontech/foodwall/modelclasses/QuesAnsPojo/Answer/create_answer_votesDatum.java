package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Answer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class create_answer_votesDatum {

    @SerializedName("ans_id")
    @Expose
    private String ansId;
    @SerializedName("ask_answer")
    @Expose
    private String askAnswer;
    @SerializedName("total_upvote")
    @Expose
    private String totalUpvote;
    @SerializedName("total_downvote")
    @Expose
    private String totalDownvote;
    @SerializedName("quest_id")
    @Expose
    private String questId;
    @SerializedName("vote_id")
    @Expose
    private String voteId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("up_down_vote")
    @Expose
    private String upDownVote;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private Object contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("isUpVoted")
    @Expose
    private boolean upVoted;

    @SerializedName("isDownVoted")
    @Expose
    private boolean downVoted;
    /**
     * No args constructor for use in serialization
     *
     */
    public create_answer_votesDatum() {
    }

    /**
     *
     * @param lastName
     * @param upDownVote
     * @param totalDownvote
     * @param picture
     * @param askAnswer
     * @param createdOn
     * @param ansId
     * @param email
     * @param dob
     * @param userId
     * @param gender
     * @param contNo
     * @param totalUpvote
     * @param firstName
     * @param questId
     * @param voteId
     */
    public create_answer_votesDatum(String ansId, String askAnswer, String totalUpvote, String totalDownvote, String questId, String voteId, String userId, String upDownVote, String createdOn, String firstName, String lastName, String email, Object contNo, String gender, String dob, String picture) {
        super();
        this.ansId = ansId;
        this.askAnswer = askAnswer;
        this.totalUpvote = totalUpvote;
        this.totalDownvote = totalDownvote;
        this.questId = questId;
        this.voteId = voteId;
        this.userId = userId;
        this.upDownVote = upDownVote;
        this.createdOn = createdOn;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
    }

    public boolean isUpVoted() {
        return upVoted;
    }

    public void setUpVoted(boolean upVoted) {
        this.upVoted = upVoted;
    }

    public boolean isDownVoted() {
        return downVoted;
    }

    public void setDownVoted(boolean downVoted) {
        this.downVoted = downVoted;
    }

    public String getAnsId() {
        return ansId;
    }

    public void setAnsId(String ansId) {
        this.ansId = ansId;
    }

    public String getAskAnswer() {
        return askAnswer;
    }

    public void setAskAnswer(String askAnswer) {
        this.askAnswer = askAnswer;
    }

    public String getTotalUpvote() {
        return totalUpvote;
    }

    public void setTotalUpvote(String totalUpvote) {
        this.totalUpvote = totalUpvote;
    }

    public String getTotalDownvote() {
        return totalDownvote;
    }

    public void setTotalDownvote(String totalDownvote) {
        this.totalDownvote = totalDownvote;
    }

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public String getVoteId() {
        return voteId;
    }

    public void setVoteId(String voteId) {
        this.voteId = voteId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUpDownVote() {
        return upDownVote;
    }

    public void setUpDownVote(String upDownVote) {
        this.upDownVote = upDownVote;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getContNo() {
        return contNo;
    }

    public void setContNo(Object contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}