package com.kaspontech.foodwall.modelclasses.Chat_Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Chat_history_single_details {
    @SerializedName("chatid")
    @Expose
    private String chatid;
    @SerializedName("sessionid")
    @Expose
    private String sessionid;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("fromuserid")
    @Expose
    private String fromuserid;
    @SerializedName("touserid")
    @Expose
    private String touserid;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("type_message")
    @Expose
    private String typeMessage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("msg_date")
    @Expose
    private String msgDate;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("lastseen")
    @Expose
    private String lastseen;
    @SerializedName("reviewid")
    @Expose
    private String reviewid;
    @SerializedName("timeline_id")
    @Expose
    private String timelineId;
    @SerializedName("from_firstname")
    @Expose
    private String fromFirstname;
    @SerializedName("from_lastname")
    @Expose
    private String fromLastname;
    @SerializedName("from_picture")
    @Expose
    private String fromPicture;
    @SerializedName("to_firstname")
    @Expose
    private String toFirstname;
    @SerializedName("to_lastname")
    @Expose
    private String toLastname;
    @SerializedName("to_picture")
    @Expose
    private String toPicture;
    @SerializedName("to_online_status")
    @Expose
    private String toOnlineStatus;
    @SerializedName("to_lastvisited")
    @Expose
    private String toLastvisited;
    @SerializedName("group_name")
    @Expose
    private String groupName;
    @SerializedName("group_icon")
    @Expose
    private String groupIcon;
    @SerializedName("changed_which")
    @Expose
    private String changedWhich;
    @SerializedName("group_createdby")
    @Expose
    private String groupCreatedby;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("group_created_firstname")
    @Expose
    private String groupCreatedFirstname;
    @SerializedName("group_created_lastname")
    @Expose
    private String groupCreatedLastname;
    @SerializedName("type_yourid")
    @Expose
    private String typeYourid;
    @SerializedName("type_status")
    @Expose
    private String typeStatus;
    @SerializedName("your_firstname")
    @Expose
    private String yourFirstname;
    @SerializedName("your_lastname")
    @Expose
    private String yourLastname;
    @SerializedName("exist_group_userid")
    @Expose
    private String existGroupUserid;
    @SerializedName("group_exits")
    @Expose
    private String groupExits;
    @SerializedName("group_exits_time")
    @Expose
    private String groupExitsTime;
    @SerializedName("deleted_message")
    @Expose private String deleted_message;

    /**
     * No args constructor for use in serialization
     *
     */


    public Chat_history_single_details(String chatid, String sessionid, String groupid, String fromuserid, String touserid, String message, String typeMessage, String status, String response, String msgDate, String createdOn, String lastseen, String reviewid, String timelineId, String fromFirstname, String fromLastname, String fromPicture, String toFirstname, String toLastname, String toPicture, String toOnlineStatus, String toLastvisited, String groupName, String groupIcon, String changedWhich, String groupCreatedby, String createdDate, String groupCreatedFirstname, String groupCreatedLastname, String typeYourid, String typeStatus, String yourFirstname, String yourLastname, String existGroupUserid, String groupExits, String groupExitsTime, String deleted_message) {
        this.chatid = chatid;
        this.sessionid = sessionid;
        this.groupid = groupid;
        this.fromuserid = fromuserid;
        this.touserid = touserid;
        this.message = message;
        this.typeMessage = typeMessage;
        this.status = status;
        this.response = response;
        this.msgDate = msgDate;
        this.createdOn = createdOn;
        this.lastseen = lastseen;
        this.reviewid = reviewid;
        this.timelineId = timelineId;
        this.fromFirstname = fromFirstname;
        this.fromLastname = fromLastname;
        this.fromPicture = fromPicture;
        this.toFirstname = toFirstname;
        this.toLastname = toLastname;
        this.toPicture = toPicture;
        this.toOnlineStatus = toOnlineStatus;
        this.toLastvisited = toLastvisited;
        this.groupName = groupName;
        this.groupIcon = groupIcon;
        this.changedWhich = changedWhich;
        this.groupCreatedby = groupCreatedby;
        this.createdDate = createdDate;
        this.groupCreatedFirstname = groupCreatedFirstname;
        this.groupCreatedLastname = groupCreatedLastname;
        this.typeYourid = typeYourid;
        this.typeStatus = typeStatus;
        this.yourFirstname = yourFirstname;
        this.yourLastname = yourLastname;
        this.existGroupUserid = existGroupUserid;
        this.groupExits = groupExits;
        this.groupExitsTime = groupExitsTime;
        this.deleted_message = deleted_message;
    }

    public String getDeleted_message() {
        return deleted_message;
    }

    public void setDeleted_message(String deleted_message) {
        this.deleted_message = deleted_message;
    }

    public String getChatid() {
        return chatid;
    }

    public void setChatid(String chatid) {
        this.chatid = chatid;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getFromuserid() {
        return fromuserid;
    }

    public void setFromuserid(String fromuserid) {
        this.fromuserid = fromuserid;
    }

    public String getTouserid() {
        return touserid;
    }

    public void setTouserid(String touserid) {
        this.touserid = touserid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTypeMessage() {
        return typeMessage;
    }

    public void setTypeMessage(String typeMessage) {
        this.typeMessage = typeMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(String msgDate) {
        this.msgDate = msgDate;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastseen() {
        return lastseen;
    }

    public void setLastseen(String lastseen) {
        this.lastseen = lastseen;
    }

    public String getReviewid() {
        return reviewid;
    }

    public void setReviewid(String reviewid) {
        this.reviewid = reviewid;
    }

    public String getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(String timelineId) {
        this.timelineId = timelineId;
    }

    public String getFromFirstname() {
        return fromFirstname;
    }

    public void setFromFirstname(String fromFirstname) {
        this.fromFirstname = fromFirstname;
    }

    public String getFromLastname() {
        return fromLastname;
    }

    public void setFromLastname(String fromLastname) {
        this.fromLastname = fromLastname;
    }

    public String getFromPicture() {
        return fromPicture;
    }

    public void setFromPicture(String fromPicture) {
        this.fromPicture = fromPicture;
    }

    public String getToFirstname() {
        return toFirstname;
    }

    public void setToFirstname(String toFirstname) {
        this.toFirstname = toFirstname;
    }

    public String getToLastname() {
        return toLastname;
    }

    public void setToLastname(String toLastname) {
        this.toLastname = toLastname;
    }

    public String getToPicture() {
        return toPicture;
    }

    public void setToPicture(String toPicture) {
        this.toPicture = toPicture;
    }

    public String getToOnlineStatus() {
        return toOnlineStatus;
    }

    public void setToOnlineStatus(String toOnlineStatus) {
        this.toOnlineStatus = toOnlineStatus;
    }

    public String getToLastvisited() {
        return toLastvisited;
    }

    public void setToLastvisited(String toLastvisited) {
        this.toLastvisited = toLastvisited;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupIcon() {
        return groupIcon;
    }

    public void setGroupIcon(String groupIcon) {
        this.groupIcon = groupIcon;
    }

    public String getChangedWhich() {
        return changedWhich;
    }

    public void setChangedWhich(String changedWhich) {
        this.changedWhich = changedWhich;
    }

    public String getGroupCreatedby() {
        return groupCreatedby;
    }

    public void setGroupCreatedby(String groupCreatedby) {
        this.groupCreatedby = groupCreatedby;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getGroupCreatedFirstname() {
        return groupCreatedFirstname;
    }

    public void setGroupCreatedFirstname(String groupCreatedFirstname) {
        this.groupCreatedFirstname = groupCreatedFirstname;
    }

    public String getGroupCreatedLastname() {
        return groupCreatedLastname;
    }

    public void setGroupCreatedLastname(String groupCreatedLastname) {
        this.groupCreatedLastname = groupCreatedLastname;
    }

    public String getTypeYourid() {
        return typeYourid;
    }

    public void setTypeYourid(String typeYourid) {
        this.typeYourid = typeYourid;
    }

    public String getTypeStatus() {
        return typeStatus;
    }

    public void setTypeStatus(String typeStatus) {
        this.typeStatus = typeStatus;
    }

    public String getYourFirstname() {
        return yourFirstname;
    }

    public void setYourFirstname(String yourFirstname) {
        this.yourFirstname = yourFirstname;
    }

    public String getYourLastname() {
        return yourLastname;
    }

    public void setYourLastname(String yourLastname) {
        this.yourLastname = yourLastname;
    }

    public String getExistGroupUserid() {
        return existGroupUserid;
    }

    public void setExistGroupUserid(String existGroupUserid) {
        this.existGroupUserid = existGroupUserid;
    }

    public String getGroupExits() {
        return groupExits;
    }

    public void setGroupExits(String groupExits) {
        this.groupExits = groupExits;
    }

    public String getGroupExitsTime() {
        return groupExitsTime;
    }

    public void setGroupExitsTime(String groupExitsTime) {
        this.groupExitsTime = groupExitsTime;
    }
}
