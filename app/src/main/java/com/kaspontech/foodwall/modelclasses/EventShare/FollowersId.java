package com.kaspontech.foodwall.modelclasses.EventShare;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by BalajiPrabhu on 4/22/2018.
 */

public class FollowersId {

    @SerializedName("id")
    @Expose
    private String id;

    public FollowersId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
