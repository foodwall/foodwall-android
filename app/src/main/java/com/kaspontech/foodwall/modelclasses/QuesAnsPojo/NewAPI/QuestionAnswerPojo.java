package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuestionAnswerPojo {

    @SerializedName("quest_id")
    @Expose
    private String questId;
    @SerializedName("ask_question")
    @Expose
    private String askQuestion;
    @SerializedName("ques_type")
    @Expose
    private String quesType;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("total_answers")
    @Expose
    private String totalAnswers;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("redirect_url")
    @Expose
    private String redirect_url;
    @SerializedName("total_followers")
    @Expose
    private int totalFollowers;
    @SerializedName("total_quest_follow")
    @Expose
    private int totalQuestFollow;
    @SerializedName("total_quest_request")
    @Expose
    private String totalQuestRequest;
    @SerializedName("poll_reg_id")
    @Expose
    private String pollRegId;
    @SerializedName("poll_id")
    @Expose
    private String pollId;
    @SerializedName("ques_follow")
    @Expose
    private int quesFollow;
    @SerializedName("quest_last_cmt")
    @Expose
    private List<QuestLastCmt> questLastCmt = null;
    @SerializedName("poll")
    @Expose
    private List<PollData> poll = null;
    @SerializedName("answer")
    @Expose
    private List<AnswerData> answer = null;
    @SerializedName("following")
    @Expose
    private boolean following;

    @SerializedName("isUpVoted")
    @Expose
    private boolean upVoted;

    @SerializedName("isDownVoted")
    @Expose
    private boolean downVoted;

    private int pollsize;
    /**
     * No args constructor for use in serialization
     *
     */
    public QuestionAnswerPojo() {
    }

    public QuestionAnswerPojo(int pollsize,String questId, String askQuestion, String quesType, String country, String city, String totalAnswers, String totalComments,
                              String latitude, String longitude, String createdBy, String createdOn, String active, String deleted, String userId,
                              String oauthProvider, String oauthUid, String firstName, String lastName, String email, String imei, String contNo,
                              String gender, String dob, String picture, int totalFollowers, int totalQuestFollow, String totalQuestRequest,
                              String pollRegId, String pollId,
                              int quesFollow, List<QuestLastCmt> questLastCmt, List<PollData> poll, List<AnswerData> answer) {
        super();
        this.pollsize = pollsize;
        this.questId = questId;
        this.askQuestion = askQuestion;
        this.quesType = quesType;
        this.country = country;
        this.city = city;
        this.totalAnswers = totalAnswers;
        this.totalComments = totalComments;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.active = active;
        this.deleted = deleted;
        this.userId = userId;
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imei = imei;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
        this.totalFollowers = totalFollowers;
        this.totalQuestFollow = totalQuestFollow;
        this.totalQuestRequest = totalQuestRequest;
        this.pollRegId = pollRegId;
        this.pollId = pollId;
        this.quesFollow = quesFollow;
        this.questLastCmt = questLastCmt;
        this.poll = poll;
        this.answer = answer;
    }

    public QuestionAnswerPojo(int pollsize,String questId, String askQuestion, String quesType, String country, String city, String totalAnswers, String totalComments,
                              String latitude, String longitude, String createdBy, String createdOn, String active, String deleted, String userId,
                              String oauthProvider, String oauthUid, String firstName, String lastName, String email, String imei, String contNo,
                              String gender, String dob, String picture, int totalFollowers, int totalQuestFollow, String totalQuestRequest,
                              String pollRegId, String pollId,
                              int quesFollow, List<QuestLastCmt> questLastCmt, List<PollData> poll, List<AnswerData> answer,String redirect_url) {
        super();
        this.pollsize = pollsize;
        this.questId = questId;
        this.askQuestion = askQuestion;
        this.quesType = quesType;
        this.country = country;
        this.city = city;
        this.totalAnswers = totalAnswers;
        this.totalComments = totalComments;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.active = active;
        this.deleted = deleted;
        this.userId = userId;
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imei = imei;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
        this.totalFollowers = totalFollowers;
        this.totalQuestFollow = totalQuestFollow;
        this.totalQuestRequest = totalQuestRequest;
        this.pollRegId = pollRegId;
        this.pollId = pollId;
        this.quesFollow = quesFollow;
        this.questLastCmt = questLastCmt;
        this.poll = poll;
        this.answer = answer;
        this.redirect_url = redirect_url;
    }

    public boolean isUpVoted() {
        return upVoted;
    }

    public void setUpVoted(boolean upVoted) {
        this.upVoted = upVoted;
    }

    public boolean isDownVoted() {
        return downVoted;
    }

    public void setDownVoted(boolean downVoted) {
        this.downVoted = downVoted;
    }

    public int getPollsize() {
        return pollsize;
    }

    public void setPollsize(int pollsize) {
        this.pollsize = pollsize;
    }

    public boolean isFollowing() {
        return following;
    }

    public void setFollowing(boolean following) {
        this.following = following;
    }

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public String getAskQuestion() {
        return askQuestion;
    }

    public void setAskQuestion(String askQuestion) {
        this.askQuestion = askQuestion;
    }

    public String getQuesType() {
        return quesType;
    }

    public void setQuesType(String quesType) {
        this.quesType = quesType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTotalAnswers() {
        return totalAnswers;
    }

    public void setTotalAnswers(String totalAnswers) {
        this.totalAnswers = totalAnswers;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getTotalFollowers() {
        return totalFollowers;
    }

    public void setTotalFollowers(int totalFollowers) {
        this.totalFollowers = totalFollowers;
    }

    public int getTotalQuestFollow() {
        return totalQuestFollow;
    }

    public void setTotalQuestFollow(int totalQuestFollow) {
        this.totalQuestFollow = totalQuestFollow;
    }

    public String getTotalQuestRequest() {
        return totalQuestRequest;
    }

    public void setTotalQuestRequest(String totalQuestRequest) {
        this.totalQuestRequest = totalQuestRequest;
    }

    public String getPollRegId() {
        return pollRegId;
    }

    public void setPollRegId(String pollRegId) {
        this.pollRegId = pollRegId;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public int getQuesFollow() {
        return quesFollow;
    }

    public void setQuesFollow(int quesFollow) {
        this.quesFollow = quesFollow;
    }

    public List<QuestLastCmt> getQuestLastCmt() {
        return questLastCmt;
    }

    public void setQuestLastCmt(List<QuestLastCmt> questLastCmt) {
        this.questLastCmt = questLastCmt;
    }

    public List<PollData> getPoll() {
        return poll;
    }

    public void setPoll(List<PollData> poll) {
        this.poll = poll;
    }

    public List<AnswerData> getAnswer() {
        return answer;
    }

    public void setAnswer(List<AnswerData> answer) {
        this.answer = answer;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public void setRedirect_url(String redirect_url) {
        this.redirect_url = redirect_url;
    }


}
