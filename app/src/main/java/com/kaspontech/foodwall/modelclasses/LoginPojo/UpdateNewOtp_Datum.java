package com.kaspontech.foodwall.modelclasses.LoginPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateNewOtp_Datum {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("verifycode")
    @Expose
    private String verifycode;

    /**
     * No args constructor for use in serialization
     *
     */
    public UpdateNewOtp_Datum() {
    }

    /**
     *
     * @param email
     * @param verifycode
     */
    public UpdateNewOtp_Datum(String email, String verifycode) {
        super();
        this.email = email;
        this.verifycode = verifycode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVerifycode() {
        return verifycode;
    }

    public void setVerifycode(String verifycode) {
        this.verifycode = verifycode;
    }

}