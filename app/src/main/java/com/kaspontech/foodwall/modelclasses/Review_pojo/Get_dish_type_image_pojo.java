package com.kaspontech.foodwall.modelclasses.Review_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_dish_type_image_pojo {
    @SerializedName("photo_id")
    @Expose
    private String photoId;
    @SerializedName("img")
    @Expose
    private String img;

    /**
     * No args constructor for use in serialization
     *
     */

    /**
     *
     * @param photoId
     * @param img
     */
    public Get_dish_type_image_pojo(String photoId, String img) {
        super();
        this.photoId = photoId;
        this.img = img;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
