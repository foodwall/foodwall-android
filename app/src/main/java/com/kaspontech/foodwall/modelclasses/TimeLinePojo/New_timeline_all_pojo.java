package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

/**
 * Created by vishnukm on 21/3/18.
 */

public class New_timeline_all_pojo {



    private String created_by;

    private String created_on;

    private String imei;

    private String cont_no;

    private String total_likes;

    private String picture;

    private String oauth_uid;

    private String first_name;

    private Image image_pojo;

    private String email;

    private String oauth_provider;

    private String dob;

    private String timeline_description;

    private String total_comments;

    private String last_name;

    private String gender;

    private String longitude;

    private String user_id;

    private String latitude;

    private String timeline_id;

    public String getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (String created_by)
    {
        this.created_by = created_by;
    }

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getImei ()
    {
        return imei;
    }

    public void setImei (String imei)
    {
        this.imei = imei;
    }

    public String getCont_no ()
    {
        return cont_no;
    }

    public void setCont_no (String cont_no)
    {
        this.cont_no = cont_no;
    }

    public String getTotal_likes ()
    {
        return total_likes;
    }

    public void setTotal_likes (String total_likes)
    {
        this.total_likes = total_likes;
    }

    public String getPicture ()
    {
        return picture;
    }

    public void setPicture (String picture)
    {
        this.picture = picture;
    }

    public String getOauth_uid ()
    {
        return oauth_uid;
    }

    public void setOauth_uid (String oauth_uid)
    {
        this.oauth_uid = oauth_uid;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public Image getImage_pojo ()
    {
        return image_pojo;
    }

    public void setImage_pojo (Image image_pojo)
    {
        this.image_pojo = image_pojo;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getOauth_provider ()
    {
        return oauth_provider;
    }

    public void setOauth_provider (String oauth_provider)
    {
        this.oauth_provider = oauth_provider;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getTimeline_description ()
    {
        return timeline_description;
    }

    public void setTimeline_description (String timeline_description)
    {
        this.timeline_description = timeline_description;
    }

    public String getTotal_comments ()
    {
        return total_comments;
    }

    public void setTotal_comments (String total_comments)
    {
        this.total_comments = total_comments;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getTimeline_id ()
    {
        return timeline_id;
    }

    public void setTimeline_id (String timeline_id)
    {
        this.timeline_id = timeline_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [created_by = "+created_by+", created_on = "+created_on+", imei = "+imei+", cont_no = "+cont_no+", total_likes = "+total_likes+", picture = "+picture+", oauth_uid = "+oauth_uid+", first_name = "+first_name+", 0 = "+0+", email = "+email+", oauth_provider = "+oauth_provider+", dob = "+dob+", timeline_description = "+timeline_description+", total_comments = "+total_comments+", last_name = "+last_name+", gender = "+gender+", longitude = "+longitude+", user_id = "+user_id+", latitude = "+latitude+", timeline_id = "+timeline_id+"]";
    }



}
