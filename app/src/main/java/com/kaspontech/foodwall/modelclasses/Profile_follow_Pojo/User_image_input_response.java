package com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo;

/**
 * Created by vishnukm on 31/3/18.
 */

public class User_image_input_response {

    private String status;

    public String getStatus ()
    {
        return status;
    }

    public User_image_input_response(String status) {
        this.status = status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+"]";
    }
}
