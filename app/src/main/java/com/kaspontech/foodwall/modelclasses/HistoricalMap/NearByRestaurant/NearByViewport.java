package com.kaspontech.foodwall.modelclasses.HistoricalMap.NearByRestaurant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NearByViewport {

    @SerializedName("northeast")
    @Expose
    private NearByNortheast northeast;
    @SerializedName("southwest")
    @Expose
    private NearBySouthwest southwest;

    public NearByNortheast getNortheast() {
        return northeast;
    }

    public void setNortheast(NearByNortheast northeast) {
        this.northeast = northeast;
    }

    public NearBySouthwest getSouthwest() {
        return southwest;
    }

    public void setSouthwest(NearBySouthwest southwest) {
        this.southwest = southwest;
    }
}
