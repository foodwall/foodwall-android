package com.kaspontech.foodwall.modelclasses.storyPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class storyViewInputPojo {
    @SerializedName("stories_id")
    @Expose
    private String storiesId;
    @SerializedName("total_view")
    @Expose
    private String totalView;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private Object contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("stories_description")
    @Expose
    private String storiesDescription;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("stories_image")
    @Expose
    private String storiesImage;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param picture
     * @param lastName
     * @param area
     * @param storiesDescription
     * @param address
     * @param email
     * @param dob
     * @param userId
     * @param gender
     * @param contNo
     * @param totalView
     * @param storiesId
     * @param firstName
     * @param storiesImage
     * @param city
     */
    public storyViewInputPojo(String storiesId, String totalView, String userId, String firstName, String lastName, String email, Object contNo, String gender, String dob, String picture, String storiesDescription, String address, String area, String city, String storiesImage) {
        super();
        this.storiesId = storiesId;
        this.totalView = totalView;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
        this.storiesDescription = storiesDescription;
        this.address = address;
        this.area = area;
        this.city = city;
        this.storiesImage = storiesImage;
    }

    public String getStoriesId() {
        return storiesId;
    }

    public void setStoriesId(String storiesId) {
        this.storiesId = storiesId;
    }

    public String getTotalView() {
        return totalView;
    }

    public void setTotalView(String totalView) {
        this.totalView = totalView;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getContNo() {
        return contNo;
    }

    public void setContNo(Object contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getStoriesDescription() {
        return storiesDescription;
    }

    public void setStoriesDescription(String storiesDescription) {
        this.storiesDescription = storiesDescription;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStoriesImage() {
        return storiesImage;
    }

    public void setStoriesImage(String storiesImage) {
        this.storiesImage = storiesImage;
    }
}
