package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.Question;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateQuestionData {

    @SerializedName("quest_id")
    @Expose
    private String questId;
    @SerializedName("ask_question")
    @Expose
    private String askQuestion;
    @SerializedName("total_answers")
    @Expose
    private String totalAnswers;

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public String getAskQuestion() {
        return askQuestion;
    }

    public void setAskQuestion(String askQuestion) {
        this.askQuestion = askQuestion;
    }

    public String getTotalAnswers() {
        return totalAnswers;
    }

    public void setTotalAnswers(String totalAnswers) {
        this.totalAnswers = totalAnswers;
    }

}
