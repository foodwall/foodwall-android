package com.kaspontech.foodwall.modelclasses.Review_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetHotelReviewImageAll {
    @SerializedName("photo_id")
    @Expose
    private String photoId;
    @SerializedName("reviewid")
    @Expose
    private String reviewid;
    @SerializedName("dishid")
    @Expose
    private String dishid;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("dishimage")
    @Expose
    private String dishimage;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param dishid
     * @param photoId
     * @param userid
     * @param reviewid
     * @param dishimage
     */
    public GetHotelReviewImageAll(String photoId, String reviewid, String dishid, String userid, String dishimage) {
        super();
        this.photoId = photoId;
        this.reviewid = reviewid;
        this.dishid = dishid;
        this.userid = userid;
        this.dishimage = dishimage;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getReviewid() {
        return reviewid;
    }

    public void setReviewid(String reviewid) {
        this.reviewid = reviewid;
    }

    public String getDishid() {
        return dishid;
    }

    public void setDishid(String dishid) {
        this.dishid = dishid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getDishimage() {
        return dishimage;
    }

    public void setDishimage(String dishimage) {
        this.dishimage = dishimage;
    }
}
