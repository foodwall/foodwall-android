package com.kaspontech.foodwall.modelclasses.CommonSearch.CommonSearchListView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kaspontech.foodwall.modelclasses.Review_pojo.Get_hotel_all_inner_review_pojo;

import java.util.ArrayList;
import java.util.List;

public class GetCommonSearchData {


    @SerializedName("people")
    @Expose
    private List<SearchPersonPojo> people = null;
    @SerializedName("timeline")
    @Expose
    private List<SearchTimelinePojo> timeline = null;
    @SerializedName("review")
    @Expose
    private ArrayList<Get_hotel_all_inner_review_pojo> review = null;
    @SerializedName("question")
    @Expose
    private List<SearchQuestionPojo> question = null;
    @SerializedName("events")
    @Expose
    private List<SearchEventPojo> events = null;
    @SerializedName("hotel")
    @Expose
    private List<SearchRestaurantPojo> hotel = null;


    public GetCommonSearchData(List<SearchPersonPojo> people, List<SearchTimelinePojo> timeline, ArrayList<Get_hotel_all_inner_review_pojo> review, List<SearchQuestionPojo> question, List<SearchEventPojo> events, List<SearchRestaurantPojo> hotel) {
        this.people = people;
        this.timeline = timeline;
        this.review = review;
        this.question = question;
        this.events = events;
        this.hotel = hotel;
    }

    public List<SearchRestaurantPojo> getHotel() {
        return hotel;
    }

    public void setHotel(List<SearchRestaurantPojo> hotel) {
        this.hotel = hotel;
    }

    public List<SearchPersonPojo> getPeople() {
        return people;
    }

    public void setPeople(List<SearchPersonPojo> people) {
        this.people = people;
    }

    public List<SearchTimelinePojo> getTimeline() {
        return timeline;
    }

    public void setTimeline(List<SearchTimelinePojo> timeline) {
        this.timeline = timeline;
    }

    public ArrayList<Get_hotel_all_inner_review_pojo> getReview() {
        return review;
    }

    public void setReview(ArrayList<Get_hotel_all_inner_review_pojo> review) {
        this.review = review;
    }

    public List<SearchQuestionPojo> getQuestion() {
        return question;
    }

    public void setQuestion(List<SearchQuestionPojo> question) {
        this.question = question;
    }

    public List<SearchEventPojo> getEvents() {
        return events;
    }

    public void setEvents(List<SearchEventPojo> events) {
        this.events = events;
    }


}
