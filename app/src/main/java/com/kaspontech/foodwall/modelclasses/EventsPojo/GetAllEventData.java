package com.kaspontech.foodwall.modelclasses.EventsPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by balaji on 16/3/18.
 */

public class GetAllEventData {

    @SerializedName("event_id")
    @Expose
    private int eventId;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_description")
    @Expose
    private String eventDescription;
    @SerializedName("total_likes")
    @Expose
    private int totalLikes;
    @SerializedName("total_comments")
    @Expose
    private int totalComments;
    @SerializedName("total_going")
    @Expose
    private String totalGoing;
    @SerializedName("event_image")
    @Expose
    private String eventImage;
    @SerializedName("event_type")
    @Expose
    private int eventType;
    @SerializedName("share_type")
    @Expose
    private int shareType;
    @SerializedName("ticket_url")
    @Expose
    private String ticketUrl;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("likes_evt_id")
    @Expose
    private String likesEvtId;
    @SerializedName("evt_likes")
    @Expose
    private int evtLikes;
    @SerializedName("going_evt_id")
    @Expose
    private String goingEvtId;
    @SerializedName("gng_likes")
    @Expose
    private int gngLikes;
    @SerializedName("total_discussion")
    @Expose
    private int total_discussion;
    @SerializedName("following_id")
    @Expose
    private String followingId;
    @SerializedName("userinterested")
    @Expose
    private boolean userInterested;
    @SerializedName("userGoing")
    @Expose
    private boolean userGoing;


    public GetAllEventData(int eventId, String eventName, String eventDescription, int totalLikes, int totalComments, String eventImage, int eventType, String ticketUrl, String location, String startDate, String endDate, String createdBy, String createdOn, String firstName, String lastName, String picture, int userLikes, int userGoing, int share_type, int total_discussion) {
        this.eventId = eventId;
        this.eventName = eventName;
        this.eventDescription = eventDescription;
        this.totalLikes = totalLikes;
        this.totalComments = totalComments;
        this.eventImage = eventImage;
        this.eventType = eventType;
        this.ticketUrl = ticketUrl;
        this.location = location;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.evtLikes = userLikes;
        this.gngLikes = userGoing;
        this.shareType = share_type;
        this.total_discussion = total_discussion;
    }

    public int getTotal_discussion() {
        return total_discussion;
    }

    public void setTotal_discussion(int total_discussion) {
        this.total_discussion = total_discussion;
    }

    public boolean isUserGoing() {
        return userGoing;
    }

    public void setUserGoing(boolean userGoing) {
        this.userGoing = userGoing;
    }

    public boolean isUserInterested() {
        return userInterested;
    }

    public void setUserInterested(boolean userInterested) {
        this.userInterested = userInterested;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public int getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(int totalLikes) {
        this.totalLikes = totalLikes;
    }

    public int getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(int totalComments) {
        this.totalComments = totalComments;
    }

    public String getTotalGoing() {
        return totalGoing;
    }

    public void setTotalGoing(String totalGoing) {
        this.totalGoing = totalGoing;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public int getShareType() {
        return shareType;
    }

    public void setShareType(int shareType) {
        this.shareType = shareType;
    }

    public String getTicketUrl() {
        return ticketUrl;
    }

    public void setTicketUrl(String ticketUrl) {
        this.ticketUrl = ticketUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLikesEvtId() {
        return likesEvtId;
    }

    public void setLikesEvtId(String likesEvtId) {
        this.likesEvtId = likesEvtId;
    }

    public int getEvtLikes() {
        return evtLikes;
    }

    public void setEvtLikes(int evtLikes) {
        this.evtLikes = evtLikes;
    }

    public String getGoingEvtId() {
        return goingEvtId;
    }

    public void setGoingEvtId(String goingEvtId) {
        this.goingEvtId = goingEvtId;
    }

    public int getGngLikes() {
        return gngLikes;
    }

    public void setGngLikes(int gngLikes) {
        this.gngLikes = gngLikes;
    }

    public String getFollowingId() {
        return followingId;
    }

    public void setFollowingId(String followingId) {
        this.followingId = followingId;
    }
}



