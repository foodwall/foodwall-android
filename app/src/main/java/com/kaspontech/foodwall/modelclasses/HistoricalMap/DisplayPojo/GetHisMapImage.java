package com.kaspontech.foodwall.modelclasses.HistoricalMap.DisplayPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetHisMapImage implements Serializable {

    @SerializedName("photo_id")
    @Expose
    private String photoId;
    @SerializedName("img")
    @Expose
    private String img;

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
