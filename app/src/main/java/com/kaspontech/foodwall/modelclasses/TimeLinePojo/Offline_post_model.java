package com.kaspontech.foodwall.modelclasses.TimeLinePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vishnukm on 12/3/18.
 */

public class Offline_post_model {
    @SerializedName("created_by")
    @Expose
    private String createdBy;

    @SerializedName("user_name")
    @Expose
    private String username;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("comments")
    @Expose
    private String comments;

    @SerializedName("created_on")
    @Expose
    private String createdOn;


    @SerializedName("captions")
    @Expose
    private String captions;
    @SerializedName("share")
    @Expose
    private String share;


    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCaptions() {
        return captions;
    }

    public void setCaptions(String captions) {
        this.captions = captions;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public Offline_post_model(String createdBy,String username, String location, String likes, String comments, String image, String createdOn, String captions, String share) {
        this.createdBy = createdBy;
        this.username = username;
        this.location = location;
        this.likes = likes;
        this.comments = comments;
        this.image = image;
        this.createdOn = createdOn;
        this.captions = captions;
        this.share = share;
    }

}
