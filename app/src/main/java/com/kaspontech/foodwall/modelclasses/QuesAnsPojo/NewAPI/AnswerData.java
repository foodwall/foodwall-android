package com.kaspontech.foodwall.modelclasses.QuesAnsPojo.NewAPI;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnswerData {

    @SerializedName("ans_id")
    @Expose
    private String ansId;
    @SerializedName("quest_id")
    @Expose
    private String questId;
    @SerializedName("ask_answer")
    @Expose
    private String askAnswer;
    @SerializedName("ques_type")
    @Expose
    private String quesType;
    @SerializedName("poll_id")
    @Expose
    private String pollId;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("total_upvote")
    @Expose
    private String totalUpvote;
    @SerializedName("total_downvote")
    @Expose
    private String totalDownvote;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_uid")
    @Expose
    private String oauthUid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("cont_no")
    @Expose
    private Object contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("ask_question")
    @Expose
    private String askQuestion;
    @SerializedName("total_answers")
    @Expose
    private String totalAnswers;
    @SerializedName("vote_id")
    @Expose
    private int voteId;
    @SerializedName("up_down_vote")
    @Expose
    private int upDownVote;
    @SerializedName("isUpVoted")
    @Expose
    private boolean upVoted;

    @SerializedName("isDownVoted")
    @Expose
    private boolean downVoted;
    /**
     * No args constructor for use in serialization
     *
     */
    public AnswerData() {
    }

    /**
     *
     * @param oauthProvider
     * @param upDownVote
     * @param totalComments
     * @param city
     * @param userId
     * @param gender
     * @param totalUpvote
     * @param contNo
     * @param longitude
     * @param firstName
     * @param questId
     * @param askQuestion
     * @param lastName
     * @param pollId
     * @param imei
     * @param deleted
     * @param totalDownvote
     * @param country
     * @param oauthUid
     * @param picture
     * @param askAnswer
     * @param createdOn
     * @param totalAnswers
     * @param ansId
     * @param createdBy
     * @param email
     * @param dob
     * @param active
     * @param latitude
     * @param voteId
     * @param quesType
     */
    public AnswerData(String ansId, String questId, String askAnswer, String quesType, String pollId, String country,
                      String city, String totalUpvote, String totalDownvote,
                      String totalComments, String latitude, String longitude,
                      String createdBy, String createdOn, String active, String deleted,
                      String userId, String oauthProvider, String oauthUid, String firstName,
                      String lastName, String email, String imei, Object contNo, String gender,
                      String dob, String picture, String askQuestion, String totalAnswers,
                      int voteId, int upDownVote) {
        super();
        this.ansId = ansId;
        this.questId = questId;
        this.askAnswer = askAnswer;
        this.quesType = quesType;
        this.pollId = pollId;
        this.country = country;
        this.city = city;
        this.totalUpvote = totalUpvote;
        this.totalDownvote = totalDownvote;
        this.totalComments = totalComments;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.active = active;
        this.deleted = deleted;
        this.userId = userId;
        this.oauthProvider = oauthProvider;
        this.oauthUid = oauthUid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imei = imei;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
        this.askQuestion = askQuestion;
        this.totalAnswers = totalAnswers;
        this.voteId = voteId;
        this.upDownVote = upDownVote;
    }


    public boolean isUpVoted() {
        return upVoted;
    }

    public void setUpVoted(boolean upVoted) {
        this.upVoted = upVoted;
    }

    public boolean isDownVoted() {
        return downVoted;
    }

    public void setDownVoted(boolean downVoted) {
        this.downVoted = downVoted;
    }

    public String getAnsId() {
        return ansId;
    }

    public void setAnsId(String ansId) {
        this.ansId = ansId;
    }

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public String getAskAnswer() {
        return askAnswer;
    }

    public void setAskAnswer(String askAnswer) {
        this.askAnswer = askAnswer;
    }

    public String getQuesType() {
        return quesType;
    }

    public void setQuesType(String quesType) {
        this.quesType = quesType;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTotalUpvote() {
        return totalUpvote;
    }

    public void setTotalUpvote(String totalUpvote) {
        this.totalUpvote = totalUpvote;
    }

    public String getTotalDownvote() {
        return totalDownvote;
    }

    public void setTotalDownvote(String totalDownvote) {
        this.totalDownvote = totalDownvote;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Object getContNo() {
        return contNo;
    }

    public void setContNo(Object contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getAskQuestion() {
        return askQuestion;
    }

    public void setAskQuestion(String askQuestion) {
        this.askQuestion = askQuestion;
    }

    public String getTotalAnswers() {
        return totalAnswers;
    }

    public void setTotalAnswers(String totalAnswers) {
        this.totalAnswers = totalAnswers;
    }

    public int getVoteId() {
        return voteId;
    }

    public void setVoteId(int voteId) {
        this.voteId = voteId;
    }

    public int getUpDownVote() {
        return upDownVote;
    }

    public void setUpDownVote(int upDownVote) {
        this.upDownVote = upDownVote;
    }

}