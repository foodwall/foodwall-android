package com.kaspontech.foodwall;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.foodFeedsPackage.TotalCountModule_Response;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppTotalCountModuleService extends Service {

    private Timer timer = new Timer();

    boolean isfirst;

    String IsFirsttime;

    public AppTotalCountModuleService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
               //  createAppCountModule();
            }
            //  }, 0, 5*60*1000);//5 Minutes
        }, 0, 10000);//10 Seconds*/

            //  }, 0, 5*60*1000);//5 Minutes
       // }, 0, 15000);//10 Seconds*/
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    private void createAppCountModule() {

     try {
         if (Utility.isConnected(getApplicationContext())) {


             ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
             try {

                 String userid = Pref_storage.getDetail(getApplicationContext(), "userId");

                 Call<TotalCountModule_Response> call = apiService.get_total_count_modules("get_total_count_modules",
                         Integer.parseInt(userid));
                 call.enqueue(new Callback<TotalCountModule_Response>() {
                     @Override
                     public void onResponse(Call<TotalCountModule_Response> call, Response<TotalCountModule_Response> response) {

                         if (response.body().getResponseCode() == 1) {

                             // String loggedout = response.body().getData();

                             //  Toast.makeText(getApplicationContext(), "" + response, Toast.LENGTH_SHORT).show();
                           /* Log.e("loggedout", "events:" + response.body().getData().get(0).getTotalEvents());
                            Log.e("loggedout", "posts:" + response.body().getData().get(0).getTotalPosts());
                            Log.e("loggedout", "question:" + response.body().getData().get(0).getTotalQuestion());
                            Log.e("loggedout", "review:" + response.body().getData().get(0).getTotalReview());
*/

                             /*Getting the app is logged in or not whether to redirect to home page ot login page */
                             IsFirsttime = Pref_storage.getDetail(getApplicationContext(), "IsFirsttime");


                             Log.e("SplashScreen", "isLoggedIn->" + IsFirsttime);

                             if (IsFirsttime == null) {

                                 Log.e("SplashScreen", "isLoggedIn->" + IsFirsttime);

                                 Pref_storage.setDetail(getApplicationContext(), "PreEventCount", String.valueOf(response.body().getData().get(0).getTotalEvents()));
                                 Pref_storage.setDetail(getApplicationContext(), "TotalPosts", String.valueOf(response.body().getData().get(0).getTotalPosts()));
                                 Pref_storage.setDetail(getApplicationContext(), "TotalQuestion", String.valueOf(response.body().getData().get(0).getTotalQuestion()));
                                 Pref_storage.setDetail(getApplicationContext(), "TotalReview", String.valueOf(response.body().getData().get(0).getTotalReview()));

                                 Log.e("previouscount", "PreEventCount->" + Pref_storage.getDetail(getApplicationContext(), "PreEventCount"));
                                 Log.e("previouscount", "TotalPosts->" + Pref_storage.getDetail(getApplicationContext(), "TotalPosts"));
                                 Log.e("previouscount", "TotalQuestion->" + Pref_storage.getDetail(getApplicationContext(), "TotalQuestion"));
                                 Log.e("previouscount", "TotalReview->" + Pref_storage.getDetail(getApplicationContext(), "TotalReview"));

                                 Pref_storage.setDetail(getApplicationContext(), "IsFirsttime", "1");

                             } else {

                                 Log.e("SplashScreen", "isLoggedIn->" + IsFirsttime);


                                 int totalevent = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "PreEventCount"));
                                 int TotalPosts = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "TotalPosts"));
                                 int TotalQuestion = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "TotalQuestion"));
                                 int TotalReview = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "TotalReview"));

                                 Log.e("PresentCount", "totalevent->" + totalevent);
                                 Log.e("PresentCount", "TotalPosts->" + TotalPosts);
                                 Log.e("PresentCount", "TotalQuestion->" + TotalQuestion);
                                 Log.e("PresentCount", "TotalReview->" + TotalReview);



                                 if (totalevent < response.body().getData().get(0).getTotalEvents()) {

                                     int val = response.body().getData().get(0).getTotalEvents()-totalevent;
                                      Log.e("totalcount", "totalevent->" + val);
                                     Pref_storage.setDetail(getApplicationContext(), "Event", String.valueOf(val));


                                 } else if (TotalPosts < response.body().getData().get(0).getTotalPosts()) {
                                     int val = response.body().getData().get(0).getTotalPosts()-TotalPosts;
                                     Log.e("totalcount", "totalpost->" + val);
                                     Pref_storage.setDetail(getApplicationContext(), "Post", String.valueOf(val));

                                 } else if (TotalQuestion < response.body().getData().get(0).getTotalQuestion()) {
                                     int val = response.body().getData().get(0).getTotalQuestion()-TotalQuestion;
                                     Log.e("totalcount", "totalquestion->" + val);
                                     Pref_storage.setDetail(getApplicationContext(), "Question", String.valueOf(val));

                                 } else if (TotalReview < response.body().getData().get(0).getTotalReview()) {
                                     int val = response.body().getData().get(0).getTotalReview()-TotalReview;
                                     Log.e("totalcount", "totalreviews->" + val);
                                     Pref_storage.setDetail(getApplicationContext(), "Review", String.valueOf(val));
                                 }else
                                 {
                                     Pref_storage.setDetail(getApplicationContext(), "Event", "0");
                                     Pref_storage.setDetail(getApplicationContext(), "Post", "0");
                                     Pref_storage.setDetail(getApplicationContext(), "Question", "0");
                                     Pref_storage.setDetail(getApplicationContext(), "Review", "0");

                                 }


                             }




                         }

                     }

                     @Override
                     public void onFailure(Call<TotalCountModule_Response> call, Throwable t) {
                         //Error
                         Log.e("FailureError", "" + t.getMessage());
                     }
                 });
             } catch (Exception e) {
                 e.printStackTrace();
             }

         }

         else {
            // Toast.makeText(getApplicationContext(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();

         }
     }catch (Exception e)
     {
         e.printStackTrace();
     }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
