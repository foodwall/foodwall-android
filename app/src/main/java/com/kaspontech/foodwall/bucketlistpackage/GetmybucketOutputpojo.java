package com.kaspontech.foodwall.bucketlistpackage;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetmybucketOutputpojo {

	@SerializedName("ResponseCode")
	private int responseCode;

	@SerializedName("custom")
	private String custom;

	@SerializedName("methodName")
	private String methodName;

	@SerializedName("Data")
	private ArrayList<GetmyBucketInputPojo> data;

	@SerializedName("ResponseMessage")
	private String responseMessage;

	@SerializedName("status")
	private int status;

	public void setResponseCode(int responseCode){
		this.responseCode = responseCode;
	}

	public int getResponseCode(){
		return responseCode;
	}

	public void setCustom(String custom){
		this.custom = custom;
	}

	public String getCustom(){
		return custom;
	}

	public void setMethodName(String methodName){
		this.methodName = methodName;
	}

	public String getMethodName(){
		return methodName;
	}

	public void setData(ArrayList<GetmyBucketInputPojo> data){
		this.data = data;
	}

	public ArrayList<GetmyBucketInputPojo> getData(){
		return data;
	}

	public void setResponseMessage(String responseMessage){
		this.responseMessage = responseMessage;
	}

	public String getResponseMessage(){
		return responseMessage;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}
}