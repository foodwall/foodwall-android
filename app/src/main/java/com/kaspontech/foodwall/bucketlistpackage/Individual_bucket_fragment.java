package com.kaspontech.foodwall.bucketlistpackage;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.bucketadapter.bucketmyselfAdapter;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Individual_bucket_fragment extends Fragment {

    /**
     * Context
     **/
    Context context;
    /**
     * View
     **/
    View view;
    /**
     * No data textview
     **/
    TextView llNoPost;
    /**
     * Page background for no data available
     **/
    RelativeLayout pageBackground;
    /**
     * Individual bucket recyclerview
     **/
    RecyclerView myBucketRecyclerview;
    /**
     * Individual Bucket adapter
     **/

    bucketmyselfAdapter bucketMyselfAdapter;
    /**
     * Group bucket arraylist
     **/
    ArrayList<GetmyBucketInputPojo> getmyBucketInputPojoArrayList = new ArrayList<>();


    private String userIdViewSelf;

    public Individual_bucket_fragment() {
        // Required empty public constructor
    }


    ProgressBar progressbar_review;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mybucket_layout, container, false);
        context = view.getContext();

        /*Widget initialization*/

        myBucketRecyclerview = view.findViewById(R.id.my_bucket_recyclerview);

        /*No post textview*/

        llNoPost = view.findViewById(R.id.ll_no_post);

        /*
          Page background for no data available
         */
        pageBackground = view.findViewById(R.id.page_background);
        progressbar_review = view.findViewById(R.id.progressbar_review);



        /* Calling API for individual bucket users */
        getBucketIndividualUser();

        return view;
    }

    /* Calling API for individual bucket users */

    private void getBucketIndividualUser() {

        if (Utility.isConnected(context)) {
            progressbar_review.setVisibility(View.VISIBLE);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));
                userIdViewSelf = Pref_storage.getDetail(context, "userId");

                Call<GetmybucketOutputpojo> call = apiService.get_bucket_individual_user("get_bucket_individual_user", userid);

                call.enqueue(new Callback<GetmybucketOutputpojo>() {
                    @Override
                    public void onResponse(Call<GetmybucketOutputpojo> call, Response<GetmybucketOutputpojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            llNoPost.setVisibility(View.GONE);
                            getmyBucketInputPojoArrayList.clear();
                            //Success
                            pageBackground.setBackgroundColor(context.getResources().getColor(R.color.page_background));

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            getmyBucketInputPojoArrayList = response.body().getData();

                            progressbar_review.setVisibility(View.GONE);

                            /*Setting adapter*/
                            bucketMyselfAdapter = new bucketmyselfAdapter(context, getmyBucketInputPojoArrayList, userIdViewSelf);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                            myBucketRecyclerview.setLayoutManager(mLayoutManager);
                            myBucketRecyclerview.setItemAnimator(new DefaultItemAnimator());
                            myBucketRecyclerview.setAdapter(bucketMyselfAdapter);
                            myBucketRecyclerview.hasFixedSize();
                            myBucketRecyclerview.setNestedScrollingEnabled(false);
                            bucketMyselfAdapter.notifyDataSetChanged();
                            myBucketRecyclerview.setVisibility(View.VISIBLE);


                        } else if (responseStatus.equals("nodata")) {

                            llNoPost.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<GetmybucketOutputpojo> call, Throwable t) {
                        progressbar_review.setVisibility(View.GONE);

                        if (getmyBucketInputPojoArrayList.isEmpty()) {
                            llNoPost.setVisibility(View.VISIBLE);
                        }

                        pageBackground.setBackgroundColor(Color.WHITE);
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(context, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }


    }
    /* Calling API for individual bucket users on resume call */

    private void getBucketIndividualUserRefresh() {


        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Call<GetmybucketOutputpojo> call = apiService.get_bucket_individual_user("get_bucket_individual_user", userid);

                call.enqueue(new Callback<GetmybucketOutputpojo>() {
                    @Override
                    public void onResponse(Call<GetmybucketOutputpojo> call, Response<GetmybucketOutputpojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            llNoPost.setVisibility(View.GONE);
                            getmyBucketInputPojoArrayList.clear();
                            //Success
                            pageBackground.setBackgroundColor(context.getResources().getColor(R.color.page_background));

                            Log.e("responseStatus", "responseSize->" + response.body().getData().size());


                            getmyBucketInputPojoArrayList = response.body().getData();

                            /*Setting adapter*/
                            bucketMyselfAdapter = new bucketmyselfAdapter(context, getmyBucketInputPojoArrayList, userIdViewSelf);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                            myBucketRecyclerview.setLayoutManager(mLayoutManager);
                            myBucketRecyclerview.setItemAnimator(new DefaultItemAnimator());
                            myBucketRecyclerview.setAdapter(bucketMyselfAdapter);
                            myBucketRecyclerview.hasFixedSize();
                            myBucketRecyclerview.setNestedScrollingEnabled(false);
                            bucketMyselfAdapter.notifyDataSetChanged();
                            myBucketRecyclerview.setVisibility(View.VISIBLE);


                        } else if (responseStatus.equals("nodata")) {

                            llNoPost.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<GetmybucketOutputpojo> call, Throwable t) {
                        llNoPost.setVisibility(View.VISIBLE);
                        pageBackground.setBackgroundColor(Color.WHITE);
                        myBucketRecyclerview.setVisibility(View.GONE);
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(context, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }


    }


    /*Size checking using interface*/
    public void indisize(int size) {
        Toast.makeText(context, "" + size, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        /* Calling API for individual bucket users on resume call */

        if (!getmyBucketInputPojoArrayList.isEmpty()) {
            getBucketIndividualUserRefresh();

        } else {
//             getBucketIndividualUser();

        }
    }
}
