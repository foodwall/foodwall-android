package com.kaspontech.foodwall.bucketlistpackage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetMyBucketListAmbiImage {

    @SerializedName("img")
    @Expose
    private String img;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetMyBucketListAmbiImage() {
    }

    /**
     *
     * @param img
     */
    public GetMyBucketListAmbiImage(String img) {
        super();
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
