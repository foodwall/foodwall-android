package com.kaspontech.foodwall.bucketlistpackage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.adapters.bucketadapter.GroupBucketAdapter;
import com.kaspontech.foodwall.adapters.bucketadapter.IndiFollowerAdapter;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.Get_grp_bucket_pojo;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.Get_grp_input_pojo;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_follower_output_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;


import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class CreateBucketlistActivty extends AppCompatActivity implements View.OnClickListener {

    /**
     * Context
     */
    private static final String TAG = "CreateBucketList";

    /**
     * Action Bar
     */
    Toolbar actionBar;
    /**
     *  Tool bar title
     */
    TextView toolbarTitle;

    /**
     *  Tool bar next
     */
    TextView toolbarNext;
    /**
     *  Back button
     */
    ImageButton back;
    /**
     *  Bucket image button
     */
    ImageButton imgBucketClick;

    /**
     *  Search button individual user
     */
    ImageButton imgIndiSearch;
    /**
     *  Search button group user
     */
    ImageButton imgGrpSearch;

    /**
     *  Button done
     */
    Button btnDone;
    /**
     *  Button group done
     */
    Button btnDoneGrp;

    /**
     * My bucket checkbox
     */
    CheckBox myBucketCheckbox;

    /**
     *  User bucket image
     */

    ImageView imgBucketUser;

    /**
     *  Recyclerview for friends
     */

    RecyclerView rvFriends;

    /**
     *  Recyclerview for group
     */

    RecyclerView rvGroupsBucket;

    /**
     *  Recyclerview for followers search
     */

    RecyclerView rvFollowerGrpSearch;
    /**
     *  Relative layout for self bucket list
     */

    RelativeLayout llMyBucketList;

    /**
     *  Relative layout for creating bucket list
     */

    RelativeLayout rlCreateBucketList;


    /**
     *  Fab button adding bucket list
     */


    FloatingActionButton fabSearchAdd;


    /**
     *  Linear layout manager for bucket list
     */
    LinearLayoutManager llm;

    /**
     * Searchview for group followers
     */
    android.support.v7.widget.SearchView search_follow_grp;

    /**
     * Arraylist of bucketlist
     */

    Get_followerlist_pojo getFollowerlistPojo;
    List<Get_followerlist_pojo> followerlist = new ArrayList<>();


    Get_grp_input_pojo getGrpInputPojo;
    ArrayList<Get_grp_input_pojo> grplist = new ArrayList<>();


    /*
    * No data layout
    *
    * */
    LinearLayout ll_nodata;

    LinearLayout ll_nodatagroup;

    ArrayList<Integer> followerlistid = new ArrayList<>();
    ArrayList<Integer> selectedgrplist = new ArrayList<>();

    HashMap<String, Integer> selectedfollowerlistId = new HashMap<>();
    HashMap<String, Integer> selectedgroupID = new HashMap<>();


    IndiFollowerAdapter individualFollowerAdapter;

    GroupBucketAdapter groupBucketAdapter;

    Call<CommonOutputPojo> call;

    String grp_frmfname, grp_frmlname;
    String fname, lname, reviewid,hotelid;
    String grpname, grp_id;

    int timelineId=0;

    int userid_get_grp;

    //Follow search list
    ArrayAdapter<Get_followerlist_pojo> adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_bucket_list);

        /*Widget initialization*/

        actionBar = findViewById(R.id.actionbar_create_bucket);

        /* individual bucket list done click */

        btnDone = findViewById(R.id.btn_done);
        btnDone.setOnClickListener(this);

        /* individual bucket list group done click */

        btnDoneGrp = findViewById(R.id.btn_done_grp);
        btnDoneGrp.setOnClickListener(this);

        /* individual back button press  click */

        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        /* individual bucket list title next click */

        toolbarNext = actionBar.findViewById(R.id.toolbar_next);
        toolbarNext.setOnClickListener(this);


        /* Imageview for user*/

        imgBucketUser = findViewById(R.id.img_bucket_user);

        /* Imageview for bucket click*/

        imgBucketClick = findViewById(R.id.img_bucket_click);
        imgBucketClick.setOnClickListener(this);

        /* Imageview for individual search click*/

        imgIndiSearch = findViewById(R.id.img_indi_search);
        imgIndiSearch.setOnClickListener(this);

        /* Imageview for group search click*/

        imgGrpSearch = findViewById(R.id.img_group_search);
        imgGrpSearch.setOnClickListener(this);

        /* Imageview for toolbar title click*/

        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);

        toolbarTitle.setText(getString(R.string.create_bucket_list));

        /* Fab search add click*/

        fabSearchAdd = findViewById(R.id.fab_search_add);
        fabSearchAdd.setOnClickListener(this);


        /* RelativeLayout for my bucket list */

        llMyBucketList = findViewById(R.id.ll_my_bucket_list);
        llMyBucketList.setOnClickListener(this);

        /* RelativeLayout for create bucket list */

        rlCreateBucketList = findViewById(R.id.rl_create_bucket_list);

        /* RecyclerView friends list*/
        rvFriends = findViewById(R.id.rv_friends);

        /* RecyclerView group list*/

        rvGroupsBucket = findViewById(R.id.rv_groups_bucket);

        /* RecyclerView followers list*/

        rvFollowerGrpSearch = findViewById(R.id.rv_follower_grp_search);

        /*My bucket list checkbox*/

        myBucketCheckbox = findViewById(R.id.my_bucket_checkbox);

        /*Linear layout initialization*/

        llm = new LinearLayoutManager(this);


        ll_nodata = findViewById(R.id.ll_nodata);

        ll_nodatagroup = findViewById(R.id.ll_nodatagroup);


        /* Search View initialization and query listener */
        search_follow_grp = findViewById(R.id.search_follow_grp);

        search_follow_grp.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() == 0) {


                } else {
                }
                return true;
            }
        });


        try {

        /*Getting user's details*/
        Intent intent = getIntent();
        fname = intent.getStringExtra("f_name");
        lname = intent.getStringExtra("l_name");
        reviewid = intent.getStringExtra("reviewid");
        hotelid = intent.getStringExtra("hotelid");


            timelineId = Integer.parseInt(intent.getStringExtra("timelineId"));

        }catch (Exception e)
        {
            e.printStackTrace();
        }

        /* Load self Image */

        Utility.picassoImageLoader(Pref_storage.getDetail(getApplicationContext(), "picture_user_login"),
                0, imgBucketUser, getApplicationContext());


        /* API for follower and group user */
        getGroupUser();

        getFollower();



    }


    //Get follower
    private void getFollower() {


        if (Utility.isConnected(CreateBucketlistActivty.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_follower_output_pojo> call = apiService.get_follower("get_follower", Integer.parseInt(createdby));
                call.enqueue(new Callback<Get_follower_output_pojo>() {
                    @Override
                    public void onResponse(Call<Get_follower_output_pojo> call, Response<Get_follower_output_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                if (response.body().getData().get(j).getFollowerId() == null) {


                                } else {


                                    followerlist = response.body().getData();

                                    /*Setting adapter*/
                                    individualFollowerAdapter = new IndiFollowerAdapter(CreateBucketlistActivty.this, followerlist, selectedfollowerlistId);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CreateBucketlistActivty.this);
                                    rvFriends.setLayoutManager(mLayoutManager);
                                    rvFriends.setHasFixedSize(true);
                                    rvFriends.setItemAnimator(new DefaultItemAnimator());
                                    rvFriends.setAdapter(individualFollowerAdapter);
                                    rvFriends.setNestedScrollingEnabled(false);
                                    individualFollowerAdapter.notifyDataSetChanged();
                                    rvFriends.setVisibility(View.VISIBLE);
                                    rvFollowerGrpSearch.setAdapter(individualFollowerAdapter);
                                    rvFollowerGrpSearch.setNestedScrollingEnabled(false);

                                }


                            }
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Get_follower_output_pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e(TAG, "FailureError-->" + t.getMessage());

//                        ll_nodata.setVisibility(View.VISIBLE);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }


    /* Get Group User */
    private void getGroupUser() {


        if (Utility.isConnected(CreateBucketlistActivty.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_grp_bucket_pojo> call = apiService.get_group_user("get_group_user", Integer.parseInt(createdby));
                call.enqueue(new Callback<Get_grp_bucket_pojo>() {
                    @Override
                    public void onResponse(Call<Get_grp_bucket_pojo> call, Response<Get_grp_bucket_pojo> response) {

                        if (response.body() != null && response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {

                                String grpuserid = response.body().getData().get(j).getUserid();
                                grp_frmfname = response.body().getData().get(j).getFromFirstname();
                                grp_frmlname = response.body().getData().get(j).getFromLastname();
                                String grpFrmpicture = response.body().getData().get(j).getFromPicture();
                                String groupname = response.body().getData().get(j).getGroupName();
                                String groupId = response.body().getData().get(j).getGroup_id();
                                String groupicon = response.body().getData().get(j).getGroupIcon();
                                String changedWhich = response.body().getData().get(j).getChangedWhich();
                                String groupCreatedby = response.body().getData().get(j).getGroupCreatedby();
                                String groupCreatedDate = response.body().getData().get(j).getCreatedDate();
                                String groupCreatedFirstname = response.body().getData().get(j).getGroupCreatedFirstname();
                                String groupCreatedLastname = response.body().getData().get(j).getGroupCreatedLastname();


                                getGrpInputPojo = new Get_grp_input_pojo(grpuserid, grp_frmfname, grp_frmlname,
                                        grpFrmpicture, groupId, groupname, groupicon, changedWhich,
                                        groupCreatedby, groupCreatedDate, groupCreatedFirstname, groupCreatedLastname);

                                grplist.add(getGrpInputPojo);



                            }
                            /*Setting adapter*/
                            groupBucketAdapter = new GroupBucketAdapter(CreateBucketlistActivty.this, grplist, selectedfollowerlistId);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CreateBucketlistActivty.this);
                            rvGroupsBucket.setLayoutManager(mLayoutManager);
                            rvGroupsBucket.setHasFixedSize(true);
                            rvGroupsBucket.setItemAnimator(new DefaultItemAnimator());
                            rvGroupsBucket.setAdapter(groupBucketAdapter);
                            rvGroupsBucket.setNestedScrollingEnabled(false);
                            rvGroupsBucket.setVisibility(View.VISIBLE);

                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Get_grp_bucket_pojo> call, @NonNull Throwable t) {
                        //Error
                        Log.e(TAG, "FailureError-->" + t.getMessage());

//                        ll_nodatagroup.setVisibility(View.VISIBLE);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }


    /* Interface for individual follower */

    public void getSelectedfollower(View view,int position) {

        String followerName;
        followerName = followerlist.get(position).getFirstName() + " " + followerlist.get(position).getLastName();


        if (((CheckBox) view).isChecked()) {

            if (selectedfollowerlistId.containsValue(Integer.valueOf(followerlist.get(position).getFollowerId()))) {

                return;

            } else {

                selectedfollowerlistId.put(followerName, Integer.valueOf(followerlist.get(position).getFollowerId()));

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

                followerlistid.add(Integer.valueOf(followerlist.get(position).getFollowerId()));


                /* Your id for individual chat */

                if (followerlistid.toString().contains(String.valueOf(userid))) {

                } else {

                    followerlistid.add(userid);
                }

                Log.e(TAG, "getSelectedfollower:" + selectedfollowerlistId.toString());
                Log.e(TAG, "followerlistid:" + followerlistid.toString());

//                followerlistid.add(userid);

            }


        } else {

            followerlistid.remove(Integer.valueOf(followerlist.get(position).getFollowerId()));
            selectedfollowerlistId.remove(followerName);

        }

    }

    /* Interface for group selecting */

    public void getgrouplist(View view, int position) {


        grpname = grplist.get(position).getGroupName();

        grp_id = grplist.get(position).getGroup_id();

        if (((CheckBox) view).isChecked()) {


            selectedgroupID.put(grpname, Integer.valueOf(grplist.get(position).getGroup_id()));

            userid_get_grp = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

//            selectedgrplist.add(userid);


            selectedgrplist.add(Integer.valueOf(grplist.get(position).getGroup_id()));

           /* if(selectedgrplist.toString().contains(String.valueOf(useridGetGrp))){

            }else {
                selectedgrplist.add(useridGetGrp);
            }*/
            Log.e(TAG, "selectedgroupID:" + selectedgroupID.toString());
            Log.e(TAG, "selectedgrplist:" + selectedgrplist.toString());


        } else {

            selectedgrplist.remove(Integer.valueOf(grplist.get(position).getGroup_id()));
            selectedgroupID.remove(grpname);
//            selectedgrplist.remove(useridGetGrp);
            Log.e(TAG, "remove:" + selectedgroupID.toString());
            Log.e(TAG, "selectedgrplist_remove:" + selectedgrplist.toString());

        }

    }



    @Override
    public void onClick(View v) {

        int newid = v.getId();

        switch (newid) {

                /*Self bucket list creating api*/
            case R.id.ll_my_bucket_list:

                create_bucket_myself_without_group_and_individual();
                break;

            /*Self bucket list creating api*/
            case R.id.img_bucket_click:

                create_bucket_myself_without_group_and_individual();
                break;

            /*Bucket list searching api*/

            case R.id.img_indi_search:

                Intent intent = new Intent(this, SearchIndiBucketListActivity.class);
                intent.putExtra("reviewId", reviewid);
                startActivity(intent);
                break;

            /*Bucket list group searching api*/
            case R.id.img_group_search:

                Intent intentgrp = new Intent(this, SearchGroupBucketActivity.class);
                intentgrp.putExtra("reviewId", reviewid);
                startActivity(intentgrp);
                break;

            /*Back button press*/
            case R.id.back:
              //  finish();
                onBackPressed();
                break;

            /*Next title click listener*/

            case R.id.toolbar_next:

                if (myBucketCheckbox.isChecked()) {
                    /*Self bucket list creating api calling*/
                    create_bucket_myself();

                } else {

                    if (!selectedgrplist.isEmpty()&& !followerlistid.isEmpty()) {

                        /*Group and individual list creating api calling*/
                        create_bucket_group_both();

                    } else if (!followerlistid.isEmpty()/* && !selectedgrplist.isEmpty()*/) {

                        //  individual list creating api calling
                        create_bucket_individual_user();

                    } else if (/*followerlistid.isEmpty() &&*/ !selectedgrplist.isEmpty()) {

                        /*Group  list creating api calling*/
                        create_bucket_group();

                    } else if (selectedgrplist.isEmpty() && followerlistid.isEmpty()) {

                        Toast.makeText(this, "Atleast one group or follower need to be selected", Toast.LENGTH_SHORT).show();

                    }

                }


                break;
            /*Fab buttom search click listener*/
            case R.id.fab_search_add:

                if (myBucketCheckbox.isChecked()) {
                    /*Self bucket list creating api calling*/

                    create_bucket_myself();

                } else {

                    if (!selectedgrplist.isEmpty()&& !followerlistid.isEmpty()) {

                        /*Group and individual list creating api calling*/
                        create_bucket_group_both();

                    } else if (!followerlistid.isEmpty() /*&& !selectedgrplist.isEmpty()*/) {

                        /* individual list creating api calling*/
                        create_bucket_individual_user();

                    } else if (/*followerlistid.isEmpty() &&*/ !selectedgrplist.isEmpty()) {

                        /*Group  list creating api calling*/
                        create_bucket_group();

                    } else if (selectedgrplist.isEmpty() && followerlistid.isEmpty()) {

                        Toast.makeText(this, "Atleast one group or follower need to be selected", Toast.LENGTH_SHORT).show();

                    }


                }

                break;

            case R.id.btn_done:


                break;

        }


    }

    /* create_bucket_individual_user API */
    private void create_bucket_individual_user() {

        Log.e(TAG, "create_bucket_individual_user: hotelid--"+hotelid );
        Log.e(TAG, "create_bucket_individual_user: reviewId--"+reviewid );
        Log.e(TAG, "create_bucket_individual_user: timelineId--"+timelineId );
        Log.e(TAG, "create_bucket_individual_user: followerlistid--"+followerlistid );

        if (Utility.isConnected(CreateBucketlistActivty.this)) {
            int post_type = 2;

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(getApplicationContext(), "userId");

                String username = fname.concat(" ").concat(lname);


                String json = new Gson().toJson(followerlistid);
                Log.e(TAG, "create_bucket_individual_user: json--"+json );

                call = apiService.create_bucket_individual_user("create_bucket_individual_user",
                        Integer.parseInt(reviewid),
                        Integer.parseInt(hotelid),
                        timelineId,
                        username,
                        Integer.parseInt(createuserid),
                        json, post_type);

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {

                            new SweetAlertDialog(CreateBucketlistActivty.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!!")
                                    .setContentText("Added to your friend's bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();


                                           /* Intent intent = new Intent(CreateBucketlistActivty.this, MyBucketlistActivity.class);
                                            intent.putExtra("redirect", "2");
                                            startActivity(intent);*/

                                            Intent intent = new Intent(CreateBucketlistActivty.this, Home.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.putExtra("redirect", "profile");
                                            intent.putExtra("redirectbucket", "2");
                                            startActivity(intent);
                                        }
                                    })
                                    .show();


                        } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("alreadyexists")) {

                            new SweetAlertDialog(CreateBucketlistActivty.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Alert!!")
                                    .setContentText("Already added to friend's bucket list!!")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                            /*Intent intent = new Intent(CreateBucketlistActivty.this, MyBucketlistActivity.class);
                                            intent.putExtra("redirect", "2");
                                            startActivity(intent);*/
                                            Intent intent = new Intent(CreateBucketlistActivty.this, Home.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.putExtra("redirect", "profile");
                                            intent.putExtra("redirectbucket", "2");
                                            startActivity(intent);

//                                            followerlistid.clear();

                                        }
                                    })
                                    .show();
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }


    /* create_bucket_group API */
    private void create_bucket_group() {

        if (Utility.isConnected(CreateBucketlistActivty.this)) {
            int post_type = 3;

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(getApplicationContext(), "userId");

//            String username= grpFrmfname.concat(" ").concat(grpFrmlname);
                String groupname = grpname;

             //   JsonArray jsonElements = (JsonArray) new Gson().toJsonTree(selectedgrplist);

                String json = new Gson().toJson(selectedgrplist);


                call = apiService.create_bucket_group("create_bucket_group",
                        Integer.parseInt(reviewid),
                        Integer.parseInt(hotelid),
                        timelineId,
                        groupname,
                        Integer.parseInt(createuserid),
                        json,
                        post_type);

                Log.e(TAG, "create_bucket_group: hotelid--"+Integer.parseInt(reviewid) );
                Log.e(TAG, "create_bucket_group: hotelid--"+Integer.parseInt(hotelid) );
                Log.e(TAG, "create_bucket_group: hotelid--"+timelineId );
                Log.e(TAG, "create_bucket_group: hotelid--"+groupname );
                Log.e(TAG, "create_bucket_group: hotelid--"+Integer.parseInt(createuserid) );
                Log.e(TAG, "create_bucket_group: jsonElements--"+json );
                Log.e(TAG, "create_bucket_group: hotelid--"+post_type );



                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1&& response.body().getResponseMessage().equalsIgnoreCase("success")) {

                            new SweetAlertDialog(CreateBucketlistActivty.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!!")
                                    .setContentText("Added to your group's bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                           /* Intent intent = new Intent(CreateBucketlistActivty.this, MyBucketlistActivity.class);
                                            intent.putExtra("redirect", "3");
                                            startActivity(intent);*/
                                            Intent intent1=new Intent(CreateBucketlistActivty.this, Home.class);
                                            intent1.putExtra("redirect","profile");
                                            startActivity(intent1); finish();


                                        }
                                    })
                                    .show();

                        } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("alreadyexists")) {

                            new SweetAlertDialog(CreateBucketlistActivty.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Alert!!")
                                    .setContentText("Already added to your group's bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                           /* Intent intent = new Intent(CreateBucketlistActivty.this, MyBucketlistActivity.class);
                                            intent.putExtra("redirect", "3");
                                            startActivity(intent);*/
                                            Intent intent1=new Intent(CreateBucketlistActivty.this, Home.class);
                                            intent1.putExtra("redirect","profile");
                                            startActivity(intent1);  finish();
                                        }
                                    })
                                    .show();
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    /*Group and individual list creating api calling*/
    private void create_bucket_group_both() {

        if (Utility.isConnected(CreateBucketlistActivty.this)) {
            int post_type = 3;

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(getApplicationContext(), "userId");

//            String username= grpFrmfname.concat(" ").concat(grpFrmlname);
                String groupname = grpname;
               // JsonArray jsonElements = (JsonArray) new Gson().toJsonTree(selectedgrplist);
                String json = new Gson().toJson(selectedgrplist);

                call = apiService.create_bucket_group("create_bucket_group",
                        Integer.parseInt(reviewid),
                        Integer.parseInt(hotelid),
                        timelineId,
                        groupname,
                        Integer.parseInt(createuserid),
                        json,
                        post_type);

                Log.e(TAG, "create_bucket_group: jsonElements--"+json );

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1&& response.body().getResponseMessage().equalsIgnoreCase("success")) {

/*
                            new SweetAlertDialog(CreateBucketlistActivty.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!!")
                                    .setContentText("Added to your group's bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();


                                            create_bucket_individual_user();



                                        }
                                    })
                                    .show();
*/
                            create_bucket_individual_user();


                        }else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("alreadyexists")) {

                            new SweetAlertDialog(CreateBucketlistActivty.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Alert!!")
                                    .setContentText("Already added to your group's bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            /*Individual user API call*/
                                            create_bucket_individual_user();


                                        }
                                    })
                                    .show();
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    /*Self bucket list creating api calling*/
    private void create_bucket_myself() {

        if (Utility.isConnected(CreateBucketlistActivty.this)) {
            int post_type = 1;

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {
                Log.e(TAG, "create_bucket_individual_user: hotelid--"+hotelid );
                Log.e(TAG, "create_bucket_individual_user: reviewId--"+reviewid );
                Log.e(TAG, "create_bucket_individual_user: timelineId--"+timelineId );
                Log.e(TAG, "create_bucket_individual_user: followerlistid--"+followerlistid );


                String createuserid = Pref_storage.getDetail(getApplicationContext(), "userId");
                String username = fname.concat(" ").concat(lname);
                Log.e(TAG, "create_bucket_individual_user: username--"+username );
                Log.e(TAG, "create_bucket_individual_user: createuserid--"+createuserid );
                Log.e(TAG, "create_bucket_individual_user: post_type--"+post_type );

                call = apiService.create_bucket_myself("create_bucket_myself",
                        Integer.parseInt(hotelid),
                        Integer.parseInt(reviewid),
                        timelineId,
                        username,
                        Integer.parseInt(createuserid),
                        post_type);

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("success")) {
                            new SweetAlertDialog(CreateBucketlistActivty.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!!")
                                    .setContentText("Added to your bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            if (selectedgrplist.isEmpty() && followerlistid.isEmpty()) {
                                                /*Intent intent = new Intent(CreateBucketlistActivty.this, MyBucketlistActivity.class);
                                                intent.putExtra("redirect", "1");
                                                startActivity(intent);*/
                                                Intent intent = new Intent(CreateBucketlistActivty.this, Home.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                intent.putExtra("redirect", "profile");
                                                intent.putExtra("redirectbucket", "1");
                                                startActivity(intent);

                                            } else {
                                                if (!selectedgrplist.isEmpty() && !followerlistid.isEmpty()) {
                                                    /*Group and individual list creating api calling*/
                                                    create_bucket_group_both();

                                                } else if (!followerlistid.isEmpty() && selectedgrplist.isEmpty()) {
                                                    /* create_bucket_individual_user API */
                                                    create_bucket_individual_user();
                                                } else if (followerlistid.isEmpty() && !selectedgrplist.isEmpty()) {
                                                    /* create_bucket_group API */
                                                    create_bucket_group();
                                                }
                                            }


                                        }
                                    })
                                    .show();

                        } else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("alreadyexists")) {
                            new SweetAlertDialog(CreateBucketlistActivty.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Alert!!")
                                    .setContentText("Already added to your bucket list!!")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                            if (selectedgrplist.isEmpty() && followerlistid.isEmpty()) {
                                                /*Intent intent = new Intent(CreateBucketlistActivty.this, MyBucketlistActivity.class);
                                                intent.putExtra("redirect", "1");
                                                startActivity(intent);*/
                                                Intent intent1=new Intent(CreateBucketlistActivty.this, Home.class);
                                                intent1.putExtra("redirect","profile");
                                                startActivity(intent1); finish();
                                            } else {
                                                if (!selectedgrplist.isEmpty() && !followerlistid.isEmpty()) {
                                                    /*Group and individual list creating api calling*/

                                                    create_bucket_group_both();
                                                } else if (!followerlistid.isEmpty()&& selectedgrplist.isEmpty()) {
                                                    /*individual list creating api calling*/

                                                    create_bucket_individual_user();
                                                } else if (followerlistid.isEmpty() && !selectedgrplist.isEmpty()) {

                                                    /*Group  list creating api calling*/

                                                    create_bucket_group();
                                                }
                                            }


                                        }
                                    })
                                    .show();
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    /*Self bucket list creating API*/
    private void create_bucket_myself_without_group_and_individual() {

        if (Utility.isConnected(CreateBucketlistActivty.this)) {
            int post_type = 1;

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(getApplicationContext(), "userId");

                Log.e(TAG, "create_bucket_individual_user: hotelid--"+hotelid );
                Log.e(TAG, "create_bucket_individual_user: reviewId--"+reviewid );
                Log.e(TAG, "create_bucket_individual_user: timelineId--"+timelineId );
                Log.e(TAG, "create_bucket_individual_user: followerlistid--"+followerlistid );

                String username = fname.concat(" ").concat(lname);

                call = apiService.create_bucket_myself("create_bucket_myself",
                        Integer.parseInt(hotelid),
                        Integer.parseInt(reviewid),
                        timelineId,
                        username,
                        Integer.parseInt(createuserid),
                        post_type);

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1&& response.body().getResponseMessage().equalsIgnoreCase("success")) {
                            new SweetAlertDialog(CreateBucketlistActivty.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!!")
                                    .setContentText("Added to your bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                            if (selectedgrplist.isEmpty() && followerlistid.isEmpty()) {
                                                /*Intent intent = new Intent(CreateBucketlistActivty.this, MyBucketlistActivity.class);
                                                intent.putExtra("redirect", "1");
                                                startActivity(intent);*/
                                                Intent intent1=new Intent(CreateBucketlistActivty.this, Home.class);
                                                intent1.putExtra("redirect","profile");
                                                startActivity(intent1); finish();
                                            } else {
                                                if (!selectedgrplist.isEmpty()&& !followerlistid.isEmpty()) {
                                                    /*Group and individual list creating api calling*/

                                                    create_bucket_group_both();
                                                } else if (!followerlistid.isEmpty() && selectedgrplist.isEmpty()) {

                                                    /* create_bucket_individual_user API */
                                                    create_bucket_individual_user();
                                                } else if (followerlistid.isEmpty()&& !selectedgrplist.isEmpty()) {
                                                    /* create_bucket_group API */

                                                    create_bucket_group();
                                                }
                                            }

                                        }
                                    })
                                    .show();


                        }else if (response.body().getResponseCode() == 1 && response.body().getResponseMessage().equalsIgnoreCase("alreadyexists")) {
                            new SweetAlertDialog(CreateBucketlistActivty.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Alert!!")
                                    .setContentText("Already added to your bucket list!!")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                            if (selectedgrplist.isEmpty() && followerlistid.isEmpty()) {
                                               /* Intent intent = new Intent(CreateBucketlistActivty.this, MyBucketlistActivity.class);
                                                intent.putExtra("redirect", "1");
                                                startActivity(intent);*/
                                                Intent intent1=new Intent(CreateBucketlistActivty.this, Home.class);
                                                intent1.putExtra("redirect","profile");
                                                startActivity(intent1);
                                                finish();
                                            } else {
                                                if (!selectedgrplist.isEmpty() && !followerlistid.isEmpty()) {

                                                    /*Group and individual list creating api calling*/
                                                    create_bucket_group_both();
                                                } else if (!followerlistid.isEmpty() && selectedgrplist.isEmpty()) {
                                                    /*Individual list creating api calling*/

                                                    create_bucket_individual_user();
                                                } else if (followerlistid.isEmpty() && !selectedgrplist.isEmpty()) {

                                                    /*Group list creating api calling*/
                                                    create_bucket_group();
                                                }
                                            }


                                        }
                                    })
                                    .show();
                        }


                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
