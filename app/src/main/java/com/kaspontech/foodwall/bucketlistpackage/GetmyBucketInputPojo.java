package com.kaspontech.foodwall.bucketlistpackage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kaspontech.foodwall.modelclasses.Review_pojo.AmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.PackImage;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;

import java.util.List;


public class GetmyBucketInputPojo {


	@SerializedName("bucket_id")
	@Expose
	private String bucketId;
	@SerializedName("buck_description")
	@Expose
	private String buckDescription;
	@SerializedName("reviewid")
	@Expose
	private String reviewid;
	@SerializedName("post_type")
	@Expose
	private String postType;
	@SerializedName("individual_userid")
	@Expose
	private String individualUserid;
	@SerializedName("group_id")
	@Expose
	private String groupId;
	@SerializedName("bucket_created_by")
	@Expose
	private String bucketCreatedBy;
	@SerializedName("bucket_created_on")
	@Expose
	private String bucketCreatedOn;
	@SerializedName("user_id")
	@Expose
	private String userId;
	@SerializedName("first_name")
	@Expose
	private String firstName;
	@SerializedName("last_name")
	@Expose
	private String lastName;
	@SerializedName("email")
	@Expose
	private String email;
	@SerializedName("cont_no")
	@Expose
	private String contNo;
	@SerializedName("gender")
	@Expose
	private String gender;
	@SerializedName("dob")
	@Expose
	private String dob;
	@SerializedName("picture")
	@Expose
	private String picture;
	@SerializedName("inv_user_id")
	@Expose
	private String invUserId;
	@SerializedName("inv_first_name")
	@Expose
	private String invFirstName;
	@SerializedName("inv_last_name")
	@Expose
	private String invLastName;
	@SerializedName("inv_email")
	@Expose
	private String invEmail;
	@SerializedName("inv_cont_no")
	@Expose
	private String invContNo;
	@SerializedName("inv_gender")
	@Expose
	private String invGender;
	@SerializedName("inv_dob")
	@Expose
	private String invDob;
	@SerializedName("inv_picture")
	@Expose
	private String invPicture;
	@SerializedName("google_id")
	@Expose
	private String googleId;
	@SerializedName("hotel_name")
	@Expose
	private String hotelName;
	@SerializedName("place_id")
	@Expose
	private String placeId;
	@SerializedName("open_times")
	@Expose
	private String openTimes;
	@SerializedName("address")
	@Expose
	private String address;
	@SerializedName("latitude")
	@Expose
	private String latitude;
	@SerializedName("longitude")
	@Expose
	private String longitude;
	@SerializedName("phone")
	@Expose
	private String phone;
	@SerializedName("photo_reference")
	@Expose
	private String photoReference;
	@SerializedName("hotel_id")
	@Expose
	private String hotelId;
	@SerializedName("hotel_review")
	@Expose
	private String hotelReview;
	@SerializedName("total_likes")
	@Expose
	private String totalLikes;
	@SerializedName("total_comments")
	@Expose
	private String totalComments;
	@SerializedName("category_type")
	@Expose
	private String categoryType;
	@SerializedName("veg_nonveg")
	@Expose
	private String vegNonveg;
	@SerializedName("food_exprience")
	@Expose
	private String foodExprience;
	@SerializedName("ambiance")
	@Expose
	private String ambiance;
	@SerializedName("taste")
	@Expose
	private String taste;
	@SerializedName("service")
	@Expose
	private String service;
	@SerializedName("package")
	@Expose
	private String _package;
	@SerializedName("timedelivery")
	@Expose
	private String timedelivery;
	@SerializedName("delivery_mode")
	@Expose
	private String deliveryMode;
	@SerializedName("value_money")
	@Expose
	private String valueMoney;
	@SerializedName("created_by")
	@Expose
	private String createdBy;
	@SerializedName("created_on")
	@Expose
	private String createdOn;
	@SerializedName("rev_user_id")
	@Expose
	private String revUserId;
	@SerializedName("rev_first_name")
	@Expose
	private String revFirstName;
	@SerializedName("rev_last_name")
	@Expose
	private String revLastName;
	@SerializedName("rev_email")
	@Expose
	private String revEmail;
	@SerializedName("rev_cont_no")
	@Expose
	private String revContNo;
	@SerializedName("rev_gender")
	@Expose
	private String revGender;
	@SerializedName("rev_dob")
	@Expose
	private String revDob;
	@SerializedName("total_followers")
	@Expose
	private String totalFollowers;
	@SerializedName("total_followings")
	@Expose
	private String totalFollowings;
	@SerializedName("rev_picture")
	@Expose
	private String revPicture;
	@SerializedName("to_user_id")
	@Expose
	private String toUserId;
	@SerializedName("to_first_name")
	@Expose
	private String toFirstName;
	@SerializedName("to_last_name")
	@Expose
	private String toLastName;
	@SerializedName("to_email")
	@Expose
	private String toEmail;
	@SerializedName("to_cont_no")
	@Expose
	private String toContNo;
	@SerializedName("to_gender")
	@Expose
	private String toGender;
	@SerializedName("to_dob")
	@Expose
	private String toDob;
	@SerializedName("to_picture")
	@Expose
	private String toPicture;
	@SerializedName("total_inv_buckets")
	@Expose
	private String totalInvBuckets;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public GetmyBucketInputPojo() {
	}

	/**
	 *
	 * @param toLastName
	 * @param phone
	 * @param invGender
	 * @param invDob
	 * @param bucketId
	 * @param totalFollowers
	 * @param hotelReview
	 * @param ambiance
	 * @param taste
	 * @param photoReference
	 * @param revLastName
	 * @param hotelName
	 * @param placeId
	 * @param userId
	 * @param gender
	 * @param longitude
	 * @param revPicture
	 * @param invEmail
	 * @param lastName
	 * @param _package
	 * @param toPicture
	 * @param categoryType
	 * @param deliveryMode
	 * @param toGender
	 * @param revGender
	 * @param valueMoney
	 * @param googleId
	 * @param toDob
	 * @param createdOn
	 * @param picture
	 * @param createdBy
	 * @param email
	 * @param invContNo
	 * @param totalFollowings
	 * @param individualUserid
	 * @param latitude
	 * @param revUserId
	 * @param bucketCreatedBy
	 * @param toEmail
	 * @param totalComments
	 * @param vegNonveg
	 * @param totalInvBuckets
	 * @param toContNo
	 * @param hotelId
	 * @param bucketCreatedOn
	 * @param openTimes
	 * @param revEmail
	 * @param contNo
	 * @param timedelivery
	 * @param firstName
	 * @param invPicture
	 * @param buckDescription
	 * @param revContNo
	 * @param revDob
	 * @param postType
	 * @param revFirstName
	 * @param invUserId
	 * @param toFirstName
	 * @param reviewid
	 * @param invLastName
	 * @param groupId
	 * @param totalLikes
	 * @param toUserId
	 * @param address
	 * @param dob
	 * @param service
	 * @param invFirstName
	 * @param foodExprience
	 */
	public GetmyBucketInputPojo(String bucketId, String buckDescription, String reviewid, String postType, String individualUserid, String groupId, String bucketCreatedBy, String bucketCreatedOn, String userId, String firstName, String lastName, String email, String contNo, String gender, String dob, String picture, String invUserId, String invFirstName, String invLastName, String invEmail, String invContNo, String invGender, String invDob, String invPicture, String googleId, String hotelName, String placeId, String openTimes, String address, String latitude, String longitude, String phone, String photoReference, String hotelId, String hotelReview, String totalLikes, String totalComments, String categoryType, String vegNonveg, String foodExprience, String ambiance, String taste, String service, String _package, String timedelivery, String deliveryMode, String valueMoney, String createdBy, String createdOn, String revUserId, String revFirstName, String revLastName, String revEmail, String revContNo, String revGender, String revDob, String totalFollowers, String totalFollowings, String revPicture, String toUserId, String toFirstName, String toLastName, String toEmail, String toContNo, String toGender, String toDob, String toPicture, String totalInvBuckets) {
		super();
		this.bucketId = bucketId;
		this.buckDescription = buckDescription;
		this.reviewid = reviewid;
		this.postType = postType;
		this.individualUserid = individualUserid;
		this.groupId = groupId;
		this.bucketCreatedBy = bucketCreatedBy;
		this.bucketCreatedOn = bucketCreatedOn;
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.contNo = contNo;
		this.gender = gender;
		this.dob = dob;
		this.picture = picture;
		this.invUserId = invUserId;
		this.invFirstName = invFirstName;
		this.invLastName = invLastName;
		this.invEmail = invEmail;
		this.invContNo = invContNo;
		this.invGender = invGender;
		this.invDob = invDob;
		this.invPicture = invPicture;
		this.googleId = googleId;
		this.hotelName = hotelName;
		this.placeId = placeId;
		this.openTimes = openTimes;
		this.address = address;
		this.latitude = latitude;
		this.longitude = longitude;
		this.phone = phone;
		this.photoReference = photoReference;
		this.hotelId = hotelId;
		this.hotelReview = hotelReview;
		this.totalLikes = totalLikes;
		this.totalComments = totalComments;
		this.categoryType = categoryType;
		this.vegNonveg = vegNonveg;
		this.foodExprience = foodExprience;
		this.ambiance = ambiance;
		this.taste = taste;
		this.service = service;
		this._package = _package;
		this.timedelivery = timedelivery;
		this.deliveryMode = deliveryMode;
		this.valueMoney = valueMoney;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.revUserId = revUserId;
		this.revFirstName = revFirstName;
		this.revLastName = revLastName;
		this.revEmail = revEmail;
		this.revContNo = revContNo;
		this.revGender = revGender;
		this.revDob = revDob;
		this.totalFollowers = totalFollowers;
		this.totalFollowings = totalFollowings;
		this.revPicture = revPicture;
		this.toUserId = toUserId;
		this.toFirstName = toFirstName;
		this.toLastName = toLastName;
		this.toEmail = toEmail;
		this.toContNo = toContNo;
		this.toGender = toGender;
		this.toDob = toDob;
		this.toPicture = toPicture;
		this.totalInvBuckets = totalInvBuckets;
	}

	public String getBucketId() {
		return bucketId;
	}

	public void setBucketId(String bucketId) {
		this.bucketId = bucketId;
	}

	public String getBuckDescription() {
		return buckDescription;
	}

	public void setBuckDescription(String buckDescription) {
		this.buckDescription = buckDescription;
	}

	public String getReviewid() {
		return reviewid;
	}

	public void setReviewid(String reviewid) {
		this.reviewid = reviewid;
	}

	public String getPostType() {
		return postType;
	}

	public void setPostType(String postType) {
		this.postType = postType;
	}

	public String getIndividualUserid() {
		return individualUserid;
	}

	public void setIndividualUserid(String individualUserid) {
		this.individualUserid = individualUserid;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getBucketCreatedBy() {
		return bucketCreatedBy;
	}

	public void setBucketCreatedBy(String bucketCreatedBy) {
		this.bucketCreatedBy = bucketCreatedBy;
	}

	public String getBucketCreatedOn() {
		return bucketCreatedOn;
	}

	public void setBucketCreatedOn(String bucketCreatedOn) {
		this.bucketCreatedOn = bucketCreatedOn;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getInvUserId() {
		return invUserId;
	}

	public void setInvUserId(String invUserId) {
		this.invUserId = invUserId;
	}

	public String getInvFirstName() {
		return invFirstName;
	}

	public void setInvFirstName(String invFirstName) {
		this.invFirstName = invFirstName;
	}

	public String getInvLastName() {
		return invLastName;
	}

	public void setInvLastName(String invLastName) {
		this.invLastName = invLastName;
	}

	public String getInvEmail() {
		return invEmail;
	}

	public void setInvEmail(String invEmail) {
		this.invEmail = invEmail;
	}

	public String getInvContNo() {
		return invContNo;
	}

	public void setInvContNo(String invContNo) {
		this.invContNo = invContNo;
	}

	public String getInvGender() {
		return invGender;
	}

	public void setInvGender(String invGender) {
		this.invGender = invGender;
	}

	public String getInvDob() {
		return invDob;
	}

	public void setInvDob(String invDob) {
		this.invDob = invDob;
	}

	public String getInvPicture() {
		return invPicture;
	}

	public void setInvPicture(String invPicture) {
		this.invPicture = invPicture;
	}

	public String getGoogleId() {
		return googleId;
	}

	public void setGoogleId(String googleId) {
		this.googleId = googleId;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	public String getOpenTimes() {
		return openTimes;
	}

	public void setOpenTimes(String openTimes) {
		this.openTimes = openTimes;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhotoReference() {
		return photoReference;
	}

	public void setPhotoReference(String photoReference) {
		this.photoReference = photoReference;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getHotelReview() {
		return hotelReview;
	}

	public void setHotelReview(String hotelReview) {
		this.hotelReview = hotelReview;
	}

	public String getTotalLikes() {
		return totalLikes;
	}

	public void setTotalLikes(String totalLikes) {
		this.totalLikes = totalLikes;
	}

	public String getTotalComments() {
		return totalComments;
	}

	public void setTotalComments(String totalComments) {
		this.totalComments = totalComments;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public String getVegNonveg() {
		return vegNonveg;
	}

	public void setVegNonveg(String vegNonveg) {
		this.vegNonveg = vegNonveg;
	}

	public String getFoodExprience() {
		return foodExprience;
	}

	public void setFoodExprience(String foodExprience) {
		this.foodExprience = foodExprience;
	}

	public String getAmbiance() {
		return ambiance;
	}

	public void setAmbiance(String ambiance) {
		this.ambiance = ambiance;
	}

	public String getTaste() {
		return taste;
	}

	public void setTaste(String taste) {
		this.taste = taste;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getPackage() {
		return _package;
	}

	public void setPackage(String _package) {
		this._package = _package;
	}

	public String getTimedelivery() {
		return timedelivery;
	}

	public void setTimedelivery(String timedelivery) {
		this.timedelivery = timedelivery;
	}

	public String getDeliveryMode() {
		return deliveryMode;
	}

	public void setDeliveryMode(String deliveryMode) {
		this.deliveryMode = deliveryMode;
	}

	public String getValueMoney() {
		return valueMoney;
	}

	public void setValueMoney(String valueMoney) {
		this.valueMoney = valueMoney;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getRevUserId() {
		return revUserId;
	}

	public void setRevUserId(String revUserId) {
		this.revUserId = revUserId;
	}

	public String getRevFirstName() {
		return revFirstName;
	}

	public void setRevFirstName(String revFirstName) {
		this.revFirstName = revFirstName;
	}

	public String getRevLastName() {
		return revLastName;
	}

	public void setRevLastName(String revLastName) {
		this.revLastName = revLastName;
	}

	public String getRevEmail() {
		return revEmail;
	}

	public void setRevEmail(String revEmail) {
		this.revEmail = revEmail;
	}

	public String getRevContNo() {
		return revContNo;
	}

	public void setRevContNo(String revContNo) {
		this.revContNo = revContNo;
	}

	public String getRevGender() {
		return revGender;
	}

	public void setRevGender(String revGender) {
		this.revGender = revGender;
	}

	public String getRevDob() {
		return revDob;
	}

	public void setRevDob(String revDob) {
		this.revDob = revDob;
	}

	public String getTotalFollowers() {
		return totalFollowers;
	}

	public void setTotalFollowers(String totalFollowers) {
		this.totalFollowers = totalFollowers;
	}

	public String getTotalFollowings() {
		return totalFollowings;
	}

	public void setTotalFollowings(String totalFollowings) {
		this.totalFollowings = totalFollowings;
	}

	public String getRevPicture() {
		return revPicture;
	}

	public void setRevPicture(String revPicture) {
		this.revPicture = revPicture;
	}

	public String getToUserId() {
		return toUserId;
	}

	public void setToUserId(String toUserId) {
		this.toUserId = toUserId;
	}

	public String getToFirstName() {
		return toFirstName;
	}

	public void setToFirstName(String toFirstName) {
		this.toFirstName = toFirstName;
	}

	public String getToLastName() {
		return toLastName;
	}

	public void setToLastName(String toLastName) {
		this.toLastName = toLastName;
	}

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	public String getToContNo() {
		return toContNo;
	}

	public void setToContNo(String toContNo) {
		this.toContNo = toContNo;
	}

	public String getToGender() {
		return toGender;
	}

	public void setToGender(String toGender) {
		this.toGender = toGender;
	}

	public String getToDob() {
		return toDob;
	}

	public void setToDob(String toDob) {
		this.toDob = toDob;
	}

	public String getToPicture() {
		return toPicture;
	}

	public void setToPicture(String toPicture) {
		this.toPicture = toPicture;
	}

	public String getTotalInvBuckets() {
		return totalInvBuckets;
	}

	public void setTotalInvBuckets(String totalInvBuckets) {
		this.totalInvBuckets = totalInvBuckets;
	}
}