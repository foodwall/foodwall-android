package com.kaspontech.foodwall.bucketlistpackage;

import android.content.Context;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.bucketadapter.GroupBucketlistAdapter;
import com.kaspontech.foodwall.adapters.bucketadapter.bucketmyselfAdapter;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.modelclasses.Review_pojo.ReviewCommentsPojo;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Groupfragment extends Fragment implements onCommentsPostListener {

    /**
     * Context
     **/
    Context context;
    /**
     * View
     **/
    View view;
    /**
     * No data textview
     **/
    TextView llNoPost;
    /**
     * Page background for no data available
     **/
    RelativeLayout pageBackground;
    /**
     * Group bucket recyclerview
     **/
    RecyclerView myBucketRecyclerview;

    /**
     * Group Bucket adapter
     **/
    GroupBucketlistAdapter bucketMyselfAdapter;
    /**
     * Group bucket arraylist
     **/
    List<GroupBucketList_Datum> getmyBucketInputPojoArrayList = new ArrayList<>();


    public Groupfragment() {
        // Required empty public constructor
    }


    ProgressBar progressbar_review;
    ProgressBar progressbar, comments_review;

    EmojiconEditText review_comment;

    TextView ll_no_post, tv_view_all_comments;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mybucket_layout, container, false);
        context = view.getContext();

        /*Widget initialization*/

        /*
          Page background for no data available
         */
        pageBackground = view.findViewById(R.id.page_background);

        /*Group bucket recyclerview*/
        myBucketRecyclerview = view.findViewById(R.id.my_bucket_recyclerview);

        /*No post textview*/

        llNoPost = view.findViewById(R.id.ll_no_post);
        progressbar_review = view.findViewById(R.id.progressbar_review);

        /* Calling api to get group bucket list*/
        getBucketGroup();

        return view;
    }

    /* Calling api to get group bucket list*/

    private void getBucketGroup() {

        /*Checking mobile connectivity*/
        if (Utility.isConnected(context)) {
            progressbar_review.setVisibility(View.VISIBLE);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Call<GroupBucketList_Response> call = apiService.get_bucket_group("get_bucket_group", userid);

                call.enqueue(new Callback<GroupBucketList_Response>() {
                    @Override
                    public void onResponse(@NonNull Call<GroupBucketList_Response> call, @NonNull Response<GroupBucketList_Response> response) {

                        String responseStatus = response.body().getResponseMessage();


                        if (responseStatus.equalsIgnoreCase("success")) {

                            llNoPost.setVisibility(View.GONE);
                            getmyBucketInputPojoArrayList.clear();
                            //Success

                            Log.e("responseStatus_oncreate", "responseSize->" + response.body().getData().size());
                            pageBackground.setBackgroundColor(context.getResources().getColor(R.color.page_background));


                            getmyBucketInputPojoArrayList = response.body().getData();
                            progressbar_review.setVisibility(View.GONE);


                            bucketMyselfAdapter = new GroupBucketlistAdapter(context, getmyBucketInputPojoArrayList, Groupfragment.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                            myBucketRecyclerview.setLayoutManager(mLayoutManager);
                            myBucketRecyclerview.setItemAnimator(new DefaultItemAnimator());
                            myBucketRecyclerview.setAdapter(bucketMyselfAdapter);
                            myBucketRecyclerview.hasFixedSize();
                            bucketMyselfAdapter.notifyDataSetChanged();
                            myBucketRecyclerview.setVisibility(View.VISIBLE);


                        } else if (responseStatus.equals("nodata")) {

                            llNoPost.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GroupBucketList_Response> call, @NonNull Throwable t) {
                        progressbar_review.setVisibility(View.GONE);

                        if (getmyBucketInputPojoArrayList.isEmpty()) {

                            llNoPost.setVisibility(View.VISIBLE);

                        }
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();

        }


    }

    /* Calling api to get group bucket list*/

    private void getBucketGroupRefresh() {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Call<GroupBucketList_Response> call = apiService.get_bucket_group("get_bucket_group", userid);

                call.enqueue(new Callback<GroupBucketList_Response>() {
                    @Override
                    public void onResponse(@NonNull Call<GroupBucketList_Response> call, @NonNull Response<GroupBucketList_Response> response) {

                        String responseStatus = response.body().getResponseMessage();


                        if (responseStatus.equalsIgnoreCase("success")) {
                            llNoPost.setVisibility(View.GONE);

                            getmyBucketInputPojoArrayList.clear();
                            //Success

                            pageBackground.setBackgroundColor(context.getResources().getColor(R.color.page_background));


                            getmyBucketInputPojoArrayList = response.body().getData();


                            bucketMyselfAdapter = new GroupBucketlistAdapter(context, getmyBucketInputPojoArrayList, Groupfragment.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                            myBucketRecyclerview.setLayoutManager(mLayoutManager);
                            myBucketRecyclerview.setItemAnimator(new DefaultItemAnimator());
                            myBucketRecyclerview.setAdapter(bucketMyselfAdapter);
                            myBucketRecyclerview.setVisibility(View.VISIBLE);
                            myBucketRecyclerview.hasFixedSize();
                            bucketMyselfAdapter.notifyDataSetChanged();

                        } else if (responseStatus.equals("nodata")) {

                            llNoPost.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GroupBucketList_Response> call, @NonNull Throwable t) {
                        llNoPost.setVisibility(View.VISIBLE);
                        myBucketRecyclerview.setVisibility(View.GONE);
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();

            }

        } else {

            Toast.makeText(context, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }


    }


    @Override
    public void onPostCommitedView(View view, int adapterPosition, View hotelDetailsView) {


        switch (view.getId()) {

            case R.id.txt_adapter_post:

                review_comment = (EmojiconEditText) hotelDetailsView.findViewById(R.id.review_comment);
                comments_review = (ProgressBar) hotelDetailsView.findViewById(R.id.comments_review);
                tv_view_all_comments = (TextView) hotelDetailsView.findViewById(R.id.tv_view_all_comments);

                if (review_comment.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();
                } else {

                    Utility.hideKeyboard(view);
                    createEditReviewComments(getmyBucketInputPojoArrayList.get(adapterPosition).getHotelId(),
                            review_comment.getText().toString(),
                            getmyBucketInputPojoArrayList.get(adapterPosition).getReviewid());

                }
                break;

        }


    }

    private void createEditReviewComments(String hotelId, String toString, String getReviewId) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            comments_review.setVisibility(View.VISIBLE);
            comments_review.setIndeterminate(true);

            String createuserid = Pref_storage.getDetail(getActivity(), "userId");

            Call<ReviewCommentsPojo> call = apiService.get_create_edit_hotel_review_comments("create_edit_hotel_review_comments", Integer.parseInt(hotelId),
                    0, Integer.parseInt(createuserid), toString, Integer.parseInt(getReviewId));
            call.enqueue(new Callback<ReviewCommentsPojo>() {
                @Override
                public void onResponse(Call<ReviewCommentsPojo> call, Response<ReviewCommentsPojo> response) {

                    review_comment.setText("");

                    if (response.code() == 500) {
                        comments_review.setVisibility(View.GONE);
                        comments_review.setIndeterminate(false);

                        Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();

                    } else {
                        comments_review.setVisibility(View.GONE);
                        comments_review.setIndeterminate(false);

                        if (response.body() != null) {

                            if (Objects.requireNonNull(response.body()).getResponseMessage().equals("success")) {

                                if (response.body().getData().size() > 0) {

                                    if (tv_view_all_comments.getVisibility() == View.GONE) {

                                        tv_view_all_comments.setVisibility(View.VISIBLE);
                                        //String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                        String comments = getString(R.string.view_one_comment);
                                        tv_view_all_comments.setText(comments);

                                    } else {

                                        String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                        tv_view_all_comments.setText(comments);
                                    }


                                } else {

                                }

                            } else {

                            }
                        }

                    }

                }

                @Override
                public void onFailure(Call<ReviewCommentsPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                    comments_review.setVisibility(View.GONE);
                    comments_review.setIndeterminate(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onResume() {
        super.onResume();


        if (!getmyBucketInputPojoArrayList.isEmpty()) {
            getBucketGroupRefresh();
            bucketMyselfAdapter.notifyDataSetChanged();
        } else {
         }
    }
}
