package com.kaspontech.foodwall.bucketlistpackage;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.adapters.bucketadapter.GroupBucketlist_userAdapter;
import com.kaspontech.foodwall.adapters.bucketadapter.IndividualBucketlist_userAdapter;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.BucketIndividualuser_Datum;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.BucketIndividualuser_Response;
import com.kaspontech.foodwall.modelclasses.Review_pojo.ReviewCommentsPojo;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.profilePackage._SwipeActivityClass;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Objects;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

@RequiresApi(api = Build.VERSION_CODES.M)
public class GroupbucketlistDetails extends _SwipeActivityClass implements View.OnClickListener,ScrollView.OnScrollChangeListener , onCommentsPostListener {

    String groupuserid;
    /**
     * Recyclerview declaration
     */
    RecyclerView ind_bucket_recyclerview;


    /**
     * Action bar
     **/
    Toolbar actionBar;

    /**
     * Tool bar title text view
     **/
    TextView toolbarTitle;

    /**
     * Back button press
     **/
    ImageButton back;

    /**
     * Progressbar
     *
     * @param savedInstanceState
     */
    ProgressBar progressbar;

    /**
     * Swiperefresh layout
     */
    SwipeRefreshLayout swiperefresh;
    /**
     * ScrollView
     */
    ScrollView scrollview;

    TextView ll_no_post;

    int pagecount = 1;

    Button page_loader;

    ProgressBar comments_review;

    EmojiconEditText review_comment;

    TextView tv_view_all_comments;


    GroupBucketlist_userAdapter individualBucketlist_userAdapter;
    ArrayList<BucketIndividualuser_Datum> bucketIndividualuserDatumList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groupbucketlist_details);


        groupuserid = getIntent().getStringExtra("groupuserid");

        ind_bucket_recyclerview = (RecyclerView) findViewById(R.id.ind_bucket_recyclerview);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        ll_no_post = (TextView) findViewById(R.id.ll_no_post);
        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        scrollview = (ScrollView) findViewById(R.id.scrollview);


        page_loader = (Button) findViewById(R.id.page_loader);
        actionBar = (Toolbar) findViewById(R.id.action_bar_bucket);

          /*
          Title text initialization
         */
        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(R.string.group_bucketlist);
        toolbarTitle.setGravity(View.TEXT_ALIGNMENT_CENTER);

        /*
         Back button press initialization
         */
        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);


        Pref_storage.setDetail(getApplicationContext(), "Pageno", String.valueOf(1));


        /* Calling my bucket list API */

        getGroupBucket();

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                /* Calling my bucket list API */

                getGroupBucketSwipe();

                scrollview.smoothScrollTo(0,0);
                swiperefresh.setRefreshing(false);

            }
        });

    }

    @Override
    public void onSwipeRight() {

    }

    @Override
    protected void onSwipeLeft() {
        finish();
    }

    private void getGroupBucket() {

        if (Utility.isConnected(this)) {
            progressbar.setVisibility(View.VISIBLE);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(this, "userId"));
                bucketIndividualuserDatumList.clear();

                Call<BucketIndividualuser_Response> call = apiService.get_bucket_group_inside("get_bucket_group_inside",
                        userid, Integer.parseInt(groupuserid), pagecount);

                call.enqueue(new Callback<BucketIndividualuser_Response>() {
                    @Override
                    public void onResponse(Call<BucketIndividualuser_Response> call, Response<BucketIndividualuser_Response> response) {


                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            ll_no_post.setVisibility(View.GONE);

                            //  pageBackground.setBackgroundColor(getResources().getColor(R.color.page_background));
                            if (response.body().getData().size() > 0) {
                                GroupbucketlistDetails.this.pagecount += 1;

//                            Log.e(TAG, "onResponse:page-->"+pagecount++ );

                                Pref_storage.setDetail(getApplicationContext(), "Pageno", String.valueOf(GroupbucketlistDetails.this.pagecount));

                             } else {

                            }


                            //Success

                            bucketIndividualuserDatumList = response.body().getData();


                            progressbar.setVisibility(View.GONE);

                            /*Setting adapter*/
                            individualBucketlist_userAdapter = new GroupBucketlist_userAdapter(getApplicationContext(), bucketIndividualuserDatumList,GroupbucketlistDetails.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            ind_bucket_recyclerview.setLayoutManager(mLayoutManager);
                            ind_bucket_recyclerview.setItemAnimator(new DefaultItemAnimator());
                            ind_bucket_recyclerview.setAdapter(individualBucketlist_userAdapter);
                            ind_bucket_recyclerview.hasFixedSize();
                            ind_bucket_recyclerview.setVisibility(View.VISIBLE);
                            ind_bucket_recyclerview.setNestedScrollingEnabled(false);
                            individualBucketlist_userAdapter.notifyDataSetChanged();

                        }
                    }

                    @Override
                    public void onFailure(Call<BucketIndividualuser_Response> call, Throwable t) {

                        progressbar.setVisibility(View.GONE);

                        if (bucketIndividualuserDatumList.isEmpty()) {
                            ll_no_post.setVisibility(View.VISIBLE);
                        }

                        //  pageBackground.setBackgroundColor(Color.WHITE);

                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();
                //  pageBackground.setBackgroundColor(Color.WHITE);


            }

        } else {

            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            //  pageBackground.setBackgroundColor(Color.WHITE);


        }
    }

    private void getGroupBucketpagination(int pagecount) {

        if (Utility.isConnected(this)) {
            progressbar.setVisibility(View.VISIBLE);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(this, "userId"));
                bucketIndividualuserDatumList.clear();

                Call<BucketIndividualuser_Response> call = apiService.get_bucket_group_inside("get_bucket_group_inside",
                        userid, Integer.parseInt(groupuserid), pagecount);

                call.enqueue(new Callback<BucketIndividualuser_Response>() {
                    @Override
                    public void onResponse(Call<BucketIndividualuser_Response> call, Response<BucketIndividualuser_Response> response) {


                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            ll_no_post.setVisibility(View.GONE);

                            //  pageBackground.setBackgroundColor(getResources().getColor(R.color.page_background));
                            if (response.body().getData().size() > 0) {
                                GroupbucketlistDetails.this.pagecount += 1;

//                            Log.e(TAG, "onResponse:page-->"+pagecount++ );

                                Pref_storage.setDetail(getApplicationContext(), "Pageno", String.valueOf(GroupbucketlistDetails.this.pagecount));

                            } else {

                            }


                            //Success

                            bucketIndividualuserDatumList = response.body().getData();


                            progressbar.setVisibility(View.GONE);

                            /* *//*Setting adapter*//*
                            individualBucketlist_userAdapter = new GroupBucketlist_userAdapter(getApplicationContext(), bucketIndividualuserDatumList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            ind_bucket_recyclerview.setLayoutManager(mLayoutManager);
                            ind_bucket_recyclerview.setItemAnimator(new DefaultItemAnimator());
                            ind_bucket_recyclerview.setAdapter(individualBucketlist_userAdapter);
                            ind_bucket_recyclerview.hasFixedSize();
                            ind_bucket_recyclerview.setVisibility(View.VISIBLE);
                            ind_bucket_recyclerview.setNestedScrollingEnabled(false);
*/
                            page_loader.setVisibility(View.GONE);

                            LinkedHashSet<BucketIndividualuser_Datum> mainSet = new LinkedHashSet<>(bucketIndividualuserDatumList);
                            bucketIndividualuserDatumList.clear();
                            bucketIndividualuserDatumList.addAll(mainSet);

                            individualBucketlist_userAdapter.notifyDataSetChanged();

                        }
                    }

                    @Override
                    public void onFailure(Call<BucketIndividualuser_Response> call, Throwable t) {

                        progressbar.setVisibility(View.GONE);

                        if (bucketIndividualuserDatumList.isEmpty()) {
                            ll_no_post.setVisibility(View.VISIBLE);
                        }

                        //  pageBackground.setBackgroundColor(Color.WHITE);

                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();
                //  pageBackground.setBackgroundColor(Color.WHITE);


            }

        } else {

            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            //  pageBackground.setBackgroundColor(Color.WHITE);


        }
    }

    private void getGroupBucketSwipe() {

        if (Utility.isConnected(this)) {
            progressbar.setVisibility(View.VISIBLE);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(this, "userId"));
                bucketIndividualuserDatumList.clear();

                Call<BucketIndividualuser_Response> call = apiService.get_bucket_group_inside("get_bucket_group_inside",
                        userid, Integer.parseInt(groupuserid), 1);

                call.enqueue(new Callback<BucketIndividualuser_Response>() {
                    @Override
                    public void onResponse(Call<BucketIndividualuser_Response> call, Response<BucketIndividualuser_Response> response) {


                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            ll_no_post.setVisibility(View.GONE);

                            //  pageBackground.setBackgroundColor(getResources().getColor(R.color.page_background));
                            if (response.body().getData().size() > 0) {
                                pagecount += 1;

//                            Log.e(TAG, "onResponse:page-->"+pagecount++ );

                                Pref_storage.setDetail(getApplicationContext(), "Pageno", String.valueOf(pagecount));

                            } else {

                            }


                            //Success

                            bucketIndividualuserDatumList = response.body().getData();


                            progressbar.setVisibility(View.GONE);

                            /*Setting adapter*/
                            individualBucketlist_userAdapter = new GroupBucketlist_userAdapter(getApplicationContext(), bucketIndividualuserDatumList,GroupbucketlistDetails.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            ind_bucket_recyclerview.setLayoutManager(mLayoutManager);
                            ind_bucket_recyclerview.setItemAnimator(new DefaultItemAnimator());
                            ind_bucket_recyclerview.setAdapter(individualBucketlist_userAdapter);
                            ind_bucket_recyclerview.hasFixedSize();
                            ind_bucket_recyclerview.setVisibility(View.VISIBLE);
                            ind_bucket_recyclerview.setNestedScrollingEnabled(false);
                            individualBucketlist_userAdapter.notifyDataSetChanged();

                        }
                    }

                    @Override
                    public void onFailure(Call<BucketIndividualuser_Response> call, Throwable t) {

                        progressbar.setVisibility(View.GONE);

                        if (bucketIndividualuserDatumList.isEmpty()) {
                            ll_no_post.setVisibility(View.VISIBLE);
                        }

                        //  pageBackground.setBackgroundColor(Color.WHITE);

                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();
                //  pageBackground.setBackgroundColor(Color.WHITE);


            }

        } else {

            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            //  pageBackground.setBackgroundColor(Color.WHITE);


        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();

        }
    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        View view = (View) scrollview.getChildAt(scrollview.getChildCount() - 1);
        int diff = (view.getBottom() - (scrollview.getHeight() + scrollview.getScrollY()));

        // if diff is zero, then the bottom has been reached


        Log.e("", "onScrollChange: PageCount Shared--> " + Pref_storage.getDetail(this, "Pageno"));

        if (diff == 0) {


            //scroll view is at bottom

            //  Toast.makeText(context, "Bottom reached", Toast.LENGTH_SHORT).show();


            if (pagecount == 1) {

            } else {


                if (Pref_storage.getDetail(this, "Pageno").isEmpty() || Pref_storage.getDetail(this, "Pageno") == null) {

                } else {
                    pagecount = Integer.parseInt(Pref_storage.getDetail(this, "Pageno"));

                    Log.e("", "onScrollChange: pagecount " + pagecount);

                     page_loader.setVisibility(View.VISIBLE);

                    getGroupBucketpagination(pagecount);

                }


            }


            // do stuff
        }

        Utility.hideKeyboard(v);
    }


    @Override
    public void onPostCommitedView(View view, int adapterPosition, View hotelDetailsView) {


        switch (view.getId()) {

            case R.id.txt_adapter_post:

                review_comment=(EmojiconEditText)hotelDetailsView.findViewById(R.id.review_comment);
                comments_review=(ProgressBar) hotelDetailsView.findViewById(R.id.comments_review);
                tv_view_all_comments=(TextView) hotelDetailsView.findViewById(R.id.tv_view_all_comments);

                if(review_comment.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();
                }else {

                    Utility.hideKeyboard(view);
                    createEditReviewComments(bucketIndividualuserDatumList.get(adapterPosition).getHotelId(),
                            review_comment.getText().toString(),
                            bucketIndividualuserDatumList.get(adapterPosition).getReviewid(),adapterPosition);

                }
                break;

        }


    }
    private void createEditReviewComments(String hotelId, String toString, String getReviewId, final int adpaterpos) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            comments_review.setVisibility(View.VISIBLE);
            comments_review.setIndeterminate(true);

            String createuserid = Pref_storage.getDetail(this, "userId");

            Call<ReviewCommentsPojo> call = apiService.get_create_edit_hotel_review_comments("create_edit_hotel_review_comments", Integer.parseInt(hotelId),
                    0, Integer.parseInt(createuserid), toString, Integer.parseInt(getReviewId));
            call.enqueue(new Callback<ReviewCommentsPojo>() {
                @Override
                public void onResponse(Call<ReviewCommentsPojo> call, Response<ReviewCommentsPojo> response) {

                    review_comment.setText("");

                    if (response.code() == 500) {
                        comments_review.setVisibility(View.GONE);
                        comments_review.setIndeterminate(false);

                        Toast.makeText(getApplicationContext(), "Internal server error", Toast.LENGTH_SHORT).show();

                    } else {
                        comments_review.setVisibility(View.GONE);
                        comments_review.setIndeterminate(false);

                        if (response.body() != null) {

                            if (Objects.requireNonNull(response.body()).getResponseMessage().equals("success")) {

                                if (response.body().getData().size() > 0) {

                                    if (tv_view_all_comments.getVisibility() == View.GONE) {

                                        tv_view_all_comments.setVisibility(View.VISIBLE);
                                        //String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                        String comments = getString(R.string.view_one_comment);
                                        tv_view_all_comments.setText(comments);

                                    } else {

                                        String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                        tv_view_all_comments.setText(comments);
                                    }

                                    Intent intent = new Intent(getApplicationContext(), Reviews_comments_all_activity.class);

                                    intent.putExtra("posttype", bucketIndividualuserDatumList.get(adpaterpos).getCategoryType());
                                    intent.putExtra("hotelid", bucketIndividualuserDatumList.get(adpaterpos).getHotelId());
                                    intent.putExtra("reviewid", bucketIndividualuserDatumList.get(adpaterpos).getReviewid());
                                    intent.putExtra("userid", bucketIndividualuserDatumList.get(adpaterpos).getUserId());

                                    intent.putExtra("hotelname", bucketIndividualuserDatumList.get(adpaterpos).getHotelName());
                                    intent.putExtra("overallrating", bucketIndividualuserDatumList.get(adpaterpos).getFoodExprience());

                                    if (bucketIndividualuserDatumList.get(adpaterpos).getCategoryType().equalsIgnoreCase("2")) {
                                        intent.putExtra("totaltimedelivery", bucketIndividualuserDatumList.get(adpaterpos).getTotalTimedelivery());
                                    } else {
                                        intent.putExtra("totaltimedelivery", bucketIndividualuserDatumList.get(adpaterpos).getAmbiance());
                                    }

                                    intent.putExtra("taste_count", bucketIndividualuserDatumList.get(adpaterpos).getTaste());
                                    intent.putExtra("vfm_rating", bucketIndividualuserDatumList.get(adpaterpos).getValueMoney());
                                    if (bucketIndividualuserDatumList.get(adpaterpos).getCategoryType().equalsIgnoreCase("2")) {
                                        intent.putExtra("package_rating", bucketIndividualuserDatumList.get(adpaterpos).getPackage());
                                    } else {
                                        intent.putExtra("package_rating", bucketIndividualuserDatumList.get(adpaterpos).getService());
                                    }

                                    intent.putExtra("hotelname", bucketIndividualuserDatumList.get(adpaterpos).getHotelName());
                                    intent.putExtra("reviewprofile", bucketIndividualuserDatumList.get(adpaterpos).getRevPicture());
                                    intent.putExtra("createdon", bucketIndividualuserDatumList.get(adpaterpos).getCreatedOn());
                                    intent.putExtra("firstname", bucketIndividualuserDatumList.get(adpaterpos).getRevFirstName());
                                    intent.putExtra("lastname", bucketIndividualuserDatumList.get(adpaterpos).getRevLastName());

                                    if (bucketIndividualuserDatumList.get(0).getCategoryType().equalsIgnoreCase("2")) {
                                        intent.putExtra("title_ambience", "Packaging");
                                    } else {
                                        intent.putExtra("title_ambience", "Hotel Ambience");
                                    }

                                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);

                                    startActivity(intent);
                                } else {

                                }

                            } else {

                            }
                        }

                    }

                }

                @Override
                public void onFailure(Call<ReviewCommentsPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                    comments_review.setVisibility(View.GONE);
                    comments_review.setIndeterminate(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
