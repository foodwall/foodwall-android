package com.kaspontech.foodwall.bucketlistpackage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kaspontech.foodwall.modelclasses.Review_pojo.AmbiImagePojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewAvoidDishPojo;
import com.kaspontech.foodwall.reviewpackage.Review_Updated_Pojo.ReviewTopDishPojo;

import java.util.List;

public class GroupBucketList_Datum {

    @SerializedName("bucket_id")
    @Expose
    private String bucketId;
    @SerializedName("buck_description")
    @Expose
    private String buckDescription;
    @SerializedName("reviewid")
    @Expose
    private String reviewid;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("individual_userid")
    @Expose
    private String individualUserid;
    @SerializedName("group_id")
    @Expose
    private String groupId;
    @SerializedName("group_name")
    @Expose
    private String groupName;
    @SerializedName("group_icon")
    @Expose
    private String groupIcon;
    @SerializedName("bucket_created_by")
    @Expose
    private String bucketCreatedBy;
    @SerializedName("bucket_created_on")
    @Expose
    private String bucketCreatedOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cont_no")
    @Expose
    private String contNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("google_id")
    @Expose
    private String googleId;
    @SerializedName("hotel_name")
    @Expose
    private String hotelName;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("open_times")
    @Expose
    private String openTimes;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("photo_reference")
    @Expose
    private String photoReference;
    @SerializedName("hotel_id")
    @Expose
    private String hotelId;
    @SerializedName("hotel_review")
    @Expose
    private String hotelReview;
    @SerializedName("total_likes")
    @Expose
    private String totalLikes;
    @SerializedName("total_comments")
    @Expose
    private String totalComments;
    @SerializedName("category_type")
    @Expose
    private String categoryType;
    @SerializedName("veg_nonveg")
    @Expose
    private String vegNonveg;
    @SerializedName("food_exprience")
    @Expose
    private String foodExprience;
    @SerializedName("ambiance")
    @Expose
    private String ambiance;
    @SerializedName("taste")
    @Expose
    private String taste;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("package")
    @Expose
    private String _package;
    @SerializedName("timedelivery")
    @Expose
    private String timedelivery;
    @SerializedName("delivery_mode")
    @Expose
    private String deliveryMode;
    @SerializedName("value_money")
    @Expose
    private String valueMoney;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("rev_user_id")
    @Expose
    private String revUserId;
    @SerializedName("rev_first_name")
    @Expose
    private String revFirstName;
    @SerializedName("rev_last_name")
    @Expose
    private String revLastName;
    @SerializedName("rev_email")
    @Expose
    private String revEmail;
    @SerializedName("rev_cont_no")
    @Expose
    private String revContNo;
    @SerializedName("rev_gender")
    @Expose
    private String revGender;
    @SerializedName("rev_dob")
    @Expose
    private String revDob;
    @SerializedName("total_followers")
    @Expose
    private String totalFollowers;
    @SerializedName("total_followings")
    @Expose
    private String totalFollowings;
    @SerializedName("rev_picture")
    @Expose
    private String revPicture;
    @SerializedName("total_review")
    @Expose
    private String totalReview;
    @SerializedName("total_review_users")
    @Expose
    private String totalReviewUsers;
    @SerializedName("total_food_exprience")
    @Expose
    private String totalFoodExprience;
    @SerializedName("total_ambiance")
    @Expose
    private String totalAmbiance;
    @SerializedName("total_taste")
    @Expose
    private String totalTaste;
    @SerializedName("total_service")
    @Expose
    private String totalService;
    @SerializedName("total_package")
    @Expose
    private String totalPackage;
    @SerializedName("total_timedelivery")
    @Expose
    private String totalTimedelivery;
    @SerializedName("total_value_money")
    @Expose
    private String totalValueMoney;
    @SerializedName("total_good")
    @Expose
    private String totalGood;
    @SerializedName("total_bad")
    @Expose
    private String totalBad;
    @SerializedName("total_good_bad_user")
    @Expose
    private String totalGoodBadUser;
    @SerializedName("likes_htl_id")
    @Expose
    private String likesHtlId;
    @SerializedName("htl_rev_likes")
    @Expose
    private String htlRevLikes;
    @SerializedName("following_id")
    @Expose
    private String followingId;
    @SerializedName("mode_id")
    @Expose
    private String modeId;
    @SerializedName("comp_name")
    @Expose
    private String compName;
    @SerializedName("comp_img")
    @Expose
    private String compImg;
    @SerializedName("topdishimage")
    @Expose
    private List<ReviewTopDishPojo> topdishimage = null;
    @SerializedName("topdish")
    @Expose
    private String topdish;
    @SerializedName("top_count")
    @Expose
    private Integer topCount;
    @SerializedName("top_dish_img_count")
    @Expose
    private Integer topDishImgCount;
    @SerializedName("avoiddishimage")
    @Expose
    private List<ReviewAvoidDishPojo> avoiddishimage = null;
    @SerializedName("avoiddish")
    @Expose
    private String avoiddish;
    @SerializedName("avoid_count")
    @Expose
    private Integer avoidCount;
    @SerializedName("avoid_dish_img_count")
    @Expose
    private Integer avoidDishImgCount;
    @SerializedName("ambi_image_count")
    @Expose
    private Integer ambiImageCount;
    @SerializedName("ambi_image")
    @Expose
    private List<AmbiImagePojo> ambiImage = null;
    private boolean userLiked;

    /**
     * No args constructor for use in serialization
     *
     */
    public GroupBucketList_Datum() {
    }

    /**
     *
     * @param phone
     * @param totalFoodExprience
     * @param bucketId
     * @param totalFollowers
     * @param hotelReview
     * @param ambiance
     * @param taste
     * @param topdish
     * @param topCount
     * @param photoReference
     * @param htlRevLikes
     * @param revLastName
     * @param totalReview
     * @param placeId
     * @param hotelName
     * @param userId
     * @param gender
     * @param longitude
     * @param avoiddish
     * @param modeId
     * @param revPicture
     * @param likesHtlId
     * @param lastName
     * @param ambiImage
     * @param _package
     * @param followingId
     * @param totalGoodBadUser
     * @param categoryType
     * @param deliveryMode
     * @param revGender
     * @param avoidDishImgCount
     * @param valueMoney
     * @param googleId
     * @param picture
     * @param createdOn
     * @param groupIcon
     * @param createdBy
     * @param totalTimedelivery
     * @param email
     * @param totalFollowings
     * @param individualUserid
     * @param compImg
     * @param latitude
     * @param totalValueMoney
     * @param bucketCreatedBy
     * @param revUserId
     * @param avoiddishimage
     * @param groupName
     * @param totalComments
     * @param vegNonveg
     * @param totalAmbiance
     * @param totalReviewUsers
     * @param topDishImgCount
     * @param hotelId
     * @param totalTaste
     * @param bucketCreatedOn
     * @param openTimes
     * @param revEmail
     * @param contNo
     * @param timedelivery
     * @param firstName
     * @param avoidCount
     * @param buckDescription
     * @param revContNo
     * @param revDob
     * @param postType
     * @param revFirstName
     * @param compName
     * @param ambiImageCount
     * @param totalBad
     * @param reviewid
     * @param topdishimage
     * @param groupId
     * @param totalLikes
     * @param totalService
     * @param address
     * @param dob
     * @param service
     * @param totalGood
     * @param totalPackage
     * @param foodExprience
     */
    public GroupBucketList_Datum(String bucketId, String buckDescription, String reviewid, String postType, String individualUserid, String groupId, String groupName, String groupIcon, String bucketCreatedBy, String bucketCreatedOn, String userId, String firstName, String lastName, String email, String contNo, String gender, String dob, String picture, String googleId, String hotelName, String placeId, String openTimes, String address, String latitude, String longitude, String phone, String photoReference, String hotelId, String hotelReview, String totalLikes, String totalComments, String categoryType, String vegNonveg, String foodExprience, String ambiance, String taste, String service, String _package, String timedelivery, String deliveryMode, String valueMoney, String createdBy, String createdOn, String revUserId, String revFirstName, String revLastName, String revEmail, String revContNo, String revGender, String revDob, String totalFollowers, String totalFollowings, String revPicture, String totalReview, String totalReviewUsers, String totalFoodExprience, String totalAmbiance, String totalTaste, String totalService, String totalPackage, String totalTimedelivery, String totalValueMoney, String totalGood, String totalBad, String totalGoodBadUser, String likesHtlId, String htlRevLikes, String followingId, String modeId, String compName, String compImg,
                                 List<ReviewTopDishPojo> topdishimage, String topdish, Integer topCount,
                                 Integer topDishImgCount, List<ReviewAvoidDishPojo> avoiddishimage, String avoiddish,
                                 Integer avoidCount, Integer avoidDishImgCount, Integer ambiImageCount, List<AmbiImagePojo> ambiImage) {
        super();
        this.bucketId = bucketId;
        this.buckDescription = buckDescription;
        this.reviewid = reviewid;
        this.postType = postType;
        this.individualUserid = individualUserid;
        this.groupId = groupId;
        this.groupName = groupName;
        this.groupIcon = groupIcon;
        this.bucketCreatedBy = bucketCreatedBy;
        this.bucketCreatedOn = bucketCreatedOn;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contNo = contNo;
        this.gender = gender;
        this.dob = dob;
        this.picture = picture;
        this.googleId = googleId;
        this.hotelName = hotelName;
        this.placeId = placeId;
        this.openTimes = openTimes;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
        this.photoReference = photoReference;
        this.hotelId = hotelId;
        this.hotelReview = hotelReview;
        this.totalLikes = totalLikes;
        this.totalComments = totalComments;
        this.categoryType = categoryType;
        this.vegNonveg = vegNonveg;
        this.foodExprience = foodExprience;
        this.ambiance = ambiance;
        this.taste = taste;
        this.service = service;
        this._package = _package;
        this.timedelivery = timedelivery;
        this.deliveryMode = deliveryMode;
        this.valueMoney = valueMoney;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.revUserId = revUserId;
        this.revFirstName = revFirstName;
        this.revLastName = revLastName;
        this.revEmail = revEmail;
        this.revContNo = revContNo;
        this.revGender = revGender;
        this.revDob = revDob;
        this.totalFollowers = totalFollowers;
        this.totalFollowings = totalFollowings;
        this.revPicture = revPicture;
        this.totalReview = totalReview;
        this.totalReviewUsers = totalReviewUsers;
        this.totalFoodExprience = totalFoodExprience;
        this.totalAmbiance = totalAmbiance;
        this.totalTaste = totalTaste;
        this.totalService = totalService;
        this.totalPackage = totalPackage;
        this.totalTimedelivery = totalTimedelivery;
        this.totalValueMoney = totalValueMoney;
        this.totalGood = totalGood;
        this.totalBad = totalBad;
        this.totalGoodBadUser = totalGoodBadUser;
        this.likesHtlId = likesHtlId;
        this.htlRevLikes = htlRevLikes;
        this.followingId = followingId;
        this.modeId = modeId;
        this.compName = compName;
        this.compImg = compImg;
        this.topdishimage = topdishimage;
        this.topdish = topdish;
        this.topCount = topCount;
        this.topDishImgCount = topDishImgCount;
        this.avoiddishimage = avoiddishimage;
        this.avoiddish = avoiddish;
        this.avoidCount = avoidCount;
        this.avoidDishImgCount = avoidDishImgCount;
        this.ambiImageCount = ambiImageCount;
        this.ambiImage = ambiImage;
    }

    public boolean isUserLiked() {
        return userLiked;
    }

    public void setUserLiked(boolean userLiked) {
        this.userLiked = userLiked;
    }

    public String getBucketId() {
        return bucketId;
    }

    public void setBucketId(String bucketId) {
        this.bucketId = bucketId;
    }

    public String getBuckDescription() {
        return buckDescription;
    }

    public void setBuckDescription(String buckDescription) {
        this.buckDescription = buckDescription;
    }

    public String getReviewid() {
        return reviewid;
    }

    public void setReviewid(String reviewid) {
        this.reviewid = reviewid;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getIndividualUserid() {
        return individualUserid;
    }

    public void setIndividualUserid(String individualUserid) {
        this.individualUserid = individualUserid;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupIcon() {
        return groupIcon;
    }

    public void setGroupIcon(String groupIcon) {
        this.groupIcon = groupIcon;
    }

    public String getBucketCreatedBy() {
        return bucketCreatedBy;
    }

    public void setBucketCreatedBy(String bucketCreatedBy) {
        this.bucketCreatedBy = bucketCreatedBy;
    }

    public String getBucketCreatedOn() {
        return bucketCreatedOn;
    }

    public void setBucketCreatedOn(String bucketCreatedOn) {
        this.bucketCreatedOn = bucketCreatedOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(String openTimes) {
        this.openTimes = openTimes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getHotelReview() {
        return hotelReview;
    }

    public void setHotelReview(String hotelReview) {
        this.hotelReview = hotelReview;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getVegNonveg() {
        return vegNonveg;
    }

    public void setVegNonveg(String vegNonveg) {
        this.vegNonveg = vegNonveg;
    }

    public String getFoodExprience() {
        return foodExprience;
    }

    public void setFoodExprience(String foodExprience) {
        this.foodExprience = foodExprience;
    }

    public String getAmbiance() {
        return ambiance;
    }

    public void setAmbiance(String ambiance) {
        this.ambiance = ambiance;
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getPackage() {
        return _package;
    }

    public void setPackage(String _package) {
        this._package = _package;
    }

    public String getTimedelivery() {
        return timedelivery;
    }

    public void setTimedelivery(String timedelivery) {
        this.timedelivery = timedelivery;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String getValueMoney() {
        return valueMoney;
    }

    public void setValueMoney(String valueMoney) {
        this.valueMoney = valueMoney;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getRevUserId() {
        return revUserId;
    }

    public void setRevUserId(String revUserId) {
        this.revUserId = revUserId;
    }

    public String getRevFirstName() {
        return revFirstName;
    }

    public void setRevFirstName(String revFirstName) {
        this.revFirstName = revFirstName;
    }

    public String getRevLastName() {
        return revLastName;
    }

    public void setRevLastName(String revLastName) {
        this.revLastName = revLastName;
    }

    public String getRevEmail() {
        return revEmail;
    }

    public void setRevEmail(String revEmail) {
        this.revEmail = revEmail;
    }

    public String getRevContNo() {
        return revContNo;
    }

    public void setRevContNo(String revContNo) {
        this.revContNo = revContNo;
    }

    public String getRevGender() {
        return revGender;
    }

    public void setRevGender(String revGender) {
        this.revGender = revGender;
    }

    public String getRevDob() {
        return revDob;
    }

    public void setRevDob(String revDob) {
        this.revDob = revDob;
    }

    public String getTotalFollowers() {
        return totalFollowers;
    }

    public void setTotalFollowers(String totalFollowers) {
        this.totalFollowers = totalFollowers;
    }

    public String getTotalFollowings() {
        return totalFollowings;
    }

    public void setTotalFollowings(String totalFollowings) {
        this.totalFollowings = totalFollowings;
    }

    public String getRevPicture() {
        return revPicture;
    }

    public void setRevPicture(String revPicture) {
        this.revPicture = revPicture;
    }

    public String getTotalReview() {
        return totalReview;
    }

    public void setTotalReview(String totalReview) {
        this.totalReview = totalReview;
    }

    public String getTotalReviewUsers() {
        return totalReviewUsers;
    }

    public void setTotalReviewUsers(String totalReviewUsers) {
        this.totalReviewUsers = totalReviewUsers;
    }

    public String getTotalFoodExprience() {
        return totalFoodExprience;
    }

    public void setTotalFoodExprience(String totalFoodExprience) {
        this.totalFoodExprience = totalFoodExprience;
    }

    public String getTotalAmbiance() {
        return totalAmbiance;
    }

    public void setTotalAmbiance(String totalAmbiance) {
        this.totalAmbiance = totalAmbiance;
    }

    public String getTotalTaste() {
        return totalTaste;
    }

    public void setTotalTaste(String totalTaste) {
        this.totalTaste = totalTaste;
    }

    public String getTotalService() {
        return totalService;
    }

    public void setTotalService(String totalService) {
        this.totalService = totalService;
    }

    public String getTotalPackage() {
        return totalPackage;
    }

    public void setTotalPackage(String totalPackage) {
        this.totalPackage = totalPackage;
    }

    public String getTotalTimedelivery() {
        return totalTimedelivery;
    }

    public void setTotalTimedelivery(String totalTimedelivery) {
        this.totalTimedelivery = totalTimedelivery;
    }

    public String getTotalValueMoney() {
        return totalValueMoney;
    }

    public void setTotalValueMoney(String totalValueMoney) {
        this.totalValueMoney = totalValueMoney;
    }

    public String getTotalGood() {
        return totalGood;
    }

    public void setTotalGood(String totalGood) {
        this.totalGood = totalGood;
    }

    public String getTotalBad() {
        return totalBad;
    }

    public void setTotalBad(String totalBad) {
        this.totalBad = totalBad;
    }

    public String getTotalGoodBadUser() {
        return totalGoodBadUser;
    }

    public void setTotalGoodBadUser(String totalGoodBadUser) {
        this.totalGoodBadUser = totalGoodBadUser;
    }

    public String getLikesHtlId() {
        return likesHtlId;
    }

    public void setLikesHtlId(String likesHtlId) {
        this.likesHtlId = likesHtlId;
    }

    public String getHtlRevLikes() {
        return htlRevLikes;
    }

    public void setHtlRevLikes(String htlRevLikes) {
        this.htlRevLikes = htlRevLikes;
    }

    public String getFollowingId() {
        return followingId;
    }

    public void setFollowingId(String followingId) {
        this.followingId = followingId;
    }

    public String getModeId() {
        return modeId;
    }

    public void setModeId(String modeId) {
        this.modeId = modeId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getCompImg() {
        return compImg;
    }

    public void setCompImg(String compImg) {
        this.compImg = compImg;
    }

    public List<ReviewTopDishPojo> getTopdishimage() {
        return topdishimage;
    }

    public void setTopdishimage(List<ReviewTopDishPojo> topdishimage) {
        this.topdishimage = topdishimage;
    }

    public String getTopdish() {
        return topdish;
    }

    public void setTopdish(String topdish) {
        this.topdish = topdish;
    }

    public Integer getTopCount() {
        return topCount;
    }

    public void setTopCount(Integer topCount) {
        this.topCount = topCount;
    }

    public Integer getTopDishImgCount() {
        return topDishImgCount;
    }

    public void setTopDishImgCount(Integer topDishImgCount) {
        this.topDishImgCount = topDishImgCount;
    }

    public List<ReviewAvoidDishPojo> getAvoiddishimage() {
        return avoiddishimage;
    }

    public void setAvoiddishimage(List<ReviewAvoidDishPojo> avoiddishimage) {
        this.avoiddishimage = avoiddishimage;
    }

    public String getAvoiddish() {
        return avoiddish;
    }

    public void setAvoiddish(String avoiddish) {
        this.avoiddish = avoiddish;
    }

    public Integer getAvoidCount() {
        return avoidCount;
    }

    public void setAvoidCount(Integer avoidCount) {
        this.avoidCount = avoidCount;
    }

    public Integer getAvoidDishImgCount() {
        return avoidDishImgCount;
    }

    public void setAvoidDishImgCount(Integer avoidDishImgCount) {
        this.avoidDishImgCount = avoidDishImgCount;
    }

    public Integer getAmbiImageCount() {
        return ambiImageCount;
    }

    public void setAmbiImageCount(Integer ambiImageCount) {
        this.ambiImageCount = ambiImageCount;
    }

    public List<AmbiImagePojo> getAmbiImage() {
        return ambiImage;
    }

    public void setAmbiImage(List<AmbiImagePojo> ambiImage) {
        this.ambiImage = ambiImage;
    }

}
