package com.kaspontech.foodwall.bucketlistpackage;


import android.content.Intent;
import android.graphics.Color;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.kaspontech.foodwall.Home;
import com.kaspontech.foodwall.chatpackage.ChatActivity;
import com.kaspontech.foodwall.chatpackage.Chat_followers_list_group;
import com.kaspontech.foodwall.chatpackage.chatfollowerslistsingle;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.profilePackage.Profile;

import java.util.Objects;

import info.hoang8f.android.segmented.SegmentedGroup;

public class MyBucketlistActivity extends Fragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {


    /**
     * Action bar
     **/

    Toolbar actionBar;

    /**
     * Tool bar title text view
     **/

    TextView toolbarTitle;

    /**
     * Back button press
     **/
    ImageButton back;

    /**
     * Event search image button
     **/
    ImageButton imgBtnBucketList;
    /**
     * My bucket frame layout
     **/
    FrameLayout bucketContainer;
    /**
     * My bucket segment group
     **/
    SegmentedGroup segmentedBucket;
    /**
     * My bucket tab layout
     **/
    TabLayout tabBucket;
    /**
     * My bucket radio button
     **/
    RadioButton mybucketType;
    /**
     * Individual bucket radio button
     **/
    RadioButton individualType;

    /**
     * Group bucket radio button
     **/
    RadioButton groupType;

    /**
     * My bucket fragment
     **/
    Fragment myBucketFragment;
    /**
     * Individual bucket fragment
     **/
    Fragment individualFragment;

    /**
     * Group bucket fragment
     **/

    Fragment groupFragment;

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.create_mybucketlist_activity, container, false);
        //  setContentView(R.layout.create_mybucketlist_activity);
        /*
         Widget initialization
         */
        actionBar = (Toolbar) view.findViewById(R.id.action_bar_bucket);

          /*
          Title text initialization
         */

        toolbarTitle = actionBar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(R.string.Bucket_List);
        toolbarTitle.setGravity(View.TEXT_ALIGNMENT_CENTER);
        /*
         Back button press initialization
         */

        back = actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        back.setVisibility(View.GONE);
        /*
         Event button press initialization
         */
        imgBtnBucketList = actionBar.findViewById(R.id.imgBtnBucketList);
         /*
         Segment group initialization
         */
        segmentedBucket = view.findViewById(R.id.segmented_bucket);
         /*
        Tab layout initialization
         */
        tabBucket = view.findViewById(R.id.tab_bucket);


         /*
         Framelayout initialization
         */
        bucketContainer = view.findViewById(R.id.bucket_container);

        /*
          My bucket radio button
         */
        mybucketType = view.findViewById(R.id.type_mybucket);
        /*
          Individual bucket radio button
         */
        individualType = view.findViewById(R.id.type_Individual);
        /*
          Group bucket radio button
         */
        groupType = view.findViewById(R.id.type_Group);


        /*
          My bucket fragment
         */
        myBucketFragment = new Mybucketfragment();
        /*
          Individual bucket fragment
         */
        individualFragment = new Individual_bucket_fragment();
        /*
          Group bucket fragment
         */
        groupFragment = new Groupfragment();


        /*Tab layout initialization and naming the tabs*/

        tabBucket.addTab(tabBucket.newTab().setText("My \nBucket List"));
        tabBucket.addTab(tabBucket.newTab().setText("Individual \nBucket List"));
        tabBucket.addTab(tabBucket.newTab().setText("Group \nBucket List"));
        tabBucket.setTabTextColors(Color.parseColor("#FFFFFF"), Color.parseColor("#FFFFFF"));


        replaceFragment(myBucketFragment);

        /* Getting Input values from various activities and adapters */

        Intent intent = getActivity().getIntent();
        try {
            if (intent.getStringExtra("redirectbucket") == null) {
                replaceFragment(myBucketFragment);
            } else {
                int redirect = Integer.parseInt(intent.getStringExtra("redirectbucket"));

                if (redirect == 1) {

                    Objects.requireNonNull(tabBucket.getTabAt(0)).select();
                    /*My bucket fragment*/
                    replaceFragment(myBucketFragment);


                } else if (redirect == 2) {

                    Objects.requireNonNull(tabBucket.getTabAt(1)).select();
                    /*Individual bucket fragment*/
                    replaceFragment(individualFragment);

                } else if (redirect == 3) {

                    Objects.requireNonNull(tabBucket.getTabAt(2)).select();
                    /*Group bucket fragment*/
                    replaceFragment(groupFragment);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        /*Tab click listener*/
        tabBucket.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {


                switch (tab.getPosition()) {

                    case 0:

                         /*My bucket fragment*/
                        replaceFragment(myBucketFragment);
                        break;

                    case 1:

                         /*Individual bucket fragment*/
                        replaceFragment(individualFragment);
                        break;

                    case 2:

                        /*Group bucket fragment*/
                        replaceFragment(groupFragment);
                        break;

                    case 3:
                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }


        });

        return view;
    }

    /*Replaciing fragment using containers*/
    public void replaceFragment(Fragment fragment) {

        try {

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.bucket_container, fragment);
            fragmentTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {


        switch (group.getCheckedRadioButtonId()) {

            case R.id.type_mybucket:

                /*My bucket fragment*/
                replaceFragment(myBucketFragment);
                break;


            case R.id.type_Individual:

                /*Individual bucket fragment*/
                replaceFragment(individualFragment);
                break;

            case R.id.type_Group:

                /*Group bucket fragment*/
                replaceFragment(groupFragment);
                break;

        }

    }


    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            default:

                break;

            /*On back press*/
            case R.id.back:
                break;

        }


    }


}

