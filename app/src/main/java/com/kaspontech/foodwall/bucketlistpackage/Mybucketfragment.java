package com.kaspontech.foodwall.bucketlistpackage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.bucketadapter.MyBucketlistAdapter;
import com.kaspontech.foodwall.adapters.bucketadapter.bucketmyselfAdapter;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.modelclasses.Review_pojo.ReviewCommentsPojo;
import com.kaspontech.foodwall.onCommentsPostListener;
import com.kaspontech.foodwall.reviewpackage.Reviews_comments_all_activity;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Mybucketfragment extends Fragment implements onCommentsPostListener {
    /**
     * Context
     **/
    Context context;
    /**
     * View
     **/
    View view;
    /**
     * No data textview
     **/
    TextView llNoPost;

    /**
     * Page background for no data available
     **/
    RelativeLayout pageBackground;
    /**
     * My bucket recyclerview
     **/
    RecyclerView myBucketRecyclerview;
    /**
     * My Bucket adapter
     **/
    MyBucketlistAdapter bucketMyselfAdapter;
    /**
     * My bucket arraylist
     **/
    List<GetMyBucketListDataPojo> getmyBucketInputPojoArrayList = new ArrayList<>();

    EmojiconEditText review_comment;

    TextView tv_view_all_comments;
    /**
     * Progressbar
     */
    ProgressBar progressbar_review, comments_review;

    public Mybucketfragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mybucket_layout, container, false);
        context = view.getContext();

        /*Widget initialization*/

        /*
          Page background for no data available
         */

        pageBackground = view.findViewById(R.id.page_background);

        /*Group bucket recyclerview*/

        myBucketRecyclerview = view.findViewById(R.id.my_bucket_recyclerview);

        /*No post textview*/

        llNoPost = view.findViewById(R.id.ll_no_post);
        progressbar_review = view.findViewById(R.id.progressbar_review);

        /* Calling my bucket list API */
        getBucketMyself();

        return view;
    }

    /* Calling my bucket list API */

    private void getBucketMyself() {

        if (Utility.isConnected(context)) {
            progressbar_review.setVisibility(View.VISIBLE);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Call<GetMyBucketListPojo> call = apiService.get_bucket_myself("get_bucket_myself", userid);

                call.enqueue(new Callback<GetMyBucketListPojo>() {
                    @Override
                    public void onResponse(Call<GetMyBucketListPojo> call, Response<GetMyBucketListPojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            llNoPost.setVisibility(View.GONE);

                            //  pageBackground.setBackgroundColor(getResources().getColor(R.color.page_background));

                            getmyBucketInputPojoArrayList.clear();
                            //Success

                            getmyBucketInputPojoArrayList = response.body().getData();


                            progressbar_review.setVisibility(View.GONE);

                            /*Setting adapter*/
                            bucketMyselfAdapter = new MyBucketlistAdapter(context, getmyBucketInputPojoArrayList, Mybucketfragment.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                            myBucketRecyclerview.setLayoutManager(mLayoutManager);
                            myBucketRecyclerview.setItemAnimator(new DefaultItemAnimator());
                            myBucketRecyclerview.setAdapter(bucketMyselfAdapter);
                            myBucketRecyclerview.hasFixedSize();
                            myBucketRecyclerview.setVisibility(View.VISIBLE);
                            myBucketRecyclerview.setNestedScrollingEnabled(false);
                            bucketMyselfAdapter.notifyDataSetChanged();

                        }
                    }

                    @Override
                    public void onFailure(Call<GetMyBucketListPojo> call, Throwable t) {

                        progressbar_review.setVisibility(View.GONE);

                        if (getmyBucketInputPojoArrayList.isEmpty()) {
                            llNoPost.setVisibility(View.VISIBLE);
                        }

                        pageBackground.setBackgroundColor(Color.WHITE);

                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();
                pageBackground.setBackgroundColor(Color.WHITE);


            }

        } else {

            Toast.makeText(context, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            pageBackground.setBackgroundColor(Color.WHITE);


        }


    }

    /* Calling my bucket list API */

    private void getBucketMyselfRefresh() {

        if (Utility.isConnected(context)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            try {

                int userid = Integer.parseInt(Pref_storage.getDetail(context, "userId"));

                Call<GetMyBucketListPojo> call = apiService.get_bucket_myself("get_bucket_myself", userid);

                call.enqueue(new Callback<GetMyBucketListPojo>() {
                    @Override
                    public void onResponse(@NonNull Call<GetMyBucketListPojo> call, @NonNull Response<GetMyBucketListPojo> response) {

                        String responseStatus = response.body().getResponseMessage();

                        Log.e("responseStatus", "responseStatus->" + responseStatus);

                        if (responseStatus.equals("success")) {

                            llNoPost.setVisibility(View.GONE);

                            try {
                                pageBackground.setBackgroundColor(getResources().getColor(R.color.page_background));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            getmyBucketInputPojoArrayList.clear();

                            //Success

                            getmyBucketInputPojoArrayList = response.body().getData();


                            bucketMyselfAdapter = new MyBucketlistAdapter(context, getmyBucketInputPojoArrayList, Mybucketfragment.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                            myBucketRecyclerview.setLayoutManager(mLayoutManager);
                            myBucketRecyclerview.setItemAnimator(new DefaultItemAnimator());
                            myBucketRecyclerview.setAdapter(bucketMyselfAdapter);
                            myBucketRecyclerview.hasFixedSize();
                            myBucketRecyclerview.setNestedScrollingEnabled(false);
                            myBucketRecyclerview.setVisibility(View.VISIBLE);
                            bucketMyselfAdapter.notifyDataSetChanged();

                        }
                    }

                    @Override
                    public void onFailure(Call<GetMyBucketListPojo> call, Throwable t) {
                        if (getmyBucketInputPojoArrayList.isEmpty()) {
                            llNoPost.setVisibility(View.VISIBLE);

                        }
                        pageBackground.setBackgroundColor(Color.WHITE);

                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();
                pageBackground.setBackgroundColor(Color.WHITE);


            }

        } else {

            Toast.makeText(context, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            pageBackground.setBackgroundColor(Color.WHITE);


        }

    }

    @Override
    public void onResume() {
        super.onResume();


        if (!getmyBucketInputPojoArrayList.isEmpty()) {
            /* Calling my bucket list API on resume */

            getBucketMyselfRefresh();

        } else {
//           getBucketMyself();
        }


    }

    @Override
    public void onPostCommitedView(View view, int adapterPosition, View hotelDetailsView) {

        switch (view.getId()) {

            case R.id.txt_adapter_post:

                review_comment = (EmojiconEditText) hotelDetailsView.findViewById(R.id.review_comment);
                tv_view_all_comments = (TextView) hotelDetailsView.findViewById(R.id.tv_view_all_comments);
                comments_review = (ProgressBar) hotelDetailsView.findViewById(R.id.comments_review);

                if (review_comment.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Enter your comments to continue", Toast.LENGTH_SHORT).show();
                } else {

                    Utility.hideKeyboard(view);
                    createEditReviewComments(getmyBucketInputPojoArrayList.get(adapterPosition).getHotelId(),
                            review_comment.getText().toString(),
                            getmyBucketInputPojoArrayList.get(adapterPosition).getReviewid(), adapterPosition);

                }
                break;

        }

    }

    private void createEditReviewComments(String hotelId, String toString, String getReviewId, final int adapterPosition) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {

            comments_review.setVisibility(View.VISIBLE);
            comments_review.setIndeterminate(true);

            String createuserid = Pref_storage.getDetail(context, "userId");

            Call<ReviewCommentsPojo> call = apiService.get_create_edit_hotel_review_comments("create_edit_hotel_review_comments", Integer.parseInt(hotelId),
                    0, Integer.parseInt(createuserid), toString, Integer.parseInt(getReviewId));
            call.enqueue(new Callback<ReviewCommentsPojo>() {
                @Override
                public void onResponse(Call<ReviewCommentsPojo> call, Response<ReviewCommentsPojo> response) {

                    review_comment.setText("");

                    if (response.code() == 500) {
                        comments_review.setVisibility(View.GONE);
                        comments_review.setIndeterminate(false);

                        Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();

                    } else {
                        comments_review.setVisibility(View.GONE);
                        comments_review.setIndeterminate(false);

                        if (response.body() != null) {

                            if (Objects.requireNonNull(response.body()).getResponseMessage().equals("success")) {

                                if (response.body().getData().size() > 0) {

                                    if (tv_view_all_comments.getVisibility() == View.GONE) {

                                        tv_view_all_comments.setVisibility(View.VISIBLE);
                                        //String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                        String comments = getString(R.string.view_one_comment);
                                        tv_view_all_comments.setText(comments);

                                    } else {

                                        String comments = "View all " + response.body().getData().get(0).getTotalComments() + " comments";
                                        tv_view_all_comments.setText(comments);
                                    }


                                    Intent intent = new Intent(context, Reviews_comments_all_activity.class);

                                    intent.putExtra("posttype", getmyBucketInputPojoArrayList.get(adapterPosition).getCategoryType());
                                    intent.putExtra("hotelid", getmyBucketInputPojoArrayList.get(adapterPosition).getHotelId());
                                    intent.putExtra("reviewid", getmyBucketInputPojoArrayList.get(adapterPosition).getReviewid());
                                    intent.putExtra("userid", getmyBucketInputPojoArrayList.get(adapterPosition).getUserId());

                                    intent.putExtra("hotelname", getmyBucketInputPojoArrayList.get(adapterPosition).getHotelName());
                                    intent.putExtra("overallrating", getmyBucketInputPojoArrayList.get(adapterPosition).getFoodExprience());

                                    if (getmyBucketInputPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                        intent.putExtra("totaltimedelivery", getmyBucketInputPojoArrayList.get(adapterPosition).getTimedelivery());
                                    } else {
                                        intent.putExtra("totaltimedelivery", getmyBucketInputPojoArrayList.get(adapterPosition).getAmbiance());
                                    }

                                    intent.putExtra("taste_count", getmyBucketInputPojoArrayList.get(adapterPosition).getTaste());
                                    intent.putExtra("vfm_rating", getmyBucketInputPojoArrayList.get(adapterPosition).getValueMoney());
                                    if (getmyBucketInputPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                        intent.putExtra("package_rating", getmyBucketInputPojoArrayList.get(adapterPosition).getPackage());
                                    } else {
                                        intent.putExtra("package_rating", getmyBucketInputPojoArrayList.get(adapterPosition).getService());
                                    }

                                    intent.putExtra("hotelname", getmyBucketInputPojoArrayList.get(adapterPosition).getHotelName());
                                    intent.putExtra("reviewprofile", getmyBucketInputPojoArrayList.get(adapterPosition).getRevPicture());
                                    intent.putExtra("createdon", getmyBucketInputPojoArrayList.get(adapterPosition).getCreatedOn());
                                    intent.putExtra("firstname", getmyBucketInputPojoArrayList.get(adapterPosition).getRevFirstName());
                                    intent.putExtra("lastname", getmyBucketInputPojoArrayList.get(adapterPosition).getRevLastName());

                                    if (getmyBucketInputPojoArrayList.get(adapterPosition).getCategoryType().equalsIgnoreCase("2")) {
                                        intent.putExtra("title_ambience", "Packaging");
                                    } else {
                                        intent.putExtra("title_ambience", "Hotel Ambience");
                                    }


                                    startActivity(intent);

                                } else {

                                }

                            } else {

                            }
                        }

                    }

                }

                @Override
                public void onFailure(Call<ReviewCommentsPojo> call, Throwable t) {
                    //Error
                    Log.e("FailureError", "" + t.getMessage());

                    comments_review.setVisibility(View.GONE);
                    comments_review.setIndeterminate(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
