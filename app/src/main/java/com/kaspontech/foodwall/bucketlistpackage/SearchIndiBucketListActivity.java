package com.kaspontech.foodwall.bucketlistpackage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.bucketadapter.IndividualFollowerAdapter;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_follower_output_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchIndiBucketListActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * Application tag
     **/

    public static final String TAG = "SearchIndiBucket";
    /**
     * Action bar
     **/
    Toolbar actionBar;
    /**
     *Adding text click
     **/
    TextView textAdd;
    /**
     * Search individual bucket frame layout
     **/
    FrameLayout searchContainer;
    /**
     * Back press button
     **/
    ImageButton imageButton;
    /**
     * Done button press
     **/
    Button btnDone;
    /**
     * Individual search view
     **/
    SearchView searchView;
    /**
     * No data found layout
     **/
    LinearLayout llNotFound;
    /**
     * Search tab layout
     **/
    LinearLayout llSearchTabs;
    /**
     *Scroll view for search
     **/
    ScrollView indiSearchScroll;
    /**
     *Auto search
     **/
    AutoCompleteTextView autoSearch;
    /**
     *Search tab layout
     **/
    TabLayout searchTablayout;
    /**
     *Relative layout for search
     **/
    RelativeLayout rlIndiSearch;



    /**
     *RecyclerView view for search
     **/
    RecyclerView recyclerViewIndiSearch;
    /**
     *Adapter search
     **/
    IndividualFollowerAdapter individualFollowerAdapter;

    /**
     *Followers pojo
     **/
    Get_followerlist_pojo getFollowerlistPojo;
    /**
     *Followers arraylist
     **/
    List<Get_followerlist_pojo> followerlist = new ArrayList<>();

    ArrayList<Integer> followerlistid = new ArrayList<>();

    HashMap<String, Integer> selectedfollowerlistId = new HashMap<>();
    HashMap<String, Integer> selectedgroupID= new HashMap<>();
    /**
     *Review id result
     **/
    String reviewId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_individual_layout);
        /*Widget initialization*/

        actionBar = findViewById(R.id.action_bar_indi_search);

        imageButton = findViewById(R.id.back);
        llNotFound = findViewById(R.id.ll_not_found);
        llSearchTabs = findViewById(R.id.ll_search_tabs);
        indiSearchScroll = findViewById(R.id.indi_search_scroll);
        autoSearch = findViewById(R.id.auto_search);
        searchTablayout = findViewById(R.id.tab_search);
        searchContainer = findViewById(R.id.search_container);
        rlIndiSearch = findViewById(R.id.rl_indi_search);

        imageButton.setOnClickListener(this);

        setSupportActionBar(actionBar);

        /* individual bucket list done click */

        btnDone = findViewById(R.id.btn_done);
        btnDone.setOnClickListener(this);

        textAdd = findViewById(R.id.text_add);
        textAdd.setOnClickListener(this);


        searchView = actionBar.findViewById(R.id.grp_search_view);
        recyclerViewIndiSearch = findViewById(R.id.recycler_view_indi_search);


        /*Search view initialization*/

        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.setQueryHint("Search followers");
        searchView.requestFocusFromTouch();

        /*Getting input values from various activities*/

        Intent intent= getIntent();
        reviewId = intent.getStringExtra("reviewId");


        /* Call follower API */

        getFollower();





        /*Search query listener*/

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                if (query.length() != 0) {
                    if (Utility.isConnected(SearchIndiBucketListActivity.this)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                            Call<Get_follower_output_pojo> call = apiService.get_follower_search("get_follower_search", Integer.parseInt(createdby),query);
                            call.enqueue(new Callback<Get_follower_output_pojo>() {
                                @Override
                                public void onResponse(Call<Get_follower_output_pojo> call, Response<Get_follower_output_pojo> response) {

                                    if (response.body().getResponseCode() == 1) {

                                        followerlist.clear();

                                        for (int j = 0; j < response.body().getData().size(); j++) {

                                            if(response.body().getData().get(j).getFollowerId()==null){


                                            }else {

                                                followerlist = response.body().getData();

                                                followerlist.add(getFollowerlistPojo);


                                                /*Setting adapter*/
                                                individualFollowerAdapter = new IndividualFollowerAdapter(SearchIndiBucketListActivity.this, followerlist,selectedfollowerlistId);
                                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchIndiBucketListActivity.this);
                                                recyclerViewIndiSearch.setLayoutManager(mLayoutManager);
                                                recyclerViewIndiSearch.setHasFixedSize(true);
                                                recyclerViewIndiSearch.setItemAnimator(new DefaultItemAnimator());
                                                recyclerViewIndiSearch.setAdapter(individualFollowerAdapter);
                                                recyclerViewIndiSearch.setNestedScrollingEnabled(false);
                                                individualFollowerAdapter.notifyDataSetChanged();
                                                recyclerViewIndiSearch.setVisibility(View.VISIBLE);
//                                                btnDone.setVisibility(View.VISIBLE);

                                                /*rvFollowerGrpSearch.setAdapter(individualFollowerAdapter);
                                                rvFollowerGrpSearch.setNestedScrollingEnabled(false);
*/
                                            }


                                        }
                                    }

                                }

                                @Override
                                public void onFailure(Call<Get_follower_output_pojo> call, Throwable t) {
                                    //Error
                                    Log.e(TAG, "FailureError" + t.getMessage());

                                    llNotFound.setVisibility(View.VISIBLE);
                                    textAdd.setVisibility(View.GONE);
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(rlIndiSearch, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        View view1 = snackbar.getView();
                        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                        textview.setTextColor(Color.WHITE);
                        snackbar.show();
                    }


                } else if (query.length()==0) {
                    /* Get follower */

                    getFollower();

                }


                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {



                if (newText.length() == 0) {
                    /* Get follower */

                    getFollower();

              /*      followerlist.clear();
                    recyclerViewIndiSearch.setAdapter(individualFollowerAdapter);
                    btnDone.setVisibility(View.GONE);*/

                }
               else {

                    if (Utility.isConnected(SearchIndiBucketListActivity.this)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                            Call<Get_follower_output_pojo> call = apiService.get_follower_search("get_follower_search", Integer.parseInt(createdby),newText);
                            call.enqueue(new Callback<Get_follower_output_pojo>() {
                                @Override
                                public void onResponse(Call<Get_follower_output_pojo> call, Response<Get_follower_output_pojo> response) {

                                    if (response.body().getResponseCode() == 1) {
                                        followerlist.clear();

                                        for (int j = 0; j < response.body().getData().size(); j++) {

                                            if(response.body().getData().get(j).getFollowerId()==null){

                                            }else {


                                                followerlist = response.body().getData();

                                                /*Setting adapter*/

                                                individualFollowerAdapter = new IndividualFollowerAdapter(SearchIndiBucketListActivity.this, followerlist,selectedfollowerlistId);
                                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchIndiBucketListActivity.this);
                                                recyclerViewIndiSearch.setLayoutManager(mLayoutManager);
                                                recyclerViewIndiSearch.setHasFixedSize(true);
                                                recyclerViewIndiSearch.setItemAnimator(new DefaultItemAnimator());
                                                recyclerViewIndiSearch.setAdapter(individualFollowerAdapter);
                                                recyclerViewIndiSearch.setNestedScrollingEnabled(false);
                                                individualFollowerAdapter.notifyDataSetChanged();
                                                recyclerViewIndiSearch.setVisibility(View.VISIBLE);
//                                                btnDone.setVisibility(View.VISIBLE);

                                                /*rvFollowerGrpSearch.setAdapter(individualFollowerAdapter);
                                                rvFollowerGrpSearch.setNestedScrollingEnabled(false);
*/
                                            }


                                        }
                                    }

                                }

                                @Override
                                public void onFailure(Call<Get_follower_output_pojo> call, Throwable t) {
                                    //Error
                                    Log.e("FailureError", "" + t.getMessage());

                                    llNotFound.setVisibility(View.VISIBLE);
                                    textAdd.setVisibility(View.GONE);

                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(rlIndiSearch, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        View view1 = snackbar.getView();
                        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                        textview.setTextColor(Color.WHITE);
                        snackbar.show();
                    }

                   


                }


                return true;
            }


        });

    }


    /* Interface for individual follower */

    public void getSelectedfollower(View view, int position) {

        String followerName;

        followerName = followerlist.get(position).getFirstName() + " " + followerlist.get(position).getLastName();


        if (((CheckBox) view).isChecked()) {

            if (selectedfollowerlistId.containsValue(Integer.valueOf(followerlist.get(position).getFollowerId()))) {

                return;

            } else {

                selectedfollowerlistId.put(followerName, Integer.valueOf(followerlist.get(position).getFollowerId()));

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(),"userId"));

                followerlistid.add(Integer.valueOf(followerlist.get(position).getFollowerId()));


                //Your id for individual chat

                if(followerlistid.toString().contains(String.valueOf(userid))){

                }else {
                    followerlistid.add(userid);
                }

                Log.e("follower", "getSelectedfollower:"+selectedfollowerlistId.toString() );
                Log.e("followerID", "followerlistid:"+followerlistid.toString() );

//                followerlistid.add(userid);

            }


        } else {
            followerlistid.remove(Integer.valueOf(followerlist.get(position).getFollowerId()));
            selectedfollowerlistId.remove(followerName);

        }

    }



    /* Get follower */
    private void getFollower() {


        if (Utility.isConnected(SearchIndiBucketListActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_follower_output_pojo> call = apiService.get_follower("get_follower", Integer.parseInt(createdby));
                call.enqueue(new Callback<Get_follower_output_pojo>() {
                    @Override
                    public void onResponse(Call<Get_follower_output_pojo> call, Response<Get_follower_output_pojo> response) {

                        if (response.body().getResponseCode() == 1) {
                            followerlist.clear();

                            for (int j = 0; j < response.body().getData().size(); j++) {



                                if (response.body().getData().get(j).getFollowerId() == null) {

                                } else {


                                    followerlist = response.body().getData();

                                    /*Setting adapter*/
                                    individualFollowerAdapter = new IndividualFollowerAdapter(SearchIndiBucketListActivity.this, followerlist,selectedfollowerlistId);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchIndiBucketListActivity.this);
                                    recyclerViewIndiSearch.setLayoutManager(mLayoutManager);
                                    recyclerViewIndiSearch.setHasFixedSize(true);
                                    recyclerViewIndiSearch.setItemAnimator(new DefaultItemAnimator());
                                    recyclerViewIndiSearch.setAdapter(individualFollowerAdapter);
                                    recyclerViewIndiSearch.setNestedScrollingEnabled(false);
                                    individualFollowerAdapter.notifyDataSetChanged();
                                    recyclerViewIndiSearch.setVisibility(View.VISIBLE);
//                                    btnDone.setVisibility(View.VISIBLE);
                                }


                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Get_follower_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());

                        llNotFound.setVisibility(View.VISIBLE);
                        textAdd.setVisibility(View.GONE);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlIndiSearch, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }





    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {


            case R.id.back:

                finish();

                break;

                case R.id.text_add:


                    if(!followerlistid.isEmpty()){
                        create_bucket_individual_user();
                    }else {
                        Toast.makeText(this, "Atleast one member to be selected", Toast.LENGTH_SHORT).show();
                    }

                    break;

        }
    }


    //create_bucket_individual_user API
    private void create_bucket_individual_user() {
        /*if (isConnected(SearchIndiBucketListActivity.this)) {
            int  post_type=2;

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(getApplicationContext(),"userId");

                String username= fname.concat(" ").concat(lname);


                call = apiService.create_bucket_individual_user("create_bucket_individual_user", Integer.parseInt(reviewId),username, Integer.parseInt(createuserid),followerlistid,post_type);
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1) {
                            new SweetAlertDialog(SearchIndiBucketListActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!!")
                                    .setContentText("Added to your friend's bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            startActivity(new Intent(SearchIndiBucketListActivity.this,MyBucketlistActivity.class));
                                            finish();

                                        }
                                    })
                                    .show();


                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlIndiSearch, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }
*/
    }


        /*Fragment replacing*/
    public void replaceFragment(Fragment fragment) {

        try {
            FragmentManager fm =getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.search_container, fragment);
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }






}
