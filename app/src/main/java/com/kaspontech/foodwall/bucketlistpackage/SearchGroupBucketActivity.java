package com.kaspontech.foodwall.bucketlistpackage;

import android.content.Intent;
import android.graphics.Color;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.bucketadapter.NewSearchGroupAdapter;
import com.kaspontech.foodwall.adapters.bucketadapter.SearchGroupAdapter;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.Get_grp_bucket_pojo;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.Get_grp_input_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchGroupBucketActivity extends AppCompatActivity implements View.OnClickListener {
/**
 * Application tag
 **/

   public static final String TAG = "SearchGroupBucket";
    /**
     * Action bar
     **/
    Toolbar actionBar;
    /**
     * Search group bucket frame layout
     **/
    FrameLayout searchContainer;
    /**
     * Back press button
     **/
    ImageButton imageButton;
    /**
     * Done button press
     **/
    Button btnDone;
    /**
     * Group search view
     **/
    SearchView searchView;
    /**
     * No data found layout
     **/
    LinearLayout llNotFound;
    /**
     * Search tab layout
     **/
    LinearLayout llSearchTabs;
    /**
     *Scroll view for search
     **/
    ScrollView groupSearchScroll;
    /**
     *Auto search
     **/
    AutoCompleteTextView autoSearch;
    /**
     *Adding text click
     **/
    TextView textAdd;

    /**
     *Search tab layout
     **/
    TabLayout searchTablayout;
    /**
     *Relative layout for search
     **/
    RelativeLayout rlIndiSearch;

    /**
     *RecyclerView view for search
     **/
    RecyclerView recyclerViewIndiSearch;


    /**
     *Search group pojo
     **/
    SearchBucketInputpojo getGrpInputPojo;

    Get_grp_input_pojo inputPojo;

    /**
     *Search group arraylist
     **/
    ArrayList<SearchBucketInputpojo> getGrpInputPojoArrayList = new ArrayList<>();
    ArrayList<Get_grp_input_pojo> grplist = new ArrayList<>();

    /**
     *Adapters for group search
     **/
    SearchGroupAdapter searchGroupAdapter;

    NewSearchGroupAdapter newSearchGroupAdapter;

    /**
     *Search group followers arraylist/ hashmap list
     **/
    ArrayList<Integer> selectedgrplist = new ArrayList<>();
    HashMap<String, Integer> selectedfollowerlistId = new HashMap<>();
    HashMap<String, Integer> selectedgroupID= new HashMap<>();
    /**
     *String search results
     **/
    String fname;
    String lname;
    String reviewid;
    String grpname;
    String grpId;
    String grpFrmfname;
    String grpFrmlname;
    int useridGetGrp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_grp_layout);

        /*Widget initialization*/

        actionBar = findViewById(R.id.action_bar_grp_search);
        imageButton = findViewById(R.id.back);
        textAdd = actionBar.findViewById(R.id.text_add);
        textAdd.setOnClickListener(this);

        llNotFound = findViewById(R.id.ll_not_found);

        llSearchTabs = findViewById(R.id.ll_search_tabs);

        groupSearchScroll = findViewById(R.id.indi_search_scroll);

        autoSearch = findViewById(R.id.auto_search);

        searchTablayout = findViewById(R.id.tab_search);

        searchContainer = findViewById(R.id.search_container);

        rlIndiSearch = findViewById(R.id.rl_indi_search);

        imageButton.setOnClickListener(this);

        setSupportActionBar(actionBar);

        /* individual bucket list done click */

        btnDone = findViewById(R.id.btn_done);
        btnDone.setOnClickListener(this);


        recyclerViewIndiSearch = findViewById(R.id.recycler_view_indi_search);

        /*Search view initialization*/
        searchView = actionBar.findViewById(R.id.grp_search_view);
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.setQueryHint("Search groups");
        searchView.requestFocusFromTouch();

        /*Getting input values from various activities*/
        Intent intent= getIntent();
        reviewid= intent.getStringExtra("reviewId");

        /* Call Group user API */

         getGroupUser();


        /*Search query listener*/
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                if (query.length() != 0) {

                    if (Utility.isConnected(SearchGroupBucketActivity.this)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                            Call<SearchBucketOutputPojo> call = apiService.get_group_search("get_group_search", Integer.parseInt(createdby),query);
                            call.enqueue(new Callback<SearchBucketOutputPojo>() {
                                @Override
                                public void onResponse(Call<SearchBucketOutputPojo> call, Response<SearchBucketOutputPojo> response) {

                                    if (response.body() != null && response.body().getResponseCode() == 1) {
                                        getGrpInputPojoArrayList.clear();

                                        for (int j = 0; j < response.body().getData().size(); j++) {

                                            if (response.body().getData().get(j).getGroupid() == null) {

                                            } else {

                                                String grpuserid = response.body().getData().get(j).getUserid();
                                                fname = response.body().getData().get(j).getFromFirstname();
                                                lname = response.body().getData().get(j).getFromLastname();
                                                String grpFrmpicture = response.body().getData().get(j).getFromPicture();
                                                String groupname = response.body().getData().get(j).getGroupName();
                                                String groupId = response.body().getData().get(j).getGroupid();
                                                String groupicon = response.body().getData().get(j).getGroupIcon();
                                                String changedWhich = response.body().getData().get(j).getChangedWhich();
                                                String groupCreatedBy = response.body().getData().get(j).getGroupCreatedby();
                                                String groupCreatedDate = response.body().getData().get(j).getCreatedDate();
                                                String groupCreatedFname = response.body().getData().get(j).getGroupCreatedFirstname();
                                                String groupCreatedLname = response.body().getData().get(j).getGroupCreatedLastname();
                                                int totalMember = response.body().getData().get(j).getTotalMember();


                                                getGrpInputPojo = new SearchBucketInputpojo("", "", groupId,
                                                        "", "", "", "", "",
                                                        "", "", grpuserid, fname, lname, grpFrmpicture, "", "",
                                                        "", "", "", "", groupname, groupicon, changedWhich, groupCreatedBy,
                                                        groupCreatedDate, groupCreatedFname, groupCreatedLname, totalMember);

                                                getGrpInputPojoArrayList.add(getGrpInputPojo);

                                                /*Setting adapter*/

                                                searchGroupAdapter = new SearchGroupAdapter(SearchGroupBucketActivity.this, getGrpInputPojoArrayList, selectedfollowerlistId);
                                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchGroupBucketActivity.this);
                                                recyclerViewIndiSearch.setLayoutManager(mLayoutManager);
                                                recyclerViewIndiSearch.setHasFixedSize(true);
                                                recyclerViewIndiSearch.setItemAnimator(new DefaultItemAnimator());
                                                recyclerViewIndiSearch.setAdapter(searchGroupAdapter);
                                                searchGroupAdapter.notifyDataSetChanged();
                                                recyclerViewIndiSearch.setNestedScrollingEnabled(false);
                                                recyclerViewIndiSearch.setVisibility(View.VISIBLE);


                                            }


                                        }
                                    }

                                }

                                @Override
                                public void onFailure(@NonNull Call<SearchBucketOutputPojo> call, @NonNull Throwable t) {
                                    //Error
                                    Log.e(TAG, "FailureError-->" + t.getMessage());

                                    llNotFound.setVisibility(View.VISIBLE);
                                    textAdd.setVisibility(View.GONE);

                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(rlIndiSearch, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        View view1 = snackbar.getView();
                        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                        textview.setTextColor(Color.WHITE);
                        snackbar.show();
                    }


                } else {

                    getGroupUser();

                }


                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText.length() == 0) {
                    getGroupUser();

                }

               else {

                    if (Utility.isConnected(SearchGroupBucketActivity.this)) {
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        try {

                            String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                            Call<SearchBucketOutputPojo> call = apiService.get_group_search("get_group_search", Integer.parseInt(createdby),newText);
                            call.enqueue(new Callback<SearchBucketOutputPojo>() {
                                @Override
                                public void onResponse(Call<SearchBucketOutputPojo> call, Response<SearchBucketOutputPojo> response) {

                                    if (response.body().getResponseCode() == 1) {
                                        getGrpInputPojoArrayList.clear();

                                        for (int j = 0; j < response.body().getData().size(); j++) {

                                            if(response.body().getData().get(j).getGroupid()==null){

                                            }else {

                                                String grpuserid = response.body().getData().get(j).getUserid();
                                                fname = response.body().getData().get(j).getFromFirstname();
                                                lname = response.body().getData().get(j).getFromLastname();
                                                String grpFrmpicture = response.body().getData().get(j).getFromPicture();
                                                String groupname = response.body().getData().get(j).getGroupName();
                                                String groupId = response.body().getData().get(j).getGroupid();
                                                String groupicon = response.body().getData().get(j).getGroupIcon();
                                                String changedWhich = response.body().getData().get(j).getChangedWhich();
                                                String groupCreatedby = response.body().getData().get(j).getGroupCreatedby();
                                                String groupCreatedDate = response.body().getData().get(j).getCreatedDate();
                                                String groupCreatedFirstname = response.body().getData().get(j).getGroupCreatedFirstname();
                                                String groupCreatedLastname = response.body().getData().get(j).getGroupCreatedLastname();
                                                int totalMember = response.body().getData().get(j).getTotalMember();


                                                getGrpInputPojo = new SearchBucketInputpojo("","",groupId,
                                                        "","","","","",
                                                        "","",grpuserid,fname,lname,grpFrmpicture,"","",
                                                        "","","","",groupname,groupicon,changedWhich,groupCreatedby,
                                                        groupCreatedDate,groupCreatedFirstname,groupCreatedLastname,totalMember);

                                                getGrpInputPojoArrayList.add(getGrpInputPojo);

                                                /*Setting adapter*/

                                                searchGroupAdapter = new SearchGroupAdapter(SearchGroupBucketActivity.this, getGrpInputPojoArrayList, selectedfollowerlistId);
                                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchGroupBucketActivity.this);
                                                recyclerViewIndiSearch.setLayoutManager(mLayoutManager);
                                                recyclerViewIndiSearch.setHasFixedSize(true);
                                                recyclerViewIndiSearch.setItemAnimator(new DefaultItemAnimator());
                                                recyclerViewIndiSearch.setAdapter(searchGroupAdapter);
                                                searchGroupAdapter.notifyDataSetChanged();
                                                recyclerViewIndiSearch.setNestedScrollingEnabled(false);
                                                recyclerViewIndiSearch.setVisibility(View.VISIBLE);


                                            }


                                        }
                                    }

                                }

                                @Override
                                public void onFailure(@NonNull Call<SearchBucketOutputPojo> call, @NonNull Throwable t) {
                                    //Error
                                    Log.e(TAG, "FailureError-->" + t.getMessage());

                                    llNotFound.setVisibility(View.VISIBLE);
                                    textAdd.setVisibility(View.GONE);

                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(rlIndiSearch, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        View view1 = snackbar.getView();
                        TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
                        textview.setTextColor(Color.WHITE);
                        snackbar.show();
                    }




                }


                return true;
            }


        });

    }


    /* Interface for group selecting */

    public void getgrouplist(View view, int position) {


        grpname = grplist.get(position).getGroupName();

        grpId = grplist.get(position).getGroup_id();

        if (((CheckBox) view).isChecked()) {


            selectedgroupID.put(grpname, Integer.valueOf(grplist.get(position).getGroup_id()));

            useridGetGrp = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(), "userId"));

//            selectedgrplist.add(userid);


            selectedgrplist.add(Integer.valueOf(grplist.get(position).getGroup_id()));

           /* if(selectedgrplist.toString().contains(String.valueOf(useridGetGrp))){

            }else {
                selectedgrplist.add(useridGetGrp);
            }*/
            Log.e("Grpid", "selectedgroupID:" + selectedgroupID.toString());
            Log.e("Grpidlist", "selectedgrplist:" + selectedgrplist.toString());


        } else {

            selectedgrplist.remove(Integer.valueOf(grplist.get(position).getGroup_id()));
            selectedgroupID.remove(grpname);
//            selectedgrplist.remove(useridGetGrp);
            Log.e("Grpid", "remove:" + selectedgroupID.toString());
            Log.e("Grpidlist", "selectedgrplist_remove:" + selectedgrplist.toString());

        }

    }



    /* Call Group user API */
    private void getGroupUser() {


        if (Utility.isConnected(SearchGroupBucketActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");
                Call<Get_grp_bucket_pojo> call = apiService.get_group_user("get_group_user", Integer.parseInt(createdby));
                call.enqueue(new Callback<Get_grp_bucket_pojo>() {
                    @Override
                    public void onResponse(Call<Get_grp_bucket_pojo> call, Response<Get_grp_bucket_pojo> response) {
                        if (response.body() != null && response.body().getResponseCode() == 1) {
                            grplist.clear();

                            for (int j = 0; j < response.body().getData().size(); j++) {

                                if (response.body().getData().get(j).getGroup_id() == null) {

                                } else {

                                    String grpuserid = response.body().getData().get(j).getUserid();
                                    grpFrmfname = response.body().getData().get(j).getFromFirstname();
                                    grpFrmlname = response.body().getData().get(j).getFromLastname();
                                    String grpFrmpicture = response.body().getData().get(j).getFromPicture();
                                    String groupname = response.body().getData().get(j).getGroupName();
                                    String groupId = response.body().getData().get(j).getGroup_id();
                                    String groupicon = response.body().getData().get(j).getGroupIcon();
                                    String changedWhich = response.body().getData().get(j).getChangedWhich();
                                    String groupCreatedBy = response.body().getData().get(j).getGroupCreatedby();
                                    String groupCreatedDate = response.body().getData().get(j).getCreatedDate();
                                    String groupCreatedFname = response.body().getData().get(j).getGroupCreatedFirstname();
                                    String groupCreatedLname = response.body().getData().get(j).getGroupCreatedLastname();


                                    inputPojo = new Get_grp_input_pojo(grpuserid, grpFrmfname, grpFrmlname,
                                            grpFrmpicture, groupId, groupname, groupicon, changedWhich,
                                            groupCreatedBy, groupCreatedDate, groupCreatedFname, groupCreatedLname);

                                    grplist.add(inputPojo);




                                }
                                /*Setting adapter*/

                                newSearchGroupAdapter = new NewSearchGroupAdapter(SearchGroupBucketActivity.this, grplist, selectedfollowerlistId);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchGroupBucketActivity.this);
                                recyclerViewIndiSearch.setLayoutManager(mLayoutManager);
                                recyclerViewIndiSearch.setHasFixedSize(true);
                                recyclerViewIndiSearch.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewIndiSearch.setAdapter(newSearchGroupAdapter);
                                recyclerViewIndiSearch.setNestedScrollingEnabled(false);
                                recyclerViewIndiSearch.setVisibility(View.VISIBLE);
                                newSearchGroupAdapter.notifyDataSetChanged();

                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Get_grp_bucket_pojo> call, Throwable t) {
                        //Error
                        Log.e(TAG, "FailureError-->" + t.getMessage());

                        llNotFound.setVisibility(View.VISIBLE);
                        textAdd.setVisibility(View.GONE);

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlIndiSearch, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }




    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {


            case R.id.back:

                finish();

                break;

            case R.id.text_add:

                if(!selectedgrplist.isEmpty()){
                    create_bucket_group();
                }else {
                    Toast.makeText(this, "Atleast one group need to be selected", Toast.LENGTH_SHORT).show();
                }

                break;

        }
    }


    //create_bucket_group API
    private void create_bucket_group() {
        /*if (isConnected(SearchGroupBucketActivity.this)) {
            int post_type = 3;

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(getApplicationContext(), "userId");

//            String username= grpFrmfname.concat(" ").concat(grpFrmlname);
                String groupname = grpname;


                call = apiService.create_bucket_group("create_bucket_group", Integer.parseInt(reviewId), 0, groupname, Integer.parseInt(createuserid), selectedgrplist, post_type);
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1) {
                            new SweetAlertDialog(SearchGroupBucketActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!!")
                                    .setContentText("Added to your group's bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            startActivity(new Intent(SearchGroupBucketActivity.this, MyBucketlistActivity.class));
                                            finish();

                                        }
                                    })
                                    .show();


                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlIndiSearch, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }*/

    }





    /*Fragmemt replacing container*/
    public void replaceFragment(Fragment fragment) {

        try {
            FragmentManager fm =getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.search_container, fragment);
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
