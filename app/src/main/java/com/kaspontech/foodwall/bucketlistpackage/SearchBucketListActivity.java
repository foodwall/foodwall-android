/*
package com.kaspontech.foodwall.bucketListPackage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaspontech.foodwall.adapters.Bucket_Adapter.GroupBucketAdapter;
import com.kaspontech.foodwall.adapters.Bucket_Adapter.IndividualFollowerAdapter;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.Get_grp_bucket_pojo;
import com.kaspontech.foodwall.modelclasses.BucketList_pojo.Get_grp_input_pojo;
import com.kaspontech.foodwall.modelclasses.CommonOutputPojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_follower_output_pojo;
import com.kaspontech.foodwall.modelclasses.Profile_follow_Pojo.Get_followerlist_pojo;
import com.kaspontech.foodwall.R;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;



import com.kaspontech.foodwall.utills.Pref_storage;
import com.kaspontech.foodwall.utills.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchBucketListActivity extends AppCompatActivity implements View.OnClickListener,android.support.v7.widget.SearchView.OnQueryTextListener {


    private static final String TAG = "SearchBucketList";
    // Action Bar
    Toolbar actionBar;
    TextView toolbarTitle,toolbarNext ;

    ImageButton back,imgBucketClick;
    Button btnDone,btnDoneGrp;

    ImageView imgBucketUser;

    RecyclerView rvFriends,rvGroupsBucket,rvFollowerGrpSearch;
    RelativeLayout llMyBucketList,rlCreateBucketList;

    LinearLayoutManager llm;

    android.support.v7.widget.SearchView search_follow_grp;

    Get_followerlist_pojo getFollowerlistPojo;
    List<Get_followerlist_pojo> followerlist = new ArrayList<>();


    Get_grp_input_pojo getGrpInputPojo;
    ArrayList<Get_grp_input_pojo> grplist = new ArrayList<>();



    ArrayList<Integer> followerlistid = new ArrayList<>();
    ArrayList<Integer> selectedgrplist = new ArrayList<>();

    HashMap<String, Integer> selectedfollowerlistId = new HashMap<>();
    HashMap<String, Integer> selectedgroupID= new HashMap<>();


    IndividualFollowerAdapter individualFollowerAdapter;

    GroupBucketAdapter groupBucketAdapter;

    Call<CommonOutputPojo> call;

    String grpFrmfname,grpFrmlname;
    String fname, lname,reviewId;
    String grpname,grpId;

    int useridGetGrp;

    //Follow search list
    ArrayAdapter<Get_followerlist_pojo> adapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_bucket_list);


        actionBar = (Toolbar) findViewById(R.id.actionbar_create_bucket);

        //individual bucket list done click

        btnDone = (Button)findViewById(R.id.btnDone);
        btnDone.setOnClickListener(this);

        btnDoneGrp = (Button)findViewById(R.id.btnDoneGrp);
        btnDoneGrp.setOnClickListener(this);

        back = (ImageButton)actionBar.findViewById(R.id.back);
        back.setOnClickListener(this);

        toolbarNext = (TextView) actionBar.findViewById(R.id.toolbarNext);
        toolbarNext.setOnClickListener(this);

        imgBucketUser=(ImageView) findViewById(R.id.imgBucketUser);

        imgBucketClick=(ImageButton) findViewById(R.id.imgBucketClick);
        imgBucketClick.setOnClickListener(this);

        toolbarTitle=(TextView)findViewById(R.id.toolbarTitle);
        toolbarTitle.setText(R.string.bucket_list);

        //RelativeLayout
        llMyBucketList= (RelativeLayout) findViewById(R.id.llMyBucketList);
        llMyBucketList.setOnClickListener(this);

        rlCreateBucketList= (RelativeLayout) findViewById(R.id.rlCreateBucketList);

        //RecyclerView
        rvFriends = (RecyclerView) findViewById(R.id.rvFriends);


        //Search View
        search_follow_grp = (android.support.v7.widget.SearchView) findViewById(R.id.search_follow_grp);
        search_follow_grp.setOnQueryTextListener(this);

        llm = new LinearLayoutManager(this);


        rvGroupsBucket = (RecyclerView) findViewById(R.id.rvGroupsBucket);
        rvFollowerGrpSearch = (RecyclerView) findViewById(R.id.rvFollowerGrpSearch);


        Intent intent = getIntent();
        fname=   intent.getStringExtra("f_name");
        lname= intent.getStringExtra("l_name");
        reviewId= intent.getStringExtra("reviewId");


        //Load self Image

        Utility.picassoImageLoader(Pref_storage.getDetail(getApplicationContext(),"picture_user_login"),
                0,imgBucketUser, getApplicationContext());
//        GlideApp.with(getApplicationContext()).load(Pref_storage.getDetail(getApplicationContext(),"picture_user_login")).
//                centerInside().into(imgBucketUser);

        //API follwer

        get_group_user();
        get_follower();

    }


    //Get follower
    private void get_follower() {


        if (isConnected(SearchBucketListActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_follower_output_pojo> call = apiService.get_follower("get_follower", Integer.parseInt(createdby));
                call.enqueue(new Callback<Get_follower_output_pojo>() {
                    @Override
                    public void onResponse(Call<Get_follower_output_pojo> call, Response<Get_follower_output_pojo> response) {

                        if (response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {


                                if(response.body().getData().get(j).getFollowerId()==null){

                                }else {

                                    followerlist = response.body().getData();
                                    individualFollowerAdapter = new IndividualFollowerAdapter(SearchBucketListActivity.this, followerlist,selectedfollowerlistId);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchBucketListActivity.this);
                                    rvFriends.setLayoutManager(mLayoutManager);
                                    rvFriends.setHasFixedSize(true);
                                    rvFriends.setItemAnimator(new DefaultItemAnimator());
                                    rvFriends.setAdapter(individualFollowerAdapter);
                                    rvFriends.setNestedScrollingEnabled(false);
                                    rvFriends.setVisibility(View.VISIBLE);

                                }


                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Get_follower_output_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    //get_group_user
    private void get_group_user() {


        if (isConnected(SearchBucketListActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(), "userId");

                Call<Get_grp_bucket_pojo> call = apiService.get_group_user("get_group_user", Integer.parseInt(createdby));
                call.enqueue(new Callback<Get_grp_bucket_pojo>() {
                    @Override
                    public void onResponse(Call<Get_grp_bucket_pojo> call, Response<Get_grp_bucket_pojo> response) {

                        if (response.body().getResponseCode() == 1) {

                            for (int j = 0; j < response.body().getData().size(); j++) {
                       */
/*     getGrpInputPojo= new Get_grp_input_pojo("","","","","",
                                    "","","","","","");

*//*

                                String grpuserid = response.body().getData().get(j).getUserid();
                                grpFrmfname = response.body().getData().get(j).getFromFirstname();
                                grpFrmlname = response.body().getData().get(j).getFromLastname();
                                String grp_frmpicture = response.body().getData().get(j).getFromPicture();
                                String groupname = response.body().getData().get(j).getGroupName();
                                String group_id = response.body().getData().get(j).getGroup_id();
                                String groupicon = response.body().getData().get(j).getGroupIcon();
                                String changed_which= response.body().getData().get(j).getChangedWhich();
                                String group_created_by= response.body().getData().get(j).getGroupCreatedby();
                                String group_created_date= response.body().getData().get(j).getCreatedDate();
                                String group_created_fname= response.body().getData().get(j).getGroupCreatedFirstname();
                                String group_created_lname= response.body().getData().get(j).getGroupCreatedLastname();



                                getGrpInputPojo= new Get_grp_input_pojo(grpuserid,grpFrmfname,grpFrmlname,
                                        grp_frmpicture,group_id,groupname,groupicon,changed_which,
                                        group_created_by,group_created_date,group_created_fname,group_created_lname);

                                grplist.add(getGrpInputPojo);
                                groupBucketAdapter = new GroupBucketAdapter(SearchBucketListActivity.this, grplist,selectedfollowerlistId);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchBucketListActivity.this);
                                rvGroupsBucket.setLayoutManager(mLayoutManager);
                                rvGroupsBucket.setHasFixedSize(true);
                                rvGroupsBucket.setItemAnimator(new DefaultItemAnimator());
                                rvGroupsBucket.setAdapter(groupBucketAdapter);
                                rvGroupsBucket.setNestedScrollingEnabled(false);
                                rvGroupsBucket.setVisibility(View.VISIBLE);


                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Get_grp_bucket_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }




//Interface for individual follower

    public void getSelectedfollower(View view, int position) {

        String followerName;
        followerName = followerlist.get(position).getFirstName() + " " + followerlist.get(position).getLastName();


        if (((CheckBox) view).isChecked()) {

            if (selectedfollowerlistId.containsValue(Integer.valueOf(followerlist.get(position).getFollowerId()))) {

                return;

            } else {

                selectedfollowerlistId.put(followerName, Integer.valueOf(followerlist.get(position).getFollowerId()));

                int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(),"userId"));

                followerlistid.add(Integer.valueOf(followerlist.get(position).getFollowerId()));


                //Your id for individual chat

                if(followerlistid.toString().contains(String.valueOf(userid))){

                }else {
                    followerlistid.add(userid);
                }

                Log.e(TAG, "getSelectedfollower:"+selectedfollowerlistId.toString() );
                Log.e(TAG, "followerlistid:"+followerlistid.toString() );

//                followerlistid.add(userid);

            }


        } else {
            followerlistid.remove(Integer.valueOf(followerlist.get(position).getFollowerId()));
            selectedfollowerlistId.remove(followerName);

        }

    }

//Interface for group selecting

    public void getgrouplist(View view, int position) {


        grpname = grplist.get(position).getGroupName() ;

        grpId = grplist.get(position).getGroup_id() ;

        if (((CheckBox) view).isChecked()) {


            selectedgroupID.put(grpname, Integer.valueOf(grplist.get(position).getGroup_id()));

            useridGetGrp = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(),"userId"));

//            selectedgrplist.add(userid);



            selectedgrplist.add(Integer.valueOf(grplist.get(position).getGroup_id()));

           */
/* if(selectedgrplist.toString().contains(String.valueOf(useridGetGrp))){

            }else {
                selectedgrplist.add(useridGetGrp);
            }*//*

            Log.e(TAG, "selectedgroupID:"+selectedgroupID.toString() );
            Log.e(TAG, "selectedgrplist:"+selectedgrplist.toString() );


        } else {

            selectedgrplist.remove(Integer.valueOf(grplist.get(position).getGroup_id()));
            selectedgroupID.remove(grpname);
//            selectedgrplist.remove(useridGetGrp);
            Log.e(TAG, "remove:"+selectedgroupID.toString() );
            Log.e(TAG, "selectedgrplist_remove:"+selectedgrplist.toString() );

        }

    }





    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }


    @Override
    public void onClick(View v) {

        int newid= v.getId();

        switch (newid){


            case R.id.llMyBucketList:

                create_bucket_myself();

                break;

            case R.id.imgBucketClick:

                create_bucket_myself();

                break;


            case R.id.back:
                finish();
                break;


            case R.id.toolbarNext:

                break;

            case R.id.btnDoneGrp:
//                  int userid = Integer.parseInt(Pref_storage.getDetail(getApplicationContext(),"userId"));

                if(selectedgrplist.size()!=0){
                    create_bucket_group();
                }else {
                    Toast.makeText(this, "Atleast one group need to be selected", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.btnDone:

                if(followerlistid.size()!=0){
                    create_bucket_individual_user();
                }else {
                    Toast.makeText(this, "Atleast one member to be selected", Toast.LENGTH_SHORT).show();
                }

                break;

        }



    }
    //create_bucket_individual_user API
    private void create_bucket_individual_user() {
        if (isConnected(SearchBucketListActivity.this)) {
            int  post_type=2;

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(getApplicationContext(),"userId");

                String username= fname.concat(" ").concat(lname);


                call = apiService.create_bucket_individual_user("create_bucket_individual_user", Integer.parseInt(reviewId),username, Integer.parseInt(createuserid),followerlistid,post_type);
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1) {
                            new SweetAlertDialog(SearchBucketListActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!!")
                                    .setContentText("Added to your friend's bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            startActivity(new Intent(SearchBucketListActivity.this,MyBucketlistActivity.class));
                                            finish();

                                        }
                                    })
                                    .show();


                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }


    //create_bucket_group API
    private void create_bucket_group() {
        if (isConnected(SearchBucketListActivity.this)) {
            int  post_type=3;

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(getApplicationContext(),"userId");

//            String username= grpFrmfname.concat(" ").concat(grpFrmlname);
                String groupname= grpname;


                call = apiService.create_bucket_group("create_bucket_group", Integer.parseInt(reviewId),0,groupname, Integer.parseInt(createuserid),selectedgrplist,post_type);
                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1) {
                            new SweetAlertDialog(SearchBucketListActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!!")
                                    .setContentText("Added to your group's bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            startActivity(new Intent(SearchBucketListActivity.this,MyBucketlistActivity.class));
                                            finish();

                                        }
                                    })
                                    .show();


                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }

    //create_bucket_myself API
    private void create_bucket_myself(){
        if (isConnected(SearchBucketListActivity.this)) {
            int  post_type=1;

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {
                String createuserid = Pref_storage.getDetail(getApplicationContext(),"userId");


                String username= fname.concat(" ").concat(lname);

                call = apiService.create_bucket_myself("create_bucket_myself", Integer.parseInt(reviewId),username, Integer.parseInt(createuserid),post_type);

                call.enqueue(new Callback<CommonOutputPojo>() {
                    @Override
                    public void onResponse(Call<CommonOutputPojo> call, Response<CommonOutputPojo> response) {

                        if (response.body().getResponseCode() == 1) {
                            new SweetAlertDialog(SearchBucketListActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Success!!")
                                    .setContentText("Added to your bucket list!!")

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            startActivity(new Intent(SearchBucketListActivity.this,MyBucketlistActivity.class));
                                            finish();

                                        }
                                    })
                                    .show();


                        }

                    }

                    @Override
                    public void onFailure(Call<CommonOutputPojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError", "" + t.getMessage());
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }} else {
            Snackbar snackbar = Snackbar.make(rlCreateBucketList, "Loading failed. Please check your internet connection.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            View view1 = snackbar.getView();
            TextView textview = view1.findViewById(android.support.design.R.id.snackbar_text);
            textview.setTextColor(Color.WHITE);
            snackbar.show();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        individualFollowerAdapter.getFilter().filter(newText);
        return true;


    }
}
*/
