package com.kaspontech.foodwall;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.kaspontech.foodwall.loginPackage.IMEIUtil;
import com.kaspontech.foodwall.modelclasses.Chat_Pojo.Get_chat_online_status_pojo;
import com.kaspontech.foodwall.REST_API.ApiClient;
import com.kaspontech.foodwall.REST_API.ApiInterface;
import com.kaspontech.foodwall.utills.Pref_storage;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppStatusActivity extends Activity implements LocationListener {

    protected static final String TAG = AppStatusActivity.class.getName();
    TelephonyManager telephonyManager;
    LocationManager locationManager;
    Location location;

    String imei;
    private int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;

    double latitude,longitude;

    @SuppressLint("HardwareIds")
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        showPhoneStatePermission();
        getLocation();
    }

    public static boolean isAppWentToBg = false;

    public static boolean isWindowFocused = false;

    public static boolean isMenuOpened = false;

    public static boolean isBackPressed = false;

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart isAppWentToBg " + isAppWentToBg);

        applicationWillEnterForeground();

        super.onStart();
    }

    private void applicationWillEnterForeground() {
        if (isAppWentToBg) {
            isAppWentToBg = false;
//            Toast.makeText(getApplicationContext(), "App is in foreground",
//                    Toast.LENGTH_SHORT).show();

            create_chat_login_sessiontime();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d(TAG, "onStop ");
        applicationdidenterbackground();
    }

    public void applicationdidenterbackground() {
        if (!isWindowFocused) {
            isAppWentToBg = true;
            create_chat_login_sessiontime_offline();
//            Toast.makeText(getApplicationContext(),
//                    "App is Going to Background", Toast.LENGTH_SHORT).show();
        }
    }


    //Check permission to IMEI for the device
    private void showPhoneStatePermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_PHONE_STATE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            requestPermission(MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            getDeviceIMEI();
        }
    }

    // Method to get IMEI for the device


    private void getDeviceIMEI() {
        imei = IMEIUtil.getDeviceId(this);

        Log.e(TAG, "getDeviceIMEI: "+imei );

    }

    // Permission request


    private void requestPermission(int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_PHONE_STATE}, permissionRequestCode);
    }

    /*    @Override
        public void onBackPressed() {

            if (this instanceof Home) {

            } else {
                isBackPressed = true;
            }

            Log.d(TAG,
                    "onBackPressed " + isBackPressed + ""
                            + this.getLocalClassName());
            super.onBackPressed();
        }*/

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        isWindowFocused = hasFocus;

        if (isBackPressed && !hasFocus) {
            isBackPressed = false;
            isWindowFocused = true;
        }

        super.onWindowFocusChanged(hasFocus);
    }







    private void create_chat_login_sessiontime() {
        if (isConnected(AppStatusActivity.this)){


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(),"userId");
                String imei = Pref_storage.getDetail(getApplicationContext(),"IMEI");
                if (imei==null){
                    imei="";
                }



                Call<Get_chat_online_status_pojo> call = apiService.create_chat_login_sessiontime( "create_chat_login_sessiontime",1,Integer.parseInt(createdby),imei,latitude,longitude);
                call.enqueue(new Callback<Get_chat_online_status_pojo>() {
                    @Override
                    public void onResponse(Call<Get_chat_online_status_pojo>call, Response<Get_chat_online_status_pojo> response) {

                        if (response.body().getResponseCode()==1){



                            String loggedintime=response.body().getData();



                            Log.e("loggedintime", "onResponse:"+ loggedintime);



                        }

                    }

                    @Override
                    public void onFailure(Call<Get_chat_online_status_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError",""+t.getMessage());
                    }
                });
            }catch (Exception e ){
                e.printStackTrace();
            }

        }else {

            Toast.makeText(this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }


    }
    private void create_chat_login_sessiontime_offline() {
        if (isConnected(AppStatusActivity.this)){


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            try {

                String createdby = Pref_storage.getDetail(getApplicationContext(),"userId");
                String imei = Pref_storage.getDetail(getApplicationContext(),"IMEI");
                if (imei==null){
                    imei="";
                }


                Call<Get_chat_online_status_pojo> call = apiService.create_chat_login_sessiontime( "create_chat_login_sessiontime",0,Integer.parseInt(createdby),imei ,latitude,longitude);
                call.enqueue(new Callback<Get_chat_online_status_pojo>() {
                    @Override
                    public void onResponse(Call<Get_chat_online_status_pojo>call, Response<Get_chat_online_status_pojo> response) {

                        if (response.body().getResponseCode()==1){



                            String loggedout=response.body().getData();



                            Log.e("loggedout", "loggedout:"+ loggedout);



                        }

                    }

                    @Override
                    public void onFailure(Call<Get_chat_online_status_pojo> call, Throwable t) {
                        //Error
                        Log.e("FailureError",""+t.getMessage());
                    }
                });
            }catch (Exception e ){
                e.printStackTrace();
            }

        }else {
            Toast.makeText(this, "Loading failed. Please check your internet connection.", Toast.LENGTH_SHORT).show();


        }


    }

    // Internet Check
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }
    public void getLocation() {
        try {
            locationManager = (LocationManager) AppStatusActivity.this.getSystemService(Context.LOCATION_SERVICE);
            if (locationManager != null) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, (LocationListener) this);
               latitude= location.getLatitude();
                longitude= location.getLongitude();
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
